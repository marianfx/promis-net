﻿
namespace Promis.NET.Localization
{
    public class Global
    {
        public static string[] Months = { LocalResources.Jan, LocalResources.Feb, LocalResources.Mar, LocalResources.Apr, LocalResources.May, LocalResources.Jun, LocalResources.Jul, LocalResources.Aug, LocalResources.Sep, LocalResources.Oct, LocalResources.Nov, LocalResources.Dec };
    }
}
