﻿using System.Collections.Generic;
using System.Linq;
using Promis.NET.DataAccess.DbContexts;
using Promis.NET.DataAccess.Extensions;
using Promis.NET.DataAccess.Repositories;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.Web.DataAccess.Entities.PromisWeb;
using Resources.Abstract;
using Resources.Entities;
using SqlKata;

namespace Promis.NET.Localization
{
    public class ResourceProvider: BaseResourceProvider, IResourceProvider
    {
        public static string[] SupportedLanguages = new string[5] { "en", "fr", "es", "ru", "it" };
        public static string DefaultCulture { get; set; } = Utilities.Models.Global.DefaultLanguage;

        private IRepository _repository;

        public ResourceProvider(IRepository repository)
        {
            _repository = repository;
        }

        public ResourceProvider() : this(new Repository(new PromisWebDbContext(ApplicationDbContext.useLocal))) { }

        protected override ResourceEntry ReadResource(string name, string culture)
        {
            if (!SupportedLanguages.Contains(culture))
                culture = DefaultCulture;

            if (string.IsNullOrWhiteSpace(name))
                return null;
            
            var query = new Query("translations")
                            .Select(culture)
                            .Where(DefaultCulture, name);
            var comp = _repository.GetCompiler().Compile(query);
            var result = _repository.ExecuteQuery<string>(comp.Sql, comp.Bindings).FirstOrDefault();
            if (string.IsNullOrWhiteSpace(result))
            {
                // load default english text
                query = new Query("translations")
                            .Select(DefaultCulture)
                            .Where(DefaultCulture, name);
                result = _repository.ExecuteQuery<string>(comp.Sql, comp.Bindings).FirstOrDefault();
            }

            result = !string.IsNullOrWhiteSpace(result) ? result : "Unknown text";
            return new ResourceEntry()
            {
                Name = name,
                Value = result,
                Culture = culture
            };
        }

        protected override IList<ResourceEntry> ReadResources()
        {
            var output = new List<ResourceEntry>();
            var query = new Query("translations");
            var comp = _repository.GetCompiler().Compile(query);
            var results = _repository.ExecuteQuery<translations>(comp.Sql, comp.Bindings).ToList();
            foreach (var item in results)
            {
                if (string.IsNullOrWhiteSpace(item.en))
                    continue;
                var itemName = item.en;

                output.Add(new ResourceEntry()
                {
                    Culture = "en",
                    Name = itemName,
                    Value = item.en
                });

                var vallToAddEs = string.IsNullOrWhiteSpace(item.es) ? item.en : item.es;
                output.Add(new ResourceEntry()
                {
                    Culture = "es",
                    Name = itemName,
                    Value = vallToAddEs
                });

                var vallToAddFr = string.IsNullOrWhiteSpace(item.fr) ? item.en : item.fr;
                output.Add(new ResourceEntry()
                {
                    Culture = "fr",
                    Name = itemName,
                    Value = vallToAddFr
                });

                var vallToAddIt = string.IsNullOrWhiteSpace(item.it) ? item.en : item.it;
                output.Add(new ResourceEntry()
                {
                    Culture = "it",
                    Name = itemName,
                    Value = vallToAddIt
                });

                var vallToAddRu = string.IsNullOrWhiteSpace(item.ru) ? item.en : item.ru;
                output.Add(new ResourceEntry()
                {
                    Culture = "ru",
                    Name = itemName,
                    Value = vallToAddRu
                });
            }
            return output;
        }
    }
}
