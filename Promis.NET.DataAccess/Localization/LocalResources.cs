using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resources.Abstract;
using Resources.Concrete;
    
namespace Promis.NET.Localization {
        public class LocalResources {
            private static IResourceProvider resourceProvider = new ResourceProvider();

                
        /// <summary>Project</summary>
        public static string Project {
               get {
                   return (string) resourceProvider.GetResource("Project", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Location</summary>
        public static string Location {
               get {
                   return (string) resourceProvider.GetResource("Location", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Welcome</summary>
        public static string Welcome {
               get {
                   return (string) resourceProvider.GetResource("Welcome", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Status</summary>
        public static string Status {
               get {
                   return (string) resourceProvider.GetResource("Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Construction</summary>
        public static string Construction {
               get {
                   return (string) resourceProvider.GetResource("Construction", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Engineering</summary>
        public static string Engineering {
               get {
                   return (string) resourceProvider.GetResource("Engineering", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Punch List</summary>
        public static string SystemPunchList {
               get {
                   return (string) resourceProvider.GetResource("System Punch List", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Management</summary>
        public static string Management {
               get {
                   return (string) resourceProvider.GetResource("Management", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Preservation</summary>
        public static string Preservation {
               get {
                   return (string) resourceProvider.GetResource("Preservation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Planning</summary>
        public static string Planning {
               get {
                   return (string) resourceProvider.GetResource("Planning", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Site</summary>
        public static string Site {
               get {
                   return (string) resourceProvider.GetResource("Site", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Activities</summary>
        public static string Activities {
               get {
                   return (string) resourceProvider.GetResource("Activities", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Progress</summary>
        public static string Progress {
               get {
                   return (string) resourceProvider.GetResource("Progress", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Inspections</summary>
        public static string Inspections {
               get {
                   return (string) resourceProvider.GetResource("Inspections", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Completion</summary>
        public static string Completion {
               get {
                   return (string) resourceProvider.GetResource("Completion", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mobile</summary>
        public static string Mobile {
               get {
                   return (string) resourceProvider.GetResource("Mobile", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Applications</summary>
        public static string Applications {
               get {
                   return (string) resourceProvider.GetResource("Applications", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>JAN</summary>
        public static string Jan {
               get {
                   return (string) resourceProvider.GetResource("JAN", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FEB</summary>
        public static string Feb {
               get {
                   return (string) resourceProvider.GetResource("FEB", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>MAR</summary>
        public static string Mar {
               get {
                   return (string) resourceProvider.GetResource("MAR", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>APR</summary>
        public static string Apr {
               get {
                   return (string) resourceProvider.GetResource("APR", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>MAY</summary>
        public static string May {
               get {
                   return (string) resourceProvider.GetResource("MAY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>JUN</summary>
        public static string Jun {
               get {
                   return (string) resourceProvider.GetResource("JUN", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>JUL</summary>
        public static string Jul {
               get {
                   return (string) resourceProvider.GetResource("JUL", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>AUG</summary>
        public static string Aug {
               get {
                   return (string) resourceProvider.GetResource("AUG", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SEP</summary>
        public static string Sep {
               get {
                   return (string) resourceProvider.GetResource("SEP", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>OCT</summary>
        public static string Oct {
               get {
                   return (string) resourceProvider.GetResource("OCT", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>NOV</summary>
        public static string Nov {
               get {
                   return (string) resourceProvider.GetResource("NOV", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>DEC</summary>
        public static string Dec {
               get {
                   return (string) resourceProvider.GetResource("DEC", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>JANUARY</summary>
        public static string January {
               get {
                   return (string) resourceProvider.GetResource("JANUARY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FEBRUARY</summary>
        public static string February {
               get {
                   return (string) resourceProvider.GetResource("FEBRUARY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>MARCH</summary>
        public static string March {
               get {
                   return (string) resourceProvider.GetResource("MARCH", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>APRIL</summary>
        public static string April {
               get {
                   return (string) resourceProvider.GetResource("APRIL", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>JUNE</summary>
        public static string June {
               get {
                   return (string) resourceProvider.GetResource("JUNE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>JULY</summary>
        public static string July {
               get {
                   return (string) resourceProvider.GetResource("JULY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>AUGUST</summary>
        public static string August {
               get {
                   return (string) resourceProvider.GetResource("AUGUST", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SEPTEMBER</summary>
        public static string September {
               get {
                   return (string) resourceProvider.GetResource("SEPTEMBER", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>OCTOBER</summary>
        public static string October {
               get {
                   return (string) resourceProvider.GetResource("OCTOBER", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>NOVEMBER</summary>
        public static string November {
               get {
                   return (string) resourceProvider.GetResource("NOVEMBER", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>DECEMBER</summary>
        public static string December {
               get {
                   return (string) resourceProvider.GetResource("DECEMBER", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mon</summary>
        public static string Mon {
               get {
                   return (string) resourceProvider.GetResource("Mon", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tue</summary>
        public static string Tue {
               get {
                   return (string) resourceProvider.GetResource("Tue", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Wed</summary>
        public static string Wed {
               get {
                   return (string) resourceProvider.GetResource("Wed", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Thu</summary>
        public static string Thu {
               get {
                   return (string) resourceProvider.GetResource("Thu", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Fri</summary>
        public static string Fri {
               get {
                   return (string) resourceProvider.GetResource("Fri", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sat</summary>
        public static string Sat {
               get {
                   return (string) resourceProvider.GetResource("Sat", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sun</summary>
        public static string Sun {
               get {
                   return (string) resourceProvider.GetResource("Sun", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PROGRESS OVERALL</summary>
        public static string ProgressOverall {
               get {
                   return (string) resourceProvider.GetResource("PROGRESS OVERALL", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cumulated</summary>
        public static string Cumulated {
               get {
                   return (string) resourceProvider.GetResource("Cumulated", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Site Map</summary>
        public static string SiteMap {
               get {
                   return (string) resourceProvider.GetResource("Site Map", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PreComm</summary>
        public static string Precomm {
               get {
                   return (string) resourceProvider.GetResource("PreComm", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PUNCH ITEMS THAT NEED TO BE CLEARED BEFORE THE SYSTEM MECHANICAL COMPLETION</summary>
        public static string PunchItemsThatNeedToBeClearedBeforeTheSystemMechanicalCompletion {
               get {
                   return (string) resourceProvider.GetResource("PUNCH ITEMS THAT NEED TO BE CLEARED BEFORE THE SYSTEM MECHANICAL COMPLETION", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Please, refer to the PROMIS project administrator</summary>
        public static string PleaseReferToThePromisProjectAdministrator {
               get {
                   return (string) resourceProvider.GetResource("Please, refer to the PROMIS project administrator", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Module not available according to your user profile</summary>
        public static string ModuleNotAvailableAccordingToYourUserProfile {
               get {
                   return (string) resourceProvider.GetResource("Module not available according to your user profile", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Terms of use</summary>
        public static string TermsOfUse {
               get {
                   return (string) resourceProvider.GetResource("Terms of use", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Monthly</summary>
        public static string Monthly {
               get {
                   return (string) resourceProvider.GetResource("Monthly", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>MAIN ACTIVITIES STATUS</summary>
        public static string MainActivitiesStatus {
               get {
                   return (string) resourceProvider.GetResource("MAIN ACTIVITIES STATUS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Overall</summary>
        public static string Overall {
               get {
                   return (string) resourceProvider.GetResource("Overall", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PROGRESS TRENDS</summary>
        public static string ProgressTrends {
               get {
                   return (string) resourceProvider.GetResource("PROGRESS TRENDS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Percentage</summary>
        public static string Percentage {
               get {
                   return (string) resourceProvider.GetResource("Percentage", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>TODAY</summary>
        public static string Today {
               get {
                   return (string) resourceProvider.GetResource("TODAY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>LAST WEEK</summary>
        public static string LastWeek {
               get {
                   return (string) resourceProvider.GetResource("LAST WEEK", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>LAST MONTH</summary>
        public static string LastMonth {
               get {
                   return (string) resourceProvider.GetResource("LAST MONTH", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>OUTSTANDING ACTIONS</summary>
        public static string OutstandingActions {
               get {
                   return (string) resourceProvider.GetResource("OUTSTANDING ACTIONS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CATEGORIES</summary>
        public static string Categories {
               get {
                   return (string) resourceProvider.GetResource("CATEGORIES", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>OUTS</summary>
        public static string Outs {
               get {
                   return (string) resourceProvider.GetResource("OUTS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>DONE</summary>
        public static string Done {
               get {
                   return (string) resourceProvider.GetResource("DONE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CLEARANCE</summary>
        public static string Clearance {
               get {
                   return (string) resourceProvider.GetResource("CLEARANCE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Planned</summary>
        public static string Planned {
               get {
                   return (string) resourceProvider.GetResource("Planned", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Physical</summary>
        public static string Physical {
               get {
                   return (string) resourceProvider.GetResource("Physical", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certified</summary>
        public static string Certified {
               get {
                   return (string) resourceProvider.GetResource("Certified", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Delay</summary>
        public static string Delay {
               get {
                   return (string) resourceProvider.GetResource("Delay", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PUNCH ITEMS THAT DO NOT PREVENT OR INTERFERE WITH PRE-COMMISSIONING ACTIVITIES BUT MUST BE CLEARED TO ALLOW A SAFE COMMISSIONING OF THE SYSTEM. THEY HAVE TO BE COMPLETED BEFORE SYSTEM RFC</summary>
        public static string PunchItemsThatDoNotPreventOrInterfereWithPrecommissioningActivitiesButMustBeClearedToAllowASafeCommissioningOfTheSystemTheyHaveToBeCompletedBeforeSystemRfc {
               get {
                   return (string) resourceProvider.GetResource("PUNCH ITEMS THAT DO NOT PREVENT OR INTERFERE WITH PRE-COMMISSIONING ACTIVITIES BUT MUST BE CLEARED TO ALLOW A SAFE COMMISSIONING OF THE SYSTEM. THEY HAVE TO BE COMPLETED BEFORE SYSTEM RFC", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PUNCH ITEMS THAT DO NOT PREVENT OR INTERFERE WITH COMMISSIONING ACTIVITIES. THEY HAVE TO BE COMPLETED BEFORE SYSTEM RFSU</summary>
        public static string PunchItemsThatDoNotPreventOrInterfereWithCommissioningActivitiesTheyHaveToBeCompletedBeforeSystemRfsu {
               get {
                   return (string) resourceProvider.GetResource("PUNCH ITEMS THAT DO NOT PREVENT OR INTERFERE WITH COMMISSIONING ACTIVITIES. THEY HAVE TO BE COMPLETED BEFORE SYSTEM RFSU", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PUNCH ITEMS THAT DO NOT PREVENT OR INTERFERE WITH PLANT START-UP AND FOLLOWING ACTIVITIES. THEIR CLOSURE DATE MUST BE AGREED WITH CUSTOMER</summary>
        public static string PunchItemsThatDoNotPreventOrInterfereWithPlantStartupAndFollowingActivitiesTheirClosureDateMustBeAgreedWithCustomer {
               get {
                   return (string) resourceProvider.GetResource("PUNCH ITEMS THAT DO NOT PREVENT OR INTERFERE WITH PLANT START-UP AND FOLLOWING ACTIVITIES. THEIR CLOSURE DATE MUST BE AGREED WITH CUSTOMER", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>EXPORT DATA TEMPLATE</summary>
        public static string ExportDataTemplate {
               get {
                   return (string) resourceProvider.GetResource("EXPORT DATA TEMPLATE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>EXPORT DATA</summary>
        public static string ExportData {
               get {
                   return (string) resourceProvider.GetResource("EXPORT DATA", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Project Status</summary>
        public static string ProjectStatus {
               get {
                   return (string) resourceProvider.GetResource("Project Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Planning and Progress</summary>
        public static string PlanningAndProgress {
               get {
                   return (string) resourceProvider.GetResource("Planning and Progress", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Construction and Engineering</summary>
        public static string ConstructionAndEngineering {
               get {
                   return (string) resourceProvider.GetResource("Construction and Engineering", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certification Management</summary>
        public static string CertificationManagement {
               get {
                   return (string) resourceProvider.GetResource("Certification Management", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Preservation Planning</summary>
        public static string PreservationPlanning {
               get {
                   return (string) resourceProvider.GetResource("Preservation Planning", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Site Activities</summary>
        public static string SiteActivities {
               get {
                   return (string) resourceProvider.GetResource("Site Activities", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PreCommissioning Inspections</summary>
        public static string PrecommissioningInspections {
               get {
                   return (string) resourceProvider.GetResource("PreCommissioning Inspections", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Project Completion</summary>
        public static string ProjectCompletion {
               get {
                   return (string) resourceProvider.GetResource("Project Completion", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mobile Applications</summary>
        public static string MobileApplications {
               get {
                   return (string) resourceProvider.GetResource("Mobile Applications", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>First Name</summary>
        public static string FirstName {
               get {
                   return (string) resourceProvider.GetResource("First Name", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Personal Information</summary>
        public static string PersonalInformation {
               get {
                   return (string) resourceProvider.GetResource("Personal Information", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Last Name</summary>
        public static string LastName {
               get {
                   return (string) resourceProvider.GetResource("Last Name", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Username</summary>
        public static string Username {
               get {
                   return (string) resourceProvider.GetResource("Username", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Password</summary>
        public static string Password {
               get {
                   return (string) resourceProvider.GetResource("Password", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Email</summary>
        public static string Email {
               get {
                   return (string) resourceProvider.GetResource("Email", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Office Number</summary>
        public static string OfficeNumber {
               get {
                   return (string) resourceProvider.GetResource("Office Number", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Country</summary>
        public static string Country {
               get {
                   return (string) resourceProvider.GetResource("Country", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mobile Number</summary>
        public static string MobileNumber {
               get {
                   return (string) resourceProvider.GetResource("Mobile Number", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Company</summary>
        public static string Company {
               get {
                   return (string) resourceProvider.GetResource("Company", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Interface Language</summary>
        public static string InterfaceLanguage {
               get {
                   return (string) resourceProvider.GetResource("Interface Language", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Project Configuration</summary>
        public static string ProjectConfiguration {
               get {
                   return (string) resourceProvider.GetResource("Project Configuration", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>General Information</summary>
        public static string GeneralInformation {
               get {
                   return (string) resourceProvider.GetResource("General Information", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Planning Curves</summary>
        public static string PlanningCurves {
               get {
                   return (string) resourceProvider.GetResource("Planning Curves", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cut-Off Calendar</summary>
        public static string CutoffCalendar {
               get {
                   return (string) resourceProvider.GetResource("Cut-Off Calendar", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certification Steps by Sub-Activity</summary>
        public static string CertificationStepsBySubactivity {
               get {
                   return (string) resourceProvider.GetResource("Certification Steps by Sub-Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>User Profiles</summary>
        public static string UserProfiles {
               get {
                   return (string) resourceProvider.GetResource("User Profiles", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>GENERAL INFORMATIONS</summary>
        public static string GeneralInformations {
               get {
                   return (string) resourceProvider.GetResource("GENERAL INFORMATIONS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Save</summary>
        public static string Save {
               get {
                   return (string) resourceProvider.GetResource("Save", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Code</summary>
        public static string Code {
               get {
                   return (string) resourceProvider.GetResource("Code", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Description</summary>
        public static string Description {
               get {
                   return (string) resourceProvider.GetResource("Description", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Client</summary>
        public static string Client {
               get {
                   return (string) resourceProvider.GetResource("Client", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Project Life</summary>
        public static string ProjectLife {
               get {
                   return (string) resourceProvider.GetResource("Project Life", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>First Working Day</summary>
        public static string FirstWorkingDay {
               get {
                   return (string) resourceProvider.GetResource("First Working Day", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sunday</summary>
        public static string Sunday {
               get {
                   return (string) resourceProvider.GetResource("Sunday", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Monday</summary>
        public static string Monday {
               get {
                   return (string) resourceProvider.GetResource("Monday", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tuesday</summary>
        public static string Tuesday {
               get {
                   return (string) resourceProvider.GetResource("Tuesday", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Wednesday</summary>
        public static string Wednesday {
               get {
                   return (string) resourceProvider.GetResource("Wednesday", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Thursday</summary>
        public static string Thursday {
               get {
                   return (string) resourceProvider.GetResource("Thursday", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Friday</summary>
        public static string Friday {
               get {
                   return (string) resourceProvider.GetResource("Friday", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Saturday</summary>
        public static string Saturday {
               get {
                   return (string) resourceProvider.GetResource("Saturday", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cut-Off Day</summary>
        public static string CutoffDay {
               get {
                   return (string) resourceProvider.GetResource("Cut-Off Day", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>OFFICIAL DOCUMENT LOGO</summary>
        public static string OfficialDocumentLogo {
               get {
                   return (string) resourceProvider.GetResource("OFFICIAL DOCUMENT LOGO", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>HOMEPAGE LOGO</summary>
        public static string HomepageLogo {
               get {
                   return (string) resourceProvider.GetResource("HOMEPAGE LOGO", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>MAIN ACTIVITIES</summary>
        public static string MainActivities {
               get {
                   return (string) resourceProvider.GetResource("MAIN ACTIVITIES", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ACTION</summary>
        public static string Action {
               get {
                   return (string) resourceProvider.GetResource("ACTION", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>View All Activities</summary>
        public static string ViewAllActivities {
               get {
                   return (string) resourceProvider.GetResource("View All Activities", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>View Activities with MHRS>0</summary>
        public static string ViewActivitiesWithMhrs0 {
               get {
                   return (string) resourceProvider.GetResource("View Activities with MHRS>0", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Baseline/Planning Curves Import</summary>
        public static string BaselineplanningCurvesImport {
               get {
                   return (string) resourceProvider.GetResource("Baseline/Planning Curves Import", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Data Structure Export</summary>
        public static string DataStructureExport {
               get {
                   return (string) resourceProvider.GetResource("Data Structure Export", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Export Selected Curves</summary>
        public static string ExportSelectedCurves {
               get {
                   return (string) resourceProvider.GetResource("Export Selected Curves", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Save Changes</summary>
        public static string SaveChanges {
               get {
                   return (string) resourceProvider.GetResource("Save Changes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>EXISTING USERS</summary>
        public static string ExistingUsers {
               get {
                   return (string) resourceProvider.GetResource("EXISTING USERS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Delete</summary>
        public static string Delete {
               get {
                   return (string) resourceProvider.GetResource("Delete", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>New User</summary>
        public static string NewUser {
               get {
                   return (string) resourceProvider.GetResource("New User", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Personal Info</summary>
        public static string PersonalInfo {
               get {
                   return (string) resourceProvider.GetResource("Personal Info", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Upload</summary>
        public static string Upload {
               get {
                   return (string) resourceProvider.GetResource("Upload", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Modules Available</summary>
        public static string ModulesAvailable {
               get {
                   return (string) resourceProvider.GetResource("Modules Available", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Hide</summary>
        public static string Hide {
               get {
                   return (string) resourceProvider.GetResource("Hide", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Available</summary>
        public static string Available {
               get {
                   return (string) resourceProvider.GetResource("Available", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Write / Modify</summary>
        public static string WriteModify {
               get {
                   return (string) resourceProvider.GetResource("Write / Modify", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Promis User</summary>
        public static string PromisUser {
               get {
                   return (string) resourceProvider.GetResource("Promis User", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Standard</summary>
        public static string Standard {
               get {
                   return (string) resourceProvider.GetResource("Standard", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Admin</summary>
        public static string Admin {
               get {
                   return (string) resourceProvider.GetResource("Admin", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Follow-Up Responsibility</summary>
        public static string FollowupResponsibility {
               get {
                   return (string) resourceProvider.GetResource("Follow-Up Responsibility", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Contractor</summary>
        public static string Contractor {
               get {
                   return (string) resourceProvider.GetResource("Contractor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Subcontractor</summary>
        public static string Subcontractor {
               get {
                   return (string) resourceProvider.GetResource("Subcontractor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Not Allowed</summary>
        public static string NotAllowed {
               get {
                   return (string) resourceProvider.GetResource("Not Allowed", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Permitted</summary>
        public static string Permitted {
               get {
                   return (string) resourceProvider.GetResource("Permitted", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>DELETE RECORD</summary>
        public static string DeleteRecord {
               get {
                   return (string) resourceProvider.GetResource("DELETE RECORD", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Delete selected record</summary>
        public static string DeleteSelectedRecord {
               get {
                   return (string) resourceProvider.GetResource("Delete selected record", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Are you sure?</summary>
        public static string AreYouSure {
               get {
                   return (string) resourceProvider.GetResource("Are you sure?", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ADD NEW RECORD</summary>
        public static string AddNewRecord {
               get {
                   return (string) resourceProvider.GetResource("ADD NEW RECORD", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>EDIT RECORD</summary>
        public static string EditRecord {
               get {
                   return (string) resourceProvider.GetResource("EDIT RECORD", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ADD SIMILAR RECORD</summary>
        public static string AddSimilarRecord {
               get {
                   return (string) resourceProvider.GetResource("ADD SIMILAR RECORD", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Activity Budget MHRS</summary>
        public static string ActivityBudgetMhrs {
               get {
                   return (string) resourceProvider.GetResource("Activity Budget MHRS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Activity Planned MHRS</summary>
        public static string ActivityPlannedMhrs {
               get {
                   return (string) resourceProvider.GetResource("Activity Planned MHRS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Balance</summary>
        public static string Balance {
               get {
                   return (string) resourceProvider.GetResource("Balance", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ERROR</summary>
        public static string Error {
               get {
                   return (string) resourceProvider.GetResource("ERROR", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>INVALID CHARACTERS...</summary>
        public static string InvalidCharacters {
               get {
                   return (string) resourceProvider.GetResource("INVALID CHARACTERS...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>INVALID NUMBERS...</summary>
        public static string InvalidNumbers {
               get {
                   return (string) resourceProvider.GetResource("INVALID NUMBERS...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>INVALID RANGE...</summary>
        public static string InvalidRange {
               get {
                   return (string) resourceProvider.GetResource("INVALID RANGE...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SOME REQUIRED DATA ARE EMPTY...</summary>
        public static string SomeRequiredDataAreEmpty {
               get {
                   return (string) resourceProvider.GetResource("SOME REQUIRED DATA ARE EMPTY...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Import Excel Data For</summary>
        public static string ImportExcelDataFor {
               get {
                   return (string) resourceProvider.GetResource("Import Excel Data For", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Choose File</summary>
        public static string ChooseFile {
               get {
                   return (string) resourceProvider.GetResource("Choose File", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Execute</summary>
        public static string Execute {
               get {
                   return (string) resourceProvider.GetResource("Execute", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FOOTING</summary>
        public static string Footing {
               get {
                   return (string) resourceProvider.GetResource("FOOTING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CUM</summary>
        public static string Cum {
               get {
                   return (string) resourceProvider.GetResource("CUM", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ELEVATION</summary>
        public static string Elevation {
               get {
                   return (string) resourceProvider.GetResource("ELEVATION", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Update Profile</summary>
        public static string UpdateProfile {
               get {
                   return (string) resourceProvider.GetResource("Update Profile", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CLICK ON ICON TO DOWNLOAD</summary>
        public static string ClickOnIconToDownload {
               get {
                   return (string) resourceProvider.GetResource("CLICK ON ICON TO DOWNLOAD", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>YOU HAVE CHOSEN A MULTILEVEL STRUCTURE...OR NOT ?</summary>
        public static string YouHaveChosenAMultilevelStructureorNot {
               get {
                   return (string) resourceProvider.GetResource("YOU HAVE CHOSEN A MULTILEVEL STRUCTURE...OR NOT ?", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PEDESTALS</summary>
        public static string Pedestals {
               get {
                   return (string) resourceProvider.GetResource("PEDESTALS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>2 mt</summary>
        public static string Mt {
               get {
                   return (string) resourceProvider.GetResource("2 mt", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>WALLS</summary>
        public static string Walls {
               get {
                   return (string) resourceProvider.GetResource("WALLS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SLABS</summary>
        public static string Slabs {
               get {
                   return (string) resourceProvider.GetResource("SLABS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Search</summary>
        public static string Search {
               get {
                   return (string) resourceProvider.GetResource("Search", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>(EACH CUM)</summary>
        public static string EachCum {
               get {
                   return (string) resourceProvider.GetResource("(EACH CUM)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>LEVELS </summary>
        public static string Levels {
               get {
                   return (string) resourceProvider.GetResource("LEVELS ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CHAR - N</summary>
        public static string CharN {
               get {
                   return (string) resourceProvider.GetResource("CHAR - N", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>TO</summary>
        public static string To {
               get {
                   return (string) resourceProvider.GetResource("TO", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ALIGNMENT FROM</summary>
        public static string AlignmentFrom {
               get {
                   return (string) resourceProvider.GetResource("ALIGNMENT FROM", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ELEVATION (EACH CUM)</summary>
        public static string ElevationEachCum {
               get {
                   return (string) resourceProvider.GetResource("ELEVATION (EACH CUM)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>COLUMNS</summary>
        public static string Columns {
               get {
                   return (string) resourceProvider.GetResource("COLUMNS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>BEAMS</summary>
        public static string Beams {
               get {
                   return (string) resourceProvider.GetResource("BEAMS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tags Assignment Status</summary>
        public static string TagsAssignmentStatus {
               get {
                   return (string) resourceProvider.GetResource("Tags Assignment Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nr.</summary>
        public static string Nr {
               get {
                   return (string) resourceProvider.GetResource("Nr.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PEDESTAL</summary>
        public static string Pedestal {
               get {
                   return (string) resourceProvider.GetResource("PEDESTAL", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Proposed tags</summary>
        public static string ProposedTags {
               get {
                   return (string) resourceProvider.GetResource("Proposed tags", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>TAG</summary>
        public static string Tag {
               get {
                   return (string) resourceProvider.GetResource("TAG", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PARENT</summary>
        public static string Parent {
               get {
                   return (string) resourceProvider.GetResource("PARENT", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>QTY</summary>
        public static string Qty {
               get {
                   return (string) resourceProvider.GetResource("QTY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Assign SOW to SubContractors</summary>
        public static string AssignSowToSubcontractors {
               get {
                   return (string) resourceProvider.GetResource("Assign SOW to SubContractors", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Assign Certification Plan to Tags</summary>
        public static string AssignCertificationPlanToTags {
               get {
                   return (string) resourceProvider.GetResource("Assign Certification Plan to Tags", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certification Summary</summary>
        public static string CertificationSummary {
               get {
                   return (string) resourceProvider.GetResource("Certification Summary", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tags Assignment Status as per Main Activity</summary>
        public static string TagsAssignmentStatusAsPerMainActivity {
               get {
                   return (string) resourceProvider.GetResource("Tags Assignment Status as per Main Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Assign Certification Plan to Tag</summary>
        public static string AssignCertificationPlanToTag {
               get {
                   return (string) resourceProvider.GetResource("Assign Certification Plan to Tag", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Each Tag must be linked to a specific Certification Plan...</summary>
        public static string EachTagMustBeLinkedToASpecificCertificationPlan {
               get {
                   return (string) resourceProvider.GetResource("Each Tag must be linked to a specific Certification Plan...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Main Activity</summary>
        public static string MainActivity {
               get {
                   return (string) resourceProvider.GetResource("Main Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sub Activity</summary>
        public static string SubActivity {
               get {
                   return (string) resourceProvider.GetResource("Sub Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Datatable</summary>
        public static string Datatable {
               get {
                   return (string) resourceProvider.GetResource("Datatable", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certification Status</summary>
        public static string CertificationStatus {
               get {
                   return (string) resourceProvider.GetResource("Certification Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Digital Signatures status relevant to each TAG assigned to a main / sub activity and in operation...</summary>
        public static string DigitalSignaturesStatusRelevantToEachTagAssignedToAMainSubActivityAndInOperation {
               get {
                   return (string) resourceProvider.GetResource("Digital Signatures status relevant to each TAG assigned to a main / sub activity and in operation...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ACTIONS</summary>
        public static string Actions {
               get {
                   return (string) resourceProvider.GetResource("ACTIONS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Save Activities by Subcontractor</summary>
        public static string SaveActivitiesBySubcontractor {
               get {
                   return (string) resourceProvider.GetResource("Save Activities by Subcontractor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Export Data to Excel</summary>
        public static string ExportDataToExcel {
               get {
                   return (string) resourceProvider.GetResource("Export Data to Excel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>MHRS DISTRIBUTION</summary>
        public static string MhrsDistribution {
               get {
                   return (string) resourceProvider.GetResource("MHRS DISTRIBUTION", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Choose a Subcontractor first</summary>
        public static string ChooseASubcontractorFirst {
               get {
                   return (string) resourceProvider.GetResource("Choose a Subcontractor first", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>TAGS NOT ASSIGNED</summary>
        public static string TagsNotAssigned {
               get {
                   return (string) resourceProvider.GetResource("TAGS NOT ASSIGNED", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Some required fields are empty</summary>
        public static string SomeRequiredFieldsAreEmpty {
               get {
                   return (string) resourceProvider.GetResource("Some required fields are empty", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>New Record Added</summary>
        public static string NewRecordAdded {
               get {
                   return (string) resourceProvider.GetResource("New Record Added", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mandatory Fields</summary>
        public static string MandatoryFields {
               get {
                   return (string) resourceProvider.GetResource("Mandatory Fields", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>EXPORT TEMPLATE</summary>
        public static string ExportTemplate {
               get {
                   return (string) resourceProvider.GetResource("EXPORT TEMPLATE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Add Similar</summary>
        public static string AddSimilar {
               get {
                   return (string) resourceProvider.GetResource("Add Similar", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Edit Selected</summary>
        public static string EditSelected {
               get {
                   return (string) resourceProvider.GetResource("Edit Selected", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Delete Selected</summary>
        public static string DeleteSelected {
               get {
                   return (string) resourceProvider.GetResource("Delete Selected", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Import Excel Data</summary>
        public static string ImportExcelData {
               get {
                   return (string) resourceProvider.GetResource("Import Excel Data", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Export Empty Template</summary>
        public static string ExportEmptyTemplate {
               get {
                   return (string) resourceProvider.GetResource("Export Empty Template", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No connection to external database...</summary>
        public static string NoConnectionToExternalDatabase {
               get {
                   return (string) resourceProvider.GetResource("No connection to external database...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Import Data from</summary>
        public static string ImportDataFrom {
               get {
                   return (string) resourceProvider.GetResource("Import Data from", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Userid / Password not recognized</summary>
        public static string UseridPasswordNotRecognized {
               get {
                   return (string) resourceProvider.GetResource("Userid / Password not recognized", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Please, try again</summary>
        public static string PleaseTryAgain {
               get {
                   return (string) resourceProvider.GetResource("Please, try again", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certification Steps by Activity</summary>
        public static string CertificationStepsByActivity {
               get {
                   return (string) resourceProvider.GetResource("Certification Steps by Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Underground Typecodes</summary>
        public static string UndergroundTypecodes {
               get {
                   return (string) resourceProvider.GetResource("Underground Typecodes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>EQUIPMENT</summary>
        public static string Equipment {
               get {
                   return (string) resourceProvider.GetResource("EQUIPMENT", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>INSTRUMENTS</summary>
        public static string Instruments {
               get {
                   return (string) resourceProvider.GetResource("INSTRUMENTS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Select SOW relevant to Subcontractors Awards</summary>
        public static string SelectSowRelevantToSubcontractorsAwards {
               get {
                   return (string) resourceProvider.GetResource("Select SOW relevant to Subcontractors Awards", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Activity</summary>
        public static string Activity {
               get {
                   return (string) resourceProvider.GetResource("Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Activity Description</summary>
        public static string ActivityDescription {
               get {
                   return (string) resourceProvider.GetResource("Activity Description", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Budget</summary>
        public static string Budget {
               get {
                   return (string) resourceProvider.GetResource("Budget", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Productivity</summary>
        public static string Productivity {
               get {
                   return (string) resourceProvider.GetResource("Productivity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Assigned to</summary>
        public static string AssignedTo {
               get {
                   return (string) resourceProvider.GetResource("Assigned to", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Configuration</summary>
        public static string Configuration {
               get {
                   return (string) resourceProvider.GetResource("Configuration", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Hydrotest Packages</summary>
        public static string HydrotestPackages {
               get {
                   return (string) resourceProvider.GetResource("Hydrotest Packages", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Instrument Loop Checks</summary>
        public static string InstrumentLoopChecks {
               get {
                   return (string) resourceProvider.GetResource("Instrument Loop Checks", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Hydrotest Packages Management</summary>
        public static string HydrotestPackagesManagement {
               get {
                   return (string) resourceProvider.GetResource("Hydrotest Packages Management", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Instrument Loop Management</summary>
        public static string InstrumentLoopManagement {
               get {
                   return (string) resourceProvider.GetResource("Instrument Loop Management", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Punch Lists Management</summary>
        public static string SystemPunchListsManagement {
               get {
                   return (string) resourceProvider.GetResource("System Punch Lists Management", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Life Cycle Setup</summary>
        public static string LifeCycleSetup {
               get {
                   return (string) resourceProvider.GetResource("Life Cycle Setup", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Life Cycles Setup</summary>
        public static string LifeCyclesSetup {
               get {
                   return (string) resourceProvider.GetResource("Life Cycles Setup", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Follow the schema updating the progress by each step...</summary>
        public static string FollowTheSchemaUpdatingTheProgressByEachStep {
               get {
                   return (string) resourceProvider.GetResource("Follow the schema updating the progress by each step...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Assign Isometrics creating the HT list and follow-up each configured step to progress...</summary>
        public static string AssignIsometricsCreatingTheHtListAndFollowupEachConfiguredStepToProgress {
               get {
                   return (string) resourceProvider.GetResource("Assign Isometrics creating the HT list and follow-up each configured step to progress...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Assign Instruments creating the LC list and follow-up each configured step to progress...</summary>
        public static string AssignInstrumentsCreatingTheLcListAndFollowupEachConfiguredStepToProgress {
               get {
                   return (string) resourceProvider.GetResource("Assign Instruments creating the LC list and follow-up each configured step to progress...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Follow-Up Items Punch List as per system up to its clearance...</summary>
        public static string FollowupItemsPunchListAsPerSystemUpToItsClearance {
               get {
                   return (string) resourceProvider.GetResource("Follow-Up Items Punch List as per system up to its clearance...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Leave 0% to bypass relative step check...</summary>
        public static string LeaveToBypassRelativeStepCheck {
               get {
                   return (string) resourceProvider.GetResource("Leave 0% to bypass relative step check...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Hydrotest Packages Steps</summary>
        public static string HydrotestPackagesSteps {
               get {
                   return (string) resourceProvider.GetResource("Hydrotest Packages Steps", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Instruments Loop Steps</summary>
        public static string InstrumentsLoopSteps {
               get {
                   return (string) resourceProvider.GetResource("Instruments Loop Steps", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Site Activities Follow-Up</summary>
        public static string SiteActivitiesFollowup {
               get {
                   return (string) resourceProvider.GetResource("Site Activities Follow-Up", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Baseline/Planning Import from Excel</summary>
        public static string BaselineplanningImportFromExcel {
               get {
                   return (string) resourceProvider.GetResource("Baseline/Planning Import from Excel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Baseline/Planning Import from Primavera</summary>
        public static string BaselineplanningImportFromPrimavera {
               get {
                   return (string) resourceProvider.GetResource("Baseline/Planning Import from Primavera", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Export Activity Curves</summary>
        public static string ExportActivityCurves {
               get {
                   return (string) resourceProvider.GetResource("Export Activity Curves", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Life Cycle Configuration</summary>
        public static string LifeCycleConfiguration {
               get {
                   return (string) resourceProvider.GetResource("Life Cycle Configuration", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ISOMETRICS</summary>
        public static string Isometrics {
               get {
                   return (string) resourceProvider.GetResource("ISOMETRICS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Unit of Measure</summary>
        public static string UnitOfMeasure {
               get {
                   return (string) resourceProvider.GetResource("Unit of Measure", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>INSTRUMENT LOOPS</summary>
        public static string InstrumentLoops {
               get {
                   return (string) resourceProvider.GetResource("INSTRUMENT LOOPS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Drawings</summary>
        public static string Drawings {
               get {
                   return (string) resourceProvider.GetResource("Drawings", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>P</summary>
        public static string P {
               get {
                   return (string) resourceProvider.GetResource("P", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Subcontractors</summary>
        public static string Subcontractors {
               get {
                   return (string) resourceProvider.GetResource("Subcontractors", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Systems / SubSystems</summary>
        public static string SystemsSubsystems {
               get {
                   return (string) resourceProvider.GetResource("Systems / SubSystems", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Piles Typecodes</summary>
        public static string PilesTypecodes {
               get {
                   return (string) resourceProvider.GetResource("Piles Typecodes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Inspection Actions</summary>
        public static string InspectionActions {
               get {
                   return (string) resourceProvider.GetResource("Inspection Actions", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PROCESS INSTRUMENTATION DIAGRAM</summary>
        public static string ProcessInstrumentationDiagram {
               get {
                   return (string) resourceProvider.GetResource("PROCESS INSTRUMENTATION DIAGRAM", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Activity Plans</summary>
        public static string ActivityPlans {
               get {
                   return (string) resourceProvider.GetResource("Activity Plans", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Start-Up Packages</summary>
        public static string StartupPackages {
               get {
                   return (string) resourceProvider.GetResource("Start-Up Packages", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Area List</summary>
        public static string AreaList {
               get {
                   return (string) resourceProvider.GetResource("Area List", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Unit List</summary>
        public static string UnitList {
               get {
                   return (string) resourceProvider.GetResource("Unit List", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Subcontractor SOW updated</summary>
        public static string SubcontractorSowUpdated {
               get {
                   return (string) resourceProvider.GetResource("Subcontractor SOW updated", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PIPING INSULATION</summary>
        public static string PipingInsulation {
               get {
                   return (string) resourceProvider.GetResource("PIPING INSULATION", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Activity Forms</summary>
        public static string ActivityForms {
               get {
                   return (string) resourceProvider.GetResource("Activity Forms", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SOIL PREPARATION</summary>
        public static string SoilPreparation {
               get {
                   return (string) resourceProvider.GetResource("SOIL PREPARATION", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FOOTINGS FROM</summary>
        public static string FootingsFrom {
               get {
                   return (string) resourceProvider.GetResource("FOOTINGS FROM", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PEDESTALS FROM</summary>
        public static string PedestalsFrom {
               get {
                   return (string) resourceProvider.GetResource("PEDESTALS FROM", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FOUNDATION BEAMS</summary>
        public static string FoundationBeams {
               get {
                   return (string) resourceProvider.GetResource("FOUNDATION BEAMS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FLOOR FROM</summary>
        public static string FloorFrom {
               get {
                   return (string) resourceProvider.GetResource("FLOOR FROM", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>STAIR FROM</summary>
        public static string StairFrom {
               get {
                   return (string) resourceProvider.GetResource("STAIR FROM", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certification Management for Construction Phase</summary>
        public static string CertificationManagementForConstructionPhase {
               get {
                   return (string) resourceProvider.GetResource("Certification Management for Construction Phase", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Hydrotest Management</summary>
        public static string HydrotestManagement {
               get {
                   return (string) resourceProvider.GetResource("Hydrotest Management", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Loop Checks Management</summary>
        public static string LoopChecksManagement {
               get {
                   return (string) resourceProvider.GetResource("Loop Checks Management", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Activities Status</summary>
        public static string ActivitiesStatus {
               get {
                   return (string) resourceProvider.GetResource("Activities Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Hydrotest Packages Status</summary>
        public static string HydrotestPackagesStatus {
               get {
                   return (string) resourceProvider.GetResource("Hydrotest Packages Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Hydrotest Packages ISO Assignment</summary>
        public static string HydrotestPackagesIsoAssignment {
               get {
                   return (string) resourceProvider.GetResource("Hydrotest Packages ISO Assignment", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Outstanding Actions by System</summary>
        public static string OutstandingActionsBySystem {
               get {
                   return (string) resourceProvider.GetResource("Outstanding Actions by System", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Outstanding Action Status</summary>
        public static string OutstandingActionStatus {
               get {
                   return (string) resourceProvider.GetResource("Outstanding Action Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Construction Planning &amp; Progress</summary>
        public static string ConstructionPlanningAmpProgress {
               get {
                   return (string) resourceProvider.GetResource("Construction Planning &amp; Progress", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Hydrotest Packages Creation</summary>
        public static string HydrotestPackagesCreation {
               get {
                   return (string) resourceProvider.GetResource("Hydrotest Packages Creation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ISO Assignment</summary>
        public static string IsoAssignment {
               get {
                   return (string) resourceProvider.GetResource("ISO Assignment", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PILES</summary>
        public static string Piles {
               get {
                   return (string) resourceProvider.GetResource("PILES", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Testpack Code</summary>
        public static string TestpackCode {
               get {
                   return (string) resourceProvider.GetResource("Testpack Code", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Existing Testpacks</summary>
        public static string ExistingTestpacks {
               get {
                   return (string) resourceProvider.GetResource("Existing Testpacks", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Save New</summary>
        public static string SaveNew {
               get {
                   return (string) resourceProvider.GetResource("Save New", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Testpack Code is required</summary>
        public static string TestpackCodeIsRequired {
               get {
                   return (string) resourceProvider.GetResource("Testpack Code is required", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Duplicated Testpack Code</summary>
        public static string DuplicatedTestpackCode {
               get {
                   return (string) resourceProvider.GetResource("Duplicated Testpack Code", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>DELETE TESTPACK</summary>
        public static string DeleteTestpack {
               get {
                   return (string) resourceProvider.GetResource("DELETE TESTPACK", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Delete selected Testpack and its associated ISOs</summary>
        public static string DeleteSelectedTestpackAndItsAssociatedIsos {
               get {
                   return (string) resourceProvider.GetResource("Delete selected Testpack and its associated ISOs", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Testpack Added</summary>
        public static string TestpackAdded {
               get {
                   return (string) resourceProvider.GetResource("Testpack Added", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Piping Design Area</summary>
        public static string PipingDesignArea {
               get {
                   return (string) resourceProvider.GetResource("Piping Design Area", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Geographical Area (WBS)</summary>
        public static string GeographicalAreaWbs {
               get {
                   return (string) resourceProvider.GetResource("Geographical Area (WBS)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Piping Areas</summary>
        public static string PipingAreas {
               get {
                   return (string) resourceProvider.GetResource("Piping Areas", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Piping Materials</summary>
        public static string PipingMaterials {
               get {
                   return (string) resourceProvider.GetResource("Piping Materials", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Piping Fluids</summary>
        public static string PipingFluids {
               get {
                   return (string) resourceProvider.GetResource("Piping Fluids", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Piping Destination</summary>
        public static string PipingDestination {
               get {
                   return (string) resourceProvider.GetResource("Piping Destination", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System</summary>
        public static string System {
               get {
                   return (string) resourceProvider.GetResource("System", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Piping Class</summary>
        public static string PipingClass {
               get {
                   return (string) resourceProvider.GetResource("Piping Class", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Isometrics Assignment to Testpack</summary>
        public static string IsometricsAssignmentToTestpack {
               get {
                   return (string) resourceProvider.GetResource("Isometrics Assignment to Testpack", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SUP</summary>
        public static string Sup {
               get {
                   return (string) resourceProvider.GetResource("SUP", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Start-Up Package</summary>
        public static string StartupPackage {
               get {
                   return (string) resourceProvider.GetResource("Start-Up Package", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Select ISOs</summary>
        public static string SelectIsos {
               get {
                   return (string) resourceProvider.GetResource("Select ISOs", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Assign ISOs</summary>
        public static string AssignIsos {
               get {
                   return (string) resourceProvider.GetResource("Assign ISOs", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Total</summary>
        public static string Total {
               get {
                   return (string) resourceProvider.GetResource("Total", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Assigned</summary>
        public static string Assigned {
               get {
                   return (string) resourceProvider.GetResource("Assigned", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Isometrics Assignment</summary>
        public static string IsometricsAssignment {
               get {
                   return (string) resourceProvider.GetResource("Isometrics Assignment", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sub-System</summary>
        public static string Subsystem {
               get {
                   return (string) resourceProvider.GetResource("Sub-System", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Buildings Typecodes</summary>
        public static string BuildingsTypecodes {
               get {
                   return (string) resourceProvider.GetResource("Buildings Typecodes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PIPING PAINTING</summary>
        public static string PipingPainting {
               get {
                   return (string) resourceProvider.GetResource("PIPING PAINTING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>STEEL STRUCTURES</summary>
        public static string SteelStructures {
               get {
                   return (string) resourceProvider.GetResource("STEEL STRUCTURES", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Punch Item Closers</summary>
        public static string PunchItemClosers {
               get {
                   return (string) resourceProvider.GetResource("Punch Item Closers", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Punch Item Originators</summary>
        public static string PunchItemOriginators {
               get {
                   return (string) resourceProvider.GetResource("Punch Item Originators", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Reference Companies</summary>
        public static string ReferenceCompanies {
               get {
                   return (string) resourceProvider.GetResource("Reference Companies", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Punchlist Discipline</summary>
        public static string PunchlistDiscipline {
               get {
                   return (string) resourceProvider.GetResource("Punchlist Discipline", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Punch Item Status</summary>
        public static string PunchItemStatus {
               get {
                   return (string) resourceProvider.GetResource("Punch Item Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Punch Item Categories</summary>
        public static string PunchItemCategories {
               get {
                   return (string) resourceProvider.GetResource("Punch Item Categories", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PUNCH ITEMS THAT DO NOT PREVENT OR INTERFERE WITH PRE-COMMISSIONING ACTIVITIES BUT MUST BE CLEARED TO ALLOW A SAFE COMMISSIONING OF THE SYSTEM.</summary>
        public static string PunchItemsThatDoNotPreventOrInterfereWithPrecommissioningActivitiesButMustBeClearedToAllowASafeCommissioningOfTheSystem {
               get {
                   return (string) resourceProvider.GetResource("PUNCH ITEMS THAT DO NOT PREVENT OR INTERFERE WITH PRE-COMMISSIONING ACTIVITIES BUT MUST BE CLEARED TO ALLOW A SAFE COMMISSIONING OF THE SYSTEM.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Save Data</summary>
        public static string SaveData {
               get {
                   return (string) resourceProvider.GetResource("Save Data", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Punch List Management</summary>
        public static string SystemPunchListManagement {
               get {
                   return (string) resourceProvider.GetResource("System Punch List Management", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Discipline</summary>
        public static string Discipline {
               get {
                   return (string) resourceProvider.GetResource("Discipline", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Originator</summary>
        public static string Originator {
               get {
                   return (string) resourceProvider.GetResource("Originator", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Action By</summary>
        public static string ActionBy {
               get {
                   return (string) resourceProvider.GetResource("Action By", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Category</summary>
        public static string Category {
               get {
                   return (string) resourceProvider.GetResource("Category", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>UNDERGROUND</summary>
        public static string Underground {
               get {
                   return (string) resourceProvider.GetResource("UNDERGROUND", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Electrical Cables Typecodes</summary>
        public static string ElectricalCablesTypecodes {
               get {
                   return (string) resourceProvider.GetResource("Electrical Cables Typecodes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>or Search into Databases...</summary>
        public static string OrSearchIntoDatabases {
               get {
                   return (string) resourceProvider.GetResource("or Search into Databases...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Choose Outstanding Item</summary>
        public static string ChooseOutstandingItem {
               get {
                   return (string) resourceProvider.GetResource("Choose Outstanding Item", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>or Search into</summary>
        public static string OrSearchInto {
               get {
                   return (string) resourceProvider.GetResource("or Search into", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Engineering Databases</summary>
        public static string EngineeringDatabases {
               get {
                   return (string) resourceProvider.GetResource("Engineering Databases", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Insert Outstanding Item</summary>
        public static string InsertOutstandingItem {
               get {
                   return (string) resourceProvider.GetResource("Insert Outstanding Item", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Assignment Status</summary>
        public static string AssignmentStatus {
               get {
                   return (string) resourceProvider.GetResource("Assignment Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Outstanding Description</summary>
        public static string OutstandingDescription {
               get {
                   return (string) resourceProvider.GetResource("Outstanding Description", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Outstanding Item</summary>
        public static string OutstandingItem {
               get {
                   return (string) resourceProvider.GetResource("Outstanding Item", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Work Details</summary>
        public static string WorkDetails {
               get {
                   return (string) resourceProvider.GetResource("Work Details", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Found Date</summary>
        public static string FoundDate {
               get {
                   return (string) resourceProvider.GetResource("Found Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Target Date</summary>
        public static string TargetDate {
               get {
                   return (string) resourceProvider.GetResource("Target Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FOUND.BEAMS</summary>
        public static string Foundbeams {
               get {
                   return (string) resourceProvider.GetResource("FOUND.BEAMS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PED.</summary>
        public static string Ped {
               get {
                   return (string) resourceProvider.GetResource("PED.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SINGLE FOOTINGS</summary>
        public static string SingleFootings {
               get {
                   return (string) resourceProvider.GetResource("SINGLE FOOTINGS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CONTINOUS FOOTINGS</summary>
        public static string ContinousFootings {
               get {
                   return (string) resourceProvider.GetResource("CONTINOUS FOOTINGS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Closure Date</summary>
        public static string ClosureDate {
               get {
                   return (string) resourceProvider.GetResource("Closure Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Reference</summary>
        public static string Reference {
               get {
                   return (string) resourceProvider.GetResource("Reference", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Closer</summary>
        public static string Closer {
               get {
                   return (string) resourceProvider.GetResource("Closer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Closer Document</summary>
        public static string CloserDocument {
               get {
                   return (string) resourceProvider.GetResource("Closer Document", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FIELD NAME</summary>
        public static string FieldName {
               get {
                   return (string) resourceProvider.GetResource("FIELD NAME", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>OPERATORS</summary>
        public static string Operators {
               get {
                   return (string) resourceProvider.GetResource("OPERATORS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FILTER VALUE</summary>
        public static string FilterValue {
               get {
                   return (string) resourceProvider.GetResource("FILTER VALUE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Equal To</summary>
        public static string EqualTo {
               get {
                   return (string) resourceProvider.GetResource("Equal To", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Not Equal To</summary>
        public static string NotEqualTo {
               get {
                   return (string) resourceProvider.GetResource("Not Equal To", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Includes</summary>
        public static string Includes {
               get {
                   return (string) resourceProvider.GetResource("Includes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Advanced Search</summary>
        public static string AdvancedSearch {
               get {
                   return (string) resourceProvider.GetResource("Advanced Search", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Advanced Data Filter for each attributes (AND mode)</summary>
        public static string AdvancedDataFilterForEachAttributesAndMode {
               get {
                   return (string) resourceProvider.GetResource("Advanced Data Filter for each attributes (AND mode)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Apply Filters</summary>
        public static string ApplyFilters {
               get {
                   return (string) resourceProvider.GetResource("Apply Filters", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Reset Filters</summary>
        public static string ResetFilters {
               get {
                   return (string) resourceProvider.GetResource("Reset Filters", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ELECTRICAL UNDERGROUND</summary>
        public static string ElectricalUnderground {
               get {
                   return (string) resourceProvider.GetResource("ELECTRICAL UNDERGROUND", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PIPING UNDERGROUND</summary>
        public static string PipingUnderground {
               get {
                   return (string) resourceProvider.GetResource("PIPING UNDERGROUND", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>TRACING</summary>
        public static string Tracing {
               get {
                   return (string) resourceProvider.GetResource("TRACING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>EQUIPMENT INSULATION</summary>
        public static string EquipmentInsulation {
               get {
                   return (string) resourceProvider.GetResource("EQUIPMENT INSULATION", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FITTINGS</summary>
        public static string Fittings {
               get {
                   return (string) resourceProvider.GetResource("FITTINGS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Reference Documentation</summary>
        public static string ReferenceDocumentation {
               get {
                   return (string) resourceProvider.GetResource("Reference Documentation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Reports</summary>
        public static string Reports {
               get {
                   return (string) resourceProvider.GetResource("Reports", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Punchlist Summary</summary>
        public static string SystemPunchlistSummary {
               get {
                   return (string) resourceProvider.GetResource("System Punchlist Summary", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Data saved</summary>
        public static string DataSaved {
               get {
                   return (string) resourceProvider.GetResource("Data saved", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certification</summary>
        public static string Certification {
               get {
                   return (string) resourceProvider.GetResource("Certification", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Structure Typecodes</summary>
        public static string StructureTypecodes {
               get {
                   return (string) resourceProvider.GetResource("Structure Typecodes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tags</summary>
        public static string Tags {
               get {
                   return (string) resourceProvider.GetResource("Tags", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Preservation Time Schedule</summary>
        public static string PreservationTimeSchedule {
               get {
                   return (string) resourceProvider.GetResource("Preservation Time Schedule", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Job Plan Assignment to Tags</summary>
        public static string JobPlanAssignmentToTags {
               get {
                   return (string) resourceProvider.GetResource("Job Plan Assignment to Tags", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Job Plans Overview</summary>
        public static string JobPlansOverview {
               get {
                   return (string) resourceProvider.GetResource("Job Plans Overview", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Remember to assign ISOs to it</summary>
        public static string RememberToAssignIsosToIt {
               get {
                   return (string) resourceProvider.GetResource("Remember to assign ISOs to it", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Job Plan</summary>
        public static string JobPlan {
               get {
                   return (string) resourceProvider.GetResource("Job Plan", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ELEVATED BEAM</summary>
        public static string ElevatedBeam {
               get {
                   return (string) resourceProvider.GetResource("ELEVATED BEAM", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SUPPLY</summary>
        public static string Supply {
               get {
                   return (string) resourceProvider.GetResource("SUPPLY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ERECTION</summary>
        public static string Erection {
               get {
                   return (string) resourceProvider.GetResource("ERECTION", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>COLUMN</summary>
        public static string Column {
               get {
                   return (string) resourceProvider.GetResource("COLUMN", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>YARD</summary>
        public static string Yard {
               get {
                   return (string) resourceProvider.GetResource("YARD", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CAST IN SITU</summary>
        public static string CastInSitu {
               get {
                   return (string) resourceProvider.GetResource("CAST IN SITU", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PRECAST</summary>
        public static string Precast {
               get {
                   return (string) resourceProvider.GetResource("PRECAST", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>thick 20 cm</summary>
        public static string ThickCm {
               get {
                   return (string) resourceProvider.GetResource("thick 20 cm", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PAVING</summary>
        public static string Paving {
               get {
                   return (string) resourceProvider.GetResource("PAVING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ROAD</summary>
        public static string Road {
               get {
                   return (string) resourceProvider.GetResource("ROAD", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ELEVATED SLABS</summary>
        public static string ElevatedSlabs {
               get {
                   return (string) resourceProvider.GetResource("ELEVATED SLABS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Insert New PL Item</summary>
        public static string InsertNewPlItem {
               get {
                   return (string) resourceProvider.GetResource("Insert New PL Item", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Update Selected</summary>
        public static string UpdateSelected {
               get {
                   return (string) resourceProvider.GetResource("Update Selected", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>NUM OF LEVELS</summary>
        public static string NumOfLevels {
               get {
                   return (string) resourceProvider.GetResource("NUM OF LEVELS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ELEVATED COLUMNS</summary>
        public static string ElevatedColumns {
               get {
                   return (string) resourceProvider.GetResource("ELEVATED COLUMNS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ELEVATED BEAMS</summary>
        public static string ElevatedBeams {
               get {
                   return (string) resourceProvider.GetResource("ELEVATED BEAMS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CONTINOUS (CAST IN SITU)</summary>
        public static string ContinousCastInSitu {
               get {
                   return (string) resourceProvider.GetResource("CONTINOUS (CAST IN SITU)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SINGLE (PRECAST)</summary>
        public static string SinglePrecast {
               get {
                   return (string) resourceProvider.GetResource("SINGLE (PRECAST)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SINGLE (CAST IN SITU)</summary>
        public static string SingleCastInSitu {
               get {
                   return (string) resourceProvider.GetResource("SINGLE (CAST IN SITU)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>New PL Item Saved</summary>
        public static string NewPlItemSaved {
               get {
                   return (string) resourceProvider.GetResource("New PL Item Saved", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Selected PL Item Updated</summary>
        public static string SelectedPlItemUpdated {
               get {
                   return (string) resourceProvider.GetResource("Selected PL Item Updated", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Info saved</summary>
        public static string InfoSaved {
               get {
                   return (string) resourceProvider.GetResource("Info saved", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Record deleted</summary>
        public static string RecordDeleted {
               get {
                   return (string) resourceProvider.GetResource("Record deleted", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Select a system first</summary>
        public static string SelectASystemFirst {
               get {
                   return (string) resourceProvider.GetResource("Select a system first", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Advanced Search for each attributes (AND mode)</summary>
        public static string AdvancedSearchForEachAttributesAndMode {
               get {
                   return (string) resourceProvider.GetResource("Advanced Search for each attributes (AND mode)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CHOOSE A TAG FROM RELEVANT DATATABLE</summary>
        public static string ChooseATagFromRelevantDatatable {
               get {
                   return (string) resourceProvider.GetResource("CHOOSE A TAG FROM RELEVANT DATATABLE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Preservation Actions</summary>
        public static string PreservationActions {
               get {
                   return (string) resourceProvider.GetResource("Preservation Actions", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Location and Companies Involved</summary>
        public static string LocationAndCompaniesInvolved {
               get {
                   return (string) resourceProvider.GetResource("Location and Companies Involved", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Summary</summary>
        public static string Summary {
               get {
                   return (string) resourceProvider.GetResource("Summary", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Preservation Overall Status</summary>
        public static string PreservationOverallStatus {
               get {
                   return (string) resourceProvider.GetResource("Preservation Overall Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Job Plan Assignment to Tag</summary>
        public static string JobPlanAssignmentToTag {
               get {
                   return (string) resourceProvider.GetResource("Job Plan Assignment to Tag", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Preservation Status</summary>
        public static string PreservationStatus {
               get {
                   return (string) resourceProvider.GetResource("Preservation Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Job Plans Management</summary>
        public static string JobPlansManagement {
               get {
                   return (string) resourceProvider.GetResource("Job Plans Management", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SPOOLS</summary>
        public static string Spools {
               get {
                   return (string) resourceProvider.GetResource("SPOOLS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Material Preservation</summary>
        public static string MaterialPreservation {
               get {
                   return (string) resourceProvider.GetResource("Material Preservation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>GROUTING</summary>
        public static string Grouting {
               get {
                   return (string) resourceProvider.GetResource("GROUTING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>STRUCTURE PAINTING</summary>
        public static string StructurePainting {
               get {
                   return (string) resourceProvider.GetResource("STRUCTURE PAINTING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>(KG)</summary>
        public static string Kg {
               get {
                   return (string) resourceProvider.GetResource("(KG)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FOOTINGS</summary>
        public static string Footings {
               get {
                   return (string) resourceProvider.GetResource("FOOTINGS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>WALL</summary>
        public static string Wall {
               get {
                   return (string) resourceProvider.GetResource("WALL", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Typecode</summary>
        public static string Typecode {
               get {
                   return (string) resourceProvider.GetResource("Typecode", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>All TAGS are assigned or no records</summary>
        public static string AllTagsAreAssignedOrNoRecords {
               get {
                   return (string) resourceProvider.GetResource("All TAGS are assigned or no records", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PUNCH ITEMS THAT NEEDS TO BE CLEARED PRIOR OF PIPING PRESSURE TESTING</summary>
        public static string PunchItemsThatNeedsToBeClearedPriorOfPipingPressureTesting {
               get {
                   return (string) resourceProvider.GetResource("PUNCH ITEMS THAT NEEDS TO BE CLEARED PRIOR OF PIPING PRESSURE TESTING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>REVAMPING</summary>
        public static string Revamping {
               get {
                   return (string) resourceProvider.GetResource("REVAMPING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SECONDARY STRUCTURE</summary>
        public static string SecondaryStructure {
               get {
                   return (string) resourceProvider.GetResource("SECONDARY STRUCTURE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Instrumentation Typecodes</summary>
        public static string InstrumentationTypecodes {
               get {
                   return (string) resourceProvider.GetResource("Instrumentation Typecodes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Concrete Objects Typecodes</summary>
        public static string ConcreteObjectsTypecodes {
               get {
                   return (string) resourceProvider.GetResource("Concrete Objects Typecodes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ELECTRICAL CABLES</summary>
        public static string ElectricalCables {
               get {
                   return (string) resourceProvider.GetResource("ELECTRICAL CABLES", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CONCRETE WORKS</summary>
        public static string ConcreteWorks {
               get {
                   return (string) resourceProvider.GetResource("CONCRETE WORKS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>HANDRAILS</summary>
        public static string Handrails {
               get {
                   return (string) resourceProvider.GetResource("HANDRAILS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>VERT.LADDERS WITHOUT SAFETY CAGE</summary>
        public static string VertladdersWithoutSafetyCage {
               get {
                   return (string) resourceProvider.GetResource("VERT.LADDERS WITHOUT SAFETY CAGE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>IMPORTED</summary>
        public static string Imported {
               get {
                   return (string) resourceProvider.GetResource("IMPORTED", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>MAIN</summary>
        public static string Main {
               get {
                   return (string) resourceProvider.GetResource("MAIN", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CHECKERED PLATES</summary>
        public static string CheckeredPlates {
               get {
                   return (string) resourceProvider.GetResource("CHECKERED PLATES", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>GRATINGS</summary>
        public static string Gratings {
               get {
                   return (string) resourceProvider.GetResource("GRATINGS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>VERT.LADDERS WITH SAFETY CAGE</summary>
        public static string VertladdersWithSafetyCage {
               get {
                   return (string) resourceProvider.GetResource("VERT.LADDERS WITH SAFETY CAGE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FROM EXCAVATON</summary>
        public static string FromExcavaton {
               get {
                   return (string) resourceProvider.GetResource("FROM EXCAVATON", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Equipment Typecodes</summary>
        public static string EquipmentTypecodes {
               get {
                   return (string) resourceProvider.GetResource("Equipment Typecodes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>INSTRUMENT UNDERGROUND</summary>
        public static string InstrumentUnderground {
               get {
                   return (string) resourceProvider.GetResource("INSTRUMENT UNDERGROUND", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Personal Statistics</summary>
        public static string PersonalStatistics {
               get {
                   return (string) resourceProvider.GetResource("Personal Statistics", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certification Follow-Up</summary>
        public static string CertificationFollowup {
               get {
                   return (string) resourceProvider.GetResource("Certification Follow-Up", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Hydrotest Package Follow-Up</summary>
        public static string HydrotestPackageFollowup {
               get {
                   return (string) resourceProvider.GetResource("Hydrotest Package Follow-Up", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Punchlist Status</summary>
        public static string PunchlistStatus {
               get {
                   return (string) resourceProvider.GetResource("Punchlist Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Activities Status and some specific suggestions regarding your responsibilities and performances.</summary>
        public static string ActivitiesStatusAndSomeSpecificSuggestionsRegardingYourResponsibilitiesAndPerformances {
               get {
                   return (string) resourceProvider.GetResource("Activities Status and some specific suggestions regarding your responsibilities and performances.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Inspection Forms</summary>
        public static string InspectionForms {
               get {
                   return (string) resourceProvider.GetResource("Inspection Forms", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>According to your subcontractor SOW responsibilities and Certification Steps Follow-Up.</summary>
        public static string AccordingToYourSubcontractorSowResponsibilitiesAndCertificationStepsFollowup {
               get {
                   return (string) resourceProvider.GetResource("According to your subcontractor SOW responsibilities and Certification Steps Follow-Up.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Hydrotest Packages Status and Steps Follow-Up.</summary>
        public static string HydrotestPackagesStatusAndStepsFollowup {
               get {
                   return (string) resourceProvider.GetResource("Hydrotest Packages Status and Steps Follow-Up.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Punchlist Status regarding Tags involved in your responsibilities.</summary>
        public static string PunchlistStatusRegardingTagsInvolvedInYourResponsibilities {
               get {
                   return (string) resourceProvider.GetResource("Punchlist Status regarding Tags involved in your responsibilities.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Preservation Activities</summary>
        public static string PreservationActivities {
               get {
                   return (string) resourceProvider.GetResource("Preservation Activities", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Not valid mhrs</summary>
        public static string NotValidMhrs {
               get {
                   return (string) resourceProvider.GetResource("Not valid mhrs", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Not valid value</summary>
        public static string NotValidValue {
               get {
                   return (string) resourceProvider.GetResource("Not valid value", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sub-Activity</summary>
        public static string Subactivity {
               get {
                   return (string) resourceProvider.GetResource("Sub-Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ELECTRICAL COMPONENTS</summary>
        public static string ElectricalComponents {
               get {
                   return (string) resourceProvider.GetResource("ELECTRICAL COMPONENTS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>LINELIST</summary>
        public static string Linelist {
               get {
                   return (string) resourceProvider.GetResource("LINELIST", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PIPING COMPONENTS</summary>
        public static string PipingComponents {
               get {
                   return (string) resourceProvider.GetResource("PIPING COMPONENTS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Punchlist</summary>
        public static string SystemPunchlist {
               get {
                   return (string) resourceProvider.GetResource("System Punchlist", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>EQUIPMENT PAINTING</summary>
        public static string EquipmentPainting {
               get {
                   return (string) resourceProvider.GetResource("EQUIPMENT PAINTING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Precommissioning / Commissioning Management</summary>
        public static string PrecommissioningCommissioningManagement {
               get {
                   return (string) resourceProvider.GetResource("Precommissioning / Commissioning Management", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CREATE TAGS USING SCHEMAS</summary>
        public static string CreateTagsUsingSchemas {
               get {
                   return (string) resourceProvider.GetResource("CREATE TAGS USING SCHEMAS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Motors Typecodes</summary>
        public static string MotorsTypecodes {
               get {
                   return (string) resourceProvider.GetResource("Motors Typecodes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PIPING DESTINATIONS</summary>
        public static string PipingDestinations {
               get {
                   return (string) resourceProvider.GetResource("PIPING DESTINATIONS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certifications</summary>
        public static string Certifications {
               get {
                   return (string) resourceProvider.GetResource("Certifications", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Civil Databases</summary>
        public static string CivilDatabases {
               get {
                   return (string) resourceProvider.GetResource("Civil Databases", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>INSTRUMENT CABLE</summary>
        public static string InstrumentCable {
               get {
                   return (string) resourceProvider.GetResource("INSTRUMENT CABLE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ELECTRICAL OBJECTS</summary>
        public static string ElectricalObjects {
               get {
                   return (string) resourceProvider.GetResource("ELECTRICAL OBJECTS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ELECTRICAL CABLE</summary>
        public static string ElectricalCable {
               get {
                   return (string) resourceProvider.GetResource("ELECTRICAL CABLE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>BUILDINGS</summary>
        public static string Buildings {
               get {
                   return (string) resourceProvider.GetResource("BUILDINGS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>MOTORS</summary>
        public static string Motors {
               get {
                   return (string) resourceProvider.GetResource("MOTORS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>INSTRUMENTATION</summary>
        public static string Instrumentation {
               get {
                   return (string) resourceProvider.GetResource("INSTRUMENTATION", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CONCRETE OBJECTS</summary>
        public static string ConcreteObjects {
               get {
                   return (string) resourceProvider.GetResource("CONCRETE OBJECTS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>UNDERGROUND PIPING</summary>
        public static string UndergroundPiping {
               get {
                   return (string) resourceProvider.GetResource("UNDERGROUND PIPING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>STEEL STRUCTURE</summary>
        public static string SteelStructure {
               get {
                   return (string) resourceProvider.GetResource("STEEL STRUCTURE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>P&amp;ID</summary>
        public static string Pampid {
               get {
                   return (string) resourceProvider.GetResource("P&amp;ID", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SYSTEMS</summary>
        public static string Systems {
               get {
                   return (string) resourceProvider.GetResource("SYSTEMS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Typecodes</summary>
        public static string Typecodes {
               get {
                   return (string) resourceProvider.GetResource("Typecodes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Project Lists</summary>
        public static string ProjectLists {
               get {
                   return (string) resourceProvider.GetResource("Project Lists", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>UNIT</summary>
        public static string Unit {
               get {
                   return (string) resourceProvider.GetResource("UNIT", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>AREA</summary>
        public static string Area {
               get {
                   return (string) resourceProvider.GetResource("AREA", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Preservation Activities Status</summary>
        public static string PreservationActivitiesStatus {
               get {
                   return (string) resourceProvider.GetResource("Preservation Activities Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>UNDERGROUND CONCRETE</summary>
        public static string UndergroundConcrete {
               get {
                   return (string) resourceProvider.GetResource("UNDERGROUND CONCRETE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Preservation Maintenance Follow-Up</summary>
        public static string PreservationMaintenanceFollowup {
               get {
                   return (string) resourceProvider.GetResource("Preservation Maintenance Follow-Up", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>GRAVEL FINISHING</summary>
        public static string GravelFinishing {
               get {
                   return (string) resourceProvider.GetResource("GRAVEL FINISHING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>INSTRUMENT COMPONENTS</summary>
        public static string InstrumentComponents {
               get {
                   return (string) resourceProvider.GetResource("INSTRUMENT COMPONENTS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>GRAVEL ROAD</summary>
        public static string GravelRoad {
               get {
                   return (string) resourceProvider.GetResource("GRAVEL ROAD", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SOIL FINISHING</summary>
        public static string SoilFinishing {
               get {
                   return (string) resourceProvider.GetResource("SOIL FINISHING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ACID LINING</summary>
        public static string AcidLining {
               get {
                   return (string) resourceProvider.GetResource("ACID LINING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>INSTRUMENT CABLES</summary>
        public static string InstrumentCables {
               get {
                   return (string) resourceProvider.GetResource("INSTRUMENT CABLES", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SQM</summary>
        public static string Sqm {
               get {
                   return (string) resourceProvider.GetResource("SQM", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ASPHALT ROAD</summary>
        public static string AsphaltRoad {
               get {
                   return (string) resourceProvider.GetResource("ASPHALT ROAD", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>EARTH ROAD</summary>
        public static string EarthRoad {
               get {
                   return (string) resourceProvider.GetResource("EARTH ROAD", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Instrument Cables Typecodes</summary>
        public static string InstrumentCablesTypecodes {
               get {
                   return (string) resourceProvider.GetResource("Instrument Cables Typecodes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Electrical Typecodes</summary>
        public static string ElectricalTypecodes {
               get {
                   return (string) resourceProvider.GetResource("Electrical Typecodes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Restoration Date</summary>
        public static string RestorationDate {
               get {
                   return (string) resourceProvider.GetResource("Restoration Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tag Location</summary>
        public static string TagLocation {
               get {
                   return (string) resourceProvider.GetResource("Tag Location", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Initialization Date</summary>
        public static string InitializationDate {
               get {
                   return (string) resourceProvider.GetResource("Initialization Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Initial Date</summary>
        public static string InitialDate {
               get {
                   return (string) resourceProvider.GetResource("Initial Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Maintenance From</summary>
        public static string MaintenanceFrom {
               get {
                   return (string) resourceProvider.GetResource("Maintenance From", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Precom/Comm Inspections Follow-Up</summary>
        public static string PrecomcommInspectionsFollowup {
               get {
                   return (string) resourceProvider.GetResource("Precom/Comm Inspections Follow-Up", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Subdivision</summary>
        public static string SystemSubdivision {
               get {
                   return (string) resourceProvider.GetResource("System Subdivision", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Progress Status</summary>
        public static string SystemProgressStatus {
               get {
                   return (string) resourceProvider.GetResource("System Progress Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Preservation Follow-Up</summary>
        public static string PreservationFollowup {
               get {
                   return (string) resourceProvider.GetResource("Preservation Follow-Up", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ELECTRICAL</summary>
        public static string Electrical {
               get {
                   return (string) resourceProvider.GetResource("ELECTRICAL", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>INSULATION</summary>
        public static string Insulation {
               get {
                   return (string) resourceProvider.GetResource("INSULATION", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>MECHANICAL</summary>
        public static string Mechanical {
               get {
                   return (string) resourceProvider.GetResource("MECHANICAL", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PAINTING</summary>
        public static string Painting {
               get {
                   return (string) resourceProvider.GetResource("PAINTING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PIPING PREFABRICATION</summary>
        public static string PipingPrefabrication {
               get {
                   return (string) resourceProvider.GetResource("PIPING PREFABRICATION", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Inspections Follow-Up</summary>
        public static string InspectionsFollowup {
               get {
                   return (string) resourceProvider.GetResource("Inspections Follow-Up", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Progress Status</summary>
        public static string ProgressStatus {
               get {
                   return (string) resourceProvider.GetResource("Progress Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Systems Status</summary>
        public static string SystemsStatus {
               get {
                   return (string) resourceProvider.GetResource("Systems Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Progress &amp; Systems Status</summary>
        public static string ProgressAmpSystemsStatus {
               get {
                   return (string) resourceProvider.GetResource("Progress &amp; Systems Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Punch Lists</summary>
        public static string SystemPunchLists {
               get {
                   return (string) resourceProvider.GetResource("System Punch Lists", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Item</summary>
        public static string Item {
               get {
                   return (string) resourceProvider.GetResource("Item", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Assign Inspection Plan to Tag</summary>
        public static string AssignInspectionPlanToTag {
               get {
                   return (string) resourceProvider.GetResource("Assign Inspection Plan to Tag", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Each Tag could be linked to a specific Inspection Plan...</summary>
        public static string EachTagCouldBeLinkedToASpecificInspectionPlan {
               get {
                   return (string) resourceProvider.GetResource("Each Tag could be linked to a specific Inspection Plan...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Assign System to Tag</summary>
        public static string AssignSystemToTag {
               get {
                   return (string) resourceProvider.GetResource("Assign System to Tag", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Assign System to Tags</summary>
        public static string AssignSystemToTags {
               get {
                   return (string) resourceProvider.GetResource("Assign System to Tags", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Progress &amp; Systems</summary>
        public static string ProgressAmpSystems {
               get {
                   return (string) resourceProvider.GetResource("Progress &amp; Systems", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certification Completion</summary>
        public static string CertificationCompletion {
               get {
                   return (string) resourceProvider.GetResource("Certification Completion", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Loop Tests by System</summary>
        public static string LoopTestsBySystem {
               get {
                   return (string) resourceProvider.GetResource("Loop Tests by System", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PIPING ABOVEGROUND</summary>
        public static string PipingAboveground {
               get {
                   return (string) resourceProvider.GetResource("PIPING ABOVEGROUND", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Loop Tests Status by System</summary>
        public static string LoopTestsStatusBySystem {
               get {
                   return (string) resourceProvider.GetResource("Loop Tests Status by System", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tags assigned</summary>
        public static string TagsAssigned {
               get {
                   return (string) resourceProvider.GetResource("Tags assigned", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tags assignment removed</summary>
        public static string TagsAssignmentRemoved {
               get {
                   return (string) resourceProvider.GetResource("Tags assignment removed", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Data deleted</summary>
        public static string DataDeleted {
               get {
                   return (string) resourceProvider.GetResource("Data deleted", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tags System assignment removed</summary>
        public static string TagsSystemAssignmentRemoved {
               get {
                   return (string) resourceProvider.GetResource("Tags System assignment removed", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tags System assigned</summary>
        public static string TagsSystemAssigned {
               get {
                   return (string) resourceProvider.GetResource("Tags System assigned", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>TOTAL TAGS</summary>
        public static string TotalTags {
               get {
                   return (string) resourceProvider.GetResource("TOTAL TAGS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PRECOM TAGS</summary>
        public static string PrecomTags {
               get {
                   return (string) resourceProvider.GetResource("PRECOM TAGS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ASSIGNED TO SYSTEM</summary>
        public static string AssignedToSystem {
               get {
                   return (string) resourceProvider.GetResource("ASSIGNED TO SYSTEM", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Inspections Status</summary>
        public static string InspectionsStatus {
               get {
                   return (string) resourceProvider.GetResource("Inspections Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Project Completion KPIs</summary>
        public static string ProjectCompletionKpis {
               get {
                   return (string) resourceProvider.GetResource("Project Completion KPIs", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Completion Overview</summary>
        public static string SystemCompletionOverview {
               get {
                   return (string) resourceProvider.GetResource("System Completion Overview", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Completion by Phase</summary>
        public static string CompletionByPhase {
               get {
                   return (string) resourceProvider.GetResource("Completion by Phase", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Browse</summary>
        public static string Browse {
               get {
                   return (string) resourceProvider.GetResource("Browse", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System List</summary>
        public static string SystemList {
               get {
                   return (string) resourceProvider.GetResource("System List", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System KPIs and Documentation</summary>
        public static string SystemKpisAndDocumentation {
               get {
                   return (string) resourceProvider.GetResource("System KPIs and Documentation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PRECOMMISSIONING</summary>
        public static string Precommissioning {
               get {
                   return (string) resourceProvider.GetResource("PRECOMMISSIONING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>COMMISSIONING</summary>
        public static string Commissioning {
               get {
                   return (string) resourceProvider.GetResource("COMMISSIONING", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>START-UP</summary>
        public static string Startup {
               get {
                   return (string) resourceProvider.GetResource("START-UP", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PROGRESS BY DATATABLE</summary>
        public static string ProgressByDatatable {
               get {
                   return (string) resourceProvider.GetResource("PROGRESS BY DATATABLE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CONSTRUCTION PROGRESS BY MAIN ACTIVITY</summary>
        public static string ConstructionProgressByMainActivity {
               get {
                   return (string) resourceProvider.GetResource("CONSTRUCTION PROGRESS BY MAIN ACTIVITY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CONSTRUCTION BY MAIN ACTIVITY</summary>
        public static string ConstructionByMainActivity {
               get {
                   return (string) resourceProvider.GetResource("CONSTRUCTION BY MAIN ACTIVITY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PRE/COMM BY MAIN ACTIVITY</summary>
        public static string PrecommByMainActivity {
               get {
                   return (string) resourceProvider.GetResource("PRE/COMM BY MAIN ACTIVITY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certification Steps</summary>
        public static string CertificationSteps {
               get {
                   return (string) resourceProvider.GetResource("Certification Steps", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Relevant Activity</summary>
        public static string RelevantActivity {
               get {
                   return (string) resourceProvider.GetResource("Relevant Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certification Steps by Sub Activity</summary>
        public static string CertificationStepsBySubActivity {
               get {
                   return (string) resourceProvider.GetResource("Certification Steps by Sub Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certification Steps Definition</summary>
        public static string CertificationStepsDefinition {
               get {
                   return (string) resourceProvider.GetResource("Certification Steps Definition", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certification Plans Definition</summary>
        public static string CertificationPlansDefinition {
               get {
                   return (string) resourceProvider.GetResource("Certification Plans Definition", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Import Steps from Excel</summary>
        public static string ImportStepsFromExcel {
               get {
                   return (string) resourceProvider.GetResource("Import Steps from Excel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Each Sub Activity relevant to a specific Certification Plan must have the sum of weighted steps at 100%...</summary>
        public static string EachSubActivityRelevantToASpecificCertificationPlanMustHaveTheSumOfWeightedStepsAt {
               get {
                   return (string) resourceProvider.GetResource("Each Sub Activity relevant to a specific Certification Plan must have the sum of weighted steps at 100%...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PRECOMMISSIONING BY MAIN ACTIVITY</summary>
        public static string PrecommissioningByMainActivity {
               get {
                   return (string) resourceProvider.GetResource("PRECOMMISSIONING BY MAIN ACTIVITY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>COMMISSIONING BY MAIN ACTIVITY</summary>
        public static string CommissioningByMainActivity {
               get {
                   return (string) resourceProvider.GetResource("COMMISSIONING BY MAIN ACTIVITY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Import from SPMAT</summary>
        public static string ImportFromSpmat {
               get {
                   return (string) resourceProvider.GetResource("Import from SPMAT", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Import from SPI</summary>
        public static string ImportFromSpi {
               get {
                   return (string) resourceProvider.GetResource("Import from SPI", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Import from SPEL</summary>
        public static string ImportFromSpel {
               get {
                   return (string) resourceProvider.GetResource("Import from SPEL", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Engineering Tools</summary>
        public static string EngineeringTools {
               get {
                   return (string) resourceProvider.GetResource("Engineering Tools", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Documentation Follow-Up</summary>
        public static string SystemDocumentationFollowup {
               get {
                   return (string) resourceProvider.GetResource("System Documentation Follow-Up", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Systems List</summary>
        public static string SystemsList {
               get {
                   return (string) resourceProvider.GetResource("Systems List", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Milestones Follow-Up</summary>
        public static string SystemMilestonesFollowup {
               get {
                   return (string) resourceProvider.GetResource("System Milestones Follow-Up", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CONSTRUCTION PHASE</summary>
        public static string ConstructionPhase {
               get {
                   return (string) resourceProvider.GetResource("CONSTRUCTION PHASE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PRECOMMISSIONING PHASE</summary>
        public static string PrecommissioningPhase {
               get {
                   return (string) resourceProvider.GetResource("PRECOMMISSIONING PHASE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>COMMISSIONING PHASE</summary>
        public static string CommissioningPhase {
               get {
                   return (string) resourceProvider.GetResource("COMMISSIONING PHASE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mechanical Completion Sent</summary>
        public static string MechanicalCompletionSent {
               get {
                   return (string) resourceProvider.GetResource("Mechanical Completion Sent", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Start-Up Phase</summary>
        public static string StartupPhase {
               get {
                   return (string) resourceProvider.GetResource("Start-Up Phase", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mechanical Completion Sent to Customer</summary>
        public static string MechanicalCompletionSentToCustomer {
               get {
                   return (string) resourceProvider.GetResource("Mechanical Completion Sent to Customer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mechanical Completion Signed by Customer</summary>
        public static string MechanicalCompletionSignedByCustomer {
               get {
                   return (string) resourceProvider.GetResource("Mechanical Completion Signed by Customer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Precommissioning Completion Sent to Customer</summary>
        public static string PrecommissioningCompletionSentToCustomer {
               get {
                   return (string) resourceProvider.GetResource("Precommissioning Completion Sent to Customer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Precommissioning Completion Signed by Customer</summary>
        public static string PrecommissioningCompletionSignedByCustomer {
               get {
                   return (string) resourceProvider.GetResource("Precommissioning Completion Signed by Customer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Commissioning Completion Sent to Customer</summary>
        public static string CommissioningCompletionSentToCustomer {
               get {
                   return (string) resourceProvider.GetResource("Commissioning Completion Sent to Customer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Commissioning Completion Signed by Customer</summary>
        public static string CommissioningCompletionSignedByCustomer {
               get {
                   return (string) resourceProvider.GetResource("Commissioning Completion Signed by Customer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Precommissioning Completion Sent</summary>
        public static string PrecommissioningCompletionSent {
               get {
                   return (string) resourceProvider.GetResource("Precommissioning Completion Sent", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Commissioning Completion Sent</summary>
        public static string CommissioningCompletionSent {
               get {
                   return (string) resourceProvider.GetResource("Commissioning Completion Sent", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Commissioning Completion Signed</summary>
        public static string CommissioningCompletionSigned {
               get {
                   return (string) resourceProvider.GetResource("Commissioning Completion Signed", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Ready for Start-Up Sent</summary>
        public static string ReadyForStartupSent {
               get {
                   return (string) resourceProvider.GetResource("Ready for Start-Up Sent", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Ready for Start-Up Signed</summary>
        public static string ReadyForStartupSigned {
               get {
                   return (string) resourceProvider.GetResource("Ready for Start-Up Signed", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mechanical Completion Signed</summary>
        public static string MechanicalCompletionSigned {
               get {
                   return (string) resourceProvider.GetResource("Mechanical Completion Signed", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Precommissioning Completion Signed</summary>
        public static string PrecommissioningCompletionSigned {
               get {
                   return (string) resourceProvider.GetResource("Precommissioning Completion Signed", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Test Run Protocol Started</summary>
        public static string TestRunProtocolStarted {
               get {
                   return (string) resourceProvider.GetResource("Test Run Protocol Started", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Test Run Protocol Completed</summary>
        public static string TestRunProtocolCompleted {
               get {
                   return (string) resourceProvider.GetResource("Test Run Protocol Completed", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Guarantee Test Protocol Sent</summary>
        public static string GuaranteeTestProtocolSent {
               get {
                   return (string) resourceProvider.GetResource("Guarantee Test Protocol Sent", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Guarantee Test Protocol Signed</summary>
        public static string GuaranteeTestProtocolSigned {
               get {
                   return (string) resourceProvider.GetResource("Guarantee Test Protocol Signed", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PAC Protocol Signed (if required)</summary>
        public static string PacProtocolSignedIfRequired {
               get {
                   return (string) resourceProvider.GetResource("PAC Protocol Signed (if required)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Please, select a system first !</summary>
        public static string PleaseSelectASystemFirst {
               get {
                   return (string) resourceProvider.GetResource("Please, select a system first !", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Component List</summary>
        public static string SystemComponentList {
               get {
                   return (string) resourceProvider.GetResource("System Component List", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PUNCH LIST STATUS</summary>
        public static string PunchListStatus {
               get {
                   return (string) resourceProvider.GetResource("PUNCH LIST STATUS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PUNCHLIST CLEARANCE</summary>
        public static string PunchlistClearance {
               get {
                   return (string) resourceProvider.GetResource("PUNCHLIST CLEARANCE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Dates Updated</summary>
        public static string SystemDatesUpdated {
               get {
                   return (string) resourceProvider.GetResource("System Dates Updated", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Progress &amp Performance Indicators</summary>
        public static string SystemProgressAmpPerformanceIndicators {
               get {
                   return (string) resourceProvider.GetResource("System Progress &amp Performance Indicators", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Subcontractor Performance Indicators</summary>
        public static string SubcontractorPerformanceIndicators {
               get {
                   return (string) resourceProvider.GetResource("Subcontractor Performance Indicators", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Subcontractor List &amp; SOW</summary>
        public static string SubcontractorListAmpSow {
               get {
                   return (string) resourceProvider.GetResource("Subcontractor List &amp; SOW", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Subcontractor Progress &amp Performance Indicators</summary>
        public static string SubcontractorProgressAmpPerformanceIndicators {
               get {
                   return (string) resourceProvider.GetResource("Subcontractor Progress &amp Performance Indicators", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>DATA ANALYSIS</summary>
        public static string DataAnalysis {
               get {
                   return (string) resourceProvider.GetResource("DATA ANALYSIS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Earned Quantity</summary>
        public static string EarnedQuantity {
               get {
                   return (string) resourceProvider.GetResource("Earned Quantity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Budget Quantity</summary>
        public static string BudgetQuantity {
               get {
                   return (string) resourceProvider.GetResource("Budget Quantity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Achieved Progress</summary>
        public static string AchievedProgress {
               get {
                   return (string) resourceProvider.GetResource("Achieved Progress", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Planned Start Date</summary>
        public static string PlannedStartDate {
               get {
                   return (string) resourceProvider.GetResource("Planned Start Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Planned End Date</summary>
        public static string PlannedEndDate {
               get {
                   return (string) resourceProvider.GetResource("Planned End Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Delayed Completion</summary>
        public static string DelayedCompletion {
               get {
                   return (string) resourceProvider.GetResource("Delayed Completion", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Delayed Progress</summary>
        public static string DelayedProgress {
               get {
                   return (string) resourceProvider.GetResource("Delayed Progress", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Planned Start</summary>
        public static string PlannedStart {
               get {
                   return (string) resourceProvider.GetResource("Planned Start", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Planned End</summary>
        public static string PlannedEnd {
               get {
                   return (string) resourceProvider.GetResource("Planned End", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Weekly Earned Quantity</summary>
        public static string WeeklyEarnedQuantity {
               get {
                   return (string) resourceProvider.GetResource("Weekly Earned Quantity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Additional Delay</summary>
        public static string AdditionalDelay {
               get {
                   return (string) resourceProvider.GetResource("Additional Delay", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>LOOP TESTS STATUS</summary>
        public static string LoopTestsStatus {
               get {
                   return (string) resourceProvider.GetResource("LOOP TESTS STATUS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FORECAST ANALYSIS</summary>
        public static string ForecastAnalysis {
               get {
                   return (string) resourceProvider.GetResource("FORECAST ANALYSIS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Planned Productivity</summary>
        public static string PlannedProductivity {
               get {
                   return (string) resourceProvider.GetResource("Planned Productivity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Current Productivity</summary>
        public static string CurrentProductivity {
               get {
                   return (string) resourceProvider.GetResource("Current Productivity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Weekly Quantity</summary>
        public static string WeeklyQuantity {
               get {
                   return (string) resourceProvider.GetResource("Weekly Quantity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>LOOP TESTS</summary>
        public static string LoopTests {
               get {
                   return (string) resourceProvider.GetResource("LOOP TESTS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CUMULATED EARNED QUANTITY</summary>
        public static string CumulatedEarnedQuantity {
               get {
                   return (string) resourceProvider.GetResource("CUMULATED EARNED QUANTITY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sub-Activities as per SOW</summary>
        public static string SubactivitiesAsPerSow {
               get {
                   return (string) resourceProvider.GetResource("Sub-Activities as per SOW", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Project Database</summary>
        public static string ProjectDatabase {
               get {
                   return (string) resourceProvider.GetResource("Project Database", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sub-Activities</summary>
        public static string Subactivities {
               get {
                   return (string) resourceProvider.GetResource("Sub-Activities", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Performance Indicators</summary>
        public static string PerformanceIndicators {
               get {
                   return (string) resourceProvider.GetResource("Performance Indicators", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>(TOTAL CUM)</summary>
        public static string TotalCum {
               get {
                   return (string) resourceProvider.GetResource("(TOTAL CUM)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ACTIVITY TRENDS</summary>
        public static string ActivityTrends {
               get {
                   return (string) resourceProvider.GetResource("ACTIVITY TRENDS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>WBS</summary>
        public static string Wbs {
               get {
                   return (string) resourceProvider.GetResource("WBS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SUB-ACTIVITY TRENDS</summary>
        public static string SubactivityTrends {
               get {
                   return (string) resourceProvider.GetResource("SUB-ACTIVITY TRENDS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PROGRESS FOLLOW-UP</summary>
        public static string ProgressFollowup {
               get {
                   return (string) resourceProvider.GetResource("PROGRESS FOLLOW-UP", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Save Follow-Up</summary>
        public static string SaveFollowup {
               get {
                   return (string) resourceProvider.GetResource("Save Follow-Up", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Export Data to PDF</summary>
        public static string ExportDataToPdf {
               get {
                   return (string) resourceProvider.GetResource("Export Data to PDF", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PR</summary>
        public static string Pr {
               get {
                   return (string) resourceProvider.GetResource("PR", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Overall Progress</summary>
        public static string OverallProgress {
               get {
                   return (string) resourceProvider.GetResource("Overall Progress", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Progress by Subcontractor</summary>
        public static string ProgressBySubcontractor {
               get {
                   return (string) resourceProvider.GetResource("Progress by Subcontractor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Save Rescheduled Curves</summary>
        public static string SaveRescheduledCurves {
               get {
                   return (string) resourceProvider.GetResource("Save Rescheduled Curves", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Project Overview by Main Activity</summary>
        public static string ProjectOverviewByMainActivity {
               get {
                   return (string) resourceProvider.GetResource("Project Overview by Main Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Each Main Activity is shown respecting a construction sequence</summary>
        public static string EachMainActivityIsShownRespectingAConstructionSequence {
               get {
                   return (string) resourceProvider.GetResource("Each Main Activity is shown respecting a construction sequence", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sub-Act</summary>
        public static string Subact {
               get {
                   return (string) resourceProvider.GetResource("Sub-Act", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Updated at</summary>
        public static string UpdatedAt {
               get {
                   return (string) resourceProvider.GetResource("Updated at", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Overall Project Progress Status</summary>
        public static string OverallProjectProgressStatus {
               get {
                   return (string) resourceProvider.GetResource("Overall Project Progress Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Monthly Progress Status</summary>
        public static string MonthlyProgressStatus {
               get {
                   return (string) resourceProvider.GetResource("Monthly Progress Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Planning &amp; Progress</summary>
        public static string PlanningAmpProgress {
               get {
                   return (string) resourceProvider.GetResource("Planning &amp; Progress", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Reference Quantities</summary>
        public static string ReferenceQuantities {
               get {
                   return (string) resourceProvider.GetResource("Reference Quantities", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Follow-Up by Tags</summary>
        public static string FollowupByTags {
               get {
                   return (string) resourceProvider.GetResource("Follow-Up by Tags", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Progress by Activity</summary>
        public static string ProgressByActivity {
               get {
                   return (string) resourceProvider.GetResource("Progress by Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Reschedule Curves</summary>
        public static string RescheduleCurves {
               get {
                   return (string) resourceProvider.GetResource("Reschedule Curves", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Planning Overview</summary>
        public static string PlanningOverview {
               get {
                   return (string) resourceProvider.GetResource("Planning Overview", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Reference Quantities by Sub-Activity</summary>
        public static string ReferenceQuantitiesBySubactivity {
               get {
                   return (string) resourceProvider.GetResource("Reference Quantities by Sub-Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Discrepancy</summary>
        public static string Discrepancy {
               get {
                   return (string) resourceProvider.GetResource("Discrepancy", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Save Reference Quantities</summary>
        public static string SaveReferenceQuantities {
               get {
                   return (string) resourceProvider.GetResource("Save Reference Quantities", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Import Data from Excel</summary>
        public static string ImportDataFromExcel {
               get {
                   return (string) resourceProvider.GetResource("Import Data from Excel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Export Template to Excel</summary>
        public static string ExportTemplateToExcel {
               get {
                   return (string) resourceProvider.GetResource("Export Template to Excel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Completed</summary>
        public static string Completed {
               get {
                   return (string) resourceProvider.GetResource("Completed", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>UM</summary>
        public static string Um {
               get {
                   return (string) resourceProvider.GetResource("UM", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Quantity</summary>
        public static string Quantity {
               get {
                   return (string) resourceProvider.GetResource("Quantity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Actual IFC</summary>
        public static string ActualIfc {
               get {
                   return (string) resourceProvider.GetResource("Actual IFC", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>MHRS</summary>
        public static string Mhrs {
               get {
                   return (string) resourceProvider.GetResource("MHRS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cut off Date</summary>
        public static string CutOffDate {
               get {
                   return (string) resourceProvider.GetResource("Cut off Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>THIRD DETAIL WORKSHEET</summary>
        public static string ThirdDetailWorksheet {
               get {
                   return (string) resourceProvider.GetResource("THIRD DETAIL WORKSHEET", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PROJ</summary>
        public static string Proj {
               get {
                   return (string) resourceProvider.GetResource("PROJ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>DOC</summary>
        public static string Doc {
               get {
                   return (string) resourceProvider.GetResource("DOC", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Budget MHRS</summary>
        public static string BudgetMhrs {
               get {
                   return (string) resourceProvider.GetResource("Budget MHRS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>REV</summary>
        public static string Rev {
               get {
                   return (string) resourceProvider.GetResource("REV", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SHEET</summary>
        public static string Sheet {
               get {
                   return (string) resourceProvider.GetResource("SHEET", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FORM C3</summary>
        public static string FormC3 {
               get {
                   return (string) resourceProvider.GetResource("FORM C3", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ALL</summary>
        public static string All {
               get {
                   return (string) resourceProvider.GetResource("ALL", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>NOTES</summary>
        public static string Notes {
               get {
                   return (string) resourceProvider.GetResource("NOTES", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PREPARED BY</summary>
        public static string PreparedBy {
               get {
                   return (string) resourceProvider.GetResource("PREPARED BY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CHECKED BY</summary>
        public static string CheckedBy {
               get {
                   return (string) resourceProvider.GetResource("CHECKED BY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>APPROVED BY</summary>
        public static string ApprovedBy {
               get {
                   return (string) resourceProvider.GetResource("APPROVED BY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>WORK STEPS</summary>
        public static string WorkSteps {
               get {
                   return (string) resourceProvider.GetResource("WORK STEPS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Progress % Weight per Work Step</summary>
        public static string ProgressWeightPerWorkStep {
               get {
                   return (string) resourceProvider.GetResource("Progress % Weight per Work Step", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>WEIGHT ON ACTIVITY</summary>
        public static string WeightOnActivity {
               get {
                   return (string) resourceProvider.GetResource("WEIGHT ON ACTIVITY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Completion KPIs</summary>
        public static string CompletionKpis {
               get {
                   return (string) resourceProvider.GetResource("Completion KPIs", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>TOT % Achieved</summary>
        public static string TotAchieved {
               get {
                   return (string) resourceProvider.GetResource("TOT % Achieved", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Equiv Qty Achieved</summary>
        public static string EquivQtyAchieved {
               get {
                   return (string) resourceProvider.GetResource("Equiv Qty Achieved", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>BUDGET QTY</summary>
        public static string BudgetQty {
               get {
                   return (string) resourceProvider.GetResource("BUDGET QTY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Page</summary>
        public static string Page {
               get {
                   return (string) resourceProvider.GetResource("Page", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PROGRESS FORM</summary>
        public static string ProgressForm {
               get {
                   return (string) resourceProvider.GetResource("PROGRESS FORM", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Completion Activities</summary>
        public static string CompletionActivities {
               get {
                   return (string) resourceProvider.GetResource("Completion Activities", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Completion Indicators</summary>
        public static string CompletionIndicators {
               get {
                   return (string) resourceProvider.GetResource("Completion Indicators", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Subcontractor Activities list</summary>
        public static string SubcontractorActivitiesList {
               get {
                   return (string) resourceProvider.GetResource("Subcontractor Activities list", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>KPIs in Evidence updated</summary>
        public static string KpisInEvidenceUpdated {
               get {
                   return (string) resourceProvider.GetResource("KPIs in Evidence updated", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tags assigned except for</summary>
        public static string TagsAssignedExceptFor {
               get {
                   return (string) resourceProvider.GetResource("Tags assigned except for", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System Certificates Follow-up updated</summary>
        public static string SystemCertificatesFollowupUpdated {
               get {
                   return (string) resourceProvider.GetResource("System Certificates Follow-up updated", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Certification Plans</summary>
        public static string CertificationPlans {
               get {
                   return (string) resourceProvider.GetResource("Certification Plans", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Work Activity</summary>
        public static string WorkActivity {
               get {
                   return (string) resourceProvider.GetResource("Work Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>HSE</summary>
        public static string Hse {
               get {
                   return (string) resourceProvider.GetResource("HSE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Personnel Management</summary>
        public static string PersonnelManagement {
               get {
                   return (string) resourceProvider.GetResource("Personnel Management", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Procurement Phases</summary>
        public static string ProcurementPhases {
               get {
                   return (string) resourceProvider.GetResource("Procurement Phases", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Site HSE Monitoring</summary>
        public static string SiteHseMonitoring {
               get {
                   return (string) resourceProvider.GetResource("Site HSE Monitoring", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Warehouse Management</summary>
        public static string WarehouseManagement {
               get {
                   return (string) resourceProvider.GetResource("Warehouse Management", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Construction Workfront</summary>
        public static string ConstructionWorkfront {
               get {
                   return (string) resourceProvider.GetResource("Construction Workfront", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Project Cost Control</summary>
        public static string ProjectCostControl {
               get {
                   return (string) resourceProvider.GetResource("Project Cost Control", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Title HSE</summary>
        public static string TitleHse {
               get {
                   return (string) resourceProvider.GetResource("Title HSE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>HSE KPIs</summary>
        public static string HseKpis {
               get {
                   return (string) resourceProvider.GetResource("HSE KPIs", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Observation</summary>
        public static string Observation {
               get {
                   return (string) resourceProvider.GetResource("Observation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Incident Report</summary>
        public static string IncidentReport {
               get {
                   return (string) resourceProvider.GetResource("Incident Report", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Observations</summary>
        public static string Observations {
               get {
                   return (string) resourceProvider.GetResource("Observations", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Incident Reports</summary>
        public static string IncidentReports {
               get {
                   return (string) resourceProvider.GetResource("Incident Reports", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Months</summary>
        public static string Months {
               get {
                   return (string) resourceProvider.GetResource("Months", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Accident / Incident Report No.</summary>
        public static string AccidentIncidentReportNo {
               get {
                   return (string) resourceProvider.GetResource("Accident / Incident Report No.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Title / Description</summary>
        public static string TitleDescription {
               get {
                   return (string) resourceProvider.GetResource("Title / Description", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Existing Reports</summary>
        public static string ExistingReports {
               get {
                   return (string) resourceProvider.GetResource("Existing Reports", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Print</summary>
        public static string Print {
               get {
                   return (string) resourceProvider.GetResource("Print", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Report Date</summary>
        public static string ReportDate {
               get {
                   return (string) resourceProvider.GetResource("Report Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Closed at Date</summary>
        public static string ClosedAtDate {
               get {
                   return (string) resourceProvider.GetResource("Closed at Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Incident Report Date</summary>
        public static string IncidentReportDate {
               get {
                   return (string) resourceProvider.GetResource("Incident Report Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Report Closed at</summary>
        public static string ReportClosedAt {
               get {
                   return (string) resourceProvider.GetResource("Report Closed at", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Report Time</summary>
        public static string ReportTime {
               get {
                   return (string) resourceProvider.GetResource("Report Time", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Incident / Injury Information</summary>
        public static string IncidentInjuryInformation {
               get {
                   return (string) resourceProvider.GetResource("Incident / Injury Information", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FATALITY</summary>
        public static string Fatality {
               get {
                   return (string) resourceProvider.GetResource("FATALITY", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>LOST TIME INJURIES</summary>
        public static string LostTimeInjuries {
               get {
                   return (string) resourceProvider.GetResource("LOST TIME INJURIES", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>RESTRICTED WORKS CASES</summary>
        public static string RestrictedWorksCases {
               get {
                   return (string) resourceProvider.GetResource("RESTRICTED WORKS CASES", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>MEDICAL TREATMENT</summary>
        public static string MedicalTreatment {
               get {
                   return (string) resourceProvider.GetResource("MEDICAL TREATMENT", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>FIRST AID TREATMENT</summary>
        public static string FirstAidTreatment {
               get {
                   return (string) resourceProvider.GetResource("FIRST AID TREATMENT", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>NEAR MISSES</summary>
        public static string NearMisses {
               get {
                   return (string) resourceProvider.GetResource("NEAR MISSES", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Investigation Information</summary>
        public static string InvestigationInformation {
               get {
                   return (string) resourceProvider.GetResource("Investigation Information", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Place of the Event</summary>
        public static string PlaceOfTheEvent {
               get {
                   return (string) resourceProvider.GetResource("Place of the Event", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Date &amp; Time of Event</summary>
        public static string DateAmpTimeOfEvent {
               get {
                   return (string) resourceProvider.GetResource("Date &amp; Time of Event", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Company Involved</summary>
        public static string CompanyInvolved {
               get {
                   return (string) resourceProvider.GetResource("Company Involved", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Incident Description</summary>
        public static string IncidentDescription {
               get {
                   return (string) resourceProvider.GetResource("Incident Description", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Injured Personnel Name</summary>
        public static string InjuredPersonnelName {
               get {
                   return (string) resourceProvider.GetResource("Injured Personnel Name", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Job Title</summary>
        public static string JobTitle {
               get {
                   return (string) resourceProvider.GetResource("Job Title", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Equipment Damaged</summary>
        public static string EquipmentDamaged {
               get {
                   return (string) resourceProvider.GetResource("Equipment Damaged", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Damage Description</summary>
        public static string DamageDescription {
               get {
                   return (string) resourceProvider.GetResource("Damage Description", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Personnel Job Title</summary>
        public static string PersonnelJobTitle {
               get {
                   return (string) resourceProvider.GetResource("Personnel Job Title", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Supervisor</summary>
        public static string Supervisor {
               get {
                   return (string) resourceProvider.GetResource("Supervisor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Lost Work Days</summary>
        public static string LostWorkDays {
               get {
                   return (string) resourceProvider.GetResource("Lost Work Days", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cost of Repair</summary>
        public static string CostOfRepair {
               get {
                   return (string) resourceProvider.GetResource("Cost of Repair", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Describe More if Needed</summary>
        public static string DescribeMoreIfNeeded {
               get {
                   return (string) resourceProvider.GetResource("Describe More if Needed", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Immediate Causes</summary>
        public static string ImmediateCauses {
               get {
                   return (string) resourceProvider.GetResource("Immediate Causes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Corrective Actions</summary>
        public static string CorrectiveActions {
               get {
                   return (string) resourceProvider.GetResource("Corrective Actions", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Date &amp; Time</summary>
        public static string DateAmpTime {
               get {
                   return (string) resourceProvider.GetResource("Date &amp; Time", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Actions - Personal Causes</summary>
        public static string ActionsPersonalCauses {
               get {
                   return (string) resourceProvider.GetResource("Actions - Personal Causes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Job - Condition Causes</summary>
        public static string JobConditionCauses {
               get {
                   return (string) resourceProvider.GetResource("Job - Condition Causes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>More Details</summary>
        public static string MoreDetails {
               get {
                   return (string) resourceProvider.GetResource("More Details", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Root Causes for Human Factor</summary>
        public static string RootCausesForHumanFactor {
               get {
                   return (string) resourceProvider.GetResource("Root Causes for Human Factor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Root Causes for Work Factor</summary>
        public static string RootCausesForWorkFactor {
               get {
                   return (string) resourceProvider.GetResource("Root Causes for Work Factor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Beavior-Based Safety Form</summary>
        public static string BeaviorbasedSafetyForm {
               get {
                   return (string) resourceProvider.GetResource("Beavior-Based Safety Form", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Observer</summary>
        public static string Observer {
               get {
                   return (string) resourceProvider.GetResource("Observer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Survey Date</summary>
        public static string SurveyDate {
               get {
                   return (string) resourceProvider.GetResource("Survey Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Time</summary>
        public static string Time {
               get {
                   return (string) resourceProvider.GetResource("Time", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>BEHAVIOUR</summary>
        public static string Behaviour {
               get {
                   return (string) resourceProvider.GetResource("BEHAVIOUR", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SAFE</summary>
        public static string Safe {
               get {
                   return (string) resourceProvider.GetResource("SAFE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>AT RISK</summary>
        public static string AtRisk {
               get {
                   return (string) resourceProvider.GetResource("AT RISK", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>POSSIBLE BARRIERS</summary>
        public static string PossibleBarriers {
               get {
                   return (string) resourceProvider.GetResource("POSSIBLE BARRIERS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Date of Observation:</summary>
        public static string DateOfObservation {
               get {
                   return (string) resourceProvider.GetResource("Date of Observation:", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Employee Observed:</summary>
        public static string EmployeeObserved {
               get {
                   return (string) resourceProvider.GetResource("Employee Observed:", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Task:</summary>
        public static string Task {
               get {
                   return (string) resourceProvider.GetResource("Task:", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PEOPLE REACTION</summary>
        public static string PeopleReaction {
               get {
                   return (string) resourceProvider.GetResource("PEOPLE REACTION", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ME:</summary>
        public static string Me {
               get {
                   return (string) resourceProvider.GetResource("ME:", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>IR:</summary>
        public static string Ir {
               get {
                   return (string) resourceProvider.GetResource("IR:", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PPE</summary>
        public static string Ppe {
               get {
                   return (string) resourceProvider.GetResource("PPE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>POSITION OF PEOPLE</summary>
        public static string PositionOfPeople {
               get {
                   return (string) resourceProvider.GetResource("POSITION OF PEOPLE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>TOOLS / EQUIPMENT</summary>
        public static string ToolsEquipment {
               get {
                   return (string) resourceProvider.GetResource("TOOLS / EQUIPMENT", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>JOB FACTOR</summary>
        public static string JobFactor {
               get {
                   return (string) resourceProvider.GetResource("JOB FACTOR", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>HOUSE KEEPING - ENVIROMENT</summary>
        public static string HouseKeepingEnviroment {
               get {
                   return (string) resourceProvider.GetResource("HOUSE KEEPING - ENVIROMENT", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>BARRIERS</summary>
        public static string Barriers {
               get {
                   return (string) resourceProvider.GetResource("BARRIERS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Observers Notes:</summary>
        public static string ObserversNotes {
               get {
                   return (string) resourceProvider.GetResource("Observers Notes:", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Corrective Action Required:</summary>
        public static string CorrectiveActionRequired {
               get {
                   return (string) resourceProvider.GetResource("Corrective Action Required:", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Leading KPI</summary>
        public static string LeadingKpi {
               get {
                   return (string) resourceProvider.GetResource("Leading KPI", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Leading KPIs</summary>
        public static string LeadingKpis {
               get {
                   return (string) resourceProvider.GetResource("Leading KPIs", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Environmental KPIs</summary>
        public static string EnvironmentalKpis {
               get {
                   return (string) resourceProvider.GetResource("Environmental KPIs", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Performance Indicator</summary>
        public static string PerformanceIndicator {
               get {
                   return (string) resourceProvider.GetResource("Performance Indicator", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Environmental Indicator</summary>
        public static string EnvironmentalIndicator {
               get {
                   return (string) resourceProvider.GetResource("Environmental Indicator", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Fatalities</summary>
        public static string Fatalities {
               get {
                   return (string) resourceProvider.GetResource("Fatalities", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>LTI</summary>
        public static string Lti {
               get {
                   return (string) resourceProvider.GetResource("LTI", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>RWC</summary>
        public static string Rwc {
               get {
                   return (string) resourceProvider.GetResource("RWC", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>MTI</summary>
        public static string Mti {
               get {
                   return (string) resourceProvider.GetResource("MTI", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>TRC</summary>
        public static string Trc {
               get {
                   return (string) resourceProvider.GetResource("TRC", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Lost Days</summary>
        public static string LostDays {
               get {
                   return (string) resourceProvider.GetResource("Lost Days", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Fist Aid</summary>
        public static string FistAid {
               get {
                   return (string) resourceProvider.GetResource("Fist Aid", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Damages</summary>
        public static string Damages {
               get {
                   return (string) resourceProvider.GetResource("Damages", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Environment</summary>
        public static string Environment {
               get {
                   return (string) resourceProvider.GetResource("Environment", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Incidents</summary>
        public static string Incidents {
               get {
                   return (string) resourceProvider.GetResource("Incidents", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Serious</summary>
        public static string Serious {
               get {
                   return (string) resourceProvider.GetResource("Serious", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Hazard</summary>
        public static string Hazard {
               get {
                   return (string) resourceProvider.GetResource("Hazard", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Infectious</summary>
        public static string Infectious {
               get {
                   return (string) resourceProvider.GetResource("Infectious", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>TRCF</summary>
        public static string Trcf {
               get {
                   return (string) resourceProvider.GetResource("TRCF", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>LTRI</summary>
        public static string Ltri {
               get {
                   return (string) resourceProvider.GetResource("LTRI", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SR</summary>
        public static string Sr {
               get {
                   return (string) resourceProvider.GetResource("SR", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SIF</summary>
        public static string Sif {
               get {
                   return (string) resourceProvider.GetResource("SIF", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>TIR</summary>
        public static string Tir {
               get {
                   return (string) resourceProvider.GetResource("TIR", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No Significant</summary>
        public static string NoSignificant {
               get {
                   return (string) resourceProvider.GetResource("No Significant", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>HSE Leading Kpi</summary>
        public static string HseLeadingKpi {
               get {
                   return (string) resourceProvider.GetResource("HSE Leading Kpi", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>HSE Incident Causes</summary>
        public static string HseIncidentCauses {
               get {
                   return (string) resourceProvider.GetResource("HSE Incident Causes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Leading Group</summary>
        public static string LeadingGroup {
               get {
                   return (string) resourceProvider.GetResource("Leading Group", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Leading Description</summary>
        public static string LeadingDescription {
               get {
                   return (string) resourceProvider.GetResource("Leading Description", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>HSE Enviroment Kpi</summary>
        public static string HseEnviromentKpi {
               get {
                   return (string) resourceProvider.GetResource("HSE Enviroment Kpi", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Parameter</summary>
        public static string Parameter {
               get {
                   return (string) resourceProvider.GetResource("Parameter", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Units</summary>
        public static string Units {
               get {
                   return (string) resourceProvider.GetResource("Units", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Type</summary>
        public static string Type {
               get {
                   return (string) resourceProvider.GetResource("Type", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>HSE Statistics</summary>
        public static string HseStatistics {
               get {
                   return (string) resourceProvider.GetResource("HSE Statistics", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cumulative HSE Performance</summary>
        public static string CumulativeHsePerformance {
               get {
                   return (string) resourceProvider.GetResource("Cumulative HSE Performance", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Reactive Indicators</summary>
        public static string ReactiveIndicators {
               get {
                   return (string) resourceProvider.GetResource("Reactive Indicators", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Lost Time Incidents</summary>
        public static string LostTimeIncidents {
               get {
                   return (string) resourceProvider.GetResource("Lost Time Incidents", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Restricted WCs</summary>
        public static string RestrictedWcs {
               get {
                   return (string) resourceProvider.GetResource("Restricted WCs", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>MTIs &amp; First Aids</summary>
        public static string MtisAmpFirstAids {
               get {
                   return (string) resourceProvider.GetResource("MTIs &amp; First Aids", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>This Month</summary>
        public static string ThisMonth {
               get {
                   return (string) resourceProvider.GetResource("This Month", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Year to Date</summary>
        public static string YearToDate {
               get {
                   return (string) resourceProvider.GetResource("Year to Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cumulative</summary>
        public static string Cumulative {
               get {
                   return (string) resourceProvider.GetResource("Cumulative", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Recordable Cases</summary>
        public static string RecordableCases {
               get {
                   return (string) resourceProvider.GetResource("Recordable Cases", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Lost Time Incidents (LTI)</summary>
        public static string LostTimeIncidentsLti {
               get {
                   return (string) resourceProvider.GetResource("Lost Time Incidents (LTI)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Restricted Work Classes (RWC)</summary>
        public static string RestrictedWorkClassesRwc {
               get {
                   return (string) resourceProvider.GetResource("Restricted Work Classes (RWC)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Meadical Treatment Injuries (MTI)</summary>
        public static string MeadicalTreatmentInjuriesMti {
               get {
                   return (string) resourceProvider.GetResource("Meadical Treatment Injuries (MTI)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>First Aid Cases (FAC)</summary>
        public static string FirstAidCasesFac {
               get {
                   return (string) resourceProvider.GetResource("First Aid Cases (FAC)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Near Misses / Non Conformities</summary>
        public static string NearMissesNonConformities {
               get {
                   return (string) resourceProvider.GetResource("Near Misses / Non Conformities", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Environmental Incidents</summary>
        public static string EnvironmentalIncidents {
               get {
                   return (string) resourceProvider.GetResource("Environmental Incidents", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Property Damages</summary>
        public static string PropertyDamages {
               get {
                   return (string) resourceProvider.GetResource("Property Damages", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>HSE Barriers</summary>
        public static string HseBarriers {
               get {
                   return (string) resourceProvider.GetResource("HSE Barriers", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Existing Observations</summary>
        public static string ExistingObservations {
               get {
                   return (string) resourceProvider.GetResource("Existing Observations", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>BBS Indicators</summary>
        public static string BbsIndicators {
               get {
                   return (string) resourceProvider.GetResource("BBS Indicators", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>BBS Results</summary>
        public static string BbsResults {
               get {
                   return (string) resourceProvider.GetResource("BBS Results", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>BBS by SubContractor</summary>
        public static string BbsBySubcontractor {
               get {
                   return (string) resourceProvider.GetResource("BBS by SubContractor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>BBS by Observer</summary>
        public static string BbsByObserver {
               get {
                   return (string) resourceProvider.GetResource("BBS by Observer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>BBS by Activity</summary>
        public static string BbsByActivity {
               get {
                   return (string) resourceProvider.GetResource("BBS by Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>BBS Form</summary>
        public static string BbsForm {
               get {
                   return (string) resourceProvider.GetResource("BBS Form", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>BBS by Main Activity</summary>
        public static string BbsByMainActivity {
               get {
                   return (string) resourceProvider.GetResource("BBS by Main Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Brief Incident Descriprion</summary>
        public static string BriefIncidentDescriprion {
               get {
                   return (string) resourceProvider.GetResource("Brief Incident Descriprion", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>New</summary>
        public static string New {
               get {
                   return (string) resourceProvider.GetResource("New", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Export</summary>
        public static string Export {
               get {
                   return (string) resourceProvider.GetResource("Export", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>HSE Root Causes</summary>
        public static string HseRootCauses {
               get {
                   return (string) resourceProvider.GetResource("HSE Root Causes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>HSE Environment Kpi</summary>
        public static string HseEnvironmentKpi {
               get {
                   return (string) resourceProvider.GetResource("HSE Environment Kpi", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Site Personnel Management</summary>
        public static string SitePersonnelManagement {
               get {
                   return (string) resourceProvider.GetResource("Site Personnel Management", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Personnel KPIs</summary>
        public static string PersonnelKpis {
               get {
                   return (string) resourceProvider.GetResource("Personnel KPIs", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Direct Workers</summary>
        public static string DirectWorkers {
               get {
                   return (string) resourceProvider.GetResource("Direct Workers", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Worktrade Configuration</summary>
        public static string WorktradeConfiguration {
               get {
                   return (string) resourceProvider.GetResource("Worktrade Configuration", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Ind. Workers</summary>
        public static string IndWorkers {
               get {
                   return (string) resourceProvider.GetResource("Ind. Workers", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mob / Demob</summary>
        public static string MobDemob {
               get {
                   return (string) resourceProvider.GetResource("Mob / Demob", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Responsabilities Configuration</summary>
        public static string ResponsabilitiesConfiguration {
               get {
                   return (string) resourceProvider.GetResource("Responsabilities Configuration", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Main Equipment</summary>
        public static string MainEquipment {
               get {
                   return (string) resourceProvider.GetResource("Main Equipment", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Responsabilities Config.</summary>
        public static string ResponsabilitiesConfig {
               get {
                   return (string) resourceProvider.GetResource("Responsabilities Config.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Config.</summary>
        public static string Config {
               get {
                   return (string) resourceProvider.GetResource("Config.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Daily Follow-Up</summary>
        public static string DailyFollowup {
               get {
                   return (string) resourceProvider.GetResource("Daily Follow-Up", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tags Involved</summary>
        public static string TagsInvolved {
               get {
                   return (string) resourceProvider.GetResource("Tags Involved", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Edit Worktrade</summary>
        public static string EditWorktrade {
               get {
                   return (string) resourceProvider.GetResource("Edit Worktrade", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Database</summary>
        public static string Database {
               get {
                   return (string) resourceProvider.GetResource("Database", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Worktrade by Main Activities</summary>
        public static string WorktradeByMainActivities {
               get {
                   return (string) resourceProvider.GetResource("Worktrade by Main Activities", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Spent MHRS</summary>
        public static string SpentMhrs {
               get {
                   return (string) resourceProvider.GetResource("Spent MHRS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pending Tag Dossiers</summary>
        public static string PendingTagDossiers {
               get {
                   return (string) resourceProvider.GetResource("Pending Tag Dossiers", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CWP OVERALL STATUS</summary>
        public static string CwpOverallStatus {
               get {
                   return (string) resourceProvider.GetResource("CWP OVERALL STATUS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>WORKTRADE</summary>
        public static string Worktrade {
               get {
                   return (string) resourceProvider.GetResource("WORKTRADE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Direct Worktrades by Main Activities (Construction Phase)</summary>
        public static string DirectWorktradesByMainActivitiesConstructionPhase {
               get {
                   return (string) resourceProvider.GetResource("Direct Worktrades by Main Activities (Construction Phase)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Direct Worktrades Configuration</summary>
        public static string DirectWorktradesConfiguration {
               get {
                   return (string) resourceProvider.GetResource("Direct Worktrades Configuration", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Direct Worktrades</summary>
        public static string DirectWorktrades {
               get {
                   return (string) resourceProvider.GetResource("Direct Worktrades", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Indirect Responsibilities</summary>
        public static string IndirectResponsibilities {
               get {
                   return (string) resourceProvider.GetResource("Indirect Responsibilities", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Indirect Workers MOB/DEMOB</summary>
        public static string IndirectWorkersMobdemob {
               get {
                   return (string) resourceProvider.GetResource("Indirect Workers MOB/DEMOB", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Main Equipment MOB/DEMOB</summary>
        public static string MainEquipmentMobdemob {
               get {
                   return (string) resourceProvider.GetResource("Main Equipment MOB/DEMOB", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Improvements Required (%) by SubContractor</summary>
        public static string ImprovementsRequiredBySubcontractor {
               get {
                   return (string) resourceProvider.GetResource("Improvements Required (%) by SubContractor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>This<br>Month</summary>
        public static string Thisbrmonth {
               get {
                   return (string) resourceProvider.GetResource("This<br>Month", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Year<br>to Date</summary>
        public static string YearbrtoDate {
               get {
                   return (string) resourceProvider.GetResource("Year<br>to Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Date</summary>
        public static string Date {
               get {
                   return (string) resourceProvider.GetResource("Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Site Personnel Daily Follow-Up</summary>
        public static string SitePersonnelDailyFollowup {
               get {
                   return (string) resourceProvider.GetResource("Site Personnel Daily Follow-Up", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Behavior-Based Safety Form</summary>
        public static string BehaviorbasedSafetyForm {
               get {
                   return (string) resourceProvider.GetResource("Behavior-Based Safety Form", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>item type</summary>
        public static string ItemType {
               get {
                   return (string) resourceProvider.GetResource("item type", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Item Type</summary>
        public static string Itemtype {
               get {
                   return (string) resourceProvider.GetResource("Item Type", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SubContractors Performance</summary>
        public static string SubcontractorsPerformance {
               get {
                   return (string) resourceProvider.GetResource("SubContractors Performance", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Based on Planned Mhrs vs Spent Mhrs</summary>
        public static string BasedOnPlannedMhrsVsSpentMhrs {
               get {
                   return (string) resourceProvider.GetResource("Based on Planned Mhrs vs Spent Mhrs", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Based on Direct Manpower</summary>
        public static string BasedOnDirectManpower {
               get {
                   return (string) resourceProvider.GetResource("Based on Direct Manpower", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Based on Weekly Average Manpower</summary>
        public static string BasedOnWeeklyAverageManpower {
               get {
                   return (string) resourceProvider.GetResource("Based on Weekly Average Manpower", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Based on Weekly Average Manpower at Site</summary>
        public static string BasedOnWeeklyAverageManpowerAtSite {
               get {
                   return (string) resourceProvider.GetResource("Based on Weekly Average Manpower at Site", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>SubContractor Crews Composition</summary>
        public static string SubcontractorCrewsComposition {
               get {
                   return (string) resourceProvider.GetResource("SubContractor Crews Composition", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Based on Weekly Average Worktrades at Site</summary>
        public static string BasedOnWeeklyAverageWorktradesAtSite {
               get {
                   return (string) resourceProvider.GetResource("Based on Weekly Average Worktrades at Site", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pipe Size</summary>
        public static string Pipesize {
               get {
                   return (string) resourceProvider.GetResource("Pipe Size", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Based on Weekly Crew Composition</summary>
        public static string BasedOnWeeklyCrewComposition {
               get {
                   return (string) resourceProvider.GetResource("Based on Weekly Crew Composition", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Weekly Average Manpower</summary>
        public static string WeeklyAverageManpower {
               get {
                   return (string) resourceProvider.GetResource("Weekly Average Manpower", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Weekly Crew Composition</summary>
        public static string WeeklyCrewComposition {
               get {
                   return (string) resourceProvider.GetResource("Weekly Crew Composition", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Weekly Average Manpower at Site</summary>
        public static string WeeklyAverageManpowerAtSite {
               get {
                   return (string) resourceProvider.GetResource("Weekly Average Manpower at Site", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Testpack/ISOs Saved</summary>
        public static string TestpackisosSaved {
               get {
                   return (string) resourceProvider.GetResource("Testpack/ISOs Saved", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Saved</summary>
        public static string Saved {
               get {
                   return (string) resourceProvider.GetResource("Saved", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Workface Planning</summary>
        public static string WorkfacePlanning {
               get {
                   return (string) resourceProvider.GetResource("Workface Planning", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>must be specified</summary>
        public static string MustBeSpecified {
               get {
                   return (string) resourceProvider.GetResource("must be specified", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Workfront / Workface Management</summary>
        public static string WorkfrontWorkfaceManagement {
               get {
                   return (string) resourceProvider.GetResource("Workfront / Workface Management", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Construction Work Package</summary>
        public static string ConstructionWorkPackage {
               get {
                   return (string) resourceProvider.GetResource("Construction Work Package", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>must be in range</summary>
        public static string MustBeInRange {
               get {
                   return (string) resourceProvider.GetResource("must be in range", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>cannot be null or empty</summary>
        public static string CannotBeNullOrEmpty {
               get {
                   return (string) resourceProvider.GetResource("cannot be null or empty", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>You can only edit your project details, not the details of another project.</summary>
        public static string YouCanOnlyEditYourProjectDetailsNotTheDetailsOfAnotherProject {
               get {
                   return (string) resourceProvider.GetResource("You can only edit your project details, not the details of another project.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>You can only get data for your project, not for another project.</summary>
        public static string YouCanOnlyGetDataForYourProjectNotForAnotherProject {
               get {
                   return (string) resourceProvider.GetResource("You can only get data for your project, not for another project.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>must be a valid double number</summary>
        public static string MustBeAValidDoubleNumber {
               get {
                   return (string) resourceProvider.GetResource("must be a valid double number", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Details</summary>
        public static string Details {
               get {
                   return (string) resourceProvider.GetResource("Details", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Data</summary>
        public static string Data {
               get {
                   return (string) resourceProvider.GetResource("Data", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Invalid selected dates</summary>
        public static string InvalidSelectedDates {
               get {
                   return (string) resourceProvider.GetResource("Invalid selected dates", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Field Installation Work Packages  - Details</summary>
        public static string FieldInstallationWorkPackagesDetails {
               get {
                   return (string) resourceProvider.GetResource("Field Installation Work Packages  - Details", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pipe and Fittings</summary>
        public static string PipeAndFittings {
               get {
                   return (string) resourceProvider.GetResource("Pipe and Fittings", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Signatures</summary>
        public static string Signatures {
               get {
                   return (string) resourceProvider.GetResource("Signatures", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pipe Size</summary>
        public static string PipeSize {
               get {
                   return (string) resourceProvider.GetResource("Pipe Size", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Item Coat</summary>
        public static string ItemCoat {
               get {
                   return (string) resourceProvider.GetResource("Item Coat", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Batch Type</summary>
        public static string BatchType {
               get {
                   return (string) resourceProvider.GetResource("Batch Type", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pipeline Definition</summary>
        public static string PipelineDefinition {
               get {
                   return (string) resourceProvider.GetResource("Pipeline Definition", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Yard Location</summary>
        public static string YardLocation {
               get {
                   return (string) resourceProvider.GetResource("Yard Location", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Welding Procedures</summary>
        public static string WeldingProcedures {
               get {
                   return (string) resourceProvider.GetResource("Welding Procedures", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Welders Credentials</summary>
        public static string WeldersCredentials {
               get {
                   return (string) resourceProvider.GetResource("Welders Credentials", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Weld Procedure Parameter Limits</summary>
        public static string WeldProcedureParameterLimits {
               get {
                   return (string) resourceProvider.GetResource("Weld Procedure Parameter Limits", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Inspector Credentials</summary>
        public static string InspectorCredentials {
               get {
                   return (string) resourceProvider.GetResource("Inspector Credentials", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Inspector Qualifications</summary>
        public static string InspectorQualifications {
               get {
                   return (string) resourceProvider.GetResource("Inspector Qualifications", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Examination Procedures</summary>
        public static string ExaminationProcedures {
               get {
                   return (string) resourceProvider.GetResource("Examination Procedures", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Examination Type</summary>
        public static string ExaminationType {
               get {
                   return (string) resourceProvider.GetResource("Examination Type", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Spacer Standard Length</summary>
        public static string SpacerStandardLength {
               get {
                   return (string) resourceProvider.GetResource("Spacer Standard Length", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>accordion-overview</summary>
        public static string Accordionoverview {
               get {
                   return (string) resourceProvider.GetResource("accordion-overview", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tag Code</summary>
        public static string TagCode {
               get {
                   return (string) resourceProvider.GetResource("Tag Code", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Code (identifier)</summary>
        public static string CodeIdentifier {
               get {
                   return (string) resourceProvider.GetResource("Code (identifier)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Scheduled Week</summary>
        public static string ScheduledWeek {
               get {
                   return (string) resourceProvider.GetResource("Scheduled Week", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>the code for the subcontractor</summary>
        public static string TheCodeForTheSubcontractor {
               get {
                   return (string) resourceProvider.GetResource("the code for the subcontractor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The code for the selected subcontractor</summary>
        public static string TheCodeForTheSelectedSubcontractor {
               get {
                   return (string) resourceProvider.GetResource("The code for the selected subcontractor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The value of sub-activity</summary>
        public static string TheValueOfSubactivity {
               get {
                   return (string) resourceProvider.GetResource("The value of sub-activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Operation</summary>
        public static string Operation {
               get {
                   return (string) resourceProvider.GetResource("Operation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Insert Similar</summary>
        public static string InsertSimilar {
               get {
                   return (string) resourceProvider.GetResource("Insert Similar", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Insert</summary>
        public static string Insert {
               get {
                   return (string) resourceProvider.GetResource("Insert", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Update</summary>
        public static string Update {
               get {
                   return (string) resourceProvider.GetResource("Update", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No file to upload</summary>
        public static string NoFileToUpload {
               get {
                   return (string) resourceProvider.GetResource("No file to upload", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>File not found.</summary>
        public static string FileNotFound {
               get {
                   return (string) resourceProvider.GetResource("File not found.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System code</summary>
        public static string SystemCode {
               get {
                   return (string) resourceProvider.GetResource("System code", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>System tag</summary>
        public static string SystemTag {
               get {
                   return (string) resourceProvider.GetResource("System tag", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Row Id</summary>
        public static string RowId {
               get {
                   return (string) resourceProvider.GetResource("Row Id", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The main activity code</summary>
        public static string TheMainActivityCode {
               get {
                   return (string) resourceProvider.GetResource("The main activity code", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>When filtering the empty sub-activities, main activity must be specified.</summary>
        public static string WhenFilteringTheEmptySubactivitiesMainActivityMustBeSpecified {
               get {
                   return (string) resourceProvider.GetResource("When filtering the empty sub-activities, main activity must be specified.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Activities list cannot be null or empty.</summary>
        public static string ActivitiesListCannotBeNullOrEmpty {
               get {
                   return (string) resourceProvider.GetResource("Activities list cannot be null or empty.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Number of months cannot be 0.</summary>
        public static string NumberOfMonthsCannotBe {
               get {
                   return (string) resourceProvider.GetResource("Number of months cannot be 0.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Progress column</summary>
        public static string ProgressColumn {
               get {
                   return (string) resourceProvider.GetResource("Progress column", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Progress column value</summary>
        public static string ProgressColumnValue {
               get {
                   return (string) resourceProvider.GetResource("Progress column value", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Related activity</summary>
        public static string RelatedActivity {
               get {
                   return (string) resourceProvider.GetResource("Related activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Preservation List</summary>
        public static string PreservationList {
               get {
                   return (string) resourceProvider.GetResource("Preservation List", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Update list</summary>
        public static string UpdateList {
               get {
                   return (string) resourceProvider.GetResource("Update list", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Completion data</summary>
        public static string CompletionData {
               get {
                   return (string) resourceProvider.GetResource("Completion data", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Selected Subcontractor Code</summary>
        public static string SelectedSubcontractorCode {
               get {
                   return (string) resourceProvider.GetResource("Selected Subcontractor Code", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Values list</summary>
        public static string ValuesList {
               get {
                   return (string) resourceProvider.GetResource("Values list", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Request data</summary>
        public static string RequestData {
               get {
                   return (string) resourceProvider.GetResource("Request data", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>File Path</summary>
        public static string FilePath {
               get {
                   return (string) resourceProvider.GetResource("File Path", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The sufix</summary>
        public static string TheSufix {
               get {
                   return (string) resourceProvider.GetResource("The sufix", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Commissioning Data</summary>
        public static string CommissioningData {
               get {
                   return (string) resourceProvider.GetResource("Commissioning Data", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Steps data</summary>
        public static string StepsData {
               get {
                   return (string) resourceProvider.GetResource("Steps data", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pds</summary>
        public static string Pds {
               get {
                   return (string) resourceProvider.GetResource("Pds", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Isometric Data</summary>
        public static string IsometricData {
               get {
                   return (string) resourceProvider.GetResource("Isometric Data", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Required Data</summary>
        public static string RequiredData {
               get {
                   return (string) resourceProvider.GetResource("Required Data", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Invalid input.</summary>
        public static string InvalidInput {
               get {
                   return (string) resourceProvider.GetResource("Invalid input.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Only admins can ADD users</summary>
        public static string OnlyAdminsCanAddUsers {
               get {
                   return (string) resourceProvider.GetResource("Only admins can ADD users", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>User not associated to any project</summary>
        public static string UserNotAssociatedToAnyProject {
               get {
                   return (string) resourceProvider.GetResource("User not associated to any project", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Invalid login attempt. Username/password don't match.</summary>
        public static string InvalidLoginAttemptUsernamepasswordDontMatch {
               get {
                   return (string) resourceProvider.GetResource("Invalid login attempt. Username/password don't match.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>User ID and Project ID must be specified.</summary>
        public static string UserIdAndProjectIdMustBeSpecified {
               get {
                   return (string) resourceProvider.GetResource("User ID and Project ID must be specified.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>User ID Must be specified.</summary>
        public static string UserIdMustBeSpecified {
               get {
                   return (string) resourceProvider.GetResource("User ID Must be specified.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>You can edit other user's details only if you are an admin, otherwise you can only edit your info.</summary>
        public static string YouCanEditOtherUsersDetailsOnlyIfYouAreAnAdminOtherwiseYouCanOnlyEditYourInfo {
               get {
                   return (string) resourceProvider.GetResource("You can edit other user's details only if you are an admin, otherwise you can only edit your info.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>All data updated successfully. Roles change will propagate in a maximum of </summary>
        public static string AllDataUpdatedSuccessfullyRolesChangeWillPropagateInAMaximumOf {
               get {
                   return (string) resourceProvider.GetResource("All data updated successfully. Roles change will propagate in a maximum of ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary> minutes or at logout.</summary>
        public static string MinutesOrAtLogout {
               get {
                   return (string) resourceProvider.GetResource(" minutes or at logout.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Data updated successfully. Some fields were not updated because you are not an admin (username and roles).</summary>
        public static string DataUpdatedSuccessfullySomeFieldsWereNotUpdatedBecauseYouAreNotAnAdminUsernameAndRoles {
               get {
                   return (string) resourceProvider.GetResource("Data updated successfully. Some fields were not updated because you are not an admin (username and roles).", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Only Admins can DELETE users.</summary>
        public static string OnlyAdminsCanDeleteUsers {
               get {
                   return (string) resourceProvider.GetResource("Only Admins can DELETE users.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>User successfully deleted.</summary>
        public static string UserSuccessfullyDeleted {
               get {
                   return (string) resourceProvider.GetResource("User successfully deleted.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>You have deleted your own account. You will now be redirected to login page.</summary>
        public static string YouHaveDeletedYourOwnAccountYouWillNowBeRedirectedToLoginPage {
               get {
                   return (string) resourceProvider.GetResource("You have deleted your own account. You will now be redirected to login page.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Theme must be specified.</summary>
        public static string ThemeMustBeSpecified {
               get {
                   return (string) resourceProvider.GetResource("Theme must be specified.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Start</summary>
        public static string Start {
               get {
                   return (string) resourceProvider.GetResource("Start", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>End</summary>
        public static string End {
               get {
                   return (string) resourceProvider.GetResource("End", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>An error occurred while processing your request.</summary>
        public static string AnErrorOccurredWhileProcessingYourRequest {
               get {
                   return (string) resourceProvider.GetResource("An error occurred while processing your request.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Dark Blue Theme</summary>
        public static string DarkBlueTheme {
               get {
                   return (string) resourceProvider.GetResource("Dark Blue Theme", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Dark Purple Theme</summary>
        public static string DarkPurpleTheme {
               get {
                   return (string) resourceProvider.GetResource("Dark Purple Theme", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Dark Red Theme</summary>
        public static string DarkRedTheme {
               get {
                   return (string) resourceProvider.GetResource("Dark Red Theme", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Dark Grey Theme</summary>
        public static string DarkGreyTheme {
               get {
                   return (string) resourceProvider.GetResource("Dark Grey Theme", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Dark Green Theme</summary>
        public static string DarkGreenTheme {
               get {
                   return (string) resourceProvider.GetResource("Dark Green Theme", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Dark Brown Theme</summary>
        public static string DarkBrownTheme {
               get {
                   return (string) resourceProvider.GetResource("Dark Brown Theme", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Light theme</summary>
        public static string LightTheme {
               get {
                   return (string) resourceProvider.GetResource("Light theme", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Material</summary>
        public static string Material {
               get {
                   return (string) resourceProvider.GetResource("Material", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Flip</summary>
        public static string Flip {
               get {
                   return (string) resourceProvider.GetResource("Flip", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>In parameters.</summary>
        public static string InParameters {
               get {
                   return (string) resourceProvider.GetResource("In parameters.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No data</summary>
        public static string NoData {
               get {
                   return (string) resourceProvider.GetResource("No data", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Initialization</summary>
        public static string Initialization {
               get {
                   return (string) resourceProvider.GetResource("Initialization", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Select Date</summary>
        public static string SelectDate {
               get {
                   return (string) resourceProvider.GetResource("Select Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Plan</summary>
        public static string Plan {
               get {
                   return (string) resourceProvider.GetResource("Plan", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Maintenance Frequency</summary>
        public static string MaintenanceFrequency {
               get {
                   return (string) resourceProvider.GetResource("Maintenance Frequency", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>(wk):</summary>
        public static string Wk {
               get {
                   return (string) resourceProvider.GetResource("(wk):", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Prev</summary>
        public static string Prev {
               get {
                   return (string) resourceProvider.GetResource("Prev", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Next</summary>
        public static string Next {
               get {
                   return (string) resourceProvider.GetResource("Next", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Favorites KPI Configuration</summary>
        public static string FavoritesKpiConfiguration {
               get {
                   return (string) resourceProvider.GetResource("Favorites KPI Configuration", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>KPI in Evidence</summary>
        public static string KpiInEvidence {
               get {
                   return (string) resourceProvider.GetResource("KPI in Evidence", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>KPIs Configurator</summary>
        public static string KpisConfigurator {
               get {
                   return (string) resourceProvider.GetResource("KPIs Configurator", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Overwrite</summary>
        public static string Overwrite {
               get {
                   return (string) resourceProvider.GetResource("Overwrite", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Uploading</summary>
        public static string Uploading {
               get {
                   return (string) resourceProvider.GetResource("Uploading", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Processing...</summary>
        public static string Processing {
               get {
                   return (string) resourceProvider.GetResource("Processing...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Civil Schemas</summary>
        public static string CivilSchemas {
               get {
                   return (string) resourceProvider.GetResource("Civil Schemas", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tags to be Assigned</summary>
        public static string TagsToBeAssigned {
               get {
                   return (string) resourceProvider.GetResource("Tags to be Assigned", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tags in Follow-Up</summary>
        public static string TagsInFollowup {
               get {
                   return (string) resourceProvider.GetResource("Tags in Follow-Up", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Inspections Overview</summary>
        public static string InspectionsOverview {
               get {
                   return (string) resourceProvider.GetResource("Inspections Overview", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Inspector</summary>
        public static string Inspector {
               get {
                   return (string) resourceProvider.GetResource("Inspector", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>View All Tags</summary>
        public static string ViewAllTags {
               get {
                   return (string) resourceProvider.GetResource("View All Tags", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary> View Not Assigned</summary>
        public static string ViewNotAssigned {
               get {
                   return (string) resourceProvider.GetResource(" View Not Assigned", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Select</summary>
        public static string Select {
               get {
                   return (string) resourceProvider.GetResource("Select", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Deselect</summary>
        public static string Deselect {
               get {
                   return (string) resourceProvider.GetResource("Deselect", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Instrument Tag</summary>
        public static string InstrumentTag {
               get {
                   return (string) resourceProvider.GetResource("Instrument Tag", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Step</summary>
        public static string Step {
               get {
                   return (string) resourceProvider.GetResource("Step", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Last</summary>
        public static string Last {
               get {
                   return (string) resourceProvider.GetResource("Last", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Only Not Assigned</summary>
        public static string OnlyNotAssigned {
               get {
                   return (string) resourceProvider.GetResource("Only Not Assigned", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Remove</summary>
        public static string Remove {
               get {
                   return (string) resourceProvider.GetResource("Remove", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>User Id</summary>
        public static string UserId {
               get {
                   return (string) resourceProvider.GetResource("User Id", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Login</summary>
        public static string Login {
               get {
                   return (string) resourceProvider.GetResource("Login", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Copyright</summary>
        public static string Copyright {
               get {
                   return (string) resourceProvider.GetResource("Copyright", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PL</summary>
        public static string Pl {
               get {
                   return (string) resourceProvider.GetResource("PL", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PL Systems</summary>
        public static string PlSystems {
               get {
                   return (string) resourceProvider.GetResource("PL Systems", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Inspection</summary>
        public static string Inspection {
               get {
                   return (string) resourceProvider.GetResource("Inspection", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Accepted</summary>
        public static string Accepted {
               get {
                   return (string) resourceProvider.GetResource("Accepted", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Acceptance</summary>
        public static string Acceptance {
               get {
                   return (string) resourceProvider.GetResource("Acceptance", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>There is not data for that user id.</summary>
        public static string ThereIsNotDataForThatUserId {
               get {
                   return (string) resourceProvider.GetResource("There is not data for that user id.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>There is no data for that username / project association.</summary>
        public static string ThereIsNoDataForThatUsernameProjectAssociation {
               get {
                   return (string) resourceProvider.GetResource("There is no data for that username / project association.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Update succesful</summary>
        public static string UpdateSuccesful {
               get {
                   return (string) resourceProvider.GetResource("Update succesful", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>TAG DOSSIER not available</summary>
        public static string TagDossierNotAvailable {
               get {
                   return (string) resourceProvider.GetResource("TAG DOSSIER not available", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>TAG DOSSIER uploaded at</summary>
        public static string TagDossierUploadedAt {
               get {
                   return (string) resourceProvider.GetResource("TAG DOSSIER uploaded at", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Ready To Loop Check</summary>
        public static string ReadyToLoopCheck {
               get {
                   return (string) resourceProvider.GetResource("Ready To Loop Check", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cannot retrieve columns for table </summary>
        public static string CannotRetrieveColumnsForTable {
               get {
                   return (string) resourceProvider.GetResource("Cannot retrieve columns for table ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The record already exists</summary>
        public static string TheRecordAlreadyExists {
               get {
                   return (string) resourceProvider.GetResource("The record already exists", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>A record with the same identifier already exists</summary>
        public static string ARecordWithTheSameIdentifierAlreadyExists {
               get {
                   return (string) resourceProvider.GetResource("A record with the same identifier already exists", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The following fields need to be populated: </summary>
        public static string TheFollowingFieldsNeedToBePopulated {
               get {
                   return (string) resourceProvider.GetResource("The following fields need to be populated: ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Error at inserting data on row </summary>
        public static string ErrorAtInsertingDataOnRow {
               get {
                   return (string) resourceProvider.GetResource("Error at inserting data on row ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cannot create the .xls file. </summary>
        public static string CannotCreateTheXlsFile {
               get {
                   return (string) resourceProvider.GetResource("Cannot create the .xls file. ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>There is no Excel Document opened in the service, cannot execute operations on nothing.</summary>
        public static string ThereIsNoExcelDocumentOpenedInTheServiceCannotExecuteOperationsOnNothing {
               get {
                   return (string) resourceProvider.GetResource("There is no Excel Document opened in the service, cannot execute operations on nothing.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cannot add rows to the .xls file. </summary>
        public static string CannotAddRowsToTheXlsFile {
               get {
                   return (string) resourceProvider.GetResource("Cannot add rows to the .xls file. ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cannot find the desired worksheet in the document.</summary>
        public static string CannotFindTheDesiredWorksheetInTheDocument {
               get {
                   return (string) resourceProvider.GetResource("Cannot find the desired worksheet in the document.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The column </summary>
        public static string TheColumn {
               get {
                   return (string) resourceProvider.GetResource("The column ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>  cannot be found in the document, in the sheet  </summary>
        public static string CannotBeFoundInTheDocumentInTheSheet {
               get {
                   return (string) resourceProvider.GetResource("  cannot be found in the document, in the sheet  ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary> is required and cannot be empty. Error at row </summary>
        public static string IsRequiredAndCannotBeEmptyErrorAtRow {
               get {
                   return (string) resourceProvider.GetResource(" is required and cannot be empty. Error at row ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary> in the sheet </summary>
        public static string InTheSheet {
               get {
                   return (string) resourceProvider.GetResource(" in the sheet ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary> week(s)</summary>
        public static string Weeks {
               get {
                   return (string) resourceProvider.GetResource(" week(s)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No Delay</summary>
        public static string NoDelay {
               get {
                   return (string) resourceProvider.GetResource("No Delay", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No</summary>
        public static string No {
               get {
                   return (string) resourceProvider.GetResource("No", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Invalid number specified.</summary>
        public static string InvalidNumberSpecified {
               get {
                   return (string) resourceProvider.GetResource("Invalid number specified.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Number must be a valid percent.</summary>
        public static string NumberMustBeAValidPercent {
               get {
                   return (string) resourceProvider.GetResource("Number must be a valid percent.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cannot find the specified project.</summary>
        public static string CannotFindTheSpecifiedProject {
               get {
                   return (string) resourceProvider.GetResource("Cannot find the specified project.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Invalid argument: username.</summary>
        public static string InvalidArgumentUsername {
               get {
                   return (string) resourceProvider.GetResource("Invalid argument: username.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Trying to update a project that does not exist in the database.</summary>
        public static string TryingToUpdateAProjectThatDoesNotExistInTheDatabase {
               get {
                   return (string) resourceProvider.GetResource("Trying to update a project that does not exist in the database.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Invalid request. When filtering the empty sub-activities, main activity must be specified.</summary>
        public static string InvalidRequestWhenFilteringTheEmptySubactivitiesMainActivityMustBeSpecified {
               get {
                   return (string) resourceProvider.GetResource("Invalid request. When filtering the empty sub-activities, main activity must be specified.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Trying to change context for invalid repository.</summary>
        public static string TryingToChangeContextForInvalidRepository {
               get {
                   return (string) resourceProvider.GetResource("Trying to change context for invalid repository.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary><i>Click on <b>Save</b> to make the change effective</i></summary>
        public static string IclickOnBsavebToMakeTheChangeEffectivei {
               get {
                   return (string) resourceProvider.GetResource("<i>Click on <b>Save</b> to make the change effective</i>", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The number of progress distribution values does not match the selected time interval. Make sure you click the SHUFFLE button after each date change.</summary>
        public static string TheNumberOfProgressDistributionValuesDoesNotMatchTheSelectedTimeIntervalMakeSureYouClickTheShuffleButtonAfterEachDateChange {
               get {
                   return (string) resourceProvider.GetResource("The number of progress distribution values does not match the selected time interval. Make sure you click the SHUFFLE button after each date change.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Encountered invalid values for progress distribution.</summary>
        public static string EncounteredInvalidValuesForProgressDistribution {
               get {
                   return (string) resourceProvider.GetResource("Encountered invalid values for progress distribution.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Encountered invalid selected start and end dates.</summary>
        public static string EncounteredInvalidSelectedStartAndEndDates {
               get {
                   return (string) resourceProvider.GetResource("Encountered invalid selected start and end dates.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Encountered invalid MH inserted.</summary>
        public static string EncounteredInvalidMhInserted {
               get {
                   return (string) resourceProvider.GetResource("Encountered invalid MH inserted.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>A main activity must be selected in order to continue.</summary>
        public static string AMainActivityMustBeSelectedInOrderToContinue {
               get {
                   return (string) resourceProvider.GetResource("A main activity must be selected in order to continue.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>There is no data for that selection.</summary>
        public static string ThereIsNoDataForThatSelection {
               get {
                   return (string) resourceProvider.GetResource("There is no data for that selection.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Functionality not available.</summary>
        public static string FunctionalityNotAvailable {
               get {
                   return (string) resourceProvider.GetResource("Functionality not available.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>User successfully created.</summary>
        public static string UserSuccessfullyCreated {
               get {
                   return (string) resourceProvider.GetResource("User successfully created.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No user selected. Will not load data.</summary>
        public static string NoUserSelectedWillNotLoadData {
               get {
                   return (string) resourceProvider.GetResource("No user selected. Will not load data.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Invalid selected start and end dates.</summary>
        public static string InvalidSelectedStartAndEndDates {
               get {
                   return (string) resourceProvider.GetResource("Invalid selected start and end dates.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>File type not specified.</summary>
        public static string FileTypeNotSpecified {
               get {
                   return (string) resourceProvider.GetResource("File type not specified.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>File path not specified.</summary>
        public static string FilePathNotSpecified {
               get {
                   return (string) resourceProvider.GetResource("File path not specified.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Equipment List</summary>
        public static string EquipmentList {
               get {
                   return (string) resourceProvider.GetResource("Equipment List", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Shielding Gases</summary>
        public static string ShieldingGases {
               get {
                   return (string) resourceProvider.GetResource("Shielding Gases", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Invalid login attempt. Username/password/project don't match.</summary>
        public static string InvalidLoginAttemptUsernamepasswordprojectDontMatch {
               get {
                   return (string) resourceProvider.GetResource("Invalid login attempt. Username/password/project don't match.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>User Date must be specified.</summary>
        public static string UserDateMustBeSpecified {
               get {
                   return (string) resourceProvider.GetResource("User Date must be specified.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Stalk Creation</summary>
        public static string StalkCreation {
               get {
                   return (string) resourceProvider.GetResource("Stalk Creation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Welding Consumables</summary>
        public static string WeldingConsumables {
               get {
                   return (string) resourceProvider.GetResource("Welding Consumables", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Coating Consumables</summary>
        public static string CoatingConsumables {
               get {
                   return (string) resourceProvider.GetResource("Coating Consumables", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Coil Number</summary>
        public static string CoilNumber {
               get {
                   return (string) resourceProvider.GetResource("Coil Number", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Salt Contamination</summary>
        public static string SaltContamination {
               get {
                   return (string) resourceProvider.GetResource("Salt Contamination", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Enviromental Condition</summary>
        public static string EnviromentalCondition {
               get {
                   return (string) resourceProvider.GetResource("Enviromental Condition", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Logo</summary>
        public static string Logo {
               get {
                   return (string) resourceProvider.GetResource("Logo", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Welding Stations</summary>
        public static string WeldingStations {
               get {
                   return (string) resourceProvider.GetResource("Welding Stations", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Errors occured. Please fill in the marked fields correctly!</summary>
        public static string ErrorsOccuredPleaseFillInTheMarkedFieldsCorrectly {
               get {
                   return (string) resourceProvider.GetResource("Errors occured. Please fill in the marked fields correctly!", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Successfully saved.</summary>
        public static string SuccessfullySaved {
               get {
                   return (string) resourceProvider.GetResource("Successfully saved.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Project details updated with success.</summary>
        public static string ProjectDetailsUpdatedWithSuccess {
               get {
                   return (string) resourceProvider.GetResource("Project details updated with success.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Project activities updated with success.</summary>
        public static string ProjectActivitiesUpdatedWithSuccess {
               get {
                   return (string) resourceProvider.GetResource("Project activities updated with success.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Geocode was not successful for the following reason: </summary>
        public static string GeocodeWasNotSuccessfulForTheFollowingReason {
               get {
                   return (string) resourceProvider.GetResource("Geocode was not successful for the following reason: ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cut-Off Date </summary>
        public static string CutoffDate {
               get {
                   return (string) resourceProvider.GetResource("Cut-Off Date ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>inserted</summary>
        public static string Inserted {
               get {
                   return (string) resourceProvider.GetResource("inserted", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>deleted</summary>
        public static string Deleted {
               get {
                   return (string) resourceProvider.GetResource("deleted", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary> with success.</summary>
        public static string WithSuccess {
               get {
                   return (string) resourceProvider.GetResource(" with success.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>An activity must be selected in order to continue.</summary>
        public static string AnActivityMustBeSelectedInOrderToContinue {
               get {
                   return (string) resourceProvider.GetResource("An activity must be selected in order to continue.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>An sub-activity must be selected in order to load the table for it.</summary>
        public static string AnSubactivityMustBeSelectedInOrderToLoadTheTableForIt {
               get {
                   return (string) resourceProvider.GetResource("An sub-activity must be selected in order to load the table for it.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Invalid quantity inserted. The value has been reverted to the old value and will be ignored upon save if not completed correctly.</summary>
        public static string InvalidQuantityInsertedTheValueHasBeenRevertedToTheOldValueAndWillBeIgnoredUponSaveIfNotCompletedCorrectly {
               get {
                   return (string) resourceProvider.GetResource("Invalid quantity inserted. The value has been reverted to the old value and will be ignored upon save if not completed correctly.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Invalid men hours inserted. The value has been reverted to the old value and will be ignored upon save if not completed correctly.</summary>
        public static string InvalidMenHoursInsertedTheValueHasBeenRevertedToTheOldValueAndWillBeIgnoredUponSaveIfNotCompletedCorrectly {
               get {
                   return (string) resourceProvider.GetResource("Invalid men hours inserted. The value has been reverted to the old value and will be ignored upon save if not completed correctly.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>You have done no changes.</summary>
        public static string YouHaveDoneNoChanges {
               get {
                   return (string) resourceProvider.GetResource("You have done no changes.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Encountered invalid values for progress distribution. Make sure they are valid percent and in linear progress.</summary>
        public static string EncounteredInvalidValuesForProgressDistributionMakeSureTheyAreValidPercentAndInLinearProgress {
               get {
                   return (string) resourceProvider.GetResource("Encountered invalid values for progress distribution. Make sure they are valid percent and in linear progress.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No data to save.</summary>
        public static string NoDataToSave {
               get {
                   return (string) resourceProvider.GetResource("No data to save.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Will update for </summary>
        public static string WillUpdateFor {
               get {
                   return (string) resourceProvider.GetResource("Will update for ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Invalid value inserted. Must be valid percent.</summary>
        public static string InvalidValueInsertedMustBeValidPercent {
               get {
                   return (string) resourceProvider.GetResource("Invalid value inserted. Must be valid percent.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>I clicked something editable.</summary>
        public static string IClickedSomethingEditable {
               get {
                   return (string) resourceProvider.GetResource("I clicked something editable.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Updated with success.</summary>
        public static string UpdatedWithSuccess {
               get {
                   return (string) resourceProvider.GetResource("Updated with success.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>A typecode must be selected in order to load the table for it.</summary>
        public static string ATypecodeMustBeSelectedInOrderToLoadTheTableForIt {
               get {
                   return (string) resourceProvider.GetResource("A typecode must be selected in order to load the table for it.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>A datatable must be selected in order to continue.</summary>
        public static string ADatatableMustBeSelectedInOrderToContinue {
               get {
                   return (string) resourceProvider.GetResource("A datatable must be selected in order to continue.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Job Follow Up saved with success.</summary>
        public static string JobFollowUpSavedWithSuccess {
               get {
                   return (string) resourceProvider.GetResource("Job Follow Up saved with success.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Please,  select a row from the typecodes table first</summary>
        public static string PleaseSelectARowFromTheTypecodesTableFirst {
               get {
                   return (string) resourceProvider.GetResource("Please,  select a row from the typecodes table first", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Please, select some dates at least for initialisation and maintenance date</summary>
        public static string PleaseSelectSomeDatesAtLeastForInitialisationAndMaintenanceDate {
               get {
                   return (string) resourceProvider.GetResource("Please, select some dates at least for initialisation and maintenance date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>In order to save, at least one tag (from the right table) must be checked.</summary>
        public static string InOrderToSaveAtLeastOneTagFromTheRightTableMustBeChecked {
               get {
                   return (string) resourceProvider.GetResource("In order to save, at least one tag (from the right table) must be checked.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Job assignment plan saved with success.</summary>
        public static string JobAssignmentPlanSavedWithSuccess {
               get {
                   return (string) resourceProvider.GetResource("Job assignment plan saved with success.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Records successfully Deleted</summary>
        public static string RecordsSuccessfullyDeleted {
               get {
                   return (string) resourceProvider.GetResource("Records successfully Deleted", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PRESERVATION ACTIVITY LIST for </summary>
        public static string PreservationActivityListFor {
               get {
                   return (string) resourceProvider.GetResource("PRESERVATION ACTIVITY LIST for ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Click on <b>Update Profile</b> to make the change effective</summary>
        public static string ClickOnBupdateProfilebToMakeTheChangeEffective {
               get {
                   return (string) resourceProvider.GetResource("Click on <b>Update Profile</b> to make the change effective", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Data updated successfully.</summary>
        public static string DataUpdatedSuccessfully {
               get {
                   return (string) resourceProvider.GetResource("Data updated successfully.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Username required.</summary>
        public static string UsernameRequired {
               get {
                   return (string) resourceProvider.GetResource("Username required.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Password required.</summary>
        public static string PasswordRequired {
               get {
                   return (string) resourceProvider.GetResource("Password required.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Loading projects for </summary>
        public static string LoadingProjectsFor {
               get {
                   return (string) resourceProvider.GetResource("Loading projects for ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Invalid project ID.</summary>
        public static string InvalidProjectId {
               get {
                   return (string) resourceProvider.GetResource("Invalid project ID.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Low</summary>
        public static string Low {
               get {
                   return (string) resourceProvider.GetResource("Low", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Currently</summary>
        public static string Currently {
               get {
                   return (string) resourceProvider.GetResource("Currently", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Visibility</summary>
        public static string Visibility {
               get {
                   return (string) resourceProvider.GetResource("Visibility", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sunset</summary>
        public static string Sunset {
               get {
                   return (string) resourceProvider.GetResource("Sunset", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Forecast (next 5 days)</summary>
        public static string ForecastNextDays {
               get {
                   return (string) resourceProvider.GetResource("Forecast (next 5 days)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Project Site Meteo Conditions</summary>
        public static string ProjectSiteMeteoConditions {
               get {
                   return (string) resourceProvider.GetResource("Project Site Meteo Conditions", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Meteo Station not found...</summary>
        public static string MeteoStationNotFound {
               get {
                   return (string) resourceProvider.GetResource("Meteo Station not found...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>In order to see the SOW table, you must select a subcontractor</summary>
        public static string InOrderToSeeTheSowTableYouMustSelectASubcontractor {
               get {
                   return (string) resourceProvider.GetResource("In order to see the SOW table, you must select a subcontractor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The PDF shown is just for testing purposes. Currently the PDF generation mechanism is not implemented.</summary>
        public static string ThePdfShownIsJustForTestingPurposesCurrentlyThePdfGenerationMechanismIsNotImplemented {
               get {
                   return (string) resourceProvider.GetResource("The PDF shown is just for testing purposes. Currently the PDF generation mechanism is not implemented.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Please first select a system.</summary>
        public static string PleaseFirstSelectASystem {
               get {
                   return (string) resourceProvider.GetResource("Please first select a system.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Successfully updated.</summary>
        public static string SuccessfullyUpdated {
               get {
                   return (string) resourceProvider.GetResource("Successfully updated.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No changes made.</summary>
        public static string NoChangesMade {
               get {
                   return (string) resourceProvider.GetResource("No changes made.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>There is no data for this selection</summary>
        public static string ThereIsNoDataForThisSelection {
               get {
                   return (string) resourceProvider.GetResource("There is no data for this selection", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mandatory Field</summary>
        public static string MandatoryField {
               get {
                   return (string) resourceProvider.GetResource("Mandatory Field", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Record successfully Updated</summary>
        public static string RecordSuccessfullyUpdated {
               get {
                   return (string) resourceProvider.GetResource("Record successfully Updated", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>There was no row selected.You need to choose one.</summary>
        public static string ThereWasNoRowSelectedyouNeedToChooseOne {
               get {
                   return (string) resourceProvider.GetResource("There was no row selected.You need to choose one.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cannot add a row similar to this row!</summary>
        public static string CannotAddARowSimilarToThisRow {
               get {
                   return (string) resourceProvider.GetResource("Cannot add a row similar to this row!", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ADD RECORD</summary>
        public static string AddRecord {
               get {
                   return (string) resourceProvider.GetResource("ADD RECORD", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cannot update this row!</summary>
        public static string CannotUpdateThisRow {
               get {
                   return (string) resourceProvider.GetResource("Cannot update this row!", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>UPDATE RECORD</summary>
        public static string UpdateRecord {
               get {
                   return (string) resourceProvider.GetResource("UPDATE RECORD", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Delete selected record...<br><br>Are you sure?</summary>
        public static string DeleteSelectedRecordbrbrareYouSure {
               get {
                   return (string) resourceProvider.GetResource("Delete selected record...<br><br>Are you sure?", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Confirm</summary>
        public static string Confirm {
               get {
                   return (string) resourceProvider.GetResource("Confirm", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CLEARANCE<br>PERCENTAGE</summary>
        public static string Clearancebrpercentage {
               get {
                   return (string) resourceProvider.GetResource("CLEARANCE<br>PERCENTAGE", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The table cannot be null.</summary>
        public static string TheTableCannotBeNull {
               get {
                   return (string) resourceProvider.GetResource("The table cannot be null.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Precommissioning Progress Status</summary>
        public static string PrecommissioningProgressStatus {
               get {
                   return (string) resourceProvider.GetResource("Precommissioning Progress Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Commissioning Progress Status</summary>
        public static string CommissioningProgressStatus {
               get {
                   return (string) resourceProvider.GetResource("Commissioning Progress Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>You must select a system in order to continue</summary>
        public static string YouMustSelectASystemInOrderToContinue {
               get {
                   return (string) resourceProvider.GetResource("You must select a system in order to continue", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No rows checked!</summary>
        public static string NoRowsChecked {
               get {
                   return (string) resourceProvider.GetResource("No rows checked!", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Successfully deleted.</summary>
        public static string SuccessfullyDeleted {
               get {
                   return (string) resourceProvider.GetResource("Successfully deleted.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>A row tag must be selected in order to continue!</summary>
        public static string ARowTagMustBeSelectedInOrderToContinue {
               get {
                   return (string) resourceProvider.GetResource("A row tag must be selected in order to continue!", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The datatable cannot be null.</summary>
        public static string TheDatatableCannotBeNull {
               get {
                   return (string) resourceProvider.GetResource("The datatable cannot be null.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The prefix cannot be null</summary>
        public static string ThePrefixCannotBeNull {
               get {
                   return (string) resourceProvider.GetResource("The prefix cannot be null", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>First, select a subcontractor to assign to.</summary>
        public static string FirstSelectASubcontractorToAssignTo {
               get {
                   return (string) resourceProvider.GetResource("First, select a subcontractor to assign to.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Select / Deselect</summary>
        public static string SelectDeselect {
               get {
                   return (string) resourceProvider.GetResource("Select / Deselect", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No data modified.</summary>
        public static string NoDataModified {
               get {
                   return (string) resourceProvider.GetResource("No data modified.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The system cannot be null.</summary>
        public static string TheSystemCannotBeNull {
               get {
                   return (string) resourceProvider.GetResource("The system cannot be null.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>A System must be selected in order to continue!</summary>
        public static string ASystemMustBeSelectedInOrderToContinue {
               get {
                   return (string) resourceProvider.GetResource("A System must be selected in order to continue!", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Row identifier is invalid</summary>
        public static string RowIdentifierIsInvalid {
               get {
                   return (string) resourceProvider.GetResource("Row identifier is invalid", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Delete selected Testpack and its associated ISOs...<br><br>Are you sure?</summary>
        public static string DeleteSelectedTestpackAndItsAssociatedIsosbrbrareYouSure {
               get {
                   return (string) resourceProvider.GetResource("Delete selected Testpack and its associated ISOs...<br><br>Are you sure?", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cannot delete this row!</summary>
        public static string CannotDeleteThisRow {
               get {
                   return (string) resourceProvider.GetResource("Cannot delete this row!", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Record successfully Deleted</summary>
        public static string RecordSuccessfullyDeleted {
               get {
                   return (string) resourceProvider.GetResource("Record successfully Deleted", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Testpack Added. Remember to assign ISOs to it</summary>
        public static string TestpackAddedRememberToAssignIsosToIt {
               get {
                   return (string) resourceProvider.GetResource("Testpack Added. Remember to assign ISOs to it", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>You must select at least one value from Piping Design list</summary>
        public static string YouMustSelectAtLeastOneValueFromPipingDesignList {
               get {
                   return (string) resourceProvider.GetResource("You must select at least one value from Piping Design list", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>In order to save, at least one row (from the left table) must be selected.</summary>
        public static string InOrderToSaveAtLeastOneRowFromTheLeftTableMustBeSelected {
               get {
                   return (string) resourceProvider.GetResource("In order to save, at least one row (from the left table) must be selected.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>You must click on 'Select ISOs button' in order to show sketches table</summary>
        public static string YouMustClickOnSelectIsosButtonInOrderToShowSketchesTable {
               get {
                   return (string) resourceProvider.GetResource("You must click on 'Select ISOs button' in order to show sketches table", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>One or more records had not been inserted because are duplicated.</summary>
        public static string OneOrMoreRecordsHadNotBeenInsertedBecauseAreDuplicated {
               get {
                   return (string) resourceProvider.GetResource("One or more records had not been inserted because are duplicated.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Isometrics assignment was saved with success.</summary>
        public static string IsometricsAssignmentWasSavedWithSuccess {
               get {
                   return (string) resourceProvider.GetResource("Isometrics assignment was saved with success.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Item Coat?ico=fa fa-cog</summary>
        public static string ItemCoaticofaFacog {
               get {
                   return (string) resourceProvider.GetResource("Item Coat?ico=fa fa-cog", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Item Type?ico=fa fa-cog</summary>
        public static string ItemTypeicofaFacog {
               get {
                   return (string) resourceProvider.GetResource("Item Type?ico=fa fa-cog", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pipe and FittingsB;CUSEë</summary>
        public static string PipeAndFittingsbcuse {
               get {
                   return (string) resourceProvider.GetResource("Pipe and FittingsB;CUSEë", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Only admins can access other user's data.</summary>
        public static string OnlyAdminsCanAccessOtherUsersData {
               get {
                   return (string) resourceProvider.GetResource("Only admins can access other user's data.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Error. Table name not specified when initializing table.</summary>
        public static string ErrorTableNameNotSpecifiedWhenInitializingTable {
               get {
                   return (string) resourceProvider.GetResource("Error. Table name not specified when initializing table.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Trying to set invalid filters.</summary>
        public static string TryingToSetInvalidFilters {
               get {
                   return (string) resourceProvider.GetResource("Trying to set invalid filters.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No table available to execute the export on.</summary>
        public static string NoTableAvailableToExecuteTheExportOn {
               get {
                   return (string) resourceProvider.GetResource("No table available to execute the export on.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No table available to execute the export template on.</summary>
        public static string NoTableAvailableToExecuteTheExportTemplateOn {
               get {
                   return (string) resourceProvider.GetResource("No table available to execute the export template on.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No table available to execute the import on.</summary>
        public static string NoTableAvailableToExecuteTheImportOn {
               get {
                   return (string) resourceProvider.GetResource("No table available to execute the import on.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>This browser doesn't support HTML5 file uploads!</summary>
        public static string ThisBrowserDoesntSupportHtml5FileUploads {
               get {
                   return (string) resourceProvider.GetResource("This browser doesn't support HTML5 file uploads!", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Wrong file extension. Please select .xls or .xlsx files.</summary>
        public static string WrongFileExtensionPleaseSelectXlsOrXlsxFiles {
               get {
                   return (string) resourceProvider.GetResource("Wrong file extension. Please select .xls or .xlsx files.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Please select a file to import.</summary>
        public static string PleaseSelectAFileToImport {
               get {
                   return (string) resourceProvider.GetResource("Please select a file to import.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>File has been imported with succes! Reloading table in 3 seconds...</summary>
        public static string FileHasBeenImportedWithSuccesReloadingTableInSeconds {
               get {
                   return (string) resourceProvider.GetResource("File has been imported with succes! Reloading table in 3 seconds...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cannot set new column list. Not a valid array.</summary>
        public static string CannotSetNewColumnListNotAValidArray {
               get {
                   return (string) resourceProvider.GetResource("Cannot set new column list. Not a valid array.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The list of columns specified is invalid. Cannot initialize the advanced filters plugin.</summary>
        public static string TheListOfColumnsSpecifiedIsInvalidCannotInitializeTheAdvancedFiltersPlugin {
               get {
                   return (string) resourceProvider.GetResource("The list of columns specified is invalid. Cannot initialize the advanced filters plugin.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>An error has been reported by DataTables: </summary>
        public static string AnErrorHasBeenReportedByDatatables {
               get {
                   return (string) resourceProvider.GetResource("An error has been reported by DataTables: ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cannot display table. Columns must be specified to load data with AJAX (specify options.column</summary>
        public static string CannotDisplayTableColumnsMustBeSpecifiedToLoadDataWithAjaxSpecifyOptionscolumn {
               get {
                   return (string) resourceProvider.GetResource("Cannot display table. Columns must be specified to load data with AJAX (specify options.column", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Test data</summary>
        public static string TestData {
               get {
                   return (string) resourceProvider.GetResource("Test data", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No Data Found</summary>
        public static string NoDataFound {
               get {
                   return (string) resourceProvider.GetResource("No Data Found", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary> Advanced Data Filter </summary>
        public static string AdvancedDataFilter {
               get {
                   return (string) resourceProvider.GetResource(" Advanced Data Filter ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>A Datatable must be selected in order to load the table for it.</summary>
        public static string ADatatableMustBeSelectedInOrderToLoadTheTableForIt {
               get {
                   return (string) resourceProvider.GetResource("A Datatable must be selected in order to load the table for it.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>A Sub-Activity must be selected in order to continue</summary>
        public static string ASubactivityMustBeSelectedInOrderToContinue {
               get {
                   return (string) resourceProvider.GetResource("A Sub-Activity must be selected in order to continue", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The item is invalid</summary>
        public static string TheItemIsInvalid {
               get {
                   return (string) resourceProvider.GetResource("The item is invalid", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Select a project</summary>
        public static string SelectAProject {
               get {
                   return (string) resourceProvider.GetResource("Select a project", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>From the array</summary>
        public static string FromTheArray {
               get {
                   return (string) resourceProvider.GetResource("From the array", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Site Meteo</summary>
        public static string SiteMeteo {
               get {
                   return (string) resourceProvider.GetResource("Site Meteo", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Can't load<br/>weather</summary>
        public static string CantLoadbrweather {
               get {
                   return (string) resourceProvider.GetResource("Can't load<br/>weather", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Today's High: </summary>
        public static string TodaysHigh {
               get {
                   return (string) resourceProvider.GetResource("Today's High: ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Wind: </summary>
        public static string Wind {
               get {
                   return (string) resourceProvider.GetResource("Wind: ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Humidity: </summary>
        public static string Humidity {
               get {
                   return (string) resourceProvider.GetResource("Humidity: ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Charts not available to change theme.</summary>
        public static string ChartsNotAvailableToChangeTheme {
               get {
                   return (string) resourceProvider.GetResource("Charts not available to change theme.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Page under construction...</summary>
        public static string PageUnderConstruction {
               get {
                   return (string) resourceProvider.GetResource("Page under construction...", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Modal</summary>
        public static string Modal {
               get {
                   return (string) resourceProvider.GetResource("Modal", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Creating</summary>
        public static string Creating {
               get {
                   return (string) resourceProvider.GetResource("Creating", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The user you are trying to update does not exist.</summary>
        public static string TheUserYouAreTryingToUpdateDoesNotExist {
               get {
                   return (string) resourceProvider.GetResource("The user you are trying to update does not exist.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The specified ID is not valid</summary>
        public static string TheSpecifiedIdIsNotValid {
               get {
                   return (string) resourceProvider.GetResource("The specified ID is not valid", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Invalid start and end date selected for activity </summary>
        public static string InvalidStartAndEndDateSelectedForActivity {
               get {
                   return (string) resourceProvider.GetResource("Invalid start and end date selected for activity ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The activity must be placed inside the bounds of the project timeline and must have a month interval of at least 1 month.</summary>
        public static string TheActivityMustBePlacedInsideTheBoundsOfTheProjectTimelineAndMustHaveAMonthIntervalOfAtLeastMonth {
               get {
                   return (string) resourceProvider.GetResource("The activity must be placed inside the bounds of the project timeline and must have a month interval of at least 1 month.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>The number of progress distribution values does not match the selected time interval (number of months) for the activity </summary>
        public static string TheNumberOfProgressDistributionValuesDoesNotMatchTheSelectedTimeIntervalNumberOfMonthsForTheActivity {
               get {
                   return (string) resourceProvider.GetResource("The number of progress distribution values does not match the selected time interval (number of months) for the activity ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Encountered invalid values for progress distribution for activity {0} at ({1}-{2}). Make sure they are valid percent and in linear progress.</summary>
        public static string EncounteredInvalidValuesForProgressDistributionForActivityAtMakeSureTheyAreValidPercentAndInLinearProgress {
               get {
                   return (string) resourceProvider.GetResource("Encountered invalid values for progress distribution for activity {0} at ({1}-{2}). Make sure they are valid percent and in linear progress.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No datatable was specified in the dynamic table operations [Count].</summary>
        public static string NoDatatableWasSpecifiedInTheDynamicTableOperationsCount {
               get {
                   return (string) resourceProvider.GetResource("No datatable was specified in the dynamic table operations [Count].", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cannot retrieve the Percent for the Actual Trends Charts.</summary>
        public static string CannotRetrieveThePercentForTheActualTrendsCharts {
               get {
                   return (string) resourceProvider.GetResource("Cannot retrieve the Percent for the Actual Trends Charts.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cannot retrieve the Monthly Percent for the Actual Trends Charts.</summary>
        public static string CannotRetrieveTheMonthlyPercentForTheActualTrendsCharts {
               get {
                   return (string) resourceProvider.GetResource("Cannot retrieve the Monthly Percent for the Actual Trends Charts.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cannot retrieve current week number for the Cumulated Trends Charts.</summary>
        public static string CannotRetrieveCurrentWeekNumberForTheCumulatedTrendsCharts {
               get {
                   return (string) resourceProvider.GetResource("Cannot retrieve current week number for the Cumulated Trends Charts.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Encountered week count = 0 in planning cutoff. Cannot compute range to compute the percent for planned trends data.</summary>
        public static string EncounteredWeekCountInPlanningCutoffCannotComputeRangeToComputeThePercentForPlannedTrendsData {
               get {
                   return (string) resourceProvider.GetResource("Encountered week count = 0 in planning cutoff. Cannot compute range to compute the percent for planned trends data.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Invalid start-end dates. End date must be bigget than start date.</summary>
        public static string InvalidStartendDatesEndDateMustBeBiggetThanStartDate {
               get {
                   return (string) resourceProvider.GetResource("Invalid start-end dates. End date must be bigget than start date.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Trying to change start date failed. Trying to drop month for which planning data exists (month {0}, year {1}).</summary>
        public static string TryingToChangeStartDateFailedTryingToDropMonthForWhichPlanningDataExistsMonthYear {
               get {
                   return (string) resourceProvider.GetResource("Trying to change start date failed. Trying to drop month for which planning data exists (month {0}, year {1}).", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Trying to change end date failed. Trying to drop month for which planning data exists (month {0}, year {1}).</summary>
        public static string TryingToChangeEndDateFailedTryingToDropMonthForWhichPlanningDataExistsMonthYear {
               get {
                   return (string) resourceProvider.GetResource("Trying to change end date failed. Trying to drop month for which planning data exists (month {0}, year {1}).", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Another entity with the given identifier already exists.</summary>
        public static string AnotherEntityWithTheGivenIdentifierAlreadyExists {
               get {
                   return (string) resourceProvider.GetResource("Another entity with the given identifier already exists.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>There is no entity to update with the given ID (" </summary>
        public static string ThereIsNoEntityToUpdateWithTheGivenId {
               get {
                   return (string) resourceProvider.GetResource("There is no entity to update with the given ID (\" ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Column already exists.</summary>
        public static string ColumnAlreadyExists {
               get {
                   return (string) resourceProvider.GetResource("Column already exists.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Command text cannot be null or empty.</summary>
        public static string CommandTextCannotBeNullOrEmpty {
               get {
                   return (string) resourceProvider.GetResource("Command text cannot be null or empty.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Error happened when getting value. {0}</summary>
        public static string ErrorHappenedWhenGettingValue {
               get {
                   return (string) resourceProvider.GetResource("Error happened when getting value. {0}", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Column list must be provided.</summary>
        public static string ColumnListMustBeProvided {
               get {
                   return (string) resourceProvider.GetResource("Column list must be provided.", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Columns already dropped</summary>
        public static string ColumnsAlreadyDropped {
               get {
                   return (string) resourceProvider.GetResource("Columns already dropped", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>View PDF</summary>
        public static string ViewPdf {
               get {
                   return (string) resourceProvider.GetResource("View PDF", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Week  </summary>
        public static string Week {
               get {
                   return (string) resourceProvider.GetResource("Week  ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>No Date...<br/>Tag Not Restored</summary>
        public static string NoDatebrtagNotRestored {
               get {
                   return (string) resourceProvider.GetResource("No Date...<br/>Tag Not Restored", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>latitude: </summary>
        public static string Latitude {
               get {
                   return (string) resourceProvider.GetResource("latitude: ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>longitude: </summary>
        public static string Longitude {
               get {
                   return (string) resourceProvider.GetResource("longitude: ", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Baseline</summary>
        public static string Baseline {
               get {
                   return (string) resourceProvider.GetResource("Baseline", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>N° of Records</summary>
        public static string NOfRecords {
               get {
                   return (string) resourceProvider.GetResource("N° of Records", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>With a Valid <br> IFC Quantities</summary>
        public static string WithAValidBrIfcQuantities {
               get {
                   return (string) resourceProvider.GetResource("With a Valid <br> IFC Quantities", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Linked to Certification</summary>
        public static string LinkedToCertification {
               get {
                   return (string) resourceProvider.GetResource("Linked to Certification", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Linked to <br> System</summary>
        public static string LinkedToBrSystem {
               get {
                   return (string) resourceProvider.GetResource("Linked to <br> System", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Unauthorized</summary>
        public static string Unauthorized {
               get {
                   return (string) resourceProvider.GetResource("Unauthorized", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Successfully inserted</summary>
        public static string SuccessfullyInserted {
               get {
                   return (string) resourceProvider.GetResource("Successfully inserted", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Construction Work Package - Predecessor(s) / Successor(s)</summary>
        public static string ConstructionWorkPackagePredecessorsSuccessors {
               get {
                   return (string) resourceProvider.GetResource("Construction Work Package - Predecessor(s) / Successor(s)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Receipt Register</summary>
        public static string ReceiptRegister {
               get {
                   return (string) resourceProvider.GetResource("Receipt Register", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Transfer Register</summary>
        public static string TransferRegister {
               get {
                   return (string) resourceProvider.GetResource("Transfer Register", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sunrise</summary>
        public static string Sunrise {
               get {
                   return (string) resourceProvider.GetResource("Sunrise", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Chill</summary>
        public static string Chill {
               get {
                   return (string) resourceProvider.GetResource("Chill", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Construction Tools</summary>
        public static string ConstructionTools {
               get {
                   return (string) resourceProvider.GetResource("Construction Tools", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Matrix Configuration</summary>
        public static string MatrixConfiguration {
               get {
                   return (string) resourceProvider.GetResource("Matrix Configuration", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Construction Equipment by Main Activities (Construction Phase)</summary>
        public static string ConstructionEquipmentByMainActivitiesConstructionPhase {
               get {
                   return (string) resourceProvider.GetResource("Construction Equipment by Main Activities (Construction Phase)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Construction Tools by Main Activity</summary>
        public static string ConstructionToolsByMainActivity {
               get {
                   return (string) resourceProvider.GetResource("Construction Tools by Main Activity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Indirect Roles and Responsibilities</summary>
        public static string IndirectRolesAndResponsibilities {
               get {
                   return (string) resourceProvider.GetResource("Indirect Roles and Responsibilities", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Worktrades / Equipment / Tools</summary>
        public static string WorktradesEquipmentTools {
               get {
                   return (string) resourceProvider.GetResource("Worktrades / Equipment / Tools", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Welders Qualification</summary>
        public static string WeldersQualification {
               get {
                   return (string) resourceProvider.GetResource("Welders Qualification", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Logical Definition</summary>
        public static string LogicalDefinition {
               get {
                   return (string) resourceProvider.GetResource("Logical Definition", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CWPs Follow-Up</summary>
        public static string CwpsFollowup {
               get {
                   return (string) resourceProvider.GetResource("CWPs Follow-Up", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CWPs Status</summary>
        public static string CwpsStatus {
               get {
                   return (string) resourceProvider.GetResource("CWPs Status", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Trades / Equipment / Tools</summary>
        public static string TradesEquipmentTools {
               get {
                   return (string) resourceProvider.GetResource("Trades / Equipment / Tools", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CONSTRUCTION EQUIPMENT</summary>
        public static string ConstructionEquipment {
               get {
                   return (string) resourceProvider.GetResource("CONSTRUCTION EQUIPMENT", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Minor Discrepancy</summary>
        public static string MinorDiscrepancy {
               get {
                   return (string) resourceProvider.GetResource("Minor Discrepancy", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Major Discrepancy (warning)</summary>
        public static string MajorDiscrepancyWarning {
               get {
                   return (string) resourceProvider.GetResource("Major Discrepancy (warning)", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Reference Quantities Updated</summary>
        public static string ReferenceQuantitiesUpdated {
               get {
                   return (string) resourceProvider.GetResource("Reference Quantities Updated", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>TAGS NOT ASSIGNED TO</summary>
        public static string TagsNotAssignedTo {
               get {
                   return (string) resourceProvider.GetResource("TAGS NOT ASSIGNED TO", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Import from Excel</summary>
        public static string ImportFromExcel {
               get {
                   return (string) resourceProvider.GetResource("Import from Excel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CWP Breakdown by Field Installation Worl Packages</summary>
        public static string CwpBreakdownByFieldInstallationWorlPackages {
               get {
                   return (string) resourceProvider.GetResource("CWP Breakdown by Field Installation Worl Packages", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>UPLOAD DOSSIER</summary>
        public static string UploadDossier {
               get {
                   return (string) resourceProvider.GetResource("UPLOAD DOSSIER", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>ALL MAIN ACTIVITIES</summary>
        public static string AllMainActivities {
               get {
                   return (string) resourceProvider.GetResource("ALL MAIN ACTIVITIES", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CWP Breakdown by FIWPs</summary>
        public static string CwpBreakdownByFiwps {
               get {
                   return (string) resourceProvider.GetResource("CWP Breakdown by FIWPs", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Written By</summary>
        public static string WrittenBy {
               get {
                   return (string) resourceProvider.GetResource("Written By", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Document Revisions</summary>
        public static string DocumentRevisions {
               get {
                   return (string) resourceProvider.GetResource("Document Revisions", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Datatables</summary>
        public static string Datatables {
               get {
                   return (string) resourceProvider.GetResource("Datatables", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>For</summary>
        public static string For {
               get {
                   return (string) resourceProvider.GetResource("For", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Name</summary>
        public static string Name {
               get {
                   return (string) resourceProvider.GetResource("Name", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Signature</summary>
        public static string Signature {
               get {
                   return (string) resourceProvider.GetResource("Signature", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Owner</summary>
        public static string Owner {
               get {
                   return (string) resourceProvider.GetResource("Owner", CultureInfo.CurrentUICulture.Name);
               }
            }

        }        
}
