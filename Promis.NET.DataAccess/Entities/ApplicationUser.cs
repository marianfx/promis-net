﻿using Promis.NET.DataAccess.Identity.MySql;

namespace Promis.NET.DataAccess.Entities
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// Holds the theme of the user
        /// </summary>
        public string UserTheme { get; set; }
    }
}
