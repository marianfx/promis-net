namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("budget")]
    public partial class pw_budget
    {
        [Key]
        [StringLength(3)]
        public string act { get; set; }

        public int budget { get; set; }
    }
}
