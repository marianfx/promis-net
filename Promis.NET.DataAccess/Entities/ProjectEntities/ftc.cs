﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.DynamicData;

namespace Promis.NET.DataAccess.Entities.ProjectEntities
{
    [Table("ftc")]
    public class ftc_container
    {
        [Key]
        [Column(Order = 1)]
        public string code { get; set; }
        public string description { get; set; }
        [Key]
        [Column(Order = 2)]
        public string kpi { get; set; }
        public string kpi_description { get; set; }
        public string kpi_type { get; set; }
        public string kpi_source { get; set; }
        public string kpi_filter { get; set; }
        public string kpi_um { get; set; }
        public string warnA { get; set; }
        public double A_pr { get; set; }
        public string crosscheck { get; set; }
        public string warnB { get; set; }
        public double B_pr { get; set; }
        public double tot { get; set; }
        public double ftc { get; set; }
        public string fav { get; set; }
        public int closed { get; set; }
    }
}
