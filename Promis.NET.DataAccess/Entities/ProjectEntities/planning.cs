namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("planning")]
    public partial class planning
    {
        [StringLength(10)]
        public string act { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        [StringLength(10)]
        public string mhrs { get; set; }

        [Column(TypeName = "char")]
        [StringLength(1)]
        public string type { get; set; }

        [StringLength(10)]
        public string start_date { get; set; }

        [StringLength(10)]
        public string end_date { get; set; }

        public long id { get; set; }
    }
}
