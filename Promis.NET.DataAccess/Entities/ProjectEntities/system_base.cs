﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    [Table("systems")]
    public partial class system_base
    {
        [StringLength(50)]

        public string SYSTEM { get; set; }

        [StringLength(255)]
        public string SYSTEM_DESCRIPTION { get; set; }

    }
}
