﻿namespace Promis.NET.DataAccess.Entities.ProjectEntities
{
    public class preservation
    {
        public string jp { get; set; }
        public string activity { get; set; }
        public string description { get; set; }
        public int seq { get; set; }
        public string frequency { get; set; }

    }
}
