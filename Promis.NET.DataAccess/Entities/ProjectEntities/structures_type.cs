namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("structures_type")]
    public partial class structures_type
    {
        [StringLength(10)]
        public string code { get; set; }

        [StringLength(50)]
        public string description { get; set; }

        public long id { get; set; }
    }
}
