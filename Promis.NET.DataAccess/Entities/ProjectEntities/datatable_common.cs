namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    public partial class datatable_common
    {
        [Required]
        [StringLength(50)]
        public string TAG { get; set; }

        [Required]
        [StringLength(255)]
        public string DESCRIPTION { get; set; }

        [Required]
        [StringLength(25)]
        public string WBS { get; set; }

        [Required]
        [StringLength(50)]
        public string DWG { get; set; }

        [Required]
        [StringLength(50)]
        public string SYSTEM { get; set; }
    }
}
