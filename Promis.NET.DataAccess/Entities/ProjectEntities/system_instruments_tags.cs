﻿
namespace Promis.NET.DataAccess.Entities.ProjectEntities
{
    public class system_instruments_tags
    {
        public string comp { get; set; }
        public string description { get; set; }
        public string act { get; set; }
        public string type { get; set; }
        public string system { get; set; }
        public string laststep { get; set; }
        public string nextstep { get; set; }
        public double progress { get; set; }
    }
}
