namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("act_actions")]
    public partial class act_actions
    {
        [Required]
        [StringLength(5)]
        public string code { get; set; }

        [Required]
        [StringLength(120)]
        public string description { get; set; }

        public long id { get; set; }
    }
}
