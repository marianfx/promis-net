﻿namespace Promis.NET.DataAccess.Entities.ProjectEntities
{
    public class work_def_sub_act
    {
        public string inspection { get; set; }
        public string code { get; set; }
        public string CColumn { get; set; }
        public string c_user { get; set; }
        public string filename { get; set; }
        public string ord { get; set; }
        public string result { get; set; }
        public string SColumn { get; set; }
        public string s_user { get; set; }
        public string OColumn { get; set; }
        public string o_user { get; set; }
        public string id { get; set; }
        public string pos { get; set; }
    }
}
