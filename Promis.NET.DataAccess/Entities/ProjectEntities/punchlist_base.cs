namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("punchlist")]
    public partial class punchlist_base
    {

        [StringLength(4)]
        public string flag { get; set; }

        [StringLength(16777215)]
        public string wd { get; set; }

        public int? pos { get; set; }
    }
}
