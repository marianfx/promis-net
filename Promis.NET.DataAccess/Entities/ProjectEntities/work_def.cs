namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("work_def")]
    public partial class work_def: work_def_base
    {
        [StringLength(15)]
        public string SUB_ACT { get; set; }

        [StringLength(150)]
        public string SUB_ACT_D { get; set; }

        [StringLength(10)]
        public string UM { get; set; }

        [StringLength(3)]
        public string STEP { get; set; }

        [StringLength(150)]
        public string INSPECTION { get; set; }

        [StringLength(30)]
        public string FORM { get; set; }

        [StringLength(5)]
        public string S { get; set; }

        [StringLength(5)]
        public string C { get; set; }

        [StringLength(5)]
        public string O { get; set; }

        [StringLength(5)]
        public string T { get; set; }

        [Column(TypeName = "char")]
        [StringLength(3)]
        public string POS { get; set; }

        [StringLength(6)]
        public string PROGR { get; set; }

        [Column(TypeName = "char")]
        [StringLength(3)]
        public string LP { get; set; }

        public int? ORD { get; set; }

        public long id { get; set; }
    }
}
