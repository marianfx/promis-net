namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("work_disc")]
    public partial class work_disc
    {
        [Column(TypeName = "char")]
        [StringLength(3)]
        public string cod { get; set; }

        [StringLength(100)]
        public string descr { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ord { get; set; }

        [Column(TypeName = "char")]
        [StringLength(1)]
        public string flag { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id { get; set; }
    }
}
