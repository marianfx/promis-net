namespace Promis.Web.DataAccess.Entities.ProjectEntities
{

    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("systems")]
    public partial class system_s
    {
        [Column("SYSTEM")]
        [Required]
        [StringLength(50)]
        public string SYSTEM { get; set; }

        [Required]
        [StringLength(255)]
        public string SYSTEM_DESCRIPTION { get; set; }

        [Required]
        [StringLength(50)]
        public string SUBSYSTEM { get; set; }

        [Required]
        [StringLength(255)]
        public string SUBSYSTEM_DESCRIPTION { get; set; }

        [Required]
        [StringLength(25)]
        public string SUP { get; set; }

        public long id { get; set; }
    }
}
