namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("pl_works")]
    public partial class pl_works
    {
        [StringLength(5)]
        public string type { get; set; }

        [StringLength(255)]
        public string descr { get; set; }

        [StringLength(3)]
        public string phase { get; set; }

        public int? ord { get; set; }

        public long id { get; set; }
    }
}
