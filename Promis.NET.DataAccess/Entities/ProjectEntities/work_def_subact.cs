namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("work_def")]
    public partial class work_def_subact: work_def_base
    {
        [StringLength(15)]
        public string SUB_ACT { get; set; }

        [StringLength(150)]
        public string SUB_ACT_D { get; set; }
    }
}
