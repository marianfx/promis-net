﻿
namespace Promis.NET.DataAccess.Entities.ProjectEntities
{
    public class main_activity_budget:work_disc_partial
    {
        public double? total_mhrs{ get; set; }
        public double? budget { get; set; }
    }
}
