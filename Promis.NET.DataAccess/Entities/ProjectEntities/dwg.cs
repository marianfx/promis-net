namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dwg")]
    public partial class dwg
    {
        [Required]
        [StringLength(50)]
        public string CODE { get; set; }

        [Required]
        [StringLength(255)]
        public string DESCRIPTION { get; set; }

        public long id { get; set; }
    }
}
