﻿
namespace Promis.NET.DataAccess.Entities.ProjectEntities
{
    public class testpack_assignment
    {
        public double sum_qty { get; set; }
        public string system { get; set; }
        public string subsystem { get; set; }
        public string wbs { get; set; }
    }
}
