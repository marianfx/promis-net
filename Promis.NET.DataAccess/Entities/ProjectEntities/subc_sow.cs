namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("subc_sow")]
    public partial class subc_sow
    {
        [StringLength(10)]
        public string code { get; set; }

        [StringLength(20)]
        public string sub_act { get; set; }

        public int id { get; set; }
    }
}
