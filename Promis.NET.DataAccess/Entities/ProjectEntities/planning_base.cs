﻿namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("planning")]
    public partial class planning_base
    {
        [StringLength(10)]
        public string act { get; set; }

        [StringLength(255)]
        public string description { get; set; }
    }
}

