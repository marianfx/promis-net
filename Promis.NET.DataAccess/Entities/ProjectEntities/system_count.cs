﻿using System.ComponentModel.DataAnnotations;
namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    public partial class system_count
    {
        [StringLength(50)]
        public string SYSTEM { get; set; }

        [StringLength(255)]
        public string SYSTEM_DESCRIPTION { get; set; }

        public double COUNT { get; set; }
    }
}
