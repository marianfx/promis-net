namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("progress")]
    public partial class progress
    {
        [StringLength(10)]
        public string m { get; set; }

        [StringLength(8)]
        public string w { get; set; }

        [StringLength(10)]
        public string act { get; set; }

        [StringLength(10)]
        public string earned { get; set; }

        [StringLength(10)]
        public string mhrs { get; set; }

        public float? pr { get; set; }

        public long id { get; set; }
    }
}
