﻿using System.ComponentModel.DataAnnotations;

namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    public partial class pl_orig_base
    {
        [StringLength(10)]
        public string initials { get; set; }

        [StringLength(255)]
        public string name { get; set; }
    }
}
