namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobstatus")]
    public partial class jobstatu
    {
        [StringLength(10)]
        public string code { get; set; }

        [StringLength(10)]
        public string flag { get; set; }

        [StringLength(150)]
        public string descr { get; set; }

        public long id { get; set; }
    }
}
