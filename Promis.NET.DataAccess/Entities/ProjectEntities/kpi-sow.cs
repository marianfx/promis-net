﻿
namespace Promis.NET.DataAccess.Entities.ProjectEntities
{
    public class kpi_sow
    {
        public string sub_act { get; set; }
        public string sub_activity_description  { get; set; }
        public string budget_qty { get; set; }
        public string um { get; set; }
    }
}
