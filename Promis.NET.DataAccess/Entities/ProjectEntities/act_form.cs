namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("act_form")]
    public partial class act_form
    {
        [Required]
        [StringLength(50)]
        public string CODE { get; set; }

        [Required]
        [StringLength(255)]
        public string DESCRIPTION { get; set; }

        public long id { get; set; }
    }
}
