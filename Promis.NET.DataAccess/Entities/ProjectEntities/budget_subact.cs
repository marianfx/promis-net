namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("budget_subact")]
    public partial class budget_subact
    {
        [StringLength(15)]
        public string sub_act { get; set; }

        public float? qty { get; set; }

        [StringLength(10)]
        public string um { get; set; }

        [StringLength(10)]
        public string prod { get; set; }

        [StringLength(15)]
        public string mhrs { get; set; }

        public long id { get; set; }
    }
}
