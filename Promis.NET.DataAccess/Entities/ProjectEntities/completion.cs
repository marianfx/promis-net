namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("completion")]
    public partial class completion
    {
        [Required]
        [StringLength(20)]
        public string SYSTEM { get; set; }
        
        [StringLength(10)]
        public string MC_SENT { get; set; }
        
        [StringLength(10)]
        public string MC_SIGN { get; set; }
        
        [StringLength(30)]
        public string MC_DOC { get; set; }
        
        [StringLength(10)]
        public string PR_SENT { get; set; }
        
        [StringLength(10)]
        public string PR_SIGN { get; set; }
        
        [StringLength(30)]
        public string PR_DOC { get; set; }
        
        [StringLength(10)]
        public string CO_SENT { get; set; }
        
        [StringLength(10)]
        public string CO_SIGN { get; set; }
        
        [StringLength(30)]
        public string CO_DOC { get; set; }
        
        [StringLength(10)]
        public string SU_SENT { get; set; }
        
        [StringLength(10)]
        public string SU_SIGN { get; set; }
        
        [StringLength(30)]
        public string SU_DOC { get; set; }
        
        [StringLength(10)]
        public string TR_SENT { get; set; }
        
        [StringLength(10)]
        public string TR_SIGN { get; set; }
        
        [StringLength(30)]
        public string TR_DOC { get; set; }
        
        [StringLength(10)]
        public string GA_SENT { get; set; }
        
        [StringLength(10)]
        public string GA_SIGN { get; set; }
        
        [StringLength(30)]
        public string GA_DOC { get; set; }
        
        [StringLength(10)]
        public string PAC_SENT { get; set; }
        
        [StringLength(10)]
        public string PAC_SIGN { get; set; }
        
        [StringLength(30)]
        public string PAC_DOC { get; set; }

        public long id { get; set; }
    }
}
