﻿using Promis.Web.DataAccess.Entities.ProjectEntities;
using System.ComponentModel.DataAnnotations;

namespace Promis.NET.DataAccess.Entities.ProjectEntities
{
    public class work_def_budget: work_def_base
    {
        [StringLength(15)]
        public string sub_act { get; set; }

        [StringLength(150)]
        public string sub_act_d { get; set; }

        [StringLength(10)]
        public string um { get; set; }
        
        public double qty { get; set; }

        public int mhrs { get; set; }

        public int sub_mhrs { get; set; }
    }
}
