namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("instrumentation_type")]
    public partial class instrumentation_type
    {
        [StringLength(15)]
        public string code { get; set; }

        [StringLength(150)]
        public string description { get; set; }

        public long id { get; set; }
    }
}
