﻿
namespace Promis.NET.DataAccess.Entities.ProjectEntities
{
    public class preservation_actions
    {
        public string donew { get; set; }
        public string activity { get; set; }
        public string seq { get; set; }
        public string nr { get; set; }
        public string schedw { get; set; }
        public string description { get; set; }
        public string code { get; set; }
    }
}
