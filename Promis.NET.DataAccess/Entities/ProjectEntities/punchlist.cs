namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("punchlist")]
    public partial class punchlist
    {
        [StringLength(30)]
        public string system { get; set; }
        
        [StringLength(30)]
        public string subsystem { get; set; }

        [Required]
        [StringLength(30)]
        public string pl_disci { get; set; }

        [StringLength(255)]
        public string item { get; set; }

        [StringLength(16777215)]
        public string wd { get; set; }

        [Column(TypeName = "char")]
        [StringLength(3)]
        public string orig { get; set; }

        [StringLength(60)]
        public string js { get; set; }

        [StringLength(4)]
        public string flag { get; set; }

        [StringLength(4)]
        public string actionby { get; set; }

        public int? pos { get; set; }

        [StringLength(20)]
        public string target_date { get; set; }

        [StringLength(20)]
        public string found_date { get; set; }

        [StringLength(150)]
        public string reference { get; set; }

        //[StringLength(50)]
        //public string inputdoc { get; set; }

        [StringLength(50)]
        public string closure { get; set; }

        [StringLength(20)]
        public string closure_date { get; set; }

        [StringLength(10)]
        public string closer { get; set; }

        public long id { get; set; }
    }
}
