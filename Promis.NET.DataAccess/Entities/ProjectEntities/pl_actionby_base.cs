﻿using System.ComponentModel.DataAnnotations;

namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    public partial class pl_actionby_base
    {
        [StringLength(10)]
        public string code { get; set; }

        [StringLength(255)]
        public string company { get; set; }
    }
}
