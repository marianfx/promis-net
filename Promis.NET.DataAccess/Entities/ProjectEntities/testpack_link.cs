﻿
namespace Promis.NET.DataAccess.Entities
{
    public class testpack_link
    {      
        public string tag { get; set; }
        public string comp { get; set; }
        public string qty { get; set; }
        public string system { get; set; }
        public int subsystem { get; set; }
    }
}
