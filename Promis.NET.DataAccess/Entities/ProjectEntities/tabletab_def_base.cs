﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Promis.NET.DataAccess.Entities.ProjectEntities
{
    [Table("tabletab_def")]
    public partial class tabletab_def_base
    {
        [StringLength(30)]
        public string datatable { get; set; }

        [StringLength(100)]
        public string description { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != this.GetType())
                return false;
            var thisObj = (tabletab_def_base)obj;
            return thisObj.datatable?.ToLower() == this.datatable?.ToLower();
        }

        public override int GetHashCode()
        {
            return datatable.GetHashCode();
        }
    }
}
