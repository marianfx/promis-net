namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using Promis.NET.DataAccess.Entities;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("area")]
    public partial class area: descriptor
    {
        [Required]
        [StringLength(50)]
        public string CODE { get; set; }

        [Required]
        [StringLength(255)]
        public string DESCRIPTION { get; set; }

        public long id { get; set; }
    }
}
