namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    public partial class testpack_essentials
    {
        public int step { get; set; }
        
        [StringLength(150)]
        public string inspection { get; set; }
        
        public int number { get; set; }
        
        public bool exists { get; set; }
    }
}
