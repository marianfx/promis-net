using Promis.NET.DataAccess.Entities.PromisWeb;

namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    public class work_planning
    {
        public string act { get; set; }
        
        public string act_d { get; set; }

        public string start_date { get; set; }

        public string end_date { get; set; }

        public string type { get; set; }

        public int? mhrs { get; set; }

        public work_planning() { }

        public work_planning(work_disc_budget activity)
        {
            if (activity == null)
                return;

            act = activity.cod;
            act_d = activity.descr;
            mhrs = activity.budget;
        }
    }
}
