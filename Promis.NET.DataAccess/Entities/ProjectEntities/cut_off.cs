namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cut_off")]
    public partial class cut_offf
    {
        [StringLength(10)]
        public string w { get; set; }

        [StringLength(10)]
        public string m { get; set; }
        
        [StringLength(10)]
        public string cut_off { get; set; }

        public long id { get; set; }
    }
}
