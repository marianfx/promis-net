﻿namespace Promis.NET.DataAccess.Entities.ProjectEntities
{
    public class cert_instruments_workdef
    {
        public string step { get; set; }
        public string sub_act { get; set; }
        public string inspection { get; set; }
    }
}
