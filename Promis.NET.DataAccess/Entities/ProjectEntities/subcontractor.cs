namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("subcontractors")]
    public class subcontractor
    {
        [Required]
        [StringLength(10)]
        public string code { get; set; }

        [Column("subcontractor")]
        [Required]
        [StringLength(50)]
        public string subcontractorCol { get; set; }

        public long id { get; set; }
    }
}
