namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("sketches")]
    public partial class sketch
    {
        [Required]
        [StringLength(50)]
        public string TAG { get; set; }

        [Required]
        [StringLength(255)]
        public string DESCRIPTION { get; set; }

        [Required]
        [StringLength(25)]
        public string WBS { get; set; }

        [Required]
        [StringLength(15)]
        public string TYPE { get; set; }

        [Required]
        [StringLength(50)]
        public string DWG { get; set; }

        [Required]
        [StringLength(10)]
        public string UM { get; set; }

        [Required]
        [StringLength(10)]
        public string QTY { get; set; }

        [Required]
        [StringLength(50)]
        public string MAINTAG { get; set; }

        [Required]
        [StringLength(50)]
        public string SYSTEM { get; set; }

        [Required]
        [StringLength(50)]
        public string SUBSYSTEM { get; set; }

        [Required]
        [StringLength(12)]
        public string ACTIVITY { get; set; }

        [Required]
        [StringLength(25)]
        public string PDS { get; set; }

        [Required]
        [StringLength(50)]
        public string LINE { get; set; }

        [Required]
        [StringLength(10)]
        public string SHEET { get; set; }

        [Required]
        [StringLength(20)]
        public string PIPING_CLASS { get; set; }

        [Required]
        [StringLength(20)]
        public string MATERIAL_CODE { get; set; }

        [Required]
        [StringLength(50)]
        public string MATERIAL { get; set; }

        [Required]
        [StringLength(6)]
        public string SIZE { get; set; }

        [Required]
        [StringLength(3)]
        public string DESTINATION { get; set; }

        [Required]
        [StringLength(10)]
        public string PIPE_LENGTH { get; set; }

        [Required]
        [StringLength(10)]
        public string INSULATION_MATERIAL { get; set; }

        [Required]
        [StringLength(10)]
        public string INSULATION_SURFACE { get; set; }

        [Required]
        [StringLength(10)]
        public string PAINT_SYSTEM { get; set; }

        [Required]
        [StringLength(10)]
        public string PAINT_SURFACE { get; set; }

        [Required]
        [StringLength(15)]
        public string FINISH_CODE { get; set; }

        [Required]
        [StringLength(3)]
        public string STATUS { get; set; }

        [Required]
        [StringLength(10)]
        public string REVISION { get; set; }

        [Required]
        [StringLength(10)]
        public string FLUID_CODE { get; set; }

        [Required]
        [StringLength(50)]
        public string FLUID_DESCRIPTION { get; set; }

        [Required]
        [StringLength(30)]
        public string FROM_LOCATION { get; set; }

        [Required]
        [StringLength(30)]
        public string TO_LOCATION { get; set; }

        [Required]
        [StringLength(50)]
        public string LINE_NUM { get; set; }

        public long id { get; set; }
    }
}
