﻿
namespace Promis.NET.DataAccess.Entities.ProjectEntities
{
    public class certification_tags_info
    {
        public string reference { get; set; } 
        public string tag { get; set; }  
        public string descr { get; set; } 
        public string type { get; set; }  
        public string um { get; set; }  
        public string qty { get; set; } 
       // public string id { get; set; }  
    }
}
