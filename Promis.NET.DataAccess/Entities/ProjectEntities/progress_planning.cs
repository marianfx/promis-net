namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    public partial class progress_planning
    {

        public string tag { get; set; }
        public string s1 { get; set; }
        public string s2 { get; set; }
        public string s3 { get; set; }
        public string s4 { get; set; }
        public string s5 { get; set; }
        public string s6 { get; set; }
        public string s7 { get; set; }
        public string s8 { get; set; }
        public string s9 { get; set; }
        public string s10 { get; set; }
        public string s11 { get; set; }
        public string s12 { get; set; }

        public string wbs { get; set; }
        public float? pr { get; set; }
        public string mhrs { get; set; }
        public string subact { get; set; }
        public string qty { get; set; }
        public string datatable { get; set; }
        public double? equiv { get; set; }

    }
}
