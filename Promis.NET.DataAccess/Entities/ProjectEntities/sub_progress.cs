namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System.ComponentModel.DataAnnotations;
    
    public partial class sub_progress
    {
        [StringLength(100)]
        public string tag { get; set; }

        [StringLength(20)]
        public string wbs { get; set; }

        [StringLength(6)]
        public string s1 { get; set; }

        [StringLength(6)]
        public string s2 { get; set; }

        [StringLength(6)]
        public string s3 { get; set; }

        [StringLength(6)]
        public string s4 { get; set; }

        [StringLength(6)]
        public string s5 { get; set; }

        [StringLength(6)]
        public string s6 { get; set; }

        [StringLength(6)]
        public string s7 { get; set; }

        [StringLength(6)]
        public string s8 { get; set; }

        [StringLength(6)]
        public string s9 { get; set; }

        [StringLength(6)]
        public string s10 { get; set; }

        [StringLength(6)]
        public string s11 { get; set; }

        [StringLength(6)]
        public string s12 { get; set; }
        
        public float? pr { get; set; }

        [StringLength(15)]
        public string sub_act { get; set; }

        [StringLength(10)]
        public string mhrs { get; set; }

        [StringLength(10)]
        public string qty { get; set; }

        [StringLength(30)]
        public string datatable { get; set; }

        public long id { get; set; }
    }
}
