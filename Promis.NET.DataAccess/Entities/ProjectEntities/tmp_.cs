namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tmp_")]
    public partial class tmp_
    {
        [StringLength(50)]
        public string tag { get; set; }

        [StringLength(150)]
        public string descr { get; set; }

        [StringLength(255)]
        public string parent { get; set; }

        [StringLength(10)]
        public string qty { get; set; }

        public int id { get; set; }
    }
}
