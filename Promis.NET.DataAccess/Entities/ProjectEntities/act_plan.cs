namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("act_plan")]
    public partial class act_plan
    {
        [Required]
        [StringLength(50)]
        public string CODE { get; set; }

        [Required]
        [StringLength(255)]
        public string DESCRIPTION { get; set; }

        public long id { get; set; }
    }
}
