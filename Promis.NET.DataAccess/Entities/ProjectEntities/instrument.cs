namespace Promis.Web.DataAccess.Entities.ProjectEntities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("instruments")]
    public partial class instrument
    {
        [Required]
        [StringLength(50)]
        public string TAG { get; set; }

        [Required]
        [StringLength(255)]
        public string DESCRIPTION { get; set; }

        [Required]
        [StringLength(25)]
        public string WBS { get; set; }

        [Required]
        [StringLength(15)]
        public string TYPE { get; set; }

        [Required]
        [StringLength(50)]
        public string DWG { get; set; }

        [Required]
        [StringLength(10)]
        public string UM { get; set; }

        [Required]
        [StringLength(10)]
        public string QTY { get; set; }

        [Required]
        [StringLength(50)]
        public string MAINTAG { get; set; }

        [Required]
        [StringLength(50)]
        public string SYSTEM { get; set; }

        [Required]
        [StringLength(50)]
        public string SUBSYSTEM { get; set; }

        [Required]
        [StringLength(12)]
        public string ACTIVITY { get; set; }

        [Required]
        [StringLength(50)]
        public string LINE { get; set; }

        [Required]
        [StringLength(50)]
        public string LOOP_NAME { get; set; }

        [Required]
        [StringLength(50)]
        public string EQUIPMENT { get; set; }

        [Required]
        [StringLength(50)]
        public string JUNCTION_BOXES { get; set; }

        [Required]
        [StringLength(255)]
        public string INTERLOCK { get; set; }

        [Required]
        [StringLength(30)]
        public string IOTYPE { get; set; }

        public long id { get; set; }
    }
}
