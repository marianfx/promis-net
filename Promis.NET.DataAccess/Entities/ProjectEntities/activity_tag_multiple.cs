﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promis.NET.DataAccess.Entities.ProjectEntities
{
    public class activity_tag_multiple
    {
        public string tag { get; set; }
        public string activity { get; set; }
        public string id { get; set; }
        public string maxOrd { get; set; }

    }
}
