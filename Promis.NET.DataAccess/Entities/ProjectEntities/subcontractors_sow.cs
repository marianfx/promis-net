﻿using Promis.Web.DataAccess.Entities.ProjectEntities;

namespace Promis.NET.DataAccess.Entities
{
    public class subcontractors_sow: subc_sow
    {
        public string sub_act_d { get; set; }
        public string subcontractor { get; set; }
        public string subcode { get; set; }
        public double mhrs { get; set; }
        public double prod { get; set; }
        public string um { get; set; }
    }
}
