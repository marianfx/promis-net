namespace Promis.Web.DataAccess.Entities.PromisWeb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("promisweb.menu")]
    public partial class menu
    {
        [StringLength(255)]
        public string CODE { get; set; }

        [StringLength(255)]
        public string MODULE { get; set; }

        [StringLength(255)]
        public string ROOT { get; set; }

        [StringLength(255)]
        public string LEVEL1 { get; set; }

        [StringLength(255)]
        public string LEVEL2 { get; set; }

        [StringLength(255)]
        public string LEVEL3 { get; set; }

        [StringLength(255)]
        public string LINK { get; set; }

        [StringLength(255)]
        public string POLICY { get; set; }

        [Column(TypeName = "char")]
        [Required]
        [StringLength(1)]
        public string disabled { get; set; }

        public long id { get; set; }
    }
}
