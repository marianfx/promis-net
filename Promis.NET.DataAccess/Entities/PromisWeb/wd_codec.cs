namespace Promis.Web.DataAccess.Entities.PromisWeb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("promisweb.wd_codec")]
    public partial class wd_codec
    {
        [StringLength(25)]
        public string wc { get; set; }

        [StringLength(25)]
        public string ACT { get; set; }

        [StringLength(25)]
        public string subwc { get; set; }

        [StringLength(25)]
        public string SUB_ACT { get; set; }

        public long id { get; set; }

        [StringLength(15)]
        public string um { get; set; }
    }
}
