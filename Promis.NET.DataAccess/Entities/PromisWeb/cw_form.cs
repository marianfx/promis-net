namespace Promis.Web.DataAccess.Entities.PromisWeb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("promisweb.cw_form")]
    public partial class cw_form
    {
        [StringLength(255)]
        public string form { get; set; }

        [StringLength(255)]
        public string descr { get; set; }

        [StringLength(255)]
        public string mtag { get; set; }

        [StringLength(255)]
        public string subtag { get; set; }

        [StringLength(255)]
        public string type { get; set; }

        [StringLength(255)]
        public string typecode { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        [StringLength(255)]
        public string projcode { get; set; }

        [StringLength(50)]
        public string keycode { get; set; }

        [Required]
        [StringLength(50)]
        public string DATATABLE { get; set; }

        public long id { get; set; }
    }
}
