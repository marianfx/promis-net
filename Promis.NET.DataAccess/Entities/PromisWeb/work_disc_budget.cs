﻿using Promis.Web.DataAccess.Entities.PromisWeb;

namespace Promis.NET.DataAccess.Entities.PromisWeb
{
    public class work_disc_budget: work_disc
    {
        public int? budget { get; set; }
    }
}
