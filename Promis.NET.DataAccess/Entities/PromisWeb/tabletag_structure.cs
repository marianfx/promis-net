namespace Promis.Web.DataAccess.Entities.PromisWeb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("promisweb.tabletag_structure")]
    public partial class tabletag_structure
    {
        [Required]
        [StringLength(25)]
        public string code { get; set; }

        [Required]
        [StringLength(100)]
        public string description { get; set; }

        [Required]
        [StringLength(10)]
        public string fld_type { get; set; }

        [Required]
        [StringLength(10)]
        public string fld_len { get; set; }

        [Column(TypeName = "char")]
        [Required]
        [StringLength(1)]
        public string is_index { get; set; }

        [Required]
        [StringLength(255)]
        public string for_table { get; set; }

        public long id { get; set; }
    }
}
