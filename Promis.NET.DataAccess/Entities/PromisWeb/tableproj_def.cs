namespace Promis.Web.DataAccess.Entities.PromisWeb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("promisweb.tableproj_def")]
    public partial class tableproj_def
    {
        [StringLength(30)]
        public string datatable { get; set; }

        [StringLength(100)]
        public string description { get; set; }

        public long id { get; set; }
    }
}
