namespace Promis.Web.DataAccess.Entities.PromisWeb
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("promisweb.projects")]
    public partial class project
    {
        [StringLength(12)]
        public string proj { get; set; }

        [StringLength(255)]
        public string descr { get; set; }

        [StringLength(50)]
        public string client { get; set; }

        [StringLength(50)]
        public string location { get; set; }

        [StringLength(50)]
        public string geoloc { get; set; }

        [StringLength(10)]
        public string s_date { get; set; }

        [StringLength(10)]
        public string e_date { get; set; }

        [StringLength(6)]
        public string wd { get; set; }

        [StringLength(6)]
        public string c_off { get; set; }

        [Column(TypeName = "text")]
        public string logo { get; set; }

        public long id { get; set; }
    }
}
