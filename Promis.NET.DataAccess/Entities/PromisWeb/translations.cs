namespace Promis.Web.DataAccess.Entities.PromisWeb
{
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("promisweb.translations")]
    public partial class translations
    {
        public string en { get; set; }
        public string fr { get; set; }
        public string es { get; set; }
        public string ru { get; set; }
        public string it { get; set; }

        public int Id { get; set; }
    }
}
