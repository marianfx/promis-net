namespace Promis.Web.DataAccess.Entities.PromisWeb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("promisweb.tabletag_def")]
    public partial class tabletag_def
    {
        [StringLength(30)]
        public string datatable { get; set; }

        [StringLength(100)]
        public string description { get; set; }

        [Required]
        [StringLength(10)]
        public string cod { get; set; }

        public int pos { get; set; }

        [Column(TypeName = "char")]
        [Required]
        [StringLength(1)]
        public string ACTIVE { get; set; }

        public long id { get; set; }

        [StringLength(255)]
        public string oldname { get; set; }
    }
}
