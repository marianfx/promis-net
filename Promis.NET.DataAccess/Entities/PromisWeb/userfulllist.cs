namespace Promis.Web.DataAccess.Entities.PromisWeb
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("promisweb.userfulllist")]
    public partial class userfulllist
    {
        [Required]
        [StringLength(15)]
        public string proj { get; set; }

        [StringLength(50)]
        public string userid { get; set; }

        [StringLength(20)]
        public string psw { get; set; }
        
        public byte[] pswb { get; set; }

        [StringLength(50)]
        public string company { get; set; }

        [StringLength(50)]
        public string Modules { get; set; }

        [StringLength(50)]
        public string Activities { get; set; }

        [StringLength(50)]
        public string Resp { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Surname { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [StringLength(50)]
        public string email { get; set; }

        [StringLength(20)]
        public string mobile { get; set; }

        [StringLength(20)]
        public string office { get; set; }

        [Column(TypeName = "char")]
        [StringLength(3)]
        public string lang { get; set; }
        
        public string photo { get; set; }

        [Column(TypeName = "char")]
        [StringLength(1)]
        public string doc { get; set; }

        public long id { get; set; }
    }
}
