namespace Promis.Web.DataAccess.Entities.PromisWeb
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("promisweb.work_disc")]
    public partial class work_disc
    {
        [Column(TypeName = "char")]
        [StringLength(3)]
        public string cod { get; set; }

        [StringLength(100)]
        public string descr { get; set; }

        public int ord { get; set; }

        [Column(TypeName = "char")]
        [StringLength(1)]
        public string flag { get; set; }

        public long id { get; set; }
    }
}
