namespace Promis.Web.DataAccess.Entities.PromisWeb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("promisweb.videos")]
    public partial class video
    {
        [Required]
        [StringLength(10)]
        public string thumb { get; set; }

        [Required]
        [StringLength(255)]
        public string url { get; set; }

        [Required]
        [StringLength(255)]
        public string descr { get; set; }

        public int Id { get; set; }
    }
}
