namespace Promis.Web.DataAccess.Entities.PromisWeb
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("promisweb.modules")]
    public class module
    {
        [StringLength(3)]
        public string code { get; set; }

        [StringLength(50)]
        public string description { get; set; }

        [Column("class")]
        [StringLength(15)]
        public string classCol { get; set; }

        public int pos { get; set; }

        public long id { get; set; }
    }
}
