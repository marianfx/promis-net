namespace Promis.Web.DataAccess.Entities.PromisWeb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("promisweb.cw_cat")]
    public partial class cw_cat
    {
        [StringLength(30)]
        public string cat { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        [StringLength(100)]
        public string fd_action { get; set; }

        [StringLength(100)]
        public string fd_file { get; set; }

        public long id { get; set; }
    }
}
