﻿using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Repositories.Interfaces;
using SqlKata;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using Promis.NET.Utilities.Models;
using System;

namespace Promis.NET.DataAccess.Repositories
{
    public class CommissioningRepository : Repository, ICommissioningRepository
    {
        public CommissioningRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        #region Common use functionality
        public List<Tuple<tabletab_def_base, int, int>> GetDatatables(bool isFromSys)
        {
            List<Tuple<tabletab_def_base, int, int>> result = new List<Tuple<tabletab_def_base, int, int>>();
            //var result = new Dictionary<tabletab_def_base, int>();
            var tablesQuery = new Query("promisweb.tabletag_def")
                            .Select("datatable", "description");
            if (isFromSys) { tablesQuery.Where("type_source", "<>", ""); }
            tablesQuery.Where("PC", "<>", "")
                       .OrderBy("cod", true);
            if (!isFromSys)
                tablesQuery.Distinct();
            var tablesCompiled = _compiler.Compile(tablesQuery);
            var tablesResults = ExecuteQuery<tabletab_def_base>(tablesCompiled.Sql, tablesCompiled.Bindings).ToList();
            foreach (var table in tablesResults)
            {
                var countResultType = 0;
                var query = new Query(table.datatable);
                query.Count();
                var compiled = _compiler.Compile(query);
                var countResult = ExecuteQueryForValue<int>(compiled.Sql, compiled.Bindings);
                if (isFromSys && countResult > 0)
                {
                    var secondQuery = query;
                    secondQuery.Where("system", "<>", "");
                    secondQuery.Count();
                    var compiledType = _compiler.Compile(secondQuery);
                    countResultType = ExecuteQueryForValue<int>(compiledType.Sql, compiledType.Bindings);
                }
                result.Add(new Tuple<tabletab_def_base, int, int>(table, countResult, countResultType));
            }
            return result;
        }

        public List<KeyValuePair<string, string>> GetTypecodesForDatatables(string tableName, bool isWithJoin = false)
        {
            var rez = new List<KeyValuePair<string, string>>();
            var typeQuery = new Query(tableName + "_type AS A");
            if (isWithJoin)
            {
                typeQuery.Join(tableName, "A.code", tableName + ".type");
            }
            typeQuery.Select("A.code", "A.description").OrderBy("description").Distinct();
            var typeCompile = _compiler.Compile(typeQuery);
            var querryRez = ExecuteQuery(typeCompile.Sql, typeCompile.Bindings);
            foreach (DataRow item in querryRez.Rows)
            {
                if (item[0] != null && item[1] != null)
                    rez.Add(new KeyValuePair<string, string>(item[0].ToString(), item[1].ToString()));
            }
            return rez;
        }
       
        #endregion

        #region Assign inspection to tags

        public List<work_def_subact> GetSubactForDatatables(string tableName, string activity)
        {
            var tableQuery = new Query("promisweb.tabletag_def").Select("PC").Where("datatable", tableName).Limit(1);
            var tableCompile = _compiler.Compile(tableQuery);
            var pcResult = ExecuteQueryForValue<string>(tableCompile.Sql, tableCompile.Bindings);

            var query = new Query("work_def as A")
                            .Select("sub_act", "sub_act_d")
                            .Join("planning as B", "A.act", "B.act")
                            .WhereStarts("A.act", pcResult + "_" + activity)
                            .Where("mhrs", ">", 0)
                            .Where("type", "P")
                            .GroupBy("sub_act")
                            .OrderBy("A.sub_act");
            var compiled = _compiler.Compile(query);
            return ExecuteQuery<work_def_subact>(compiled.Sql, compiled.Bindings).ToList();

        }


        public List<DataKeeper> GetColumnsNecessaryForTagsTable(string mainActivity)
        {
            return new DataKeeper[] {
                new DataKeeper(" "),
                new DataKeeper(mainActivity+"_ACTIVITY","INSPECTION"),
                new DataKeeper("TAG"),
                new DataKeeper("DESCRIPTION"),
                new DataKeeper("QTY"),
                new DataKeeper("UM"),
                new DataKeeper("WBS","AREA"),
                new DataKeeper("SYSTEM"),
                new DataKeeper("DWG","DRAWING") }.ToList();
        }

        public List<activity_tag_multiple> GetTagAndActivityforDatatables(string prefix, string tableName, List<string> counter)
        {
            var rez = new List<KeyValuePair<string, string>>();
            var query = new Query(tableName).Select("tag", prefix.Equals("")?"activity":prefix+"activity AS activity", "id").WhereIn("id", counter);
            var compile = _compiler.Compile(query);
            var rezult = ExecuteQuery<activity_tag_multiple>(compile.Sql, compile.Bindings).ToList();
            return rezult;
        }

        public void CreateCertTableRow(string datatable, string tag, string subActivity)
        {
            var selQuery = new Query("work_def")
                .SelectRaw("@tag, @sub_act, `step`")
                .Where("sub_act", "=", subActivity)
                .OrderBy("ord");
            var inQuery = new Query("cert_"+datatable)
                .Insert(new string[] { "tag", "subact", "step" }, selQuery);
            var queryCompiled = _compiler.Compile(inQuery);
            var parameters = queryCompiled.Bindings;
            parameters.Add("@tag", tag);
            parameters.Add("@sub_act", subActivity);
            ExecuteCommandWithNoResults(queryCompiled.Sql, parameters);
        }

        public void DeleteCertTableRow(string datatable, string tag, string activity)
        {
            var removeQuery = new Query("cert_"+datatable)
                .Delete()
                .Where("tag", "=", tag)
                .Where("subact", "=", activity);
            var queryString = _compiler.Compile(removeQuery);
            ExecuteCommandWithNoResults(queryString.Sql, queryString.Bindings);
        }
        #endregion

        #region Status
        public List<tabletab_def_base> GetDataTablesForStatusTab(bool isCertification = true)
        {
            var selQuery = new Query("promisweb.tabletag_def")
                             .Select("datatable", "description")
                             .Join("work_disc", j => j.On("tabletag_def.pc", "work_disc.cod"))
                             .Where("tabletag_def.flag", "=", "P")
                             .OrderBy("tabletag_def.datatable");

            if (!isCertification)
                selQuery = selQuery.Where("tabletag_def.pc", "<>", "");

            var queryString = _compiler.Compile(selQuery);
            return ExecuteQuery<tabletab_def_base>(queryString.Sql, queryString.Bindings).ToList();
        }

        public KeyValuePair<double, double> GetDataTableCount(string dataTable, string prefix = null)
        {
            var selQueryCountNotEq = new Query(dataTable)
                                    .Count();
            var selQueryCountEq = new Query(dataTable)
                                    .Count();
            if (prefix == null)
            {
                selQueryCountNotEq=selQueryCountNotEq.Where("system" , "<>","");
                selQueryCountEq= selQueryCountEq.Where("system", "=", "");
            }
            else
            {
                selQueryCountNotEq = selQueryCountNotEq.Where(prefix + "_activity", "<>", "");
                selQueryCountEq = selQueryCountEq.WhereRaw(prefix + "_activity='' and qty >0");
            }

            var queryStringNotEq = _compiler.Compile(selQueryCountNotEq);           
            var queryStringEq = _compiler.Compile(selQueryCountEq);

            var countNotEqual = ExecuteQueryForValue<double?>(queryStringNotEq.Sql, queryStringNotEq.Bindings);
            var countEqual = ExecuteQueryForValue<double?>(queryStringEq.Sql, queryStringEq.Bindings);
            return new KeyValuePair<double, double>(countNotEqual ?? 0, countEqual ?? 0);
        }

        public List<DataKeeper> GetColumnsForTablesWithTagsNotAssigned(string dataTable)
        {
            return new DataKeeper[] {
                new DataKeeper("TAG"),
                new DataKeeper("DESCRIPTION"),
                new DataKeeper("TYPE"),
                new DataKeeper("UM"),
                new DataKeeper("QTY")}.ToList();
        }

        public double GetManHoursForProjectStatus(string sufix)
        {
            var planningQuery = new Query("planning")
                           .SelectRaw("sum(mhrs)")
                           .Where("type", "=", "p")
                           .WhereRaw("mid(act,4,2)='" + sufix + "'");

            var queryCompiled = _compiler.Compile(planningQuery);
            var result = ExecuteQueryForValue<double?>(queryCompiled.Sql, queryCompiled.Bindings);
            return result ?? 0;
        }

        public double? GetManHoursPlannedForStatus(string column, double totalManHours, string sufix)
        {
            var sqlString = new Query("planning")
                           .SelectRaw("round(sum(" + column + "*mhrs)/" + totalManHours + ",2)")
                           .Where("type", "=", "P")
                           .WhereRaw("mid(act,4,2)='" + sufix + "'");

            var querySumMhr = _compiler.Compile(sqlString);
            var result = ExecuteQueryForValue<double?>(querySumMhr.Sql, querySumMhr.Bindings);
            return result ?? 0;
        }

        public double? GetSumEarned(string sufix, string month, double totmhrs)
        {
            var sqlString = new Query("progress" + sufix)
                          .SelectRaw("round(sum(earned) / " + totmhrs + "* 100, 2)")
                          .Where("m", "=", month)
                          .GroupBy("w")
                          .OrderByDesc("w")
                          .Limit(1);

            var querySumMhr = _compiler.Compile(sqlString);
            var result = ExecuteQueryForValue<double?>(querySumMhr.Sql, querySumMhr.Bindings);
            return result ?? 0;
        }

        public List<system_base> GetSystemsForLoop()
        {
            var sysQuery = new Query("loopcheck")
                             .Select("loopcheck.system", "systems.system_description")
                             .Distinct()
                             .Join("systems", j => j.On("loopcheck.system", "systems.system"))
                             .OrderBy("loopcheck.system");

            var queryString = _compiler.Compile(sysQuery);
            return ExecuteQuery<system_base>(queryString.Sql, queryString.Bindings).ToList();
        }

        public List<string> GetTagsForSystem(string system)
        {
            var tags = new List<string>();
            var query= new Query("loopcheck_link")
                             .Select("tag")
                             .Distinct()
                             .Where("system", "=", system);

            var queryString = _compiler.Compile(query);
            var result = ExecuteQuery(queryString.Sql, queryString.Bindings);

            for(int i = 0; i < result.Rows.Count; i++)
            {
                var tag = result.Rows[i][0].ToString();
                tags.Add(tag);
            }
            return tags;
        }

        public List<Instruments_loopcheck> GetAllInstrumentsData(string tag)
        {
            var query = new Query("instruments")
                             .Select("loopcheck_link.comp", "instruments.description","instruments.system","instruments.type")
                             .Join("loopcheck_link", j => j.On("instruments.tag", "loopcheck_link.comp"))
                             .Where("loopcheck_link.tag","=", tag);

            var queryCompiled= _compiler.Compile(query);              
            return ExecuteQuery<Instruments_loopcheck>(queryCompiled.Sql, queryCompiled.Bindings).ToList();
        }

        public double GetProgressSum(string comp)
        {
            var query = new Query("work_def")
                             .SelectRaw("sum(work_def.progr)")
                             .Join("cert_instruments", j => j.On("work_def.sub_act", "cert_instruments.subact").On("work_def.step", "cert_instruments.step"))
                             .Where("cert_instruments.tag", "=", comp)
                             .Where("cert_instruments.c", "<>", "")
                             .OrderByRaw("cast(cert_instruments.step as signed) desc")
                             .Limit(1);

            var queryCompiled = _compiler.Compile(query);
            var result = ExecuteQueryForValue<double?>(queryCompiled.Sql, queryCompiled.Bindings);
            return result ?? 0;
        }

        public cert_instruments_workdef GetInfoForInstruments(string comp)
        {
            var query = new Query("cert_instruments")
                             .Select("cert_instruments.step", "work_def.sub_act", "work_def.inspection")
                             .Join("work_def", j => j.On("cert_instruments.subact", "work_def.sub_act").On("cert_instruments.step", "work_def.step"))
                             .WhereRaw("cert_instruments.c <>''")
                             .Where("cert_instruments.tag", "=", comp)
                             .OrderByRaw("cast(cert_instruments.step as signed) desc")
                             .Limit(1);

            var queryCompiled = _compiler.Compile(query);
            var result = ExecuteQueryForValue<cert_instruments_workdef>(queryCompiled.Sql, queryCompiled.Bindings);
            return result;
        }

        public workdef_step GetNextStepForLoop(string step, string sub_act)
        {
            var query = new Query("work_def")
                             .Select("inspection", "progr")
                             .Where("step", ">", int.Parse(step))
                             .Where("sub_act","=",sub_act)
                             .Where("progr",">","0")
                             .Limit(1);

            var queryCompiled = _compiler.Compile(query);
            var result= ExecuteQueryForValue<workdef_step>(queryCompiled.Sql, queryCompiled.Bindings);
            return result;
        }
        #endregion

        #region System Subdivision
        public List<DataKeeper> GetColumnsNecessaryForSystemTable()
        {
            return new DataKeeper[] {
                new DataKeeper(" "),
                new DataKeeper("SYSTEM"),
                new DataKeeper("TAG"),
                new DataKeeper("DESCRIPTION"),
                new DataKeeper("TYPE"),
                new DataKeeper("QTY"),
                new DataKeeper("UM"),
                new DataKeeper("WBS"),
                new DataKeeper("DWG","DRAWING") }.ToList();
        }

        public List<system_base> GetSystemList()
        {
            var query = new Query("systems").Select("SYSTEM", "system_description").OrderBy("system");
            var compiled = _compiler.Compile(query);
            return ExecuteQuery<system_base>(compiled.Sql, compiled.Bindings).ToList();
        }

        public KeyValuePair<int, int> GetTagCountsAndPercentageForTable(string dataTable)
        {
            var totCount = 0;
            var assignedCount = 0;
            var queryTagCount = new Query(dataTable)
                              .Count();
            var compiledQuery = _compiler.Compile(queryTagCount);
            var resultTotalCount = ExecuteQueryForValue<int>(compiledQuery.Sql, compiledQuery.Bindings);

            if (resultTotalCount > 0)
            {
                totCount = totCount + resultTotalCount;
                var tagAssigned = queryTagCount.Where("system", "<>", "");
                var compiledTagAss = _compiler.Compile(tagAssigned);
                var resultAssignedCount = ExecuteQueryForValue<int>(compiledTagAss.Sql, compiledTagAss.Bindings);

                if (resultAssignedCount > 0)
                    assignedCount = assignedCount + resultAssignedCount;
            }
            return new KeyValuePair<int, int>(totCount, assignedCount);
        }
        #endregion

        #region Inspection Follow Up
        public List<DataKeeper> GetColumnsNecessaryForFollowUpTable()
        {
            return new DataKeeper[] {
                new DataKeeper("TAG","TAG CODE"),
                new DataKeeper("DESCRIPTION") }.ToList();
        }

        public activity_tag_multiple GetTagAndSubAct(string datatable, string tagId, string activity)
        {
            var query = new Query(datatable).Select("tag", activity + "activity AS activity", "id").Where("id", tagId);
            var compile = _compiler.Compile(query);
            return ExecuteQueryForValue<activity_tag_multiple>(compile.Sql, compile.Bindings);
        }

        public activity_tag_multiple GetSubactForFollowUpSelectedRow(string subActivity)
        {
            var query = new Query("work_def").SelectRaw("sub_act_d AS activity,Max(ord) As maxOrd").Where("sub_act", subActivity);
            var compile = _compiler.Compile(query);
            return ExecuteQuery<activity_tag_multiple>(compile.Sql, compile.Bindings).SingleOrDefault();
        }

        public List<work_def_sub_act> GetAllForInspectionStatus(string fullTableName,string tag)
        {
            var query = new Query(fullTableName + " AS A").Select("B.inspection AS inspection", "A.c AS code", "c_user", "filename", "B.ord AS ord", "result")
                .Join("work_def AS B",j => j.On( "A.subact", "B.sub_act").On("B.ord", "A.step"))
                .Where("tag", tag)
                .GroupBy("ord");
            var compile = _compiler.Compile(query);
            return ExecuteQuery<work_def_sub_act>(compile.Sql, compile.Bindings).ToList();
        }

        public void UpdateInspectionsForDatatable(string dataTable, string userName, string tag, string result, string ord)
        {
            var entries = new Dictionary<string, object>();
            entries.Add("c", (result.Equals("N"))?DateTime.UtcNow.ToShortDateString():"");
            entries.Add("c_user", userName);
            entries.Add("result", result);
            var query = new Query(dataTable)
                .Update(entries)
                .Where("tag", tag)
                .Where("step", ord);
            var compile = _compiler.Compile(query);
            ExecuteCommandWithNoResults(compile.Sql, compile.Bindings);
        }
        #endregion

    }


}
