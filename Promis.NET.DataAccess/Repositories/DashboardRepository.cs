﻿using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using SqlKata;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Promis.NET.Localization;

namespace Promis.NET.DataAccess.Repositories
{
    public enum ActivityType { Main, Full, Sub};

    public class DashboardRepository : Repository, IDashboardRepository
    {
        public DashboardRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        #region Planning & Progress
        public DataTable GetAllPlanningData()
        {
            const string sqlString = "SELECT * FROM PLANNING LIMIT 1";
            return ExecuteQuery(sqlString);
        }

        public double GetManHoursForProject(string activity = null, ActivityType type = ActivityType.Main)
        {
            var query = new Query(type == ActivityType.Sub ? "budget_subact" : "planning");
            query = (type == ActivityType.Sub ? query.Select("mhrs") : query.Sum("mhrs"));

            if (type != ActivityType.Sub)
            {
                query = query.Where("type", 'p');
                if (activity != null && type == ActivityType.Main)
                    query = query.WhereRaw("LEFT(act, 2) = ?", activity);
                else if (activity != null && type == ActivityType.Full)
                    query = query.Where("act", activity);
            }
            else
            {
                query = query.WhereIf(activity != null, "sub_act", activity).Limit(1);
            }

            var compiled = _compiler.Compile(query);
            var resultedMh = ExecuteQueryForValue<double?>(compiled.Sql, compiled.Bindings);
            return resultedMh ?? 0;
        }

        public double GetManHoursPlanned(string progressIdentifier, double totalManHours, string activity = null)
        {
            //?var alterActs = new string[] { "PR", "CO", "SU" };
            var query = new Query("planning");
            query = query.SelectRaw(string.Format("ROUND(SUM({0}*mhrs)/{1}, 2)", progressIdentifier, totalManHours));
            query = query.Where("type", 'p');
            if (activity != null)
                query = query.WhereRaw("LEFT(act, 2) = ?", activity);
            var compiled = _compiler.Compile(query);

            var resultedMh = ExecuteQueryForValue<double?>(compiled.Sql, compiled.Bindings);
            return resultedMh ?? 0;
        }

        public double GetMonthActualPercent(double totalManHours, string monthValue, string activity = null)
        {
            //?var alterActs = new string[] { "PR", "CO", "SU" };
            var parameters = new Dictionary<string, object>() { { "@month", monthValue } };
            var alterCondition = string.IsNullOrWhiteSpace(activity) ? "" : " AND LEFT(act, 2) = @activity";
            if (!string.IsNullOrWhiteSpace(activity))
                parameters.Add("@activity", activity);

            var sqlString = String.Format(@"SELECT ROUND(SUM(earned)/{0}*100, 2) FROM PROGRESS
                                            WHERE m = @month" + alterCondition + @"
                                            GROUP BY w
                                            ORDER BY w DESC
                                            LIMIT 1",
                                            totalManHours);

            var resultedMh = ExecuteQueryForValue<double?>(sqlString, parameters);
            return resultedMh ?? 0;
        }
        #endregion
       
        #region Trends
        public PercentData GetTrendsActualData(double totalManHours, string year, string weekNumber, string activity = null)
        {
            var query = new Query("progress")
                            .Select("m", "w")
                            .SelectRaw("SUM(earned) AS earned, ROUND(SUM(earned) / ? * 100, 2) AS spent", totalManHours)
                            .Where("w", "<=", year + "_" + weekNumber)
                            .GroupBy("w")
                            .OrderBy("w", false)
                            .Limit(2);
            if (!string.IsNullOrWhiteSpace(activity))
                query = query.Where("act", activity);
            var compiled = _compiler.Compile(query);
            
            var resultSumEarned = ExecuteQuery(compiled.Sql, compiled.Bindings);
            if (resultSumEarned.Rows.Count < 2)
                throw new Exception(LocalResources.CannotRetrieveThePercentForTheActualTrendsCharts);

            var output =  new PercentData()
            {
                Month = Utilities.Models.Global.DataCellToString(resultSumEarned.Rows[0]["m"]),
                Week = Utilities.Models.Global.DataCellToString(resultSumEarned.Rows[0]["w"])
            };

            double.TryParse(Utilities.Models.Global.DataCellToString(resultSumEarned.Rows[0]["spent"], "0"), out double percent);
            double.TryParse(Utilities.Models.Global.DataCellToString(resultSumEarned.Rows[1]["spent"], "0"), out double percentWeek);
            output.Percent = percent;
            output.PercentWeekly = percentWeek;
            return output;
        }

        public PercentData GetTrendsMonthlyData(double totalManHours, string month, string activity = null)
        {
            var query = new Query("progress")
                            .Select("m")
                            .SelectRaw("ROUND(SUM(earned) / ? * 100, 2) AS spent", totalManHours)
                            .Where("m", "<", month)
                            .GroupBy("w")
                            .OrderBy("w", false)
                            .Limit(1);
            if (!string.IsNullOrWhiteSpace(activity))
                query = query.Where("act", activity);
            var compiled = _compiler.Compile(query);

            var resultSumEarnedSec = ExecuteQuery(compiled.Sql, compiled.Bindings);
            if (resultSumEarnedSec.Rows.Count == 0)
                throw new Exception(LocalResources.CannotRetrieveTheMonthlyPercentForTheActualTrendsCharts);

            var output =  new PercentData()
            {
                Month = Utilities.Models.Global.DataCellToString(resultSumEarnedSec.Rows[0]["m"])
            };
            double.TryParse(Utilities.Models.Global.DataCellToString(resultSumEarnedSec.Rows[0]["spent"]), out double percent);
            output.PercentMonthly = percent;
            return output;
        }

        public PercentData GetTrendsPlannedData(string monthInitial, string weekInitial, string monthMonthly)
        {
            var output = new PercentData();
            var mhrsString = @"SELECT ROUND(SUM({0}*mhrs)/SUM(mhrs), 2)
                                                        FROM PLANNING
                                                        WHERE TYPE = 'p'";
            var sqlCurentMonthmhr = String.Format(mhrsString, monthInitial);
            output.Percent = ExecuteQueryForValue<double?>(sqlCurentMonthmhr) ?? 0;


            var sqlSumPerMonth = String.Format(mhrsString, monthMonthly);
            output.PercentMonthly = ExecuteQueryForValue<double?>(sqlSumPerMonth) ?? 0;

            var sqlSumCutOff = String.Format(@"SELECT m, COUNT(*)
                                                FROM cut_off
                                                WHERE m = @month1 OR m = @month2
                                                    GROUP BY m
                                                    ORDER BY m
                                                LIMIT 2");
            var parameters = new Dictionary<string, object>()
            {
                { "@month1", monthInitial },
                { "@month2", monthMonthly }
            };
            var resSumCutOff = ExecuteQuery(sqlSumCutOff, parameters);
            if (resSumCutOff.Rows.Count < 2)
                throw new Exception(LocalResources.CannotRetrieveCurrentWeekNumberForTheCumulatedTrendsCharts);

            double.TryParse(Utilities.Models.Global.DataCellToString(resSumCutOff.Rows[1][1], "0"), out double weekCount);
            if (weekCount == 0)
                throw new Exception(LocalResources.EncounteredWeekCountInPlanningCutoffCannotComputeRangeToComputeThePercentForPlannedTrendsData);
            var range = (output.Percent - output.PercentMonthly) / weekCount;

            var sqlCutOffCurWeek = String.Format(@"SELECT COUNT(1)
                                                    FROM cut_off
                                                    WHERE m = @month and w <= @week
                                                    LIMIT 1");
            parameters = new Dictionary<string, object>()
            {
                { "@month", monthInitial },
                { "@week", weekInitial }
            };
            var resultValue = ExecuteQueryForValue<double?>(sqlCutOffCurWeek, parameters) ?? 0;

            output.Percent = Math.Round(output.PercentMonthly + (range * resultValue), 2);
            output.PercentWeekly = Math.Round(output.PercentMonthly + (range * (resultValue - 1)), 2);
            return output;
        }
        #endregion


        #region Activities
        public DataTable GetMainActivities(string year, string month)
        {
            var sqlString = String.Format(@"SELECT cod, descr, ROUND(SUM({0}*mhrs) / SUM(mhrs), 2) AS pr, SUM(mhrs) AS mhrs 
                                            FROM planning, work_disc
                                            WHERE LEFT(act, 2) = cod AND  type = 'p' AND mhrs > 0
                                                GROUP BY LEFT(act, 2)
                                                ORDER BY ord",
                                            "p_" + year + "_" + month);
            return ExecuteQuery(sqlString);
        }
        
        public double? GetActualPercentByActivity(double manHours, string code, string year, string week)
        {
            var sqlString = String.Format(@"SELECT ROUND(SUM(earned) / {0} * 100, 2)
                                            FROM progress
                                            WHERE LEFT(act, 2) = @act AND w = @week",
                                            manHours);
            var parameters = new Dictionary<string, object>() { { "@act", code }, { "@week", year + "_" + week } };
            return ExecuteQueryForValue<double?>(sqlString, parameters);
        }
        #endregion

        #region Outstandings / Punchlist
        public int GetPunchlistCount()
        {
            return CountAll<punchlist>();
        }

        public int GetPunchlistOutsCount()
        {
            return GetAll<punchlist>().Where(p => p.js != "DONE" && p.js != "CANC").Count();
        }

        public List<punchlist_base> GetAllPunchlistData()
        {
            string sqlString = @"SELECT DISTINCT C.CODE as FLAG, C.DESCRIPTION as WD, COUNT(CASE WHEN P.JS = 'DONE' OR P.JS = 'CANC' THEN 1 ELSE NULL END) as POS
                                FROM PUNCHLIST AS P
                                    JOIN PL_CATEGORY AS C ON P.FLAG = C.CODE
                                    GROUP BY C.CODE
                                    ORDER BY C.CODE";

            return ExecuteQuery<punchlist_base>(sqlString).ToList();
        }
    #endregion
    }
}
