﻿using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Entities.PromisWeb;
using Promis.NET.DataAccess.Repositories.Interfaces;
using SqlKata;
using System.Collections.Generic;
using System.Linq;
using Promis.Web.DataAccess.Entities.PromisWeb;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using Promis.NET.Utilities.Models;
using System;
using Promis.NET.Localization;

namespace Promis.NET.DataAccess.Repositories
{
    public class ProjectRepository : Repository, IProjectRepository
    {

        public ProjectRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        #region Activities
        public Dictionary<string, string> GetMainActivities()
        {
            var tableName = "promisweb.work_disc";
            var output = new Dictionary<string, string>();
            var result = new Query(tableName + " AS A")
                                .Select("cod as Key", "descr as Value")
                                .OrderBy("A.ord", true);
            var compiled = _compiler.Compile(result);
            var results = ExecuteQuery(compiled.Sql, compiled.Bindings);
            for (var i = 0; i < results.Rows.Count; i++)
            {
                var key = results.Rows[i]["Key"]?.ToString();
                var value = results.Rows[i]["Value"]?.ToString();
                if (key != null && !output.Keys.Contains(key))
                    output.Add(key, value);
            }

            return output;
        }

        public List<work_disc_budget> GetMainActivities(string flag = null, ActivityOrderType orderType = ActivityOrderType.None)
        {
            var tableName = DoesTableExist("work_disc") ? "work_disc" : "promisweb.work_disc";
            var result = new Query(tableName + " AS A")
                                .LeftJoin("budget", "A.cod", "budget.act")
                                .OrderBy("A.ord", true);

            if (!string.IsNullOrWhiteSpace(flag) && flag != "<>")
                result = result.Where("flag", flag);
            if (!string.IsNullOrEmpty(flag) && flag == "<>")
                result = result.WhereNotNull("flag").WhereNot("flag", "");

            if (orderType == ActivityOrderType.Under16)
                result = result.Where("A.ord", "<=", 16);
            else if (orderType == ActivityOrderType.Over16)
                result = result.Where("A.ord", ">", 16);

            var compiled = _compiler.Compile(result);
            return ExecuteQuery<work_disc_budget>(compiled.Sql, compiled.Bindings).ToList();
        }

        public List<string> GetMainActivityTables(string mainActivity)
        {
            var query = new Query("tabletag_def")
                            .Select("datatable")
                            .Where("cod", mainActivity)
                            .Distinct();
            var compiled = _compiler.Compile(query);
            return ExecuteQuery<string>(compiled.Sql, compiled.Bindings).ToList();
        }

        public List<work_def_base> GetAssociatedActivities(string mainActivityCode)
        {
            var alterActs = new string[] { "PR", "CO", "SU" };
            var query = new Query("work_def")
                            .Select("act", "act_d")
                            .OrderBy("ord", true)
                                .OrderBy("act", true)
                            .Distinct();
            query = alterActs.Contains(mainActivityCode) ? query.WhereContains("act", "_" + mainActivityCode, true) : query.WhereRaw("LEFT(act, 2) = ?", mainActivityCode);

            var compiled = _compiler.Compile(query);
            return ExecuteQuery<work_def_base>(compiled.Sql, compiled.Bindings).ToList();
        }

        public List<work_planning> GetAssociatedActivitiesWithMainData(string mainActivityCode, bool getOnlyBaselineAndPlanned = true)
        {
            var joinCond = new Join().On("WD.act", "P.act");
            if (getOnlyBaselineAndPlanned)
                joinCond = joinCond.WhereIn("P.type", new[] { "B", "P" });

            var query = new Query("work_def AS WD")
                            .Select("WD.act", "WD.act_d", "P.start_date", "P.end_date", "P.type")
                            .SelectRaw("CASE `P`.`mhrs` when null then 0 when '' then 0 ELSE `p`.`mhrs` END AS mhrs")
                            .LeftJoin("planning AS P", j => joinCond)
                            .OrderBy("WD.ord", true)
                                .OrderBy("WD.act", true)
                                .OrderBy("P.type", true)
                            .Distinct();

            var alterActs = new string[] { "PR", "CO", "SU" };
            query = alterActs.Contains(mainActivityCode) ? query.WhereContains("WD.act", "_" + mainActivityCode, true) : query.WhereRaw("LEFT(WD.act, 2) = ?", mainActivityCode);

            var compiled = _compiler.Compile(query);
            return ExecuteQuery<work_planning>(compiled.Sql, compiled.Bindings).ToList();
        }

        public bool CheckIfAnySubactivityHasDataInTables(IEnumerable<work_def_subact> subactivities, IEnumerable<string> tables)
        {
            // build the static sub-query that checks for activities (OR)
            var subQuery = new Query();
            foreach (var subact in subactivities)
            {
                subQuery = subQuery.OrWhere("activity", subact.SUB_ACT);
            }

            foreach (var table in tables)
            {
                var query = new Query(table)
                                .Count()
                                .Where("id", ">", 0)
                                .Where(q => subQuery);

                var compiled = _compiler.Compile(query);
                var result = ExecuteQueryForValue<long>(compiled.Sql, compiled.Bindings);
                if (result > 0)
                    return true;
            }

            return false;
        }

        public void UpdateActivityPlanningData(project project, work_planning activityData, List<double> progressValues)
        {
            var existing = GetAll<planning>().Where(p => p.act == activityData.act && p.type == activityData.type).FirstOrDefault();
            if (existing == null)
            {
                var toInsert = new Dictionary<string, object>()
                {
                    { "act", activityData.act },
                    { "description", activityData.act_d },
                    { "type", activityData.type }
                };
                var insertQuery = new Query("planning").Insert(toInsert);
                var compiledInsert = _compiler.Compile(insertQuery);
                ExecuteCommandWithNoResults(compiledInsert.Sql, compiledInsert.Bindings);
            }

            // will need to execute non-standard UPDATE because the row needs to be cleared
            var toUpdate = new Dictionary<string, object>()
            {
                { "mhrs", activityData.mhrs?.ToString()},
                {  "start_date", string.IsNullOrWhiteSpace(activityData.start_date) ? (object)DBNull.Value : activityData.start_date },
                { "end_date", string.IsNullOrWhiteSpace(activityData.end_date) ? (object)DBNull.Value : activityData.end_date }
            };

            // add project months to clear them all in update
            var projStartMonth = Utilities.Models.Global.MonthFromString(project.s_date);
            var projStartYear = Utilities.Models.Global.YearFromString(project.s_date);
            var projEndMonth = Utilities.Models.Global.MonthFromString(project.e_date);
            var projEndYear = Utilities.Models.Global.YearFromString(project.e_date);
            var actStartMonth = Utilities.Models.Global.MonthFromString(activityData.start_date);
            var actStartYear = Utilities.Models.Global.YearFromString(activityData.start_date);
            var actEndMonth = Utilities.Models.Global.MonthFromString(activityData.end_date);
            var actEndYear = Utilities.Models.Global.YearFromString(activityData.end_date);

            var projMonths = Utilities.Models.Global.DifferenceInMonths(projStartMonth, projStartYear, projEndMonth, projEndYear);
            var actMonths = Utilities.Models.Global.DifferenceInMonths(actStartMonth, actStartYear, actEndMonth, actEndYear);
            var spanFromProjStart = Utilities.Models.Global.DifferenceInMonths(projStartMonth, projStartYear, actStartMonth, actStartYear);
            var spanProjStartToActEnd = Utilities.Models.Global.DifferenceInMonths(projStartMonth, projStartYear, actEndMonth, actEndYear);
            var diffToEnd = Utilities.Models.Global.DifferenceInMonths(actEndMonth, actEndYear, projEndMonth, projEndYear);

            if (actMonths <= 0 || spanFromProjStart <= 0 || diffToEnd <= 0)// invalid dates selected; outside of project bounds or start bigger than end
                throw new Exception(LocalResources.InvalidStartAndEndDateSelectedForActivity + activityData.act + ". " + LocalResources.TheActivityMustBePlacedInsideTheBoundsOfTheProjectTimelineAndMustHaveAMonthIntervalOfAtLeastMonth);

            //allowed to send null, but then the activity months will be set to 0 (not the default 1)
            if (string.IsNullOrWhiteSpace(activityData.start_date) || string.IsNullOrWhiteSpace(activityData.end_date))
            {
                actMonths = 0;
                spanFromProjStart = -1;
                spanProjStartToActEnd = -1;
            }

            if (progressValues.Count != actMonths)
                throw new Exception(LocalResources.TheNumberOfProgressDistributionValuesDoesNotMatchTheSelectedTimeIntervalNumberOfMonthsForTheActivity + activityData.act + ".");

            actMonths--; spanFromProjStart--; spanProjStartToActEnd--;
            var currentMonthIndex = 0;
            var valuesIndex = 0;
            for (var i = 0; i < projMonths; i++)
            {
                var currentMonth = projStartMonth - 1 + i;
                var currentMonthString = (currentMonth % 12 + 1).ToString().PadLeft(2, '0');
                var currentYearString = (projStartYear + (currentMonth / 12)).ToString();
                var colName = string.Format("p_{0}_{1}", currentYearString, currentMonthString);

                if (valuesIndex < progressValues.Count && // check if inside array
                    (progressValues[valuesIndex] < 0 || progressValues[valuesIndex] > 100 || //values must be valid percent
                        (valuesIndex > 1 && progressValues[valuesIndex] < progressValues[valuesIndex - 1]))) //if previous value exists, must be lower or equal
                    throw new Exception(string.Format(LocalResources.EncounteredInvalidValuesForProgressDistributionForActivityAtMakeSureTheyAreValidPercentAndInLinearProgress, activityData.act, currentMonthString, currentYearString));

                // if inside activity bounds, update with the values, otherwise put null
                if (currentMonthIndex >= spanFromProjStart && currentMonthIndex <= spanProjStartToActEnd)
                    toUpdate.Add(colName, valuesIndex < progressValues.Count ? progressValues[valuesIndex++] : 0);
                else
                    toUpdate.Add(colName, DBNull.Value);
                currentMonthIndex++;
            }

            // need to solve the bindings
            var query = new Query("planning")
                            .Where("act", activityData.act)
                            .Where("type", activityData.type)
                            .Update(toUpdate);
            var compiled = _compiler.Compile(query);
            ExecuteCommandWithNoResults(compiled.Sql, compiled.Bindings);
        }
        #endregion


        #region Subactivities
        public string GetTableNameSubactivities()
        {
            return "work_def";
        }

        public List<DataKeeper> GetColumnsNecessaryForSubactivities()
        {
            return new DataKeeper[] {
                new DataKeeper("STEP", "Step"),
                new DataKeeper("INSPECTION", "Inspection"),
                new DataKeeper("FORM", "Form"),
                new DataKeeper("S"),
                new DataKeeper("C"),
                new DataKeeper("O"),
                new DataKeeper("PROGR", "PR%"),
                new DataKeeper("LP", "LP%") }.ToList();
        }

        public List<work_def_subact> GetSubActivities(string activityCode)
        {
            var query = new Query("work_def")
                            .Select("act", "sub_act", "act_d", "sub_act_d")
                            .Where("act", activityCode)
                            .OrderBy("sub_act", true)
                            .Distinct();

            var compiled = _compiler.Compile(query);
            return ExecuteQuery<work_def_subact>(compiled.Sql, compiled.Bindings).ToList();
        }

        public List<Web.DataAccess.Entities.ProjectEntities.work_def> GetAllDataForSubActivity(string subActivityCode)
        {
            var query = new Query("work_def")
                            .Where("sub_act", subActivityCode)
                            .Where("progr", ">", 0)
                            .OrderBy("ord");
            var compiled = _compiler.Compile(query);
            return ExecuteQuery<Web.DataAccess.Entities.ProjectEntities.work_def>(compiled.Sql, compiled.Bindings).ToList();
        }

        public string GetSubactivityUnitMeasure(string subActivityCode)
        {
            var query = new Query("work_def").Select("um")
                            .Where("sub_act", subActivityCode).Where("progr", ">", 0)
                            .OrderBy("ord").Limit(1);
            var compiled = _compiler.Compile(query);
            return ExecuteQueryForValue<string>(compiled.Sql, compiled.Bindings);
        }
        #endregion


        #region Progress / Budget
        public void ResetProjectActivities()
        {
            CreateTable("work_disc", true, "SELECT * FROM promisweb.work_disc");
            ClearTableData("budget", false);
        }

        public int GetActivityBudget(string mainActivityCode)
        {
            var valueDb = GetAll<pw_budget>().Where(b => b.act == mainActivityCode).FirstOrDefault();
            return valueDb != null ? valueDb.budget : 0;
        }

        public int GetActivityBudgetPlanned(string mainActivityCode)
        {
            var alterActs = new string[] { "PR", "CO", "SU" };
            var query = new Query("planning")
                               .Sum("mhrs")
                               .Where("type", "P");
            query = alterActs.Contains(mainActivityCode) ? query.WhereContains("act", "_" + mainActivityCode, true) : query.WhereRaw("LEFT(act, 2) = ?", mainActivityCode);

            var compiled = _compiler.Compile(query);
            var valueDb = ExecuteQueryForValue<int?>(compiled.Sql, compiled.Bindings);
            return valueDb ?? 0;
        }

        public List<double> GetActivityProgressArray(string activityCode, string progressType, bool isOverall = false)
        {
            // get results calling the temp sp
            var queryCode = @"CALL SP_PLANNING_PER_ACTIVITY(@project_name_param, @activity_code_param, @activity_flag_param, @is_overall_param)";
            var parameters = new Dictionary<string, object>()
            {
                { "@project_name_param", DbContext.DatabaseName },
                { "@activity_code_param", activityCode },
                { "@activity_flag_param", progressType },
                { "@is_overall_param", (isOverall ? "1" : "0") }
            };
            return ExecuteQuery<double>(queryCode, parameters).ToList();
        }

        public work_dates GetActivityDatesForType(string activityCode, string progressType, bool isOverall = false)
        {
            // get results calling the temp sp
            var queryCode = @"CALL SP_PLANNING_PER_ACTIVITY_DATES(@project_name_param, @activity_code_param, @activity_flag_param, @is_overall_param)";
            var parameters = new Dictionary<string, object>()
            {
                { "@project_name_param", DbContext.DatabaseName },
                { "@activity_code_param", activityCode },
                { "@activity_flag_param", progressType },
                { "@is_overall_param", (isOverall ? "1" : "0") }
            };
            return ExecuteQueryForValue<work_dates>(queryCode, parameters);
        }

        public void StartGettingAllActivityProgressArrays()
        {
            #region Inline SP Code Progress Array
            var spString = @"
                            -- SP THAT GETS PLANNING DATA FOR ACTIVITY (BUDGET / PLANNED). FLAG CAN BE B, P, A, R
                            -- RETURN: MULTIPLE ROWS WITH [DOUBLE] VALUES, ONLY FOR THE ROWS WITH VALUES
                            DROP PROCEDURE IF EXISTS SP_PLANNING_PER_ACTIVITY//
                            CREATE PROCEDURE SP_PLANNING_PER_ACTIVITY(
                                IN PROJECT_NAME VARCHAR(10),
                                IN ACTIVITY_CODE VARCHAR(10),
                                IN FLAG CHAR(1),
                                in OVERALL_FLAG CHAR(1)
                            )
                            BEGIN
                                DECLARE S_DATE VARCHAR(10);
                                DECLARE S_YEAR VARCHAR(4);
                                DECLARE S_MONTH VARCHAR(2);
                                DECLARE E_DATE VARCHAR(10);
                                DECLARE E_YEAR VARCHAR(4);
                                DECLARE E_MONTH VARCHAR(2);
                                DECLARE COLUMN_NAME VARCHAR(64) DEFAULT '';
                                DECLARE CURSOR_FINISHED INTEGER DEFAULT 0;
	                            declare TOTAL_MHRS DOUBLE DEFAULT 1.0;

                                -- CURSOR COLUMN NAMES
                                DECLARE COL_NAMES_PLANNING CURSOR FOR -- CURSOR FOR GOING THROUGH COLUMNS
                                      SELECT `COLUMNS`.COLUMN_NAME
                                      FROM INFORMATION_SCHEMA.COLUMNS
                                      WHERE TABLE_SCHEMA = PROJECT_NAME AND TABLE_NAME = 'PLANNING'
                                      ORDER BY ORDINAL_POSITION;

                                -- THIS HANDLES NOT FOUND ERRORS
                                DECLARE CONTINUE HANDLER FOR NOT FOUND SET CURSOR_FINISHED = 1;

                                -- CREATE THE TEMPORARY TABLE
                                DROP TEMPORARY TABLE IF EXISTS PLANNING_PER_ACTIVITY;
                                CREATE TEMPORARY TABLE PLANNING_PER_ACTIVITY (
                                    T_VALUE DOUBLE
                                )
                                ENGINE=MEMORY;

                                SET @IND = -1;
                                SET @FOUND_LAST = FALSE;
                                SET @CURRENT_VALUE = 0.0;
	                            set TOTAL_MHRS = 1.0;
	                            set @TOTAL_MHRS_EXISTS = 0;
	                            set @ACTUAL_FLAG = FLAG;
	                            if overall_flag = '1' then
		                            if FLAG = 'R' or flag = 'r' or flag = 'A' or flag = 'a' then-- R and A don't have their own mhrs but are based on planning (relative)
			                            set @ACTUAL_FLAG = 'P';
		                            end if;
		                            select COUNT(1) into @TOTAL_MHRS_EXISTS FROM PLANNING P WHERE P.`TYPE` = @ACTUAL_FLAG AND LEFT(P.act, 2) = ACTIVITY_CODE LIMIT 1;
		                            if @TOTAL_MHRS_EXISTS > 0 then
			                            SELECT SUM(P.mhrs) into TOTAL_MHRS FROM PLANNING P WHERE P.`TYPE` = @ACTUAL_FLAG AND LEFT(P.act, 2) = ACTIVITY_CODE LIMIT 1;
		                            end if;
	                            end if;

                                #GO THROUGH COLUMNS
                                OPEN COL_NAMES_PLANNING;

                                FOR_COLS: LOOP
                                    FETCH COL_NAMES_PLANNING INTO COLUMN_NAME; -- GET DATA
                                    IF COLUMN_NAME = 'id' OR @FOUND_LAST = TRUE THEN -- EXIT
                                        LEAVE FOR_COLS;
                                    END IF;
                                    -- START FROM THE 6TH COLUMN
                                    SET @IND = @IND + 1;
                                    IF @IND < 6 THEN
                                        ITERATE FOR_COLS;
                                    END IF;

                                    -- GET COLUMN VALUE
                                    SET @SQL_COL_VAL = '';
                                    IF FLAG = 'A' or FLAG = 'a' then
        	                            if overall_flag = '0' then
             	                            SET @SQL_COL_VAL = CONCAT('SELECT ROUND(MAX(pr), 2) INTO @CURRENT_VALUE FROM PROGRESS WHERE ACT=? AND m=? LIMIT 1');
			                            else
             	                            SET @SQL_COL_VAL = CONCAT('SELECT ROUND(SUM(pr * mhrs) / ', TOTAL_MHRS ,', 2) INTO @CURRENT_VALUE FROM (SELECT MAX(P2.pr) as pr, (select P.mhrs from planning P where P.act = P2.act and P.`type` = \'P\') as mhrs
	FROM progress P2
	WHERE LEFT(P2.act, 2) = ? and P2.m = ? GROUP BY P2.act) AS a');
			                            end if;
		                            else
			                            if overall_flag = '0' then
				                            SET @SQL_COL_VAL = CONCAT('SELECT ', COLUMN_NAME, ' INTO @CURRENT_VALUE FROM PLANNING WHERE ACT=? AND `TYPE`=? LIMIT 1');
			                            else
				                            SET @SQL_COL_VAL = CONCAT('SELECT ROUND(SUM(', COLUMN_NAME, ' * mhrs) / ', TOTAL_MHRS, ', 2) INTO @CURRENT_VALUE FROM PLANNING WHERE LEFT(ACT, 2)=? AND `TYPE`=? LIMIT 1');
			                            end if;
                                    END IF;
                                    PREPARE STMT FROM @SQL_COL_VAL;
                                    SET @TMP_ACT = ACTIVITY_CODE;
    	                            SET @TMP_TYPE = '';
                                    IF FLAG = 'A' or FLAG = 'a' THEN
    		                            SET @TMP_TYPE = COLUMN_NAME;
		                            ELSE
    		                            SET @TMP_TYPE = FLAG;
                                    END IF;
		                            EXECUTE STMT USING @TMP_ACT, @TMP_TYPE;
                                    DEALLOCATE PREPARE STMT;

                                    IF @CURRENT_VALUE IS null or @CURRENT_VALUE = '' THEN
                                        SET @CURRENT_VALUE = 0;
                                        ITERATE FOR_COLS;
                                    END IF;
		                            -- if @CURRENT_VALUE = 100 and @FOUND_LAST = false then
			                        --    set @FOUND_LAST = true;
		                            -- end if;
		
                                    INSERT INTO PLANNING_PER_ACTIVITY VALUES(@CURRENT_VALUE);
                                END LOOP FOR_COLS;
                                CLOSE COL_NAMES_PLANNING;

                                -- SEND DATA TO CLIENT
                                SELECT * FROM PLANNING_PER_ACTIVITY;
                                DROP TEMPORARY TABLE IF EXISTS PLANNING_PER_ACTIVITY;
                            END//";
            #endregion

            #region  Inline SP Code Progress Dates
            var spStringDatesOnly = @"
                                    -- Stores procedure equivalent to the one with progress arrays, but computes the dates for a certain (activity, type) combo depending on how values are assigned in columns on the database
                                    -- Use this when not having start date and end date stored
                                    DROP PROCEDURE IF EXISTS SP_PLANNING_PER_ACTIVITY_DATES//
                                    CREATE PROCEDURE SP_PLANNING_PER_ACTIVITY_DATES(
                                        IN PROJECT_NAME VARCHAR(10),
                                        IN ACTIVITY_CODE VARCHAR(10),
                                        IN FLAG CHAR(1),
                                        in OVERALL_FLAG CHAR(1)
                                    )
                                    BEGIN
                                        DECLARE S_DATE VARCHAR(10);
                                        DECLARE E_DATE VARCHAR(10);

                                        -- TRY PRE-COMPUTE START, END
                                        if OVERALL_FLAG = '0' then
    	                                    if FLAG = 'A' or FLAG = 'a' then -- from progress
    		                                    select min(m), max(m) into S_DATE, E_DATE FROM PROGRESS P 
					                                    WHERE P.act = ACTIVITY_CODE;-- group by progress to remove all of the 100s and keep the first one only
    	                                    else
		                                        SELECT P.START_DATE, P.END_DATE 
		                                                INTO S_DATE, E_DATE FROM PLANNING P
		                                                WHERE P.ACT = ACTIVITY_CODE AND  P.`TYPE` = FLAG LIMIT 1;
    	                                    end if;
	                                    else
		                                    if flag = 'A' or flag = 'a' then -- from progress
    		                                    select min(m), max(m) into S_DATE, E_DATE FROM PROGRESS P 
					                                    WHERE LEFT(P.act, 2) = ACTIVITY_CODE;-- group by progress to remove all of the 100s and keep the first one only
		                                    else
		                                        SELECT MIN(P.START_DATE), MAX(P.END_DATE) 
		                                                INTO S_DATE, E_DATE FROM PLANNING P
		                                                WHERE LEFT(P.ACT, 2) = ACTIVITY_CODE AND  P.`TYPE` = FLAG and P.start_date <> '' and p.end_date <>'' LIMIT 1;
		                                    end if;
	                                    end if;

                                        IF S_DATE is NOT null and s_date <> '' AND E_DATE IS not null and e_date <> '' THEN
	                                        -- SEND DATA TO CLIENT and stop
	                                        set s_date = replace(replace(s_date, 'p_', ''), '_', '-');
		                                    set e_date = replace(replace(e_date, 'p_', ''), '_', '_');
	                                    end if;
	                                    SELECT s_date, e_date; 
                                    END//";
            #endregion

            // create temp SP
            ExecuteSqlScript(spString);
            ExecuteSqlScript(spStringDatesOnly);
        }

        public void FinishGettingAllActivityProgressArrays()
        {
            // drop SP;
            var dropSpCode = @"DROP PROCEDURE IF EXISTS SP_PLANNING_PER_ACTIVITY;";
            var dropSpCodeDatesOnly = @"DROP PROCEDURE IF EXISTS SP_PLANNING_PER_ACTIVITY_DATES;";
            ExecuteCommandWithNoResults(dropSpCode);
            ExecuteCommandWithNoResults(dropSpCodeDatesOnly);
        }
        #endregion

        public bool IsClientHidden()
        {
            try
            {
                var queryClient = new Query("config")
                                            .Select("val")
                                            .Where("code", "clienthide");
                var compiled = _compiler.Compile(queryClient);
                var result = ExecuteQuery<string>(compiled.Sql, compiled.Bindings);
                return result.Contains("Q") ? true : false;
            }
            catch (Exception e)
            {
                return true;
            }
        }

        public string ClientHidden()
        {
            var configExists = DoesTableExist("config");
            if (configExists)
            {
                var queryClient = new Query("config")
                                    .Select("val")
                                    .Where("code", "clienthide");
                var compiled = _compiler.Compile(queryClient);
                return ExecuteQueryForValue<string>(compiled.Sql, compiled.Bindings);
            }
            else
                return "QM";
        }


        #region Project Data
        public List<project> GetAllProjectsForUsername(string username)
        {
            var query = new Query("projects as p")
                            .SelectRaw("p.*")
                            .Join("userfulllist as u", j => j.On("p.proj", "u.proj").Where("u.userid", username));
            var compiled = _compiler.Compile(query);
            return ExecuteQuery<project>(compiled.Sql, compiled.Bindings).ToList();
        }

        public int GetAllProjectsCountForUsername(string username)
        {
            var query = new Query("projects as p")
                            .Join("userfulllist as u", j => j.On("p.proj", "u.proj").Where("u.userid", username))
                            .Count();
            var compiled = _compiler.Compile(query);
            return ExecuteQueryForValue<int>(compiled.Sql, compiled.Bindings);
        }

        public List<module> GetAllModules()
        {
            var modules = GetAll<module>().OrderBy(m => m.pos).ToList();
            return modules;
        }

        public List<subcontractor> GetAllSubcontractors()
        {
            var subs = GetAll<subcontractor>().OrderBy(s => s.subcontractorCol).ToList();
            return subs;
        }


        public void RunProjectUpdatePrerequisites(project project, project oldProject, string type)
        {
            if (type == "logo")//ignore when updating only logo
                return;

            var oldProjStartMonth = Utilities.Models.Global.MonthFromString(oldProject.s_date);
            var oldProjStartYear = Utilities.Models.Global.YearFromString(oldProject.s_date);
            var oldProjEndMonth = Utilities.Models.Global.MonthFromString(oldProject.e_date);
            var oldProjEndYear = Utilities.Models.Global.YearFromString(oldProject.e_date);
            var projStartMonth = Utilities.Models.Global.MonthFromString(project.s_date);
            var projStartYear = Utilities.Models.Global.YearFromString(project.s_date);
            var projEndMonth = Utilities.Models.Global.MonthFromString(project.e_date);
            var projEndYear = Utilities.Models.Global.YearFromString(project.e_date);

            // diff in months is minimum 1 => -1
            var diffNewStartNewEnd = Utilities.Models.Global.DifferenceInMonths(projStartMonth, projStartYear, projEndMonth, projEndYear);
            var diffNewStartToOldStart = Utilities.Models.Global.DifferenceInMonths(projStartMonth, projStartYear, oldProjStartMonth, oldProjStartYear);
            var diffNewEndToOldEnd = Utilities.Models.Global.DifferenceInMonths(projEndMonth, projEndYear, oldProjEndMonth, oldProjEndYear);
            diffNewEndToOldEnd--; diffNewStartNewEnd--; diffNewStartToOldStart--;

            if (diffNewStartNewEnd < 0)//validate dates
                throw new Exception(LocalResources.InvalidStartendDatesEndDateMustBeBiggetThanStartDate);

            // errors can appear when trying to drop columns; so first try dropping
            var columnsToDrop = new List<string>();
            if (diffNewStartToOldStart < 0)//cutting months at the start;
            {
                // must check if one of the cutted columns has values (on any row); if yes => stop
                for (var i = 0; i < Math.Abs(diffNewStartToOldStart); i++)
                {
                    var currentMonth = oldProjStartMonth - 1 + i;
                    var currentMonthString = (currentMonth % 12 + 1).ToString().PadLeft(2, '0');
                    var currentYearString = (oldProjStartYear + (currentMonth / 12)).ToString();
                    var colName = string.Format("p_{0}_{1}", currentYearString, currentMonthString);
                    var sqlQuery = String.Format("SELECT MAX({0}) FROM planning", colName);
                    var value = ExecuteQueryForValue<double?>(sqlQuery);
                    if (value.HasValue)
                        throw new Exception(String.Format(LocalResources.TryingToChangeStartDateFailedTryingToDropMonthForWhichPlanningDataExistsMonthYear, currentMonthString, currentYearString));
                    columnsToDrop.Add(colName);//allgood
                }
            }

            if (diffNewEndToOldEnd > 0)//cutting months fron end
            {
                // must check if one of the cutted columns has values smaller than 100 (on any row); 
                // if min is 100 everywhere or there are no values => can cut
                // NULL or '' as MIN on all of the cutted columns means
                //      - activity already finished OR activity not yet started
                // 100 as MIN on all of the cutted columns means
                //      - activity finished that month (column)
                // if any of the columns has at least one row not 100% or not null / '' => can't cut
                // also must not remove last column with 100

                // check before if column with 100% / 1000% (NULL | '') exists before cutted months (to see if I can cut the first month with 100% or I must leave it)
                var beforeMonth = projEndMonth - 1;
                var beforeMonthString = (beforeMonth % 12 + 1).ToString().PadLeft(2, '0');
                var beforeYearString = (projEndYear + (beforeMonth / 12)).ToString();
                var exColName = string.Format("p_{0}_{1}", beforeYearString, beforeMonthString);
                var exValue = 100.0;//default case for when column does not exist
                try
                {
                    var exQuery = String.Format("SELECT MIN(CASE {0} WHEN '' THEN 1000 WHEN NULL THEN 1000 ELSE CAST({0} as DECIMAL(9, 2)) END) FROM planning", exColName);
                    exValue = ExecuteQueryForValue<double?>(exQuery) ?? 100;
                }
                catch { }//remain silent

                for (var i = 0; i < Math.Abs(diffNewEndToOldEnd); i++)
                {
                    var currentMonth = projEndMonth + i;
                    var currentMonthString = (currentMonth % 12 + 1).ToString().PadLeft(2, '0');
                    var currentYearString = (projEndYear + (currentMonth / 12)).ToString();
                    var colName = string.Format("p_{0}_{1}", currentYearString, currentMonthString);

                    // get the value
                    var sqlQuery = String.Format("SELECT MIN(CASE {0} WHEN '' THEN 1000 WHEN NULL THEN 1000 ELSE CAST({0} as DECIMAL(9, 2)) END) FROM planning", colName);
                    var value = ExecuteQueryForValue<double?>(sqlQuery);

                    // throw error if one of the columns has activities not finished
                    if (value.HasValue && value.Value < 100)
                        throw new Exception(String.Format(LocalResources.TryingToChangeEndDateFailedTryingToDropMonthForWhichPlanningDataExistsMonthYear, currentMonthString, currentYearString));

                    // if this is the first one ever completed everywhere must not be dropped
                    if ((exValue != 100 && exValue != 1000) && value.HasValue && value.Value == 100)
                        throw new Exception(String.Format(LocalResources.TryingToChangeEndDateFailedTryingToDropMonthForWhichPlanningDataExistsMonthYear, currentMonthString, currentYearString));

                    columnsToDrop.Add(colName);//allgood
                }
            }

            foreach (var colName in columnsToDrop)//execute all drops; good from now on
            {
                DropColumn("planning", colName);
            }

            // handle start change - here no STOP errors can appear; only need  to add columns and eventually copy data
            if (diffNewStartToOldStart > 0) //adding months at the start;
            {
                var oldMonth = "end_date";
                //must add all new columns to planning table, first one after 'end_date', then one after another
                for (var i = 0; i < Math.Abs(diffNewStartToOldStart); i++)
                {
                    var currentMonth = projStartMonth - 1 + i;
                    var currentMonthString = (currentMonth % 12 + 1).ToString().PadLeft(2, '0');
                    var currentYearString = (projStartYear + (currentMonth / 12)).ToString();
                    var colName = string.Format("p_{0}_{1}", currentYearString, currentMonthString);

                    // execute the column add
                    AddColumnAfterColumn("planning", colName, "VARCHAR(10)", oldMonth);
                    oldMonth = colName;
                }
            }

            if (diffNewEndToOldEnd < 0) //adding months at the end;
            {
                var beforeMonth = oldProjEndMonth - 1;
                var beforeMonthString = (beforeMonth % 12 + 1).ToString().PadLeft(2, '0');
                var beforeYearString = (oldProjEndYear + (beforeMonth / 12)).ToString();
                var oldMonth = string.Format("p_{0}_{1}", beforeYearString, beforeMonthString);
                //must add all new columns to planning table, first one after the column representing the last month of the project, then one after another
                for (var i = 0; i < Math.Abs(diffNewEndToOldEnd); i++)
                {
                    var currentMonth = oldProjEndMonth + i;
                    var currentMonthString = (currentMonth % 12 + 1).ToString().PadLeft(2, '0');
                    var currentYearString = (oldProjEndYear + (currentMonth / 12)).ToString();
                    var colName = string.Format("p_{0}_{1}", currentYearString, currentMonthString);
                    // add column
                    AddColumnAfterColumn("planning", colName, "VARCHAR(10)", oldMonth);
                    oldMonth = colName;
                }
            }
        }

        public void UpdateProjectSelectedDetails(project project, string type)
        {
            var excludedFields = new List<string>();
            switch (type)
            {
                case "general":
                    excludedFields = new List<string>() { "proj", "descr", "client", "logo" };
                    break;
                case "logo":
                    excludedFields = project.GetType().GetProperties().Where(p => p.Name != "logo").Select(p => p.Name).ToList();
                    break;
            }

            UpdateById(project, "proj", project.proj, excludedFields);
        }
        #endregion
    }
}
