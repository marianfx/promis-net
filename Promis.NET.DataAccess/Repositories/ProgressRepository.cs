﻿using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using SqlKata;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Promis.NET.DataAccess.Repositories
{
    public class ProgressRepository : Repository, IProgressRepository
    {
        public ProgressRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public List<work_def_budget> GetQuantitiesData(string activity)
        {
            var query = new Query("work_def as A")
                                .Join("budget_subact as B", "A.sub_act", "B.sub_act")
                                .Join("planning as P", j => j.On("A.act", "P.act").Where("P.type", "P").Where("P.mhrs", ">", 0))
                                .Select("A.act", "A.act_d", "P.mhrs", "A.sub_act", "A.sub_act_d", "A.UM", "B.qty", "B.mhrs AS sub_mhrs")
                                .WhereStarts("A.sub_act", activity)
                                .GroupBy("A.sub_act");
            var compiled = _compiler.Compile(query);
            return ExecuteQuery<work_def_budget>(compiled.Sql, compiled.Bindings).ToList();
        }

        public double GetSumQuantityForSubactivityFromTables(IEnumerable<string> tables, string subActivity)
        {
            var innerQuery = "(";
            for (var i = 0; i < tables.Count(); i++)
            {
                var thisQuery = (i >= 1 ? " UNION ALL " : "") + " SELECT qty FROM " + tables.ElementAt(i) + " WHERE activity = @activity";
                innerQuery += thisQuery;
            }
            innerQuery += ") AS A";
            var outerQuery = "SELECT SUM(qty) FROM " + innerQuery;
            var parameters = new Dictionary<string, object>() { { "@activity", subActivity } };
            var value = ExecuteQueryForValue<double?>(outerQuery, parameters);
            return value ?? 0;
        }


        public string GetProgressTableName(string projectDatabase, string activityCode, string year, string week)
        {
            if (string.IsNullOrWhiteSpace(projectDatabase) || string.IsNullOrWhiteSpace(activityCode) || string.IsNullOrWhiteSpace(year) || string.IsNullOrWhiteSpace(week))
                return "progress";

            return string.Format("{0}_progress.{1}_{2}_{3}", projectDatabase, activityCode, year, week);
        }

        public void CreateProgressTableForWeek(string projectDatabase, string activityCode, string year, string week)
        {
            if (string.IsNullOrWhiteSpace(projectDatabase) || string.IsNullOrWhiteSpace(activityCode) || string.IsNullOrWhiteSpace(year) || string.IsNullOrWhiteSpace(week))
                return;

            string fullTableName = string.Format("{0}_progress.{1}_{2}_{3}", projectDatabase, activityCode, year, week);
            var columns = new Dictionary<string, string>()
            {
                { "tag", "varchar(100)"},
                { "wbs", "varchar(20)"},
                { "s1", "varchar(6)"},
                { "s2", "varchar(6)"},
                { "s3", "varchar(6)"},
                { "s4", "varchar(6)"},
                { "s5", "varchar(6)"},
                { "s6", "varchar(6)"},
                { "s7", "varchar(6)"},
                { "s8", "varchar(6)"},
                { "s9", "varchar(6)"},
                { "s10", "varchar(6)"},
                { "s11", "varchar(6)"},
                { "s12", "varchar(6)"},
                { "pr", "float(6, 2)"},
                { "sub_act", "varchar(15)"},
                { "mhrs", "varchar(10)"},
                { "qty", "varchar(10)"},
                { "datatable", "varchar(30)"},
                { "id", "bigint(20) NOT NULL AUTO_INCREMENT"},
                { "PRIMARY KEY (`id`)", ""},
                { "KEY `tag` (`tag`)", ""},
                { "KEY `sub_act` (`sub_act`)", ""},
                { "KEY `datatable` (`datatable`)", ""},
            };
            CreateTable(fullTableName, columns);
        }

        public void CopyDataFromRealTablesToProgress(string projectDatabase, string mainActivityCode, string year, string week, string subActivity, IEnumerable<string> tables)
        {
            if (string.IsNullOrWhiteSpace(projectDatabase) || string.IsNullOrWhiteSpace(mainActivityCode) || string.IsNullOrWhiteSpace(year) || string.IsNullOrWhiteSpace(week))
                return;

            string fullTableName = string.Format("{0}_progress.{1}_{2}_{3}", projectDatabase, mainActivityCode, year, week);
            foreach (var table in tables)
            {
                var selectQuery = new Query(table + " AS A")
                                    .Select("A.tag", "A.wbs", "A.activity")
                                    .SelectRaw("?, SUM(REPLACE(A.qty, ',', '.'))", table)
                                    .SelectRaw("SUM(REPLACE(A.qty, ',', '.')) * (SELECT BS.mhrs / BS.qty FROM BUDGET_SUBACT BS WHERE BS.sub_act = ?)", subActivity)
                                    .LeftJoin(fullTableName + " AS B", j => j.On("A.tag", "B.tag").On("A.activity", "B.sub_act"))
                                     .Where("A.activity", subActivity)
                                     .WhereNull("B.id")
                                     .GroupBy("A.tag");
                var insertCommand = new Query(fullTableName)
                                        .Insert(new string[] { "tag", "wbs", "sub_act", "datatable", "qty", "mhrs" }, selectQuery);
                var compiled = _compiler.Compile(insertCommand);
                ExecuteCommandWithNoResults(compiled.Sql, compiled.Bindings);

                var deleteCommand = string.Format(@"DELETE A FROM {0} AS A 
                                                        LEFT JOIN {1} AS B
                                                            ON A.tag = B.tag AND A.sub_act = B.activity
                                                    WHERE A.sub_act = @p0 AND A.datatable = @p1 AND B.id IS NULL", fullTableName, table);
                var parameters = new Dictionary<string, object>()
                {
                    { "@p0", subActivity },
                    { "@p1", table }
                };
                ExecuteCommandWithNoResults(deleteCommand, parameters);
            }
        }



        public PercentData GetTrendsDataForSubActivity(string projectSchema, string progressTable, string mainActivity, string subActivity, double totalMhrs, string week, string year, IEnumerable<string> allSubactivities = null, double totalMhrsFull = 0)
        {
            var output = new PercentData();
            var mhrsUsed = allSubactivities != null ? totalMhrsFull : totalMhrs;
            var conditionQuery = new Query().Where("sub_act", subActivity);

            if(allSubactivities != null)
            {
                conditionQuery = new Query().Where("id", ">", 0);
                var subConditionalQuery = new Query();
                foreach (var subactivity in allSubactivities)
                {
                    subConditionalQuery = subConditionalQuery.OrWhere("sub_act", subactivity);
                }
                conditionQuery = conditionQuery.Where(q => subConditionalQuery);
            }

            // get percent today
            var query = new Query(progressTable)
                            .SelectRaw(string.Format("ROUND(SUM(pr * mhrs / 100) / {0} * 100, 2)", mhrsUsed))
                            .Where(q => conditionQuery);
            var compiled = _compiler.Compile(query);
            output.Percent = ExecuteQueryForValue<double?>(compiled.Sql, compiled.Bindings) ?? 0;

            // get month and week
            var altQuery = new Query("cut_off")
                                .Where("w", "<=", year + "_" + week)
                                .OrderBy("w", false)
                                .Limit(6);
            compiled = _compiler.Compile(altQuery);
            var items = ExecuteQuery<cut_offf>(compiled.Sql, compiled.Bindings).ToList();
            if (items == null || items.Count == 0)//cannot continue
                return output;

            var monthTableCode = items[0].m;//p_2015_05
            var weekTableCode = items[0].w;//2015_05
            var weekLastReadTableCode = items[0].w;
            foreach (var item in items)
            {
                if (item.m != monthTableCode)
                    weekLastReadTableCode = item.w;
            }

            // get percent week
            query = new Query(projectSchema + "_progress." + mainActivity + "_" + weekTableCode)
                            .SelectRaw(string.Format("ROUND(SUM(pr * mhrs / 100) / {0} * 100, 2)", mhrsUsed))
                            .Where(q => conditionQuery);
            compiled = _compiler.Compile(query);
            output.PercentWeekly = ExecuteQueryForValue<double?>(compiled.Sql, compiled.Bindings) ?? 0;

            // get percent month
            query = new Query(projectSchema + "_progress." + mainActivity + "_" + weekLastReadTableCode)
                            .SelectRaw(string.Format("ROUND(SUM(pr * mhrs / 100) / {0} * 100, 2)", mhrsUsed))
                            .Where(q => conditionQuery);
            compiled = _compiler.Compile(query);
            output.PercentMonthly = ExecuteQueryForValue<double?>(compiled.Sql, compiled.Bindings) ?? 0;

            return output;
        }

        public void UpdateSubActivityProgressColumn(string progressTable, string subActivity, string tag, string columnS, string valueS)
        {
            var updated = new Dictionary<string, object>(){ { columnS, valueS } };
            var query = new Query(progressTable)
                            .Update(updated)
                            .Where("sub_act", subActivity)
                            .Where("tag", tag);
            var compiled = _compiler.Compile(query);
            ExecuteCommandWithNoResults(compiled.Sql, compiled.Bindings);
        }

        public double UpdateSubActivityProgressPercent(string progressTable, string subActivity, IEnumerable<work_def> allSubactivities, string tag)
        {
            var conditions = new Query().Where("sub_act", subActivity).Where("tag", tag);
            // pre-compute percent
            var percent = 0.0;
            var rowQuery = new Query(progressTable).Where(q => conditions).Limit(1);
            var compiled = _compiler.Compile(rowQuery);
            var subprogress = ExecuteQueryForValue<sub_progress>(compiled.Sql, compiled.Bindings);
            
            foreach (var sub in allSubactivities)
            {
                double.TryParse(sub.PROGR, NumberStyles.Float, CultureInfo.InvariantCulture, out double workDefProgress);

                var propertyName = "s" + sub.POS;
                var propInfo = subprogress.GetType().GetProperties().Where(p => p.Name == propertyName).FirstOrDefault();
                if (propInfo == null)
                    continue;
                var propValue = propInfo.GetValue(subprogress)?.ToString() ?? "0.0";
                propValue = propValue.Replace(",", ".");
                double.TryParse(propValue, NumberStyles.Float, CultureInfo.InvariantCulture, out double sColumnProgress);

                percent += workDefProgress * sColumnProgress / 100;
            }


            // run update
            var updated = new Dictionary<string, object>(){ { "pr", percent } };
            var query = new Query(progressTable)
                            .Update(updated)
                            .Where(q => conditions);
            compiled = _compiler.Compile(query);
            ExecuteCommandWithNoResults(compiled.Sql, compiled.Bindings);
            return percent;
        }

        public Tuple<string, string> GetSubactivityData(string subAct)
        {
            var query = new Query("work_def")
                            .Select("sub_act_d")
                            .Where("sub_act", subAct)
                            .Limit(1);
            var comp = _compiler.Compile(query);
            var description = ExecuteQueryForValue<string>(comp.Sql, comp.Bindings);
            return new Tuple<string, string>(subAct, description ?? "");
        }

        public List<progress_planning> GetProgressAllData(string table, string sub_act)
        {
            var qtyTag = new Query(table)
                       .SelectRaw("*, round(qty*pr/100,2) as equiv")
                       .Where("sub_act", sub_act)
                       .GroupBy("tag")
                       .OrderBy("tag");

            var compiled = _compiler.Compile(qtyTag);
            return ExecuteQuery<progress_planning>(compiled.Sql, compiled.Bindings).ToList();
        }

        public double GetTotalBudget(string subact)
        {
            var totalBudget = new Query("budget_subact")
                        .SelectRaw("sum(replace(qty, ',', '.'))")
                       .Where("sub_act", subact)
                       .Where("qty", ">", "0");
            var compiled = _compiler.Compile(totalBudget);
            return ExecuteQueryForValue<double>(compiled.Sql, compiled.Bindings);
        }
    }
}
