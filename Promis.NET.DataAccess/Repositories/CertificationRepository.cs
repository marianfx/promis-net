﻿using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Entities;
using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using Promis.NET.Utilities.Models;
using SqlKata;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;

namespace Promis.NET.DataAccess.Repositories
{
    public class CertificationRepository : Repository, ICertificationRepository
    {
        public CertificationRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        #region Tags Assignment Status
        public List<work_disc_partial> GetDataTablesForStatusTab()
        {
            var selQuery = new Query("work_disc")
                             .Select("cod", "descr")
                             .WhereRaw("flag <>''")
                             .WhereRaw("ord <= 16")
                             .OrderBy("ord");

            var queryString = _compiler.Compile(selQuery);
            return ExecuteQuery<work_disc_partial>(queryString.Sql, queryString.Bindings).ToList();
        }

        public List<tabletab_def_base> GetTablesForTag(string code)
        {
            var queryString = new Query("promisweb.tabletag_def")
                              .Select("datatable", "description")
                              .Distinct()
                              .Where("cod", "=", code);
            var compiledQuery = _compiler.Compile(queryString);
            return ExecuteQuery<tabletab_def_base>(compiledQuery.Sql, compiledQuery.Bindings).ToList();
        }

        public KeyValuePair<double, double> GetDataTableCount(string dataTable)
        {
            var selQueryCountNotEq = new Query(dataTable)
                                .Count()
                                .WhereRaw("activity <>''");

            var selQueryCountEq = new Query(dataTable)
                                        .Count()
                                        .WhereRaw("activity='' and qty >0");

            var queryStringNotEq = _compiler.Compile(selQueryCountNotEq);
            var queryStringEq = _compiler.Compile(selQueryCountEq);

            var countNotEqual = ExecuteQueryForValue<double?>(queryStringNotEq.Sql, queryStringNotEq.Bindings);
            var countEqual = ExecuteQueryForValue<double?>(queryStringEq.Sql, queryStringEq.Bindings);
            return new KeyValuePair<double, double>(countNotEqual ?? 0, countEqual ?? 0);
        }

        public List<DataKeeper> GetColumnsForTablesWithTagsNotAssigned()
        {
            return new DataKeeper[] {
                new DataKeeper("REFERENCE"),
                new DataKeeper("TAG"),
                new DataKeeper("DESCRIPTION"),
                new DataKeeper("TYPE"),
                new DataKeeper("UM"),
                new DataKeeper("QTY")}.ToList();
        }

        public List<List<DataKeeper>> GetRowsForTagsNotAssigned(string datatable, string description)
        {
            var allRows = new List<List<DataKeeper>>();
            var queryString = new Query(datatable)
                           .SelectRaw("'" + description + "'" + " as reference, tag, description, type, um, qty, id")
                           .Distinct()
                           .Where("qty", ">", 0)
                           .WhereRaw("activity=''");
            var compiledQuery = _compiler.Compile(queryString);
            var tagsList = ExecuteQuery<certification_tags_info>(compiledQuery.Sql, compiledQuery.Bindings);

            if (tagsList == null)
                return allRows;

            foreach (var tagVal in tagsList)
            {
                var currentRow = new List<DataKeeper>
                {
                    new DataKeeper("reference", tagVal.reference),
                    new DataKeeper("tag", tagVal.tag),
                    new DataKeeper("description", tagVal.descr),
                    new DataKeeper("type", tagVal.type),
                    new DataKeeper("um", tagVal.um),
                    new DataKeeper("qty", tagVal.qty)
                };
                allRows.Add(currentRow);
            }
            return allRows;
        }
        #endregion Tags Assignment Status

        #region Assign SOW to SubContractors
        public List<subc_sow> GetScopeOfWorkForAllSubContractors()
        {
            var queryString = new Query("subc_sow")
                            .Select("code", "sub_act", "id");
            var compiledQuery = _compiler.Compile(queryString);
            return ExecuteQuery<subc_sow>(compiledQuery.Sql, compiledQuery.Bindings).ToList();
        }

        public Dictionary<string, double> GetMainActivityBudget()
        {
            var budgetsForMainAct = new Dictionary<string, double>();
            var selQuery = new Query("work_disc")
                            .SelectRaw("work_disc.cod, work_disc.descr, budget.budget as budget,(SELECT sum(b.budget) FROM work_disc as a, budget as b WHERE a.cod=b.act AND flag<>'') as total_mhrs")
                            .Join("budget", j => j.On("work_disc.cod", "budget.act"))
                            .WhereRaw("work_disc.flag <>''")
                            .OrderBy("work_disc.ord");
            var queryString = _compiler.Compile(selQuery);
            var result = ExecuteQuery<main_activity_budget>(queryString.Sql, queryString.Bindings);

            if (result == null)
                return budgetsForMainAct;

            foreach (var mainActBudget in result)
            {
                var description = mainActBudget.descr;
                var total_mhrs = mainActBudget.total_mhrs ?? 0;
                var mhrsPerActivity = mainActBudget.budget ?? 0;
                var mhrs = Math.Round((mhrsPerActivity / total_mhrs * 100), 2);

                if (!budgetsForMainAct.ContainsKey(description))
                    budgetsForMainAct.Add(description, 0);
                budgetsForMainAct[description] = mhrs;
            }
            return budgetsForMainAct;
        }

        public List<subcontractors_sow> GetSubActivities(string code)
        {
            var query = new Query("work_def as WD")
                       .Select("WD.sub_act", "WD.sub_act_d", "SS.code", "subcontractors.subcontractor", "B.mhrs", "B.prod", "B.um")
                       .SelectRaw("CASE WHEN subcontractors.code IS NULL THEN '' ELSE subcontractors.code END as subcode, '0' as id")
                       .Distinct()
                       .LeftJoin("subc_sow as SS", j => j.On("WD.sub_act", "SS.sub_act"))
                       .LeftJoin("subcontractors", j => j.On("SS.code", "subcontractors.code"))
                       .Join("budget_subact as B", j => j.On("WD.sub_act", "B.sub_act"))
                       .WhereRaw("left(WD.sub_act,2)=?", code)
                       .OrderBy("WD.sub_act");

            var compiledQuery = _compiler.Compile(query);
            return ExecuteQuery<subcontractors_sow>(compiledQuery.Sql, compiledQuery.Bindings).ToList();
        }

        public void DeleteSOWForSubcontractor(string subcontractor, string subActivity)
        {
            var cols = new[] { "code", "sub_act" };
            var values = new[] { subcontractor, subActivity };

            //delete sow
            var queryStringForDelete = new Query("subc_sow")
                                 .Where("code", subcontractor)
                                 .Where("sub_act", subActivity)
                                 .Delete();

            var compiledDelQuery = _compiler.Compile(queryStringForDelete);
            ExecuteCommandWithNoResults(compiledDelQuery.Sql, compiledDelQuery.Bindings);
        }

        public void InsertSOWForSubcontractor(string subcontractor, string subActivity)
        {
            var cols = new[] { "code", "sub_act" };
            var values = new[] { subcontractor, subActivity };

            //insert sow
            var queryStringForInsert = new Query("subc_sow").Insert(cols, values);
            var compiledInsertQuery = _compiler.Compile(queryStringForInsert);
            try
            {
                ExecuteCommandWithNoResults(compiledInsertQuery.Sql, compiledInsertQuery.Bindings);
            }
            catch { }//just in case
        }
        #endregion Assign SOW to SubContractors

        #region Assign Certification Plan to Tags
        public List<tabletab_def_base> GetDatatables(string mainActivity)
        {
            List<tabletab_def_base> result = new List<tabletab_def_base>();
            var tablesQuery = new Query("promisweb.tabletag_def")
                            .Select("datatable", "description")
                            .Where("cod", mainActivity)
                       .OrderBy("cod", true);

            var tablesCompiled = _compiler.Compile(tablesQuery);
            var tablesResults = ExecuteQuery<tabletab_def_base>(tablesCompiled.Sql, tablesCompiled.Bindings).ToList();
            foreach (var table in tablesResults)
            {
                var query = new Query(table.datatable);
                query.Count();
                var compiled = _compiler.Compile(query);
                var countResult = ExecuteQueryForValue<int>(compiled.Sql, compiled.Bindings);
                if (countResult > 0)
                {
                    result.Add(table);
                }
            }
            return result;
        }

        public List<planning_base> GetSubactivityForPlans(string activity)
        {
            var planningQuery = new Query("planning")
                           .Select("act", "description")
                           .WhereLike("act", activity + "%", true)
                           .Where("mhrs", ">", "0")
                           .Where("type", "p")
                           .OrderBy("act");
            var queryCompiled = _compiler.Compile(planningQuery);
            var result = ExecuteQuery<planning_base>(queryCompiled.Sql, queryCompiled.Bindings).ToList();
            return result;
        }

        public List<DataKeeper> GetColumnsForTableForPlanToTags()
        {
            return new DataKeeper[] {
                new DataKeeper(" "),
                new DataKeeper("ACTIVITY","Sub_Act"),
                new DataKeeper("TAG"),
                new DataKeeper("DESCRIPTION"),
                new DataKeeper("QTY"),
                new DataKeeper("UM"),
                new DataKeeper("WBS","AREA"),
                new DataKeeper("DWG","DRAWING") }.ToList();
        }
        #endregion

        #region Certification summary
        public List<tabletab_def_base> GetDatatablesByActivity(string activity)
        {
            List<tabletab_def_base> result = new List<tabletab_def_base>();
            var tablesResults = GetTablesForTag(activity);
            foreach (var table in tablesResults)
            {
                var query = new Query(table.datatable).Count();
                var compiled = _compiler.Compile(query);
                var countResult = ExecuteQueryForValue<int>(compiled.Sql, compiled.Bindings);
                if ( countResult > 0)
                {
                result.Add(table);
                }
            }
            return result;
        }

        public List<work_def_sub_act> GetAllForInspectionStatus(string fullTableName, string tag)
        {
            var query = new Query(fullTableName + " AS A")
                .Select("B.inspection AS inspection","A.s AS SColumn","s_user", "A.c AS code", "c_user","A.o AS OColumn","o_user", "filename", "B.ord AS ord")
                .Join("work_def AS B", j => j.On("A.subact", "B.sub_act").On("B.ord", "A.step"))
                .Where("tag", tag)
                .Where("B.c","<>","")
                .GroupBy("ord");
            var compile = _compiler.Compile(query);
            return ExecuteQuery<work_def_sub_act>(compile.Sql, compile.Bindings).ToList();
        }

        public List<string> GetUsers(string projectID, string activityCode)
        {
            var result = new List<string>();
            var query = new Query("promisweb.userfulllist")
                .Select("Name", "Surname")
                .Where("resp", 'C')
                .Where("proj", projectID)
                .WhereLike("activities", "%"+activityCode+"%");
            var compile = _compiler.Compile(query);
            var queryResult = ExecuteQuery(compile.Sql, compile.Bindings);
            foreach (DataRow item in queryResult.Rows)
            {
                result.Add((item[0].ToString().Substring(0,1).ToUpper()+"."+item[1].ToString()));
            }
            return result;
        }

        public string GetTableOfActivityWithAtleastOneRow(string activity)
        {
            var tablesResults = GetTablesForTag(activity.Substring(0,1));
            foreach (var table in tablesResults)
            {
                var query = new Query(table.datatable)
                    .Count()
                    .Where("activity", activity);
                var compile = _compiler.Compile(query);
                var count = ExecuteQueryForValue<int>(compile.Sql, compile.Bindings);
                if (count > 0)
                    return table.datatable;
            }
            return string.Empty;
        }

        public List<work_def_sub_act> GetIdAndPosFromCerttable(string table,string tag, string ord)
        {
            var query = new Query("cert_" + table + " AS A")
                .Select("A.id", "B.pos")
                .Join("work_def AS B", j => j.On("A.subact", "B.sub_act").On("B.ord", "A.step"))
                .Where("tag", tag)
                .Where("B.ord", ord)
                .GroupBy("ord").OrderBy("ord");
            var compile = _compiler.Compile(query);
            return ExecuteQuery<work_def_sub_act>(compile.Sql, compile.Bindings).ToList();
        }

        public void UpdateCertTableByUser(Dictionary<string, object> inputs, string table,string tag,string act, string step)
        {
            var update = new Query(table)
                .Update(inputs)
                .Where("tag", tag)
                .Where("subact", act)
                .Where("step", step);
            var compile = _compiler.Compile(update);
            ExecuteCommandWithNoResults(compile.Sql, compile.Bindings);
        }

        public void UpdateProgressTable( string table, string pos, string act, string tag)
        {
            var input = new Dictionary<string, object>();
            input.Add("s" + pos, "100");
            var update = new Query(table)
                .Update(input)
                .Where("tag", tag)
                .Where("sub_act", act);
            var compile = _compiler.Compile(update);
            ExecuteCommandWithNoResults(compile.Sql, compile.Bindings);
        }

        public List<work_def> GetPosAndPRogr(string act)
        {
            var query = new Query("work_def")
                .Where("sub_act", act)
                .Where("progr", ">", "0").OrderBy("ord");
            var compile = _compiler.Compile(query);
            return ExecuteQuery<work_def>(compile.Sql, compile.Bindings).ToList();
        }

        public DataTable GetProgressTableRows(string table,string tag, string act)
        {
            var query = new Query(table)
                .Where("sub_act", act)
                .Where("tag", tag);
            var compile = _compiler.Compile(query);
            return ExecuteQuery(compile.Sql, compile.Bindings);
        }

        public void UpdatePercentageInProgressTable(string table,string percentage, string tag, string act)
        {
            var input = new Dictionary<string, object>();
            input.Add("pr", percentage);
            var update = new Query(table)
                .Update(input)
                .Where("tag", tag)
                .Where("sub_act", act);
            var compile = _compiler.Compile(update);
            ExecuteCommandWithNoResults(compile.Sql, compile.Bindings);
        }

        public Tuple<string, string> GetSubactivityData(string subAct)
        {
            var query = new Query("work_def")
                            .Select("sub_act_d")
                            .Where("sub_act", subAct)
                            .Limit(1);
            var comp = _compiler.Compile(query);
            var description = ExecuteQueryForValue<string>(comp.Sql, comp.Bindings);
            return new Tuple<string, string>(subAct, description ?? "");
        }

        public string GetMaxOrdForSubact(string subAct)
        {
            var query = new Query("work_def")
                            .Max("ord")
                            .Where("sub_act", subAct);
            var comp = _compiler.Compile(query);
            return ExecuteQueryForValue<string>(comp.Sql, comp.Bindings);
        }

        public List<work_def_sub_act> GetCertificationWorkDefData(string table, string subAct, string tag)
        {
            var query = new Query("work_def as A")
                            .Select("A.inspection", "A.c as code", "A.ord", "B.c_user", "B.c AS CColumn", "B.s_user", "B.s AS SColumn", "B.o_user", "B.o as OColumn", "B.result")
                            .Join("cert_" + table + " AS B", j => j.On("A.sub_act", "B.subact").On("A.ord", "B.step"))
                            .Where("tag", tag)
                            .Where("A.sub_act", subAct)
                            .OrderBy("A.ord");
            var comp = _compiler.Compile(query);
            return ExecuteQuery<work_def_sub_act>(comp.Sql, comp.Bindings).ToList();
        }
        #endregion
    }
}
