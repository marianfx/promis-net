﻿using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Utilities.Models;
using SqlKata;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Promis.NET.DataAccess.Repositories
{
    public class PreservationRepository : Repository, IPreservationRepository
    {
        public PreservationRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        #region All Tabs
        public List<string> GetDatatableNames(bool isFollowUp)
        {
            var datatableList = new List<string>();
            var query = "SELECT DISTINCT datatable FROM pres_code WHERE datatable<>'' ORDER BY datatable";

            if (string.IsNullOrWhiteSpace(query))
                return datatableList;

            var result = ExecuteQuery(query);
            for (int i = 0; i < result.Rows.Count; i++)
            {
                if (!isFollowUp)
                {
                    datatableList.Add(result.Rows[i]["datatable"].ToString());
                    continue;
                }

                var presTable = "pres_" + result.Rows[i]["datatable"].ToString();
                var tableExist = DoesTableExist(presTable);
                if (tableExist)
                {
                    var sqlCount = new Query(presTable).Count();
                    var queryCompiled = _compiler.Compile(sqlCount);
                    var resultCount = ExecuteQuery(queryCompiled.Sql, queryCompiled.Bindings);

                    if (Convert.ToDouble(resultCount.Rows[0][0].ToString()) > 0)
                    {
                        datatableList.Add(result.Rows[i]["datatable"].ToString());
                    }
                }
            }
            return datatableList;
        }

        public Dictionary<string, List<string>> GetDatatableTypecodes(string dataTable)
        {
            var typecodes = new Dictionary<string, List<string>>();
            var query = @"SELECT DISTINCT distinct typecode, b.description
                         FROM pres_code as a, @dataTable_type as b  
                         WHERE a.typecode = b.code AND datatable = @table 
                         ORDER BY typecode";

            query = query.Replace("@dataTable", dataTable);

            var parameters = new Dictionary<string, object>(); 
            parameters.Add("@table", dataTable);

            if (string.IsNullOrWhiteSpace(query))
                return typecodes;

            var result = ExecuteQuery<pres_code>(query, parameters).ToList();
            foreach (var row in result)
            {
                var typecode = row.typecode;
                var description = row.description;

                var queryCount = new Query(dataTable).Count().Where("type", "=", typecode);
                var queryString = _compiler.Compile(queryCount);
                var resultCount = ExecuteQuery(queryString.Sql, queryString.Bindings);

                if (Convert.ToDouble(resultCount.Rows[0][0].ToString()) > 0)
                {
                    if (!typecodes.ContainsKey(typecode))
                        typecodes.Add(typecode, new List<string>());

                    typecodes[typecode].Add(description);
                }

            }
            return typecodes;
        }

        public string GetTableNameTypecodes()
        {
            return "pres_code";
        }
        #endregion All Tabs

        #region plan Overview
        public List<preservation> GetPreservationData(string code)
        {
            var query = @"SELECT DISTINCT jp, description, cast(seq as signed) as seq, activity,frequency 
                            FROM preservation
                            WHERE jp IN ('IN','MP','RE')
                            AND code = @codeT
                            ORDER BY 1, cast(seq as signed)";

            var parameters = new Dictionary<string, object>();
            parameters.Add("@codeT", code);
            return ExecuteQuery<preservation>(query, parameters).ToList();
        }
        #endregion Plan Overview

        #region Job Assignment Tab
        public List<string> GetTagsLocation()
        {
            var locations = new List<string>();
            var query = "SELECT * FROM pres_location ORDER BY code";
            var result = ExecuteQuery(query);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var locationDescription = result.Rows[i]["description"].ToString();
                locations.Add(locationDescription);
            }
            return locations;
        }

        public List<DataKeeper> GetColumnsForAssignmentTagsTable(string dataTable)
        {
            return new DataKeeper[] {
                new DataKeeper(" "),
                new DataKeeper("TAG"),
                new DataKeeper("DESCRIPTION"),
                new DataKeeper("PRES_" + dataTable.ToUpper() + ".CODE","JOB CARD"),
                new DataKeeper("QTY"),
                new DataKeeper("UM"),
                new DataKeeper("WBS"),
                new DataKeeper("SYSTEM") }.ToList();
        }

        public string GetPreservationFrequency(string code)
        {
            var query = @"SELECT DISTINCT frequency FROM preservation WHERE code=@codeT AND JP='MP'";
            var parameters = new Dictionary<string, object>();
            parameters.Add("@codeT", code);
            return ExecuteQueryForValue<string>(query, parameters);
        }

        public void DeleteTags(string datatable, string tag)
        {
            //datatable is already in the form "pres_NameOftableSelected"        
            var query = "DELETE FROM @table WHERE tag = @tagT";
            query = query.Replace("@table", datatable);

            var parameters = new Dictionary<string, object>();
            parameters.Add("@tagT", tag);
            ExecuteCommandWithNoResults(query, parameters: parameters);
        }

        public List<string> GetCutOffW(string date_m, string date_in, string date_re)
        {
            var cutOffWeeks = new List<string>();

            var query = new Query("cut_off");
            query = query.Where("w", ">", date_m);
            if (!String.IsNullOrWhiteSpace(date_in))
                query = query.Where("w", ">", date_in);

            if (!String.IsNullOrWhiteSpace(date_re))
                query = query.Where("w", "<", date_re);

            query = query.OrderBy("w");

            var compiled = _compiler.Compile(query);
            var finalResult = ExecuteQuery(compiled.Sql, compiled.Bindings);

            for (int i = 0; i < finalResult.Rows.Count; i++)
            {
                var week = finalResult.Rows[i]["w"].ToString();
                cutOffWeeks.Add(week);
            }
            return cutOffWeeks;
        }

        public void SaveTagChanges(string dataTable, string preservDate, string jp, string tags, string rowCode)
        {
            if (tags.Count() == 0)
                return;

            if (String.IsNullOrEmpty(rowCode))
                return;

            var selQuery = new Query("preservation");
            selQuery = selQuery.SelectRaw("@code1, @code2, @code3, `seq`, @code4");
            selQuery = selQuery.Where("code", "=", rowCode).Where("jp", "=", jp);

            var insertQuery = new Query("pres_" + dataTable);
            insertQuery = insertQuery.Insert(new string[] { "tag", "code", "jp", "nr", "schedw" }, selQuery);

            var queryString = _compiler.Compile(insertQuery);
            var parameters = queryString.Bindings;
            parameters.Add("@code1", tags);
            parameters.Add("@code2", rowCode);
            parameters.Add("@code3", jp);
            parameters.Add("@code4", preservDate);
            var preservData = ExecuteQuery(queryString.Sql, parameters);
        }
        #endregion Job Assignment Tab

        #region Preservation Follow Up
        public List<DataKeeper> GetColumnsForFollowUpMainTable(string dataTable)
        {
            return new DataKeeper[] {
                new DataKeeper("TAG"),
                new DataKeeper("DESCRIPTION"),
                new DataKeeper("PRES_" + dataTable.ToUpper() + ".CODE","JOB PLAN"),
                new DataKeeper("SYSTEM") }.ToList();
        }

        public List<preservation_week> GetScheduledAndDoneWeek(string dataTable, string tagVal)
        {
            var weekQuery = new Query("pres_" + dataTable)
                            .Distinct()
                            .Select("donew", "schedw", "jp")
                            .Where("tag", "=", tagVal)
                            .WhereIn("jp", new[] { "IN", "RE" })
                            .OrderBy("jp");

            var compiledQuery = _compiler.Compile(weekQuery);
            return ExecuteQuery<preservation_week>(compiledQuery.Sql, compiledQuery.Bindings).ToList();
        }

        public List<preservation_scheduled_week> GetScheduledWeek(string dataTable, string schedVal, string tagVal)
        {
            var weekQueryCount = new Query("pres_" + dataTable)
                            .SelectRaw("Count( Distinct schedw, tag,code)")
                            .Where("schedw", "<", schedVal)
                            .Where("tag", "=", tagVal)
                            .Where("jp", "=", "MP");

            var compiledQueryCount = _compiler.Compile(weekQueryCount);

            var weekQuery = new Query("pres_" + dataTable)
                            .Select(weekQueryCount, "Count")
                            .Select("schedw", "tag", "code")
                            .Distinct()
                            .Where("schedw", "<", schedVal)
                            .Where("tag", "=", tagVal)
                            .Where("jp", "=", "MP")
                            .OrderBy("schedw");

            var compiledQuery = _compiler.Compile(weekQuery);
            return ExecuteQuery<preservation_scheduled_week>(compiledQuery.Sql, compiledQuery.Bindings).ToList();
        }

        public string GetCutOffValue(string yearWeek)
        {
            var cutOffList = new List<string>();
            var cutoffQuery = new Query("cut_off")
                             .Select("cut_off")
                             .Where("w", "<=", yearWeek)
                             .OrderByDesc("w")
                             .Limit(1);

            var compiledQuery = _compiler.Compile(cutoffQuery);
            return ExecuteQueryForValue<string>(compiledQuery.Sql, compiledQuery.Bindings);
        }

        public List<preservation_actions> GetPreservationInfo(string dataTable, string code, string tagVal, string schedVal)
        {
            var weekQuery = new Query("preservation")
                            .Select("preservation.description", "preservation.activity", "preservation.seq", "pres_" + dataTable + ".code", "pres_" + dataTable + ".donew", "pres_" + dataTable + ".schedw", "pres_" + dataTable + ".nr")
                            .Join("pres_" + dataTable, j => j.On("preservation.code", "pres_" + dataTable + ".code").On("preservation.jp", "pres_" + dataTable + ".jp").On("preservation.seq", "pres_" + dataTable + ".nr"))
                            .Where("pres_" + dataTable + ".tag", " = ", tagVal)
                            .Where("preservation.code", "=", code)
                            .Where("pres_" + dataTable + ".schedw", "=", schedVal)
                            .GroupBy("preservation.seq")
                            .OrderByRaw("CAST(preservation.seq as signed)");

            var compiledQuery = _compiler.Compile(weekQuery);
            return ExecuteQuery<preservation_actions>(compiledQuery.Sql, compiledQuery.Bindings).ToList();
        }

        public void SaveActivitiesStatusChanges(string dataTable, string sequence, string donew, string tag, string jpCode, string schedw)
        {
            var data = new Dictionary<string, object>();
            data.Add("donew", string.IsNullOrWhiteSpace(donew) ? (object)DBNull.Value : donew);
            
            var updateQuery = new Query("pres_" + dataTable)
                            .Update(data)
                            .Where("tag", "=", tag)
                            .Where("nr", "=", sequence)
                            .Where("code", "=", jpCode)
                            .Where("schedw", "=", schedw);

            var queryCompiled = _compiler.Compile(updateQuery);
            ExecuteCommandWithNoResults(queryCompiled.Sql, queryCompiled.Bindings);
        }
        #endregion Preservation Follow Up      
    }
}
