﻿using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Models;
using Promis.NET.Utilities.Models;
using SqlKata.Compilers;
using System.Collections.Generic;
using System.Data;

namespace Promis.NET.DataAccess.Repositories.Interfaces
{
    public interface IRepository
    {
        IDbContext DbContext { get; set; }

        // CRUD operations WITH EF
        T GetById<T, I>(string idFieldName, I id, bool leaveConnectionOpen = true) where T : class;
        IEnumerable<T> GetAll<T>(bool leaveConnectionOpen = true) where T : class;
        int CountAll<T>(bool leaveConnectionOpen = true) where T : class;
        T Create<T, I>(T obj, string idFieldName, I idFieldValue, bool saveChanges = false) where T : class;

        /// <summary>
        /// Updates the given entity. Must specify the name of the idField (as in the object property name and table column name) and the value of the id (in order to search to see if it already exists.
        /// Will throw exception if data does not exist. (ObjectNotFoundException)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="I"></typeparam>
        /// <param name="obj"></param>
        /// <param name="idFieldName"></param>
        /// <param name="idFieldValue"></param>
        /// <param name="excludedFields"></param>
        /// <param name="excludedFields"></param>
        /// <param name="saveChanges"></param>
        void UpdateById<T, I>(T obj, string idFieldName, I idFieldValue, List<string> excludedFields = null, IEnumerable<string> includedFields = null, bool saveChanges = false) where T : class;
        void Delete<T, I>(string idFieldName, I idFieldValue, bool saveChanges = false) where T : class;
        void DeleteByConditions(string datatable, string projectId, List<WhereCondition> conditions);


        //table operations
        bool DoesTableExist(string table, bool leaveConnectionOpen = true);
        void DropTable(string table, bool leaveConnectionOpen = true);
        void CreateTable(string table, bool overrideIfExists = false, string asQuery = "", bool leaveconnectionOpen = true);
        void CreateTable(string table, Dictionary<string, string> columns, bool overrideIfExists = false);
        void ClearTableData(string table, bool leaveConnectionOpen = true);
        void AddColumnAfterColumn(string tableName, string colName, string type, string oldColumn);
        void DropColumn(string tableName, string colName);

        /// <summary>
        /// Returns the column name and Type for the given table. It extracts data using MySql's build in SHOW FIELDS FOR. If wanting to use the built-in database metadata combined with system.information, use the other method that gets columns.
        /// </summary>
        /// <param name="datatable">Name of the table</param>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNameAndType(string datatable);

        /// <summary>
        /// Returns a datatable that has all the information (from system.information takes only the column name, the rest of info from tabletag_structure) for the columns for the given table
        /// </summary>
        /// <param name="tableName">Name of the table</param>
        /// <param name="projectId">Project Id</param>
        /// <returns></returns>
        DataTable GetTableColumnsMetadata(string tableName, string projectId);
        
        // non standard queries
        DataRow GetById(string table, string idColumnName, object idValue, bool leaveConnectionOpen = true);
        void UpdateById(string table, List<DataKeeper> columns, string idColumn, object idValue, bool saveChanges = false);


        // dynamic query operations
        IEnumerable<T> ExecuteQuery<T>(string queryString, Dictionary<string, object> parameters = null, bool leaveConnectionOpen = true);
        DataTable ExecuteQuery(string queryString, Dictionary<string, object> parameters = null, bool leaveConnectionOpen = true);
        T ExecuteQueryForValue<T>(string queryString, Dictionary<string, object> parameters = null, bool leaveConnectionOpen = true);
        long ExecuteCommandWithNoResults(string queryString, Dictionary<string, object> parameters = null, bool saveChanges = false);
        void ExecuteSqlScript(string queryString, bool saveChanges = false);


        // utilities
        Compiler GetCompiler();
        void ChangeDatabase(string newDatabase);
        void ChangeDbContext(IDbContext newDbContext);
        void CloseConnection(bool saveChanges = true);
        void FinishTransaction(bool commit = true, bool dispose = true);
    }
}
