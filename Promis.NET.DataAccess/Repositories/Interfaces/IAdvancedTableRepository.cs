﻿using Promis.NET.DataAccess.Models;
using SqlKata;
using System.Collections.Generic;
using System.Data;

namespace Promis.NET.DataAccess.Repositories.Interfaces
{
    public interface IAdvancedTableRepository: IRepository
    {
        string DataTable { get; set; }
        List<string> Columns { get; set; }
        string Search { get; set; }
        List<WhereCondition> Filters { get; set; }
        List<JoinCondition> Joins { get; set; }
        List<string> GroupBy { get; set; }
        bool Distinct { get; set; }

        void Initialize(string dataTable);
        void Initialize(string dataTable, IEnumerable<string> columns);
        void Initialize(string dataTable, IEnumerable<string> columns, IEnumerable<WhereCondition> filters);
        void Initialize(string dataTable, IEnumerable<string> columns, IEnumerable<WhereCondition> filters, IEnumerable<JoinCondition> joins);
        void Initialize(string dataTable, IEnumerable<string> columns, IEnumerable<WhereCondition> filters, bool distinct);
        void Initialize(string dataTable, IEnumerable<string> columns, IEnumerable<WhereCondition> filters, IEnumerable<JoinCondition> joins, bool distinct);
        void Initialize(string dataTable, IEnumerable<string> columns, IEnumerable<WhereCondition> filters, IEnumerable<JoinCondition> joins, bool distinct, string search);
        void Initialize(string dataTable, IEnumerable<string> columns, IEnumerable<WhereCondition> filters, IEnumerable<JoinCondition> joins, IEnumerable<DataTableOrder> orders, bool distinct, string search);
        void SetGroupBy(IEnumerable<string> groups);

        /// <summary>
        /// Returns a list of columns names in the format 'table.column'. Remember: all column names, not the one selected by the user. 
        /// It takes into account the main table and the tables involved in the joins.
        /// </summary>
        /// <returns></returns>
        List<string> GetAllColumnsStandardized();

        /// <summary>
        /// Returns a list of column names in the format 'table.column'. Remember: only the columns specified in the 'Columns' list. If the ID column is not given as column, but it is contained by the table, it is auto-added.
        /// The Columns list can contain two types of columns: one in format 'table.column' which means those columns are part of a join clause, and others with no tabla name in them, simply 'column' which means they are from the main table.
        /// This method returns the full list of columns when the 'Columns' property is empty or null.
        /// </summary>
        /// <returns></returns>
        List<string> GetAllColumnsInvolvedStandardized();

        /// <summary>
        /// Returns the SqlKata object built that when executed counts all the records of the table (ignoring the search string).
        /// </summary>
        /// <returns></returns>
        Query GetCountAllQuery();

        /// <summary>
        /// Counts all the records, ignoring the search string.
        /// </summary>
        /// <param name="leaveConnectionOpen"></param>
        /// <returns></returns>
        long CountAll(bool leaveConnectionOpen = true);

        /// <summary>
        ///  Returns the SqlKata object built that when executed counts all the records of the table (INCLUDING the search string).
        /// </summary>
        /// <returns></returns>
        Query GetCountAllFilteredQuery();

        /// <summary>
        /// Counts all the records, taken into account the search string.
        /// </summary>
        /// <param name="leaveConnectionOpen"></param>
        /// <returns></returns>
        long CountAllFiltered(bool leaveConnectionOpen = true);


        /// <summary>
        /// Builds and returns a query, based on the data initialized, that will return upon execution all the data for this table.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        Query GetTableDataQuery(int? start = null, int? limit = null);

        /// <summary>
        /// Returns, as DataTable object, all the data from a table, according to all the specified filters at the initializations. Use start or limit to specify an offset or the maximum number of rows to load.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="leaveConnectionOpen"></param>
        /// <returns></returns>
        DataTable GetTableData(int? start = null, int? limit = null, bool leaveConnectionOpen = true);
    }
}
