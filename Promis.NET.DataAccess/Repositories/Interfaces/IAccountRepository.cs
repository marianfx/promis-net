﻿using Promis.NET.DataAccess.Identity.MySql;
using System.Collections.Generic;

namespace Promis.NET.DataAccess.Repositories.Interfaces
{
    public interface IAccountRepository: IRepository
    {
        IdentityUser GetUserById(string id);
        IdentityUser GetUserByNameAndProject(string username, string project);
        List<IdentityUser> GetAllNonAdminUsersForProject(string projectId);
        void UpdateBasicUserInfoById(IdentityUser user, bool updateFullDetails = false);
        void DeleteUserById(string userId);
    }
   

}
