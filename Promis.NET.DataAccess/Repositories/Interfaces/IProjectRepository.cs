﻿using Promis.NET.DataAccess.Entities.PromisWeb;
using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using Promis.Web.DataAccess.Entities.PromisWeb;
using System.Collections.Generic;

namespace Promis.NET.DataAccess.Repositories.Interfaces
{
    public enum ActivityOrderType { None, Under16, Over16 };

    public interface IProjectRepository : IRepository
    {
        /// <summary>
        /// Returns just the list of Main Activity codes and their descriptions as a Dictionary from the promisweb table (not the project table).
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> GetMainActivities();

        /// <summary>
        /// Works on project's db. Gets the list of all main activities. Set the flag it you want only activities with a certain flag.
        /// </summary>
        /// <param name="flag">If specified, will return only activities with that flag. If flag is '<>' will return only activities with flag that has a value (not null or empty).</param>
        /// <returns></returns>
        List<work_disc_budget> GetMainActivities(string flag = null, ActivityOrderType orderType = ActivityOrderType.None);

        /// <summary>
        /// Works on main db (promisweb). Given a main activity code, returns all the list of tables (according tu table-tag definitions) that are related to the activity.
        /// </summary>
        /// <param name="mainActivity">The code of the main activity.</param>
        /// <returns></returns>
        List<string> GetMainActivityTables(string mainActivity);

        /// <summary>
        /// Works on project's db. Gets only the list of associated (related) activities with a main activity, without their planning data
        /// </summary>
        /// <param name="mainActivityCode">The code of the main activity to load data for</param>
        /// <returns></returns>
        List<work_def_base> GetAssociatedActivities(string mainActivityCode);

        /// <summary>
        /// Works on project's db. Returns a list with all the related activities for a main activity, combined with some planning details(start date, end date, men hours)
        /// </summary>
        /// <param name="mainActivityCode">The code of the main activity to load data for</param>
        /// <param name="getOnlyBaselineAndPlanned">If true, will return only the entries with type = B or type = P</param>
        /// <returns></returns>
        List<work_planning> GetAssociatedActivitiesWithMainData(string mainActivityCode, bool getOnlyBaselineAndPlanned = true);

        /// <summary>
        /// Checks all the provided subactivities
        /// </summary>
        /// <param name="subactivities"></param>
        /// <param name="tables"></param>
        /// <returns></returns>
        bool CheckIfAnySubactivityHasDataInTables(IEnumerable<work_def_subact> subactivities, IEnumerable<string> tables);

        /// <summary>
        /// Works on project's db. Fiven the data for an activity, finds it in the database and updates its data (mhrs, start + end-date) and then also updates the progress values (first clears the previous existing progress per months, then adds the new ones)
        /// </summary>
        /// <param name="activityData">Contains the basic data for the activity</param>
        /// <param name="progressValue">Contaisn the progress values associated to the months the activity is set to take place</param>
        void UpdateActivityPlanningData(project project, work_planning activityData, List<double> progressValues);



        /// <summary>
        /// Returns the name of the table that contains data for subactivities. Initially it was work_def, but it might change so that's why this method exists.
        /// Does not execute any query to the DB.
        /// </summary>
        /// <returns></returns>
        string GetTableNameSubactivities();

        /// <summary>
        /// Simply return a list of columns that are representative for the table that contains subactivities. Does not execute query on the DB.
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForSubactivities();

        /// <summary>
        /// Works on project's db. Gets a list of activities that are sub-activities for another activity (select sub_act where act = x).
        /// </summary>
        /// <param name="activityCode">The activity related to a main activity to load sub-activity data for. Ex: SP130</param>
        /// <returns></returns>
        List<work_def_subact> GetSubActivities(string activityCode);

        /// <summary>
        /// Returns all the data from the work definition table, for a certain sub-activity.
        /// </summary>
        /// <param name="subActivityCode"></param>
        /// <returns></returns>
        List<Web.DataAccess.Entities.ProjectEntities.work_def> GetAllDataForSubActivity(string subActivityCode);

        /// <summary>
        /// Returns the unit measure for a given subactivity, as specified in the work_def table.
        /// </summary>
        /// <param name="subactivityCode"></param>
        /// <returns></returns>
        string GetSubactivityUnitMeasure(string subActivityCode);



        /// <summary>
        /// Works on project's db. Deletes everything from budget and resets work_disk to default
        /// </summary>
        void ResetProjectActivities();

        /// <summary>
        /// Works on project's db. Returns the budget (from the budget table) for the given activity.
        /// </summary>
        /// <param name="mainActivityCode">The activity code</param>
        /// <returns></returns>
        int GetActivityBudget(string mainActivityCode);

        /// <summary>
        /// Works on project's db. Returns the budget (by counting all related activities from the planning table) for the given activity.
        /// </summary>
        /// <param name="mainActivityCode"></param>
        /// <returns></returns>
        int GetActivityBudgetPlanned(string mainActivityCode);

        /// <summary>
        /// Works on project's db. Make sure to call StartGetting.. method before calling this otherwise it will not work. Returns an array that contains the progress (percent) for the months of the project.
        /// So the first value will correspond to the first month of the project (according to the type; so it cal be the planned, or the actual), and the last value for the last month of the project (which usually is 100%);
        /// </summary>
        /// <param name="activityCode">The code of the activity (usually not Main Activity, but Specific activity)</param>
        /// <param name="progressType">Represents the flag to load the data for. Examples: 'P' for Planned, 'B' for budget.</param>
        /// <param name="isOverall">Set this to true if you want to get the overall progress arrays for a main activity (the provided activity code must be a valid main activity shortcode otherwise will return nothing)</param>
        /// <returns></returns>
        List<double> GetActivityProgressArray(string activityCode, string progressType, bool isOverall = false);

        /// <summary>
        /// Returns the start and end dates for a given (activity, progress type) combination. Use this only when start and end dates cannot be retrieved in other manners (for example when they are not found in the planning table). This computes the start and end dates by going through all the weeks of the project and retrieving progress values.
        /// </summary>
        /// <param name="activityCode"></param>
        /// <param name="progressType"></param>
        /// <param name="isOverall">Set this to true if you want to get the overall progress dates for a main activity (the provided activity code must be a valid main activity shortcode otherwise will return nothing)</param>
        /// <returns></returns>
        work_dates GetActivityDatesForType(string activityCode, string progressType, bool isOverall = false);

        /// <summary>
        /// This method initializes the temporary stored procedure that easily retrieves data required for the progress arrays for an activity. Make sure to call it before any call to the method that returns progress arrays. Also at the end of work, be sure to call Finish
        /// </summary>
        void StartGettingAllActivityProgressArrays();

        /// <summary>
        /// Drops the temporary stored procedure used while retrieving progress arrays. This way the procedure exists only during the lifetime of user interaction
        /// </summary>
        void FinishGettingAllActivityProgressArrays();


        /// <summary>
        /// Returns all the projects that an username is associated to.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        List<project> GetAllProjectsForUsername(string username);

        /// <summary>
        /// Returns projects count associated to an user.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        int GetAllProjectsCountForUsername(string username);

        /// <summary>
        /// Works on main db. Gets a list of all the modules, ordered by their position (from the modules table);
        /// </summary>
        /// <returns></returns>
        List<module> GetAllModules();

        /// <summary>
        /// Works on main db. Gets a list of all the subcontractors, ordered by their subcontractor name value (from the subcontractors table);
        /// </summary>
        /// <returns></returns>
        List<subcontractor> GetAllSubcontractors();


        /// <summary>
        /// Runs the pre-requisite checks (validations and eventual updates of other tables) before updating project details.
        /// </summary>
        /// <param name="project"></param>
        void RunProjectUpdatePrerequisites(project project, project oldProject, string type);

        /// <summary>
        /// Works on main db. Updates only: Start Date, End Date, First Working Day, Cutoff Day, Logo, Location, Geolocation
        /// </summary>
        /// <param name="project"></param>
        void UpdateProjectSelectedDetails(project project, string type);

        bool IsClientHidden();
        /// <summary>
        /// Gets the value for the Client from config Table
        /// </summary>
        /// <returns></returns>
        string ClientHidden();
    }
}
