﻿using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using System.Collections.Generic;
using System.Data;

namespace Promis.NET.DataAccess.Repositories.Interfaces
{
    public interface IDashboardRepository: IRepository
    {
        double? GetActualPercentByActivity(double totalManHours, string code, string year, string week);
        DataTable GetAllPlanningData();
        /// <summary>
        /// Returns the number of all category of punch items.
        /// </summary>
        /// <returns></returns>
        int GetPunchlistCount();

        /// <summary>
        /// Returns the number of punch items that are not DONE or CANC
        /// </summary>
        /// <returns></returns>
        int GetPunchlistOutsCount();

        /// <summary>
        /// Returns all the punchlist data that is DONE or CANC
        /// </summary>
        /// <returns></returns>
        List<punchlist_base> GetAllPunchlistData();
        DataTable GetMainActivities(string year, string month);


        /// <summary>
        /// Returns the number of man hours for the current project. Can return mhrs overall, for an activity or for an sub-activity.
        /// </summary>
        /// <param name="activity">The code identifying the activity / sub-activity. Optional.</param>
        /// <param name="isSubActivity">If having the code for act/subact, this tells the method which type of code is it (true = sub-activity, false = activity, default value)</param>
        /// <returns></returns>
        double GetManHoursForProject(string activity = null, ActivityType type = ActivityType.Main);

        /// <summary>
        /// Returns the percent (mhrs percent) of completed data overall or for a specific activity/sub-activity.
        /// </summary>
        /// <param name="progressIdentifier">This represents, if isSubActivity = false, the column from the planning table for which data is sepected(p_x_y) or the name of the table (progress table) where progress data resides into.</param>
        /// <param name="totalManHours"></param>
        /// <param name="activity"></param>
        /// <param name="isSubActivity">Specify if the data retrieved should be for an activity or for an sub-activity.</param>
        /// <returns></returns>
        double GetManHoursPlanned(string progressIdentifier, double totalManHours, string activity = null);

        double GetMonthActualPercent(double totalManHours, string monthValue, string activity = null);
        PercentData GetTrendsActualData(double totalManHours, string year, string weekNumber, string activity = null);
        PercentData GetTrendsPlannedData(string monthInitial, string weekInitial, string monthMonthly);
        PercentData GetTrendsMonthlyData(double totalManHours, string month, string activity = null);
    }
}
