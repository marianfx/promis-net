﻿using Promis.NET.DataAccess.Models;
using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.PromisWeb;
using System.Collections.Generic;
using System.Data;

namespace Promis.NET.DataAccess.Repositories.Interfaces
{
    public interface IEngineeringRepository : IRepository
    {
        /// <summary>
        /// Returns the menu-items and menu subitems for the Engineering Databases area
        /// </summary>
        /// <returns></returns>
        DataTable GetMenuDataForEngineeringDatabases();
        /// <summary>
        /// Returns the menu-items and menu subitems for the Civil Database area
        /// </summary>
        /// <returns></returns>
        DataTable GetMenuDataForCivilDataBase();

        /// <summary>
        /// Returns the datatable and description from tabletag_def
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> GetTableDataFromTableTagDef(string active = "");

        /// <summary>
        /// Returns the data for Engineering tables
        /// </summary>
        /// <param name="datatable"></param>
        /// <returns></returns>
        DataRow GetDataForEngineeringPage(string datatable);

        /// <summary>
        /// Returns the predefined data for the given table and column
        /// </summary>
        /// <param name="columnName">Nameof the column</param>
        /// <param name="TableName">Name of the table</param>
        /// <returns></returns>
        Dictionary<string, string> GetPredefinedValuesForColumn(string columnName, string TableName, bool isFiltering = false);

        /// <summary>
        /// Returns true if there was no error regarding to the data
        /// </summary>
        /// <param name="datatable">Name of the given table</param>
        /// <param name="projectId">id of the project</param>
        /// <param name="columnsToVerify">the list of columns which are verified</param>
        /// <returns></returns>
        bool VerifyExistenceOfTheRecord(string datatable, string projectId, List<WhereCondition> columnsToVerify);

        /// <summary>
        /// inserts data into the given table
        /// </summary>
        /// <param name="datatable">The name of the given table</param>
        /// <param name="values">The list of DataBaseCell objects</param>
        string InsertData(string datatable, List<DataKeeper> values);

        /// <summary>
        /// Updates the selected record
        /// </summary>
        /// <param name="datatable">The name of the given table</param>
        /// <param name="values">The list of DatabaseCell objects</param>
        /// <param name="id">The id of the chosen row</param>
        void UpdateData(string datatable, List<DataKeeper> values, string id);

        /// <summary>
        /// Executes the operation of Deleting records
        /// </summary>
        /// <param name="datatable">Name of the given table</param>
        /// <param name="id">the id of the chosen row to be deleted</param>
        void DeleteData(string datatable, string id);

        /// <summary>
        /// Returns a list of given table's columns
        /// </summary>
        /// <param name="dataTable">The name of the given table</param>
        /// <returns></returns>
        List<string> GetColumnsFromTable(string dataTable);

        //method for create tags using schema Page
        /// <summary>
        /// Returns the Data for existing tags
        /// </summary>
        /// <returns></returns>
        List<cw_cat> GetDataForTags();
    }
}
