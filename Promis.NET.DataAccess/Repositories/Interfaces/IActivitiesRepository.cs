﻿
using Promis.NET.DataAccess.Entities;
using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using System.Collections.Generic;

namespace Promis.NET.DataAccess.Repositories.Interfaces
{
    public interface IActivitiesRepository : IRepository
    {
        /// <summary>
        /// Returns a list with all the unique testpacks from test_certpack table, alongside count data and wether if activity with progress exists in work_def.
        /// </summary>
        /// <returns></returns>
        List<testpack_essentials> GetAllCertifiedTestPacksWithData();

        #region Hydrotest Packages ISO Assignment
        /// <summary>
        /// Returs the count for Sketches and testPack packages
        /// </summary>
        /// <returns></returns>
        KeyValuePair<double, double> GetSketchesAndTestPackNumber();

        /// <summary>
        /// Gets the geographicalWBS
        /// </summary>
        /// <returns></returns>
        List<area> GetGeographicalWBS();

        /// <summary>
        /// Gets the list of Pipings
        /// </summary>
        /// <param name="isometric">the isometric</param>
        /// <param name="parameterNeeded">The value of column searched</param>
        /// <returns></returns>
        List<string> GetPipingList(string isometric, string parameterNeeded);

        /// <summary>
        /// Returns System info
        /// </summary>
        /// <param name="pc">The Pc</param>
        /// <returns></returns>
        List<descriptor> GetSystemsInfo(string pc);

        /// <summary>
        /// Gets the info about the subsystems
        /// </summary>
        /// <param name="system">The system</param>
        /// <returns></returns>
        List<descriptor> GetSubSystemsInfo(string system);

        /// <summary>
        /// Gets the name of the table for testpackage
        /// </summary>
        /// <returns></returns>
        string GetTableNameTestpackages();

        /// <summary>
        /// Gets the list of columns for testpack table
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForTestPackTable();

        /// <summary>
        ///Gets the name of the table for  sketches table
        /// </summary>
        /// <returns></returns>
        string GetTableNameForIsometrics();

        /// <summary>
        /// Gets the list of columns for sketches table
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForIsometrics();

        /// <summary>
        /// inserts new record into testpack table
        /// </summary>
        /// <param name="code">The code for testpack</param>
        /// <param name="description">The description for the new testpack</param>
        /// <returns></returns>
        bool InsertHydrotestPack(string code, string description);

        /// <summary>
        /// Deletes the selected testpackage
        /// </summary>
        /// <param name="id">The id of the testpackage</param>
        /// <param name="tag">the tag of the selected testpackage</param>
        void DeleteHydrotest(string id, string tag);

        /// <summary>
        /// Get the value for the maintag
        /// </summary>
        /// <param name="selectedIsometricTag">The selected tag from sketches table</param>
        /// <returns></returns>
        /// 
        string GetMaintag(string selectedIsometricTag);

        /// <summary>
        /// Counts the tags from testpack
        /// </summary>
        /// <param name="selectedTag">The selected tag</param>
        /// <param name="maintag">The  miantag</param>
        /// <returns></returns>
        int GetCountTagFromTestPack(string selectedTag, string maintag);
        
        /// <summary>
        /// Deletes records from testpack_link table
        /// </summary>
        /// <param name="selectedTag"></param>
        void DeleteTestPackLink(string selectedTag);

        /// <summary>
        /// Gets the sum for all qty from sketches table
        /// </summary>
        /// <param name="maintag">The maintag</param>
        /// <returns></returns>
        double GetSketchesSum(string maintag);

        /// <summary>
        /// Inserts records into testpacklink table
        /// </summary>
        /// <param name="selectedTag">The selected tag</param>
        /// <param name="maintag">The maintag</param>
        /// <param name="qty">The qty</param>
        /// <param name="system">The system</param>
        /// <param name="subsystem">The subsystem</param>
        void InsertIntoTestpacklink(string selectedTag, string maintag, string qty, string system, string subsystem);
        
        /// <summary>
        /// Updates the testpack table
        /// </summary>
        /// <param name="selectedTag">The selected tag from testpack</param>
        /// <param name="system">The system</param>
        /// <param name="subsystem">The subsystem</param>
        /// <param name="wbs">The wbs</param>
        void UpdateTestPack(string selectedTag, string system, string subsystem, string wbs);
        #endregion


        #region System Punch Lists Management
        /// <summary>
        /// Gets all system codes and theyr description from punchlist
        /// </summary>
        /// <returns></returns>
        List<system_base> GetAllSystemFromPunchlist(bool plOnly);

        /// <summary>
        /// Gests all disciplines
        /// </summary>
        /// <returns></returns>
        List<pl_disci_base> GetAllDisciplines();

        /// <summary>
        /// Gets all Originators
        /// </summary>
        /// <returns></returns>
        List<pl_orig_base> GetAllOriginators();

        /// <summary>
        /// Gets all categoryes
        /// </summary>
        /// <returns></returns>
        List<pl_disci_base> GetAllCategory();

        /// <summary>
        /// Gets all records from ActionsBy
        /// </summary>
        /// <returns></returns>
        List<pl_actionby_base> GetAllActionsBy();

        /// <summary>
        /// Gets all status codes
        /// </summary>
        /// <returns></returns>
        List<string> GetAllStatus();

        /// <summary>
        /// Gets all records from Closer table
        /// </summary>
        /// <returns></returns>
        List<pl_orig_base> GetAllCloser();

        /// <summary>
        /// Gets columns for datatable
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForPunchlistTable();

        /// <summary>
        /// Gets closure field in punchlist table at specified row
        /// </summary>
        /// <param name="rowId">The Id of the specified Row</param>
        /// <returns></returns>
        string GetClosureByRowId(string rowId);

        /// <summary>
        /// Inserts new row in punchlist table
        /// </summary>
        /// <param name="commData">Data to be inserted</param>
        /// <returns></returns>
        void InsertPunchlistRow(Dictionary<string,object> commData);

        /// <summary>
        /// Gets list of tables
        /// </summary>
        /// <returns></returns>
        List<tabletab_def_base> GetDataTablesForModal();

        /// <summary>
        /// Gets columns for table
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForPunchlistModalTable();

        /// <summary>
        /// Gets the number of rows in a table
        /// </summary>
        /// <param name="tablename">The table</param>
        /// <param name="system">The condition</param>
        /// <returns></returns>
        double CountRowsInTableBySystem(string tablename, string system);

        #endregion
    }
}
