﻿
using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.Utilities.Models;
using System.Collections.Generic;
using System.Data;

namespace Promis.NET.DataAccess.Repositories.Interfaces
{
    public interface IPreservationRepository : IRepository
    {
        /// <summary>
        /// Gets the name of the datatables
        /// </summary>
        /// <param name="isFollowUp">Indicates if the follow up tab is selected</param>
        /// <returns>Returns the list of the dataTables</returns>
        List<string> GetDatatableNames(bool isFollowUp);

        /// <summary>
        /// Gets the typecodes
        /// </summary>
        /// <param name="dataTable">The name of the chosen dataTable</param>
        /// <returns>Returns the list of typecodes for the selected datatable</returns>
        Dictionary<string, List<string>> GetDatatableTypecodes(string dataTable);

        /// <summary>
        /// Gets the name of table from where are taken the typecodes
        /// </summary>
        /// <returns>returns the datatable name from which we are taking the typecodes</returns>
        string GetTableNameTypecodes();


        /// <summary>
        /// get all the preservation data for the selection made
        /// </summary>
        /// <param name="code">The code for the chosen preservation</param>
        /// <returns>Returns the list of : descriptions, activities and sequences for the selected preservation</returns>
        List<preservation> GetPreservationData(string code);


        /// <summary>
        /// Gets the locations
        /// </summary>
        /// <returns>Returns the list of locations for the preservation</returns>
        List<string> GetTagsLocation();

        /// <summary>
        /// Gets the columns for the tags table
        /// </summary>
        /// <returns>Returns the needed columns for the tags table</returns>
        List<DataKeeper> GetColumnsForAssignmentTagsTable(string dataTable);

        /// <summary>
        /// Gets the preservation frequency
        /// </summary>
        /// <param name="code">The code of the selected preservation</param>
        /// <returns>Returns the value for the selected preservation's frequency </returns>
        string GetPreservationFrequency(string code);
      
        /// <summary>
        /// Deletes the selected tag
        /// </summary>
        /// <param name="datatable">The name of the selected table</param>
        /// <param name="tag">The selected tag</param>
        void DeleteTags(string datatable, string tag);

        /// <summary>
        /// Gets the cut-off data
        /// </summary>
        /// <param name="date_m">Maintenance date</param>
        /// <param name="date_in">Initialisation date</param>
        /// <param name="date_re">Restoration date</param>
        /// <returns></returns>
        List<string> GetCutOffW(string date_m, string date_in, string date_re);

        /// <summary>
        /// Inserts data for the selected tag
        /// </summary>
        /// <param name="dataTable">The name of the chosen table</param>
        /// <param name="preservDate">The year and week for the chosen preservation date </param>
        /// <param name="jp">the job plan</param>
        /// <param name="tags">The tags</param>
        /// <param name="rowCode">The code of the selected preservation</param>
        void SaveTagChanges(string dataTable, string preservDate, string jp, string tags, string rowCode);


        /// <summary>
        /// Returns the Columns for the table from left side
        /// </summary>
        /// <param name="dataTable">The name of the selected datatable</param>
        /// <returns></returns>
        List<DataKeeper> GetColumnsForFollowUpMainTable(string dataTable);

        /// <summary>
        /// Gets the value for the done week and the scheduled week
        /// </summary>
        /// <param name="dataTable">The dataTable</param>
        /// <param name="tagVal">The tag</param>
        /// <returns></returns>
        List<preservation_week> GetScheduledAndDoneWeek(string dataTable, string tagVal);

        /// <summary>
        /// Gets the List of info for the scheduled week
        /// </summary>
        /// <param name="dataTable">The datatble</param>
        /// <param name="schedVal">The scheduled week</param>
        /// <param name="tagVal">The tag</param>
        /// <returns></returns>
        List<preservation_scheduled_week> GetScheduledWeek(string dataTable, string schedVal, string tagVal);

        /// <summary>
        /// Gets the the value for the cut-off
        /// </summary>
        /// <param name="yearWeek">The year and week</param>
        /// <returns>Returns the value for the cut-off</returns>
        string GetCutOffValue(string yearWeek);

        /// <summary>
        /// Gets the code, description, activity and sequence for the selected preservation and date
        /// </summary>
        /// <param name="dataTable">The datatable chosen</param>
        /// <param name="code">The code for the preservation</param>
        /// <param name="tagVal">The tag</param>
        /// <param name="schedVal">The scheduled Value for week</param>
        /// <returns></returns>
        List<preservation_actions> GetPreservationInfo(string dataTable, string code, string tagVal, string schedVal);

        /// <summary>
        /// Save the changes made for the Activity Status
        /// </summary>
        /// <param name="dataTable">The chosen table</param>
        /// <param name="sequence">The sequence</param>
        /// <param name="donew">The done week</param>
        /// <param name="tag">The tag</param>
        /// <param name="jpCode">The job plan Code</param>
        /// <param name="schedw">The scheduled week</param>
        void SaveActivitiesStatusChanges(string dataTable, string sequence, string donew, string tag, string jpCode, string schedw);


    }

}
