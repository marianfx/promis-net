﻿using Promis.NET.DataAccess.Entities;
using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using System;
using System.Collections.Generic;
using System.Data;

namespace Promis.NET.DataAccess.Repositories.Interfaces
{
    public interface ICommissioningRepository: IRepository
    {
        /// <summary>
        /// Works on project's db. Gets the list of all datatables.
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        List<Tuple<tabletab_def_base, int, int>> GetDatatables(bool isFromSys);

        /// <summary>
        /// Works on project's db. Gets the list of the sub-activities based on the tablename and subactivity provied.
        /// </summary>
        /// <param name="tableName"> The name of the datatable provided.</param>
        /// <param name="activity"> The code of the activity of which the subactivity belongs to.</param>
        /// <returns></returns>
        List<work_def_subact> GetSubactForDatatables(string tableName, string activity);

        /// <summary>
        /// Works on project's db. Gets the list of the typecodes belonging to the tablename provied.
        /// </summary>
        /// <param name="tableName"> The name of the datatable.</param>
        /// <returns></returns>
        List<KeyValuePair<string, string>> GetTypecodesForDatatables(string tableName,bool isWithJoin = false);

        /// <summary>
        /// Simply return a list of columns that are representative for the table that contains Tags. Does not execute query on the DB
        /// </summary>
        /// <param name="mainActivity"> The code of the activity required in column prefix.</param>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForTagsTable(string mainActivity);

        /// <summary>
        /// Works on project's db. Gets the list of tags and activityes belonging to the provided table within the indicators list.
        /// </summary>
        /// <param name="tableName"> The name of the datatable provided.</param>
        /// <param name="contor"> The list of the indicators that determin the range of the table rows.</param>
        /// <returns></returns>
        List<activity_tag_multiple> GetTagAndActivityforDatatables(string prefix,string tableName, List<string> counter);

        /// <summary>
        /// Works on project's db. Creates (insert) rows in the given datatable using the specified codes
        /// </summary>
        /// <param name="datatable"> The name of the datatable provided.</param>
        /// <param name="tag"> Tag code for the inserting row.</param>
        /// <param name="subActivity"> Subactivity code for the inserting row.</param>
        /// <returns></returns>
        void CreateCertTableRow(string datatable, string tag, string subActivity);

        /// <summary>
        /// Works on project's db. Deletes (removes) rows in the given datatable using the specified codes
        /// </summary>
        /// <param name="datatable"> The name of the datatable provided.</param>
        /// <param name="tag"> Tag code for the removed row.</param>
        /// <param name="subActivity"> Subactivity code for the removed row.</param>
        /// <returns></returns>
        void DeleteCertTableRow(string datatable, string tag, string activity);


        /// <summary>
        /// Returns the name and description for Ststus Tab
        /// </summary>
        /// <param name="isCertification">Bool value for is </param>
        /// <returns></returns>
        List<tabletab_def_base> GetDataTablesForStatusTab(bool isCertification = true);

        /// <summary>
        /// Gets the number of tags assigned and not assigned for the given datatable
        /// </summary>
        /// <param name="dataTable">The name of the datatable</param>
        /// <param name="prefix">The prefix for the column</param>
        /// <returns></returns>
        KeyValuePair<double, double>GetDataTableCount(string dataTable, string prefix = null);

        /// <summary>
        /// Gets the list of columns with tags not assigned
        /// </summary>
        /// <param name="dataTable">The datatable name</param>
        /// <returns></returns>
        List<DataKeeper> GetColumnsForTablesWithTagsNotAssigned(string dataTable);

        /// <summary>
        /// Gets the manHours for precommissioning and commissioning
        /// </summary>
        /// <param name="sufix">The sufix ('CO' Or 'PR')</param>
        /// <returns></returns>
        double GetManHoursForProjectStatus(string sufix);

        /// <summary>
        /// Returns the values  for planned line of chart
        /// </summary>
        /// <param name="column">The column</param>
        /// <param name="totalManHours">The manHours value</param>
        /// <param name="sufix">The sufix</param>
        /// <returns></returns>
        double? GetManHoursPlannedForStatus(string column, double totalManHours, string sufix);

        /// <summary>
        /// Returns the sum of earned values 
        /// </summary>
        /// <param name="sufix">The sufix for the table</param>
        /// <param name="month">the month</param>
        /// <param name="totmhrs">The main hours value</param>
        /// <returns></returns>
        double? GetSumEarned(string sufix, string month, double totmhrs);

        //for Loop Tests By System
        /// <summary>
        /// Gets the code and description for the systems
        /// </summary>
        /// <returns></returns>
        List<system_base> GetSystemsForLoop();

        /// <summary>
        /// Gets the list of tags for the selected system
        /// </summary>
        /// <param name="system">The code of the system</param>
        /// <returns></returns>
        List<string> GetTagsForSystem(string system);

        /// <summary>
        /// Gets the Data from instruments loopcheck tables
        /// </summary>
        /// <param name="tag">The chosen tag</param>
        /// <returns></returns>
        List<Instruments_loopcheck> GetAllInstrumentsData(string tag);

        /// <summary>
        /// Gets the sum of progress values
        /// </summary>
        /// <param name="comp">The comp</param>
        /// <returns></returns>
        double GetProgressSum(string comp);

        /// <summary>
        /// Gets the infor from instruments
        /// </summary>
        /// <param name="comp">The comp</param>
        /// <returns></returns>
        cert_instruments_workdef GetInfoForInstruments(string comp);      

        /// <summary>
        /// Gets the value for Next Step column
        /// </summary>
        /// <param name="step">The value of the step</param>
        /// <param name="sub_act">The sub-activity</param>
        /// <returns></returns>
        workdef_step GetNextStepForLoop(string step, string sub_act);

        //System Subdivision

        /// <summary>
        /// Gets the list of columns for the table showing system assigned tables
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForSystemTable();

        /// <summary>
        /// Gets the list of systems
        /// </summary>
        /// <returns></returns>
        List<system_base> GetSystemList();
        
        /// <summary>
        /// Returns the tags assigned number and total tags for the selected table
        /// </summary>
        /// <param name="dataTable">The selected table</param>
        /// <returns></returns>
        KeyValuePair<int, int> GetTagCountsAndPercentageForTable(string dataTable);

        //Inspection Follow Up

        /// <summary>
        /// Works on project's db. Gets the list of the colums belonging to the tablename provied.
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForFollowUpTable();

        /// <summary>
        /// Works on project's db. Gets the tag code and subactivity.
        /// </summary>
        /// <param name="datatable">Provided datatable</param>
        /// <param name="tagId">The identifier of the tag</param>
        /// <param name="activity">provided activity code</param>
        /// <returns></returns>
        activity_tag_multiple GetTagAndSubAct(string datatable, string tagId,string activity);

        /// <summary>
        /// Works on project's db. Gets the description of the provided subactivity code.
        /// </summary>
        /// <param name="subActivity">Provided subactivity code</param>
        /// <returns></returns>
        activity_tag_multiple GetSubactForFollowUpSelectedRow(string subActivity);

        /// <summary>
        /// Works on project's db. Gets the inspection status of the tag.
        /// </summary>
        /// <param name="fullTableName">The table form witch the row is selected above</param>
        /// <param name="tag">The identifier of the row</param>
        /// <returns></returns>
        List<work_def_sub_act> GetAllForInspectionStatus(string fullTableName, string tag);

        /// <summary>
        /// Works on project's db. Saves the added inspections.
        /// </summary>
        /// <param name="datatable">The table that has been inspected</param>
        /// <param name="userName">The person representing the inspector</param>
        /// <param name="tag">The specific tag code that was inspected</param>
        /// <param name="ord">The order of the rows belonging to the tag code that was inspected</param>
        /// <param name="result">The final result of the inspection</param>
        /// <returns></returns>
        void UpdateInspectionsForDatatable(string dataTable, string userName, string tag, string ord, string result);






    }
}
