﻿
using Promis.NET.DataAccess.Entities;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.Utilities.Models;
using System.Collections.Generic;
using System.Data;
using System;

namespace Promis.NET.DataAccess.Repositories.Interfaces
{
    public interface ICertificationRepository : IRepository
    {
        //for Status Tab
        /// <summary>
        /// Gets The list of datatables
        /// </summary>
        /// <returns></returns>
        List<work_disc_partial> GetDataTablesForStatusTab();

        /// <summary>
        /// Gets the Tables for the given tag code
        /// </summary>
        /// <param name="code">The code</param>
        /// <returns></returns>
        List<tabletab_def_base> GetTablesForTag(string code);

        /// <summary>
        /// Gets the value of counted records which have activity column value equl to/different of ""
        /// </summary>
        /// <param name="dataTable">The name of datatable</param>
        /// <returns></returns>
        KeyValuePair<double, double> GetDataTableCount(string dataTable);

        /// <summary>
        /// Returns the list of columns for the table with tags not assigned
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsForTablesWithTagsNotAssigned();

        /// <summary>
        /// Gets the rows for tags not assigned
        /// </summary>
        /// <param name="datatable">The given datatable</param>
        /// <param name="description">The description of the datatable</param>
        /// <returns></returns>
        List<List<DataKeeper>> GetRowsForTagsNotAssigned(string datatable, string description);

        #region Assign Sow To SubContractors
        /// <summary>
        /// Gets the SOW for the existing subcontractors
        /// </summary>
        /// <returns></returns>
        List<subc_sow> GetScopeOfWorkForAllSubContractors();
        
        /// <summary>
        ///  Gets the budget for every main activity
        /// </summary>
        /// <returns></returns>
        Dictionary<string, double> GetMainActivityBudget();

        /// <summary>
        /// Gets The list of subactivities
        /// </summary>
        /// <param name="code">The code of subactivity</param>
        /// <returns></returns>
        List<subcontractors_sow> GetSubActivities(string code);

        /// <summary>
        /// Deletes association subc - subact
        /// </summary>
        /// <param name="subcontractor"></param>
        /// <param name="subActivity"></param>
        void DeleteSOWForSubcontractor(string subcontractor, string subActivity);

        /// <summary>
        /// Inserts the SOW for the chosen subcontractor and subActivity
        /// </summary>
        /// <param name="subcontractor"></param>
        /// <param name="subActivity"></param>
        void InsertSOWForSubcontractor(string subcontractor, string subActivity);
        #endregion


        #region Assign plan to tags
        /// <summary>
        /// Gets a list of datatables with the provided activity code
        /// </summary>
        /// <param name="mainActivity">The provided activity code</param>
        /// <returns></returns>
        List<tabletab_def_base> GetDatatables(string mainActivity);

        /// <summary>
        /// Gets a list of subactivityes with the provided activity code
        /// </summary>
        /// <param name="activity">Provided activity code</param>
        /// <returns></returns>
        List<planning_base> GetSubactivityForPlans(string activity);

        /// <summary>
        /// Get columns list forthe main table
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsForTableForPlanToTags();

        #endregion

        #region Certification Summary
        /// <summary>
        /// Gets list of datatables using provided Activity code
        /// </summary>
        /// <param name="activity">Required activity code</param>
        /// <returns></returns>
        List<tabletab_def_base> GetDatatablesByActivity(string activity);

        /// <summary>
        /// Gets a list of many columns from the provided table identifying them by tag
        /// </summary>
        /// <param name="fullTableName">The name of the table</param>
        /// <param name="tag">The tag code</param>
        /// <returns></returns>
        List<work_def_sub_act> GetAllForInspectionStatus(string fullTableName, string tag);

        /// <summary>
        /// Gets users list
        /// </summary>
        /// <param name="projectID">Only users belonging to this project</param>
        /// <param name="activityCode">Belonging to this Activity</param>
        /// <returns></returns>
        List<string> GetUsers(string projectID, string activityCode);

        /// <summary>
        /// Gets the table name of the table that has values for that activity
        /// </summary>
        /// <param name="activity">The activity code</param>
        /// <returns></returns>
        string GetTableOfActivityWithAtleastOneRow(string activity);

        /// <summary>
        /// Gets id and post of specific table with conditions
        /// </summary>
        /// <param name="table">The specific table</param>
        /// <param name="tag">First Condition </param>
        /// <param name="ord">Second Condition </param>
        /// <returns></returns>
        List<work_def_sub_act> GetIdAndPosFromCerttable(string table, string tag, string ord);

        /// <summary>
        /// Updates specified table with the impusts specific to the provided conditions
        /// </summary>
        /// <param name="inputs">The data to be updated</param>
        /// <param name="table">The table to be updated</param>
        /// <param name="tag">Condition one</param>
        /// <param name="act">Condition two</param>
        /// <param name="step">Condition three</param>
        void UpdateCertTableByUser(Dictionary<string, object> inputs, string table, string tag, string act, string step);

        /// <summary>
        /// Updates Progress Table
        /// </summary>
        /// <param name="table">Table</param>
        /// <param name="pos">Conditions</param>
        /// <param name="act"></param>
        /// <param name="tag"></param>
        void UpdateProgressTable(string table, string pos, string act, string tag);

        /// <summary>
        /// Gets rows from work_def Table
        /// </summary>
        /// <param name="act"></param>
        /// <returns></returns>
        List<work_def> GetPosAndPRogr(string act);

        /// <summary>
        /// Gets rows of the big Progress table
        /// </summary>
        /// <param name="table">Table name</param>
        /// <param name="tag">Tag condition</param>
        /// <param name="act">Activity condition</param>
        /// <returns></returns>
        DataTable GetProgressTableRows(string table, string tag, string act);

        /// <summary>
        /// Updates Progress table with percentage
        /// </summary>
        /// <param name="table"></param>
        /// <param name="percentage"></param>
        /// <param name="tag"></param>
        /// <param name="act"></param>
        void UpdatePercentageInProgressTable(string table, string percentage, string tag, string act);


        /// <summary>
        /// Gets the description for a subactivity.
        /// </summary>
        /// <param name="subAct"></param>
        /// <returns></returns>
        Tuple<string, string> GetSubactivityData(string subAct);
            
        /// <summary>
        /// Returns max(ord) for a given subact.
        /// </summary>
        /// <param name="subAct"></param>
        /// <returns></returns>
        string GetMaxOrdForSubact(string subAct);

        /// <summary>
        /// Returns a list of certification data for a table, subact and tag
        /// </summary>
        /// <param name="table"></param>
        /// <param name="subAct"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        List<work_def_sub_act> GetCertificationWorkDefData(string table, string subAct, string tag);
        #endregion
    }
}
