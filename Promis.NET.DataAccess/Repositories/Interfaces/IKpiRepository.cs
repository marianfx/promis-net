﻿
using Promis.NET.Utilities.Models;
using System.Collections.Generic;
using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using Promis.NET.DataAccess.Entities;

namespace Promis.NET.DataAccess.Repositories.Interfaces
{
    public interface IKpiRepository : IRepository
    {
        /// <summary>
        /// Returns the name of the table that represents the systems.
        /// </summary>
        /// <returns></returns>
        string GetTableNameSystems();

        /// <summary>
        /// Returns the columns to display when displaying the systems table.
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForSystems();


        #region CompletionOverview
        /// <summary>
        /// Returns the total Men Hours planned for construction phase.
        /// </summary>
        /// <param name="yearCode"></param>
        /// <param name="weekCode"></param>
        /// <param name="system">Can be null. Queries act according to this if it is null or not.</param>
        /// <returns></returns>
        double GetTotalMhrsConstruction(string yearCode, string weekCode, string system = null);

        /// <summary>
        /// Returns the total Men Hours planned for precommissioning phase.
        /// </summary>
        /// <param name="yearCode"></param>
        /// <param name="weekCode"></param>
        /// <param name="system">Can be null. Queries act according to this if it is null or not.</param>
        /// <returns></returns>
        double GetTotalMhrsPrecommissioning(string yearCode, string weekCode, string system = null);

        /// <summary>
        /// Returns the total Men Hours planned for commissioning phase.
        /// </summary>
        /// <param name="yearCode"></param>
        /// <param name="weekCode"></param>
        /// <param name="system">Can be null. Queries act according to this if it is null or not.</param>
        /// <returns></returns>
        double GetTotalMhrsCommissioning(string yearCode, string weekCode, string system = null);

        /// <summary>
        /// Returns the total Count for the loopchecks.
        /// </summary>
        /// <param name="yearCode"></param>
        /// <param name="weekCode"></param>
        /// <param name="system">Can be null. Queries act according to this if it is null or not.</param>
        /// <returns></returns>
        double GetLoopCheckCount(string system = null);


        /// <summary>
        /// Returns the progress for the construction phase.
        /// </summary>
        /// <param name="yearCode"></param>
        /// <param name="weekCode"></param>
        /// <param name="totalMhrs"></param>
        /// <param name="system">Can be null. Queries act according to this if it is null or not.</param>
        /// <returns></returns>
        double GetConstructionProgress(string yearCode, string weekCode, double totalMhrs, string system = null);

        /// <summary>
        /// Returns the progress for the precommissioning phase.
        /// </summary>
        /// <param name="yearCode"></param>
        /// <param name="weekCode"></param>
        /// <param name="totalMhrs"></param>
        /// <param name="system">Can be null. Queries act according to this if it is null or not.</param>
        /// <returns></returns>
        double GetPrecommissioningProgress(string yearCode, string weekCode, double totalMhrs, string system = null);

        /// <summary>
        /// Returns the progress for the commissioning phase.
        /// </summary>
        /// <param name="yearCode"></param>
        /// <param name="weekCode"></param>
        /// <param name="totalMhrs"></param>
        /// <param name="system">Can be null. Queries act according to this if it is null or not.</param>
        /// <returns></returns>
        double GetCommissioningProgress(string yearCode, string weekCode, double totalMhrs, string system = null);

        /// <summary>
        /// Returns the progress for the loop checks phase.
        /// </summary>
        /// <param name="yearCode"></param>
        /// <param name="weekCode"></param>
        /// <param name="totalMhrs"></param>
        /// <param name="system">Can be null. Queries act according to this if it is null or not.</param>
        /// <returns></returns>
        double GetLoopCheckProgress(double totalCount, string system = null);


        /// <summary>
        /// Returns the labels and progress values for punchlist status.
        /// </summary>
        /// <param name="system">Can be null. Queries act according to this if it is null or not.</param>
        /// <returns></returns>
        List<DataKeeper> GetPunchlistStatus(string system = null);

        /// <summary>
        /// Returns the labels and progress values for construction phase
        /// </summary>
        /// <param name="system">Can be null. Queries act according to this if it is null or not.</param>
        /// <returns></returns>
        List<DataKeeper> GetConstructionStatus(string yearCode, string weekCode, string system = null);

        /// <summary>
        /// Returns the labels and progress values for precommissioning phase
        /// </summary>
        /// <param name="system">Can be null. Queries act according to this if it is null or not.</param>
        /// <returns></returns>
        List<DataKeeper> GetPreCommissioningStatus(string yearCode, string weekCode, string system = null);

        /// <summary>
        /// Returns the labels and progress values for commissioning phase
        /// </summary>
        /// <param name="system">Can be null. Queries act according to this if it is null or not.</param>
        /// <returns></returns>
        List<DataKeeper> GetCommissioningStatus(string yearCode, string weekCode, string system = null);
        #endregion

        #region Subcontractor Performance Indicators
        /// <summary>
        /// Gets the list of Subcontractors
        /// </summary>
        /// <returns></returns>
        List<subcontractor> GetSubContractors();

        /// <summary>
        /// Gets the data for all rows of the SOW table
        /// </summary>
        /// <param name="subcontractorCode">The code for the selected subcontractor</param>
        /// <returns></returns>
        List<List<DataKeeper>> GetRowsForSOWTable(string subcontractorCode);

        /// <summary>
        /// Get all the cut-off data
        /// </summary>
        /// <returns></returns>
        List<cut_offf> GetCutOffData();

        /// <summary>
        /// Gets the whole quantity for the selected sub-activity
        /// </summary>
        /// <param name="sub_act">The sub-activity</param>
        /// <param name="dataTable">The name of the datatable</param>
        /// <returns></returns>
        double GetTotalQty(string sub_act, string dataTable);

        /// <summary>
        /// get the budget for the selected subactivity
        /// </summary>
        /// <param name="sub_act">The sub-activity</param>
        /// <returns></returns>
        budget_subact GetBudgetForSubact(string sub_act);

        /// <summary>
        /// Gets the act and the quantity for a selected sub-act
        /// </summary>
        /// <param name="sub_act">The sub-activity</param>
        /// <returns></returns>
        sub_act_qty GetActAndQtyForSubact(string sub_act);

        /// <summary>
        /// Gets the start and end of an activity
        /// </summary>
        /// <param name="act"></param>
        /// <returns></returns>
        planning GetActivityStartAndEnd(string act);

        /// <summary>
        /// Gets the start date and the end date of an activity
        /// </summary>
        /// <param name="startDate">The start date</param>
        /// <param name="endDate">the end date</param>
        /// <returns></returns>
        KeyValuePair<int, int> GetStartEndActivityDates(string startDate, string endDate);
        #endregion


        /// <summary>
        /// Gets full data for a given system code (including description)
        /// </summary>
        /// <param name="systemCode"></param>
        /// <returns></returns>
        descriptor GetFullSystemData(string systemCode);

        /// <summary>
        /// For a given system code, returns a dictionary that as keys has datatables that have data for that system code, and as value a list of data from the key table for the system.
        /// </summary>
        /// <param name="systemCode"></param>
        /// <returns></returns>
        Dictionary<string, IEnumerable<datatable_common>> GetDataForSystemCodeFromDatatables(string systemCode);
    }
}
