﻿using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using System;
using System.Collections.Generic;

namespace Promis.NET.DataAccess.Repositories.Interfaces
{
    public interface IProgressRepository : IRepository
    {
        /// <summary>
        /// Returns a list of data for that contains all the activities and sub-activities of a main activity, with descriptional, quantities and mhrs data.
        /// </summary>
        /// <param name="activity">The Code of the main activity to get data for.</param>
        /// <returns></returns>
        List<work_def_budget> GetQuantitiesData(string activity);

        /// <summary>
        /// Given a subactivity and a list of tables (related to the main activity of this sub-activity), it sums all the quantities found in these tables and returns the result.
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="subActivity"></param>
        /// <returns></returns>
        double GetSumQuantityForSubactivityFromTables(IEnumerable<string> tables, string subActivity);


        /// <summary>
        /// Returns the name of the progress table.
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <param name="activityCode"></param>
        /// <param name="year"></param>
        /// <param name="week"></param>
        /// <returns></returns>
        string GetProgressTableName(string projectDatabase, string activityCode, string year, string week);

        /// <summary>
        /// Runs the pre-requisites, creating the progress table for the given mainactivity-year-week in the progress table, if it does not exist.
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <param name="activityCode"></param>
        /// <param name="year"></param>
        /// <param name="week"></param>
        void CreateProgressTableForWeek(string projectDatabase, string activityCode, string year, string week);

        /// <summary>
        /// Copies data from each of the actual tables to the progress table, deleting invalid data at the same time.
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <param name="mainActivityCode"></param>
        /// <param name="year"></param>
        /// <param name="week"></param>
        /// <param name="subActivity"></param>
        /// <param name="tables"></param>
        void CopyDataFromRealTablesToProgress(string projectDatabase, string mainActivityCode, string year, string week, string subActivity, IEnumerable<string> tables);

        /// <summary>
        /// Returns the trends data (today / week / month percent completed) for a subactivity or for a parent (full) activity if the list of sub-activities is specified.
        /// </summary>
        /// <param name="projectSchema">The project table name (schema for progress table)</param>
        /// <param name="progressTable">The progress table name (schema must be included in name).</param>
        /// <param name="mainActivity">The code for the main activity.</param>
        /// <param name="subActivity">The code for the subactivity.</param>
        /// <param name="totalMhrs">The total mhrs for the subactivity.</param>
        /// <param name="week">The current week.</param>
        /// <param name="year">The current year.</param>
        /// <param name="allSubactivities">The list with all the subacvitities. Required only if you want to compute data for the overall (full) activity containing these sub-activities.</param>
        /// <param name="totalMhrsFull">The total man hours for the full activity. Only specified when computing data for full activity.</param>
        /// <returns></returns>
        PercentData GetTrendsDataForSubActivity(string projectSchema, string progressTable, string mainActivity, string subActivity, double totalMhrs, string week, string year, IEnumerable<string> allSubactivities = null, double totalMhrsFull = 0);

        /// <summary>
        /// Updates the Sx (provided in columnS parameter) column with the 'valueS' value, in the progress table, for the given tag-subactivity combination.
        /// </summary>
        /// <param name="progressTable"></param>
        /// <param name="subActivity"></param>
        /// <param name="tag"></param>
        /// <param name="columnS"></param>
        /// <param name="valueS"></param>
        void UpdateSubActivityProgressColumn(string progressTable, string subActivity, string tag, string columnS, string valueS);

        /// <summary>
        /// Finishes the subactivity progress update cicle by re-computting the progress column for the subactivity, taking into accoult the s1-s12 columns and the progress for this subactivity recorder in the work_def table.
        /// </summary>
        /// <param name="progressTable">The name of the progress table.</param>
        /// <param name="subActivity">The code for the sub-activity.</param>
        /// <param name="allSubactivities">All the data for the subactivity, recorder in the work definition table.</param>
        /// <param name="tag">The tag code.</param>
        /// <returns></returns>
        double UpdateSubActivityProgressPercent(string progressTable, string subActivity, IEnumerable<work_def> allSubactivities, string tag);

        /// <summary>
        /// Get the subactivity info
        /// </summary>
        /// <param name="subAct">The subactivity code</param>
        /// <returns></returns>
        Tuple<string, string> GetSubactivityData(string subAct);

        /// <summary>
        /// Get all the progress Data
        /// </summary>
        /// <param name="table">The table</param>
        /// <param name="sub_act">Sub-activity code</param>
        /// <returns></returns>
        List<progress_planning> GetProgressAllData(string table, string sub_act);

        /// <summary>
        /// Get the total budget value
        /// </summary>
        /// <param name="subact">The subactivity</param>
        /// <returns></returns>
        double GetTotalBudget(string subact);
    }
}
