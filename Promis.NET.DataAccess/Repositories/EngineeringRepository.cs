﻿using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Extensions;
using Promis.NET.DataAccess.Models;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.PromisWeb;
using SqlKata;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace Promis.NET.DataAccess.Repositories
{
    public class EngineeringRepository : Repository, IEngineeringRepository
    {
        public EngineeringRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        #region Metadata and Summaries
        public DataTable GetMenuDataForEngineeringDatabases()
        {
            //query for Engineering DataBase Menu
            var queryString = @"SELECT  DISTINCT a.descr as description, c.description as tag_description, c.datatable as dataTbl
	                            FROM work_disc a
                                JOIN promisweb.work_disc b on a.cod = b.cod
                                LEFT JOIN promisweb.tabletag_def c
		                            ON a.cod = c.cod
	                            WHERE  a.flag <> '' and b.FLAG = 'P'
	                            ORDER BY a.cod, c.description";

            var resOfQuery = ExecuteQuery(queryString);
            return resOfQuery;

        }

        public DataTable GetMenuDataForCivilDataBase()
        {
            //query for Civil DataBase Menu
            var queryCivilDb = @"SELECT  DISTINCT a.descr as description, c.description as tag_description, c.datatable as dataTbl
	                            FROM work_disc a
                                JOIN promisweb.work_disc b on a.cod = b.cod
                                LEFT JOIN promisweb.tabletag_def c
		                            ON a.cod = c.cod 
	                            WHERE  a.flag <> '' and b.FLAG = 'C'
	                            ORDER BY a.cod, c.description";

            var resultOfQueryCivil = ExecuteQuery(queryCivilDb);
            return resultOfQueryCivil;

        }

        public Dictionary<string, string> GetTableDataFromTableTagDef(string active = "")
        {
            //query for Civil DataBase Home Table
            var queryString = @"SELECT datatable, description
                                    FROM promisweb.tabletag_def A
                                    JOIN work_disc B On A.cod = B.cod
                                    WHERE A.flag<>'' and A.active = @active
                                    ORDER BY A.description";

            var parameters = new Dictionary<string, object>() { { "@active", active } };
            var result = ExecuteQuery(queryString, parameters: parameters);
            var dict = new Dictionary<string, string>();

            foreach (DataRow row in result.Rows)
            {
                dict.Add(row["datatable"].ToString(), row["description"].ToString());
            }
            return dict;
        }


        public DataRow GetDataForEngineeringPage(string datatable)
        {
            //return values for the number of records, quantity, certification and system 
            var countR = @"SELECT max(R.a) AS count, max(R.b) AS count_qty, max(R.c) AS count_act, max(R.d) AS count_sys
                            FROM (SELECT COUNT(*) as a, null as b, null as c, null as d FROM @datatable AS b1
                            UNION ALL
                            SELECT null, COUNT(*), null, null FROM @datatable AS b2 WHERE qty<>''
                            UNION ALL
                            SELECT null, null, COUNT(*), null FROM @datatable AS b3 WHERE activity<>''
                            UNION ALL
                            SELECT null, null, null, COUNT(*) FROM @datatable AS b4 WHERE system<>'') R
                        HAVING count > 0 OR count_qty > 0 OR count_act > 0 OR count_sys > 0";

            countR = countR.Replace("@datatable", datatable);
            var resCountR = ExecuteQuery(countR);

            return resCountR.Rows.Count > 0 ? resCountR.Rows[0] : null;

        }
        #endregion

        #region Server Side Processing Data

        public bool VerifyExistenceOfTheRecord(string datatable, string projectId, List<WhereCondition> columnsToVerify)
        {
            if (columnsToVerify.Count == 0)
                return false;

            var verifyString = string.Join(" AND ", columnsToVerify.Select(c => c.LeftOperand + " " + c.Operator + " @" + c.LeftOperand));
            var sqlQuery = "SELECT EXISTS(SELECT 1 FROM  @projectid.@datatable WHERE @columnsToVerify) AS Q";
            sqlQuery = sqlQuery.Replace("@projectid", projectId).Replace("@datatable", datatable).Replace("@columnsToVerify", verifyString);

            var param = new Dictionary<string, object>();
            foreach (var column in columnsToVerify)
            {
                param.Add("@" + column.LeftOperand, column.RightOperand);
            }

            var resNew = ExecuteQuery(sqlQuery, parameters: param);
            int value = int.Parse(resNew.Rows[0][0].ToString());

            return value == 0 ? false : true;
        }

        public string InsertData(string datatable, List<DataKeeper> values)
        {
            var sqlQuery = "INSERT INTO @datatable (@columns) values (@values)";
            string columns = string.Join(", ", values.Select(x => x.ColumnName));
            var col = values.Select(x => x.ColumnName).ToList();
            string vals = string.Join(", ", values.Select(y => "'" + y.Value + "'"));//this is a string with all values

            sqlQuery = sqlQuery.Replace("@datatable", datatable).Replace("@columns", columns).Replace("@values", vals);

            return ExecuteCommandWithNoResults(sqlQuery).ToString();
        }

        public void UpdateData(string datatable, List<DataKeeper> values, string id)
        {
            Func<string, bool> functionThatChecksColumns = (column) =>
            {
                return (column.ToLower() != "id" && column.ToLower() != "subsystem" && column.ToLower() != "system" && column.ToLower() != "activity");
            };
            var columns = values.Select(x => x.ColumnName).Where(functionThatChecksColumns).ToList();//this is a list with all values
            var vls = values.Where(v => functionThatChecksColumns(v.ColumnName)).Select(y => y.Value).ToList();

            var updateString = string.Join(",", columns.Select(c => c + " = @" + c));


            var sqlQuery = "UPDATE " + datatable + " set " + updateString + " WHERE id = @id";
            var param = new Dictionary<string, object>();
            for (int i = 0; i < columns.Count(); i++)
            {
                var column = columns[i].ToString().ToLower();
                param.Add("@" + columns[i], vls[i]);
            }
            param.Add("@id", id);

            ExecuteCommandWithNoResults(sqlQuery, parameters: param);
        }

        public void DeleteData(string datatable, string id)
        {
            var slqQuery = "DELETE FROM " + datatable + " WHERE id = @id";
            var param = new Dictionary<string, object>
            {
                { "@id", id }
            };

            ExecuteCommandWithNoResults(slqQuery, parameters: param);
        }
        
        public Dictionary<string, string> GetPredefinedValuesForColumn(string columnName, string TableName, bool isFiltering = false)
        {
            var values = new Dictionary<string, string>();

            try
            {
                var query = "";
                switch (columnName)
                {
                    case "type":
                        query = "SELECT * FROM " + TableName + "_type ORDER BY code";
                        break;
                    case "wbs":
                        query = "SELECT * FROM area ORDER BY code";
                        break;
                    case "um":
                        query = "SELECT * FROM promisweb.um ORDER BY code";
                        break;
                    case "system":
                        if(isFiltering)
                            query = "SELECT * FROM systems ORDER BY system";
                        break;
                    case "activity":
                        if(isFiltering)
                            query = "SELECT DISTINCT(activity),sub_act_d FROM " + TableName + " AS a,work_def AS b where a.activity=b.sub_act ORDER BY a.activity";
                        break;
                }

                if (string.IsNullOrWhiteSpace(query))
                    return values;

                var result = ExecuteQuery(query);
                for (int i = 0; i < result.Rows.Count; i++)
                {
                    var val = result.Rows[i][0].ToString();
                    var descr = result.Rows[i][1].ToString();
                    values.Add(val, descr);
                }

            }
            catch (Exception){}

            return values;
        }

        public List<cw_cat> GetDataForTags()
        {

            var result = new Query("promisweb.cw_cat");
            result = result.Select("cat", "description", "fd_action");
            result = result.Where("cat", "<>", "FIREPROOFING").Where("cat","<>", "UNDERGROUND").Where("description","<>","").WhereNotNull("description");
            result = result.OrderBy("cat");
            result = result.OrderByRaw("REPLACE('description', '<BR>', ' ')");
            var queryString = _compiler.Compile(result);
            var tagsList = ExecuteQuery<cw_cat>(queryString.Sql, queryString.Bindings);

            return tagsList.ToList();
        }

        #endregion


    }
}
