﻿using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Extensions;
using Promis.NET.DataAccess.Models;
using Promis.NET.DataAccess.Repositories.Interfaces;
using SqlKata;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Promis.NET.Localization;

namespace Promis.NET.DataAccess.Repositories
{
    public class AdvancedTableRepository: Repository, IAdvancedTableRepository
    {
        public string DataTable { get; set; } = "";
        public List<string> Columns { get; set; } = new List<string>();
        public string Search { get; set; } = "";
        public List<WhereCondition> Filters { get; set; } = new List<WhereCondition>();
        public List<JoinCondition> Joins { get; set; } = new List<JoinCondition>();
        public List<DataTableOrder> Orders { get; set; } = new List<DataTableOrder>();
        public List<string> GroupBy { get; set; } = new List<string>();
        public bool Distinct { get; set; } = false;

        private List<string> _columnsStandard = new List<string>();
        private List<string> _initialColumnsList = new List<string>();
        private List<string> _allColumns = new List<string>();

        #region Constructors
        public AdvancedTableRepository(IDbContext dbContext): base(dbContext)
        {
        }

        public void Initialize(string dataTable)
        {
            if(dataTable != null)
                DataTable = dataTable;
            Columns = new List<string>();
            Search = "";
            Filters = new List<WhereCondition>();
            Joins = new List<JoinCondition>();
            Orders = new List<DataTableOrder>();
            Distinct = false;
        }

        public void Initialize(string dataTable, IEnumerable<string> columns)
        {
            Initialize(dataTable);
            if (columns != null)
            {
                Columns = columns.ToList();
                if (!Columns.Contains("id") && !Columns.Contains("ID"))
                    Columns.Add(DataTable.ToLower() + ".id");
                _initialColumnsList = Columns.ToList();
            }
        }

        public void Initialize(string dataTable, IEnumerable<string> columns, IEnumerable<WhereCondition> filters)
        {
            Initialize(dataTable, columns);
            if (filters != null)
                Filters = filters.ToList();
        }

        public void Initialize(string dataTable, IEnumerable<string> columns, IEnumerable<WhereCondition> filters, IEnumerable<JoinCondition> joins)
        {
            Initialize(dataTable, columns, filters);
            if(joins != null)
                Joins = joins.ToList();
        }

        public void Initialize(string dataTable, IEnumerable<string> columns, IEnumerable<WhereCondition> filters, bool distinct)
        {
            Initialize(dataTable, columns, filters);
            Distinct = distinct;
        }

        public void Initialize(string dataTable, IEnumerable<string> columns, IEnumerable<WhereCondition> filters, IEnumerable<JoinCondition> joins, bool distinct)
        {
            Initialize(dataTable, columns, filters, joins);
            Distinct = distinct;
        }

        public void Initialize(string dataTable, IEnumerable<string> columns, IEnumerable<WhereCondition> filters, IEnumerable<JoinCondition> joins, bool distinct, string search)
        {
            Initialize(dataTable, columns, filters, joins);
            Distinct = distinct;
            Search = search;
        }

        public void Initialize(string dataTable, IEnumerable<string> columns, IEnumerable<WhereCondition> filters, IEnumerable<JoinCondition> joins, IEnumerable<DataTableOrder> orders, bool distinct, string search)
        {
            Initialize(dataTable, columns, filters, joins, distinct, search);
            if (orders != null)
                Orders = orders.ToList();
        }

        public void SetGroupBy(IEnumerable<string> groups)
        {
            if (groups != null)
                GroupBy = groups.ToList();
        }
        #endregion


        public List<string> GetAllColumnsStandardized()
        {
            var cols = new List<string>();
            var allTablesInvolved = new List<string>();
            if (!string.IsNullOrWhiteSpace(DataTable))
                allTablesInvolved.Add(DataTable);

            if (Joins != null)
                allTablesInvolved.AddRange(Joins.Select(j => j.DataTable).Distinct());

            foreach (var table in allTablesInvolved)
            {
                var columnsForThisOne = GetColumnsFromTable(table).Select(c => table.ToLower() + "." + c.ToLower()).ToList();
                cols.AddRange(columnsForThisOne);
            }

            return cols;
        }

        public List<string> GetAllColumnsInvolvedStandardized()
        {
            _allColumns = GetAllColumnsStandardized();
            if (Columns == null || Columns.Count == 0)//if columns not specified, return all
                return _allColumns;

            if (Columns.Count == 1 && Columns[0].ToLower() == DataTable.ToLower() + ".id")// if only the default id column is in the list, then no list has been specified => return all
                return _allColumns;

            var output = new List<string>();
            foreach (var column in Columns)
            {
                var newName = column.Contains(".") ? column.ToLower() : DataTable.ToLower() + "." + column.ToLower();
                if(_allColumns.Contains(newName))//validity check so i don't send other columns names inexistent
                    output.Add(newName);
            }

            _columnsStandard = output;
            return output;
        }

        public Query GetCountAllQuery()
        {
            if (string.IsNullOrWhiteSpace(DataTable))
                throw new System.Exception(LocalResources.NoDatatableWasSpecifiedInTheDynamicTableOperationsCount);

            var result = new Query(DataTable);

            var columnsUsed = GetRealInvolvedColumnList(GetAllColumnsInvolvedStandardized());

            if (Filters != null)
                result = Filters.ToWhereQuery(result, DataTable);

            if (Joins != null)
                result = Joins.ToJoinQuery(result);

            if (GroupBy != null)
                result = GroupBy.ToGroupBy(result, DataTable, _allColumns);

            if (!Distinct)
            {
                result = result.Count();
                return result;
            }

            // count for distinct (to consider non-null columns in left/right joins) => select count(*) from (select distinct columns from table)
            if (columnsUsed != null)
                result = columnsUsed.ToSelectColumns(result, DataTable);

            result = result.Distinct();

            var upperQuery = new Query().From(result, "xxx").Count();
            return upperQuery;
        }

        public long CountAll(bool leaveConnectionOpen = true)
        {
            var query = GetCountAllQuery();
            var comp = _compiler.Compile(query);
            return ExecuteQueryForValue<long>(comp.Sql, comp.Bindings, leaveConnectionOpen);
        }

        public Query GetCountAllFilteredQuery()
        {
            if (!Distinct)
            {
                var query = GetCountAllQuery();
                if (_columnsStandard == null || _columnsStandard.Count == 0)
                    _columnsStandard = GetAllColumnsInvolvedStandardized();

                if (Search != null)
                    query = Search.ToSearchInEveryColumn(query, _columnsStandard);

                return query;
            }

            // count for distinct (to consider non-null columns in left/right joins) => select count(*) from (select distinct columns from table)
            var innerQuery = GetTableDataQuery();
            var upperQuery = new Query().From(innerQuery, "xxx").Count();
            return upperQuery;
        }

        public long CountAllFiltered(bool leaveConnectionOpen = true)
        {
            var query = GetCountAllFilteredQuery();
            var comp = _compiler.Compile(query);
            return ExecuteQueryForValue<long>(comp.Sql, comp.Bindings, leaveConnectionOpen);
        }


        public Query GetTableDataQuery(int? start = null, int? limit = null)
        {
            if (string.IsNullOrWhiteSpace(DataTable))
                throw new System.Exception(LocalResources.NoDatatableWasSpecifiedInTheDynamicTableOperationsCount);

            var columnsRenamed = GetAllColumnsInvolvedStandardized();
            columnsRenamed = GetRealInvolvedColumnList(columnsRenamed);

            var result = new Query(DataTable);

            if (columnsRenamed != null)
                result = columnsRenamed.ToSelectColumns(result, DataTable);

            if (Filters != null)
                result = Filters.ToWhereQuery(result, DataTable);

            if (Joins != null)
                result = Joins.ToJoinQuery(result);

            if (GroupBy != null)
                result = GroupBy.ToGroupBy(result, DataTable, _allColumns);

            if (Search != null)
                result = Search.ToSearchInEveryColumn(result, _columnsStandard);

            if (Orders != null)
                result = Orders.ToOrderBy(result, columnsRenamed);

            if (start.HasValue && start >= 0)
                result = result.Offset(start.Value);

            if (limit.HasValue && limit >= 0)
                result = result.Limit(limit.Value);

            if (Distinct)
                result = result.Distinct();

            return result;
        }

        public DataTable GetTableData(int? start = null, int? limit = null, bool leaveConnectionOpen = true)
        {
            var query = GetTableDataQuery(start, limit);
            var comp = _compiler.Compile(query);
            return ExecuteQuery(comp.Sql, comp.Bindings, leaveConnectionOpen);
        }

        /// <summary>
        /// Basically checks if I am using distinct, and if so, when counting, removes the ID column if not specifically given as parameter by the user / programmer
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private List<string> GetRealInvolvedColumnList(List<string> input)
        {
            var notIncluded = DataTable.ToLower() + ".id";
            if (Distinct && _initialColumnsList != null && !_initialColumnsList.Contains(DataTable.ToLower() + ".id") && !_initialColumnsList.Contains(DataTable.ToLower() + ".ID"))
                return input.Where(c => c != notIncluded).ToList();
            return input;
        }
    }
}
