﻿using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Entities;
using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using SqlKata;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Promis.NET.DataAccess.Repositories
{
    public class ActivitiesRepository : Repository, IActivitiesRepository
    {
        public ActivitiesRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public List<testpack_essentials> GetAllCertifiedTestPacksWithData()
        {
            var query = new Query("cert_testpack as A")
                            .SelectRaw("cast(A.step as signed) as `step`")
                            .Select("C.inspection")
                            .SelectRaw("COUNT(A.c) as `number`")
                            .SelectRaw("CASE WHEN C.progr <> '' THEN true ELSE false END AS `exists`")
                                .LeftJoin("work_def as C", j => j.On("A.subact", "C.sub_act").On("A.step", "C.step"))
                            .Where("A.c", "<>", "")
                            .GroupBy("A.step")
                            .OrderByRaw("`step`");
            var compiled = _compiler.Compile(query);
            return ExecuteQuery<testpack_essentials>(compiled.Sql, compiled.Bindings).ToList();
        }

        #region Hydrotest Packages ISO Assignment
        public KeyValuePair<double, double> GetSketchesAndTestPackNumber()
        {
            var sketchesCountQuery = new Query("sketches")
                                    .Select("maintag")
                                    .Distinct()
                                    .Count("maintag");

            var testpackCountQuery = new Query("testpack_link")
                                    .Select("comp")
                                    .Distinct()
                                    .Count("comp");
            var compiledSketches = _compiler.Compile(sketchesCountQuery);
            var compiledTestpack = _compiler.Compile(testpackCountQuery);

            var countSketches = ExecuteQueryForValue<double?>(compiledSketches.Sql, compiledSketches.Bindings);
            var countTestpack = ExecuteQueryForValue<double?>(compiledTestpack.Sql, compiledTestpack.Bindings);
            return new KeyValuePair<double, double>(countSketches ?? 0, countTestpack ?? 0);

        }

        public List<string> GetPipingList(string isometric, string parameterNeeded)
        {
            var pipingList = new List<string>();
            var pipingQuery = new Query("sketches");

            switch (isometric)
            {
                case "pipingClass":
                    pipingQuery = pipingQuery.Select("piping_class")
                            .Distinct()
                            .Where("pds", parameterNeeded)
                            .OrderBy("piping_class");
                    break;
                case "system":
                    pipingQuery = pipingQuery.Select("system")
                            .Distinct()
                            .Where("piping_class", parameterNeeded)
                            .OrderBy("system");
                    break;
                case "pipingDesign":
                    pipingQuery = pipingQuery.Select("pds").Distinct().OrderBy("pds");
                    break;
            }

            var compiledQuery = _compiler.Compile(pipingQuery);
            var piping = ExecuteQuery(compiledQuery.Sql, compiledQuery.Bindings);
            if (piping.Rows.Count == 0)
                return pipingList;

            for (int i = 0; i < piping.Rows.Count; i++)
            {
                var pipingInfo = piping.Rows[i][0].ToString();
                pipingList.Add(pipingInfo);
            }
            return pipingList;
        }

        public List<area> GetGeographicalWBS()
        {
            var pipingList = new List<string>();
            var areaQuery = new Query("area")
                              .OrderBy("code");

            var compiledQuery = _compiler.Compile(areaQuery);
            return ExecuteQuery<area>(compiledQuery.Sql, compiledQuery.Bindings).ToList();
        }

        public List<descriptor> GetSystemsInfo(string pc)
        {
            var systemInfo = new List<descriptor>();
            var systemsList = GetPipingList("system", pc);
            if (systemsList.Count == 0)
                return systemInfo;


            for (int i = 0; i < systemsList.Count; i++)
            {
                var systemsQuery = new Query();
                var system = systemsList[i];
                systemsQuery = new Query("systems").SelectRaw("system as code, system_description as description").Where("system", system);

                var compiledQuery = _compiler.Compile(systemsQuery);
                var sys = ExecuteQueryForValue<descriptor>(compiledQuery.Sql, compiledQuery.Bindings);
                if(sys != null)
                    systemInfo.Add(sys);
            }
            return systemInfo;
        }

        public List<descriptor> GetSubSystemsInfo(string system)
        {
            var systemsQuery = new Query("systems")
                               .SelectRaw("subsystem as code, subsystem_description as description")
                               .Where("system", system)
                               .WhereNotNull("subsystem")
                               .OrderBy("subsystem");

            var compiledQuery = _compiler.Compile(systemsQuery);
            return ExecuteQuery<descriptor>(compiledQuery.Sql, compiledQuery.Bindings).ToList();
        }

        public string GetTableNameTestpackages()
        {
            return "testpack";
        }

        public List<DataKeeper> GetColumnsNecessaryForTestPackTable()
        {
            return new DataKeeper[] {
                new DataKeeper("TAG","HYDROTEXT"),
                new DataKeeper("WBS"),
                new DataKeeper("QTY"),
                new DataKeeper("SYSTEM"),
                new DataKeeper("SUBSYSTEM","SUB-SYSTEM") }.ToList();
        }

        public string GetTableNameForIsometrics()
        {
            return "sketches";
        }

        public List<DataKeeper> GetColumnsNecessaryForIsometrics()
        {
            return new DataKeeper[] {
                new DataKeeper(" "),
                new DataKeeper("TAG","ISOMETRIC"),
                new DataKeeper("WBS"),
                new DataKeeper("QTY"),
                new DataKeeper("PIPING_CLASS","CLASS"),
                new DataKeeper("SYSTEM"),
                new DataKeeper("SUBSYSTEM","SUB-SYSTEM") }.ToList();
        }

        public bool InsertHydrotestPack(string code, string description)
        {
            try
            {
                var testpack = new testpack()
                {
                    TAG = code,
                    DESCRIPTION = description
                };
                Create(testpack, "TAG", code);
            }
            catch (System.Exception)
            {
                return false;
            }

            return true;
        }

        public void DeleteHydrotest(string id, string tag)
        {
            var deleteTestpackQuery = new Query("testpack")
                               .Delete()
                               .Where("id", id);

            var deleteTestpackLinkQuery = new Query("testpack_link")
                              .Delete()
                              .Where("tag", tag);

            var compiledTestpack = _compiler.Compile(deleteTestpackQuery);
            var compiledTestpackLink = _compiler.Compile(deleteTestpackLinkQuery);
            ExecuteCommandWithNoResults(compiledTestpack.Sql, compiledTestpack.Bindings);
            ExecuteCommandWithNoResults(compiledTestpackLink.Sql, compiledTestpackLink.Bindings);
        }

        public string GetMaintag(string selectedIsometricTag)
        {
            var selectMainTagQuery = new Query("sketches")
                             .Select("maintag")
                             .Where("tag", selectedIsometricTag);
            var compiledQuery = _compiler.Compile(selectMainTagQuery);
            return ExecuteQueryForValue<string>(compiledQuery.Sql, compiledQuery.Bindings); ;
        }

        public int GetCountTagFromTestPack(string selectedTag, string maintag)
        {
            var checkTestpackLinkQuery = new Query("testpack_link")
                              .Count()
                              .Where("tag", selectedTag)
                              .Where("comp", maintag);
            var compiledTestPackQuery = _compiler.Compile(checkTestpackLinkQuery);
            return ExecuteQueryForValue<int>(compiledTestPackQuery.Sql, compiledTestPackQuery.Bindings);
        }

        public void DeleteTestPackLink(string selectedTag)
        {
            var deleteQuery = new Query("testpack_link").Delete().Where("tag", selectedTag);
            var compiledDeleteQuery = _compiler.Compile(deleteQuery);
            ExecuteCommandWithNoResults(compiledDeleteQuery.Sql, compiledDeleteQuery.Bindings);
        }

        public double GetSketchesSum(string maintag)
        {
            var resSum = 0.0;
            if (string.IsNullOrWhiteSpace(maintag))
                return resSum;

            var selSum = new Query("sketches")
                      .SelectRaw("sum(qty)")
                      .Where("maintag", maintag);
            var compileSel = _compiler.Compile(selSum);
            return ExecuteQueryForValue<double>(compileSel.Sql, compileSel.Bindings); ;
        }

        public void InsertIntoTestpacklink(string selectedTag, string maintag, string qty, string system, string subsystem)
        {
            var dataToInsert = new Dictionary<string, object> {
                {"tag",selectedTag},
                {"comp", maintag},
                {"qty", qty},
                {"system", system},
                {"subsystem", subsystem},
            };
            var insertQuery = new Query("testpack_link").Insert(dataToInsert);
            var compiledInsertQuery = _compiler.Compile(insertQuery);
            ExecuteCommandWithNoResults(compiledInsertQuery.Sql, compiledInsertQuery.Bindings);
        }
  
        public void UpdateTestPack(string selectedTag, string system, string subsystem, string wbs)
        {
            var selQuery = new Query("sketches")
                        .SelectRaw("sum(sketches.qty) as sum_qty")
                        .Join("testpack_link", j => j.On("sketches.maintag", "testpack_link.comp"))
                        .Join("testpack", j => j.On("testpack_link.tag", "testpack.tag"))
                        .Where("testpack.tag", selectedTag);

            var compiledQuery = _compiler.Compile(selQuery);
            var testpackVal = ExecuteQueryForValue<double>(compiledQuery.Sql, compiledQuery.Bindings);
 

            var data = new Dictionary<string, object> {
                {"WBS", wbs},
                {"QTY",(Math.Round(testpackVal,2)).ToString()},
                {"SYSTEM", system},
                {"SUBSYSTEM", subsystem}
            };
            var query = new Query("testpack").Update(data).Where("tag", selectedTag);
            var compiledTestPackQuery = _compiler.Compile(query);
            var result = ExecuteCommandWithNoResults(compiledTestPackQuery.Sql, compiledTestPackQuery.Bindings);                        
        }
        #endregion

        #region System punch list Management
        public List<system_base> GetAllSystemFromPunchlist(bool plOnly)
        {
            var query = new Query("SYSTEMS AS A")
                .Select("A.SYSTEM", "A.SYSTEM_DESCRIPTION");
            if (plOnly) { query.Join("PUNCHLIST AS B", "A.SYSTEM", "B.SYSTEM"); }
            query.Distinct()
            .OrderBy("A.SYSTEM");
            var compiled = _compiler.Compile(query);
            return ExecuteQuery<system_base>(compiled.Sql, compiled.Bindings).ToList();
        }

        public List<pl_disci_base> GetAllDisciplines()
        {
            var query = new Query("pl_disci")
                .Select("code", "description")
                .OrderBy("code");
            var compiled = _compiler.Compile(query);
            return ExecuteQuery<pl_disci_base>(compiled.Sql, compiled.Bindings).ToList();
        }

        public List<pl_orig_base> GetAllOriginators()
        {
            var query = new Query("pl_orig")
                .Select("initials", "name")
                .OrderBy("initials");
            var compiled = _compiler.Compile(query);
            return ExecuteQuery<pl_orig_base>(compiled.Sql, compiled.Bindings).ToList();
        }
        public List<pl_disci_base> GetAllCategory()
        {
            var query = new Query("pl_category")
                .Select("code", "description")
                .OrderBy("code");
            var compiled = _compiler.Compile(query);
            return ExecuteQuery<pl_disci_base>(compiled.Sql, compiled.Bindings).ToList();
        }
        public List<pl_actionby_base> GetAllActionsBy()
        {
            var query = new Query("pl_actionby")
                .Select("code", "company")
                .OrderBy("code");
            var compiled = _compiler.Compile(query);
            return ExecuteQuery<pl_actionby_base>(compiled.Sql, compiled.Bindings).ToList();
        }

        public List<string> GetAllStatus()
        {
            var query = new Query("pl_status")
                .Select("code")
                .OrderBy("code");
            var compiled = _compiler.Compile(query);
            return ExecuteQuery<string>(compiled.Sql, compiled.Bindings).ToList();
        }
        public List<pl_orig_base> GetAllCloser()
        {
            var query = new Query("pl_closer")
                .Select("initials", "name")
                .OrderBy("initials");
            var compiled = _compiler.Compile(query);
            return ExecuteQuery<pl_orig_base>(compiled.Sql, compiled.Bindings).ToList();
        }
        public List<DataKeeper> GetColumnsNecessaryForPunchlistTable()
        {
            return new DataKeeper[] {
                new DataKeeper("ITEM"),
                new DataKeeper("wd","DETAILS"),
                new DataKeeper("flag","CAT"),
                new DataKeeper("pl_disci","DISCIPLINE"),
                new DataKeeper("actionby","ACTION BY"),
                new DataKeeper("orig","ORIGINATOR"),
                new DataKeeper("found_date","FOUND DATE"),
                new DataKeeper("js","STATUS"),
                new DataKeeper("CLOSER"),
                new DataKeeper("target_date","TARGET DATE"),
                new DataKeeper("closure_date","CLOSE DATE"),
                new DataKeeper("reference","DOC")
             }.ToList();
        }
        public string GetClosureByRowId(string rowId)
        {
            var query = new Query("punchlist")
               .Select("closure")
               .Where("id", rowId);
            var compiled = _compiler.Compile(query);
            return ExecuteQueryForValue<string>(compiled.Sql, compiled.Bindings);
        }

        public void InsertPunchlistRow(Dictionary<string,object> punchData)
        {
            var insertQuery = new Query("punchlist").Insert(punchData.Select(d => d.Value == null ? new KeyValuePair<string, object>(d.Key, System.DBNull.Value) : new KeyValuePair<string, object>(d.Key, d.Value)).ToDictionary(d => d.Key, d => d.Value));
            var compiledInsert = _compiler.Compile(insertQuery);
            ExecuteCommandWithNoResults(compiledInsert.Sql, compiledInsert.Bindings);
        }

        public List<tabletab_def_base> GetDataTablesForModal()
        {
            var selQuery = new Query("promisweb.tabletag_def")
                             .Select("datatable", "description")
                             .Join("work_disc", j => j.On("tabletag_def.cod", "work_disc.cod"))
                             .Where("tabletag_def.flag", "<>", "")
                             .OrderBy("pos");

            var queryString = _compiler.Compile(selQuery);
            return ExecuteQuery<tabletab_def_base>(queryString.Sql, queryString.Bindings).ToList();
        }

        public List<DataKeeper> GetColumnsNecessaryForPunchlistModalTable()
        {
            return new DataKeeper[] {
                new DataKeeper("TAG"),
                new DataKeeper("DESCRIPTION"),
                new DataKeeper("WBS")
            }.ToList();
        }
        public double CountRowsInTableBySystem(string tablename,string system)
        {
            var query = new Query(tablename)
                .Count()
                .Where("system", system);
            var compile = _compiler.Compile(query);
            return ExecuteQueryForValue<double>(compile.Sql, compile.Bindings);
        }
        #endregion
    }

}
