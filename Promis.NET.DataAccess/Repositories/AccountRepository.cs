﻿using Microsoft.AspNet.Identity;
using Promis.NET.DataAccess.DbContexts;
using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Identity.MySql;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.Web.DataAccess.Entities.PromisWeb;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Promis.NET.Localization;

namespace Promis.NET.DataAccess.Repositories
{
    public class AccountRepository : Repository, IAccountRepository
    {
        private UserManager<IdentityUser> _manager;
        private UserTable<IdentityUser> _userTable;

        public AccountRepository(IDbContext dbContext) : base(dbContext)
        {
            _manager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(new ApplicationDbContext()));
            _userTable = new UserTable<IdentityUser>(new ApplicationDbContext());
        }

        public IdentityUser GetUserByNameAndProject(string username, string project)
        {
            var users = _userTable.GetUserByName(username);
            if (users == null || users.Count == 0)
                return null;

            var user = users.Where(u => u.ProjectId == project).FirstOrDefault();
            if (user != null)
                user.Password = "";

            return user;
        }

        public IdentityUser GetUserById(string id)
        {
            return _userTable.GetUserById(id);
        }

        public List<IdentityUser> GetAllNonAdminUsersForProject(string projectId)
        {
            return _userTable.GetAllUsersNonAdminsForProject(projectId);
        }

        public void UpdateBasicUserInfoById(IdentityUser user, bool updateFullDetails = false)
        {
            var existingUser = _userTable.GetUserById(user.Id);
            if (existingUser == null)
                throw new System.Exception(LocalResources.TheUserYouAreTryingToUpdateDoesNotExist);

            existingUser.FirstName = user.FirstName;
            existingUser.LastName = user.LastName;
            existingUser.Email = user.Email;
            existingUser.OfficePhone = user.OfficePhone;
            existingUser.MobilePhone = user.MobilePhone;
            existingUser.Country = user.Country;
            existingUser.Company = user.Company;
            existingUser.Language = user.Language;
            existingUser.PhotoUrl = user.PhotoUrl;

            //handle roles
            if (updateFullDetails)
            {
                existingUser.UserName = user.UserName;
                existingUser.RolesModulesString = user.RolesModulesString;
                existingUser.RolesActivitiesString = user.RolesActivitiesString;
                existingUser.RolesResponsabilitiesString = user.RolesResponsabilitiesString;
            }

            //handle password change
            if (!string.IsNullOrWhiteSpace(user.Password) && !string.IsNullOrWhiteSpace(user.Password.Trim()))
            {
                existingUser.Password = _manager.PasswordHasher.HashPassword(user.Password);
            }


            var x = _userTable.Update(existingUser);
        }

        public void DeleteUserById(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new System.Exception(LocalResources.TheSpecifiedIdIsNotValid);

            Delete<userfulllist, long>("id", long.Parse(userId));
        }
    }
}
