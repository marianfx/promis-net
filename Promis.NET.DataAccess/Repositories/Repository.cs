﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MySql.Data.MySqlClient;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Utilities.Extensions;
using Promis.NET.DataAccess.DbContexts.Interfaces;
using System.Transactions;
using SqlKata.Compilers;
using System.Threading;
using SqlKata;
using Promis.NET.Utilities.Models;
using System.Data.Entity;
using Promis.NET.DataAccess.Models;
using Promis.NET.DataAccess.Extensions;
using System.Data.Entity.Core;
using Promis.NET.Utilities.Exceptions;
using Promis.NET.Localization;

namespace Promis.NET.DataAccess.Repositories
{
    public class Repository: IRepository, IDisposable
    {
        public IDbContext DbContext { get; set; }
        protected MySqlConnection _connection;

        protected TransactionScope _transaction;
        protected System.Transactions.IsolationLevel _isolationLevelQuery = System.Transactions.IsolationLevel.ReadCommitted;
        protected System.Transactions.IsolationLevel _isolationLevelCommand = System.Transactions.IsolationLevel.ReadCommitted;

        protected MySqlCompiler _compiler;

        public Repository(IDbContext dbContext)
        {
            DbContext = dbContext;
            _connection = (MySqlConnection)DbContext.Database.Connection;
            _compiler = new MySqlCompiler();
        }


        #region Usual CRUD repository methods 
        public IEnumerable<T> GetAll<T>(bool leaveConnectionOpen = true) 
            where T : class
        {
            try
            {
                EnsureTransactionExists(_isolationLevelQuery);
                EnsureConnectionOpen();
                return DbContext.Set<T>();
            }
            finally
            {
                if (!leaveConnectionOpen)
                {
                    FinishTransaction(commit: false);
                }
            }
        }

        public T GetById<T, I>(string idFieldName, I id, bool leaveConnectionOpen = true) 
            where T : class
        {
            try
            {
                EnsureTransactionExists(_isolationLevelQuery);
                EnsureConnectionOpen();
                return DbContext.Set<T>().AsEnumerable().FirstOrDefault(o => EqualityComparer<I>.Default.Equals(o.GetIdProperty<I>(idFieldName), id));
            }
            finally
            {
                if (!leaveConnectionOpen)
                {
                    FinishTransaction(commit: false);
                }
            }
        }

        public int CountAll<T>(bool leaveConnectionOpen = true) where T : class
        {
            try
            {
                EnsureTransactionExists(_isolationLevelQuery);
                EnsureConnectionOpen();
                return DbContext.Set<T>().Count();
            }
            finally
            {
                if (!leaveConnectionOpen)
                {
                    FinishTransaction(commit: false);
                }
            }
        }

        /// <summary>
        /// Creates a new object. Has the ability to check if an object with the same identifier already exists (id-value). Give the idFieldname and idFieldValue as NULL parameters to ignore them.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="I"></typeparam>
        /// <param name="obj"></param>
        /// <param name="idFieldName"></param>
        /// <param name="idFieldValue"></param>
        /// <param name="saveChanges"></param>
        /// <returns></returns>
        public T Create<T, I>(T obj, string idFieldName, I idFieldValue, bool saveChanges = false) where T : class
        {
            try
            {
                EnsureTransactionExists(_isolationLevelCommand);
                EnsureConnectionOpen();

                if (idFieldName != null)
                {
                    var existing = GetById<T, I>(idFieldName, idFieldValue);
                    if (existing != null)
                        throw new Exception(LocalResources.AnotherEntityWithTheGivenIdentifierAlreadyExists);
                }

                DbContext.Entry(obj).State = EntityState.Added;
                DbContext.SaveChanges();
                if (saveChanges)
                    FinishTransaction();

                return obj;
            }
            catch(Exception ex)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch { }
                
                throw ex;
            }
        }

        public void UpdateById<T, I>(T obj, string idFieldName, I idFieldValue, List<string> excludedFields = null, IEnumerable<string> includedFields = null, bool saveChanges = false) where T : class
        {
            excludedFields = excludedFields ?? new List<string>();
            if(!excludedFields.Contains(idFieldName))
                excludedFields.Add(idFieldName);
            if (!excludedFields.Contains("id"))
                excludedFields.Add("id");

            try
            {
                EnsureTransactionExists(_isolationLevelCommand);
                EnsureConnectionOpen();

                var existing = GetById<T, I>(idFieldName, idFieldValue);
                if (existing == null)
                    throw new ObjectNotFoundException(LocalResources.ThereIsNoEntityToUpdateWithTheGivenId+ idFieldValue + ")");

                foreach (var prop in typeof(T).GetProperties())
                {
                    // add only those that
                    // - are not in the excluded list
                    // - if the included list is not blank, are in the included list
                    if (excludedFields.Contains(prop.Name))
                        continue;
                    if (includedFields != null && includedFields.Count() > 0 && !includedFields.Contains(prop.Name))
                        continue;
                    prop.SetValue(existing, prop.GetValue(obj));
                }

                DbContext.Entry(existing).State = EntityState.Modified;
                DbContext.SaveChanges();

                if (saveChanges)
                    FinishTransaction();
            }
            catch(Exception ex)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch { }
                throw ex;
            }
        }

        public void Delete<T, I>(string idFieldName, I idFieldValue, bool saveChanges = false) where T : class
        {
            try
            {
                EnsureTransactionExists(_isolationLevelCommand);
                EnsureConnectionOpen();

                var existing = GetById<T, I>(idFieldName, idFieldValue);
                if (existing == null)
                    throw new Exception(LocalResources.ThereIsNoEntityToUpdateWithTheGivenId + idFieldValue + ")");

                DbContext.Entry(existing).State = EntityState.Deleted;
                DbContext.SaveChanges();

                if (saveChanges)
                    FinishTransaction();
            }
            catch (Exception ex)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch { }
                throw ex;
            }
        }

        public void DeleteByConditions(string datatable, string projectId, List<WhereCondition> conditions)
        {
            if (conditions.Count == 0)
                return;

            var query = new Query(datatable)
                            .Delete();
            foreach (var condition in conditions)
            {
                query = query.Where(condition.LeftOperand, condition.Operator, (condition.RightOperand ?? DBNull.Value));
            }

            var compiled = _compiler.Compile(query);

            ExecuteCommandWithNoResults(compiled.Sql, compiled.Bindings);
        }
        #endregion


        #region Table Operations
        public bool DoesTableExist(string table, bool leaveConnectionOpen = true)
        {
            try
            {
                var sqlText = "SELECT 1 FROM " + table + " LIMIT 1";
                ExecuteQuery<int>(sqlText, null, leaveConnectionOpen);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DropTable(string table, bool leaveConnectionOpen = true)
        {
            var query = "DROP TABLE IF EXISTS " + table;
            ExecuteCommandWithNoResults(query, saveChanges: !leaveConnectionOpen);
        }
        
        public void CreateTable(string table, bool overrideIfExists = false, string asQuery = "", bool leaveconnectionOpen = true)
        {
            if (overrideIfExists)
                DropTable(table);

            var query = "CREATE TABLE IF NOT EXISTS " + table + (!string.IsNullOrWhiteSpace(asQuery) ? " AS (" + asQuery + ")" : "");
            ExecuteCommandWithNoResults(query, saveChanges: !leaveconnectionOpen);
        }

        public void CreateTable(string table, Dictionary<string, string> columns, bool overrideIfExists = false)
        {
            if (columns == null || columns.Count == 0)
                throw new ArgumentNullException(LocalResources.ColumnListMustBeProvided);

            if (overrideIfExists)
                DropTable(table);

            string columnsDeclarations = string.Join(",", columns.Select(c => String.Format("{0} {1}", !string.IsNullOrWhiteSpace(c.Value) ? "`" + c.Key + "`" : c.Key, c.Value)));
            var query = "CREATE TABLE IF NOT EXISTS " + table + "(" + columnsDeclarations + ")";
            ExecuteCommandWithNoResults(query);
        }

        public void ClearTableData(string table, bool leaveConnectionOpen = true)
        {
            if (DoesTableExist(table))
            {
                var query = "DELETE FROM " + table;
                ExecuteCommandWithNoResults(query, saveChanges: !leaveConnectionOpen);
            }
        }
        
        public List<DataKeeper> GetColumnsNameAndType(string datatable)
        {
            //query for getting fields name from the given datatable
            var queryColumnName = @"SHOW FIELDS FROM " + datatable;
            var resultOfQueryColName = ExecuteQuery(queryColumnName);
            var resultList = new List<DataKeeper>();

            foreach (DataRow dr in resultOfQueryColName.Rows)
            {
                var cell = new DataKeeper();
                cell.ColumnName = dr["Field"].ToString();
                cell.Value = dr["Type"].ToString();
                resultList.Add(cell);

            }
            return resultList;
        }

        public DataTable GetTableColumnsMetadata(string tableName, string projectId)
        {
            var possibleTableSchema = tableName.Contains(".") ? tableName.Split('.')[0] : projectId;
            var possibleTableName = tableName.Contains(".") ? tableName.Split('.')[1] : tableName;
            //query for getting the data which i want to display
            var queryString = @"SELECT T.COLUMN_NAME, TD.*
                                FROM information_schema.`COLUMNS` T
                                LEFT JOIN promisweb.tabletag_structure TD 
                                ON lcase(T.COLUMN_NAME) = lcase(TD.code)
                                WHERE TABLE_SCHEMA = @projectid and TABLE_NAME = @tableName
                                ORDER BY T.ORDINAL_POSITION";
            var param = new Dictionary<string, object>
            {
                { "@projectid", possibleTableSchema },
                { "@tableName", possibleTableName }
            };

            var result = ExecuteQuery(queryString, parameters: param);
            return result;
        }

        public List<string> GetColumnsFromTable(string dataTable)
        {
            var cols = GetColumnsNameAndType(dataTable);
            var col = cols.Select(x => x.ColumnName).ToList();
            return col;
        }

        public void AddColumnAfterColumn(string tableName, string colName, string type, string oldColumn)
        {
            var sqlQuery = String.Format("ALTER TABLE {0} ADD COLUMN {1} {2} AFTER {3}", tableName, colName, type, oldColumn);
            try
            {
                ExecuteCommandWithNoResults(sqlQuery);
            }
            catch (Exception e)
            {
                PromisException.GetFromExistingException(e);
                Console.WriteLine(LocalResources.ColumnAlreadyExists);
            }
        }

        public void DropColumn(string tableName, string colName)
        {
            var sqlQuery = String.Format("ALTER TABLE {0} DROP COLUMN {1}", tableName, colName);
            try
            {
                ExecuteCommandWithNoResults(sqlQuery);
            }
            catch (Exception e)
            {
                PromisException.GetFromExistingException(e);
                Console.WriteLine(LocalResources.ColumnsAlreadyDropped);
            }
        }

        #endregion


        #region Non Standard Queries
        public DataRow GetById(string table, string idColumnName, object idValue, bool leaveConnectionOpen = true)
        {
            var result = new Query(table)
                            .Where(idColumnName, idValue);
            var compiled = _compiler.Compile(result);

            var dataTable = ExecuteQuery(compiled.Sql, parameters: compiled.Bindings);
            if (dataTable.Rows.Count == 0)
                return null;

            return dataTable.Rows[0];
        }

        public void UpdateById(string table, List<DataKeeper> columns, string idColumn, object idValue, bool saveChanges = false)
        {
            var result = new Query(table)
                            .Update(columns.ToDictionary(k => k.ColumnName, v => v.Value ?? DBNull.Value))
                            .Where(idColumn, idValue);
            var compiled = _compiler.Compile(result);

            ExecuteCommandWithNoResults(compiled.Sql, compiled.Bindings, saveChanges);
        }
        #endregion


        #region Dynamic Query Execution Helpers
        public IEnumerable<T> ExecuteQuery<T>(string queryString, Dictionary<string, object> parameters = null, bool leaveConnectionOpen = true)
        {
            var output = new List<T>();
            if (String.IsNullOrEmpty(queryString))
            {
                throw new ArgumentException(LocalResources.CommandTextCannotBeNullOrEmpty);
            }

            try
            {
                EnsureTransactionExists(_isolationLevelQuery);
                EnsureConnectionOpen();
                output = DbContext.Database.SqlQuery<T>(queryString, ParseParameters(parameters)).ToList();
            }
            finally
            {
                if (!leaveConnectionOpen)
                {
                    FinishTransaction(commit: false);
                }
            }

            return output;
        }
        
        public DataTable ExecuteQuery(string queryString, Dictionary<string, object> parameters = null, bool leaveConnectionOpen = true)
        {
            var results = new DataTable();

            if (String.IsNullOrEmpty(queryString))
            {
                throw new ArgumentException(LocalResources.CommandTextCannotBeNullOrEmpty);
            }

            try
            {
                EnsureTransactionExists(_isolationLevelQuery);
                EnsureConnectionOpen();
                var command = CreateCommand(queryString, parameters);
                using (var dataAdapter = new MySqlDataAdapter(command))
                    dataAdapter.Fill(results);
            }
            finally
            {
                if (!leaveConnectionOpen)
                {
                    FinishTransaction(commit: false);
                }
            }

            return results;
        }
        
        public T ExecuteQueryForValue<T>(string queryString, Dictionary<string, object> parameters = null, bool leaveConnectionOpen = true)
        {
            T result = default(T);
            try
            {
                result = ExecuteQuery<T>(queryString, parameters, leaveConnectionOpen).FirstOrDefault();
            }
            catch(Exception e)
            {
                PromisException.GetFromExistingException(e);
                Console.WriteLine(LocalResources.ErrorHappenedWhenGettingValue, e.ToString());
            }

            if (result == null)
                return default(T);

            return result;
        }

        public long ExecuteCommandWithNoResults(string queryString, Dictionary<string, object> parameters = null, bool saveChanges = false)
        {
            if (String.IsNullOrEmpty(queryString))
            {
                throw new ArgumentException(LocalResources.CommandTextCannotBeNullOrEmpty);
            }

            try
            {
                EnsureTransactionExists(_isolationLevelCommand);
                EnsureConnectionOpen();
                var command = CreateCommand(queryString, parameters);
                command.ExecuteNonQuery();

                if (saveChanges)
                    FinishTransaction();

                return command.LastInsertedId;
            }
            catch(Exception ex)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch { }
                throw ex;
            }
        }
        
        public void ExecuteSqlScript(string queryString, bool saveChanges = false)
        {
            if (String.IsNullOrEmpty(queryString))
            {
                throw new ArgumentException(LocalResources.CommandTextCannotBeNullOrEmpty);
            }

            try
            {
                EnsureTransactionExists(_isolationLevelCommand);
                EnsureConnectionOpen();
                var script = new MySqlScript(_connection, queryString)
                {
                    Delimiter = "//"
                };
                script.Execute();

                if (saveChanges)
                    FinishTransaction();
            }
            catch (Exception ex)
            {
                try
                {
                    FinishTransaction(commit: false);
                }
                catch { }
                throw ex;
            }
        }
        #endregion


        #region Functionalities Internal

        public Compiler GetCompiler()
        {
            return _compiler;
        }

        /// <summary>
        /// Changes the database currently being used.
        /// </summary>
        /// <param name="newDatabase"></param>
        public void ChangeDatabase(string newDatabase)
        {
            CloseConnection(false);
            var connection = new MySqlConnectionStringBuilder(DbContext.Database.Connection.ConnectionString)
            {
                Database = newDatabase
            };
            DbContext.Database.Connection.ConnectionString = connection.ConnectionString;
            DbContext.DatabaseName = newDatabase;
            FinishTransaction(commit: false);
        }

        public void ChangeDbContext(IDbContext newDbContext)
        {
            CloseConnection(false);
            FinishTransaction(commit: false);
            DbContext = null;
            DbContext = newDbContext;
            _connection = (MySqlConnection)DbContext.Database.Connection;
        }

        /// <summary>
        /// Closes a connection if open
        /// </summary>
        public void CloseConnection(bool saveChanges = true)
        {
            if (saveChanges)
                DbContext.SaveChanges();

            if (_connection.State == ConnectionState.Open)
            {
                _connection.Close();
            }
        }

        /// <summary>
        /// Finishes a transaction, disposing the transaction scope object. Can be specified if to commit the transaction or dispose it.
        /// </summary>
        /// <param name="commit">Commit the transaction?</param>
        /// <param name="dispose">Dispose the transaction?</param>
        public void FinishTransaction(bool commit = true, bool dispose = true)
        {
            if (_transaction == null)
            {
                CloseConnection(commit);
                return;
            }
            
            try
            {
                if (commit)
                {
                    CloseConnection();
                    _transaction.Complete();
                }

                if (dispose)
                {
                    if(!commit)
                        CloseConnection(false);
                    try
                    {
                        _transaction.Dispose();
                    }
                    catch { }// dispose if not already disposed
                }
            }
            finally
            {
                _transaction = null;
            }
        }

        /// <summary>
        /// Disposes everything. If everything was executing leaving the connection / transactions open for rapidity, closes them now.
        /// </summary>
        public void Dispose()
        {
            if (DbContext?.Database == null) return;

            // make sure no transaction is left out hanging when services dispose this object
            // also autocommits the transaction
            FinishTransaction();
        }


        /// <summary>
        /// Given a set of parameters as dictionary, transforms them into a list of mysql parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private object[] ParseParameters(Dictionary<string, object> parameters)
        {
            if (parameters == null)
                return Array.Empty<object>();

            var outputList = new List<MySqlParameter>();
            foreach (var param in parameters)
            {
                var parameter = new MySqlParameter
                {
                    ParameterName = param.Key,
                    Value = param.Value ?? DBNull.Value
                };
                outputList.Add(parameter);
            }

            return outputList.ToArray();
        }

        /// <summary>
        /// Creates a MySQLCommand with the given parameters
        /// </summary>
        /// <param name="commandText">The MySQL query to execute</param>
        /// <param name="parameters">Parameters to pass to the MySQL query</param>
        /// <returns></returns>
        private MySqlCommand CreateCommand(string commandText, Dictionary<string, object> parameters)
        {
            MySqlCommand command = _connection.CreateCommand();
            command.CommandText = commandText;
            if (parameters != null)
                AddParameters(command, parameters);

            return command;
        }

        /// <summary>
        /// Adds the parameters to a MySQL command
        /// </summary>
        /// <param name="commandText">The MySQL query to execute</param>
        /// <param name="parameters">Parameters to pass to the MySQL query</param>
        private static void AddParameters(MySqlCommand command, Dictionary<string, object> parameters)
        {
            if (parameters == null)
            {
                return;
            }

            foreach (var param in parameters)
            {
                var parameter = command.CreateParameter();
                parameter.ParameterName = param.Key;
                parameter.Value = param.Value ?? DBNull.Value;
                command.Parameters.Add(parameter);
            }
        }


        /// <summary>
        /// If the transaction scope is null, opens a new one, otherwise leaves it be.
        /// </summary>
        /// <param name="isLvl"></param>
        private void EnsureTransactionExists(System.Transactions.IsolationLevel isLvl)
        {
            if (_transaction == null)
            {
                var transactionOptions = new TransactionOptions()
                {
                    IsolationLevel = isLvl,
                    Timeout = TransactionManager.MaximumTimeout
                };
                _transaction = new TransactionScope(TransactionScopeOption.RequiresNew, transactionOptions, TransactionScopeAsyncFlowOption.Enabled);
            }
        }


        /// <summary>
        /// Opens a connection if not open
        /// </summary>
        private void EnsureConnectionOpen()
        {
            var retries = 3;
            if (_connection.State == ConnectionState.Open)
            {
                return;
            }
            else
            {
                while (retries >= 0 && _connection.State != ConnectionState.Open)
                {
                    _connection.Open();
                    retries--;
                    Thread.Sleep(30);
                }
            }
        }
        #endregion
    }
}
