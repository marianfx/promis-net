﻿using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Entities;
using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using SqlKata;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Promis.NET.DataAccess.Repositories
{
    public class KpiRepository : Repository, IKpiRepository
    {
        public KpiRepository(IDbContext dbContext) : base(dbContext)
        {
        }

        public string GetTableNameSystems()
        {
            return "systems";
        }

        public List<DataKeeper> GetColumnsNecessaryForSystems()
        {
            return new DataKeeper[] {
                new DataKeeper("SYSTEM"),
                new DataKeeper("SYSTEM_DESCRIPTION", "DESCRIPTION")}.ToList();
        }

        #region CompletionOverview
        public double GetTotalMhrsConstruction(string yearCode, string weekCode, string system = null)
        {
            var conditionSystem = "INSTR(act, '_PR') = 0 AND INSTR(act, '_CO') = 0";
            var conditionNonSystem = "LEFT(act, 2) <> 'PR' AND LEFT(act, 2) <> 'CO'";
            return GenericGetMhrsProgress(yearCode, weekCode, conditionSystem, conditionNonSystem, system);
        }

        public double GetTotalMhrsPrecommissioning(string yearCode, string weekCode, string system = null)
        {
            var conditionSystem = "INSTR(act, '_PR') > 0";
            var conditionNonSystem = "act LIKE '%_PR%'";
            return GenericGetMhrsProgress(yearCode, weekCode, conditionSystem, conditionNonSystem, system);
        }

        public double GetTotalMhrsCommissioning(string yearCode, string weekCode, string system = null)
        {
            var conditionSystem = "INSTR(act, '_CO') > 0";
            var conditionNonSystem = "act LIKE '%_CO%'";
            return GenericGetMhrsProgress(yearCode, weekCode, conditionSystem, conditionNonSystem, system);
        }

        private double GenericGetMhrsProgress(string yearCode, string weekCode, string conditionSystem, string conditionNonSystem, string system = null)
        {
            var table = !string.IsNullOrWhiteSpace(system) ? "progress_sys" : "planning";
            var query = new Query(table).SelectRaw("ROUND(SUM(mhrs), 1)");

            query = !string.IsNullOrWhiteSpace(system)
                ? query.Where("w", "=", string.Format("{0}_{1}", yearCode, weekCode))
                        .Where("sys", system)
                        .WhereRaw(conditionSystem)
                : query.Where("type", "P")
                        .WhereRaw(conditionNonSystem);

            return GenericGetMhrs(query);
        }

        public double GetLoopCheckCount(string system = null)
        {
            var query = new Query("loopcheck").Count();
            query = !string.IsNullOrWhiteSpace(system)
                ? query.Where("system", system)
                : query;

            return GenericGetMhrs(query);
        }

        private double GenericGetMhrs(Query query)
        {
            var compiled = _compiler.Compile(query);
            var result = ExecuteQueryForValue<double?>(compiled.Sql, compiled.Bindings);
            return result ?? 0.0;
        }


        public double GetConstructionProgress(string yearCode, string weekCode, double totalMhrs, string system = null)
        {
            var progressTable = "progress";
            var conditionSystem = "INSTR(act, '_PR') = 0 AND INSTR(act, '_CO') = 0";
            return GenericGetProgressForMhrs(yearCode, weekCode, totalMhrs, progressTable, conditionSystem, system);
        }

        public double GetPrecommissioningProgress(string yearCode, string weekCode, double totalMhrs, string system = null)
        {
            var progressTable = "progress_pr";
            var conditionSystem = "INSTR(act, '_PR') > 0";
            return GenericGetProgressForMhrs(yearCode, weekCode, totalMhrs, progressTable, conditionSystem, system);
        }

        public double GetCommissioningProgress(string yearCode, string weekCode, double totalMhrs, string system = null)
        {
            var progressTable = "progress_co";
            var conditionSystem = "INSTR(act, '_CO') > 0";
            return GenericGetProgressForMhrs(yearCode, weekCode, totalMhrs, progressTable, conditionSystem, system);
        }

        private double GenericGetProgressForMhrs(string yearCode, string weekCode, double totalMhrs, string progressTableNonSystem, string conditionSystem, string system = null)
        {
            var table = !string.IsNullOrWhiteSpace(system) ? "progress_sys" : progressTableNonSystem;
            var query = new Query(table).SelectRaw("ROUND(SUM(earned), 1)");
            query = !string.IsNullOrWhiteSpace(system)
                ? query.Where("w", "=", string.Format("{0}_{1}", yearCode, weekCode))
                        .Where("sys", system)
                        .WhereRaw(conditionSystem)
                : query.Where("w", "=", string.Format("{0}_{1}", yearCode, weekCode));

            return GenericGetProgress(totalMhrs, query);
        }

        public double GetLoopCheckProgress(double totalCount, string system = null)
        {
            var maxStepQuery = "SELECT MAX(step) as step FROM precomm_loopcheck WHERE subact <> '' GROUP BY subact LIMIT 1";
            var result = ExecuteQueryForValue<double?>(maxStepQuery);
            if (!result.HasValue)
                return 0;

            var query = new Query("PRECOMM_LOOPCHECK AS A")
                        .Count()
                        .Join("LOOPCHECK AS B", "A.tag", "B.tag")
                        .Where("A.c", "<>", "")
                        .Where("A.step", result.Value);
            query = !string.IsNullOrWhiteSpace(system) ? query.Where("system", system) : query;

            return GenericGetProgress(totalCount, query);
        }

        private double GenericGetProgress(double total, Query query)
        {
            if (total == 0)
                return 0;

            var compiled = _compiler.Compile(query);
            var result = ExecuteQueryForValue<double?>(compiled.Sql, compiled.Bindings);
            if (!result.HasValue)
                return 0;

            return Math.Round(result.Value / total * 100, 1);
        }


        public List<DataKeeper> GetPunchlistStatus(string system = null)
        {
            var join = new Join().On("P.code", "PL.js");
            if (!string.IsNullOrWhiteSpace(system))
                join = join.Where("PL.system", system);

            var query = new Query("pl_status as P")
                            .SelectRaw("P.code as system, COUNT(PL.system) as system_description")
                            .LeftJoin("punchlist as PL", j => join)
                            .GroupBy("P.code")
                            .OrderBy("code");

            var compiled = _compiler.Compile(query);
            var results = ExecuteQuery<system_base>(compiled.Sql, compiled.Bindings);
            var output = new List<DataKeeper>();
            foreach (var item in results)
            {
                int.TryParse(item.SYSTEM_DESCRIPTION, out int number);
                output.Add(new DataKeeper(item.SYSTEM, number));
            }
            return output;
        }

        public List<DataKeeper> GetConstructionStatus(string yearCode, string weekCode, string system = null)
        {
            var nonSystemTable = "progress";
            var ordOperator = "<=";
            var compareMainAct = true;
            return GenericGetStatus(yearCode, weekCode, nonSystemTable, ordOperator, compareMainAct, system);
        }

        public List<DataKeeper> GetPreCommissioningStatus(string yearCode, string weekCode, string system = null)
        {
            var nonSystemTable = "progress_pr";
            var ordOperator = ">";
            var compareMainAct = false;
            string actWhere = "act LIKE '%_PR%'";
            return GenericGetStatus(yearCode, weekCode, nonSystemTable, ordOperator, compareMainAct, system, actWhere);
        }

        public List<DataKeeper> GetCommissioningStatus(string yearCode, string weekCode, string system = null)
        {
            var nonSystemTable = "progress_co";
            var ordOperator = ">";
            var compareMainAct = false;
            string actWhere = "act LIKE '%_CO%'";
            return GenericGetStatus(yearCode, weekCode, nonSystemTable, ordOperator, compareMainAct, system, actWhere);
        }

        private List<DataKeeper> GenericGetStatus(string yearCode, string weekCode, string nonSystemTable, string ordOperator, bool compareMainAct, string system = null, string actWhere = null)
        {
            var output = new List<DataKeeper>();
            var table = !string.IsNullOrWhiteSpace(system) ? "progress_sys" : nonSystemTable;
            var descrColumn = compareMainAct ? "WD.descr" : "P.description";
            var query = new Query("work_disc as WD")
                        .SelectRaw("P.act as system, " + descrColumn + " as system_description, SUM(P.mhrs) as count")
                        .Join("planning as P", "P.act", "P.act")
                        .Where("WD.ord", ordOperator, 16)
                        .Where("WD.flag", "<>", "").Where("P.type", "P").Where("P.mhrs", ">", 0)
                        .GroupBy(!compareMainAct ? "P.act" : "WD.cod")
                        .OrderBy("WD.ord").OrderBy(descrColumn);
            query = compareMainAct 
                ? query.WhereRaw("P.act LIKE CONCAT('%', CONCAT(WD.cod, '%'))") 
                : query.WhereRaw(actWhere);
            var compiled = _compiler.Compile(query);

            var results = ExecuteQuery<system_count>(compiled.Sql, compiled.Bindings);
            foreach (var item in results)
            {
                var _Index = item.SYSTEM != null ? item.SYSTEM.IndexOf('_') + 1 : 0;
                var actWithUnderscoreRemoved = item.SYSTEM?.Substring(_Index);

                var whatToSelect = !string.IsNullOrWhiteSpace(system)
                    ? "ROUND(SUM(earned) / SUM(mhrs) * 100, 1)"
                    : "ROUND(SUM(earned) / " + item.COUNT + " * 100, 1)";//sys_desc is sum(mhrs)
                var newQuery = new Query(table)
                        .SelectRaw(whatToSelect)
                        .Where("w", string.Format("{0}_{1}", yearCode, weekCode));
                if (compareMainAct)
                    newQuery.WhereRaw("LEFT(act, 2) = ?", item.SYSTEM?.Substring(0, 2));
                else
                {
                    newQuery = !string.IsNullOrWhiteSpace(system) 
                        ? newQuery.Where("act", item.SYSTEM) 
                        : newQuery.Where("act", actWithUnderscoreRemoved);// for 'progress_pr/co' tables, remove dash from act
                }
                newQuery = !string.IsNullOrWhiteSpace(system) ? newQuery.Where("sys", system) : newQuery;

                compiled = _compiler.Compile(newQuery);
                var result = ExecuteQueryForValue<double?>(compiled.Sql, compiled.Bindings);
                output.Add(new DataKeeper(item.SYSTEM_DESCRIPTION, result ?? 0.0));
            }

            return output;
        }
        #endregion

        #region Subcontractor Performance Indicators
        public List<subcontractor> GetSubContractors()
        {
            var query = new Query("subcontractors as s")
                       .SelectRaw("s.code,s.subcontractor as subcontractorCol, s.id")
                       .Distinct()
                       .Join("subc_sow as ss", j => j.On("s.code", "ss.code"))
                       .OrderBy("s.subcontractor");

            var compiledQuery = _compiler.Compile(query);
            return ExecuteQuery<subcontractor>(compiledQuery.Sql, compiledQuery.Bindings).ToList();
        }

        public List<DataKeeper> GetColumnsNecessarySOWTable()
        {
            return new DataKeeper[] {
                new DataKeeper("SUB_ACT","SUB-ACTIVITY DESCRIPTION"),
                new DataKeeper("QTY","BUDGET QTY"),
                new DataKeeper("UM")}.ToList();
        }

        public List<List<DataKeeper>> GetRowsForSOWTable(string subcontractorCode)
        {
            var allRows = new List<List<DataKeeper>>();
            var query = new Query("work_def as WD")
                       .Select("SS.sub_act as sub_act", "WD.sub_act_d as sub_activity_description", "BS.qty as budget_qty", "BS.um")
                       .Distinct()
                       .Join("subc_sow as SS", j => j.On("WD.sub_act", "SS.sub_act"))
                       .Join("budget_subact as BS", j => j.On("SS.sub_act", "BS.sub_act"))
                       .Where("SS.code", subcontractorCode)
                       .OrderBy("SS.sub_act");

            var compiledQuery = _compiler.Compile(query);
            var sowList = ExecuteQuery<kpi_sow>(compiledQuery.Sql, compiledQuery.Bindings);
            if (sowList == null)
                return allRows;

            foreach (var sow in sowList)
            {
                var currentRow = new List<DataKeeper>
                {
                    new DataKeeper("SUB-ACTIVITY DESCRIPTION", sow.sub_act+"-"+sow.sub_activity_description),
                    new DataKeeper("BUDGET QTY", sow.budget_qty),
                    new DataKeeper("UM", sow.um)
                };
                allRows.Add(currentRow);
            }
            return allRows;
        }

        public List<cut_offf> GetCutOffData()
        {
            var queryString = new Query("cut_off")
                              .OrderBy("w");
            var compiledQuery = _compiler.Compile(queryString);
            return ExecuteQuery<cut_offf>(compiledQuery.Sql, compiledQuery.Bindings).ToList();
        }


        public double GetTotalQty(string sub_act, string dataTable)
        {
            var queryString = new Query(dataTable)
                             .SelectRaw("SUM(qty*pr/100)")
                             .Where("sub_act", sub_act);
            var compiledQuery = _compiler.Compile(queryString);
            return ExecuteQueryForValue<double?>(compiledQuery.Sql, compiledQuery.Bindings) ?? 0;
        }
          
        public budget_subact GetBudgetForSubact(string sub_act)
        {
            var queryString = new Query("budget_subact")
                       .SelectRaw("sub_act,qty,um, round(prod,2)as 'prod',mhrs, id")
                       .Where("sub_act", sub_act);
            var compiledQuery = _compiler.Compile(queryString);
            return ExecuteQueryForValue<budget_subact>(compiledQuery.Sql, compiledQuery.Bindings);
        }

        public sub_act_qty GetActAndQtyForSubact(string sub_act)
        {
            var queryString = new Query("work_def as WD")
                       .Select("WD.act","BS.qty")
                       .Distinct()
                       .Join("budget_subact as BS", j => j.On("WD.sub_act", "BS.sub_act"))
                       .Where("WD.sub_act", sub_act);
            var compiledQuery = _compiler.Compile(queryString);
            return ExecuteQueryForValue<sub_act_qty>(compiledQuery.Sql, compiledQuery.Bindings);
        }

        public planning GetActivityStartAndEnd(string act)
        {
            var queryString = new Query("planning")
                              .Where("act",act)
                              .Where("type", 'p');
            var compiledQuery = _compiler.Compile(queryString);
            return ExecuteQueryForValue<planning>(compiledQuery.Sql, compiledQuery.Bindings);
        }

        public KeyValuePair<int, int> GetStartEndActivityDates(string startDate, string endDate)
        {
            var startD = startDate.Replace("-", "_");
            var endD= endDate.Replace("-", "_");
            var queryForStartString = new Query("cut_off")
                                     .Count()                      
                                     .WhereRaw("m<'p_"+startD +"'");
            var queryForEndString = new Query("cut_off")
                                    .Count()
                                    .WhereRaw("m<='p_" + endD+"'");
            var compiledStartQuery = _compiler.Compile(queryForStartString);
            var compiledEndQuery = _compiler.Compile(queryForEndString);

            var start= ExecuteQueryForValue<int>(compiledStartQuery.Sql, compiledEndQuery.Bindings);
            var end = ExecuteQueryForValue<int>(compiledEndQuery.Sql, compiledEndQuery.Bindings);
            return new KeyValuePair<int, int>(start, end);
        }
        #endregion

        #region Milestones PDF
        public descriptor GetFullSystemData(string systemCode)
        {
            var systemsQuery = new Query("systems")
                                .SelectRaw("system as code, system_description as description")
                                .Where("system", systemCode)
                                .Limit(1);
            var compiled = _compiler.Compile(systemsQuery);
            return ExecuteQueryForValue<descriptor>(compiled.Sql, compiled.Bindings);
        }

        public Dictionary<string, IEnumerable<datatable_common>> GetDataForSystemCodeFromDatatables(string systemCode)
        {
            var output = new Dictionary<string, IEnumerable<datatable_common>>();
            var query = new Query("promisweb.tabletag_def")
                                .Select("datatable", "description")
                                .OrderBy("pos").OrderBy("description")
                                .Distinct();
            var compiled = _compiler.Compile(query);
            var datatables = ExecuteQuery<tabletab_def_base>(compiled.Sql, compiled.Bindings);

            foreach (var datatable in datatables)
            {
                var querySys = new Query(datatable.datatable)
                                        .Select("tag", "description", "wbs", "system", "dwg")
                                        .Where("system", systemCode)
                                        .OrderBy("tag");
                compiled = _compiler.Compile(querySys);

                try
                {
                    var tableData = ExecuteQuery<datatable_common>(compiled.Sql, compiled.Bindings).ToList();
                    if (tableData == null || tableData.Count() == 0)
                        continue;//ignore empty
                    
                    if (!output.ContainsKey(datatable.description))
                        output.Add(datatable.description, new List<datatable_common>());

                    foreach (var data in tableData)
                    {
                        ((List<datatable_common>)output[datatable.description]).Add(data);
                    }
                }
                catch { }//if failed, table does not exist, so ignore
            }

            return output;
        }
        #endregion
    }
}
