﻿using Promis.NET.DataAccess.Models;
using SqlKata;
using System.Collections.Generic;
using System.Linq;

namespace Promis.NET.DataAccess.Extensions
{
    public static class QueryExtensions
    {
        /// <summary>
        /// Turns a list of Where Conditions into a WHERE MySQL query and returns it as an SqlKata object (that needs to be preinitialized with the table).
        /// </summary>
        /// <param name="list"></param>
        /// <param name="query"></param>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        public static Query ToWhereQuery(this List<WhereCondition> list, Query query, string dataTable)
        {
            if (query == null)
                throw new System.NullReferenceException("Query cannot be null.");

            if (list.Count == 0)
                return query;
            
            for (int i = 0; i < list.Count; i++)
            {
                var condition = list[i];
                var columnName = condition.LeftOperand.Contains(".") ? condition.LeftOperand.ToLower() : dataTable.ToLower() + "." + condition.LeftOperand.ToLower();
                if (new string[] { "eq", "neq", "=", "<>", ">", "<" }.Contains(condition.Operator))
                {
                    condition.Operator = condition.Operator == "eq" ? "=" : condition.Operator;
                    condition.Operator = condition.Operator == "neq" ? "<>" : condition.Operator;
                    query = query.Where(columnName, condition.Operator, condition.RightOperand);
                }
                else if(condition.Operator == "inc")
                {
                    query = query.WhereContains(columnName, condition.RightOperand.ToString().ToUpper());
                }
                else if(condition.Operator == "like")
                {
                    query = query.WhereLike(columnName, condition.RightOperand.ToString().ToUpper());
                }
            }

            return query;
        }

        /// <summary>
        /// Turns a list of JOIN Conditions into a JOIN MySQL query and returns it as an SqlKata object (that needs to be preinitialized with the table).
        /// </summary>
        /// <param name="list"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public static Query ToJoinQuery(this List<JoinCondition> list, Query query)
        {
            if (query == null)
                throw new System.NullReferenceException("Query cannot be null.");

            if (list.Count == 0)
                return query;
            
            for (int i = 0; i < list.Count; i++)
            {
                var joinCondition = list[i];
                if (new string[] { "eq", "neq", "=", "<>" }.Contains(joinCondition.Operator))
                {
                    joinCondition.Operator = (joinCondition.Operator == "eq" || joinCondition.Operator == "=" ? "=" : "<>");
                    query = query.Join(joinCondition.DataTable, joinCondition.LeftColumn, joinCondition.RightColumn, joinCondition.Operator, joinCondition.JoinType.ToLower());
                }
            }

            return query;
        }

        /// <summary>
        /// Given a string with some text and a list of columns for a table, and a query predefined for that table, ads in the WHERE condition the conditions that each column can contain a value (OR clause)
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="query"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public static Query ToSearchInEveryColumn(this string searchString, Query query, IEnumerable<string> columns)
        {
            if (query == null)
                throw new System.NullReferenceException("Query cannot be null.");

            searchString = searchString ?? "";
            searchString = searchString.Trim();
            if (string.IsNullOrWhiteSpace(searchString))
                return query;

            var searchQuery = new Query();
            foreach (var column in columns)
            {
                if (!column.EndsWith(".id"))
                {
                    searchQuery = searchQuery.OrWhereContains(column, searchString);
                }
            }
            query = query.Where(q => searchQuery);

            return query;
        }

        /// <summary>
        /// Given a datatable, a query object and a list of columns, adds in the query object an select statement with the given columns. Columns from the main table will be selected by their name (because they are main columns) and the ones from the JOINED tables will be selected with alias 'table.column'.
        /// </summary>
        /// <param name="columns"></param>
        /// <param name="query"></param>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        public static Query ToSelectColumns(this List<string> columns, Query query, string dataTable)
        {
            if (query == null)
                throw new System.NullReferenceException("Query cannot be null.");

            if (string.IsNullOrWhiteSpace(dataTable) || columns.Count == 0)
                return query;

            var loweredDtString = dataTable.ToLower() + ".";
            foreach (var column in columns)
            {
                var isFromMainTable = column.StartsWith(loweredDtString);
                query = query.Select(column + (isFromMainTable ? "" : " AS " + column));
            }

            return query;
        }

        /// <summary>
        /// Given an object that holds information for ordering for a datatable, builds up the query to do that, using the list of columns.
        /// </summary>
        /// <param name="orders"></param>
        /// <param name="query"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public static Query ToOrderBy(this List<DataTableOrder> orders, Query query, List<string> columns)
        {
            if (query == null)
                throw new System.NullReferenceException("Query cannot be null.");

            if (columns.Count == 0)
                return query;

            if(orders.Count == 0)
            {
                query = query.OrderBy(columns[0], true);
                return query;
            }

            foreach (var orderObject in orders)
            {
                // check if the order exists
                var exists = orderObject.Column >= 0 && orderObject.Column < columns.Count;
                var isAscOrDesc = orderObject.Dir.ToLower() == "asc" || orderObject.Dir.ToLower() == "desc";

                if (!exists)
                    continue;

                if (!isAscOrDesc)
                    continue;

                if (!string.IsNullOrWhiteSpace(orderObject.AdvancedFunction))
                    query = query.OrderByRaw(orderObject.AdvancedFunction + " " + orderObject.Dir);
                else
                {
                    var columnName = columns[orderObject.Column];
                    query = query.OrderBy(columnName, orderObject.Dir);
                }
            }

            return query;
        }

        /// <summary>
        /// Given an object that holds information for grouping for a datatable, builds up the query to do that, using the list of columns.
        /// </summary>
        /// <param name="orders"></param>
        /// <param name="query"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public static Query ToGroupBy(this List<string> groupColumns, Query query, string dataTable, List<string> columns)
        {
            if (query == null)
                throw new System.NullReferenceException("Query cannot be null.");

            if (columns.Count == 0)
                return query;

            if (groupColumns.Count == 0)
                return query;

            foreach (var groupObject in groupColumns)
            {
                // check if the order exists
                var tempColName = groupObject.Contains(".") ? groupObject.ToLower() : dataTable + "." + groupObject.ToLower();
                var exists = columns.Contains(tempColName);

                if (!exists)
                    continue;

                if (!string.IsNullOrWhiteSpace(groupObject))
                    query = query.GroupBy(tempColName);
            }

            return query;
        }
    }
}
