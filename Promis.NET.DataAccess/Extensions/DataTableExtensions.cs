﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Promis.NET.Utilities.Models;

namespace Promis.NET.DataAccess.Extensions
{
    public static class DataTableExtensions
    {
        public static IList<T> ToList<T>(this DataTable table) where T : class, new()
        {
            IList<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            IList<T> result = new List<T>();

            try
            {
                foreach (var row in table.Rows)
                {
                    var item = CreateItemFromRow<T>((DataRow)row, properties);
                    result.Add(item);
                }
            }
            catch(Exception)
            {
                throw new Exception("Cannot convert the data receive from the database into the specified  type (" + typeof(T).Name + ").");
            }

            return result;
        }

        private static T CreateItemFromRow<T>(DataRow row, IList<PropertyInfo> properties) where T : new()
        {
            T item = new T();
            foreach (var property in properties)
            {
                var val = row[property.Name];
                if(val != null && DBNull.Value != val)
                    property.SetValue(item, row[property.Name], null);
            }
            return item;
        }
        
        public static T ToEntity<T>(this DataRow row) where T : new()
        {
            IList<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            return CreateItemFromRow<T>(row, properties);
        }

        public static List<List<DataKeeper>> ToDataKeeperList(this DataTable table, List<string> columnList = null)
        {
            var output = new List<List<DataKeeper>>();
            var rowsRead = table.Rows.Count;
            var columns = (columnList ?? table.Columns.Cast<DataColumn>().Select(c => c.ColumnName).ToList());

            for (int i = 0; i < rowsRead; i++)
            {
                var row = table.Rows[i];
                var rowData = new List<DataKeeper>();
                foreach (var c in columns)
                {
                    rowData.Add(new DataKeeper(c, row[c]));
                }
                output.Add(rowData);
            }

            return output;
        }
    }
}
