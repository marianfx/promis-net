﻿

namespace Promis.NET.DataAccess.Models
{
    public class DataTableOrder
    {
        public int Column { get; set; } = 0;
        public string Dir { get; set; } = "asc";
        public string AdvancedFunction { get; set; } = "";
    }
}
