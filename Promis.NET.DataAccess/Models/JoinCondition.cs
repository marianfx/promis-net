﻿
namespace Promis.NET.DataAccess.Models
{
    public class JoinCondition
    {
        public string DataTable { get; set; } = string.Empty;
        public string LeftColumn { get; set; } = string.Empty;
        public string RightColumn { get; set; } = string.Empty;
        public string Operator { get; set; } = "=";
        public string JoinType { get; set; } = "INNER";


        public JoinCondition()
        {

        }
    }
}
