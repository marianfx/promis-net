﻿namespace Promis.NET.DataAccess.Models
{
    public class DataTableSearch
    {
        public string Value { get; set; } = "";
        public bool Regex { get; set; } = false;
    }
}
