﻿namespace Promis.NET.DataAccess.Models
{
    public class WhereCondition
    {
        public string LeftOperand { get; set; } = string.Empty;
        public object RightOperand { get; set; } = string.Empty;
        public string Operator { get; set; } = "=";

        public WhereCondition()
        {

        }

        public WhereCondition(string columnName, object expectedValue, string op): this()
        {
            LeftOperand = columnName;
            RightOperand = expectedValue;
            Operator = op;
        }

        /// <summary>
        /// Inits a condition with the operator set to = and the logical operand to AND
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="expectedValue"></param>
        public WhereCondition(string columnName, object expectedValue): this(columnName, expectedValue, "=")
        {

        }
        
    }
}
