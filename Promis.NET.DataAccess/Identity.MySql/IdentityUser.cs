﻿using Microsoft.AspNet.Identity;
using System;

namespace Promis.NET.DataAccess.Identity.MySql
{
    /// <summary>
    /// Class that implements the ASP.NET Identity
    /// IUser interface 
    /// </summary>
    public class IdentityUser : IUser
    {
        /// <summary>
        /// Default constructor 
        /// </summary>
        public IdentityUser()
        {
            Id = String.Empty;
        }

        /// <summary>
        /// Constructor that takes user name as argument
        /// </summary>
        /// <param name="userName"></param>
        public IdentityUser(string userName)
            : this()
        {
            UserName = userName;
        }

        /// <summary>
        /// User ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// User's name
        /// </summary>
        public string UserName { get; set; }
        

        public string ProjectId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string Language { get; set; }
        public string Company { get; set; }
        public string MobilePhone { get; set; }
        public string OfficePhone { get; set; }

        public bool HasDoc { get; set; }
        public string PhotoUrl { get; set; }

        public string RolesModulesString { get; set; } = String.Empty;
        public string RolesActivitiesString { get; set; } = String.Empty;
        public string RolesResponsabilitiesString { get; set; } = String.Empty;
    }
}
