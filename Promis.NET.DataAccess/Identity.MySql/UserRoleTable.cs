﻿using System;
using System.Collections.Generic;

namespace Promis.NET.DataAccess.Identity.MySql
{
    /// <summary>
    /// Class that represents the UserRoles table in the MySQL Database
    /// </summary>
    public class UserRolesTable
    {
        private MySQLDatabase _database;

        /// <summary>
        /// Constructor that takes a MySQLDatabase instance 
        /// </summary>
        /// <param name="database"></param>
        public UserRolesTable(MySQLDatabase database)
        {
            _database = database;
        }

        /// <summary>
        /// Returns a list of user's roles
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public List<string> FindByUserId(string userId)
        {
            List<string> roles = new List<string>();
            string commandText = "SELECT Modules, Activities, Resp FROM userfulllist WHERE id = @userId LIMIT 1";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@userId", userId);

            var rows = _database.Query(commandText, parameters);

            var RolesModulesString = !string.IsNullOrWhiteSpace(rows[0]["Modules"] as String) ? rows[0]["Modules"] as String : "";
            var RolesActivitiesString = !string.IsNullOrWhiteSpace(rows[0]["Activities"] as String) ? rows[0]["Activities"] as String : "";
            var RolesResponsabilitiesString = !string.IsNullOrWhiteSpace(rows[0]["Resp"] as String) ? rows[0]["Resp"] as String : "";
            var rolesManager = new RolesManager(RolesModulesString, RolesActivitiesString, RolesResponsabilitiesString);

            foreach (var role in rolesManager.RolesAll)
            {
                roles.Add(role);
            }

            return roles;
        }

        /// <summary>
        /// Deletes all roles from a user in the UserRoles table
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public int Delete(string userId)
        {
            string commandText = "UPDATE userfulllist SET Modules = null, Activities = null, Resp = null WHERE id = @userId";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("UserId", userId);

            return _database.Execute(commandText, parameters);
        }

        /// <summary>
        /// Inserts a new role for a user in the UserRoles table
        /// </summary>
        /// <param name="user">The User</param>
        /// <param name="roleId">The Role's id</param>
        /// <returns></returns>
        public int Insert(IdentityUser user, string roleId)
        {
            var rolesManager = new RolesManager(user.RolesModulesString, user.RolesActivitiesString, user.RolesResponsabilitiesString);
            rolesManager.AddRole(roleId);

            var userTable = new UserTable<IdentityUser>(_database);
            user.RolesModulesString = new string(rolesManager.RolesModulesString.ToCharArray());
            user.RolesActivitiesString = new string(rolesManager.RolesActivitiesString.ToCharArray());
            user.RolesResponsabilitiesString = new string(rolesManager.RolesResponsabilitiesString.ToCharArray());

            return userTable.Update(user);
        }
    }
}
