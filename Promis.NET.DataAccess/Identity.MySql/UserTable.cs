﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Promis.NET.DataAccess.Identity.MySql
{
    /// <summary>
    /// Class that represents the Users table in the MySQL Database
    /// </summary>
    public class UserTable<TUser>
        where TUser : IdentityUser
    {
        private MySQLDatabase _database;

        /// <summary>
        /// Constructor that takes a MySQLDatabase instance 
        /// </summary>
        /// <param name="database"></param>
        public UserTable(MySQLDatabase database)
        {
            _database = database;
        }

        /// <summary>
        /// Returns the user's name given a user id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetUserName(long userId)
        {
            string commandText = "SELECT userid FROM userfulllist WHERE id = @id";
            Dictionary<string, object> parameters = new Dictionary<string, object>() { { "@id", userId } };

            return _database.GetStrValue(commandText, parameters);
        }

        /// <summary>
        /// Returns a User ID given a user name
        /// </summary>
        /// <param name="userName">The user's name</param>
        /// <returns></returns>
        public long GetUserId(string userName)
        {
            string commandText = "SELECT id FROM userfulllist WHERE userid = @name LIMIT 1";
            Dictionary<string, object> parameters = new Dictionary<string, object>() { { "@name", userName } };

            return long.Parse(_database.GetStrValue(commandText, parameters));
        }

        /// <summary>
        /// Returns an TUser given the user's id
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public TUser GetUserById(string userId)
        {
            TUser user = null;
            string commandText = "SELECT * FROM userfulllist WHERE id = @id LIMIT 1";
            Dictionary<string, object> parameters = new Dictionary<string, object>() { { "@id", userId } };

            var rows = _database.Query(commandText, parameters);
            if (rows == null || rows.Count == 0)
                return null;
            
            var row = rows[0];
            user = (TUser)Activator.CreateInstance(typeof(TUser));
            user.Id = row["id"]?.ToString();
            user.UserName = row["userid"]?.ToString();
            var passBytes = row["pswb"] as byte[];
            user.Password = Encoding.UTF8.GetString(passBytes);
            user.ProjectId = string.IsNullOrEmpty(row["proj"]?.ToString()) ? null : row["proj"]?.ToString();
            user.Email = string.IsNullOrEmpty(row["email"]?.ToString()) ? null : row["email"]?.ToString();
            user.FirstName = string.IsNullOrEmpty(row["Name"]?.ToString()) ? null : row["Name"]?.ToString();
            user.LastName = string.IsNullOrEmpty(row["Surname"]?.ToString()) ? null : row["Surname"]?.ToString();
            user.Country = string.IsNullOrEmpty(row["Country"]?.ToString()) ? null : row["Country"]?.ToString();
            user.Language = string.IsNullOrEmpty(row["lang"]?.ToString()) ? null : row["lang"]?.ToString();
            user.Company = string.IsNullOrEmpty(row["company"]?.ToString()) ? null : row["company"]?.ToString();
            user.MobilePhone = string.IsNullOrEmpty(row["mobile"]?.ToString()) ? null : row["mobile"]?.ToString();
            user.OfficePhone = string.IsNullOrEmpty(row["office"]?.ToString()) ? null : row["office"]?.ToString();
            user.HasDoc = string.IsNullOrEmpty(row["doc"]?.ToString()) ? false : (row["doc"]?.ToString() == "y" ? true : false);
            user.PhotoUrl = string.IsNullOrEmpty(row["photo"]?.ToString()) ? null : row["photo"]?.ToString();
            user.RolesModulesString = string.IsNullOrEmpty(row["Modules"]?.ToString()) ? String.Empty : row["Modules"]?.ToString();
            user.RolesActivitiesString = string.IsNullOrEmpty(row["Activities"]?.ToString()) ? String.Empty : row["Activities"]?.ToString();
            user.RolesResponsabilitiesString = string.IsNullOrEmpty(row["Resp"]?.ToString()) ? String.Empty : row["Resp"]?.ToString();
            return user;
        }

        /// <summary>
        /// Returns a list of TUser instances given a user name
        /// </summary>
        /// <param name="userName">User's name</param>
        /// <returns></returns>
        public List<TUser> GetUserByName(string userName)
        {
            List<TUser> users = new List<TUser>();
            string commandText = "SELECT * FROM userfulllist WHERE userid = @name";
            Dictionary<string, object> parameters = new Dictionary<string, object>() { { "@name", userName } };

            var rows = _database.Query(commandText, parameters);
            foreach (var row in rows)
            {
                TUser user = (TUser)Activator.CreateInstance(typeof(TUser));
                user.Id = row["id"]?.ToString();
                user.UserName = row["userid"]?.ToString();
                var passBytes = row["pswb"] as byte[];
                user.Password = Encoding.UTF8.GetString(passBytes);
                user.ProjectId = string.IsNullOrEmpty(row["proj"]?.ToString()) ? null : row["proj"]?.ToString();
                user.Email = string.IsNullOrEmpty(row["email"]?.ToString()) ? null : row["email"]?.ToString();
                user.FirstName = string.IsNullOrEmpty(row["Name"]?.ToString()) ? null : row["Name"]?.ToString();
                user.LastName = string.IsNullOrEmpty(row["Surname"]?.ToString()) ? null : row["Surname"] ?.ToString();
                user.Country = string.IsNullOrEmpty(row["Country"] ?.ToString()) ? null : row["Country"] ?.ToString();
                user.Language = string.IsNullOrEmpty(row["lang"] ?.ToString()) ? null : row["lang"] ?.ToString();
                user.Company = string.IsNullOrEmpty(row["company"]?.ToString()) ? null : row["company"]?.ToString();
                user.MobilePhone = string.IsNullOrEmpty(row["mobile"]?.ToString()) ? null : row["mobile"]?.ToString();
                user.OfficePhone = string.IsNullOrEmpty(row["office"]?.ToString()) ? null : row["office"]?.ToString();
                user.HasDoc = string.IsNullOrEmpty(row["doc"]?.ToString()) ? false : (row["doc"]?.ToString() == "y" ? true : false);
                user.PhotoUrl = string.IsNullOrEmpty(row["photo"]?.ToString()) ? null : row["photo"]?.ToString();
                user.RolesModulesString = string.IsNullOrEmpty(row["Modules"]?.ToString()) ? String.Empty : row["Modules"]?.ToString();
                user.RolesActivitiesString = string.IsNullOrEmpty(row["Activities"]?.ToString()) ? String.Empty : row["Activities"]?.ToString();
                user.RolesResponsabilitiesString = string.IsNullOrEmpty(row["Resp"]?.ToString()) ? String.Empty : row["Resp"]?.ToString();
                users.Add(user);
            }

            return users;
        }

        /// <summary>
        /// Returns an user by searching for username & project Id combination
        /// </summary>
        /// <param name="username"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public TUser GetUserByNameAndProjectId(string username, string projectId)
        {
            var possibleUsers = GetUserByName(username);
            if (possibleUsers == null || possibleUsers.Count == 0)
                return null;

            return possibleUsers.Where(u => u.ProjectId == projectId).FirstOrDefault();
        }



        /// <summary>
        /// Returns a list of TUser instances given a user name
        /// </summary>
        /// <param name="userName">User's name</param>
        /// <returns></returns>
        public List<TUser> GetAllUsersNonAdminsForProject(string projectId)
        {
            List<TUser> users = new List<TUser>();
            string commandText = "SELECT * FROM userfulllist WHERE proj = @proj AND Resp <> 'A'";
            Dictionary<string, object> parameters = new Dictionary<string, object>() { { "@proj", projectId } };

            var rows = _database.Query(commandText, parameters);
            foreach (var row in rows)
            {
                TUser user = (TUser)Activator.CreateInstance(typeof(TUser));
                user.Id = row["id"]?.ToString();
                user.UserName = row["userid"]?.ToString();
                var passBytes = row["pswb"] as byte[];
                user.Password = Encoding.UTF8.GetString(passBytes);
                user.ProjectId = string.IsNullOrEmpty(row["proj"]?.ToString()) ? null : row["proj"]?.ToString();
                user.Email = string.IsNullOrEmpty(row["email"]?.ToString()) ? null : row["email"]?.ToString();
                user.FirstName = string.IsNullOrEmpty(row["Name"]?.ToString()) ? null : row["Name"]?.ToString();
                user.LastName = string.IsNullOrEmpty(row["Surname"]?.ToString()) ? null : row["Surname"]?.ToString();
                user.Country = string.IsNullOrEmpty(row["Country"]?.ToString()) ? null : row["Country"]?.ToString();
                user.Language = string.IsNullOrEmpty(row["lang"]?.ToString()) ? null : row["lang"]?.ToString();
                user.Company = string.IsNullOrEmpty(row["company"]?.ToString()) ? null : row["company"]?.ToString();
                user.MobilePhone = string.IsNullOrEmpty(row["mobile"]?.ToString()) ? null : row["mobile"]?.ToString();
                user.OfficePhone = string.IsNullOrEmpty(row["office"]?.ToString()) ? null : row["office"]?.ToString();
                user.HasDoc = string.IsNullOrEmpty(row["doc"]?.ToString()) ? false : (row["doc"]?.ToString() == "y" ? true : false);
                user.PhotoUrl = string.IsNullOrEmpty(row["photo"]?.ToString()) ? null : row["photo"]?.ToString();
                user.RolesModulesString = string.IsNullOrEmpty(row["Modules"]?.ToString()) ? String.Empty : row["Modules"]?.ToString();
                user.RolesActivitiesString = string.IsNullOrEmpty(row["Activities"]?.ToString()) ? String.Empty : row["Activities"]?.ToString();
                user.RolesResponsabilitiesString = string.IsNullOrEmpty(row["Resp"]?.ToString()) ? String.Empty : row["Resp"]?.ToString();
                users.Add(user);
            }

            return users;
        }

        public List<TUser> GetUserByEmail(string email)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Return the user's password hash
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public string GetPasswordHash(string userId)
        {
            string commandText = "SELECT pswb FROM userfulllist WHERE id = @id";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@id", userId);

            var bytes = _database.GetBlobValue(commandText, parameters);
            var passHash = Encoding.UTF8.GetString(bytes);
            if (string.IsNullOrEmpty(passHash))
            {
                return null;
            }

            return passHash;
        }

        /// <summary>
        /// Sets the user's password hash
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="passwordHash"></param>
        /// <returns></returns>
        public int SetPasswordHash(string userId, string passwordHash)
        {
            string commandText = "UPDATE userfulllist SET pswb = @pwdHash WHERE id = @id";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@pwdHash", Encoding.UTF8.GetBytes(passwordHash));
            parameters.Add("@id", userId);

            return _database.Execute(commandText, parameters);
        }

        /// <summary>
        /// Inserts a new user in the Users table
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int Insert(TUser user)
        {
            string commandText = @"INSERT INTO userfulllist (userid, pswb, proj, email, Name, Surname, Country, lang, company, mobile, office, doc, photo, Modules, Activities, Resp)
                VALUES (@userid, @pswb, @proj, @email, @Name, @Surname, @Country, @lang, @company, @mobile, @office, @doc, @photo, @Modules, @Activities, @Resp)";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@userid", user.UserName);
            parameters.Add("@pswb", Encoding.UTF8.GetBytes(user.Password));
            parameters.Add("@proj", user.ProjectId);
            parameters.Add("@email", user.Email);
            parameters.Add("@Name", user.FirstName);
            parameters.Add("@Surname", user.LastName);
            parameters.Add("@Country", user.Country);
            parameters.Add("@lang", user.Language);
            parameters.Add("@company", user.Company);
            parameters.Add("@mobile", user.MobilePhone);
            parameters.Add("@office", user.OfficePhone);
            parameters.Add("@doc", user.HasDoc ? "y" : "n");
            parameters.Add("@photo", user.PhotoUrl);
            parameters.Add("@Modules", user.RolesModulesString);
            parameters.Add("@Activities", user.RolesActivitiesString);
            parameters.Add("@Resp", user.RolesResponsabilitiesString);

            user.Id = _database.ExecuteGettingLastInsertId(commandText, parameters).ToString();
            return 1;
        }

        /// <summary>
        /// Deletes a user from the Users table
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        private int Delete(string userId)
        {
            string commandText = "DELETE FROM userfulllist WHERE id = @userId";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@userId", userId);

            return _database.Execute(commandText, parameters);
        }

        /// <summary>
        /// Deletes a user from the Users table
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int Delete(TUser user)
        {
            if (user == null || user.Id == null)
                throw new ArgumentNullException();

            return Delete(user.Id);
        }

        /// <summary>
        /// Updates a user in the Users table
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int Update(TUser user)
        {
            if (user == null || user.Id == null)
                throw new ArgumentNullException();

            string commandText = @"UPDATE userfulllist SET userid = @userid, pswb = @pswb, proj = @proj, 
                email = @email, Name = @Name, Surname = @Surname, Country = @Country,
                lang = @lang, company = @company, mobile = @mobile, office = @office, doc = @doc, photo = @photo, Modules = @Modules, Activities = @Activities, Resp = @Resp  
                WHERE id = @id";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@id", user.Id);
            parameters.Add("@userid", user.UserName);
            parameters.Add("@pswb", Encoding.UTF8.GetBytes(user.Password));
            parameters.Add("@proj", user.ProjectId);
            parameters.Add("@email", user.Email);
            parameters.Add("@Name", user.FirstName);
            parameters.Add("@Surname", user.LastName);
            parameters.Add("@Country", user.Country);
            parameters.Add("@lang", user.Language);
            parameters.Add("@company", user.Company);
            parameters.Add("@mobile", user.MobilePhone);
            parameters.Add("@office", user.OfficePhone);
            parameters.Add("@doc", user.HasDoc ? "y" : "n");
            parameters.Add("@photo", user.PhotoUrl);
            parameters.Add("@Modules", user.RolesModulesString);
            parameters.Add("@Activities", user.RolesActivitiesString);
            parameters.Add("@Resp", user.RolesResponsabilitiesString);

            return _database.Execute(commandText, parameters);
        }
    }
}
