using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Promis.NET.Utilities.Roles;
    
namespace Promis.NET.Roles {
    public class LocalRoles {

                
        /// <summary>Module Read: Construction & Engineering Database</summary>
        public const string ModuleReadConstructionEngineeringDatabase = "M_DB_R";
            
        /// <summary>Module Write: Construction & Engineering Database</summary>
        public const string ModuleWriteConstructionEngineeringDatabase = "M_DB_W";
            
        /// <summary>Module Read: Certification Management</summary>
        public const string ModuleReadCertificationManagement = "M_CM_R";
            
        /// <summary>Module Write: Certification Management</summary>
        public const string ModuleWriteCertificationManagement = "M_CM_W";
            
        /// <summary>Module Read: Preservation Planning</summary>
        public const string ModuleReadPreservationPlanning = "M_PRE_R";
            
        /// <summary>Module Write: Preservation Planning</summary>
        public const string ModuleWritePreservationPlanning = "M_PRE_W";
            
        /// <summary>Module Read: Site Activities</summary>
        public const string ModuleReadSiteActivities = "M_SA_R";
            
        /// <summary>Module Write: Site Activities</summary>
        public const string ModuleWriteSiteActivities = "M_SA_W";
            
        /// <summary>Module Read: Planning & Progress</summary>
        public const string ModuleReadPlanningProgress = "M_PP_R";
            
        /// <summary>Module Write: Planning & Progress</summary>
        public const string ModuleWritePlanningProgress = "M_PP_W";
            
        /// <summary>Module Read: Pre/Comm Inspections</summary>
        public const string ModuleReadPrecommInspections = "M_PC_R";
            
        /// <summary>Module Write: Pre/Comm Inspections</summary>
        public const string ModuleWritePrecommInspections = "M_PC_W";
            
        /// <summary>Module Read: Project Completion KPIs</summary>
        public const string ModuleReadProjectCompletionKpis = "M_KPI_R";
            
        /// <summary>Module Write: Project Completion KPIs</summary>
        public const string ModuleWriteProjectCompletionKpis = "M_KPI_W";
            
        /// <summary>Module Read: Mobile Suite</summary>
        public const string ModuleReadMobileSuite = "M_APP_R";
            
        /// <summary>Module Write: Mobile Suite</summary>
        public const string ModuleWriteMobileSuite = "M_APP_W";
            
        /// <summary>Responsability: Admin</summary>
        public const string ResponsabilityAdmin = "R_A";
            
        /// <summary>Responsability: Contractor</summary>
        public const string ResponsabilityContractor = "R_C";
            
        /// <summary>Responsability: Client</summary>
        public const string ResponsabilityClient = "R_O";
            
        /// <summary>Responsability: Subcontractor - CONSER</summary>
        public const string ResponsabilitySubcontractorConser = "R_S_CONS";
            
        /// <summary>Responsability: Subcontractor - INVEST</summary>
        public const string ResponsabilitySubcontractorInvest = "R_S_INV";
            
        /// <summary>Responsability: Subcontractor - SALA</summary>
        public const string ResponsabilitySubcontractorSala = "R_S_SL";
            
        /// <summary>Responsability: Subcontractor - SICES</summary>
        public const string ResponsabilitySubcontractorSices = "R_S_SIC";
            
        /// <summary>Responsability: Subcontractor - SITIE</summary>
        public const string ResponsabilitySubcontractorSitie = "R_S_SITIE";
            
        /// <summary>Responsability: Subcontractor - SOFMAN</summary>
        public const string ResponsabilitySubcontractorSofman = "R_S_SOF";
            
        /// <summary>Responsability: Subcontractor - SOLHYDRO</summary>
        public const string ResponsabilitySubcontractorSolhydro = "R_S_SOL";
            
        /// <summary>Responsability: Subcontractor - STEELCON</summary>
        public const string ResponsabilitySubcontractorSteelcon = "R_S_STCON";
            
        /// <summary>Responsability: Subcontractor - STRABAG</summary>
        public const string ResponsabilitySubcontractorStrabag = "R_S_STRBG";
            
        /// <summary>Activity Access: SOIL PREPARATION</summary>
        public const string ActivityAccessSoilPreparation = "A_SP";
            
        /// <summary>Activity Access: PILES</summary>
        public const string ActivityAccessPiles = "A_PI";
            
        /// <summary>Activity Access: PIPING PREFABRICATION</summary>
        public const string ActivityAccessPipingPrefabrication = "A_PP";
            
        /// <summary>Activity Access: UNDERGROUND</summary>
        public const string ActivityAccessUnderground = "A_UG";
            
        /// <summary>Activity Access: CONCRETE WORKS</summary>
        public const string ActivityAccessConcreteWorks = "A_CW";
            
        /// <summary>Activity Access: BUILDINGS</summary>
        public const string ActivityAccessBuildings = "A_BD";
            
        /// <summary>Activity Access: TANKS</summary>
        public const string ActivityAccessTanks = "A_TK";
            
        /// <summary>Activity Access: ELECTRICAL</summary>
        public const string ActivityAccessElectrical = "A_EL";
            
        /// <summary>Activity Access: STEEL STRUCTURES</summary>
        public const string ActivityAccessSteelStructures = "A_SS";
            
        /// <summary>Activity Access: PIPING COMPONENTS</summary>
        public const string ActivityAccessPipingComponents = "A_PC";
            
        /// <summary>Activity Access: PIPING ABOVEGROUND</summary>
        public const string ActivityAccessPipingAboveground = "A_AG";
            
        /// <summary>Activity Access: MECHANICAL</summary>
        public const string ActivityAccessMechanical = "A_MC";
            
        /// <summary>Activity Access: INSTRUMENTS</summary>
        public const string ActivityAccessInstruments = "A_IN";
            
        /// <summary>Activity Access: PAINTING</summary>
        public const string ActivityAccessPainting = "A_PA";
            
        /// <summary>Activity Access: INSULATION</summary>
        public const string ActivityAccessInsulation = "A_IS";
            
        /// <summary>Activity Access: FIRE FIGHTING</summary>
        public const string ActivityAccessFireFighting = "A_FP";
            
        /// <summary>Activity Access: PRECOMMISSIONING</summary>
        public const string ActivityAccessPrecommissioning = "A_PR";
            
        /// <summary>Activity Access: COMMISSIONING</summary>
        public const string ActivityAccessCommissioning = "A_CO";
            
        /// <summary>Activity Access: START-UP</summary>
        public const string ActivityAccessStartup = "A_SU";

        public static bool IsResponsabilitySubcontractor(string role)
        {
            return role.StartsWith("R_S");
        }
        public static string[] GetAllModuleRoles(bool getOnlyRead = false, bool getOnlyWrite = false)
        {
            var output = new List<string>() { ResponsabilityAdmin };
            var properties = typeof(LocalRoles).GetFields().Where(p => p.Name.StartsWith("Module")).ToDictionary(p => p.Name, p => p.GetValue(null).ToString());
            foreach (var property in properties)
            {
                if (getOnlyRead)
                {
                    if (property.Key.Contains("Read"))
                        output.Add(property.Value);
                    continue;
                }

                if (getOnlyWrite)
                {
                    if(property.Key.Contains("Write"))
                        output.Add(property.Value);
                    continue;
                }

                // else add all
                output.Add(property.Value);
            }
            return output.ToArray();
        }

        }        
}
