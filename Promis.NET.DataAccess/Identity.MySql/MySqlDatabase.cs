﻿using Promis.NET.DataAccess.DbContexts;
using Promis.NET.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Data;

namespace Promis.NET.DataAccess.Identity.MySql
{
    /// <summary>
    /// Class that encapsulates a MySQL database connections 
    /// and CRUD operations
    /// </summary>
    public class MySQLDatabase : IDisposable
    {
        private Repository _repository;

        /// Default constructor which uses the "ApplicationDbContext" connectionString
        /// </summary>
        public MySQLDatabase()
        {
            var useLocalDatabase = ApplicationDbContext.useLocal;
            _repository = new Repository(new PromisWebDbContext(useLocalDatabase));
        }

        /// <summary>
        /// Executes a non-query MySQL statement
        /// </summary>
        /// <param name="commandText">The MySQL query to execute</param>
        /// <param name="parameters">Optional parameters to pass to the query</param>
        /// <returns>The count of records affected by the MySQL statement</returns>
        public int Execute(string commandText, Dictionary<string, object> parameters)
        {
            _repository.ExecuteCommandWithNoResults(commandText, parameters, true);
            return 1;
        }

        /// <summary>
        /// Executes a non-query MySQL statement
        /// </summary>
        /// <param name="commandText">The MySQL query to execute</param>
        /// <param name="parameters">Optional parameters to pass to the query</param>
        /// <returns>The ID of the last inserted row</returns>
        public long ExecuteGettingLastInsertId(string commandText, Dictionary<string, object> parameters)
        {
            return _repository.ExecuteCommandWithNoResults(commandText, parameters, true);
        }

        /// <summary>
        /// Executes a MySQL query that returns a single scalar value as the result.
        /// </summary>
        /// <param name="commandText">The MySQL query to execute</param>
        /// <param name="parameters">Optional parameters to pass to the query</param>
        /// <returns></returns>
        public T QueryValue<T>(string commandText, Dictionary<string, object> parameters)
        {
            return _repository.ExecuteQueryForValue<T>(commandText, parameters, true);
        }

        /// <summary>
        /// Executes a SQL query that returns a list of rows as the result.
        /// </summary>
        /// <param name="commandText">The MySQL query to execute</param>
        /// <param name="parameters">Parameters to pass to the MySQL query</param>
        /// <returns>A list of a Dictionary of Key, values pairs representing the 
        /// ColumnName and corresponding value</returns>
        public List<Dictionary<string, object>> Query(string commandText, Dictionary<string, object> parameters)
        {
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            var dataTable = _repository.ExecuteQuery(commandText, parameters, true);

            foreach (DataRow row in dataTable.Rows)
            {
                var rowData = new Dictionary<string, object>();
                for(var i = 0; i < dataTable.Columns.Count; i++)
                {
                    var columnName = dataTable.Columns[i].ColumnName;
                    var columnValue = row[i] == DBNull.Value ? null : row[i];
                    rowData.Add(columnName, columnValue);
                }
                rows.Add(rowData);
            }

            return rows;
        }

        /// <summary>
        /// Helper method to return query a string value 
        /// </summary>
        /// <param name="commandText">The MySQL query to execute</param>
        /// <param name="parameters">Parameters to pass to the MySQL query</param>
        /// <returns>The string value resulting from the query</returns>
        public string GetStrValue(string commandText, Dictionary<string, object> parameters)
        {
            var value = QueryValue<string>(commandText, parameters);
            return value;
        }
        
        /// <summary>
        /// Helper method to return query a string value 
        /// </summary>
        /// <param name="commandText">The MySQL query to execute</param>
        /// <param name="parameters">Parameters to pass to the MySQL query</param>
        /// <returns>The string value resulting from the query</returns>
        public byte[] GetBlobValue(string commandText, Dictionary<string, object> parameters)
        {
            var value = QueryValue<byte[]>(commandText, parameters) as byte[];
            return value;
        }


        public void Dispose()
        {
            if (_repository != null)
            {
                _repository.Dispose();
                _repository = null;
            }
        }
    }
}
