﻿using Promis.NET.DataAccess.DbContexts;
using Promis.NET.DataAccess.Repositories;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Utilities.Extensions;
using Promis.NET.Utilities.Roles;
using Promis.Web.DataAccess.Entities.PromisWeb;
using System.Collections.Generic;
using System.Linq;

namespace Promis.NET.DataAccess.Identity.MySql
{
    public class RolesProvider: IRolesProvider
    {
        private IProjectRepository _projectRepository;

        public RolesProvider(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public RolesProvider() : this(new ProjectRepository(new PromisWebDbContext(ApplicationDbContext.useLocal))) { }

        public IList<RoleEntity> ReadModulesRoles()
        {
            var modules = _projectRepository.GetAllModules();
            var output = new List<RoleEntity>();
            foreach (var module in modules)
            {
                output.Add(new RoleEntity
                {
                    Name = string.Format("M_{0}_{1}", module.code.ToUpper(), "R"),
                    Value = "Module Read: " + module.description
                });
                output.Add(new RoleEntity
                {
                    Name = string.Format("M_{0}_{1}", module.code.ToUpper(), "W"),
                    Value = "Module Write: " + module.description
                });
            }

            return output;
        }

        public IList<RoleEntity> ReadResponsabilitiesRoles()
        {
            var output = new List<RoleEntity>
            {
                new RoleEntity()
                {
                    Name = "R_A",
                    Value = "Responsability: Admin"
                },
                new RoleEntity()
                {
                    Name ="R_C",
                    Value =  "Responsability: Contractor"
                },
                new RoleEntity()
                {
                    Name ="R_O",
                    Value =  "Responsability: Client"
                }
            };

            var allActiveProjects = _projectRepository.GetAll<project>().ToList();
            var usedSubcontractors = new List<string>();
            foreach (var project in allActiveProjects)
            {
                _projectRepository.ChangeDbContext(new ProjectDbContext("pw_" + project.proj, _projectRepository.DbContext.IsLocalDb));
                var allSubcontractors = _projectRepository.GetAllSubcontractors();
                foreach (var subc in allSubcontractors)
                {
                    if (usedSubcontractors.Contains(subc.code.ToCamelCase()))
                        continue;

                    usedSubcontractors.Add(subc.code.ToCamelCase());
                    output.Add(new RoleEntity()
                    {
                        Name = string.Format("R_S_{0}", subc.code.ToUpper()),
                        Value = "Responsability: Subcontractor - " + subc.subcontractorCol
                    });
                }
            }
            _projectRepository.ChangeDbContext(new PromisWebDbContext(_projectRepository.DbContext.IsLocalDb));

            return output;
        }

        public IList<RoleEntity> ReadActivitiesRoles()
        {
            var activities = _projectRepository.GetMainActivities();
            var output = new List<RoleEntity>();
            foreach (var activity in activities)
            {
                output.Add(new RoleEntity
                {
                    Name = string.Format("A_{0}", activity.Key.ToUpper()),
                    Value = "Activity Access: " + activity.Value
                });
            }

            return output;
        }
    }
}
