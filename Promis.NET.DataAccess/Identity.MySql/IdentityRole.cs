﻿using Microsoft.AspNet.Identity;
using System;

namespace Promis.NET.DataAccess.Identity.MySql
{
    /// <summary>
    /// Class that implements the ASP.NET Identity
    /// IRole interface 
    /// </summary>
    public class IdentityRole : IRole
    {
        /// <summary>
        /// Default constructor for Role 
        /// </summary>
        public IdentityRole()
        {
            Id = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Constructor that takes names as argument 
        /// </summary>
        /// <param name="name"></param>
        public IdentityRole(string name) : this()
        {
            Name = name;
        }

        public IdentityRole(string name, string id)
        {
            Name = name;
            Id = id;
        }

        private void SetDataFromName()
        {
            //default -> Responsability
            if (!(Name.StartsWith("R_") || Name.StartsWith("A_") || Name.StartsWith("M_")))
                Name = "R_" + Name;

            var splitted = Name.Split('_');
            Type = splitted[0];
            LocalName = splitted[1];
        }

        /// <summary>
        /// Role ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Role name (full, including meta-type. Ex: R_A = Responsability Admin)
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Role name (short, local; knowing and storing the type separated; ex: A, with type = R)
        /// </summary>
        public string LocalName { get; set; }

        /// <summary>
        /// Role Type. M (Module) / A (Activity) / R (Responsability)
        /// </summary>
        public string Type { get; set; }
    }
}
