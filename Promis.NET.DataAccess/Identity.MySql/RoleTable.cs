﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Promis.NET.DataAccess.Identity.MySql
{
    /// <summary>
    /// Class that represents the Role table in the MySQL Database. For simplicity, Role Id will be the sane with Role name (code)
    /// </summary>
    public class RoleTable
    {
        private MySQLDatabase _database;

        /// <summary>
        /// Constructor that takes a MySQLDatabase instance 
        /// </summary>
        /// <param name="database"></param>
        public RoleTable(MySQLDatabase database)
        {
            _database = database;
        }

        /// <summary>
        /// Returns a role name given the roleId
        /// </summary>
        /// <param name="roleId">The role Id (starting with prefix Type)</param>
        /// <returns>Role name</returns>
        public string GetRoleName(string roleId)
        {
            return roleId;
        }

        /// <summary>
        /// Returns the role Id given a role name
        /// </summary>
        /// <param name="roleName">Role's name</param>
        /// <returns>Role's Id</returns>
        public string GetRoleId(string roleName)
        {
            return roleName;
        }

        /// <summary>
        /// Gets the IdentityRole given the role Id
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public IdentityRole GetRoleById(string roleId)
        {
            var roleName = GetRoleName(roleId);
            IdentityRole role = null;

            if (roleName != null)
            {
                role = new IdentityRole(roleName, roleId);
            }

            return role;

        }

        /// <summary>
        /// Gets the IdentityRole given the role name
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public IdentityRole GetRoleByName(string roleName)
        {
            var roleId = GetRoleId(roleName);
            IdentityRole role = null;

            if (roleId != null)
            {
                role = new IdentityRole(roleName, roleId);
            }

            return role;
        }

        #region Unused 
        /// <summary>
        /// Deltes a role from the Roles table
        /// </summary>
        /// <param name="roleId">The role Id</param>
        /// <returns></returns>
        public int Delete(string roleId)
        {
            //string commandText = "Delete from Roles where Id = @id";
            //Dictionary<string, object> parameters = new Dictionary<string, object>();
            //parameters.Add("@id", roleId);

            //return _database.Execute(commandText, parameters);
            return 1;
        }

        /// <summary>
        /// Inserts a new Role in the Roles table
        /// </summary>
        /// <param name="roleName">The role's name</param>
        /// <returns></returns>
        public int Insert(IdentityRole role)
        {
            //string commandText = "Insert into Roles (Id, Name) values (@id, @name)";
            //Dictionary<string, object> parameters = new Dictionary<string, object>();
            //parameters.Add("@name", role.Name);
            //parameters.Add("@id", role.Id);

            //return _database.Execute(commandText, parameters);
            return 1;
        }

        public int Update(IdentityRole role)
        {
            //string commandText = "Update Roles set Name = @name where Id = @id";
            //Dictionary<string, object> parameters = new Dictionary<string, object>();
            //parameters.Add("@id", role.Id);

            //return _database.Execute(commandText, parameters);
            return 1;
        }
        #endregion
    }
}
