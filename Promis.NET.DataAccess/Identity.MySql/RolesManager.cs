﻿using Promis.NET.Utilities.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Promis.NET.DataAccess.Identity.MySql
{
    public class RolesManager: IDisposable
    {
        /// <summary>
        /// Holds the roles for the MODULES, separated by Global.RolesSeparator ($). Ex: SS_R$SS_W
        /// </summary>
        public string RolesModulesString { get; set; } = String.Empty;
        
        /// <summary>
        /// Holds the roles for the ACTIVITIES, separated by Global.RolesSeparator ($). Ex: SS_R$SS_W
        /// </summary>
        public string RolesActivitiesString { get; set; } = String.Empty;

        /// <summary>
        /// Holds the roles for the RESPONSABILITIES, separated by Global.RolesSeparator ($). Ex: SS_R$SS_W
        /// </summary>
        public string RolesResponsabilitiesString { get; set; } = String.Empty;

        /// <summary>
        /// Holds the list of roles for MODULES, using the global naming format (SS_R$SS_W => [M_SS_R, M_SS_W])
        /// </summary>
        public List<string> RolesModules { get; set; } = new List<string>();
        /// <summary>
        /// Holds the list of roles for MODULES (SS_R$SS_W => [SS_R, SS_W])
        /// </summary>
        public List<string> RolesModulesLocal { get; set; } = new List<string>();


        /// <summary>
        /// Holds the list of roles for ACTIVITIES, using the global naming format (SS_R$SS_W => [A_SS_R, A_SS_W])
        /// </summary>
        public List<string> RolesActivities { get; set; } = new List<string>();
        /// <summary>
        /// Holds the list of roles for ACTIVITIES (SS_R$SS_W => [SS_R, SS_W])
        /// </summary>
        public List<string> RolesActivitiesLocal { get; set; } = new List<string>();


        /// <summary>
        /// Holds the list of roles for RESPONSABILITIES, using the global naming format (SS_R$SS_W => [R_SS_R, R_SS_W])
        /// </summary>
        public List<string> RolesResponsabilities { get; set; } = new List<string>();
        /// <summary>
        /// Holds the list of roles for ACTIVITIES (SS_R$SS_W => [SS_R, SS_W])
        /// </summary>
        public List<string> RolesResponsabilitiesLocal { get; set; } = new List<string>();

        public RolesManager() { }

        public RolesManager(string roleModulesSring, string roleActivitiesString, string rolesRespString)
        {
            SetStrings(roleModulesSring, roleActivitiesString, rolesRespString);
        }

        /// <summary>
        /// Given the strings containing the roles, separated bu Global.RoleSeparator, creates the lists holding the roles (including the global list using the global naming - preceeding each role type with M, A or R for Module, Activity and Responsability)
        /// NOTE: Updates only the fields provided, the rest remain intact.
        /// </summary>
        /// <param name="rolesModulesString"></param>
        /// <param name="rolesActivitiesString"></param>
        /// <param name="rolesResponsabilitiesString"></param>
        public void SetStrings(string rolesModulesString = null, string rolesActivitiesString = null, string rolesResponsabilitiesString = null)
        {
            if (!string.IsNullOrWhiteSpace(rolesModulesString))
            {
                RolesModulesString = rolesModulesString;
                RolesModules = rolesModulesString.Split(new[] { Global.RolesSeparator }, StringSplitOptions.RemoveEmptyEntries).Select(rm => rm.StartsWith("M_") ? rm : "M_" + rm).ToList();
                RolesModulesLocal = rolesModulesString.Split(new[] { Global.RolesSeparator }, StringSplitOptions.RemoveEmptyEntries).Select(rm => rm.StartsWith("M_") ? rm.Substring(2) : rm).ToList();
            }

            if (!string.IsNullOrWhiteSpace(rolesActivitiesString))
            {
                RolesActivitiesString = rolesActivitiesString;
                RolesActivities = rolesActivitiesString.Split(new[] { Global.RolesSeparator }, StringSplitOptions.RemoveEmptyEntries).Select(ra => ra.StartsWith("A_") ? ra : "A_" + ra).ToList();
                RolesActivitiesLocal = rolesActivitiesString.Split(new[] { Global.RolesSeparator }, StringSplitOptions.RemoveEmptyEntries).Select(ra => ra.StartsWith("A_") ? ra.Substring(2) : ra).ToList();
            }

            if (!string.IsNullOrWhiteSpace(rolesResponsabilitiesString))
            {
                RolesResponsabilitiesString = rolesResponsabilitiesString;
                RolesResponsabilities = rolesResponsabilitiesString.Split(new[] { Global.RolesSeparator }, StringSplitOptions.RemoveEmptyEntries).Select(rr => rr.StartsWith("R_") ? rr : "R_" + rr).ToList();
                RolesResponsabilitiesLocal = rolesResponsabilitiesString.Split(new[] { Global.RolesSeparator }, StringSplitOptions.RemoveEmptyEntries).Select(rr => rr.StartsWith("R_") ? rr.Substring(2) : rr).ToList();
            }
        }

        /// <summary>
        /// Reverse creation of the roles strings. Given the lists with roles for each category, create the strings using the separator (updates the local lists too, all is synced)
        /// NOTE: Updates only the fields provided, the rest remain intact.
        /// </summary>
        /// <param name="rolesModules"></param>
        /// <param name="rolesActivities"></param>
        /// <param name="rolesResponsabilities"></param>
        public void SetLists(List<string> rolesModules = null, List<string> rolesActivities = null, List<string> rolesResponsabilities = null)
        {
            if (rolesModules != null)
            {
                var rolesModulesLocal = rolesModules.Select(rm => rm.Remove(0, rm.IndexOf('_') + 1));
                RolesModulesString = string.Join(Global.RolesSeparator, rolesModulesLocal);
                SetStrings(rolesModulesString: RolesModulesString);
            }

            if (rolesActivities != null)
            {
                var rolesActivitiesLocal = rolesActivities.Select(rm => rm.Remove(0, rm.IndexOf('_') + 1));
                RolesActivitiesString = string.Join(Global.RolesSeparator, rolesActivitiesLocal);
                SetStrings(rolesActivitiesString: RolesActivitiesString);
            }

            if (rolesResponsabilities != null)
            {
                var rolesRespLocal = rolesResponsabilities.Select(rm => rm.Remove(0, rm.IndexOf('_') + 1));
                RolesResponsabilitiesString = string.Join(Global.RolesSeparator, rolesRespLocal);
                SetStrings(rolesResponsabilitiesString: RolesResponsabilitiesString);
            }
        }

        /// <summary>
        /// Providing the fact that the string containing the roles have been externally modified, forces the update of the lists of strings 
        /// </summary>
        public void UpdateListsFromStrings()
        {
            SetStrings(RolesModulesString, RolesActivitiesString, RolesResponsabilitiesString);
        }
        
        /// <summary>
        /// Contains the list with all the roles contained, in global naming format. Ex. [M_SS, M_SP, A_P, R_SA]
        /// </summary>
        public List<string> RolesAll
        {
            get
            {
                var allRoles = new List<string>(RolesModules);
                allRoles.AddRange(RolesActivities);
                allRoles.AddRange(RolesResponsabilities);
                return allRoles.ToList();
            }
        }

        public void AddRole(string role)
        {
            if (!(role.StartsWith("R_") || role.StartsWith("A_") || role.StartsWith("M_")))
                role = "R_" + role;//default

            var splitted = role.Split('_');
            var type = splitted[0];
            var value = splitted[1];

            switch (type)
            {
                case "M":
                    if (!RolesModules.Contains(role))
                    {
                        RolesModules.Add(role);
                        SetLists(rolesModules: RolesModules);
                    }
                    break;
                case "A":
                    if (!RolesActivities.Contains(role))
                    {
                        RolesActivities.Add(role);
                        SetLists(rolesActivities: RolesActivities);
                    }
                    break;
                case "R":
                    if (!RolesResponsabilities.Contains(role))
                    {
                        RolesResponsabilities.Add(role);
                        SetLists(rolesResponsabilities: RolesResponsabilities);
                    }
                    break;
            }
        }

        public void Dispose()
        {
            RolesActivities = null;
            RolesActivitiesLocal = null;
            RolesActivitiesString = null;
            RolesModules = null;
            RolesModulesLocal = null;
            RolesModulesString = null;
            RolesResponsabilities = null;
            RolesResponsabilitiesLocal = null;
            RolesResponsabilitiesString = null;
        }
    }
}
