﻿using Promis.NET.DataAccess.Identity.MySql;

namespace Promis.NET.DataAccess.DbContexts
{
    public class ApplicationDbContext : MySQLDatabase
    {
        public ApplicationDbContext()
            : base()
        {
        }

        public static bool useLocal = false;


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public string DatabaseName { get => "promisweb"; set { return; } }
    }
}
