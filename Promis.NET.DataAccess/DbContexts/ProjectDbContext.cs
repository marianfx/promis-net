﻿using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using System.Data.Entity;

namespace Promis.NET.DataAccess.DbContexts
{
    public class ProjectDbContext: DbContext, IDbContext
    {
        public string DatabaseName { get; set; }
        public bool IsLocalDb { get; set; }

        public ProjectDbContext(string databaseName, bool useLocal = false): base("name=ApplicationDbContext" + (useLocal ? "Local" : ""))
        {
            DatabaseName = databaseName;
            IsLocalDb = useLocal;
            Database.Connection.ConnectionString = Database.Connection.ConnectionString.Replace("xxxxdb", databaseName).Replace("xxxx", Global.DatabaseUserPassword);
        }

        public virtual DbSet<act_form> act_form { get; set; }
        public virtual DbSet<act_plan> act_plan { get; set; }
        public virtual DbSet<act_actions> act_actions { get; set; }
        public virtual DbSet<ag_components> ag_components { get; set; }
        public virtual DbSet<ag_type> ag_type { get; set; }
        public virtual DbSet<area> areas { get; set; }
        public virtual DbSet<pw_budget> pw_budget { get; set; }
        public virtual DbSet<budget_subact> budget_subact { get; set; }
        public virtual DbSet<building> buildings { get; set; }
        public virtual DbSet<buildings_type> buildings_type { get; set; }
        public virtual DbSet<completion> completions { get; set; }
        public virtual DbSet<concrete> concretes { get; set; }
        public virtual DbSet<concrete_type> concrete_type { get; set; }
        public virtual DbSet<cut_offf> cut_off { get; set; }
        public virtual DbSet<dwg> dwgs { get; set; }
        public virtual DbSet<el_cable> el_cable { get; set; }
        public virtual DbSet<electrical> electricals { get; set; }
        public virtual DbSet<electrical_type> electrical_type { get; set; }
        public virtual DbSet<equipment> equipments { get; set; }
        public virtual DbSet<equipment_type> equipment_type { get; set; }
        public virtual DbSet<ff_hydrants> ff_hydrants { get; set; }
        public virtual DbSet<ff_piping> ff_piping { get; set; }
        public virtual DbSet<ff_structure> ff_structure { get; set; }
        public virtual DbSet<fitting> fittings { get; set; }
        public virtual DbSet<ftc_container> ftcs { get; set; }
        public virtual DbSet<grouting> groutings { get; set; }
        public virtual DbSet<in_cable> in_cable { get; set; }
        public virtual DbSet<in_components> in_components { get; set; }
        public virtual DbSet<in_loops> in_loops { get; set; }
        public virtual DbSet<instrumentation_type> instrumentation_type { get; set; }
        public virtual DbSet<instrument> instruments { get; set; }
        public virtual DbSet<is_equipment> is_equipment { get; set; }
        public virtual DbSet<is_piping> is_piping { get; set; }
        public virtual DbSet<jobstatu> jobstatus { get; set; }
        public virtual DbSet<linelist> linelists { get; set; }
        public virtual DbSet<pa_equipment> pa_equipment { get; set; }
        public virtual DbSet<pa_piping> pa_piping { get; set; }
        public virtual DbSet<pa_structure> pa_structure { get; set; }
        public virtual DbSet<pid> pids { get; set; }
        public virtual DbSet<piles_type> piles_type { get; set; }
        public virtual DbSet<pipeline> pipelines { get; set; }
        public virtual DbSet<pl_works> pl_works { get; set; }
        public virtual DbSet<planning> plannings { get; set; }
        public virtual DbSet<progress> progresses { get; set; }
        public virtual DbSet<punchlist> punchlists { get; set; }
        public virtual DbSet<scaffolding> scaffoldings { get; set; }
        public virtual DbSet<sketch> sketches { get; set; }
        public virtual DbSet<soil> soils { get; set; }
        public virtual DbSet<spool> spools { get; set; }
        public virtual DbSet<structure_type> structure_type { get; set; }
        public virtual DbSet<structure> structures { get; set; }
        public virtual DbSet<structures_type> structures_type { get; set; }
        public virtual DbSet<subc_sow> subc_sow { get; set; }
        public virtual DbSet<subcontractor> subcontractors { get; set; }
        public virtual DbSet<sup> sups { get; set; }
        public virtual DbSet<system_s> systems { get; set; }
        public virtual DbSet<tank> tanks { get; set; }
        public virtual DbSet<tcf> tcfs { get; set; }
        public virtual DbSet<testpack> testpacks { get; set; }
        public virtual DbSet<tmp_> tmp_ { get; set; }
        public virtual DbSet<tmp_guest> tmp_guest { get; set; }
        public virtual DbSet<tracing> tracings { get; set; }
        public virtual DbSet<ug_electrical> ug_electrical { get; set; }
        public virtual DbSet<ug_instrument> ug_instrument { get; set; }
        public virtual DbSet<ug_piles> ug_piles { get; set; }
        public virtual DbSet<ug_piping> ug_piping { get; set; }
        public virtual DbSet<underground> undergrounds { get; set; }
        public virtual DbSet<underground_type> underground_type { get; set; }
        public virtual DbSet<unit> units { get; set; }
        public virtual DbSet<work_def> work_def { get; set; }
        public virtual DbSet<work_disc> work_disc { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<act_form>()
                .Property(e => e.CODE)
                .IsUnicode(false);

            modelBuilder.Entity<act_form>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<act_plan>()
                .Property(e => e.CODE)
                .IsUnicode(false);

            modelBuilder.Entity<act_plan>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<ag_components>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<ag_components>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<ag_components>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<ag_components>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<ag_components>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<ag_components>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<ag_components>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<ag_components>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<ag_components>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ag_components>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ag_components>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<ag_type>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<ag_type>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<area>()
                .Property(e => e.CODE)
                .IsUnicode(false);

            modelBuilder.Entity<area>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<budget_subact>()
                .Property(e => e.sub_act)
                .IsUnicode(false);

            modelBuilder.Entity<budget_subact>()
                .Property(e => e.um)
                .IsUnicode(false);

            modelBuilder.Entity<budget_subact>()
                .Property(e => e.prod)
                .IsUnicode(false);

            modelBuilder.Entity<budget_subact>()
                .Property(e => e.mhrs)
                .IsUnicode(false);

            modelBuilder.Entity<building>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<building>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<building>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<building>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<building>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<building>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<building>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<building>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<building>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<building>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<building>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<buildings_type>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<buildings_type>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<concrete>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<concrete>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<concrete>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<concrete>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<concrete>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<concrete>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<concrete>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<concrete>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<concrete>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<concrete>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<concrete>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<concrete_type>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<concrete_type>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<cut_offf>()
                .Property(e => e.w)
                .IsUnicode(false);

            modelBuilder.Entity<cut_offf>()
                .Property(e => e.m)
                .IsUnicode(false);

            modelBuilder.Entity<cut_offf>()
                .Property(e => e.cut_off)
                .IsUnicode(false);

            modelBuilder.Entity<dwg>()
                .Property(e => e.CODE)
                .IsUnicode(false);

            modelBuilder.Entity<dwg>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<el_cable>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<el_cable>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<el_cable>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<el_cable>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<el_cable>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<el_cable>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<el_cable>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<el_cable>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<el_cable>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<el_cable>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<el_cable>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<el_cable>()
                .Property(e => e.FROM_LOCATION)
                .IsUnicode(false);

            modelBuilder.Entity<el_cable>()
                .Property(e => e.TO_LOCATION)
                .IsUnicode(false);

            modelBuilder.Entity<electrical>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<electrical>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<electrical>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<electrical>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<electrical>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<electrical>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<electrical>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<electrical>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<electrical>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<electrical>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<electrical>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<electrical_type>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<electrical_type>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<equipment>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<equipment_type>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<equipment_type>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<ff_hydrants>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<ff_hydrants>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<ff_hydrants>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<ff_hydrants>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<ff_hydrants>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<ff_hydrants>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<ff_hydrants>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<ff_hydrants>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<ff_hydrants>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ff_hydrants>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ff_hydrants>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<ff_piping>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<ff_piping>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<ff_piping>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<ff_piping>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<ff_piping>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<ff_piping>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<ff_piping>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<ff_piping>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<ff_piping>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ff_piping>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ff_piping>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<ff_structure>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<ff_structure>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<ff_structure>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<ff_structure>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<ff_structure>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<ff_structure>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<ff_structure>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<ff_structure>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<ff_structure>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ff_structure>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ff_structure>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<fitting>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<fitting>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<fitting>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<fitting>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<fitting>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<fitting>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<fitting>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<fitting>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<fitting>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<fitting>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<fitting>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<grouting>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<grouting>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<grouting>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<grouting>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<grouting>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<grouting>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<grouting>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<grouting>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<grouting>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<grouting>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<grouting>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<in_cable>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<in_cable>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<in_cable>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<in_cable>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<in_cable>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<in_cable>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<in_cable>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<in_cable>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<in_cable>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<in_cable>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<in_cable>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<in_cable>()
                .Property(e => e.FROM_LOCATION)
                .IsUnicode(false);

            modelBuilder.Entity<in_cable>()
                .Property(e => e.TO_LOCATION)
                .IsUnicode(false);

            modelBuilder.Entity<in_components>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<in_components>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<in_components>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<in_components>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<in_components>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<in_components>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<in_components>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<in_components>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<in_components>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<in_components>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<in_components>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<in_loops>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<in_loops>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<in_loops>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<in_loops>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<in_loops>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<in_loops>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<in_loops>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<in_loops>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<in_loops>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<in_loops>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<in_loops>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<instrumentation_type>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<instrumentation_type>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.LINE)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.LOOP_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.EQUIPMENT)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.JUNCTION_BOXES)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.INTERLOCK)
                .IsUnicode(false);

            modelBuilder.Entity<instrument>()
                .Property(e => e.IOTYPE)
                .IsUnicode(false);

            modelBuilder.Entity<is_equipment>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<is_equipment>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<is_equipment>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<is_equipment>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<is_equipment>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<is_equipment>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<is_equipment>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<is_equipment>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<is_equipment>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<is_equipment>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<is_equipment>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<is_equipment>()
                .Property(e => e.FINISH_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<is_piping>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<is_piping>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<is_piping>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<is_piping>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<is_piping>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<is_piping>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<is_piping>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<is_piping>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<is_piping>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<is_piping>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<is_piping>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<is_piping>()
                .Property(e => e.FINISH_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<jobstatu>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<jobstatu>()
                .Property(e => e.flag)
                .IsUnicode(false);

            modelBuilder.Entity<jobstatu>()
                .Property(e => e.descr)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.LINE)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.SIZE)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.FLUID_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.FLUID_DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.FROM_LOCATION)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.TO_LOCATION)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.line_desp)
                .IsUnicode(false);

            modelBuilder.Entity<linelist>()
                .Property(e => e.piping_class)
                .IsUnicode(false);

            modelBuilder.Entity<pa_equipment>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<pa_equipment>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<pa_equipment>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<pa_equipment>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<pa_equipment>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<pa_equipment>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<pa_equipment>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<pa_equipment>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<pa_equipment>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<pa_equipment>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<pa_equipment>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<pa_piping>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<pa_piping>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<pa_piping>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<pa_piping>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<pa_piping>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<pa_piping>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<pa_piping>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<pa_piping>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<pa_piping>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<pa_piping>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<pa_piping>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<pa_structure>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<pa_structure>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<pa_structure>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<pa_structure>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<pa_structure>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<pa_structure>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<pa_structure>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<pa_structure>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<pa_structure>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<pa_structure>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<pa_structure>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.CODE)
                .IsUnicode(false);

            modelBuilder.Entity<pid>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<piles_type>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<piles_type>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<pipeline>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<pipeline>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<pipeline>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<pipeline>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<pipeline>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<pipeline>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<pipeline>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<pipeline>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<pipeline>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<pipeline>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<pipeline>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<pl_works>()
                .Property(e => e.type)
                .IsUnicode(false);

            modelBuilder.Entity<pl_works>()
                .Property(e => e.descr)
                .IsUnicode(false);

            modelBuilder.Entity<pl_works>()
                .Property(e => e.phase)
                .IsUnicode(false);

            modelBuilder.Entity<planning>()
                .Property(e => e.act)
                .IsUnicode(false);

            modelBuilder.Entity<planning>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<planning>()
                .Property(e => e.mhrs)
                .IsUnicode(false);

            modelBuilder.Entity<planning>()
                .Property(e => e.type)
                .IsUnicode(false);

            modelBuilder.Entity<planning>()
                .Property(e => e.start_date)
                .IsUnicode(false);

            modelBuilder.Entity<planning>()
                .Property(e => e.end_date)
                .IsUnicode(false);

            modelBuilder.Entity<progress>()
                .Property(e => e.m)
                .IsUnicode(false);

            modelBuilder.Entity<progress>()
                .Property(e => e.w)
                .IsUnicode(false);

            modelBuilder.Entity<progress>()
                .Property(e => e.act)
                .IsUnicode(false);

            modelBuilder.Entity<progress>()
                .Property(e => e.earned)
                .IsUnicode(false);

            modelBuilder.Entity<progress>()
                .Property(e => e.mhrs)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.system)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.pl_disci)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.item)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.wd)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.orig)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.js)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.flag)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.actionby)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.target_date)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.found_date)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.reference)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.closure)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.closure_date)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.subsystem)
                .IsUnicode(false);

            modelBuilder.Entity<punchlist>()
                .Property(e => e.closer)
                .IsUnicode(false);

            modelBuilder.Entity<scaffolding>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<scaffolding>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<scaffolding>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<scaffolding>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<scaffolding>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<scaffolding>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<scaffolding>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<scaffolding>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<scaffolding>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<scaffolding>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<scaffolding>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.PDS)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.LINE)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.SHEET)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.PIPING_CLASS)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.MATERIAL_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.MATERIAL)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.SIZE)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.DESTINATION)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.PIPE_LENGTH)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.INSULATION_MATERIAL)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.INSULATION_SURFACE)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.PAINT_SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.PAINT_SURFACE)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.FINISH_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.STATUS)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.REVISION)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.FLUID_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.FLUID_DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.FROM_LOCATION)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.TO_LOCATION)
                .IsUnicode(false);

            modelBuilder.Entity<sketch>()
                .Property(e => e.LINE_NUM)
                .IsUnicode(false);

            modelBuilder.Entity<soil>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<soil>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<soil>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<soil>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<soil>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<soil>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<soil>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<soil>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<soil>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<soil>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<soil>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<spool>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<spool>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<spool>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<spool>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<spool>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<spool>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<spool>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<spool>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<spool>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<spool>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<spool>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<structure_type>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<structure_type>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<structure>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<structure>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<structure>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<structure>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<structure>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<structure>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<structure>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<structure>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<structure>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<structure>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<structure>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<structures_type>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<structures_type>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<subc_sow>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<subc_sow>()
                .Property(e => e.sub_act)
                .IsUnicode(false);

            modelBuilder.Entity<subcontractor>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<subcontractor>()
                .Property(e => e.subcontractorCol)
                .IsUnicode(false);

            modelBuilder.Entity<sup>()
                .Property(e => e.CODE)
                .IsUnicode(false);

            modelBuilder.Entity<sup>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<system_s>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<system_s>()
                .Property(e => e.SYSTEM_DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<system_s>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<system_s>()
                .Property(e => e.SUBSYSTEM_DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<system_s>()
                .Property(e => e.SUP)
                .IsUnicode(false);

            modelBuilder.Entity<tank>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<tank>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<tank>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<tank>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<tank>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<tank>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<tank>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<tank>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<tank>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<tank>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<tank>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<tcf>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<tcf>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<tcf>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<tcf>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<tcf>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<tcf>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<tcf>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<tcf>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<tcf>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<tcf>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<tcf>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<testpack>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<testpack>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<testpack>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<testpack>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<testpack>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<testpack>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<testpack>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<testpack>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<testpack>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<testpack>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<testpack>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<tmp_>()
                .Property(e => e.tag)
                .IsUnicode(false);

            modelBuilder.Entity<tmp_>()
                .Property(e => e.descr)
                .IsUnicode(false);

            modelBuilder.Entity<tmp_>()
                .Property(e => e.parent)
                .IsUnicode(false);

            modelBuilder.Entity<tmp_>()
                .Property(e => e.qty)
                .IsUnicode(false);

            modelBuilder.Entity<tmp_guest>()
                .Property(e => e.tag)
                .IsUnicode(false);

            modelBuilder.Entity<tmp_guest>()
                .Property(e => e.descr)
                .IsUnicode(false);

            modelBuilder.Entity<tmp_guest>()
                .Property(e => e.parent)
                .IsUnicode(false);

            modelBuilder.Entity<tmp_guest>()
                .Property(e => e.qty)
                .IsUnicode(false);

            modelBuilder.Entity<tracing>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<tracing>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<tracing>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<tracing>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<tracing>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<tracing>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<tracing>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<tracing>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<tracing>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<tracing>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<tracing>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<ug_electrical>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<ug_electrical>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<ug_electrical>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<ug_electrical>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<ug_electrical>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<ug_electrical>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<ug_electrical>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<ug_electrical>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<ug_electrical>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ug_electrical>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ug_electrical>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<ug_instrument>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<ug_instrument>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<ug_instrument>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<ug_instrument>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<ug_instrument>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<ug_instrument>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<ug_instrument>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<ug_instrument>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<ug_instrument>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ug_instrument>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ug_instrument>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piles>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piles>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piles>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piles>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piles>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piles>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piles>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piles>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piles>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piles>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piles>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piping>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piping>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piping>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piping>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piping>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piping>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piping>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piping>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piping>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piping>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piping>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piping>()
                .Property(e => e.FROM_LOCATION)
                .IsUnicode(false);

            modelBuilder.Entity<ug_piping>()
                .Property(e => e.TO_LOCATION)
                .IsUnicode(false);

            modelBuilder.Entity<underground>()
                .Property(e => e.TAG)
                .IsUnicode(false);

            modelBuilder.Entity<underground>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<underground>()
                .Property(e => e.WBS)
                .IsUnicode(false);

            modelBuilder.Entity<underground>()
                .Property(e => e.TYPE)
                .IsUnicode(false);

            modelBuilder.Entity<underground>()
                .Property(e => e.DWG)
                .IsUnicode(false);

            modelBuilder.Entity<underground>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<underground>()
                .Property(e => e.QTY)
                .IsUnicode(false);

            modelBuilder.Entity<underground>()
                .Property(e => e.MAINTAG)
                .IsUnicode(false);

            modelBuilder.Entity<underground>()
                .Property(e => e.SYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<underground>()
                .Property(e => e.SUBSYSTEM)
                .IsUnicode(false);

            modelBuilder.Entity<underground>()
                .Property(e => e.ACTIVITY)
                .IsUnicode(false);

            modelBuilder.Entity<underground_type>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<underground_type>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<unit>()
                .Property(e => e.CODE)
                .IsUnicode(false);

            modelBuilder.Entity<unit>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.ACT)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.ACT_D)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.SUB_ACT)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.SUB_ACT_D)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.UM)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.STEP)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.INSPECTION)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.FORM)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.S)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.C)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.O)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.T)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.POS)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.PROGR)
                .IsUnicode(false);

            modelBuilder.Entity<work_def>()
                .Property(e => e.LP)
                .IsUnicode(false);

            modelBuilder.Entity<work_disc>()
                .Property(e => e.cod)
                .IsUnicode(false);

            modelBuilder.Entity<work_disc>()
                .Property(e => e.descr)
                .IsUnicode(false);

            modelBuilder.Entity<work_disc>()
                .Property(e => e.flag)
                .IsUnicode(false);
        }
    }
}
