using System.Data.Entity;
using Promis.Web.DataAccess.Entities.PromisWeb;
using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.Utilities.Models;

namespace Promis.NET.DataAccess.DbContexts
{
    public class PromisWebDbContext : DbContext, IDbContext
    {
        public string DatabaseName { get; set; }
        public bool IsLocalDb { get; set; }

        public PromisWebDbContext(bool useLocal = false)
            : base("name=ApplicationDbContext" + (useLocal ? "Local" : ""))
        {
            DatabaseName = "promisweb";
            IsLocalDb = useLocal;
            Database.Connection.ConnectionString = Database.Connection.ConnectionString.Replace("xxxxdb", Global.MainDatabaseName).Replace("xxxx", Global.DatabaseUserPassword);
        }

        public virtual DbSet<cw_cat> cw_cat { get; set; }
        public virtual DbSet<cw_form> cw_form { get; set; }
        public virtual DbSet<jobstatu> jobstatus { get; set; }
        public virtual DbSet<menu> menus { get; set; }
        public virtual DbSet<module> modules { get; set; }
        public virtual DbSet<pl_works> pl_works { get; set; }
        public virtual DbSet<project> projects { get; set; }
        public virtual DbSet<tableproj_def> tableproj_def { get; set; }
        public virtual DbSet<tableproj_structure> tableproj_structure { get; set; }
        public virtual DbSet<tabletag_def> tabletag_def { get; set; }
        public virtual DbSet<tabletag_structure> tabletag_structure { get; set; }
        public virtual DbSet<translations> translations { get; set; }
        public virtual DbSet<userfulllist> userfulllists { get; set; }
        public virtual DbSet<video> videos { get; set; }
        public virtual DbSet<wd_codec> wd_codec { get; set; }
        public virtual DbSet<work_def> work_def { get; set; }
        public virtual DbSet<work_defold> work_defold { get; set; }
        public virtual DbSet<work_disc> work_disc { get; set; }
    }
}
