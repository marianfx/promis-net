﻿using System;
using System.Text.RegularExpressions;

namespace Promis.NET.Utilities.Extensions
{
    public static class StringExtensions
    {
        public static string EscapeSingleQuote(this string value)
        {
            return value.Replace("'", "''");
        }

        // Convert the string to camel case.
        public static string ToCamelCase(this string the_string, bool keepSpaces = false, bool removeNonLetters = true)
        {
            // If there are 0 or 1 characters, just return the string.
            if (the_string == null)
                return the_string;
            if(removeNonLetters)
                the_string = Regex.Replace(the_string, "[^a-zA-Z0-9 _]", string.Empty);
            // Split the string into words.
            string[] words = the_string.Split(
                new char[] { ' ', '_' },
                StringSplitOptions.RemoveEmptyEntries);

            // Combine the words.
            var result = "";
            for (int i = 0; i < words.Length; i++)
            {
                var startingIndex = 0;
                while (startingIndex < words[i].Length && !char.IsLetter(words[i][startingIndex]))
                    startingIndex++;

                if (startingIndex >= words[i].Length)
                    continue;

                result +=
                    words[i].Substring(startingIndex, 1).ToUpper() +
                    words[i].Substring(startingIndex + 1).ToLower();
                if (keepSpaces)
                    result += " ";
            }

            result = result.Trim();
            return result;
        }
    }
}
