﻿using Newtonsoft.Json;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Serialization;
using System.Web;
using System.Linq.Expressions;
using System;

namespace Promis.NET.Utilities.Extensions
{
    public static class ObjectExtensions
    {
        public static I GetIdProperty<I>(this object obj, string idFieldName)
        {
            var property = obj.GetType().GetProperties().FirstOrDefault(x => x.Name == idFieldName);
            if (property == null)
                throw new System.Exception("Field " + idFieldName + " does not exist for the specified object.");
            return (I)property.GetValue(obj);
        }

        /// <summary>
        /// Transforms the current Object into a HTML-not-escaped JSON string
        /// </summary>
        /// <returns></returns>
        public static IHtmlString ToHtmlJson(this object obj)
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonWriter = new JsonTextWriter(stringWriter))
                {
                    var serializer = new JsonSerializer
                    {
                        // Let's use camelCasing as is common practice in JavaScript
                        ContractResolver = new DefaultContractResolver()
                    };

                    // We don't want quotes around object names
                    jsonWriter.QuoteName = false;
                    jsonWriter.Formatting = Formatting.Indented;
                    serializer.Serialize(jsonWriter, obj);

                    return new HtmlString(stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Returns the name of an object (no need to copy-paste. expr should be "() => obj.DesiredProperty"
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="expr"></param>
        /// <returns></returns>
        public static string GetPropertyName(this object obj, Expression<Func<object>> expr)
        {
            Expression<Func<object>> expression = expr;
            MemberExpression body = (MemberExpression)expression.Body;
            string name = body.Member.Name;
            return name;
        }
    }
}
