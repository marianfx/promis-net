﻿using System;
using System.Configuration;
using System.IO;

namespace Promis.NET.Utilities.Logging
{
    public class FileLogger : ILogger
    {
        public string FilePath { get; set; }

        private string _defaultFileName => "log_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + ".txt";

        /// <summary>
        /// Initializes a new FileLogger. If not specified, the logger will store the data in the folder "Server Logs", in files named by the current day (so a log file per day).
        /// </summary>
        /// <param name="baseDirectory"></param>
        public FileLogger()
        {
            ValidateDirectoryAndFile();
        }

        private void ValidateDirectoryAndFile()
        {
            // directory creation
            var baseDir = ConfigurationManager.AppSettings["LoggerFolder"];
            if(string.IsNullOrWhiteSpace(baseDir))
            {
                Console.WriteLine("Attention: You should specify a folder in which the application will write log files.");
                baseDir = Directory.GetCurrentDirectory();
            }

            try
            {
                var logDirPath = Path.Combine(baseDir, "Server Logs");
                var di = new DirectoryInfo(logDirPath);
                if (!di.Exists)
                    di.Create();

                FilePath = Path.Combine(logDirPath, _defaultFileName);
            }
            catch (Exception)
            {
                FilePath = "";
                Console.WriteLine("Cannot access the specified folder (" + baseDir + ").");
            }
        }

        public void Log(string message, LoggingLevel level = LoggingLevel.Information)
        {
            if (string.IsNullOrWhiteSpace(FilePath))
                return;

            try
            {
                using (var writer = new StreamWriter(FilePath, true))//open as append
                {
                    writer.WriteLine(ParseMessage(message, level));
                }
            }
            catch(Exception)
            {
                //if something wrong with writing into the logging file, write a message to the console (default)
                Console.WriteLine(ParseMessage(message, level));
            }
        }

        public string ParseMessage(string message, LoggingLevel level)
        {
            var levelString = level.ToString();
            var dateNowAsString = DateTime.Now.ToString();

            return "[" + levelString + "]" + " [" + dateNowAsString + "]" + "\r\n" + message.Replace("\n", "\r\n") + "\r\n\r\n\r\n\n";
        }
    }
}
