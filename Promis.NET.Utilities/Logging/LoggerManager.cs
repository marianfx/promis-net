﻿namespace Promis.NET.Utilities.Logging
{
    public class LoggerManager
    {
        private static ILogger _currentLogger;

        public LoggerManager(ILogger logger)
        {
            _currentLogger = logger;
        }

        public static ILogger GetLogger()
        {
            return _currentLogger;
        }
    }
}
