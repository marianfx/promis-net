﻿namespace Promis.NET.Utilities.Logging
{
    public interface ILogger
    {
        /// <summary>
        /// This function will log the message, using the specific implementation way.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="level">The type of event to log. By default it is Information.</param>
        void Log(string message, LoggingLevel level = LoggingLevel.Information);

        /// <summary>
        /// Method used to parse the message before logging it. Use it to pre-format your message if you need it.
        /// For example, in a file Logger, if your message is "Exception happened." and type is "Exception" you can format the message as "[Exception][12/03/2017 - 12:00:00] Exception happened."
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="level">The type of event to log. By default it is Information.</param>
        string ParseMessage(string message, LoggingLevel level);
    }
}
