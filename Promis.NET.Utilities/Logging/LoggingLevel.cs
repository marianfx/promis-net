﻿namespace Promis.NET.Utilities.Logging
{
    public enum LoggingLevel
    {
        Information,
        Warning,
        Exception
    }
}
