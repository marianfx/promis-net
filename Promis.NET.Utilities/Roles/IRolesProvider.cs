﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promis.NET.Utilities.Roles
{
    public class RoleEntity
    {
        public string Value { get; set; } = "";
        public string Name { get; set; } = "";
    }

    public interface IRolesProvider
    {
        IList<RoleEntity> ReadModulesRoles();
        IList<RoleEntity> ReadResponsabilitiesRoles();
        IList<RoleEntity> ReadActivitiesRoles();
    }
}
