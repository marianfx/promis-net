﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Promis.NET.Utilities.Extensions;

namespace Promis.NET.Utilities.Roles
{

    public class RolesBuilder
    {
        /// <summary>
        /// Generates a class with properties for each resource key
        /// </summary>
        /// <param name="provider">Resource provider instance</param>
        /// <param name="namespaceName">Name of namespace containing the resource class</param>
        /// <param name="className">Name of the class</param>
        /// <param name="filePath">Where to generate the source file</param>
        /// <param name="summaryRoles">If not null, adds a &lt;summary&gt; tag to each property using the specified culture.</param>
        /// <returns>Generated class file full path</returns>
        public string Create(IRolesProvider provider, string namespaceName = "Promis.NET.Roles", string className = "Roles", string filePath = null, string summaryRoles = null)
        {
            var modules = provider.ReadModulesRoles();
            var responsabilities = provider.ReadResponsabilitiesRoles();
            var activities = provider.ReadActivitiesRoles();
            var resources = modules.Union(responsabilities).Union(activities).ToList();

            if (resources == null || resources.Count == 0)
                throw new Exception(string.Format("No roles were found in {0}", provider.GetType().Name));

            // Get a unique list of resource names (keys)
            var keys = resources.Select(r => r.Name).Distinct();

            #region Templates
            const string header =
@"using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Promis.NET.Utilities.Roles;
    
namespace {0} {{
    public class {1} {{

    {3}
        }}        
}}"; // {0}: namespace {1}:class name   {2}:provider class name   {3}: body  

            const string property =
@"
        {2}
        public const string {0} = ""{1}"";"; // {0}: key
            const string additionalMethods =
                @"
        public static bool IsResponsabilitySubcontractor(string role)
        {
            return role.StartsWith(""R_S"");
        }
        public static string[] GetAllModuleRoles(bool getOnlyRead = false, bool getOnlyWrite = false)
        {
            var output = new List<string>() { ResponsabilityAdmin };
            var properties = typeof(LocalRoles).GetProperties().Where(p => p.Name.StartsWith(""Module"")).ToDictionary(p => p.Name, p => p.GetConstantValue().ToString());
            foreach (var property in properties)
            {
                if (getOnlyRead && property.Key.Contains(""Read""))
                {
                    output.Add(property.Value);
                    continue;
                }

                if (getOnlyWrite && property.Key.Contains(""Write""))
                {
                    output.Add(property.Value);
                    continue;
                }

                // else add all
                output.Add(property.Value);
            }
            return output.ToArray();
        }";
            #endregion


            // store keys in a string builder
            var usedKeys = new List<string>();
            var sbKeys = new StringBuilder();

            foreach (string key in keys)
            {
                var keyName = key;
                if (usedKeys.Contains(keyName))
                    continue;
                usedKeys.Add(keyName);
                var resource = resources.Where(r => r.Name == key).FirstOrDefault();
                var displayName = resource.Value;
                var propertyName = resource.Value.ToCamelCase();
                if (resource == null)
                {
                    throw new Exception(string.Format("Could not find resource {0}", key));
                }

                sbKeys.Append(new String(' ', 12)); // indentation
                sbKeys.AppendFormat(property,
                    propertyName,
                    keyName,
                    summaryRoles == null ? string.Empty : string.Format("/// <summary>{0}</summary>", displayName));
                sbKeys.AppendLine();
            }
            sbKeys.AppendLine(additionalMethods);

            if (filePath == null)
                filePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Roles.cs");
            // write to file
            using (var writer = File.CreateText(filePath))
            {

                // write header along with keys
                writer.WriteLine(header, namespaceName, className, provider.GetType().Name, sbKeys.ToString());

                writer.Flush();
                writer.Close();
            }

            return filePath;
        }
    }
}
