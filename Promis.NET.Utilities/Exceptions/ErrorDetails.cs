﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace Promis.NET.Utilities.Exceptions
{
    public class ErrorDetails
    {
        public Guid UniqueId { get; set; }
        public List<string> Causes { get; set; }
        public string StartingMethod { get; set; }
        public string Message { get; set; }

        public ErrorDetails()
        {
            Causes = new List<string>();
            StartingMethod = string.Empty;
            Message = string.Empty;
        }

        public ErrorDetails(List<string> causes, string startingMethod, string message)
        {
            Causes = causes;
            StartingMethod = startingMethod;
            Message = message;
        }

        public override string ToString()
        {
            var startingMethodStr = !string.IsNullOrWhiteSpace(StartingMethod) ? StartingMethod : "unknown yet";
            var messageStr = !string.IsNullOrWhiteSpace(Message) ? Message : "unknown yet";
            var causesStr = Causes.Count > 0 ? string.Join(", ", Causes) : "[]";
            return "Unique ID: " + UniqueId.ToString() + ".\n" 
                + "Starting method: " + startingMethodStr + ".\n" 
                + "Message: " + messageStr + ".\n" 
                + "List of causes (exception names): " + causesStr + ".";
        }

        /// <summary>
        /// Transforms the current Object into a HTML-not-escaped JSON string
        /// </summary>
        /// <returns></returns>
        public IHtmlString ToHtmlJson()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonWriter = new JsonTextWriter(stringWriter))
                {
                    var serializer = new JsonSerializer
                    {
                        //do not alter property names
                        ContractResolver = new DefaultContractResolver()
                    };

                    // We don't want quotes around object names
                    jsonWriter.QuoteName = false;
                    jsonWriter.Formatting = Formatting.Indented;
                    serializer.Serialize(jsonWriter, this);

                    return new HtmlString(stringWriter.ToString());
                }
            }
        }
    }
}
