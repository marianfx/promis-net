﻿using Promis.NET.Utilities.Logging;
using Promis.NET.Utilities.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Promis.NET.Utilities.Exceptions
{
    public class PromisException: Exception
    {
        public ErrorDetails ErrorDetailsUserLevel { get; set; }
        public DeveloperErrorDetails ErrorDetailsDevLevel { get; set; }

        public ILogger Logger { get; set; } = LoggerManager.GetLogger();


        public PromisException() : base()
        {
            ConstructorBuilding();
        }

        public PromisException(string message) : base(message)
        {
            ConstructorBuilding();
        }

        public PromisException(Exception e): base("", e)
        {
            ConstructorBuilding();
        }

        public static PromisException GetFromExistingException(Exception e)
        {
            return e.GetType() == typeof(PromisException) ? (PromisException)e : new PromisException(e);
        }

        public override string StackTrace => base.StackTrace ?? (InnerException != null ? InnerException.StackTrace : "No stack trace.");
        public override string Message => ToString();

        /// <summary>
        /// Builds up and parses all the necessary data for this exception (details for the developer + details for the user)
        /// </summary>
        private void ConstructorBuilding()
        {
            ErrorDetailsUserLevel = new ErrorDetails
            {
                UniqueId = Guid.NewGuid(),//each error will have this way an unique identifier
                Causes = ComputeCauses()
            };

            //identify starter method name
            var stackTrace = InnerException != null ? new StackTrace(InnerException) : new StackTrace(this);
            var methods = stackTrace.FrameCount > 0 ? stackTrace.GetFrames().Select(f => f.GetMethod()).ToList() : new List<System.Reflection.MethodBase>();
            var currentAssemblies = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.FullName.Contains("Promis.NET.")).ToList();
            methods = methods.Count > 0 ? methods.Where(m => m.DeclaringType != null && currentAssemblies.Contains(m.DeclaringType.Assembly)).ToList() : new List<System.Reflection.MethodBase>();
            if (methods != null && methods.Count > 0)
                methods.Reverse();//inverse order

            // to the user, send only the logical method from the services (exception rises in Controllers => to identify the one from services = [1] (2nd) )
            ErrorDetailsUserLevel.StartingMethod = methods.Count > 1 ? methods[1].Name : (methods.Count > 0 ? methods[0].Name : "can't identify any");
            ErrorDetailsUserLevel.StartingMethod = Global.RegexThatSplitsByCapitalLetters.Replace(ErrorDetailsUserLevel.StartingMethod, " ");

            // until here, they both have the same constitution
            ErrorDetailsDevLevel = new DeveloperErrorDetails(ErrorDetailsUserLevel);

            //at the developer level, send all the method chain
            var methodStarterDev = methods.Count > 0 ? string.Join(" -> ", methods.Select(m => m.Name)) : "can't identify any exactly";
            ErrorDetailsDevLevel.StartingMethod = Global.RegexThatSplitsByCapitalLetters.Replace(methodStarterDev, " ");

            // sets all the inner exception messages + the Stack Trace
            ComputeDeveloperMessage();

            // the user sees the upper error only
            var userMessage = "Error. ";
            if (ErrorDetailsDevLevel.FullMessageList.Count > 0)
                userMessage += ErrorDetailsDevLevel.FullMessageList[0];
            if (ErrorDetailsDevLevel.FullMessageList.Count > 1)
                userMessage += " " + ErrorDetailsDevLevel.FullMessageList[ErrorDetailsDevLevel.FullMessageList.Count - 1];
            userMessage += ".. If the error persists, please contact the system administrator.";
            ErrorDetailsUserLevel.Message = userMessage;

            // write the details with the logger
            InvokeLogger();
        }

        /// <summary>
        /// </summary>
        /// <returns>Returns the 'causes' of the exceptions, meaning the type of exception. For example, for "IndexOutOfRange" will return "Causes: Index Out of Range"</returns>
        private List<string> ComputeCauses()
        {
            var e = (Exception)this;
            var causes = new List<string>();
            
            while (e != null)
            {
                if(e.GetType().Name != "PromisException")//do not add this exception
                    causes.Add(Global.RegexThatSplitsByCapitalLetters.Replace(e.GetType().Name, " "));
                e = e.InnerException;
            }

            if (causes.Count == 0)
                causes.Add("Unknown exception cause.");


            return causes.Distinct().ToList();
        }

        /// <summary>
        /// Adds All the Inner exceptions messages + the stack trace to the details object
        /// </summary>
        private void ComputeDeveloperMessage()
        {
            var e = (Exception)this;
            
            while (e != null)
            {
                if(!string.IsNullOrWhiteSpace(e.Message) && !(e.Message == ToString()))
                    ErrorDetailsDevLevel.FullMessageList.Add(e.Message);
                e = e.InnerException;
            }

            if (ErrorDetailsDevLevel.FullMessageList.Count == 0)
                ErrorDetailsDevLevel.FullMessageList.Add("Unknown exception happened.");

            ErrorDetailsDevLevel.Message = ErrorDetailsDevLevel.FullMessageList[0];
            ErrorDetailsDevLevel.StackTrace = StackTrace;
        }

        /// <summary>
        /// Invokes the logger to write down the exception details.
        /// </summary>
        private void InvokeLogger()
        {
            if (Logger != null)
                Logger.Log(ToString(), LoggingLevel.Exception);//logs the message, with the proper "new line"
        }

        public override string ToString()
        {
            return ErrorDetailsDevLevel.ToString();
        }
    }
}
