﻿using System.Collections.Generic;
using System.Linq;

namespace Promis.NET.Utilities.Exceptions
{
    public class DeveloperErrorDetails: ErrorDetails
    {
        public List<string> FullMessageList { get; set; }
        public string StackTrace { get; set; }

        public DeveloperErrorDetails(ErrorDetails details)
        {
            Causes = details.Causes;
            StartingMethod = details.StartingMethod;
            Message = details.Message;
            UniqueId = details.UniqueId;
            FullMessageList = new List<string>();
            StackTrace = string.Empty;
        }

        public DeveloperErrorDetails(ErrorDetails details, List<string> allInnerMessages, string stackTrace)
        {
            Causes = details.Causes;
            StartingMethod = details.StartingMethod;
            Message = details.Message;
            UniqueId = details.UniqueId;
            FullMessageList = allInnerMessages;
            StackTrace = stackTrace;
        }

        public override string ToString()
        {
            string fullMessageListStr = FullMessageList.Count > 0 ? string.Join(".\n", FullMessageList.Select(m => "\t - " + m)) : "[]";
            string stackTraceStr = !string.IsNullOrWhiteSpace(StackTrace) ? StackTrace : "unknown yet";
            return base.ToString() + "\n" + "Inner messages list:\n" + fullMessageListStr + ".\n" + "Stack trace:\n" + stackTraceStr;
        }
    }
}
