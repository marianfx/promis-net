﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Linq;
using System.Web;
using System;

namespace Promis.NET.Utilities.Models
{
    public class Global
    {
        public static Random randomGenerator = new Random();
        public static int GenerateRandomNumber(int initVal, int lastVal)
        {
            int randomNumber = randomGenerator.Next(initVal, lastVal) + 1;
            return randomNumber;
        }

        public static string NumberToString(int number, bool isCaps)
        {
            Char c = (Char)((isCaps ? 65 : 97) + (number - 1));
            return c.ToString();
        }

        /// <summary>
        /// Tries parsing a string by a separator and extract the year. If not possible, will return the current year.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static int YearFromString(string container, char separator = '-')
        {
            if (string.IsNullOrWhiteSpace(container) || !container.Contains(separator))
                return DateTime.Now.Year;
            var splitted = container.Split(separator);
            var possibleYear = splitted.Where(s => s.Length == 4).FirstOrDefault();
            if (string.IsNullOrWhiteSpace(possibleYear))
                return DateTime.Now.Year;
            bool ok = int.TryParse(possibleYear, out int year);
            if (!ok)
                return DateTime.Now.Year;
            return year;
        }


        /// <summary>
        /// Tries parsing a string by a separator and extract the month. If not possible, will return the current year.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static int MonthFromString(string container, char separator = '-')
        {
            if (string.IsNullOrWhiteSpace(container) || !container.Contains(separator))
                return DateTime.Now.Month;
            var splitted = container.Split(separator);
            var possibleMonth = splitted.Where(s => s.Length >= 1 && s.Length <= 2).FirstOrDefault();
            if (string.IsNullOrWhiteSpace(possibleMonth))
                return DateTime.Now.Month;
            bool ok = int.TryParse(possibleMonth, out int month);
            if (!ok)
                return DateTime.Now.Month;
            return month;
        }

        /// <summary>
        /// Given a datacell from a C# DataTable Row object, converts that to a String value, doing the appropriate checks.
        /// </summary>
        /// <param name="dataCell">Should be the value from a DataRow Cell (eg row[0][0])</param>
        /// <param name="whatToReturnWhenNothing">This method will check  to see if the value is DbNull or null. If so, you can specify to return a default value on null (otherwise it will return plain null).</param>
        /// <returns></returns>
        public static string DataCellToString(object dataCell, string whatToReturnWhenNothing = null)
        {
            if (DBNull.Value == dataCell || dataCell == null)
                return (whatToReturnWhenNothing ?? null);

            return dataCell.ToString();
        }

        public static int DifferenceInMonths(int startMonth, int startYear, int endMonth, int endYear)
        {
            return ((endYear - startYear) * 12 + endMonth - startMonth + 1);
        }

        public static Regex RegexThatSplitsByCapitalLetters = new Regex(@"(?<=[A-Z])(?=[A-Z][a-z]) |
                                                                         (?<=[^A-Z])(?=[A-Z]) |
                                                                         (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);


        public static string RolesSeparator = "$";
        public static string DefaultTheme = "dark_blue";
        public static string DefaultLanguage = "en";

        public static string DatabaseUserPassword = "Happy";
        public static string MainDatabaseName = "promisweb";


        private static bool _countriesLoaded = false;
        private static List<Country> _countries = new List<Country>();
        public static List<Country> Countries
        {
            get
            {
                if (_countriesLoaded)
                    return _countries;

                var cPath = HttpContext.Current.Server.MapPath("~/App_Data/Countries.xml");
                var xdoc = XDocument.Load(cPath);
                var root = xdoc.Descendants("root").FirstOrDefault();
                var countries = from country in root.Descendants("option")
                                select new Country()
                                {
                                    Name = country.Value,
                                    Code = country.Attribute("value").Value
                                };

                _countriesLoaded = true;
                _countries = countries.ToList();
                return _countries;
            }
        }

        public static string DeformatData(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                value = "";

            if (value != "")
            {
                var newValue = value.Split('/');
                if (newValue.Length > 1)
                    return newValue[2] + newValue[1] + newValue[0];
                else
                    return value;
            }
            else
                return "";
        }
    }
}
