﻿
namespace Promis.NET.Utilities.Models
{
    public class DataKeeper
    {
        public string ColumnName { get; set; }
        public dynamic Value { get; set; }

        public DataKeeper()
        {
            ColumnName = string.Empty;
            Value = null;
        }

        public DataKeeper(string columnName, dynamic value)
        {
            ColumnName = columnName;
            Value = value;
        }

        public DataKeeper(string columnName)
        {
            ColumnName = columnName;
            Value = null;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
                return false;

            var objAsThis = obj as DataKeeper;
            return ColumnName == objAsThis.ColumnName && Value == objAsThis.Value;
        }

        public override int GetHashCode()
        {
            return ColumnName.GetHashCode() + Value.GetHashCode();
        }
    }
}
