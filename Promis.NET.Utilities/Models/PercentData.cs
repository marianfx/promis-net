﻿namespace Promis.NET.Utilities.Models
{
    public class PercentData
    {
        public string Month { get; set; }
        public string Week { get; set; }

        public double Percent { get; set; }
        public double PercentMonthly { get; set; }
        public double PercentWeekly { get; set; }
    }
}
