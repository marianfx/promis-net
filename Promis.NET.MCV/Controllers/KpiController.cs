﻿using Promis.NET.DataAccess.Repositories;
using Promis.NET.Localization;
using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.MCV.ViewModels.Engineering;
using Promis.NET.MCV.ViewModels.Kpi;
using Promis.NET.MCV.ViewModels.Project;
using Promis.NET.Roles;
using Promis.NET.Services.Extensions;
using Promis.NET.Services.Models.Engineering;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Promis.NET.MCV.Controllers
{
    [Authorize]
    [RoutePrefix("kpis")]
    public class KpiController : Controller
    {
        private IKpiService _kpiService;
        private ControllerValidator _validator = new ControllerValidator();

        public KpiController(IKpiService kpiService)
        {
            _kpiService = kpiService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _kpiService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        //GET: kpis/ (the module layout)
        [Route("")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadProjectCompletionKpis)]
        public ActionResult Index()
        {
            var viewModel = new ModuleLayoutViewModel()
            {
                ModuleTitle = LocalResources.ProjectCompletionKpis,
                ModuleVersion = "F010.V1.0.0.0",
                ContentContainerId = "kpi-content",
                WorkingPath = "/kpis",
                PageDescriptorsContainer = new Dictionary<string, string>()
                {
                    { "home", LocalResources.CompletionKpis },
                    { "overview", LocalResources.SystemCompletionOverview },
                    { "subcontractors", LocalResources.SubcontractorPerformanceIndicators },
                    { "milestones", LocalResources.SystemMilestonesFollowup }
                },
                StyleSheetsList = new List<string>() { "/pages/kpi.css"}
            };

            return View("_ModuleLayout", viewModel);
        }

        //GET: kpis/{projectId}/partial
        [Route("{projectId}/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadProjectCompletionKpis)]
        public ActionResult IndexPartial(string projectId)
        {
            var viewModel = new CompletionKpiViewModel();
            var htmlFileName = "_Index";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Categories = _kpiService.GetAllKpiCategories();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: kpis/{projectId}/load/partial[?code=xxx]
        [Route("{projectId}/load/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadProjectCompletionKpis)]
        public ActionResult LoadedKpisPartial(string projectId, string code = null)
        {
            var viewModel = new CompletionLoadedKpisViewModel();
            var htmlFileName = "_IndexKpis";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            if (code != null)
                code = HttpUtility.UrlDecode(code);

            try
            {
                var isFavorites = string.IsNullOrWhiteSpace(code);
                viewModel.Kpis = _kpiService.GetAllKpis(code, isFavorites);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: kpis/{projectId}/favorites/partial
        [Route("{projectId}/favorites/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadProjectCompletionKpis)]
        public ActionResult FavoritesSetupPartial(string projectId)
        {
            var viewModel = new CompletionLoadedKpisViewModel();
            var htmlFileName = "_IndexFavorites";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Kpis = _kpiService.GetAllKpis();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        #region Completion Overview
        //GET: kpis/{projectId}/overview/partial
        [Route("{projectId}/overview/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadProjectCompletionKpis)]
        public ActionResult OverviewPartial(string projectId)
        {
            var viewModel = new ProjectCertificationViewModel();
            var htmlFileName = "_Overview";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }
            
            try
            {
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: kpis/{projectId}/overview/indicators/partial[?system=xxx]
        [Route("{projectId}/overview/indicators/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadProjectCompletionKpis)]
        public ActionResult OverviewIndicatorsPartial(string projectId, string system = null)
        {
            var viewModel = new CompletionOverviewViewModel();
            var htmlFileName = "_OverviewIndicators";
            var errors = _validator.ValidateString(projectId, "Project Identifier")
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            system = system ?? HttpUtility.UrlDecode(system);
            try
            {
                var repo = _kpiService.GetRepository<KpiRepository>();
                var yearCode = DateTime.Now.YearAsString();
                var weekCode = DateTime.Now.WeekOfTheYearAsTwoDigitString();

                var totalMhrsConstruction = repo.GetTotalMhrsConstruction(yearCode, weekCode, system);
                viewModel.PercentConstruction = repo.GetConstructionProgress(yearCode, weekCode, totalMhrsConstruction, system);
                var totalMhrsPrecom = repo.GetTotalMhrsPrecommissioning(yearCode, weekCode, system);
                viewModel.PercentPreCommissioning = repo.GetPrecommissioningProgress(yearCode, weekCode, totalMhrsPrecom, system);
                var totalMhrsCom = repo.GetTotalMhrsCommissioning(yearCode, weekCode, system);
                viewModel.PercentCommissioning = repo.GetCommissioningProgress(yearCode, weekCode, totalMhrsCom, system);
                var totalCountLoop = repo.GetLoopCheckCount(system);
                viewModel.PercentLoopTests = repo.GetLoopCheckProgress(totalCountLoop, system);

                var punchlistData = repo.GetPunchlistStatus(system);
                viewModel.PunchlistLabels = punchlistData.Select(x => x.ColumnName);
                viewModel.PunchlistValues = punchlistData.Select(x => (int)x.Value);
                
                var constructionData = repo.GetConstructionStatus(yearCode, weekCode, system);
                viewModel.ConstructionLabels = constructionData.Select(x => x.ColumnName);
                viewModel.ConstructionValues = constructionData.Select(x => (double)x.Value);

                var preCommissioningData = repo.GetPreCommissioningStatus(yearCode, weekCode, system);
                viewModel.PreCommissioningLabels = preCommissioningData.Select(x => x.ColumnName);
                viewModel.PreCommissioningValues = preCommissioningData.Select(x => (double)x.Value);

                var commissioningData = repo.GetCommissioningStatus(yearCode, weekCode, system);
                viewModel.CommissioningLabels = commissioningData.Select(x => x.ColumnName);
                viewModel.CommissioningValues = commissioningData.Select(x => (double)x.Value);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
        #endregion

        #region Subcontractor Performance Indicators
        //GET: kpis/{projectId}/subcontractors/partial
        [Route("{projectId}/subcontractors/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadProjectCompletionKpis)]
        public ActionResult GetSubcontractorsPartial(string projectId)
        {
            var viewModel = new CompletionKPISubContractorViewModel();
            var htmlFileName = "_Subcontractors";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Subcontractors = _kpiService.GetSubContractors();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: kpis/{projectId}/subcontractors/sow-table/partial
        [Route("{projectId}/subcontractors/sow-table/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadProjectCompletionKpis)]
        public ActionResult GetSOWData(string projectId, string subcontractorCode)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .ValidateString(subcontractorCode, LocalResources.TheCodeForTheSubcontractor)
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Base = new GridData();
                viewModel.Base.Columns = _kpiService.GetRepository<KpiRepository>().GetColumnsNecessarySOWTable();
                viewModel.Base.Rows = _kpiService.GetSOWData(subcontractorCode);
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = "kpi-sow";
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: kpis/{projectDb}/subcontractors/analisys/partial
        [Route("{projectDb}/subcontractors/analisys/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadProjectCompletionKpis)]
        public ActionResult GetSubcontractorsPerformanceAndProgressPartial(string projectDb, string subcontractorCode, string subActivity)
        {
            var viewModel = new CompletionSubcontractorPerformanceViewModel();
            var htmlFileName = "_SubcontractorsAnalisys";
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                   .ValidateProject(User.Identity, projectDb, "db", "get")
                                   .ValidateString(subcontractorCode, LocalResources.TheCodeForTheSelectedSubcontractor)
                                   .ValidateString(subActivity, LocalResources.TheValueOfSubactivity)
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.AnalysisData = _kpiService.GetForecastAnalisysData(projectDb,subActivity, subcontractorCode);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
        #endregion

        //GET: kpis/{projectId}/milestones/partial
        [Route("{projectId}/milestones/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadProjectCompletionKpis)]
        public ActionResult MilestonesPartial(string projectId)
        {
            var viewModel = new ProjectPlanningViewModel();
            var htmlFileName = "_Milestones";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: kpis/{projectId}/milestones/table/partial
        [Route("{projectId}/milestones/table/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadProjectCompletionKpis)]
        public ActionResult MilestonesTablePartial(string projectId)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var repo = _kpiService.GetRepository<KpiRepository>();
                var tableName = repo.GetTableNameSystems();
                var columns = repo.GetColumnsNecessaryForSystems();
                viewModel.TableName = tableName;
                viewModel.Base = new Services.Models.Engineering.GridData()
                {
                    Columns = columns
                };
                viewModel.AlternativeColumnNames = columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
    }
}