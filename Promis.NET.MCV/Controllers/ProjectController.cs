﻿using DevTrends.MvcDonutCaching;
using Promis.NET.Localization;
using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.MCV.ViewModels.Engineering;
using Promis.NET.MCV.ViewModels.Project;
using Promis.NET.Roles;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;

namespace Promis.NET.MCV.Controllers
{
    [Authorize]
    [RoutePrefix("project")]
    public class ProjectController : Controller
    {
        private IProjectService _projectService;
        private IAccountService _accountService;
        private IEngineeringService _engineeringService;
        private ControllerValidator _validator = new ControllerValidator();

        public ProjectController(IProjectService projectService, IAccountService accountService, IEngineeringService engineeringService)
        {
            _projectService = projectService;
            _accountService = accountService;
            _engineeringService = engineeringService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _engineeringService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        //GET: project/ (the module layout)
        [Authorize(Roles = LocalRoles.ResponsabilityAdmin)]
        public ActionResult Index()
        {
            var viewModel = new ModuleLayoutViewModel()
            {
                ModuleTitle = LocalResources.ProjectConfiguration,
                ModuleVersion = "F003.V1.0.0.0",
                ContentContainerId = "setup-content",
                WorkingPath = "/project",
                PageDescriptorsContainer = new Dictionary<string, string>()
                {
                    { "home", LocalResources.GeneralInformation},
                    { "certification",LocalResources.CertificationSteps},
                    { "planning", LocalResources.PlanningCurves},
                    { "cutoff", LocalResources.CutoffCalendar},
                    { "users", LocalResources.UserProfiles}
                },
                StyleSheetsList = new List<string>() { "/pages/project.css"}
            };

            return View("_ModuleLayout", viewModel);
        }

        //GET: project/partial
        [Route("{projectId}/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin)]
        public ActionResult IndexPartial(string projectId)
        {
            var viewModel = new ProjectViewModel();
            var htmlFileName = "_Index";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.Base = _projectService.GetProjectById(projectId);
                viewModel.MainActivities = _projectService.GetMainActivitiesList(projectDatabase);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
        
        //GET: project/{projectId}/certification/partial
        [Route("{projectId}/certification/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin)]
        public ActionResult CertificationPartial(string projectId)
        {
            var viewModel = new ProjectCertificationViewModel();
            var htmlFileName = "_Certification";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }
            
            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.MainActivities = _projectService.GetMainActivitiesList(projectDatabase, "<>");
                viewModel.Modules = _projectService.GetActivityActions(projectDatabase);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        [Route("{projectId}/certification/sub")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin)]
        public ActionResult GetSubactivitiesTable(string projectId)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var tableName = _projectService.GetTableNameSubactivities();
                viewModel.Base = new Services.Models.Engineering.GridData();
                viewModel.Base.Columns = _projectService.GetColumnsNecessaryForSubactivities();
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = tableName;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: project/{projectId}/planning/partial
        [Route("{projectId}/planning/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin)]
        public ActionResult PlanningPartial(string projectId)
        {
            var viewModel = new ProjectPlanningViewModel();
            var htmlFileName = "_Planning";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.MainActivities = _projectService.GetMainActivitiesList(projectDatabase, "<>");

                // compute months / year
                var projectData = _projectService.GetProjectById(projectId);
                viewModel.StartYear = projectData.StartYear;
                viewModel.StartMonth = projectData.StartMonth;
                viewModel.MonthsCount = projectData.MonthsCount;
                viewModel.MonthsName = Localization.Global.Months;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: project/{projectId}/planning/partial/menu?mainActivity=x[&includeNonzero=true&ieEditable=true&isOverall=false]
        [Route("{projectId}/planning/partial/menu")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public ActionResult PlanningMenuPartial(string projectId, string mainActivity, bool includeNonzero = true, bool isWithCurves = true, bool isOverall = false, bool isBaselineVisible = true, bool isRescheduleEditable = true)
        {
            var viewModel = new ProjectPlanningMenuViewModel();
            var htmlFileName = "_PlanningMenu";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();
            if (!isOverall)
                errors = _validator.ValidateString(mainActivity, LocalResources.MainActivity).GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.IsWithCurves = isWithCurves;
                viewModel.IsBaselineVisible = isBaselineVisible;
                viewModel.IsREditable = isRescheduleEditable;
                viewModel.Activities = _projectService.GetAllAssociatedActivitiesWithPlanningData(projectId, projectDatabase, mainActivity, includeNonzero, isWithCurves, isOverall);
                viewModel.Budget = _projectService.GetActivityBudget(projectDatabase, mainActivity);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: project/{projectId}/cutoff/partial
        [Route("{projectId}/cutoff/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin)]
        public ActionResult CutoffPartial(string projectId)
        {
            var viewModel = new ProjectCutoffViewModel();
            var htmlFileName = "_Cutoff";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }
            
            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.CutoffDays = _projectService.GetAllCutoffDaysOrderedByWeek(projectDatabase);
                viewModel.FirstDay = viewModel.CutoffDays.Count > 0 ? viewModel.CutoffDays[0] : DateTime.Now;
                viewModel.LastDay = viewModel.CutoffDays.Count > 0 ? viewModel.CutoffDays[viewModel.CutoffDays.Count - 1] : DateTime.Now;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: project/{projectId}/users/partial
        [Route("{projectId}/users/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin)]
        public ActionResult UsersPartial(string projectId, string userId = null)
        {
            var viewModel = new ProjectUsersViewModel();
            var username = User.Identity.Name;
            var projectDatabase = User.Identity.GetProjectDatabaseName();

            try
            {
                viewModel.User = !string.IsNullOrWhiteSpace(userId) ? _accountService.GetUserDetailsById(userId) :  _accountService.GetUserDetails(username, projectId);
                viewModel.User.Password = "";//do not send password
                viewModel.Users = _accountService.GetAllNonAdminUsersForProject(projectId);
                viewModel.Modules = _projectService.GetModules();
                viewModel.Subcontractors = _projectService.GetSubcontractors(projectDatabase);
                viewModel.Activities = _projectService.GetMainActivitiesList(projectDatabase, "*");
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView("_Users", viewModel);
        }

        #region Child Actions
        //This attribute specifies to the client (browser) to catch its output per project
        [ChildActionOnly]
        [AllowAnonymous]
        [DonutOutputCache(Duration = 86400, VaryByParam = "id")]//child actions do not need Location set
        public ActionResult GetProjectData(string id)
        {
            var viewModel = new ProjectDataViewModel();
            var htmlFileName = "_ProjectDetails";
            var errors = _validator.ValidateString(id, LocalResources.Project)
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Base = _projectService.GetProjectSmallDataById(id);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
        
        //GET: project/all/{username}
        [AllowAnonymous]
        [Route("all/{username}")]
        public ActionResult GetAllProjectsForUserData(string username)
        {
            var viewModel = new List<ProjectViewModel>();
            var htmlFileName = "_ProjectDetailsMultiple";
            var errors = _validator.ValidateString(username, LocalResources.Username)
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projects = _projectService.GetAllProjectsDataForUsername(username);
                foreach (var project in projects)
                {
                    viewModel.Add(new ProjectViewModel(project));
                }
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //PUT: project/{projectId}/refresh
        [HttpPost]
        [AllowAnonymous]
        [Route("{projectId}/refresh")]
        public ActionResult Refresh(string projectId)
        {
            var cacheManager = new OutputCacheManager();
            cacheManager.RemoveItem("Project", "GetProjectData", new { id = projectId });
            return Json(new { success = true });
        }
        #endregion
    }
}