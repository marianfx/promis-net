﻿using Promis.NET.DataAccess.Models;
using Promis.NET.Localization;
using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.Models.Engineering;
using Promis.NET.MCV.ViewModels;
using Promis.NET.Roles;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using Promis.NET.Utilities.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace Promis.NET.MCV.Controllers.ApiControllers
{
    [Authorize]
    [RoutePrefix("api/engineering")]
    public class EngineeringApiController : ApiController
    {
        private IEngineeringService _engineeringService;
        private ControllerValidator _validator = new ControllerValidator();

        public EngineeringApiController(IEngineeringService engineeringService)
        {
            _engineeringService = engineeringService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _engineeringService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }


        //GET: api/engineering/summary/civil    
        [Route("summary/civil")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadConstructionEngineeringDatabase)]
        public IHttpActionResult GetCivilSummary()
        {
            try
            {
                var civilData = _engineeringService.GetSummaryForCivilTables();
                return new Envelope(Request).SetData(civilData).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: api/engineering/summary/engineering    
        [Route("summary/engineering")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadConstructionEngineeringDatabase)]
        public IHttpActionResult GetEngineeringSummary()
        {
            try
            {
                var engineeringData = _engineeringService.GetSummaryForEngineeringTables();
                return new Envelope(Request).SetData(engineeringData).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        
        ///GET: api/engineering/tables/{projectDb}/{dataTable}/data
        [HttpGet]
        [Route("tables/{projectDb}/{dataTable}/data")]
        public IHttpActionResult GetDataFromDataTable(string projectDb, string dataTable, DataTableRequestData requestData)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                 .ValidateString(dataTable, LocalResources.Datatable)
                                 .ValidateProject(User.Identity, projectDb, "db", "get")
                                 .ValidateUserToBeInAnyReadModuleRole(User)
                                 .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                //get the number of records in the table
                var noOfTableRecords = _engineeringService.GetRowsNumberForTable(dataTable, requestData.AdvFilters, requestData.Join, requestData.Columns, requestData.Distinct);
                //get the number of records after the filters where applied
                var filteredRec = _engineeringService.GetRowsNumberForFilteredData(dataTable, requestData.AdvFilters, requestData.Join, requestData.Search.Value, requestData.Columns, requestData.Distinct);
                //get the records after filtering data              
                var results = _engineeringService.GetDataForTableServerSide(dataTable, requestData.Start, requestData.Length, requestData.Order, requestData.Search, requestData.Columns, requestData.AdvFilters, requestData.Join, requestData.Distinct);

                //return the result as Json Data; the same for error, because this is Datatables.NET specific output
                var output = new DataTableResponseData()
                {
                    recordsTotal = Convert.ToInt32(noOfTableRecords),
                    recordsFiltered = Convert.ToInt32(filteredRec),
                    draw = requestData.Draw,
                    data = results.ToArray()
                };
                return Json(output);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return Json(new { error = pe.ErrorDetailsUserLevel.Message });
            }
        }

        //POST: api/engineering/tables/{projectDb}/{dataTable} 
        [HttpPost]
        [Route("tables/{projectDb}/{dataTable}")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWriteConstructionEngineeringDatabase)]
        public IHttpActionResult InsertDataForTable(string projectDb, string dataTable, IEnumerable<DataKeeper> values)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                        .ValidateString(dataTable, LocalResources.Datatable)
                        .ValidateArray(values,LocalResources.ValuesList)
                        .ValidateProject(User.Identity, projectDb, "db", "get")
                        .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var rowId = _engineeringService.InsertData(dataTable, projectDb, values.ToList());
                return new Envelope(Request).SetData(rowId).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //PUT: api/engineering/tables/{projectDb}/{dataTable}/{rowId}
        [HttpPut]
        [Route("tables/{projectDb}/{dataTable}/{rowId}")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWriteConstructionEngineeringDatabase)]
        public IHttpActionResult UpdateDataForTable(string projectDb, string dataTable, string rowId, IEnumerable<DataKeeper> values)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                        .ValidateString(dataTable, LocalResources.Datatable)
                        .ValidateString(rowId,"Row Id")
                        .ValidateArray(values, LocalResources.ValuesList)
                        .ValidateProject(User.Identity, projectDb, "db", "get")
                        .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                _engineeringService.UpdateData(dataTable, projectDb, values.ToList(), rowId);
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //DELETE: api/engineering/tables/{projectDb}/{dataTable}/{rowId}
        [HttpDelete]
        [Route("tables/{projectDb}/{dataTable}/{rowId}")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWriteConstructionEngineeringDatabase)]
        public IHttpActionResult DeleteDataForTable(string projectDb, string dataTable, string rowId)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                        .ValidateString(dataTable, LocalResources.Datatable)
                        .ValidateString(rowId, LocalResources.RowId)
                        .ValidateProject(User.Identity, projectDb, "db", "get")
                        .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                _engineeringService.DeleteData(dataTable, rowId);
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }


        //GET: api/engineering/tables/{projectDb}/{dataTable}/export
        [HttpGet]
        [Route("tables/{projectDb}/{dataTable}/export")]
        public IHttpActionResult ExportTableToExcel(string projectDb, string dataTable, ExportRequestData requestData)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                        .ValidateString(dataTable, LocalResources.Datatable)
                        .ValidateObject(requestData, LocalResources.RequestData)
                        .ValidateProject(User.Identity, projectDb, "db", "get")
                        .ValidateUserToBeInAnyReadModuleRole(User)
                        .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var username = User.Identity.Name;
                var dir = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Excel Exports/");
                var fileName = _engineeringService.GenerateExcel(dataTable, projectDb, username, dir, !requestData.IsFullExport, requestData.AdvFilters, requestData.Join, requestData.ColumnNames, requestData.GroupByColumns);
                var url = "/content/download?type=excel&filePath=" + fileName;
                return new Envelope(Request).SetData(url).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //POST: api/engineering/tables/{projectDb}/{dataTable}/import
        [HttpPost]
        [Route("tables/{projectDb}/{dataTable}/import")]
        public IHttpActionResult ImportDataFromFile(string projectDb, string dataTable, string filePath, bool overWrite)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                        .ValidateString(dataTable, LocalResources.Datatable)
                        .ValidateString(filePath, LocalResources.FilePath)
                        .ValidateProject(User.Identity, projectDb, "db", "get")
                        .ValidateUserToBeInAnyWriteModuleRole(User)
                        .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            filePath = HttpUtility.UrlDecode(filePath);
            try
            {
                var dir = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/");
                filePath = Path.Combine(dir, filePath);
                _engineeringService.ImportDataFromExcel(dataTable, projectDb, filePath, overWrite);
                return new Envelope(Request).SetOk().GetResponse();                
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
    }
}
