﻿using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.Services.Models.Dashboard;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace Promis.NET.MCV.Controllers.ApiControllers
{
    [Authorize]
    [RoutePrefix("api/charts")]
    public class ChartsApiController : ApiController
    {
        private IDashboardService _dashboardService;

        public ChartsApiController(IDashboardService dashboardService)
        {
            _dashboardService = dashboardService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _dashboardService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        //GET: api/charts
        [Route("")]
        public IHttpActionResult Get()
        {
            var allData = new DashboardData();

            try
            {
                _dashboardService.GetChartsProgressData(ref allData);
                _dashboardService.GetChartsDelayData(ref allData);
                _dashboardService.GetChartsTrendsData(ref allData);
                _dashboardService.GetChartsMainActivitiesData(ref allData);
                _dashboardService.GetChartsOutstandingsData(ref allData);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }

            return new Envelope(Request).SetData(allData).GetResponse();
        }

        //GET: api/charts/progress[?activity=xx]
        [Route("progress")]
        public IHttpActionResult GetProgressData(string activity = null)
        {
            var allData = new DashboardData();
            if (!string.IsNullOrWhiteSpace(activity))
                activity = HttpUtility.UrlDecode(activity);

            try
            {
                _dashboardService.GetChartsProgressData(ref allData, activity);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }

            return new Envelope(Request).SetData(new
            {
                 allData.ProgressChartLabels,
                 allData.ProgressChartValuesPlanned,
                 allData.ProgressChartValuesActual,
                 allData.ProgressChartValuesCertified,
                 allData.ProgressMonthlyChartValuesActual
            })
            .GetResponse();
        }
        
        //GET: api/charts/delay
        [Route("delay")]
        public IHttpActionResult GetDelayData()
        {
            var allData = new DashboardData();

            try
            {
                _dashboardService.GetChartsDelayData(ref allData);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }

            return new Envelope(Request).SetData(new
            {
                 allData.ProgressChartLabels,
                 allData.ProgressChartValuesPlanned,
                 allData.ProgressChartValuesActual,
                 allData.ProgressChartValuesCertified,
                 allData.ProgressMonthlyChartValuesActual,
                 allData.ProgressMonthlyChartValuesDelay
            })
            .GetResponse();
        }

        //GET: api/charts/trends
        [Route("trends")]
        public IHttpActionResult GetTrendsData()
        {
            var allData = new DashboardData();

            try
            {
                _dashboardService.GetChartsTrendsData(ref allData);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }

            return new Envelope(Request).SetData(new
            {
                 allData.TrendingChartPlannedPercent,
                 allData.TrendingChartPlannedPercentWeekly,
                 allData.TrendingChartPlannedPercentMonthly,
                 allData.TrendingChartActualPercent,
                 allData.TrendingChartActualPercentWeekly,
                 allData.TrendingChartActualPercentMonthly,
                 allData.TrendingChartCertifiedPercent,
                 allData.TrendingChartCertifiedPercentWeekly,
                 allData.TrendingChartCertifiedPercentMonthly
            })
            .GetResponse();
        }

        //GET: api/charts/activities
        [Route("activities")]
        public IHttpActionResult GetMainActivitiesData()
        {
            var allData = new DashboardData();

            try
            {
                _dashboardService.GetChartsMainActivitiesData(ref allData);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }

            return new Envelope(Request).SetData(new
            {
                 allData.ActivitiesCodes,
                 allData.ActivitiesPlannedData,
                 allData.ActivitiesActualData,
                 allData.ActivitiesWithProgresses
            })
            .GetResponse();
        }

        //GET: api/charts/outstandings
        [Route("outstandings")]
        public IHttpActionResult GetOutstandingData()
        {
            var allData = new DashboardData();

            try
            {
                _dashboardService.GetChartsOutstandingsData(ref allData);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }

            return new Envelope(Request).SetData(new
            {
                 allData.OutstandingChartFlags,
                 allData.OutstandingChartFlagDescriptions,
                 allData.OutstandingChartCounts,
                 allData.PunchlistCount,
                 allData.PunchlistDoneCount,
                 allData.ClearancePercentage
            })
            .GetResponse();
        }
    }
}