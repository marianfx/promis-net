﻿using Promis.NET.Localization;
using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.Roles;
using Promis.NET.Services.Models.Kpi;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace Promis.NET.MCV.Controllers.ApiControllers
{
    [Authorize]
    [RoutePrefix("api/kpis")]
    public class KpiApiController : ApiController
    {
        private IKpiService _kpiService;
        private ControllerValidator _validator = new ControllerValidator();

        public KpiApiController(IKpiService kpiService)
        {
            _kpiService = kpiService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _kpiService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        #region Home
        //PUT: api/kpis/{projectDb}/favorites
        [HttpPut]
        [Route("{projectDb}/favorites")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWriteProjectCompletionKpis)]
        public IHttpActionResult UpdateFavoriteKpis(string projectDb, IEnumerable<KeyValuePair<string, bool>> data)
        {
            var validEntries = new string[] { "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10", "s11", "s12" };
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .ValidateArray(data, LocalResources.UpdateList)
                                    .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();
            
            try
            {
                _kpiService.UpdateFavoriteKpis(data);
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }


        //GET: api/kpis/{projectDb}/completion?system=xxx
        [HttpGet]
        [Route("{projectDb}/completion")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadProjectCompletionKpis)]
        public IHttpActionResult GetCompletionData(string projectDb, string system)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .ValidateString(system, LocalResources.SystemCode)
                                    .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            system = HttpUtility.UrlDecode(system);
            try
            {
                var data = _kpiService.GetCompletion(system);
                return new Envelope(Request).SetData(data).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        
        //PUT: api/kpis/{projectDb}/completion?system=xxx
        [HttpPut]
        [Route("{projectDb}/completion")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWriteProjectCompletionKpis)]
        public IHttpActionResult UpdateCompletionData(string projectDb, Completion data)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .ValidateObject(data, LocalResources.CompletionData)
                                    .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();
            
            try
            {
                _kpiService.SaveCompletion(data.GetCompletion());
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        #endregion

        #region Subcontractor Performance Indicators
        //GET: api/kpis/data/{projectDb}/charts //need to complete the method
        [HttpGet]
        [Route("data/{projectDb}/charts")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadProjectCompletionKpis)]
        public IHttpActionResult GetKpiChartsData(string projectDb, string selectedSubcontractor,string subActivity)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                   .ValidateProject(User.Identity, projectDb, "db", "get")
                                   .ValidateString(selectedSubcontractor, LocalResources.SelectedSubcontractorCode)
                                   .ValidateString(subActivity, LocalResources.SubActivity)
                                   .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var allData = new KPIsChartData();
                allData = _kpiService.GetKpiChartsData(projectDb, selectedSubcontractor, subActivity);
                return new Envelope(Request).SetData(new
                {
                    ChartBarsLabels = allData.ChartBarsLabels,
                    ChartPr = allData.ChartPr,
                    ChartBars = allData.ChartBars,
                    ChartRit = allData.ChartRit,
                    ChartWkty = allData.ChartWkty,
                    ChartWorkW=allData.ChartWorkW
                })
                .GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        #endregion

        #region Milestones PDF
        //GET: api/kpis/{projectId}/export/pdf?systemCode=xxx
        [HttpGet]
        [Route("{projectId}/export/pdf")]
        public IHttpActionResult ExportSystemDataToPdf(string projectId, string systemCode)
        {
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                        .ValidateString(systemCode, LocalResources.SystemCode)
                        .ValidateProject(User.Identity, projectId, "id", "get")
                        .ValidateUserToBeInAnyReadModuleRole(User)
                        .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                systemCode = HttpUtility.UrlDecode(systemCode);
                var pdfDirName = "Reports";
                var serverRelativePath = "App_Data\\PDFs\\" + pdfDirName + "\\";
                var fileName = _kpiService.ExportMilestonesSystemDataToPdf(serverRelativePath, projectId, systemCode);
                var url = pdfDirName + "/" + fileName;
                return new Envelope(Request).SetData(url).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        #endregion
    }
}