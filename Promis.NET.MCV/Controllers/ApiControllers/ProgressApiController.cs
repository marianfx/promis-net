﻿using Promis.NET.Localization;
using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.Roles;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace Promis.NET.MCV.Controllers.ApiControllers
{
    [Authorize]
    [RoutePrefix("api/progress")]
    public class ProgressApiController : ApiController
    {
        private IProgressService _progressService;
        private ControllerValidator _validator = new ControllerValidator();

        public ProgressApiController(IProgressService progressService)
        {
            _progressService = progressService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _progressService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        //PUT: api/progress/{projectDb}/subactivities/quantities
        [HttpPut]
        [Route("{projectDb}/subactivities/quantities")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWritePlanningProgress)]
        public IHttpActionResult UpdateProjectSubActivitiesQuantities(string projectDb, IEnumerable<SubActivity> subactivities)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                   .ValidateArray(subactivities, LocalResources.Subactivities)
                                   .ValidateProject(User.Identity, projectDb, "db", "get")
                                   .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                _progressService.UpdateSubactivitiesQuantities(subactivities);
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //PUT: api/progress/{projectDb}/subactivities/progress
        [HttpPut]
        [Route("{projectDb}/subactivities/{subActivity}/progress")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWritePlanningProgress)]
        public IHttpActionResult UpdateProjectSubActivitiesProgress(string projectDb, string mainActivity, string subActivity, string tag, KeyValuePair<string, string> data)
        {
            var validEntries = new string[] { "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10", "s11", "s12" };
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateString(mainActivity, LocalResources.MainActivity)
                                    .ValidateString(subActivity, LocalResources.SubActivity)
                                    .ValidateString(tag, LocalResources.Tag)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .ValidateString(data.Key, validEntries, valueName: LocalResources.ProgressColumn)
                                    .ValidateDouble(data.Value, LocalResources.ProgressColumnValue)
                                    .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            mainActivity = HttpUtility.UrlDecode(mainActivity);
            subActivity = HttpUtility.UrlDecode(subActivity);
            tag = HttpUtility.UrlDecode(tag);
            try
            {
                var newPercent = _progressService.UpdateSubactivityProgress(projectDb, mainActivity, subActivity, tag, data.Key, data.Value);
                return new Envelope(Request).SetData(newPercent).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: api/progress/{projectDb}/trends/all?mainActivity=xx&activity=yy&subActivity=zz
        [HttpGet]
        [Route("{projectDb}/trends/all")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public IHttpActionResult GetActivitiesAndSubactivitiesTrends(string projectDb, string mainActivity, string activity, string subActivity)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateString(mainActivity, LocalResources.MainActivity)
                                    .ValidateString(activity, LocalResources.RelatedActivity)
                                    .ValidateString(subActivity, LocalResources.SubActivity)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            mainActivity = HttpUtility.UrlDecode(mainActivity);
            activity = HttpUtility.UrlDecode(activity);
            subActivity = HttpUtility.UrlDecode(subActivity);
            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                var percentageData = _progressService.GetPlanningDataForSubactivity(projectDatabase, mainActivity, activity, subActivity);
                return new Envelope(Request).SetData(percentageData).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: api/progress/{projectId}/export/pdf?table=yyy&subActivity=xxx
        [HttpGet]
        [Route("{projectId}/export/pdf")]
        public IHttpActionResult ExportSystemDataToPdf(string projectId, string table, string subActivity)
        {
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                        .ValidateString(table, LocalResources.Datatable)
                        .ValidateString(subActivity, LocalResources.Subactivity)
                        .ValidateProject(User.Identity, projectId, "id", "get")
                        .ValidateUserToBeInAnyReadModuleRole(User)
                        .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                table = HttpUtility.UrlDecode(table);
                subActivity = HttpUtility.UrlDecode(subActivity);
                var pdfDirName = "Reports";
                var serverRelativePath = "App_Data\\PDFs\\" + pdfDirName + "\\";

            var fileName = _progressService.ExportProgressDataToPdf(serverRelativePath, projectId, table, subActivity);
            var url = pdfDirName + "/" + fileName;
                return new Envelope(Request).SetData(url).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
    }
}