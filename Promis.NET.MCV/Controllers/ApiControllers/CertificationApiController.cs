﻿using Promis.NET.MCV.Extensions;
using System.Threading;
using System.Web.Http;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.MCV.ViewModels;
using System;
using Promis.NET.Utilities.Exceptions;
using System.Collections.Generic;
using Promis.NET.Services.Models.Commissioning;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Models.Certification;
using System.Globalization;
using Promis.NET.Roles;
using Promis.NET.Localization;
using System.Web;

namespace Promis.NET.MCV.Controllers.ApiControllers
{
    [Authorize]
    [RoutePrefix("api/certification")]
    public class CertificationApiController : ApiController
    {
        private ICertificationService _certificationService;
        private ICommissioningService _commissioningService;
        private ControllerValidator _validator = new ControllerValidator();

        public CertificationApiController(ICertificationService certificationService, ICommissioningService commissioningService)
        {
            _commissioningService = commissioningService;
            _certificationService = certificationService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _certificationService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _commissioningService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }
        
        #region Assign Plan to Tags

        //GET: api/certification/data/{projectDb}/datatable/{datatable}/type
        [HttpGet]
        [Route("data/{projectDb}/datatable/{datatable}/type")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadCertificationManagement)]
        public IHttpActionResult GetTypecodesForDatatable(string projectDb, string dataTable)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                .ValidateString(dataTable, LocalResources.Datatable)
                                .ValidateProject(User.Identity, projectDb, "db", "get")
                                .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var typecodes = _commissioningService.GetTypecodesForDatatables(dataTable, true);
                return new Envelope(Request).SetData(typecodes).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: api/certification/data/{projectDb}/activity/{mainActivity}/sub
        [HttpGet]
        [Route("data/{projectDb}/activity/{mainActivity}/sub")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadCertificationManagement)]
        public IHttpActionResult GetSubactForDatatable(string projectDb, string mainActivity)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                .ValidateString(mainActivity, LocalResources.MainActivity)
                                .ValidateProject(User.Identity, projectDb, "db", "get")
                                .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var tables = _certificationService.GetDatatables(mainActivity);
                var subactivities = _certificationService.GetSubactivityByMainActivity(mainActivity);

                return new Envelope(Request).SetData(new KeyValuePair<List<Datatables>, List<Activity>>(tables, subactivities)).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        #endregion

        #region Assign SOW to Subcontractors
        //PUT: api/certification/{projectDb}/subcontractors/sow
        [HttpPut]
        [Route("{projectDb}/{dataTable}/subcontractors/sow")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWriteCertificationManagement)]
        public IHttpActionResult InsertSOWForSubcontractor(string projectDb, [FromBody]IEnumerable<SOWForSubcontractors> subactivities)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                               .ValidateObject(subactivities, LocalResources.Subactivities)
                               .ValidateProject(User.Identity, projectDb, "db", "get")
                               .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                _certificationService.UpdateSOWForSubcontractor(subactivities);       
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        #endregion

        #region Certification Summary
        //GET: api/certification/data/{projectDb}/activity/{activity}/table
        [HttpGet]
        [Route("data/{projectDb}/activity/{activity}/table")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadCertificationManagement)]
        public IHttpActionResult GetDatatableByActivity(string projectDb, string activity)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                              .ValidateString(activity, LocalResources.Activity)
                              .ValidateProject(User.Identity, projectDb, "db", "get")
                              .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                if (activity.Equals("undefined")) { activity = " "; }
                var tables = _certificationService.GetDatatablesByActivity(activity);
                return new Envelope(Request).SetData(tables).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        [HttpPut]
        //GET: api/certification/{projectDb}/follou-up/table
        [Route("{projectDb}/follou-up/table")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWriteCertificationManagement)]
        public IHttpActionResult UpdateTablesOnUserChange(string projectDb, FollowUpData data)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                   .ValidateProject(User.Identity, projectDb, "db", "get")
                                   .ValidateObject(data, LocalResources.Data)
                                   .GetErrors();

            try
            {
                var message = _certificationService.UpdateCertTableByUser(projectDb,data.Datatable,data.RowId, data.Ord, data.UserValue);
                return new Envelope(Request).SetData(message).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: api/certification/{projectId}/export/pdf?table=yyy&subActivity=xxx&tag=zzz
        [HttpGet]
        [Route("{projectId}/export/pdf")]
        public IHttpActionResult ExportSystemDataToPdf(string projectId, string table, string subActivity, string tag)
        {
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                        .ValidateString(table, LocalResources.Datatable)
                        .ValidateString(subActivity, LocalResources.Subactivity)
                        .ValidateString(tag, LocalResources.Tag)
                        .ValidateProject(User.Identity, projectId, "id", "get")
                        .ValidateUserToBeInAnyReadModuleRole(User)
                        .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                table = HttpUtility.UrlDecode(table);
                subActivity = HttpUtility.UrlDecode(subActivity);
                tag = HttpUtility.UrlDecode(tag);
                var pdfDirName = "Reports";
                var serverRelativePath = "App_Data\\PDFs\\" + pdfDirName + "\\";
                var fileName = _certificationService.ExportCertificationDataToPdf(serverRelativePath, projectId, table, subActivity, tag);
                var url = pdfDirName + "/" + fileName;
                return new Envelope(Request).SetData(url).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        #endregion
    }
}