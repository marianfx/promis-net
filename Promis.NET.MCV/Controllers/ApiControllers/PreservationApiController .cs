﻿using Promis.NET.Localization;
using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.Roles;
using Promis.NET.Services.Models.Preservation;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using Promis.NET.Utilities.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace Promis.NET.MCV.Controllers.ApiControllers
{
    [Authorize]
    [RoutePrefix("api/preservation")]
    public class PreservationApiController : ApiController
    {
        private IPreservationService _preservationService;
        private ControllerValidator _validator = new ControllerValidator();

        public PreservationApiController(IPreservationService preservationService)
        {
            _preservationService = preservationService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _preservationService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        #region Plan Overview
        //GET: api/preservation/data/{projectDb}/tables/{datatable}/typecodes
        [HttpGet]
        [Route("data/{projectDb}/tables/{dataTable}/typecodes")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public IHttpActionResult GetTypecodesForDatatables(string projectDb, string dataTable)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                   .ValidateString(dataTable, LocalResources.Datatable)
                                   .ValidateProject(User.Identity, projectDb, "db", "get")
                                   .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var typecodes = _preservationService.GetTypecodesNames(dataTable);
                var outputDict = new List<KeyValuePair<string, string>>();
                foreach (var key in typecodes.Keys)
                {
                    foreach (var item in typecodes[key])
                    {
                        outputDict.Add(new KeyValuePair<string, string>(key, item));
                    }
                }

                return new Envelope(Request).SetData(outputDict).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }

        }

        //GET: api/preservation/{projectId}/overview/info?code=xxx)
        [HttpGet]
        [Route("{projectId}/overview/info")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public IHttpActionResult GetPreservationActionsDetails(string projectDb, string code)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                   .ValidateString(code, LocalResources.Code)
                                   .ValidateProject(User.Identity, projectDb, "db", "get")
                                   .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var result = _preservationService.GetAllDataForPreservation(code);
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        #endregion Plan Overview

        #region Job Assignment Tab 
        //GET: api/preservation/{projectDb}/assignment/frequency
        [HttpGet]
        [Route("{projectDb}/assignment/frequency")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public IHttpActionResult GetPreservationFrequency(string projectDb, string code)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                               .ValidateString(code, LocalResources.Code)
                               .ValidateProject(User.Identity, projectDb, "db", "get")
                               .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var frequency = _preservationService.GetFrequencyForPreservation(code);
                return new Envelope(Request).SetData(frequency).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //DELETE: api/preservation/data/{projectDb}/assignment/tags
        [HttpDelete]
        [Route("data/{projectDb}/assignment/tags")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWritePreservationPlanning)]
        public IHttpActionResult DeleteJobPlanAssignment(string projectDb, string datatable, string tag)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                               .ValidateString(datatable, LocalResources.Datatable)
                               .ValidateString(tag, LocalResources.Tag)
                               .ValidateProject(User.Identity, projectDb, "db", "get")
                               .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                _preservationService.DeleteTags(datatable, tag);
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //PUT: api/preservation/data/{projectDb}/tables/{dataTable}/typecodes/rowCode=xxx
        [HttpPut]
        [Route("data/{projectDb}/tables/{dataTable}/typecodes/{rowCode}")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWritePreservationPlanning)]
        public IHttpActionResult SaveJobAssignmentData(string projectDb, string dataTable, string rowCode, [FromBody]PreservationAssignment preservation)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                   .ValidateString(dataTable, LocalResources.Datatable)
                                   .ValidateString(rowCode, LocalResources.RowId)
                                   .ValidateObject(preservation, LocalResources.Preservation)
                                   .ValidateProject(User.Identity, projectDb, "db", "get")
                                   .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                _preservationService.SaveChangesToTags(dataTable, preservation, rowCode);
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }

        }
        #endregion Job Assignment Tab 

        #region Preservation Follow Up
        //GET: api/preservation/{projectDb}/followup/actvities
        [HttpGet]
        [Route("{projectDb}/followup/activities")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public IHttpActionResult GetPreservationActivitiesStatus(string projectDb, string datatable, string tag)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                   .ValidateString(datatable, LocalResources.Datatable)
                                   .ValidateString(tag, LocalResources.Tag)
                                   .ValidateProject(User.Identity, projectDb, "db", "get")
                                   .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var activities = _preservationService.GetScheduledWeeksForPreservation(datatable, tag);
                return new Envelope(Request).SetData(activities).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: api/preservation/{projectDb}/followup/activities/timeline
        [HttpGet]
        [Route("{projectDb}/followup/activities/timeline")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public IHttpActionResult GetPreservationTimelineSchedule(string projectDb, string datatable, string tag)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                     .ValidateString(datatable, LocalResources.Datatable)
                     .ValidateString(tag, LocalResources.Tag)
                     .ValidateProject(User.Identity, projectDb, "db", "get")
                     .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                string schedule = _preservationService.GetYearWeek(DateTime.Now);
                var timeline = _preservationService.GetCutOffAndSchedule(datatable, schedule, tag);
                return new Envelope(Request).SetData(timeline).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //PUT: api/preservation/{projectDb}/followup/doneweek
        [HttpPut]
        [Route("{projectDb}/{dataTable}/followup/doneweek")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWritePreservationPlanning)]
        public IHttpActionResult SaveFollowUpData(string projectDb, string dataTable, IEnumerable<DataKeeper> preservationList, string tag, string code, string schedw)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                         .ValidateString(dataTable, LocalResources.Datatable)
                         .ValidateArray(preservationList, LocalResources.PreservationList)
                         .ValidateString(tag, LocalResources.Tag)
                         .ValidateString(code, LocalResources.Code)
                         .ValidateString(schedw, LocalResources.ScheduledWeek)
                         .ValidateProject(User.Identity, projectDb, "db", "get")
                         .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();


            tag = HttpUtility.UrlDecode(tag);
            code = HttpUtility.UrlDecode(code);
            schedw = HttpUtility.UrlDecode(schedw);
            try
            {
                _preservationService.SaveActivtiesStatusChanges(dataTable, preservationList.ToList(), tag, code, schedw);
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        #endregion Preservation Follow Up    
    }
}