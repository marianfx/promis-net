﻿using Promis.NET.DataAccess.Repositories;
using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.Roles;
using Promis.NET.Services.Models.Activities;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using System;
using System.Globalization;
using System.Threading;
using System.Web.Http;
using Promis.NET.Localization;
using System.Collections.Generic;

namespace Promis.NET.MCV.Controllers.ApiControllers
{
    [Authorize]
    [RoutePrefix("api/activities")]
    public class ActivitiesApiController : ApiController
    {
        private IActivitiesService _activitiesService;
        private ControllerValidator _validator = new ControllerValidator();

        public ActivitiesApiController(IActivitiesService activitiesService)
        {
            _activitiesService = activitiesService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _activitiesService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        #region Hydrotest Packages ISO Assignment
        //PUT: api/activities/data/{projectDb}/hydrotest/package
        [HttpPut]
        [Route("data/{projectDb}/hydrotest/package")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWriteSiteActivities)]
        public IHttpActionResult InsertHydrotestPackage(string projectDb, string tag, string description)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                        .ValidateString(tag, LocalResources.Tag)
                        .ValidateString(description, LocalResources.Description)
                        .ValidateProject(User.Identity, projectDb, "db", "get")
                        .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var insertResult = _activitiesService.GetRepository<ActivitiesRepository>().InsertHydrotestPack(tag, description);
                return new Envelope(Request).SetData(insertResult).GetResponse();

            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //DELETE: api/activities/data/{projectDb}/testpacks/hydrotest
        [HttpDelete]
        [Route("data/{projectDb}/testpacks/hydrotest")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWriteSiteActivities)]
        public IHttpActionResult DeleteTestpackItem(string projectDb, string rowId, string tag)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                       .ValidateString(rowId,LocalResources.RowId)
                       .ValidateString(tag, LocalResources.Tag)
                       .ValidateProject(User.Identity, projectDb, "db", "get")
                       .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                _activitiesService.GetRepository<ActivitiesRepository>().DeleteHydrotest(rowId, tag);
                return new Envelope(Request).SetOk().GetResponse();

            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: api/activities/data/{projectDb}/iso/pipingClass
        [HttpGet]
        [Route("data/{projectDb}/iso/pipingClass")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public IHttpActionResult GetPipingClasses (string projectDb, string pds)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                       .ValidateString(pds, LocalResources.Pds)
                       .ValidateProject(User.Identity, projectDb, "db", "get")
                       .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var pipingClases = _activitiesService.GetPipingList("pipingClass", pds);
                return new Envelope(Request).SetData(pipingClases).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: api/activities/data/{projectDb}/iso/systems
        [HttpGet]
        [Route("data/{projectDb}/iso/systems")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public IHttpActionResult GetSystems(string projectDb, string pipingClass)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                       .ValidateString(pipingClass, LocalResources.PipingClass)
                       .ValidateProject(User.Identity, projectDb, "db", "get")
                       .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var systems = _activitiesService.GetIsometricsValues("system", pipingClass);
                return new Envelope(Request).SetData(systems).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: api/activities/data/{projectDb}/iso/subsystems
        [HttpGet]
        [Route("data/{projectDb}/iso/subsystems")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public IHttpActionResult GetSubSystems(string projectDb, string system)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                       .ValidateString(system, LocalResources.System)
                       .ValidateProject(User.Identity, projectDb, "db", "get")
                       .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var subSystems = _activitiesService.GetIsometricsValues("subsystem", system);
                return new Envelope(Request).SetData(subSystems).GetResponse();
               
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //PUT: api/activities/data/{projectDb}/isometrics/assignment
        [HttpPut]
        [Route("data/{projectDb}/isometrics/assignment")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWriteSiteActivities)]
        public IHttpActionResult AssignIsometricsToTestpackage(string projectDb, [FromBody]Isometrics isometricData)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                .ValidateObject(isometricData, LocalResources.IsometricData)
                                .ValidateProject(User.Identity, projectDb, "db", "get")
                                .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var isInserted = _activitiesService.AssignIso(isometricData);
                return new Envelope(Request).SetData(isInserted).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        #endregion

        #region System Punch Lists Management
        //GET: api/activities/data/{projectDb}/system/{plIncluded}"
        [HttpGet]
        [Route("data/{projectDb}/system/{plIncluded}")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public IHttpActionResult GetSystems(string projectDb, bool plIncluded)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                .ValidateProject(User.Identity, projectDb, "db", "get")
                                .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var result = _activitiesService.GetAllSystemFromPunchlist(plIncluded);
                return new Envelope(Request).SetData(result).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        
        //GET /api/activities/data/{projectDb}/closure
        [Route("data/{projectDb}/closure")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public IHttpActionResult GetClosure(string projectDb, string rowId)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                .ValidateString(rowId, LocalResources.RowId)
                                .ValidateProject(User.Identity, projectDb, "db", "get")
                                .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var result = _activitiesService.GetClosureByRowId(rowId);
                return new Envelope(Request).SetData(result).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //PUT /api/activities/data/{projectDb}/punchlist/entry
        [HttpPut]
        [Route("data/{projectDb}/punchlist/entry")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWriteSiteActivities)]
        public IHttpActionResult UpdateActivityForSelectedRows(string projectDb, Punchlist commData,bool isInsert,string rowId)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                .ValidateString(rowId, LocalResources.RowId)
                                .ValidateObject(commData, LocalResources.RequiredData)
                                .ValidateProject(User.Identity, projectDb, "db", "get")
                                .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var messageList = _activitiesService.UpdateOrInsertPunchlistRow(commData, isInsert, rowId);
                return new Envelope(Request).SetData(messageList).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        #endregion
    }
}