﻿using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using Promis.NET.Localization;
using Promis.NET.Utilities.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using Promis.NET.Roles;

namespace Promis.NET.MCV.Controllers.ApiControllers
{
    [Authorize]
    [RoutePrefix("api/project")]
    public class ProjectApiController : ApiController
    {
        private IProjectService _projectService;
        private ControllerValidator _validator = new ControllerValidator();

        public ProjectApiController(IProjectService projectService)
        {
            _projectService = projectService;

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        //PUT: /api/project/{projectId}?type=xxx
        /// <summary>
        /// Update the project details.
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="type"> Can be 'general' or 'logo'.</param>
        /// <param name="project"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{projectId}")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin)]
        public IHttpActionResult UpdateProject(string projectId, string type, Project project)
        {
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(type, new[] { "general", "logo" }, valueName: LocalResources.Type)
                                    .ValidateObject(project, LocalResources.Project + " " + LocalResources.Data)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();
            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                project.Code = projectId;//id is actually the code
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                _projectService.UpdateProjectDetails(projectDatabase, project, type);

                this.NotifyControllerSync("/project/" + projectId + "/refresh");
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }


        //PUT: /api/project/data/{projectDb}/activities
        [HttpPut]
        [Route("data/{projectDb}/activities")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin)]
        public IHttpActionResult UpdateProjectActivities(string projectDb, IEnumerable<DataKeeper> activitiesData)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateArray(activitiesData, LocalResources.Activities)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .GetErrors();
            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();
            
            try
            {
                _projectService.UpdateMainActivities(projectDb, activitiesData);
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: /api/project/data/{projectDb}/activities/{activity]/related[?filterEmpty=false]
        [HttpGet]
        [Route("data/{projectDb}/activities/{activity}/related")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public IHttpActionResult GetActivityRelatedActivities(string projectDb, string activity, bool filterEmpty = false)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateString(activity, LocalResources.MainActivity)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .GetErrors();
            if(errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            activity = HttpUtility.UrlDecode(activity);
            try
            {
                var projectId = User.Identity.GetProjectId();
                var activitiesRelated = _projectService.GetAllAssociatedActivities(projectDb, activity, filterEmpty);
                var codeAndDesc = activitiesRelated.Select(a => new { a.Code, a.Description }).ToList();
                return new Envelope(Request).SetData(codeAndDesc).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: /api/project/data/{projectDb}/activities/{activity]/sub[?filterEmpty=false&mainActivity=xxx]
        [HttpGet]
        [Route("data/{projectDb}/activities/{activity}/sub")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public IHttpActionResult GetActivitySubActivities(string projectDb, string activity, bool filterEmpty = false, string mainActivity = null)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateString(activity, LocalResources.Activity)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .GetErrors();
            if (filterEmpty && string.IsNullOrWhiteSpace(mainActivity))//special case
                errors.Add(LocalResources.WhenFilteringTheEmptySubactivitiesMainActivityMustBeSpecified);

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            mainActivity = HttpUtility.UrlDecode(mainActivity);
            activity = HttpUtility.UrlDecode(activity);
            try
            {
                var activitiesSub = _projectService.GetAllSubActivities(projectDb, activity, filterEmpty, mainActivity);
                var codeAndDesc = activitiesSub.Select(a => new { a.Code, a.Description }).ToList();
                return new Envelope(Request).SetData(codeAndDesc).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //PUT: /api/project/data/{projectDb}/activities/{activity]/related[?isRescheduled=false]
        [HttpPut]
        [Route("data/{projectDb}/activities/{activity}/related")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWritePlanningProgress)]
        public IHttpActionResult UpdateActivityRelatedActivities(string projectDb, string activity, [FromBody]IEnumerable<ActivityPlanned> relatedActivities, bool isRescheduled = false)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                   .ValidateString(activity, LocalResources.Activity)
                                   .ValidateArray(relatedActivities, LocalResources.Activities)
                                   .ValidateProject(User.Identity, projectDb, "db", "get")
                                   .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var projectId = User.Identity.GetProjectId();
                var typeOfUpdpate = isRescheduled ? ActivityUpdateType.RescheduledCurves : ActivityUpdateType.BaselineAndPlanning;
                _projectService.UpdateAllAssociatedActivities(projectId, projectDb, activity, relatedActivities.ToList(), typeOfUpdpate);
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: /api/project/data/{projectDb}/subactivities/{activity]/um
        [HttpGet]
        [Route("data/{projectDb}/subactivities/{activity}/um")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public IHttpActionResult GetSubactivityUnitMeasure(string projectDb, string activity)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateString(activity, LocalResources.SubActivity)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            activity = HttpUtility.UrlDecode(activity);
            try
            {
                var unitMeasure = _projectService.GetUnitMeasureForSubActivity(projectDb, activity);
                return new Envelope(Request).SetData(unitMeasure).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }


        //POST: /api/project/data/{projectDb}/cutoff
        [HttpPost]
        [Route("data/{projectDb}/cutoff")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin)]
        public IHttpActionResult InsertCutoffDay(string projectDb, [FromBody]string date)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                 .ValidateDate(date, out DateTime realDate, LocalResources.Date)
                                 .ValidateProject(User.Identity, projectDb, "db", "get")
                                 .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                _projectService.InsertCutoffDay(projectDb, realDate);
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //DELETE: /api/project/data/{projectDb}/cutoff
        [HttpDelete]
        [Route("data/{projectDb}/cutoff")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin)]
        public IHttpActionResult DeleteCutoffDay(string projectDb, [FromBody]string date)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                .ValidateDate(date, out DateTime realDate, LocalResources.Date)
                                .ValidateProject(User.Identity, projectDb, "db", "get")
                                .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                _projectService.DeleteCutoffDay(projectDb, realDate);
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }


        //GET: /api/project/curves
        // Mostly redundant, the array can be computed in JavaScript with the formula.
        [HttpGet]
        [Route("curves")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin)]
        public IHttpActionResult GetCurves(int months)
        {
            if (months == 0)
                return new Envelope(Request).AddError(LocalResources.NumberOfMonthsCannotBe).GetResponse();

            try
            {
                var curves = _projectService.GetTagCurvesForMonths(months);
                return new Envelope(Request).SetData(curves).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
    }
}