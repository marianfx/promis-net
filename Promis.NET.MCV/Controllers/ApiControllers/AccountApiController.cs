﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Promis.NET.Services.Models.Account;
using Promis.NET.DataAccess.Entities;
using Promis.NET.Utilities.Exceptions;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.MCV.ViewModels.Account;
using Promis.NET.Utilities.Models;
using System.Web;
using System.Configuration;
using Microsoft.AspNet.Identity.Owin;
using System.Threading;
using System.Globalization;
using Promis.NET.Roles;
using System.Security.Permissions;
using Promis.NET.Localization;

namespace Promis.NET.MCV.Controllers.ApiControllers
{
    [Authorize]
    [RoutePrefix("api/account")]
    public class AccountApiController : ApiController
    {
        #region Managers and Constructor
        protected ApplicationUserManager _userManager;
        private IAccountService _accountService;
        private IProjectService _projectService;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public AccountApiController(IAccountService accountService, IProjectService projectService)
        {
            _accountService = accountService;
            _projectService = projectService;

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }
        #endregion

        #region Authentication
        // POST: api/account/basic
        // the basic version of the registering, that allows only adding users with password and email to database (normal users). Giving them roles and others requires admin privileges.
        [HttpPost]
        [AllowAnonymous]
        [Route("basic")]
        public IHttpActionResult RegisterBasic(PromisUser form)
        {
            if (form == null)
                return new Envelope(Request).AddError(LocalResources.InvalidInput).GetResponse();

            var formData = new PromisUserViewModel(form);
            var errors = formData.GetValidationErrors();
            if (errors != null && errors.Count() != 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();
            
            try
            {
                var user = new ApplicationUser
                {
                    UserName = form.UserName,
                    Email = form.Email,
                    ProjectId = form.ProjectId
                };

                var result = UserManager.Create(user, form.Password);
                if (!result.Succeeded)
                    return new Envelope(Request).AddErrors(result.Errors).GetResponse();

                user.Password = "";
                return new Envelope(Request).SetData(user).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //POST: api/account
        [HttpPost]
        [Route("")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin)]
        public IHttpActionResult Register(PromisUser form)
        {
            if (form == null)
                return new Envelope(Request).AddError(LocalResources.InvalidInput).GetResponse();

            var formData = new PromisUserViewModel(form);
            var errors = formData.GetValidationErrors();
            if (errors != null && errors.Count() != 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            var userIsAdmin = User.IsInRole("R_A");
            if (!userIsAdmin)
                return new Envelope(Request).AddError(LocalResources.OnlyAdminsCanAddUsers).GetResponse();

            try
            {
                var user = new ApplicationUser
                {
                    UserName = form.UserName,
                    Email = form.Email,
                    Company = form.Company,
                    Country = form.Country,
                    FirstName = form.FirstName,
                    HasDoc = form.HasDoc,
                    Language = form.Language,
                    LastName = form.LastName,
                    MobilePhone = form.MobilePhone,
                    OfficePhone = form.OfficePhone,
                    Password = form.Password,
                    ProjectId = form.ProjectId,
                    PhotoUrl = form.PhotoUrl,
                    RolesActivitiesString = form.RolesActivitiesString,
                    RolesModulesString = form.RolesModulesString,
                    RolesResponsabilitiesString = form.RolesResponsabilitiesString
                };

                var result = UserManager.Create(user, form.Password);
                if (!result.Succeeded)
                    return new Envelope(Request).AddErrors(result.Errors).GetResponse();

                user.Password = "";
                return new Envelope(Request).SetData(user).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        

        // POST: api/account/login/pre
        [HttpPost]
        [AllowAnonymous]
        [Route("login/pre")]
        public async Task<IHttpActionResult> PreLogin(LoginDataViewModel formData)
        {
            if (formData == null)
                return new Envelope(Request).AddError(LocalResources.InvalidInput).GetResponse();

            var errors = formData.GetValidationErrors();
            if (errors != null && errors.Count() != 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var projectsForUser = _projectService.GetAllProjectsDataForUsername(formData.Username);
                if (projectsForUser == null || projectsForUser.Count == 0)
                    return new Envelope(Request).AddError(LocalResources.UserNotAssociatedToAnyProject).GetResponse();

                // if user associated to 1 project only, do the login on the project
                if (projectsForUser.Count == 1)
                    return await Login(new LoginDataWithProjectViewModel(formData, projectsForUser[0].Code));

                // preventive password check if user with this password exists on any project
                var user = await UserManager.MatchAnyByUsernameAndPassword(formData.Username, formData.Password);
                if (user == null)
                    return new Envelope(Request).SetNext("/account/login").AddError(LocalResources.InvalidLoginAttemptUsernamepasswordDontMatch).GetResponse();

                // otherwise return response to indicate that it must display projects
                return new Envelope(Request).SetData(new { loadProjects = true, username = formData.Username }).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        
        // POST: api/account/login
        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public async Task<IHttpActionResult> Login(LoginDataWithProjectViewModel formData)
        {
            if (formData == null)
                return new Envelope(Request).AddError(LocalResources.InvalidInput).GetResponse();

            formData.RememberMe = true;
            var next = !string.IsNullOrWhiteSpace(formData.ReturnUrl) ? formData.ReturnUrl : "/";
            next = next.StartsWith("/") ? next : "/" + next;

            var errors = formData.GetValidationErrors();
            if (errors != null && errors.Count() != 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var user = await UserManager.MatchAsync(formData.Username, formData.ProjectId, formData.Password);
                if (user == null)
                    return new Envelope(Request).SetNext("/account/login").AddError(LocalResources.InvalidLoginAttemptUsernamepasswordprojectDontMatch).GetResponse();

                await SignInAsync(user, formData.RememberMe);
                user.Password = "";
                return new Envelope(Request).SetData(user).SetNext(next).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }

        }


        //POST: api/account/logout
        [HttpPost]
        [Route("logout")]
        public IHttpActionResult LogOut()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return new Envelope(Request).SetNext("/account/login").SetOk().GetResponse();
        }
        #endregion

        //GET: api/account/details[?userId=xxx]; If userId not specified, returns current user.
        [HttpGet]
        [Route("details")]
        public IHttpActionResult GetUser(string userId = null)
        {
            var username = User.Identity.Name;
            var project = User.Identity.GetProjectId();

            if (!string.IsNullOrWhiteSpace(userId) && !User.IsInRole(LocalRoles.ResponsabilityAdmin))
                throw new UnauthorizedAccessException(LocalResources.OnlyAdminsCanAccessOtherUsersData);
                
            try
            {
                var result = !string.IsNullOrWhiteSpace(userId) ? _accountService.GetUserDetailsById(userId) : _accountService.GetUserDetails(username, project);
                return new Envelope(Request).SetData(result).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //PUT: api/account/{userId}[?type=xxx]
        [HttpPut]
        [Route("{userId}")]
        public IHttpActionResult UpdateUser(string userId, PromisUser userData, string type = "basic")
        {
            if(string.IsNullOrWhiteSpace(userId))
                return new Envelope(Request).AddError(LocalResources.UserIdMustBeSpecified).GetResponse();
            if (userData == null)
                return new Envelope(Request).AddError(LocalResources.UserDateMustBeSpecified).GetResponse();
            
            var formData = new PromisUserViewModel(userData);
            var errors = formData.GetValidationErrorsOnUpdate();
            if (errors != null && errors.Count() != 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            var userIsAdmin = User.IsInRole("R_A");
            if (!(userIsAdmin || userId == User.Identity.GetUserId()))
                return new Envelope(Request).AddError(LocalResources.YouCanEditOtherUsersDetailsOnlyIfYouAreAnAdminOtherwiseYouCanOnlyEditYourInfo).GetResponse();

            try
            {
                userData.UserId = long.Parse(userId);
                _accountService.UpdateUserDetails(userData, (userIsAdmin && type != "basic"));//will update the roles only if admin and only if specified to do so

                // if success, update claim language (only if editing my info)
                if (userId == User.Identity.GetUserId())
                    User.SetUserLanguage(userData.Language);

                // finished, notify others
                this.NotifyControllerSync("/account/data/" + userId + "/refresh");
                this.NotifyControllerSync("/engineering/menu/" + User.Identity.GetProjectDatabaseName() + "/refresh");

                var config = ConfigurationManager.AppSettings;
                var updateMessage = userIsAdmin ?
                    LocalResources.AllDataUpdatedSuccessfullyRolesChangeWillPropagateInAMaximumOf + config["RolesInvalidateIntervalInMinutes"] + LocalResources.MinutesOrAtLogout:
                    LocalResources.DataUpdatedSuccessfullySomeFieldsWereNotUpdatedBecauseYouAreNotAnAdminUsernameAndRoles;
                return new Envelope(Request).SetData(updateMessage).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //DELETE: api/account/{userId} 
        [HttpDelete]
        [Route("{userId}")]
        public IHttpActionResult DeleteUser(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                return new Envelope(Request).AddError(LocalResources.UserIdMustBeSpecified).GetResponse();

            var userIsAdmin = User.IsInRole("R_A");
            if (!userIsAdmin)
                return new Envelope(Request).AddError(LocalResources.OnlyAdminsCanDeleteUsers).GetResponse();

            try
            {
                _accountService.DeleteUserById(userId);
                var message = LocalResources.UserSuccessfullyDeleted;
                var deletedSelf = false;

                //if this user deletes himself, log him out
                if (userId == User.Identity.GetUserId())
                {
                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                    message = LocalResources.YouHaveDeletedYourOwnAccountYouWillNowBeRedirectedToLoginPage;
                    deletedSelf = true;
                }

                return new Envelope(Request).SetData(new {
                                                             deletedSelf, 
                                                             message
                                                        }).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: api/account/theme
        [HttpGet]
        [Route("theme")]
        public IHttpActionResult GetTheme()
        {
            try
            {
                var theme = User.Identity.GetUserTheme() ?? Utilities.Models.Global.DefaultTheme;
                return new Envelope(Request).SetData(theme).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //PUT: api/account/theme
        [HttpPut]
        [Route("theme")]
        public IHttpActionResult PutTheme([FromBody] string theme)
        {
            if (string.IsNullOrWhiteSpace(theme))
                return new Envelope(Request).AddError(LocalResources.ThemeMustBeSpecified).GetResponse();

            try
            {
                User.SetUserTheme(theme);
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return Request.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await user.CreateAuthenticationCookie(UserManager);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (UserManager != null)
                {
                    UserManager.Dispose();
                    UserManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #endregion
    }
}
