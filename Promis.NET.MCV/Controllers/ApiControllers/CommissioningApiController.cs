﻿using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using System;
using System.Threading;
using System.Web.Http;
using Promis.NET.Services.Models.Commissioning;
using Promis.NET.MCV.ViewModels.Commissioning;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Globalization;
using Promis.NET.Localization;
using Promis.NET.Roles;

namespace Promis.NET.MCV.Controllers.ApiControllers
{

    [Authorize]
    [RoutePrefix("api/commissioning")]
    public class CommissioningApiController : ApiController
    {
        private ICommissioningService _commissioningService;
        private IAccountService _accountService;
        private ControllerValidator _validator = new ControllerValidator();

        public CommissioningApiController(ICommissioningService commissioningService, IAccountService accountService)
        {
            _commissioningService = commissioningService;
            _accountService = accountService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _commissioningService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        #region Status
        //GET: api/commissioning/data/{projectDb}/charts
        [HttpGet]
        [Route("data/{projectDb}/charts")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public IHttpActionResult GetProgressStatusData(string projectDb, string sufix)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                .ValidateString(sufix, LocalResources.TheSufix)
                                .ValidateProject(User.Identity, projectDb, "db", "get")
                                .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var allData = new ChartsData();
                _commissioningService.GetChartsProgressStatusData(ref allData, sufix);
                return new Envelope(Request).SetData(new
                {
                    ProgressChartLabels = allData.ProgressStatusChartLabels,
                    ProgressChartValuesPlanned = allData.ProgressStatusChartValuesPlanned,
                    ProgressChartValuesActual = allData.ProgressStatusChartValuesActual

                })
                .GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        #endregion Status

        //GET: api/commissioning/data/{projectDb}/datatable/{datatable}/type
        [HttpGet]
        [Route("data/{projectDb}/datatable/{datatable}/type")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public IHttpActionResult GetTypecodesForDatatable(string projectDb, string dataTable, bool isWithJoin)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                               .ValidateString(dataTable, LocalResources.Datatable)
                               .ValidateProject(User.Identity, projectDb, "db", "get")
                               .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var typecodes = _commissioningService.GetTypecodesForDatatables(dataTable, isWithJoin);
                return new Envelope(Request).SetData(typecodes).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: api/commissioning/data/{projectDb}/datatable/{datatable}/associatedTags
        [HttpGet]
        [Route("data/{projectDb}/datatable/associatedTags")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public IHttpActionResult GetTagsCountForDatatable(string projectDb, string dataTable = null)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)                               
                               .ValidateProject(User.Identity, projectDb, "db", "get")
                               .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var viewModel = new SystemSubdivisionViewModel();
                var counts = _commissioningService.GetTotalCountsAndPercentage(dataTable);
                viewModel.PrPc = counts.Item1;
                viewModel.Tottags = counts.Item2;
                viewModel.TotAss = counts.Item3;
                return new Envelope(Request).SetData(viewModel).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //GET: api/commissioning/data/{projectDb}/datatable/{dataTable}/activity/{mainActivity}/sub
        [HttpGet]
        [Route("data/{projectDb}/datatable/{dataTable}/activity/{mainActivity}/sub")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public IHttpActionResult GetSubactForDatatable(string projectDb, string dataTable, string mainActivity)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                        .ValidateString(dataTable, LocalResources.Datatable)
                        .ValidateString(mainActivity, LocalResources.MainActivity)
                        .ValidateProject(User.Identity, projectDb, "db", "get")
                        .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var subactivities = _commissioningService.GetSubactForDatatables(dataTable, mainActivity);
                return new Envelope(Request).SetData(subactivities).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //PUT: api/commissioning/data/{projectDb}/datatable/rows/activity
        [HttpPut]
        [Route("data/{projectDb}/datatable/rows/activity")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWritePrecommInspections, LocalRoles.ModuleWriteCertificationManagement)]
        public IHttpActionResult UpdateActivityForSelectedRows(string projectDb, CommissioningInspectionTagsProgress commData)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                       .ValidateObject(commData, LocalResources.CommissioningData)
                       .ValidateProject(User.Identity, projectDb, "db", "get")
                       .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                string[] mainActArray = { "CO", "PR" };
                if (mainActArray.Contains(commData.MainActivity))
                    commData.MainActivity = commData.MainActivity + "_";
                else
                    commData.MainActivity = "";
                var messageList = _commissioningService.UpdateTagAndActivityforDatatables(commData);
                return new Envelope(Request).SetData(messageList).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //DELETE: api/commissioning/data/{projectDb}/datatable/rows/activity
        [HttpDelete]
        [Route("data/{projectDb}/datatable/rows/activity")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWritePrecommInspections, LocalRoles.ModuleWriteCertificationManagement)]
        public IHttpActionResult DeleteActivityForSelectedRows(string projectDb, CommissioningInspectionTagsProgress commData)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                       .ValidateObject(commData, LocalResources.CommissioningData)
                       .ValidateProject(User.Identity, projectDb, "db", "get")
                       .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                string[] mainActArray = { "CO", "PR" };
                if (mainActArray.Contains(commData.MainActivity))
                    commData.MainActivity = commData.MainActivity + "_";
                else
                    commData.MainActivity = "";
                _commissioningService.UpdateTagAndDeleteActivityforDatatables(commData);
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }

        //PUT: api/commissioning/data/{projectDb}/datatable/rows/system
        [HttpPut]
        [Route("data/{projectDb}/datatable/rows/system")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWritePrecommInspections)]
        public IHttpActionResult UpdateOrDeleteSysForSelectedRows(string projectDb, CommissioningSysProgress commData, string dataTable)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                       .ValidateObject(commData, LocalResources.CommissioningData)
                       .ValidateProject(User.Identity, projectDb, "db", "get")
                       .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var viewModel = new SystemSubdivisionViewModel();
                _commissioningService.UpdateOrDeleteSysforDatatables(commData);
                var counts = _commissioningService.GetTotalCountsAndPercentage(dataTable);
                viewModel.PrPc = counts.Item1;
                viewModel.Tottags = counts.Item2;
                viewModel.TotAss = counts.Item3;
                return new Envelope(Request).SetData(viewModel).GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
        
        //PUT: api/commissioning/{projectDb}/inspections/{tag}
        [HttpPut]
        [Route("{projectDb}/inspections/{tag}")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleWritePrecommInspections)]
        public IHttpActionResult UpdateTableinspectionByTagAndOrder(string projectDb, string tag, string mainActivity, string dataTable, IEnumerable<KeyValuePair<string, string>> steps)
        {
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                          .ValidateObject(steps, LocalResources.StepsData)
                          .ValidateString(dataTable, LocalResources.Datatable)
                          .ValidateString(mainActivity, LocalResources.MainActivity)
                          .ValidateString(tag, LocalResources.Tag)
                          .ValidateProject(User.Identity, projectDb, "db", "get")
                          .GetErrors();

            if (errors.Count > 0)
                return new Envelope(Request).AddErrors(errors).GetResponse();

            try
            {
                var prefix = "precomm_";
                var userId = User.Identity.GetUserId();
                var user = _accountService.GetUserDetailsById(userId);
                if (mainActivity.Equals("CO")) { prefix = "comm_"; }
                foreach (var item in steps)
                {
                    _commissioningService.UpdateinspectionforDatatables(prefix+dataTable, (item.Key.Equals("N"))?user.FullName:"", tag, item.Key, item.Value);

                }
                return new Envelope(Request).SetOk().GetResponse();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                return new Envelope(Request).SetErrorObject(pe.ErrorDetailsUserLevel).GetResponse();
            }
        }
    }
}
