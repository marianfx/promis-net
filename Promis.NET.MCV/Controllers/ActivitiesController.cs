﻿using Promis.NET.DataAccess.Repositories;
using Promis.NET.Localization;
using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.MCV.ViewModels.Activities;
using Promis.NET.MCV.ViewModels.Engineering;
using Promis.NET.Roles;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;

namespace Promis.NET.MCV.Controllers
{
    [Authorize]
    [RoutePrefix("activities")]
    public class ActivitiesController : Controller
    {
        private IActivitiesService _activitiesService;
        private ControllerValidator _validator = new ControllerValidator();

        public ActivitiesController(IActivitiesService activitiesService)
        {
            _activitiesService = activitiesService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _activitiesService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        //GET: activities/ (the module layout)
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public ActionResult Index()
        {
            var viewModel = new ModuleLayoutViewModel()
            {
                ModuleTitle = LocalResources.SiteActivities,
                ModuleVersion = "F007.V1.0.0.0",
                ContentContainerId = "activities-content",
                WorkingPath = "/activities",
                PageDescriptorsContainer = new Dictionary<string, string>()
                {
                    { "home", LocalResources.HydrotestPackagesStatus },
                    { "iso", LocalResources.HydrotestPackagesIsoAssignment },
                    { "punchlist", LocalResources.SystemPunchListManagement }
                },
                StyleSheetsList = new List<string>() { "/pages/activities.css"}
            };

            return View("_ModuleLayout", viewModel);
        }

        //GET: activities/{projectId}/partial
        [Route("{projectId}/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public ActionResult IndexPartial(string projectId)
        {
            var viewModel = new HomeViewModel();
            var htmlFileName = "_Index";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Inspections = _activitiesService.GetAllTestPackages();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        #region Hydrotest Packages ISO Assignment
        //GET: activities/{projectId}/iso/partial
        [Route("{projectId}/iso/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public ActionResult IsoPartial(string projectId)
        {
            var viewModel = new ActivitiesISOViewModel();
            var htmlFileName = "_Iso";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }
            
            try
            {
                viewModel.HydroPackagesCount = _activitiesService.GetRepository<ActivitiesRepository>().GetSketchesAndTestPackNumber();
                viewModel.PipingDesignList = _activitiesService.GetPipingList("pipingDesign", "");
                viewModel.GeographicalWBS = _activitiesService.GetIsometricsValues("geographical","");
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: activities/{projectId}/iso/testpack-table/partial
        [Route("{projectId}/iso/testpack-table/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public ActionResult GetTestPackTableData(string projectId)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var tableName = _activitiesService.GetRepository<ActivitiesRepository>().GetTableNameTestpackages();
                viewModel.Base = new Services.Models.Engineering.GridData();
                viewModel.Base.Columns = _activitiesService.GetRepository<ActivitiesRepository>().GetColumnsNecessaryForTestPackTable();
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = tableName;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: activities/{projectId}/iso/isometrics/partial
        [Route("{projectId}/iso/isometrics/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public ActionResult GetIsometricsTableData(string projectId)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var tableName = _activitiesService.GetRepository<ActivitiesRepository>().GetTableNameForIsometrics();
                viewModel.Base = new Services.Models.Engineering.GridData();
                viewModel.Base.Columns = _activitiesService.GetRepository<ActivitiesRepository>().GetColumnsNecessaryForIsometrics();
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = tableName;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: activities/{projectId}/iso/isometrics/assigned/partial
        [Route("{projectId}/iso/isometrics/assigned/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public ActionResult GetAlreadyAssignedIsometrics(string projectId, string theSelectedTag)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(theSelectedTag,LocalResources.Tag)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var tableName = _activitiesService.GetRepository<ActivitiesRepository>().GetTableNameForIsometrics();
                viewModel.Base = new Services.Models.Engineering.GridData();
                viewModel.Base.Columns = _activitiesService.GetRepository<ActivitiesRepository>().GetColumnsNecessaryForIsometrics();
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = tableName;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
        #endregion

        #region System Punch Lists Management
        //GET: activities/{projectId}/punchlist/partial
        [Route("{projectId}/punchlist/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public ActionResult PunchlistPartial(string projectId)
        {
            var viewModel = new SystemPunchListsManagementViewModel();
            var htmlFileName = "_Punchlist";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.ActionBy = _activitiesService.GetAllActionsBy();
                viewModel.Category = _activitiesService.GetAllCategory();
                viewModel.Closer = _activitiesService.GetAllCloser();
                viewModel.Discipline = _activitiesService.GetAllDisciplines();
                viewModel.Originator = _activitiesService.GetAllOriginators();
                viewModel.Status = _activitiesService.GetAllStatus();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: activities/{projectId}/system/table
        [Route("{projectId}/system/table")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public ActionResult GetPunchlistTableData(string projectId,string systemCode)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Base = new Services.Models.Engineering.GridData();
                viewModel.Base.Columns = _activitiesService.GetColumnsNecessaryForPunchlistTable();
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = "punchlist";
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
        //GET: activities/{projectId}/system/modal/table
        [Route("{projectId}/system/modal/table")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public ActionResult GetPunchlistModalTableData(string projectId, string tableName)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .ValidateString(tableName, "System Code")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Base = new Services.Models.Engineering.GridData();
                viewModel.Base.Columns = _activitiesService.GetColumnsNecessaryForPunchlistModalTable();
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = tableName;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
        //GET activities/{projectId}/punchlist/module
        [Route("{projectId}/punchlist/module")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadSiteActivities)]
        public ActionResult GetDatatablesForPunchlistModule(string projectId,string system)
        {
            var viewModel = new OutstandingModalViewModel();
            var htmlFileName = "~/Views/Activities/_OutstandingModal.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                .ValidateProject(User.Identity, projectId, "id", "get")
                                .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }
            try
            {
                 viewModel.Datatables = _activitiesService.GetDataTablesForModal(system);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }
            return PartialView(htmlFileName, viewModel);
        }
        #endregion
    }
}