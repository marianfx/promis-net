﻿using Promis.NET.Localization;
using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.MCV.ViewModels.Engineering;
using Promis.NET.MCV.ViewModels.Preservation;
using Promis.NET.Roles;
using Promis.NET.Services.Models.Engineering;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;

namespace Promis.NET.MCV.Controllers
{
    [Authorize]
    [RoutePrefix("preservation")]
    public class PreservationController : Controller
    {
        private IPreservationService _preservationService;
        private IEngineeringService _engineeringService;
        private ControllerValidator _validator = new ControllerValidator();

        public PreservationController(IPreservationService preservationService, IEngineeringService engineeringService)
        {
            _preservationService = preservationService;
            _engineeringService = engineeringService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _preservationService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _engineeringService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        //GET: preservation/
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public ActionResult Index()
        {
            var viewModel = new ModuleLayoutViewModel()
            {
                ModuleTitle = LocalResources.MaterialPreservation,
                ModuleVersion = "F006.V1.0.0.0",
                ContentContainerId = "preservation-content",
                WorkingPath = "/preservation",
                PageDescriptorsContainer = new Dictionary<string, string>()
                {
                    { "home", LocalResources.JobPlansOverview },
                    { "assignment", LocalResources.JobPlanAssignmentToTags },
                    { "followup", LocalResources.PreservationFollowup }
                },
                StyleSheetsList = new List<string>() { "/pages/preservation.css" },
                ScriptsList = new List<string>() { "/libraries/modernizr.js" }
            };

            return View("_ModuleLayout", viewModel);
        }

        #region Plan Overview
        //GET: preservation/{projectId}/partial
        [Route("{projectId}/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public ActionResult GetPlanOverviewPartial(string projectId)
        {
            var viewModel = new PlanningOverviewViewModel();
            var htmlFileName = "_PlanningOverview";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.DataTableList = _preservationService.GetDatatableNames(false);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }
            return PartialView(htmlFileName, viewModel);
        }

        //GET: preservation/{projectId}/overview/table
        [Route("{projectId}/overview/table")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public ActionResult GetTypecodesTable(string projectId)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var tableName = _preservationService.GetTableNameTypecodes();
                viewModel.Base = _engineeringService.GetMetadataForTable(tableName);// take all columns               
                viewModel.Base.Columns = viewModel.Base.Columns.Take(2).ToList();// take only code and description
                viewModel.TableName = tableName;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: preservation/{projectDb}/overview/info
        [Route("{projectDb}/overview/info")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public ActionResult GetPlanOverviewActionsPartial(string projectDb, string code)
        {
            var viewModel = new PreservationActionsDataViewModel();
            var htmlFileName = "_PreservationActions";
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateString(code, LocalResources.Code)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.PreservationDetails = _preservationService.GetAllDataForPreservation(code);
            }

            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }
            return PartialView(htmlFileName, viewModel);
        }
        #endregion  Plan Overview

        #region Job Assignment 
        //GET: preservation/{projectId}/assignment/partial
        [Route("{projectId}/assignment/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public ActionResult GetPlanAssignmentPartial(string projectId)
        {
            var viewModel = new JobAssignmentViewModel();
            var htmlFileName = "_PlanningAssignment";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.DataTableList = _preservationService.GetDatatableNames(false);
                viewModel.PreservationLocations = _preservationService.GetLocations();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: preservation/{projectId}/assignment/table
        [Route("{projectId}/assignment/table")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public ActionResult GetTypecodesTableForAssignment(string projectId)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var tableName = _preservationService.GetTableNameTypecodes();
                viewModel.Base = _engineeringService.GetMetadataForTable(tableName);// take all columns 
                viewModel.Base.Columns = viewModel.Base.Columns.Take(2).ToList();// take only code and description
                viewModel.TableName = tableName;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: preservation/{projectId}/assignment/tagstable
        [Route("{projectId}/assignment/tagstable")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public ActionResult GetTypecodesTagsTable(string projectId, string datatable)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(datatable, LocalResources.Datatable)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Base = new GridData();
                viewModel.Base.Columns = _preservationService.GetRepository().GetColumnsForAssignmentTagsTable(datatable);
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = datatable;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
        #endregion Job Assignment 

        #region  Follow Up
        //GET: preservation/{projectId}/followup/partial
        [Route("{projectId}/followup/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public ActionResult GetPreservationFollowUpPartial(string projectId)
        {
            var viewModel = new PreservationFollowUpViewModel();
            var htmlFileName = "_PreservationFollowUp";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.DataTableList = _preservationService.GetDatatableNames(true);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }
            return PartialView(htmlFileName, viewModel);
        }

        //GET: preservation/{projectId}/followup/maintaintable
        [Route("{projectId}/followup/maintaintable")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public ActionResult GetTypecodesMaintainTable(string projectId, string datatable)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(datatable, LocalResources.Datatable)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Base = new GridData();
                viewModel.Base.Columns = _preservationService.GetRepository().GetColumnsForFollowUpMainTable(datatable);
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = datatable;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: preservation/{projectDb}/followup/activities/status
        [Route("{projectDb}/followup/activities/status")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPreservationPlanning)]
        public ActionResult GetPreservationFollowUpActivitiesPartial(string projectDb, string datatable, string tag, string code, string scheduledW)
        {
            var viewModel = new PreservationFollowupActivitiesViewModel();
            var htmlFileName = "_PreservationFollowUpActivities";
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateString(datatable, LocalResources.Datatable)
                                    .ValidateString(tag, LocalResources.TagCode)
                                    .ValidateString(code, LocalResources.CodeIdentifier)
                                    .ValidateString(scheduledW, LocalResources.ScheduledWeek)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.PreservationActionsDetails = _preservationService.GetPreservationActions(datatable, code, tag, scheduledW);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }
            return PartialView(htmlFileName, viewModel);
        }
        #endregion Follow Up
    }
}