﻿using System.Web.Mvc;
using Promis.NET.MCV.ViewModels.Dashboard;
using Promis.NET.MCV.Extensions;
using System.Threading;
using Promis.NET.Services.Services.Interfaces;
using System.Globalization;

namespace Promis.NET.MCV.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private IDashboardService _dashboardService;

        public HomeController(IDashboardService dashboardService)
        {
            _dashboardService = dashboardService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _dashboardService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        //GET: / or /home or /home/index
        public ActionResult Index()
        {
            var viewModel = new DashboardDataViewModel();
            return View("Index", viewModel);
        }
    }
}