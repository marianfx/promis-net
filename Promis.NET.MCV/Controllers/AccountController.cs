﻿using DevTrends.MvcDonutCaching;
using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.MCV.ViewModels.Account;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using System;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;

namespace Promis.NET.MCV.Controllers
{
    [Authorize]
    [RoutePrefix("account")]
    public class AccountController : Controller
    {
        private IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }
        
        //GET: account/login
        [AllowAnonymous]
        [Route("login")]
        public ActionResult Login()
        {
            return View("Login");
        }

        //GET: account/details
        [Route("details")]
        public ActionResult PersonalInfo()
        {
            return PartialView("_UserDetails", new UserDetailsViewModel());
        }


        //GET: account/data
        //The [ChildActionOnly] attribute specifies to the client (browser) to catch its output per account; Duration for cache is set to 1 day
        [ChildActionOnly]
        [DonutOutputCache(Duration = 86400, Location = System.Web.UI.OutputCacheLocation.Server, VaryByParam = "userId")]
        public ActionResult GetUserDataById(string userId)
        {
            var viewModel = new UserDataViewModel();
            try
            {
                viewModel.Base = _accountService.GetUserSmallDataById(userId);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView("_UserData", viewModel);
        }

        //PUT: account/data/{userId}/refresh
        [HttpPost]
        [AllowAnonymous]
        [Route("data/{userId}/refresh")]
        public ActionResult Refresh(string userId)
        {
            var cacheManager = new OutputCacheManager();
            cacheManager.RemoveItem("Account", "GetUserDataById", new {  userId });
            return Json(new { success = true });
        }
    }
}