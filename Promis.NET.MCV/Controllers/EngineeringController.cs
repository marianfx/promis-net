﻿
using DevTrends.MvcDonutCaching;
using Promis.NET.Localization;
using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.MCV.ViewModels.Engineering;
using Promis.NET.Roles;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;

namespace Promis.NET.MCV.Controllers
{
    [Authorize]
    [RoutePrefix("engineering")]
    public class EngineeringController : Controller
    {
        private IEngineeringService _engineeringService;
        private ControllerValidator _validator = new ControllerValidator();

        public EngineeringController(IEngineeringService engineeringService)
        {
            _engineeringService = engineeringService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _engineeringService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        //GET: engineering/
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadConstructionEngineeringDatabase)]
        public ActionResult Index()
        {
            var viewModel = new EngineeringHomeViewModel();
            return View("Index", viewModel);
        }

        //GET: engineering/grid
        [Route("grid")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadConstructionEngineeringDatabase)]
        public ActionResult GridPlaceholder(string dataTable, string tableTitle)
        {
            return View("GridHome");
        }

        //GET | POST: engineering/tables/{projectDb}/{dataTable}/
        [AcceptVerbs("GET", "POST")]
        [Route("tables/{projectDb}/{dataTable}")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadConstructionEngineeringDatabase)]
        public ActionResult GetTableData(string projectDb, string dataTable, string tableTitle)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "_TableData";
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateString(dataTable, LocalResources.Datatable)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            var isAdmin = User.IsInRole(LocalRoles.ResponsabilityAdmin);
            var hasWrite = User.IsInRole(LocalRoles.ModuleWriteConstructionEngineeringDatabase);

            try
            {
                viewModel.Base = _engineeringService.GetMetadataForTable(dataTable);//here i receive only the columns
                viewModel.TableName = dataTable;
                viewModel.TableTitle = (string.IsNullOrWhiteSpace(tableTitle) ? dataTable : tableTitle).ToUpper();
                viewModel.HasOperations = isAdmin || hasWrite;
                viewModel.HasImportRole = isAdmin || hasWrite;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: engineering/tables/{projectDb}/{dataTable}/metadata
        [Route("tables/{projectDb}/{dataTable}/metadata")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadConstructionEngineeringDatabase)]
        public ActionResult GetTableMetadata(string projectDb, string dataTable, string operation)
        {
            var viewModel = new EngineeringTableMetadataViewModel();
            var htmlFileName = "_TableDisplay";
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateString(dataTable, LocalResources.Datatable)
                                    .ValidateString(operation, LocalResources.Operation)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                switch (operation)
                {
                    case "S":
                        viewModel.OperationType = "Insert Similar";
                        break;
                    case "I":
                        viewModel.OperationType = "Insert";
                        break;
                    case "U":
                        viewModel.OperationType = "Update";
                        break;
                    case "D":
                        viewModel.OperationType = "Delete";
                        break;
                }
                viewModel.TableName = dataTable;
                viewModel.ColumnMetadatas = _engineeringService.GetColumnMetadataForTable(dataTable, projectDb);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: engineering/tables/{projectDb}/{dataTable}/filters
        [Route("tables/{projectDb}/{dataTable}/filters")]
        public ActionResult GetTableFiltersForAdvancedSearch(string projectDb, string dataTable)
        {
            var viewModel = new EngineeringTableMetadataViewModel();
            var htmlFileName = "_FiltersDisplay";
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .ValidateUserToBeInAnyReadModuleRole(User)
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {                       
                viewModel.TableName = dataTable;
                viewModel.ColumnMetadatas = _engineeringService.GetColumnMetadataForTable(dataTable, projectDb, true);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: engineering/tables/{projectId}/{dataTable}/import
        [Route("tables/{projectId}/{dataTable}/import")]
        public ActionResult GetTableDataForExcelDataImport(string datatable, string filePath)
        {
            var errors = _validator.ValidateUserToBeInAnyReadModuleRole(User)
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
            }

            return PartialView("_ImportData");
        }
        

        //GET: engineering/tags
        [HttpGet]
        [Route("tags")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadConstructionEngineeringDatabase)]
        public ActionResult GetTagsUsingSchemas()
        {
            var viewModel = new EngineeringTagsViewModel();
            var htmlFileName = "CreateTags";
            try
            {
                viewModel.TagsData = _engineeringService.GetDataForTags();
                viewModel.Tags = viewModel.TagsData.Select(t => t.Code).Distinct().ToList();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return View(htmlFileName, viewModel);
        }

        #region Utilities
        //This attribute specifies to the client (browser) to catch its output per project; Duration for cache is set to 1 day
        [ChildActionOnly]
        [DonutOutputCache(Duration = 86400, Location = System.Web.UI.OutputCacheLocation.Server, VaryByParam = "projectDb")]//child actions do not need Location set
        public ActionResult GetEngineeringMenu(string projectDb)
        {
            var viewModel = new EngineeringMenuViewModel();
            try
            {
                viewModel.Base = _engineeringService.GetMenuItemsForProject(projectDb);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView("_EngineeringMenu", viewModel);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("menu/{projectDb}/refresh")]
        public ActionResult RefreshEngineeringMenu(string projectDb)
        {
            var cacheManager = new OutputCacheManager();
            cacheManager.RemoveItem("Engineering", "GetEngineeringMenu", new { projectDb });
            return Json(new { success = true });
        }
        #endregion
    }
}