﻿using Promis.NET.Localization;
using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.Utilities.Exceptions;
using System;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Promis.NET.MCV.Controllers
{
    [Authorize]
    [RoutePrefix("content")]
    public class ContentController : Controller
    {
        private ControllerValidator _validator = new ControllerValidator();

        public ContentController()
        {
            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        // GET: content
        [Route("download")]
        public FileResult Download(string type, string filePath)
        {
            if (string.IsNullOrWhiteSpace(type))
                throw new HttpException(400, LocalResources.FileTypeNotSpecified);
            if (string.IsNullOrWhiteSpace(filePath))
                throw new HttpException(400, LocalResources.FilePathNotSpecified);

            var errors = _validator.ValidateUserToBeInAnyReadModuleRole(User).GetErrors();
            if (errors.Count > 0)
                throw new UnauthorizedAccessException(errors[0]);

            string mimeType = "application/octet-stream";
            filePath = filePath.StartsWith("/") ? filePath.Substring(1) : filePath;
            var directory = "";

            if(type == "excel")
            {
                mimeType = "application/vnd.ms-excel";
                directory = "Excel Exports/";
            }

            if (type.StartsWith("image"))
            {
                mimeType = "image/*";
                directory = ParsePossibleSubdir("Images/", type);
            }

            if (type.StartsWith("pdf"))
            {
                mimeType = "application/pdf";
                directory = ParsePossibleSubdir("PDFs/", type);
            }

            var serverPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/" + directory + filePath);
            if (!(new FileInfo(serverPath).Exists))
                throw new HttpException(404, LocalResources.FileNotFound);

            var stream = new FileStream(serverPath, FileMode.Open);
            return File(stream, mimeType, new FileInfo(serverPath).Name);
        }

        private string ParsePossibleSubdir(string directory, string type)
        {
            var typeSplit = type.Split('/');
            if (typeSplit.Length < 2)
                return directory;

            var subDir = typeSplit[1];
            if (string.IsNullOrWhiteSpace(subDir))
                return directory;
            
            subDir = subDir[0].ToString().ToUpper() + subDir.Substring(1);
            return directory + subDir + "/";
        }

        [HttpPost]
        [Route("upload")]
        public ActionResult Upload(string type)
        {
            var errors = _validator.ValidateUserToBeInAnyWriteModuleRole(User).GetErrors();
            if (errors.Count > 0)
                throw new UnauthorizedAccessException(errors[0]);

            var userName = User.Identity.Name;
            var projectDb = User.Identity.GetProjectDatabaseName();
            var baseFileName = projectDb + "-(" + userName + ")-" + Guid.NewGuid().ToString();
            var directory = "Uploads";

            if (Request.Files.Count == 0 || Request.Files[0].ContentLength == 0)
                return Json(new Envelope().AddError(LocalResources.NoFileToUpload).GetOnlyData());

            try
            {
                var file = Request.Files[0];
                var indexOfExtensionPoint = file.FileName.LastIndexOf(".");
                baseFileName += indexOfExtensionPoint > 0 ? "." + file.FileName.Substring(indexOfExtensionPoint + 1) : "";

                switch (type)
                {
                    case "excel":
                        directory = "Excel Imports";
                        break;
                    default:
                        break;
                }

                //create dir if it does not exist
                string dirPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/" + directory + "/");
                if (!Directory.Exists(dirPath))
                    Directory.CreateDirectory(dirPath);

                string serverPath = dirPath + baseFileName;
                file.SaveAs(serverPath);
            }
            catch(Exception e)
            {
                return Json(new Envelope().SetErrorObject(PromisException.GetFromExistingException(e).ErrorDetailsUserLevel).GetOnlyData());
            }

            return Json(new Envelope().SetData(directory + "/" + baseFileName).GetOnlyData());
        }
        
        //GET: pdf?document=xxx&pageMode=xxy&title=xxz
        // document must be the path relative to the "PDFs" directory
        [Route("pdf")]
        public ActionResult DownloadAndViewPdf(string document, string pageMode, string title)
        {
            var errors = _validator.ValidateUserToBeInAnyReadModuleRole(User).GetErrors();
            if (errors.Count > 0)
                throw new UnauthorizedAccessException(errors[0]);

            var viewModel = new PdfViewerViewModel();
            var htmlFileName = "PdfViewer";

            viewModel.DocUrl = HttpUtility.UrlDecode(document);
            viewModel.PageMode = HttpUtility.UrlDecode(pageMode);
            viewModel.Title = HttpUtility.UrlDecode(title);

            return View(htmlFileName, viewModel);
        }

    }
}