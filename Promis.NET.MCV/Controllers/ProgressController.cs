﻿using Promis.NET.Localization;
using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.MCV.ViewModels.Progress;
using Promis.NET.Roles;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Promis.NET.MCV.Controllers
{
    [Authorize]
    [RoutePrefix("progress")]
    public class ProgressController : Controller
    {
        private IProgressService _progressService;
        private IProjectService _projectService;
        private ControllerValidator _validator = new ControllerValidator();

        public ProgressController(IProgressService progressService, IProjectService projectService)
        {
            _projectService = projectService;
            _progressService = progressService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _progressService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        //GET: progress/
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public ActionResult Index()
        {
            var viewModel = new ModuleLayoutViewModel()
            {
                ModuleTitle =LocalResources.ConstructionPlanningAmpProgress,
                ModuleVersion = "F008.V1.0.0.0",
                ContentContainerId = "progress-content",
                WorkingPath = "/progress",
                PageDescriptorsContainer = new Dictionary<string, string>()
                {
                    { "home", LocalResources.ProgressStatus},
                    { "quantities", LocalResources.ReferenceQuantities },
                    { "followup", LocalResources.FollowupByTags },
                    { "progress", LocalResources.ProgressByActivity },
                    //{ "curves", LocalResources.RescheduleCurves },
                    { "planning", LocalResources.PlanningOverview}
                },
                StyleSheetsList = new List<string>() { "/pages/progress.css"}
            };

            return View("_ModuleLayout", viewModel);
        }

        //GET: progress/{projectId}/partial
        [Route("{projectId}/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public ActionResult IndexPartial(string projectId)
        {
            var viewModel = new ProgressStatusViewModel();
            var htmlFileName = "_Index";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.MainActivities = _projectService.GetMainActivitiesList(projectDatabase, "<>", DataAccess.Repositories.Interfaces.ActivityOrderType.Under16);

                // compute months / year
                var projectData = _projectService.GetProjectById(projectId);
                viewModel.StartYear = projectData.StartYear;
                viewModel.StartMonth = projectData.StartMonth;
                viewModel.MonthsCount = projectData.MonthsCount;
                viewModel.MonthsName = Localization.Global.Months;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope.SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: progress/{projectId}/quantities/partial
        [Route("{projectId}/quantities/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public ActionResult QuantitiesPartial(string projectId)
        {
            var viewModel = new QuantitiesViewModel();
            var htmlFileName = "_Quantities";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.MainActivities = _projectService.GetMainActivitiesList(projectDatabase, "<>", DataAccess.Repositories.Interfaces.ActivityOrderType.Under16);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: progress/{projectId}/quantities/table/partial[?activity=xxx]
        [Route("{projectId}/quantities/table/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public ActionResult QuantitiesTablePartial(string projectId, string activity = null)
        {
            var viewModel = new QuantitiesTableViewModel();
            var htmlFileName = "_QuantitiesTable";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(activity,LocalResources.Activity)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                activity = HttpUtility.UrlDecode(activity);
                viewModel.Activities = _progressService.GetQuantitiesData(activity);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: progress/{projectId}/followup/partial
        [Route("{projectId}/followup/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public ActionResult FollowUpPartial(string projectId)
        {
            var viewModel = new FollowUpViewModel();
            var htmlFileName = "_Followup";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.ProgressTableName = _progressService.GetTableNameProgress(projectDatabase, "@ma");
                viewModel.MainActivities = _projectService.GetMainActivitiesList(projectDatabase, "<>", DataAccess.Repositories.Interfaces.ActivityOrderType.Under16);
                viewModel.Areas = _projectService.GetAllAreas(projectDatabase);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: progress/{projectId}/followup/table/partial?subActivity=xxx
        [Route("{projectId}/followup/table/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public ActionResult FollowUpTablePartial(string projectId, string mainActivity, string subActivity)
        {
            var viewModel = new FollowUpTableViewModel();
            var htmlFileName = "_FollowupTable";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(mainActivity, LocalResources.MainActivity)
                                    .ValidateString(subActivity, LocalResources.SubActivity)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            mainActivity = HttpUtility.UrlDecode(mainActivity);
            subActivity = HttpUtility.UrlDecode(subActivity);
            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                var subs = _projectService.GetAllDataForSubActivity(projectDatabase, subActivity);
                viewModel.SetSubactivities(subs);

                // prepare progress table
                var progressTable = _progressService.RunPrerequisitesForProgressTables(projectDatabase, mainActivity, subActivity);
                viewModel.ProgressTableName = progressTable;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: progress/{projectId}/progress/partial
        [Route("{projectId}/progress/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public ActionResult ProgressPartial(string projectId, bool isBaselineVisible = true, bool isReschedule = false)
        {
            var viewModel = new ProgressPlanningViewModel();
            var htmlFileName = "_Progress";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.MainActivities = _projectService.GetMainActivitiesList(projectDatabase, "<>", DataAccess.Repositories.Interfaces.ActivityOrderType.Under16);

                // compute months / year
                var projectData = _projectService.GetProjectById(projectId);
                viewModel.StartYear = projectData.StartYear;
                viewModel.StartMonth = projectData.StartMonth;
                viewModel.MonthsCount = projectData.MonthsCount;
                viewModel.MonthsName = Localization.Global.Months;

                // editables
                viewModel.IsBaselineVisible = isBaselineVisible;
                viewModel.IsReschedule = isReschedule;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }
            
            return PartialView(htmlFileName, viewModel);
        }

        //GET: progress/{projectId}/curves/partial
        [Route("{projectId}/curves/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public ActionResult CurvesPartial(string projectId)
        {
            return ProgressPartial(projectId, false, true);
        }

        //GET: progress/{projectId}/planning/partial
        [Route("{projectId}/planning/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public ActionResult OverviewPartial(string projectId)
        {
            var htmlFileName = "_Planning";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName);
            }

            return PartialView(htmlFileName);
        }

        //GET: progress/{projectId}/planning/accordion/partial
        [Route("{projectId}/planning/accordion/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public ActionResult GetOverviewAccordion(string projectId)
        {
            var viewModel = new AccordionViewModel();
            var htmlFileName = "_Accordion";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Title = LocalResources.ProjectOverviewByMainActivity;
                viewModel.Description =LocalResources.EachMainActivityIsShownRespectingAConstructionSequence;
                viewModel.Identifier = "accordion-overview";
                viewModel.PathToRetrieveElements = string.Format("progress/{0}/planning/accordion/table/partial", projectId);

                var projectDatabase = User.Identity.GetProjectDatabaseName();
                var mainActivities = _projectService.GetMainActivitiesList(projectDatabase, "<>", DataAccess.Repositories.Interfaces.ActivityOrderType.Under16);

                viewModel.Elements = mainActivities.ToDictionary(ks => ks.Code, vs => vs.Description);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: progress/{projectId}/planning/accordion/table/partial[?code=xxx"]
        [Route("{projectId}/planning/accordion/table/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPlanningProgress)]
        public ActionResult GetOverviewAccordionTable(string projectId, string code)
        {
            var viewModel = new ProgressOverviewViewModel();
            var htmlFileName = "_PlanningTable";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(code, LocalResources.MainActivity)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                var projectData = _projectService.GetProjectById(projectId);
                viewModel.StartYear = projectData.StartYear;
                viewModel.StartMonth = projectData.StartMonth;
                viewModel.MonthsCount = projectData.MonthsCount;
                viewModel.MonthsName = Localization.Global.Months;

                viewModel.Activities = _projectService.GetAllAssociatedActivitiesWithPlanningData(projectId, projectDatabase, code, false, populateProgressArrays: false);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
    }
}