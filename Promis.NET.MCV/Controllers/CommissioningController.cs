﻿using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.MCV.ViewModels.Commissioning;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.MCV.ViewModels.Engineering;
using System.Web;
using Promis.NET.Services.Models.Engineering;
using Microsoft.AspNet.Identity;
using System.Globalization;
using Promis.NET.Roles;
using Promis.NET.Localization;

namespace Promis.NET.MCV.Controllers
{
    [Authorize]
    [RoutePrefix("commissioning")]
    public class CommissioningController : Controller
    {
        private IProjectService _projectService;
        private ICommissioningService _commissioningService;
        private IAccountService _accountService;
        private ControllerValidator _validator = new ControllerValidator();

        public CommissioningController(ICommissioningService commissioningService, IProjectService projectService, IAccountService accountService)
        {
            _commissioningService = commissioningService;
            _accountService = accountService;
            _projectService = projectService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
                _commissioningService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public ActionResult Index()
        {
            var viewModel = new ModuleLayoutViewModel()
            {
                ModuleTitle = LocalResources.PrecommissioningCommissioningManagement,
                ModuleVersion = "F009.V1.0.0.0",
                ContentContainerId = "commiss-setup-content",
                WorkingPath = "/commissioning",
                PageDescriptorsForDropdowns = new Dictionary<string, Dictionary<string, string>>()
                {
                    {
                        "home", new Dictionary<string,string>()
                        {
                            { "home", LocalResources.ProgressAmpSystems },
                            { "tests", LocalResources.LoopTestsStatusBySystem },
                            { "certification", LocalResources.CertificationCompletion }
                        }
                    }
                },
                PageDescriptorsContainer = new Dictionary<string, string>()
                {
                    { "home", LocalResources.Status },
                    { "inspection-tags", LocalResources.AssignInspectionPlanToTag },
                    { "subdivisions", LocalResources.SystemSubdivision },
                    { "inspection", LocalResources.InspectionsFollowup },
                },
                StyleSheetsList = new List<string>() { "/pages/commissioning.css" } // CSS
            };

            return View("_ModuleLayout", viewModel);
        }

        #region Status
        //GET: commissioning/{projectId}/partial
        [Route("{projectId}/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public ActionResult GetProgressAndSystemPartial(string projectId)
        {
            var viewModel = new ProgressAndSystemsViewModel();
            var htmlFileName = "_ProgressAndSystems";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.DataTablesInfo = _commissioningService.GetDataTableInfoComplete();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: commissioning/{projectDb}/progress/tags/notassigned
        [Route("{projectDb}/progress/tags/notassigned")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public ActionResult GetTagsNotAssignedPartial(string projectDb, string dataTable)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateString(dataTable, LocalResources.Datatable)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Base = new GridData();
                viewModel.Base.Columns = _commissioningService.GetRepository().GetColumnsForTablesWithTagsNotAssigned(dataTable);
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = dataTable;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        } 

        //GET: commissioning/{projectId}/tests/partial
        [Route("{projectId}/tests/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public ActionResult GetLoopTestsPartial(string projectId)
        {
            var viewModel = new LoopTestsViewModel();
            var htmlFileName = "_LoopTestsBySystem";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Systems = _commissioningService.GetSystemListForLoops();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: commissioning/data/{projectDb}/loop/tags
        [Route("data/{projectDb}/loop/tags")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public ActionResult GetLoopTagsPartial(string projectDb, string system)
        {
            var viewModel = new LoopTagsViewModel();
            var htmlFileName = "_LoopTags";
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateString(system, LocalResources.SystemCode)                             
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.Tags = _commissioningService.GetTagsForSystem(system);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }
            return PartialView(htmlFileName, viewModel);
        }

        //GET: commissioning/data/{projectDb}/loop/tags/table
        [Route("data/{projectDb}/loop/tags/table")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public ActionResult GetLoopTagsTablePartial(string projectDb, string tag)
        {
            var viewModel = new LoopTagsTableViewModel();
            var htmlFileName = "_LoopTagsTables";
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateString(tag, LocalResources.SystemTag)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.TagsTableInfo = _commissioningService.GetDataForLoopTagsTable(tag);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }
            return PartialView(htmlFileName, viewModel);
        }

        // GET: /commissioning/{projectId}/certification/partial
        [Route("{projectId}/certification/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public ActionResult GetCertificationCompletionPartial(string projectId)
        {          
            var viewModel = new CertificationCompletionViewModel();
            var htmlFileName = "_CertificationCompletion";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.CertificationDataTablesInfo = _commissioningService.GetCertificationCompletionData();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }
            return PartialView(htmlFileName, viewModel);    
    }
        #endregion Status

        #region Assign Inspection to Tags
        //GET: commissioning/{projectId}/inspection-tags/partial
        [Route("{projectId}/inspection-tags/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public ActionResult InspecionToTagsPartial(string projectId)
        {
            var viewModel = new InspectionTagsViewModel();
            var htmlFileName = "_InspectionTags";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.MainActivities = _projectService.GetMainActivitiesList(projectDatabase, "<>", ActivityOrderType.Over16);
                viewModel.Datatables = _commissioningService.GetDatatables();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: commissioning/{projectId}/inspection-tags/table/partial
        [Route("{projectId}/inspection-tags/table/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public ActionResult GetTagsDataTable(string projectId, string mainActivity, string tableName)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(mainActivity, LocalResources.MainActivity)
                                    .ValidateString(tableName, LocalResources.Datatable)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            mainActivity = HttpUtility.UrlDecode(mainActivity);
            tableName = HttpUtility.UrlDecode(tableName);

            try
            {
                viewModel.Base = new Services.Models.Engineering.GridData();
                viewModel.Base.Columns = _commissioningService.GetColumnsNecessaryForTagsTable(mainActivity);
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = tableName;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
        #endregion

        #region System subdivision
        //GET: commissioning/{projectId}/subdivisions/partial
        [Route("{projectId}/subdivisions/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public ActionResult SystemSubdivPartial(string projectId)
        {
            var viewModel = new SystemSubdivisionViewModel();
            var htmlFileName = "_SystemSubdivision";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }
            if (projectId != User.Identity.GetProjectId())
            {
                ViewBag.Envelope = new Envelope().AddError(LocalResources.YouCanOnlyGetDataForYourProjectNotForAnotherProject).GetResponse();
                return PartialView("_SystemSubdivision", viewModel);
            }
            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.Datatables = _commissioningService.GetDatatables(true);
                viewModel.Systems = _commissioningService.GetSystemList();
                var counts = _commissioningService.GetTotalCountsAndPercentage("");
                viewModel.PrPc = counts.Item1;
                viewModel.Tottags = counts.Item2;
                viewModel.TotAss = counts.Item3;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: commissioning/{projectId}/subdivisions/table/partial
        [Route("{projectId}/subdivisions/table/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public ActionResult GetSystemDataTable(string projectId, string tableName)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(tableName, LocalResources.Datatable)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }
            tableName = HttpUtility.UrlDecode(tableName);
            try
            {
                viewModel.Base = new GridData();
                viewModel.Base.Columns = _commissioningService.GetColumnsNecessaryForSystemTable();
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = tableName;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
        #endregion

        #region Inspection Follow-Up
        //GET: commissioning/{projectId}/inspection/partial
        [Route("{projectId}/inspection/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public ActionResult InspectionFollowUpPartial(string projectId)
        {
            var viewModel = new InspectionFollowUpViewModel();
            var htmlFileName = "_InspectionFollowUp";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var userId = User.Identity.GetUserId();
                var user = _accountService.GetUserDetailsById(userId);
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.MainActivities = _projectService.GetMainActivitiesList(projectDatabase, "<>", ActivityOrderType.Over16);
                viewModel.Datatables = _commissioningService.GetDatatables();
                viewModel.UserName = user.FullName;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        [Route("{projectId}/inspection-follow-up/table/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public ActionResult GetFollowUpLeftDataTable(string projectId, string tableName)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(tableName, LocalResources.Datatable)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }
            tableName = HttpUtility.UrlDecode(tableName);

            try
            {
                viewModel.Base = new Services.Models.Engineering.GridData();
                viewModel.Base.Columns = _commissioningService.GetColumnsNecessaryForFollowUpTable();
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = tableName;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        [Route("{projectId}/inspection-follow-up/statusTable/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadPrecommInspections)]
        public ActionResult GetDataForStatusDatatable(string projectId, string dataTable, string rowId, string mainActivity)
        {
            var viewModel = new FollowUpStatusViewModel();
            var htmlFileName = "~/Views/Commissioning/_Inspectionfollowupstatus.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(dataTable, LocalResources.Datatable)
                                    .ValidateString(rowId, LocalResources.RowId)
                                    .ValidateString(mainActivity, LocalResources.MainActivity)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();
            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }
            try
            {
                
                 var fullData = _commissioningService.GetStatusOfSelectedRow(dataTable, rowId, mainActivity);
                viewModel.Status = fullData.Key;
                viewModel.Subactivity = fullData.Value.Code;
                viewModel.S_Description = fullData.Value.Description;

            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }
            return PartialView(htmlFileName, viewModel);

        }
        #endregion
    }
}