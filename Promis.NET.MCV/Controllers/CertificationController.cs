﻿using Promis.NET.MCV.Extensions;
using Promis.NET.MCV.ViewModels;
using Promis.NET.MCV.ViewModels.Certification;
using Promis.NET.MCV.ViewModels.Engineering;
using Promis.NET.Services.Models.Engineering;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Exceptions;
using Promis.NET.DataAccess.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Web.Mvc;
using System.Linq;
using System.Web;
using System.Globalization;
using Promis.NET.Roles;
using Promis.NET.Localization;

namespace Promis.NET.MCV.Controllers
{
    [Authorize]
    [RoutePrefix("certification")]
    public class CertificationController : Controller
    {
        private IProjectService _projectService;
        private ICommissioningService _commissioningService;
        private ICertificationService _certificationService;
        private ControllerValidator _validator = new ControllerValidator();

        public CertificationController(ICertificationService certificationService, ICommissioningService commissioningService, IProjectService projectService)
        {
            _commissioningService = commissioningService;
            _certificationService = certificationService;
            _projectService = projectService;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            { _certificationService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());
                _certificationService.SetWorkingProject(Thread.CurrentPrincipal.Identity.GetProjectDatabaseName());
            }

            // set current UI culture with the user language stored in claims
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal.Identity.GetProjectId() != null)
            {
                var userLang = Thread.CurrentPrincipal.Identity.GetUserLanguage();
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLang);
            }
        }

        //GET: certification/
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadCertificationManagement)]
        public ActionResult Index()
        {
            var viewModel = new ModuleLayoutViewModel()
            {
                ModuleTitle = LocalResources.CertificationManagementForConstructionPhase,
                ModuleVersion = "F005.V1.0.0.0",
                ContentContainerId = "certification-content",
                WorkingPath = "/certification",
                PageDescriptorsContainer = new Dictionary<string, string>()
                {
                    { "home", LocalResources.TagsAssignmentStatus },
                    { "subcontractors", LocalResources.AssignSowToSubcontractors },
                    { "tags", LocalResources.AssignCertificationPlanToTags },
                    { "summary", LocalResources.CertificationFollowup },
                },
                StyleSheetsList = new List<string>() { "/pages/certification.css" }
            };

            return View("_ModuleLayout", viewModel);
        }

        #region Tags Assignment Status
        //GET: certification/{projectId}/partial
        [Route("{projectId}/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadCertificationManagement)]
        public ActionResult GetTagsAssignmentStatusPartial(string projectId)
        {
            var viewModel = new CertificationTagsAssignmentViewModel();
            var htmlFileName = "_TagsAssignment";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.TagsStatusInfo = _certificationService.GetCertificationCompletionData();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: certification/{projectDb}/progress/tags/notassigned
        [Route("{projectDb}/progress/tags/notassigned")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadCertificationManagement)]
        public ActionResult GetTagsNotAssignedPartial(string projectDb, string code)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectDb, LocalResources.Project)
                                    .ValidateString(code, LocalResources.Code)
                                    .ValidateProject(User.Identity, projectDb, "db", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Base = new GridData();            
                
                viewModel.Base.Columns = _certificationService.GetRepository().GetColumnsForTablesWithTagsNotAssigned();
                viewModel.Base.Rows = _certificationService.GetAllUnassignedTags(code);
               // viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = "tags_not_assigned";
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        #endregion Tags Assignment Status

        #region Assign SOW to SubContractors
        //GET: certification/{projectId}/subcontractors/partial
        [Route("{projectId}/subcontractors/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadCertificationManagement)]
        public ActionResult GetSubcontractorsPartial(string projectId)
        {
            var viewModel = new CertificationAssignSOWViewModel();
            var htmlFileName = "_AssignSOWtoSubcontractors";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Subcontractors = _certificationService.GetSubcontractors("pw_" + projectId);
                viewModel.ActivitiesMhrs = _certificationService.GetMainActivityBudget();
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: certification/{projectId}/subcontractors/accordion/partial
        [Route("{projectId}/subcontractors/accordion/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadCertificationManagement)]
        public ActionResult GetSubcontractorsAccordion(string projectId)
        {
            var viewModel = new AccordionViewModel();
            var htmlFileName = "_Accordion";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Title =LocalResources.MainActivities;
                viewModel.Description = LocalResources.SelectSowRelevantToSubcontractorsAwards;
                viewModel.Identifier = "accordion-subcontractors";
                viewModel.MethodToInvoke = "initializeSowAccordion";
                viewModel.PathToRetrieveElements = string.Format("certification/data/{0}/subcontractors/activity/partial", projectId);

                var projectDatabase = User.Identity.GetProjectDatabaseName();
                var mainActivities = _projectService.GetMainActivitiesList(projectDatabase, "<>", DataAccess.Repositories.Interfaces.ActivityOrderType.Under16);

                viewModel.Elements = mainActivities.ToDictionary(ks => ks.Code, vs => vs.Description);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: certification/data/{projectId}/subcontractors/activity/partial
        [Route("data/{projectId}/subcontractors/activity/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadCertificationManagement)]
        public ActionResult GetSubcontractorsPartial(string projectId, string code)
        {
            var viewModel = new CertificationMainActivitiesViewModel();
            var htmlFileName = "_AssignSOWTables";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(code, LocalResources.TheMainActivityCode)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.MainActivitiesDetails = _certificationService.GetMainActivitiesDetails(code);             
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
        #endregion Assign SOW to SubContractors

        #region Assign Certification Plan to Tags
        //GET: certification/{projectId}/tags/partial
        [Route("{projectId}/tags/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadCertificationManagement)]
        public ActionResult GetTagsPartial(string projectId)
        {
            var viewModel = new CertificationAssignPlanViewModel();
            var htmlFileName = "_AssignCertificationPlanToTags";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.MainActivities = _projectService.GetMainActivitiesList(projectDatabase, "<>", ActivityOrderType.Under16);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
        
        //GET: certification/{projectId}/inspection-tags/table/partial
        [Route("{projectId}/tags/table/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadCertificationManagement)]
        public ActionResult GetTagsDataTable(string projectId, string tableName)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(tableName, LocalResources.Datatable)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }
            tableName = HttpUtility.UrlDecode(tableName);
            try
            {
                viewModel.Base = new Services.Models.Engineering.GridData();
                viewModel.Base.Columns = _certificationService.GetColumnsNecessaryForTagsTable();
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = tableName;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
        #endregion Assign Certification Plan to Tags

        #region Certification Summary
        //GET: certification/{projectId}/summary/partial
        [Route("{projectId}/summary/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadCertificationManagement)]
        public ActionResult GetCerificationSummaryPartial(string projectId)
        {
            var viewModel = new CertificationSummaryViewModel();
            var htmlFileName = "_CertificationSummary";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                var projectDatabase = User.Identity.GetProjectDatabaseName();
                viewModel.MainActivities = _projectService.GetMainActivitiesList(projectDatabase, "<>", ActivityOrderType.Under16);
                viewModel.Modules = _projectService.GetActivityActions(projectDatabase);
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }

        //GET: certification/{projectId}/summary/table/partial
        [Route("{projectId}/summary/table/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadCertificationManagement)]
        public ActionResult GetCerificationSummaryTable(string projectId, string dataTable)
        {
            var viewModel = new GridDataViewModel();
            var htmlFileName = "~/Views/Engineering/_TableData.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(dataTable, LocalResources.Datatable)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();

            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }

            try
            {
                viewModel.Base = new GridData();
                viewModel.Base.Columns = _commissioningService.GetColumnsNecessaryForFollowUpTable();
                viewModel.AlternativeColumnNames = viewModel.Base.Columns.Select(c => c.Value).Cast<string>().ToList();
                viewModel.TableName = dataTable;
                viewModel.TableTitle = "";
                viewModel.HasOperations = false;
                viewModel.HasImportExportActions = false;
            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }

            return PartialView(htmlFileName, viewModel);
        }
        
        //GET: certification/{projectId}//summary/stateTable/partial
        [Route("{projectId}/summary/stateTable/partial")]
        [AuthorizeRoles(LocalRoles.ResponsabilityAdmin, LocalRoles.ModuleReadCertificationManagement)]
        public ActionResult GetCerificationSummaryStepsTable(string projectId, string dataTable,string rowId)
        {
            var viewModel = new CertificationSummaryStepsViewModel();
            var htmlFileName = "~/Views/Certification/_CertificationSummarySteps.cshtml";
            var errors = _validator.ValidateString(projectId, LocalResources.Project)
                                    .ValidateString(dataTable, LocalResources.Datatable)
                                    .ValidateString(rowId, LocalResources.RowId)
                                    .ValidateProject(User.Identity, projectId, "id", "get")
                                    .GetErrors();
            if (errors.Count > 0)
            {
                ViewBag.Envelope = new Envelope().AddErrors(errors);
                return PartialView(htmlFileName, viewModel);
            }
            try
            {
                var fullData = _certificationService.GetStatusOfSelectedRow(projectId,dataTable, rowId);
                viewModel.Status = fullData.Key;
                viewModel.Subactivity = fullData.Value.Code;
                viewModel.S_Description = fullData.Value.Description;

            }
            catch (Exception e)
            {
                var pe = PromisException.GetFromExistingException(e);
                ViewBag.Envelope = new Envelope().SetErrorObject(pe.ErrorDetailsUserLevel);
            }
            return PartialView(htmlFileName, viewModel);
        }
        #endregion Certification Summary
    }
}