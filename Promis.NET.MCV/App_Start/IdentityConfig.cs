﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using Microsoft.Owin.Security;
using Promis.NET.DataAccess.Entities;
using Promis.NET.DataAccess.DbContexts;
using Promis.NET.DataAccess.Identity.MySql;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Linq;

namespace Promis.NET.MCV
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>() as MySQLDatabase));
            var config = ConfigurationManager.AppSettings;

            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = false
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = int.Parse(config["PassMinimumLength"]),
                RequireNonLetterOrDigit = bool.Parse(config["PassRequiresNonLetterOrDigit"]),
                RequireLowercase = bool.Parse(config["PassRequiresLowercase"]),
                RequireUppercase = bool.Parse(config["PassRequiresUppercase"])
            };

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("This is PROMIS.NET, Reloaded."));
            }
            return manager;
        }

        /// <summary>
        /// Overrides the default Create method (with almost the same code from the official repo); the difference is that it calls the overriden method CreateAsync defined here.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public IdentityResult Create(ApplicationUser user, string password)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException(nameof(password));

            // validate and hash password
            var result = PasswordValidator.ValidateAsync(password).Result;
            if (!result.Succeeded)
                return result;
            user.Password = PasswordHasher.HashPassword(password);

            return CreateAsync(user).Result;
        }

        public async override Task<IdentityResult> CreateAsync(ApplicationUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            //https://github.com/aspnet/Identity/blob/dev/src/Core/UserValidator.cs
            //await base.UpdateSecurityStampAsync(user.Id);// not implementing security stamp

            // 1. ignore default username unique validation
            var result = await UserValidator.ValidateAsync(user);
            if (!result.Succeeded && result.Errors != null && result.Errors.Where(m => !m.Contains(user.UserName)).Count() > 0)
            {
                return result;
            }

            // 2. use the new (username, projectid) pair validation
            if (Store is UserStore<ApplicationUser> localStore)//
            {
                var possibleExistingUser = localStore.FindByNameAndProjectIdAsync(user.UserName, user.ProjectId).Result;
                if (possibleExistingUser != null)
                    return new IdentityResult(String.Format("The username {0} is already taken.", user.UserName));
            }

            try
            {
                await Store.CreateAsync(user);
                return IdentityResult.Success;// all good
            }
            catch
            {
                return new IdentityResult("Cannot create user.");
            }
        }

        public async Task<ApplicationUser> MatchAnyByUsernameAndPassword(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentNullException(nameof(username));
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException(nameof(password));

            if (!(Store is UserStore<ApplicationUser>))
                throw new Exception("Method valid only for custom UserStore.");

            var localStore = Store as UserStore<ApplicationUser>;
            var allUsersWithThisName = await localStore.FindAllByNameAsync(username);
            foreach (var user in allUsersWithThisName)
            {
                var passwordCheck = PasswordHasher.VerifyHashedPassword(user.Password, password);
                if (passwordCheck != PasswordVerificationResult.Failed)// password not matching
                    return user;
            }

            return null;
        }

        public async Task<ApplicationUser> MatchAsync(string username, string projectId, string password)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentNullException(nameof(username));
            if (string.IsNullOrWhiteSpace(projectId))
                throw new ArgumentNullException(nameof(projectId));
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException(nameof(password));

            if (!(Store is UserStore<ApplicationUser>))
                throw new Exception("Method valid only for custom UserStore.");

            var localStore = Store as UserStore<ApplicationUser>;
            var possibleUser = await localStore.FindByNameAndProjectIdAsync(username, projectId);

            if (possibleUser == null)//user not found
                return null;

            var passwordCheck = PasswordHasher.VerifyHashedPassword(possibleUser.Password, password);
            if (passwordCheck == PasswordVerificationResult.Failed)// password not matching
                return null;

            return possibleUser;//all good
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }
}
