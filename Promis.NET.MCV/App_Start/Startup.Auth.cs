﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Promis.NET.DataAccess.DbContexts;
using Promis.NET.DataAccess.Entities;
using System;
using System.Configuration;
using System.Globalization;
using Promis.NET.DataAccess.Identity.MySql;
using System.Security.Claims;
using System.Linq;
using System.Threading.Tasks;
using Promis.NET.Services.Models.Account;
using Promis.NET.MCV.Extensions;
using Microsoft.Owin.Security;

namespace Promis.NET.MCV
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            var config = ConfigurationManager.AppSettings;

            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            //app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            var invalidateRolesTimespan = TimeSpan.FromMinutes(double.Parse(config["RolesInvalidateIntervalInMinutes"], CultureInfo.InvariantCulture));
            var cookieOptions = new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/account/login"),
                ExpireTimeSpan = TimeSpan.FromMinutes(double.Parse(config["CookieTimespanInMinutes"], CultureInfo.InvariantCulture)),
                Provider = new CookieAuthenticationProvider
                { 
                    OnValidateIdentity = context => {
                        
                        // 1. first checks to see if user is valid / logged in
                        if(context.Identity == null || string.IsNullOrWhiteSpace(context.Identity.GetUserId()) || !context.Identity.IsAuthenticated || string.IsNullOrWhiteSpace(context.Identity.GetClaimValue("SecurityTimestamp")))
                        {
                            context.RejectIdentity();
                            return Task.FromResult(context);
                        }

                        // 2. Verify if check needs to be done (the required time span for checks for this user has passed)
                        var lastCheckedDate = DateTime.Parse(context.Identity.GetClaimValue("SecurityTimestamp"));
                        var nowDate = DateTime.Now;
                        var difference = nowDate - lastCheckedDate;
                        if(difference < invalidateRolesTimespan)
                            return Task.FromResult(context);

                        // 3. get user; if user does not exist, reject
                        var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(ApplicationDbContext.Create()));
                        var AuthenticationManager = context.OwinContext.Authentication;
                        var user = userManager.FindById(context.Identity.GetUserId());
                        if(user == null)
                        {
                            context.RejectIdentity();
                            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                            return Task.FromResult(context);
                        }

                        // 4. check roles with database; if roles are the same, all good
                        var rolesFromDatabase = new PromisUser(user).RolesAll;
                        var rolesFromCookie = context.Identity.Claims
                                    .Where(c => c.Type == ClaimTypes.Role)
                                    .Select(c => c.Value);
                        var isOK = !(rolesFromCookie.Except(rolesFromDatabase).Count() > 0 || rolesFromDatabase.Except(rolesFromCookie).Count() > 0);
                        if (isOK)//mark as verified at the current timestamp
                        {
                            context.Identity.SetClaimValue("SecurityTimestamp", DateTime.Now.ToLongTimeString());
                            return Task.FromResult(context);
                        }

                        // 5. if roles have changed, user is stil valid so update (remove + init) user cookie
                        var newIdentity = user.CreateAuthenticationCookie(userManager).Result;
                        context.ReplaceIdentity(newIdentity);
                        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                        AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = true }, newIdentity);
                        return Task.FromResult(context);
                    }
                }
            };
            app.UseCookieAuthentication(cookieOptions);
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
        }
    }
}
