﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Promis.NET.DataAccess.DbContexts;
using Promis.NET.DataAccess.Repositories;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.MCV.Controllers;
using Promis.NET.Services.Services;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Logging;
using System.Configuration;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;

namespace Promis.NET.MCV.App_Start
{
    public class AutofacConfig
    {
        public static IContainer container;

        public static void RegisterAutofac(HttpConfiguration config)
        {
            config = config ?? new HttpConfiguration();

            var builder = new ContainerBuilder();
            var builderApi = new ContainerBuilder();

            AddDataComponentsRegistration(builder);
            AddMvcRegistration(builder);
            AddApiRegistration(builder, config);

            container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));//set up for mvc controllers
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);//set up for WebApi Controllers
        }

        private static void AddDataComponentsRegistration(ContainerBuilder builder)
        {
            var defaultproj = ConfigurationManager.AppSettings["DefaultProject"] ?? "pw_0001";
            var useLocalDatabase = ConfigurationManager.AppSettings["UseLocalDatabase"] != null ? bool.Parse(ConfigurationManager.AppSettings["UseLocalDatabase"]) : false;

            //register utilities
            builder.RegisterType<FileLogger>().As<ILogger>().InstancePerLifetimeScope();
            new LoggerManager(new FileLogger());

            // register repositories
            // registering them per request enables the disposal + closure of connections / transactions at the end of a request, keeping them open per request
            ApplicationDbContext.useLocal = useLocalDatabase;//applicationdbcontext is created per owin context, so no autofac config needed, just the local / serverdb
            builder.Register(c => new AccountRepository(new PromisWebDbContext(useLocalDatabase))).As<IAccountRepository>().InstancePerDependency();
            builder.Register(c => new ActivitiesRepository(new ProjectDbContext(defaultproj, useLocalDatabase))).As<IActivitiesRepository>().InstancePerDependency();
            builder.Register(c => new AdvancedTableRepository(new ProjectDbContext(defaultproj, useLocalDatabase))).As<IAdvancedTableRepository>().InstancePerDependency();
            builder.Register(c => new CertificationRepository(new ProjectDbContext(defaultproj, useLocalDatabase))).As<ICertificationRepository>().InstancePerDependency();
            builder.Register(c => new CommissioningRepository(new ProjectDbContext(defaultproj, useLocalDatabase))).As<ICommissioningRepository>().InstancePerDependency();
            builder.Register(c => new DashboardRepository(new ProjectDbContext(defaultproj, useLocalDatabase))).As<IDashboardRepository>().InstancePerDependency();
            builder.Register(c => new EngineeringRepository(new ProjectDbContext(defaultproj, useLocalDatabase))).As<IEngineeringRepository>().InstancePerDependency();
            builder.Register(c => new KpiRepository(new ProjectDbContext(defaultproj, useLocalDatabase))).As<IKpiRepository>().InstancePerDependency();
            builder.Register(c => new PreservationRepository(new ProjectDbContext(defaultproj, useLocalDatabase))).As<IPreservationRepository>().InstancePerDependency();
            builder.Register(c => new ProgressRepository(new ProjectDbContext(defaultproj, useLocalDatabase))).As<IProgressRepository>().InstancePerDependency();
            builder.Register(c => new ProjectRepository(new PromisWebDbContext(useLocalDatabase))).As<IProjectRepository>().InstancePerDependency();


            // add services
            var serverPath = System.Web.Hosting.HostingEnvironment.MapPath("~");
            builder.RegisterType<AccountService>().As<IAccountService>().InstancePerDependency();
            builder.RegisterType<ActivitiesService>().As<IActivitiesService>().InstancePerDependency();
            builder.RegisterType<CertificationService>().As<ICertificationService>().InstancePerDependency();
            builder.RegisterType<CommissioningService>().As<ICommissioningService>().InstancePerDependency();
            builder.RegisterType<DashboardService>().As<IDashboardService>().InstancePerDependency();
            builder.RegisterType<EngineeringService>().As<IEngineeringService>().InstancePerDependency();
            builder.RegisterType<ExcelService>().As<IExcelService>().InstancePerDependency();
            builder.RegisterType<KpiService>().As<IKpiService>().InstancePerDependency();
            builder.Register(c => new PdfCreatorCompletion(serverPath)).As<IPdfCreatorCompletion>().InstancePerDependency();
            builder.Register(c => new PdfCreatorCertification(serverPath)).As<IPdfCreatorCertification>().InstancePerDependency();
            builder.Register(c => new PdfCreatorPlanningAndProgress(serverPath)).As<IPdfCreatorPlanningAndProgress>().InstancePerDependency();
            builder.RegisterType<PreservationService>().As<IPreservationService>().InstancePerDependency();
            builder.RegisterType<ProgressService>().As<IProgressService>().InstancePerDependency();
            builder.RegisterType<ProjectService>().As<IProjectService>().InstancePerDependency();

        }

        private static void AddMvcRegistration(ContainerBuilder builder)
        {
            //mvc
            builder.RegisterControllers(Assembly.GetExecutingAssembly()).InstancePerRequest();
            builder.RegisterType<CertificationController>().InstancePerDependency();//use this to make it transient, to be able to render its own methods in Views
            builder.RegisterType<CommissioningController>().InstancePerDependency();//use this to make it transient, to be able to render its own methods in Views
            builder.RegisterType<EngineeringController>().InstancePerDependency();//use this to make it transient, to be able to render its own methods in Views
            builder.RegisterType<KpiController>().InstancePerDependency();//use this to make it transient, to be able to render its own methods in Views
            builder.RegisterType<PreservationController>().InstancePerDependency();//use this to make it transient, to be able to render its own methods in Views
            builder.RegisterType<ProjectController>().InstancePerDependency();//use this to make it transient, to be able to render its own methods in Views
            builder.RegisterType<ProgressController>().InstancePerDependency();//use this to make it transient, to be able to render its own methods in Views
            builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());
            //builder.RegisterFilterProvider();
            //builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            //builder.RegisterModelBinderProvider();
        }

        private static void AddApiRegistration(ContainerBuilder builder, HttpConfiguration config)
        {
            //webapi
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()).InstancePerRequest();
            //builder.RegisterWebApiFilterProvider(GlobalConfiguration.Configuration);
            //builder.RegisterWebApiModelBinders();
        }
    }
}