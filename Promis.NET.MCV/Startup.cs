﻿using Microsoft.Owin;
using Owin;
using Promis.NET.MCV.App_Start;
using System.Web.Http;

[assembly: OwinStartup(typeof(Promis.NET.MCV.Startup))]
namespace Promis.NET.MCV
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            // Get your HttpConfiguration. In OWIN, you'll create one
            // rather than using GlobalConfiguration.
            var config = new HttpConfiguration();

            AutofacConfig.RegisterAutofac(config);
            app.UseAutofacMiddleware(AutofacConfig.container);
            app.UseAutofacMvc();
            app.UseAutofacWebApi(config);
            app.UseWebApi(config);
        }
    }
}
