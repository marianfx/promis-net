﻿using Promis.NET.Localization;
using Promis.NET.MCV.ViewModels.Interfaces;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Engineering
{
    public class EngineeringHomeViewModel : BaseViewModel, IBaseViewModel
    {
        public List<string> EngineeringHeaders = new List<string>() { LocalResources.Datatable, LocalResources.NOfRecords, LocalResources.WithAValidBrIfcQuantities, LocalResources.LinkedToCertification, LocalResources.LinkedToBrSystem };
    }
}
