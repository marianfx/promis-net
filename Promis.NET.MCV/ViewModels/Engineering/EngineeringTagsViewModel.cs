﻿
using Promis.NET.Services.Models.Engineering;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Engineering
{
    public class EngineeringTagsViewModel
    {
        public List<TagsData> TagsData { get; set; }
        public List<string> Tags { get; set; }
        
    }
}