﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Engineering;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Engineering
{
    public class GridDataViewModel : BaseViewModel, IBaseViewModel
    {
        public GridData Base { get; set; } = new GridData();
        public string TableTitle { get; set; } = "";
        public string TableName { get; set; } = "";
        public List<string> AlternativeColumnNames { get; set; } = new List<string>();
        public bool HasOperations { get; set; } = true;
        public bool HasImportExportActions { get; set; } = true;
        public bool HasImportRole { get; set; } = true;

        public GridDataViewModel()
        {
        }

        public GridDataViewModel(GridData baseModel)
        {
            Base = baseModel;
        }
    }
}
