﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Engineering;

namespace Promis.NET.MCV.ViewModels.Engineering
{
    public class EngineeringMenuViewModel : BaseViewModel, IBaseViewModel
    {
        public EngineeringMenuData Base { get; set; }

        public EngineeringMenuViewModel()
        {
            Base = new EngineeringMenuData();
        }

        public EngineeringMenuViewModel(EngineeringMenuData baseModel)
        {
            Base = baseModel;
        }
    }
   
}

