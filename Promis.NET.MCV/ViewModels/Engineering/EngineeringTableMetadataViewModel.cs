﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Engineering;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Engineering
{
    public class EngineeringTableMetadataViewModel : BaseViewModel, IBaseViewModel
    {
        public List<ColumnMetadata> ColumnMetadatas { get; set; }
        public string TableName { get; set; }
        public string OperationType { get; set; }

        public EngineeringTableMetadataViewModel()
        {
            ColumnMetadatas = new List<ColumnMetadata>();
            TableName = string.Empty;
        }
    }
   
}

