﻿using Promis.NET.MCV.ViewModels.Interfaces;
using System.Collections.Generic;


namespace Promis.NET.MCV.ViewModels.Preservation
{
    public class PlanningOverviewViewModel : BaseViewModel, IBaseViewModel
    {
        public List<string> DataTableList { get; set; } = new List<string>();
    }
}