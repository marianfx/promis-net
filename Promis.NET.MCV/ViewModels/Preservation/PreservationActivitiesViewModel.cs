﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Preservation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Promis.NET.MCV.ViewModels.Preservation
{
    public class PreservationActivitiesViewModel : BaseViewModel, IBaseViewModel
    {
        public Dictionary<string, List<PreservationActivities>> ScheduledAndDoneWeeks { get; set; } = new Dictionary<string, List<PreservationActivities>>();
    }
   
}