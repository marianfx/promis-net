﻿
using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Preservation;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Preservation
{

    public class PreservationActionsDataViewModel: BaseViewModel, IBaseViewModel
    {
        public Dictionary<string, List<PreservationDetails>> PreservationDetails { get; set; } = new Dictionary<string, List<PreservationDetails>>();
    }
}