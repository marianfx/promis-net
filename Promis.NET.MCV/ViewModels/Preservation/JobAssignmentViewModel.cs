﻿
using Promis.NET.MCV.ViewModels.Interfaces;
using System.Collections.Generic;


namespace Promis.NET.MCV.ViewModels.Preservation
{
    public class JobAssignmentViewModel : BaseViewModel, IBaseViewModel
    {   
        public List<string> DataTableList { get; set; } = new List<string>();
        public List<string> PreservationLocations { get; set; } = new List<string>();
    }
}