﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Preservation;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Preservation
{
    public class PreservationFollowupActivitiesViewModel : BaseViewModel, IBaseViewModel
    {      
        public Dictionary<string, List<PreservationActions>> PreservationActionsDetails{ get; set; } = new Dictionary<string, List<PreservationActions>>();   
    }
}