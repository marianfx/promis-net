﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Certification;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Certification
{
    public class CertificationTagsAssignmentViewModel: BaseViewModel, IBaseViewModel
    {
        public Dictionary<string, CertificationTagsAssignmentStatus> TagsStatusInfo { get; set; } = new Dictionary<string, CertificationTagsAssignmentStatus>();
    }
}