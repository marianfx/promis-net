﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Project;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Certification
{
    public class CertificationAssignSOWViewModel : BaseViewModel, IBaseViewModel
    {
        public List<Subcontractor> Subcontractors { get; set; } = new List<Subcontractor>();
        public Dictionary<string, double> ActivitiesMhrs { get; set; } = new Dictionary<string, double>();
    }
}