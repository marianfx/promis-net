﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Project;
using System.Collections.Generic;
namespace Promis.NET.MCV.ViewModels.Certification
{
    public class CertificationSummaryViewModel : BaseViewModel, IBaseViewModel
    {
        public List<Activity> MainActivities { get; set; } = new List<Activity>();
        public List<Module> Modules { get; set; } = new List<Module>();
    }
}