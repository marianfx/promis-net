﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Certification;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Certification
{
    public class CertificationSummaryStepsViewModel : BaseViewModel, IBaseViewModel
    {
        public List<CertificationSummarySteps> Status { get; set; } = new List<CertificationSummarySteps>();
        public string Subactivity { get; set; } = string.Empty;
        public string S_Description { get; set; } = string.Empty;

    }
}