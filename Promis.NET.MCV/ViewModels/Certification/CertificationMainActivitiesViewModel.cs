﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Certification;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Certification
{
    public class CertificationMainActivitiesViewModel : BaseViewModel, IBaseViewModel
    {
        public List<CertificationSubcontractorsAssignment> MainActivitiesDetails { get; set; } = new List<CertificationSubcontractorsAssignment>();
    }
}