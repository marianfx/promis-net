﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace Promis.NET.MCV.ViewModels
{
    public class Envelope
    {
        public dynamic data { get; set; }
        public dynamic  errors { get; set; }
        public bool success { get; set; }
        public string next { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        private HttpRequestMessage _request;

        public Envelope()
        {
            data = null;
            errors = new List<string>();
            success = false;
            next = string.Empty;
        }

        public Envelope(dynamic obj, bool isDynamic): this()
        {
            if (obj == null || obj.GetType() != typeof(Envelope))
                return;
            var envObj = obj as Envelope;

            data = envObj.data;
            errors = envObj.errors;
            success = envObj.success;
            next = envObj.next;
        }

        public Envelope(HttpRequestMessage requestFromController)
        {
            _request = requestFromController;
            data = null;
            errors = new List<string>();
            success = false;
            next = string.Empty;
        }

        public IHttpActionResult GetResponse()
        {
            return new EnvelopeResult(this);
        }

        public Envelope SetRequestObject(HttpRequestMessage requestFromController)
        {
            _request = requestFromController;
            return this;
        }

        public Envelope SetOk()
        {
            success = true;
            StatusCode = HttpStatusCode.OK;
            return this;
        }

        public Envelope SetBadRequest()
        {
            success = false;
            StatusCode = HttpStatusCode.BadRequest;
            return this;
        }

        public Envelope SetData(dynamic dataToSet)
        {
            data = dataToSet;
            errors = new List<string>();
            success = true;
            return this;
        }

        public Envelope SetNext(string nextUrl)
        {
            next = nextUrl;
            return this;
        }

        public Envelope SetErrorObject(dynamic errorsObj)
        {
            errors = errorsObj;
            StatusCode = HttpStatusCode.BadRequest;
            success = false;
            return this;
        }

        public Envelope AddError(string errorMessage)
        {
            var list = new List<string>(errors)
            {
                errorMessage
            };

            errors = list;
            success = false;
            StatusCode = HttpStatusCode.BadRequest;
            return this;
        }

        public Envelope AddErrors(IEnumerable<string> errorMessages)
        {
            var list = new List<string>(errors);
            foreach (var error in errorMessages)
            {
                list.Add(error);
            }

            errors = list;
            success = false;
            StatusCode = HttpStatusCode.BadRequest;
            return this;
        }

        public object GetEnvelope()
        {
            return new
            {
                data = data,
                errors = errors,
                status = success,
                next = next
            };
        }

        public object GetOnlyData()
        {
            return data;
        }

        public HttpResponseMessage CreateResponse()
        {
            return _request.CreateResponse(StatusCode, GetEnvelope());
        }
    }

    public class EnvelopeResult : IHttpActionResult
    {
        private Envelope _envelope;

        public EnvelopeResult(Envelope envelope)
        {
            _envelope = envelope;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(_envelope.CreateResponse());
        }
    }
}
