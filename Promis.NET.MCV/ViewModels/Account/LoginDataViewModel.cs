﻿using Promis.NET.MCV.ViewModels.Interfaces;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Account
{
    public class LoginDataViewModel: BaseViewModel, IBaseViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }

        public LoginDataViewModel() { }

        public LoginDataViewModel(LoginDataViewModel copy)
        {
            Username = copy.Username;
            Password = copy.Password;
            ReturnUrl = copy.ReturnUrl;
            RememberMe = copy.RememberMe;
        }

        public override IEnumerable<string> GetValidationErrors()
        {
            var output = new List<string>();

            if (string.IsNullOrWhiteSpace(Username))
                output.Add("Username must be provided.");
            if(string.IsNullOrWhiteSpace(Password))
                output.Add("Password must be provided.");

            return output;
        }
    }
}
