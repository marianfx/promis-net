﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Utilities.Models;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Account
{
    public class UserDetailsViewModel: BaseViewModel, IBaseViewModel
    {
        public List<Country> Countries => Global.Countries;
    }
}