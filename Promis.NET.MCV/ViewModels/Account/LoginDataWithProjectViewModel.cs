﻿using Promis.NET.MCV.ViewModels.Interfaces;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Account
{
    public class LoginDataWithProjectViewModel : LoginDataViewModel, IBaseViewModel
    {
        public string ProjectId { get; set; } = string.Empty;

        public LoginDataWithProjectViewModel() { }

        public LoginDataWithProjectViewModel(LoginDataViewModel baseModel) : base(baseModel) { }

        public LoginDataWithProjectViewModel(LoginDataViewModel baseModel, string projectId): base(baseModel)
        {
            ProjectId = projectId;
        }

        public override IEnumerable<string> GetValidationErrors()
        {
            var output = new List<string>();

            if (string.IsNullOrWhiteSpace(Username))
                output.Add("Username must be provided.");
            if(string.IsNullOrWhiteSpace(Password))
                output.Add("Password must be provided.");
            if (string.IsNullOrWhiteSpace(ProjectId))
                output.Add("Project ID must be provided.");

            return output;
        }
    }
}
