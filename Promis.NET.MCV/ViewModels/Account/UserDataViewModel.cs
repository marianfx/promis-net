﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Account;

namespace Promis.NET.MCV.ViewModels.Account
{
    public class UserDataViewModel : BaseViewModel, IBaseViewModel
    {
        public UserData Base { get; set; }

        public UserDataViewModel()
        {
            Base = new UserData();
        }

        public UserDataViewModel(UserData baseModel)
        {
            Base = baseModel;
        }
    }
}