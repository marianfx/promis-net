﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Account;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Account
{
    public class PromisUserViewModel : BaseViewModel, IBaseViewModel
    {
        public PromisUser Base { get; set; }

        public PromisUserViewModel(PromisUser baseModel)
        {
            Base = baseModel;
        }

        public override IEnumerable<string> GetValidationErrors()
        {
            var output = new List<string>();

            if (string.IsNullOrWhiteSpace(Base.UserName))
                output.Add("Username must not be empty.");
            if (string.IsNullOrWhiteSpace(Base.Email))
                output.Add("Email must not be empty.");
            if (string.IsNullOrWhiteSpace(Base.Password))
                output.Add("Password must not be empty.");
            if (string.IsNullOrWhiteSpace(Base.ProjectId))
                output.Add("ProjectId must not be empty.");

            return output;
        }

        public IEnumerable<string> GetValidationErrorsOnUpdate()
        {
            var output = new List<string>();

            if (string.IsNullOrWhiteSpace(Base.UserName))
                output.Add("Username must not be empty.");
            if (string.IsNullOrWhiteSpace(Base.Email))
                output.Add("Email must not be empty.");

            return output;
        }
    }
}
