﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Project;

namespace Promis.NET.MCV.ViewModels.Project
{
    public class ProjectDataViewModel : BaseViewModel, IBaseViewModel
    {
        public ProjectData Base { get; set; }

        public ProjectDataViewModel()
        {
            Base = new ProjectData();
        }

        public ProjectDataViewModel(ProjectData baseModel)
        {
            Base = baseModel;
        }
    }
}