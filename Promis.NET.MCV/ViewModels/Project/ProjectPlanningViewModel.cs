﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Project;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Project
{
    public class ProjectPlanningViewModel : BaseViewModel, IBaseViewModel
    {
        public List<Activity> MainActivities { get; set; } = new List<Activity>();
        public string[] MonthsName { get; set; } = new string[12];
        public int MonthsCount { get; set; } = 0;
        public int StartYear { get; set; } = 0;
        public int StartMonth { get; set; } = 0;
    }
}