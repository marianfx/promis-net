﻿using Promis.NET.MCV.ViewModels.Interfaces;
using System;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Project
{
    public class ProjectCutoffViewModel : BaseViewModel, IBaseViewModel
    {
        public DateTime FirstDay { get; set; } = DateTime.Now;
        public DateTime LastDay { get; set; } = DateTime.Now;
        public List<DateTime> CutoffDays { get; set; } = new List<DateTime>();
    }
}