﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Project;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Project
{
	public class ProjectViewModel: BaseViewModel, IBaseViewModel
	{
        public Services.Models.Project.Project Base { get; set; }
        public List<Activity> MainActivities { get; set; }

        public ProjectViewModel()
        {
            Base = new Services.Models.Project.Project();
            MainActivities = new List<Activity>();
        }

        public ProjectViewModel(Services.Models.Project.Project baseModel): this()
        {
            Base = baseModel;
        }

        public ProjectViewModel(Services.Models.Project.Project baseModel, List<Activity> mainActivities): this()
        {
            Base = baseModel;
            MainActivities = mainActivities;
        }
    }
}