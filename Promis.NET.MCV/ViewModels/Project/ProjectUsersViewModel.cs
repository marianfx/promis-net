﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Account;
using Promis.NET.Services.Models.Project;
using Promis.NET.Utilities.Models;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Project
{
	public class ProjectUsersViewModel : BaseViewModel, IBaseViewModel
    {
        public PromisUser User { get; set; } = new PromisUser();
        public List<PromisUser> Users { get; set; } = new List<PromisUser>();
        public List<Module> Modules { get; set; } = new List<Module>();
        public List<Subcontractor> Subcontractors { get; set; } = new List<Subcontractor>();
        public List<Activity> Activities { get; set; } = new List<Activity>();
        public List<Country> Countries => Global.Countries;
    }
}