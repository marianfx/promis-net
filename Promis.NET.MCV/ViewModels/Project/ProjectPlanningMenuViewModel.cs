﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Project;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Project
{
    public class ProjectPlanningMenuViewModel : BaseViewModel, IBaseViewModel
    {
        public bool IsWithCurves { get; set; } = true;
        public bool IsBaselineVisible { get; set; } = true;
        public bool IsBaselineEditable { get; set; } = true;
        public bool IsPlanningEditable { get; set; } = true;
        public bool IsREditable { get; set; } = true;
        public bool IsAEditable { get; set; } = true;

        public Budget Budget { get; set; } = new Budget();
        public List<ActivityPlanned> Activities { get; set; } = new List<ActivityPlanned>();
    }
}