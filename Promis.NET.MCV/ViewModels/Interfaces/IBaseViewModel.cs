﻿using Promis.NET.Utilities.Exceptions;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Interfaces
{
    public interface IBaseViewModel
    {
        Envelope Envelope { get; set; }
        IEnumerable<string> GetValidationErrors();
    }
}
