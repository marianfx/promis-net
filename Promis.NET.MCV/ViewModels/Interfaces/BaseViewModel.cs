﻿using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Interfaces
{
    public class BaseViewModel : IBaseViewModel
    {
        public BaseViewModel()
        {

        }

        public Envelope Envelope { get; set; } = null;

        public virtual IEnumerable<string> GetValidationErrors()
        {
            return new List<string>();
        }
    }
}