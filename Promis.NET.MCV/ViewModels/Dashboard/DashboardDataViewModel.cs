﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Dashboard;

namespace Promis.NET.MCV.ViewModels.Dashboard
{
    public class DashboardDataViewModel : BaseViewModel, IBaseViewModel
    {
        public DashboardData Model { get; set; }

        public DashboardDataViewModel()
        {
            Model = new DashboardData();
        }

        public DashboardDataViewModel(DashboardData model)
        {
            Model = model;
        }
    }
}