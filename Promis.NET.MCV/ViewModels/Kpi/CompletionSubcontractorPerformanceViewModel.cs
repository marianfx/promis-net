﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Kpi;

namespace Promis.NET.MCV.ViewModels.Kpi
{
    public class CompletionSubcontractorPerformanceViewModel : BaseViewModel, IBaseViewModel
    {
        public KpiAnalysis AnalysisData { get; set; } = new KpiAnalysis();
    }
     
}