﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Kpi;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Kpi
{
    public class CompletionLoadedKpisViewModel : BaseViewModel, IBaseViewModel
    {
        public List<KpiData> Kpis { get; set; } = new List<KpiData>();

        public CompletionLoadedKpisViewModel() { }
    }
}