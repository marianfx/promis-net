﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Kpi;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Kpi
{
    public class CompletionKpiViewModel : BaseViewModel, IBaseViewModel
    {
        public List<KpiCategory> Categories { get; set; } = new List<KpiCategory>();

        public CompletionKpiViewModel() { }
    }
}