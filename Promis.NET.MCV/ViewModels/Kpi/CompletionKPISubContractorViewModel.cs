﻿
using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Kpi;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Kpi
{
    public class CompletionKPISubContractorViewModel : BaseViewModel, IBaseViewModel
    {
        public List<KPISubcontractors> Subcontractors { get; set; } = new List<KPISubcontractors>();        
    }
}