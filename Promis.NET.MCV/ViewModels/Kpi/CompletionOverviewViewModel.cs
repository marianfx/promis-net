﻿using Promis.NET.MCV.ViewModels.Interfaces;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Kpi
{
    public class CompletionOverviewViewModel : BaseViewModel, IBaseViewModel
    {
        public double PercentConstruction { get; set; } = 0.0;
        public double PercentPreCommissioning { get; set; } = 0.0;
        public double PercentCommissioning { get; set; } = 0.0;
        public double PercentLoopTests { get; set; } = 0.0;

        public IEnumerable<string> PunchlistLabels { get; set; }
        public IEnumerable<int> PunchlistValues { get; set; }

        public IEnumerable<string> ConstructionLabels { get; set; }
        public IEnumerable<double> ConstructionValues { get; set; }

        public IEnumerable<string> PreCommissioningLabels { get; set; }
        public IEnumerable<double> PreCommissioningValues { get; set; }

        public IEnumerable<string> CommissioningLabels { get; set; }
        public IEnumerable<double> CommissioningValues { get; set; }

        public CompletionOverviewViewModel() { }
    }
}