﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Activities;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Activities
{
    public class HomeViewModel : BaseViewModel, IBaseViewModel
    {
        public List<Inspection> Inspections { get; set; } = new List<Inspection>();
    }
}