﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Commissioning;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Activities
{
    public class ActivitiesISOViewModel : BaseViewModel, IBaseViewModel
    {
        public KeyValuePair<double, double> HydroPackagesCount { get; set; } = new KeyValuePair<double, double>();
        public List<string> PipingDesignList { get; set; } = new List<string>();
        public List<Datatables> GeographicalWBS { get; set; } = new List<Datatables>();
    }
}