﻿using System.Collections.Generic;
using Promis.NET.Services.Models.Activities;
using Promis.NET.MCV.ViewModels.Interfaces;

namespace Promis.NET.MCV.ViewModels.Activities
{
    public class SystemPunchListsManagementViewModel : BaseViewModel, IBaseViewModel
    {
        public List<PlBaseTables> Discipline { get; set; } = new List<PlBaseTables>();
        public List<PlBaseTables> Originator { get; set; } = new List<PlBaseTables>();
        public List<PlBaseTables> Category { get; set; } = new List<PlBaseTables>();
        public List<PlBaseTables> ActionBy { get; set; } = new List<PlBaseTables>();
        public List<string> Status { get; set; } = new List<string>();
        public List<PlBaseTables> Closer { get; set; } = new List<PlBaseTables>();
    }
}