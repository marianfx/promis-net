﻿using Promis.NET.Services.Models.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Promis.NET.MCV.ViewModels.Activities
{
    public class OutstandingModalViewModel
    {
        public List<PlBaseTables> Datatables { get; set; } = new List<PlBaseTables>();
    }
}