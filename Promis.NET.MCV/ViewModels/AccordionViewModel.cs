﻿using Promis.NET.MCV.ViewModels.Interfaces;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels
{
    public class AccordionViewModel: BaseViewModel, IBaseViewModel
    {
        public string Title { get; set; } = string.Empty;
        public string Identifier { get; set; } = "accordion";
        public string Description { get; set; } = string.Empty;
        public string PathToRetrieveElements { get; set; } = string.Empty;
        public string MethodToInvoke { get; set; } = string.Empty;
        public Dictionary<string, string> Elements { get; set; } = new Dictionary<string, string>();

        public AccordionViewModel() { }
    }
}