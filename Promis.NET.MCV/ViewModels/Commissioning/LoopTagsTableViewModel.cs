﻿

using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Commissioning;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Commissioning
{
    public class LoopTagsTableViewModel : BaseViewModel, IBaseViewModel
    {
        public Dictionary<string, List<SystemInstrumentsTags>> TagsTableInfo { get; set; } = new  Dictionary<string, List<SystemInstrumentsTags>>();
    }
}