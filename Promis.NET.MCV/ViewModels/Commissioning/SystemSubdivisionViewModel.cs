﻿using System.Collections.Generic;
using Promis.NET.Services.Models.Commissioning;


namespace Promis.NET.MCV.ViewModels.Commissioning
{
    public class SystemSubdivisionViewModel
    {
        public List<Datatables> Datatables { get; set; } = new List<Datatables>();
        public List<SystemModel> Systems { get; set; } = new List<SystemModel>();
        public string PrPc { get; set; } = "0";
        public string Tottags { get; set; } = "0";
        public string TotAss { get; set; } = "0";


    }
}