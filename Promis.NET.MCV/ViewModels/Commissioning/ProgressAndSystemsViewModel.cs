﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Commissioning;
using System.Collections.Generic;


namespace Promis.NET.MCV.ViewModels.Commissioning
{
    public class ProgressAndSystemsViewModel : BaseViewModel, IBaseViewModel
    {
        public Dictionary<string, DatatablesInfo> DataTablesInfo { get; set; } = new Dictionary<string, DatatablesInfo>();
    }
}