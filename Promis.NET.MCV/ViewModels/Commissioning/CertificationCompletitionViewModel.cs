﻿
using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Commissioning;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Commissioning
{
    public class CertificationCompletionViewModel : BaseViewModel, IBaseViewModel
    {
        public Dictionary<string, CommissioningCertificationCompletion>CertificationDataTablesInfo { get; set; } = new Dictionary<string, CommissioningCertificationCompletion>();
    }
}