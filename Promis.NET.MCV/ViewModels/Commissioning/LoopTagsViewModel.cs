﻿using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Commissioning
{
    public class LoopTagsViewModel
    {
        public List<string> Tags { get; set; } = new List<string>();
    }
}