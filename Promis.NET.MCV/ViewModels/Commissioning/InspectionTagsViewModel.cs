﻿using Promis.NET.MCV.ViewModels.Interfaces;
using System.Collections.Generic;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Models.Commissioning;


namespace Promis.NET.MCV.ViewModels.Commissioning
{
    public class InspectionTagsViewModel : BaseViewModel, IBaseViewModel
    {
        public List<Activity> MainActivities { get; set; } = new List<Activity>();
        public List<Datatables> Datatables { get; set; } = new List<Datatables>();

    }
}