﻿using Promis.NET.Services.Models.Commissioning;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Commissioning
{
    public class LoopTestsViewModel
    {
        public List<SystemModel> Systems { get; set; } = new List<SystemModel>();
    }
}