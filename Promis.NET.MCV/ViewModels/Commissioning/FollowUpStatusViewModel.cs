﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Commissioning;
using System.Collections.Generic;
namespace Promis.NET.MCV.ViewModels.Commissioning
{
    public class FollowUpStatusViewModel : BaseViewModel, IBaseViewModel
    {
        public List<CommissioningFollowUpStatus> Status { get; set; } = new List<CommissioningFollowUpStatus>();
        public string Subactivity { get; set; } = string.Empty;
        public string S_Description { get; set; } = string.Empty;
    }
}