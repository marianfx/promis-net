﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Project;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Progress
{
    public class QuantitiesTableViewModel: BaseViewModel, IBaseViewModel
    {
        public Dictionary<Activity, List<SubActivity>> Activities { get; set; } = new Dictionary<Activity, List<SubActivity>>();
    }
}