﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.MCV.ViewModels.Project;
using Promis.NET.Services.Models.Project;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Progress
{
    public class ProgressOverviewViewModel : ProjectPlanningViewModel, IBaseViewModel
    {
        public List<ActivityPlanned> Activities { get; set; } = new List<ActivityPlanned>();
    }
}