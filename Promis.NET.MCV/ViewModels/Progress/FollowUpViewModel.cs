﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.MCV.ViewModels.Project;
using Promis.NET.Services.Models.Project;
using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels.Progress
{
    public class FollowUpViewModel: ProjectPlanningViewModel, IBaseViewModel
    {
        public string ProgressTableName { get; set; } = string.Empty;
        public List<Area> Areas { get; set; } = new List<Area>();
    }
}