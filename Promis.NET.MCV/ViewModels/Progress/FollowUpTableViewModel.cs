﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.Services.Models.Project;
using System.Collections.Generic;
using System.Linq;

namespace Promis.NET.MCV.ViewModels.Progress
{
    public class FollowUpTableViewModel : BaseViewModel, IBaseViewModel
    {
        public string ProgressTableName { get; set; } = string.Empty;
        public List<SubActivityProgress> Subactivities { get; set; } = new List<SubActivityProgress>();

        public string[] InspectionArray { get; set; } = new string[12];
        public string[] ProgressArray { get; set; } = new string[12];
        public string[] LpArray { get; set; } = new string[12];
        public string[] AnglesArray { get; set; } = new string[12];
        public string[] BackgroundsArray { get; set; } = new string[12];
        public int FinalIndex { get; set; } = 0;

        public FollowUpTableViewModel() { }

        public FollowUpTableViewModel(IEnumerable<SubActivityProgress> subactivities)
        {
            SetSubactivities(subactivities);
        }

        public void SetSubactivities(IEnumerable<SubActivityProgress> subactivities)
        {
            Subactivities = subactivities.ToList();
            var lastStep = false;

            for (var i = 0; i < 12; i++)
            {
                InspectionArray[i] = "";
                ProgressArray[i] = "";
                LpArray[i] = "";
                AnglesArray[i] = "";
                BackgroundsArray[i] = "";
                if (i < Subactivities.Count)
                {
                    var subAct = Subactivities[i];
                    InspectionArray[i] = subAct.Inspection;
                    ProgressArray[i] = subAct.Progress.ToString();
                    LpArray[i] = subAct.Lp;
                    AnglesArray[i] = "<span class='ti-angle-double-down'></span>";
                    if (!string.IsNullOrWhiteSpace(subAct.Lp))
                        BackgroundsArray[i] = "background-color:green;";
                }

                if (i == Subactivities.Count - 1 && !lastStep)
                {
                    BackgroundsArray[i] = "background-color:red;";
                    FinalIndex = i;
                    lastStep = true;
                }
            }
        }
    }
}