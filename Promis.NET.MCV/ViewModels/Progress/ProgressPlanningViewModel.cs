﻿using Promis.NET.MCV.ViewModels.Interfaces;
using Promis.NET.MCV.ViewModels.Project;

namespace Promis.NET.MCV.ViewModels.Progress
{
    public class ProgressPlanningViewModel : ProjectPlanningViewModel, IBaseViewModel
    {
        public bool IsReschedule { get; set; } = false;
        public bool IsBaselineVisible { get; set; } = true;
    }
}