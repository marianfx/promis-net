﻿using System.Collections.Generic;

namespace Promis.NET.MCV.ViewModels
{
    public class ModuleLayoutViewModel
    {
        public string ModuleTitle { get; set; } = "";

        public string ModuleVersion { get; set; } = "";

        /// <summary>
        /// This should hold the key-value pairs for page description. The keys should be unique identifier for pages "of-this-format" and values should be "Titles for The Pages".
        /// The first entry should be 'home'.
        /// </summary>
        public Dictionary<string, string> PageDescriptorsContainer { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// This should hold the key-value pairs for page description, for the menu items that have dropdown selectors. The Key should be the equivalent of the key from the PageDescriptorsContainer dictionary (and the title displayed should be there), and the values should be the dropdown menu items. 
        /// </summary>
        public Dictionary<string, Dictionary<string, string>> PageDescriptorsForDropdowns { get; set; } = new Dictionary<string, Dictionary<string, string>>();

        /// <summary>
        /// Represents the id for the HTML element that will hold all the elements of the submodules (where submodule partial views will be loaded). Without the '#'.
        /// </summary>
        public string ContentContainerId { get; set; } = "module-container";

        public string WorkingPath { get; set; } = "mymodule";

        /// <summary>
        /// Define a list of stylesheets that should be available in all the module (usually there is just one).
        /// They should be strings, representing the path relative to 'assets/css' directory and starting with '/'.
        /// Example: '/pages/project-planning.css'.
        /// </summary>
        public List<string> StyleSheetsList { get; set; } = new List<string>();

        /// <summary>
        /// Define a list of Javascript elements that should be available in all the module (usually there is just one).
        /// They should be strings, representing the path relative to 'assets/js' directory and starting with '/'.
        /// Example: '/pages/project-planning.js'.
        /// </summary>
        public List<string> ScriptsList { get; set; } = new List<string>();
    }
}
