﻿using Promis.NET.MCV.ViewModels.Interfaces;

namespace Promis.NET.MCV.ViewModels
{
    public class PdfViewerViewModel: BaseViewModel, IBaseViewModel
    {
        public string PageMode { get; set; } = "single";
        public string DocUrl { get; set; } = string.Empty;
        public string Title { get; set; } = "View PDF";
    }
}