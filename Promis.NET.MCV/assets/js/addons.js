﻿// #### Theming Utilities ####
function switch_theme(theme) {

    // add loading
    var body = document.getElementsByTagName("body")[0];
    if(body.className.indexOf("loading") < 0)
        body.className += " loading loading-body";

    // change theme
    document.querySelector("#theme").setAttribute("href", "/assets/css/themes/" + theme + ".css");
    if (document.querySelector("*[data-theme='" + theme + "']") != undefined) {
        var logo = document.querySelector("*[data-theme='" + theme + "']").getAttribute("data-logo");
        document.querySelector("#logo").setAttribute("src", '../assets/img/' + logo);
    }

    try {
        GLOBAL_DATA.USER_THEME = theme;
        configure_chart(theme);
    }
    catch (e) {
        console.log(GLOBAL_TRANSLATIONS_FOR_LAYOUT.ChartsNotAvailableToChangeTheme);
    }

    // remove loading
    setTimeout(function () {
        body.className = body.className.replace(' loading loading-body', '');
    }, 300);
}

function switch_theme_without_css(theme){
    if (document.querySelector("*[data-theme='" + theme + "']") != undefined) {
        var logo = document.querySelector("*[data-theme='" + theme + "']").getAttribute("data-logo");
        document.querySelector("#logo").setAttribute("src", '../assets/img/' + logo);
    }

    try {
        GLOBAL_DATA.USER_THEME = theme;
        configure_chart(theme);
    }
    catch (e) {
        console.log(GLOBAL_TRANSLATIONS_FOR_LAYOUT.ChartsNotAvailableToChangeTheme);
    }
}

window.chartColors = {
    red: 'rgb(255, 98, 98)',
    orange: 'rgb(240, 173, 78)',
    yellow: 'rgb(255, 184, 3)',
    green: 'rgb(44, 196, 205)',
    blue: 'rgb(108, 139, 239)',
    purple: 'rgb(97, 61, 124)',
    grey: 'rgb(231,233,237)'
};

function configure_chart(theme) {
    Chart.defaults.global.defaultFontFamily = "light";
    Chart.defaults.global.defaultFontSize = 12;
    switch (theme) {
        case "light":
            Chart.defaults.global.defaultFontColor = "#222";
            break;
        default:
            Chart.defaults.global.defaultFontColor = "#fff";
            break;
    }

    Chart.defaults.global.animation.duration = 5000;
    Chart.defaults.global.responsive = true;
    Chart.defaults.global.maintainAspectRatio = false;
    Chart.defaults.global.title.display = false;
    Chart.defaults.global.tooltips.mode = "label";
    Chart.defaults.global.hover.mode = "dataset";
}

function makeCurrentMonthYellow() {
    // visuals
    var currentMonth = new Date().getMonth() + 1;
    var currentYear = new Date().getFullYear();

    var currentYearOnTable = $("[data-month='" + currentMonth + "'][data-year='" + currentYear + "']");
    if (!currentYearOnTable)//means old project, not having in it the current month + year => select the last year
        $(".table-progress-cell:last-child").addClass("table-progress-cell-current");
    else
        currentYearOnTable.addClass("table-progress-cell-current");
}
// #### END Theming Utilities ####

function validate(e) {
    var ev = e || window.event;
    var key = ev.keyCode || ev.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]/;
    if (!regex.test(key)) {
        ev.returnValue = false;
        if (ev.preventDefault) ev.preventDefault();
    }
}

function validateEnvelopeError(envelope) {
    // check for errors
    console.log(envelope);
    if (!envelope || envelope == '' || envelope.errors == null)
        return;

    if (window.$) {
        if ($.isReady)
            defaultErrorFunction(parseErrorObject(envelope.errors));
        else {
            $(document).ready(function () {
                defaultErrorFunction(parseErrorObject(envelope.errors));
            });
        }
        return;
    }

    // fallback to non-jq handling (wait for jquery)
    document.addEventListener("DOMContentLoaded", function (event) {
        if ($.isReady)
            defaultErrorFunction(parseErrorObject(envelope.errors));
        else {
            $(document).ready(function () {
                defaultErrorFunction(parseErrorObject(envelope.errors));
            });
        }
    });
}

function addPageUnderConstructionMarker(container) {
    var pageUnderConstrString = `
            <div id="loading">
                <div class="load-wrapper2">
                    <div class="sk-three-bounce">
                        <div class="sk-child sk-bounce1"></div>
                        <div class="sk-child sk-bounce2"></div>
                        <div class="sk-child sk-bounce3"></div>
                    </div>
                    <h3 class="center-align">`+ GLOBAL_TRANSLATIONS_FOR_LAYOUT.PageUnderConstruction +`</h3>
                </div>
            </div>`;

    $(container).parent().prepend(pageUnderConstrString);
    $(container).hide();
    setTimeout(function () {
        $("#loading").hide();
        $(container).show();
    }, 5000);
}

// ### Modal Utilities
function open_modal(url, options) {
    var defaults = {
        title: GLOBAL_TRANSLATIONS_FOR_LAYOUT.Modal,
        elementsToAddLoading: [".modal-content"],
        next: function () { },
        version: GLOBAL_DATA.MODAL_VERSIONS.PERSONAL_INFORMATION
    };

    var opts = $.extend({}, defaults, options);//merge properties, options will override defaults

    if (!_.isArray(opts.elementsToAddLoading))
        opts.elementsToAddLoading = [".modal-content"];

    $('#modal-container').modal();
    $('#modal-container h6.modal-title').html(opts.title);
    $('#modal-container #modal-version').html(opts.version);

    callServer(url, {
        successFunc: function (viewData) {
            $('#modal-container .modal-body').html(viewData);
            if (_.isFunction(opts.next))
                opts.next(viewData);
        },
        elementsToAddLoading: opts.elementsToAddLoading
    });
};

function open_modal_normal(options) {
    var defaults = {
        title: GLOBAL_TRANSLATIONS_FOR_LAYOUT.Modal,
        content: "",
        footerButton: "",
        footerHtml: null,
        version: GLOBAL_DATA.MODAL_VERSIONS.PERSONAL_INFORMATION
    };

    var opts = $.extend({}, defaults, options);//merge properties, options will override defaults


    $('#modal-container').modal();
    $('#modal-container h6.modal-title').html(opts.title);
    $('#modal-container #modal-version').html(opts.version);
    if (opts.footerHtml != null)
        $('#modal-container #modal-footer-content').html(opts.footerHtml);
    else {
        var footer = `<button id="run-operation-button" type='button' class='btn btn-sm btn-primary pull-right'>` + opts.footerButton + `</button>`;
        $('#modal-container #modal-footer-content').html(footer);
    }
    $('#modal-container .modal-body').html(opts.content);
};

function close_modal() {
    $('#modal-container .modal-body').html("");
    $("#modal-container").modal('toggle');
}


// ### CSS Utilities 
function setElementAsInvalid(element) {
    $(element).addClass("invalid");
    //$(element).focus();
}

function setElementAsValid(element) {
    $(element).removeClass("invalid");
    //$(element).focus();
}

function changeIcon(domElement, srcImage) {
    var img = new Image();
    img.onload = function () {
        domElement.src = this.src;
    };
    img.src = srcImage;
}

/*
   Loads the specified resource(CSS / JS) into the page if not already loaded. Please use resourceType = 'css' or resourceType = 'js'
*/
function loadResourceIfNotAlreadyLoaded(resourcePath, resourceType) {
    if (resourceType == "css") {
        var ss = document.styleSheets;
        for (var i = 0, max = ss.length; i < max; i++) {
            if (ss[i].href == resourcePath)
                return;
        }
        var link = document.createElement("link");
        link.rel = "stylesheet";
        link.href = resourcePath;
        document.getElementsByTagName("head")[0].appendChild(link);
        return;
    }

    if (resourceType == "js") {
        var ss = document.scripts;
        for (var i = 0, max = ss.length; i < max; i++) {
            if (ss[i].src == resourcePath)
                return;
        }
        var script = document.createElement("script");
        script.src = resourcePath;
        console.log(GLOBAL_TRANSLATIONS_FOR_LAYOUT.Creating, script);
        document.getElementsByTagName("body")[0].appendChild(script);
        return;
    }
}

// END CSS Utilities

// Displays this container as loading (with the white loading spins).
function setContainerAsLoading(container) {
    //1. .html(loading)
    var htmlload = `<div style="margin-top:200px;overflow:hidden">
                    <div class="load-wrapper2">
                        <div class="sk-three-bounce">
                            <div class="sk-child sk-bounce1"></div>
                            <div class="sk-child sk-bounce2"></div>
                            <div class="sk-child sk-bounce3"></div>
                        </div>
                    </div>
                </div>`;
    $(container).html(htmlload);
}


// #### Virtual Navigation Utilities ####

//FROM: https://www.sitepoint.com/get-url-parameters-with-javascript/
// use: getAllUrlParams().paramname
function getAllUrlParams(url) {

    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // we'll store the parameters here
    var obj = {};

    // if query string exists
    if (queryString) {

        // stuff after # is not part of query string, so get rid of it
        queryString = queryString.split('#')[0];

        // split our query string into its component parts
        var arr = queryString.split('&');

        for (var i = 0; i < arr.length; i++) {
            // separate the keys and the values
            var a = arr[i].split('=');

            // in case params look like: list[]=thing1&list[]=thing2
            var paramNum = undefined;
            var paramName = a[0].replace(/\[\d*\]/, function (v) {
                paramNum = v.slice(1, -1);
                return '';
            });

            // set parameter value (use 'true' if empty)
            var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

            // (optional) keep case consistent
            paramName = paramName.toLowerCase();
            paramValue = decodeURIComponent(paramValue);

            // if parameter name already exists
            if (obj[paramName]) {
                // convert value to array (if still string)
                if (typeof obj[paramName] === 'string') {
                    obj[paramName] = [obj[paramName]];
                }
                // if no array index number specified...
                if (typeof paramNum === 'undefined') {
                    // put the value on the end of the array
                    obj[paramName].push(paramValue);
                }
                // if array index number specified...
                else {
                    // put the value at that index number
                    obj[paramName][paramNum] = paramValue;
                }
            }
            // if param name doesn't exist yet, set it
            else {
                obj[paramName] = paramValue;
            }
        }
    }

    return obj;
}


// ### Forms Utilities ###
function extractObjectFromForm(fieldContainer) {
    var output = {};
    _.forEach($(fieldContainer).find(":input"), function (input, index) {
        if ($(input).is("input:text") || $(input).is("textarea") || /*$(input).is("input:hidden") || */ $(input).is("input:password") || $(input).is("select") || $(input).attr('type') == 'email' || $(input).attr('type') == 'tel')
            output[input.id] = $(input).val();
        else if ($(input).is("input:checkbox"))
            output[input.id] = $(input).is(":checked") ? true : false;
        else if ($(input).is("input:radio") && $(input).is(":checked"))
            output[$(input).attr("name")] = $(input).val();
    });
    
    return output;
}

function extractObjectFromFormModal(fieldContainer) {
    var output = {};
    _.forEach($(fieldContainer).find(":input"), function (input, index) {
        var fieldName = input.id.endsWith("modal") ? input.id.substr(0, input.id.indexOf("modal")) : input.id;
        var nameVal = $(input).attr("name");
        var altFieldName = (nameVal && nameVal.endsWith("modal")) ? nameVal.substr(0, nameVal.indexOf("modal")) : nameVal;
        if ($(input).is("input:text") || $(input).is("textarea") || /*$(input).is("input:hidden") || */ $(input).is("input:password") || $(input).is("select") || $(input).attr('type') == 'email' || $(input).attr('type') == 'tel')
            output[fieldName] = $(input).val();
        else if ($(input).is("input:checkbox"))
            output[fieldName] = $(input).is(":checked") ? true : false;
        else if ($(input).is("input:radio") && $(input).is(":checked"))
            output[altFieldName] = $(input).val();
    });

    return output;
}

function transformObjectToKeyValueArray(data) {
    return _.map(_.toPairs(data), function (d) {
        return _.fromPairs([d])
    });
}

function transformObjectToDataCellArray(data) {
    return _.transform(data, function (result, value, key) {
        result.push({
            ColumnName: key,
            Value: value
        });
    }, []);
}
// ### END Forms Utilities ###


// Mathematical Utilities Start
function getMonthsDistribution(numberOfMonths){
    var vals = [];
    var P = numberOfMonths - 2;
    var N = P / 10 + P / 100;
    if (N == 0) {
        vals = [50, 100];
        return vals;
    }

    for (var i = -P / 2; i <= P / 2; i++) {
        var value = (1 / (1 + (Math.pow(Math.E, -i / N))));
        value = Number((value * 100).toFixed(2));
        vals.push(value);
    }
    vals.push(100);
    return vals;
}

function getMonthsFromStartUntilEnd(startDate, endDate) {
    if (startDate == undefined || endDate == undefined || startDate == '' || endDate == '' || startDate.indexOf("-") < 0 || endDate.indexOf("-") < 0)
        return 0;

    var monthFilterFunc = function (o) {
        return o.length <= 2;
    };
    var yearFilterFunc = function (o) {
        return o.length == 4;
    };

    var firstMonth = _.filter(startDate.split("-"), monthFilterFunc);
    var firstYear = _.filter(startDate.split("-"), yearFilterFunc);
    var secondMonth = _.filter(endDate.split("-"), monthFilterFunc);
    var secondYear = _.filter(endDate.split("-"), yearFilterFunc);
    try {
        return (parseInt(secondYear - firstYear) * 12 + parseInt(secondMonth) - parseInt(firstMonth) + 1);
    }
    catch (e) {
        return 0;
    }
}
// End