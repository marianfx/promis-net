﻿
/**
 * Initializes a new Advanced Filters object. Options parameter is an object which can have the following properties:
    - tableName: the name of the table these filters apply to
    - startFrom: the index to start setting advfilters[index] array; by default it is 0, but when having another filtres, it is good to set it otherwise it will ignore the previous filters.
    - onResetFilters: the method that should be called when the reset button is clicked
    - callbackFunction: function(advancedFiltersString) {} The method that is invoked after clicking 'Apply' on the advanced filters modal. It is called with a parameter that represents the string with all the advanced filters as selected in the form (datatables.net ready)
    Notice that in the VIEW, a JS variable names columnsKeeperArray must be declared, in the form of an array, and columnsKeeperArra['tablename'] will be the list of columns for the loadded table.
 */
function PromisAdvancedFilters(options) {
    var defaults = {
        tableName: "",
        columns: [],
        startFrom: 0,
        onResetFilters: function () {},
        callbackFunction: function (advancedFiltersString) { }
    };
    var opts = $.extend({}, defaults, options);//merge properties, options will override defaults
    var that = this;
    // load table name
    if (!opts.tableName || opts.tableName == "")
        return displayNotify(GLOBAL_TRANSLATIONS.ErrorTableNameNotSpecifiedWhenInitializingTable);
    this.tableName = opts.tableName.toLowerCase();   
    this.columns = [];
    this.columnNames = [];

    // save column operations object; it is used to reload this
    this.column_operations = [];

    // save the advanced query string;
    this.advFiltersQueryString = "";

    // the method that sets the columns
    this.setColumns = function (newColumnList) {
        if (!_.isArray(newColumnList))
            return console.log(GLOBAL_TRANSLATIONS.CannotSetNewColumnListNotAValidArray);
        // remove those for joins (contain dot) or those with empty names
        var tempColumns1 = _.filter(newColumnList, function (col) { return (_.isString(col.ColumnName) && col.ColumnName.trim() != "" && col.ColumnName.indexOf(".") < 0); });
        // set as lowercase all and get just the name
        var tempColumns2 = _.map(tempColumns1, function (obj) { return obj.ColumnName.toLowerCase(); });
        that.columns = tempColumns2;
    }
    this.setColumns(opts.columns);

    // the method that opens the modal which displays the filtering
    this.openModalWithFilters = function () {
        $(".modal-content").css("width", "600px");
        var url = GLOBAL_DATA.APP_ROOT + "/engineering/tables/" + GLOBAL_DATA.PROJECT_DATABASE + "/" + opts.tableName + "/filters";

        open_modal(url, {
            title: GLOBAL_TRANSLATIONS.AdvancedDataFilterForEachAttributesAndMode,
            footerHtml: '',
            version: GLOBAL_DATA.MODAL_VERSIONS.ENGINEERING_ADVANCED_SEARCH,
            next: function (v) {
                that.initContentForFilters();
            }
        });
    }

    // the method that initializes the content of the modal, after it is opened
    this.initContentForFilters = function () {
        // put the right footer
        var footer = `<div class='pull-right' style='padding-right: 20px'><button type='button' class='btn btn_sm btn-success ' style='margin-right: 20px; padding: 5px; font-size: 12px' id="applyFilters">` + GLOBAL_TRANSLATIONS.ApplyFilters + `</button>`;
        footer = footer + `<button type='button' class='btn btn_sm btn-danger ' style='padding: 5px; font-size: 12px' id="resetFilter">` + GLOBAL_TRANSLATIONS.ResetFilters + `</button ></div >`;
        $('#modal-container #modal-footer-content').html(footer);

        // set and update column list
        console.log(GLOBAL_DATA.TRANSLATIONS.FromTheArray, columnsKeeperArray, that.columns, opts.tableName);
        if ((!columnsKeeperArray || !columnsKeeperArray.hasOwnProperty(opts.tableName) || !_.isArray(columnsKeeperArray[opts.tableName]) || columnsKeeperArray[opts.tableName].length == 0))
            return displayNotify(GLOBAL_DATA.TRANSLATIONS.TheListOfColumnsSpecifiedIsInvalidCannotInitializeTheAdvancedFiltersPlugin);

        var tempColumns = _.filter(columnsKeeperArray[opts.tableName], function (obj) { return _.indexOf(that.columns, obj.ColumnName) >= 0; });
        that.setColumns(tempColumns);
        
        _.forEach(columnsKeeperArray[opts.tableName], function (column) {
            var name = column.ColumnName ? column.ColumnName : column;
            name = name.toLowerCase();
            if (_.indexOf(that.columns, name) < 0)
                $('tr*[data-for="' + name + '"]').remove();

        });

        // add events
        $("#resetFilter").click(that.onClickResetFilter);
        $("#applyFilters").click(that.onClickApplyFilter);

        // reload old fitlers if available
        that.rememberColumnsFilters();
    }

    // when clicking reset filters: reloads the page with the table when clicking reset
    this.onClickResetFilter = function () {
        that.advFiltersQueryString = "";
        that.column_operations = [];
        if (_.isFunction(opts.onResetFilters))
            opts.onResetFilters();
        close_modal();
    }

    // the method that executes when clicking Apply
    this.onClickApplyFilter = function () {
        // get data
        var arrayData = that.getClickedFilter();
        //console.log(arrayData);
        //console.log(that.advFiltersQueryString);

        //pass next everything
        opts.callbackFunction(that.advFiltersQueryString);

        //close modal
        close_modal();
    }

    // method that gets the advanced filters string (format: '?advfilters[x].leftoperand=column&advfilters[x].rightoperand=val&advfilters[x].operator=eq')
    this.getClickedFilter = function() {

        that.column_operations = [];
        that.advFiltersQueryString = "";
        var addedCount = (_.isFinite(opts.startFrom) && opts.startFrom >= 0 ? opts.startFrom : 0);
        
        for (var a = 0; a < that.columns.length; a++) {
            var colName = that.columns[a];

            if (!$("#adv-ck-" + colName).is(":checked"))
                continue;
        
            var options = document.getElementsByName("adv-options-" + colName);
            var option_value;

            for (var i = 0; i < options.length; i++) {
                if ($(options[i]).is(":checked")) {
                    option_value = $(options[i]).val();
                    var columnValue = $("#adv-" + colName).val();
                    that.advFiltersQueryString += "&advfilters[" + addedCount + "][leftoperand]=" + encodeURIComponent(colName) +
                        "&advfilters[" + addedCount + "][rightoperand]=" + encodeURIComponent(columnValue) +
                        "&advfilters[" + addedCount + "][operator]=" + encodeURIComponent(option_value);
                    addedCount++;
                    that.column_operations.push({
                        LeftOperand: colName,
                        RightOperand: columnValue,
                        Operator: option_value
                    });
                }
            }
            that.advFiltersQueryString = "?" + that.advFiltersQueryString.substr(1);
        }
        //console.log("Filter " + advFiltersQueryString);
        //console.log(that.column_operations);
        return that.column_operations;
    }

    this.rememberColumnsFilters = function () {
        //console.log("Started remembering");
        //console.log("Columns", opts.columns);
        //console.log("Column operations", that.column_operations);

        for (var a = 0; a < that.columns.length; a++) {
            var colName = that.columns[a];
            var object = that.column_operations;

            for (var i = 0; i < object.length; i++) {
                if (object[i].LeftOperand != colName)
                    continue;

                // set as checked, put value
                $("#adv-ck-" + colName).prop('checked', true);
                $("#adv-" + colName).val(object[i].RightOperand);

                // add checked option
                var operation = object[i].Operator;
                var options = document.getElementsByName("adv-options-" + colName);
                $(options[0]).parent("label").removeClass("active focus");

                for (var j = 0; j < options.length; j++) {
                    if ($(options[j]).val() == operation) {
                        $(options[j].parentElement).addClass("active focus");
                        $(options[j]).prop("checked", true);
                        break;
                    }
                }
            }
        }
    }
}