﻿/**
 * This is used to custom render data-tables on pages (assume that you already have the HTML tags for the tables); Use the options to customize.
 * This works well with _TableData.cshtml (HTML Template).
   Use .reinitDataTable(url) method to trigger data refresh. url can be left null if you do not want to change the source of data.
 * Options is an object. Inside it you can specify:
    - hasColumnToggle: boolean; Does the table display a column Dropdown that allows to show or hide columns?
    - hasAdvancedFilter: boolean; Does the table display a button that will load an advanced filter selector for the table? If so, you will have to define the method that defines the event on this button
    - hasActions: boolean; Does the tablle have Add / AddSimilar / Update / Delete actions?
    - hasImportExport: boolean: Does the table have Import / Export / Export Template Dropdown?
    - hasAjaxDataLoad: boolean; If true, it means the table with retreave data from an endpoint with AJAX (the ajaxURL and columnList must be specified); otherwise, it means all the rows are available at initialize time (in the HTML CODE specified)
    - hasOrdering: indicates if columns can be ordered

    - tableName: specify the name of the table (the main table, the identifier); if this is not specified, it will not work
    - ajaxUrl: string; specify the FULL URL where the table will send requests to get data; ignored if hasAjaxDataLoad is false
    - advancedFiltersString: string; specify advanced filters, in datatables.net format. Must start with '?'
    - columnList: object {ColumnName: string, Value: object }; specify the column list; ignored if hasAjaxDataLoad is false,
    - rowsPerPage: number; Represents the number of rows displayed per page in the table; if not specified, default is 100
    - marginFromElements: number; represents the margin between the top of the page and the beginning of the table
    - minHeight: five it only if you want the table to have a minimum height
    - fontSize: give it only if you want to change the font size for rows. default is 14px

    - onRowSelect: function(row) {}; function that executes everytime a row is selected; use 'row' parameter to get the row data / id etc.
    - onDraw: function(){} ; triggered after the table finished drawing itself
    - onDataPreprocessing: function(d) {}; function that is executed before a request is sent to the server; can be used to modify the request sent, to ignore columns, remove ordering, do advanced stuff with columns etc. 'd' is the full object described in datatables.net as the REQUEST object.
    - specialRenderingObject: [],; an ARRAY that contains objects of type {'aTargets' : [], 'mData': "", "mRender": function(data, type, full){} }; Used to do special rendering of cells.
                                        - aTargets is an array with indexes of columns that this rendering method will apply to.
                                        - mData is the column name / title
                                        - mRender is the function that MUST RETURN a string (containig or not HTML code and data, fully custom) with the element rendered accordingly. data is the current data and full is the data for the whole row.

    - onClickAddSimilar: Set this method to a method  that will execute when clicking on the 'Add Similar' button; is valid only if 'hasActions' is true; 'this' object will be the clicked element
    - onClickAdd: Set this method to a method  that will execute when clicking on the 'Add' button; is valid only if 'hasActions' is true; 'this' object will be the clicked element
    - onClickUpdate: Set this method to a method  that will execute when clicking on the 'Update' button; is valid only if 'hasActions' is true; 'this' object will be the clicked element
    - onClickDelete: Set this method to a method  that will execute when clicking on the 'Delete' button; is valid only if 'hasActions' is true; 'this' object will be the clicked element
     OBS: In the HTML, the buttons must have the IDs, in order: adds, add, edit, canc

     - onClickExport: Set this method to a method  that will execute when clicking on the 'Export' button; is valid only if 'hasImportExport' is true; 'this' object will be the clicked element
     - onClickExportTemplate: Set this method to a method  that will execute when clicking on the 'Export Template' button; is valid only if 'hasImportExport' is true; 'this' object will be the clicked element
     - onClickImport: Set this method to a method  that will execute when clicking on the 'Import' button; is valid only if 'hasImportExport' is true; 'this' object will be the clicked element
     OBS: In the HTML, the buttons must have the IDs, in order: exportExcel, exportExcelTemplate, importExcel

     - onClickAdvancedFilters: Set this to a method that will execute when clicking the Advanced Filters button; is valid only of 'hasAdvancedFilters' is true; 'this' object will be the clicked element
 */
function PromisDataTable(options) {
    var defaults = {
        // booleans
        hasColumnToggle: true,
        hasAdvancedFilter: true,
        advancedFiltersTitle: GLOBAL_TRANSLATIONS.AdvancedDataFilter,
        hasActions: true,
        hasImportExport: true,
        hasAjaxDataLoad: true,
        hasOrdering: true,
        hasScrollCollapse: true,
        hasScrollX: true,
        isResponsive: false,
        hasRowSelect: true,
        isStriped: false,

        // table data
        tableName: "",
        ajaxUrl: "",
        advancedFiltersString: "",
        columnList: [],
        rowsPerPage: 100,
        marginFromElements: 180,
        minHeight: null,
        fontSize: 14,

        // events actions
        onRowSelect: function (row) { },
        onDraw: null,
        onDataPreprocessing: function (d) { },
        specialRenderingObject: [],

        // actions
        onClickAddSimilar: function () { },
        onClickAdd: function () { },
        onClickUpdate: function () { },
        onClickDelete: function () { },

        // events import/export + adv filters
        onClickExport: function () { },
        onClickExportTemplate: function () { },
        onClickImport: function () { },
        onClickAdvancedFilters: function () { }
    };
    var opts = $.extend({}, defaults, options);//merge properties, options will override defaults
    var that = this;

    // load table name
    if (!opts.tableName || opts.tableName == "")
        return displayNotify(GLOBAL_TRANSLATIONS.ErrorTableNameNotSpecifiedWhenInitializingTable);
    this.tableName = opts.tableName.toLowerCase();   
    this.url = opts.ajaxUrl;
    this.filters = opts.advancedFiltersString;

    // the table reference
    this.table = null;

    // the generated table id
    this.tableId = '#promisdt-' + that.tableName;

    // set columns *objects* and column names
    this.columns = opts.columnList;
    this.columnNames = _.map(opts.columnList, function (obj) { return obj.ColumnName; });

    // to return the params
    this.getParams = function () {
        return that.table.ajax.params();
    }

    // to return url
    this.getUrl = function () {
        return that.table.ajax.url();
    }

    // method that initializes tables with eventual events on buttons
    this.initContentForTable = function () {
        // init and display table
        that.initDataTable();

        // click on action buttons
        if (opts.hasActions == true && $('#add-' + that.tableName).get().length == 0)
            opts.hasActions = false;

        if (opts.hasActions) {
            $('#adds-' + that.tableName).click(opts.onClickAddSimilar);
            $('#add-' + that.tableName).click(opts.onClickAdd);
            $('#edit-' + that.tableName).click(opts.onClickUpdate);
            $('#canc-' + that.tableName).click(opts.onClickDelete);
        }

        //click on IMPORT or EXPORT. Works thanks to the use of the import-export.js file
        if (opts.hasImportExport) {
            $('#exportExcel-' + that.tableName).click(opts.onClickExport);
            $('#exportExcelTemplate-' + that.tableName).click(opts.onClickExportTemplate);

            if($('#importExcel-' + that.tableName).get().length > 0)
                $('#importExcel-' + that.tableName).click(opts.onClickImport);
        }

        // COLUMNS Visibility
        if (opts.hasColumnToggle) {
            that.addColumnsToggle();

            $(that.tableId + '-container .dropdown-menu').click(function (event) {
                event.stopPropagation();
            });

            $(that.tableId + '-container .toggle-vis').on('click', function (e) {
                //e.preventDefault();
                var column = that.table.column($(this).attr('data-column'));
                column.visible(!column.visible());
            });
        }

        // ADVANCED FILTERS
        if (opts.hasAdvancedFilter) {
            that.addAdvancedFilters();
        }
    }

    // adjust columns on menu hide (special case of resize)

    $('body').bind("menuclose-changed", function (e) {
        if (that.table)
            that.table.columns.adjust();
    });

    // on resize
    this.onResize = function () {
        var containerHeight = $('body').innerHeight() - opts.marginFromElements;
        if (_.isFinite(opts.minHeight) && containerHeight < opts.minHeight)
            containerHeight = opts.minHeight;
        var contentHeight = containerHeight - 150;
        console.log("Hmmm", opts.minHeight, _.isFinite(opts.minHeight), opts.tableName, containerHeight, contentHeight);
        $(that.tableId + '-container').css("height", containerHeight + "px");
        $(that.tableId + '-container .dataTables_scrollBody').css("height", contentHeight + "px");
        $(that.tableId + '-container .dataTables_scrollBody').css("max-height", contentHeight + "px");

        if(that.table)
            that.table.columns.adjust();
    }
    
    //extension to change data source
    $.fn.dataTableExt.oApi.changeAjaxSource = function (oSettings, sNewSource) {
        if (typeof sNewSource != 'undefined' && sNewSource != null) {
            oSettings.sAjaxSource = sNewSource;
        }
        this.fnDraw();
    }

    // suppress warnings
    $.fn.dataTable.ext.errMode = 'none';
    $(that.tableId).on('error.dt', function (e, settings, techNote, message) {
        console.log(GLOBAL_TRANSLATIONS.AnErrorHasBeenReportedByDatatables, message);
    });

    //method that initializes the actual data table (using DataTables)
    this.initDataTable = function() {
        // 2/. assign onResize
        $(window).resize(that.onResize);
        var containerHeight = $('body').innerHeight() - opts.marginFromElements;
        if (_.isFinite(opts.minHeight) && containerHeight < opts.minHeight)
            containerHeight = opts.minHeight;
        var contentHeight = containerHeight - 150;
        that.onResize();
        
        //console.log(that.tableName);
        if (_.isFinite(opts.fontSize))
            $(that.tableId + '-container .promisdt').css("font-size", opts.fontSize + "px");

        
        var tableOptions = {
            "serverSide": false,
            "pageLength": (_.isFinite(opts.rowsPerPage) ? opts.rowsPerPage : 100),
            "ordering": opts.hasOrdering,
            "paging": true,
            "searching": true,
            "scrollY": contentHeight + 'px',
            "scrollX": opts.hasScrollX,
            "scrollCollapse": opts.hasScrollCollapse,
            "responsive": opts.isResponsive,
            "sPaginationType": 'full_numbers',
            "oLanguage": {
                "oPaginate": {
                    "sFirst": '<<',
                    "sPrevious": '<',
                    "sNext": '>',
                    "sLast": '>>'
                }
            },
            "select": opts.hasRowSelect
        };

        if (_.isArray(opts.specialRenderingObject))
            tableOptions.aoColumnDefs = opts.specialRenderingObject;

        if (opts.hasAjaxDataLoad) {
            if (!_.isArray(opts.columnList) || opts.columnList.length <= 0)
                return displayNotify(GLOBAL_TRANSLATIONS.CannotDisplayTableColumnsMustBeSpecifiedToLoadDataWithAjaxSpecifyOptionscolumn);

            var parsedColumns = _.map(opts.columnList, function (val) {
                return { "data": (val.ColumnName && val.ColumnName != "" ? val.ColumnName.replace(".", "\\.") : "") };
            });
            //console.log(parsedColumns);

            tableOptions.serverSide = true;
            tableOptions.processing = true;
            tableOptions.ajax = {
                "url": (opts.ajaxUrl + (_.isString(opts.advancedFiltersString) ? opts.advancedFiltersString : "")),
                "type": "GET",
                "data": function (d) {
                    if (_.isFunction(opts.onDataPreprocessing))
                        opts.onDataPreprocessing(d);
                }
            };
            tableOptions.columns = parsedColumns;
        }
        
        that.table = $(that.tableId).DataTable(tableOptions);

        // add event on draw
        if (_.isFunction(opts.onDraw))
            that.table.on("draw", opts.onDraw);

        // 4. onSelected effect
        if (opts.hasRowSelect) {
            $(that.tableId + ' tbody').on('click', 'tr', function () {
                if ($(this).hasClass('selected')) {
                    return $(this).removeClass('selected');
                }

                that.table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                if (_.isFunction(opts.onRowSelect))
                    opts.onRowSelect(this);
            });
        }

        //5. show
        $(that.tableId + '-body').show();
        that.table.columns.adjust();
        if (opts.isStriped)//check if striped
            $(that.tableId).addClass("table-striped");

        //6. change pagination proportions
        $(".dataTables_wrapper > :last-child").children(":first-child").removeClass();
        $(".dataTables_wrapper > :last-child").children(":last-child").removeClass();
        $(".dataTables_wrapper > :last-child").children(":first-child").addClass("col-md-5");
        $(".dataTables_wrapper > :last-child").children(":last-child").addClass("col-md-11");
    }
    
    // method that reinitializes table, with optional change of url
    this.reinitDataTable = function (newUrl) {
        if (!newUrl || newUrl == "")
            return that.table.ajax.reload();
        that.table.ajax.url(newUrl).load();
    }

    // method that reinitilizes the table data source, changing the filters (part that starts with /data?)
    this.changeFilters = function (newFilters) {
        if (!_.isString(newFilters) || (newFilters.trim() != "" && !newFilters.startsWith("?")))
            return console.log(GLOBAL_TRANSLATIONS.TryingToSetInvalidFilters);
        that.filters = newFilters;
        that.reinitDataTable(that.url + that.filters);
    }

    // method that inits the column list
    this.addColumnsToggle = function () {
        var elements = "";
        for (var i = 0; i < opts.columnList.length; i++) {
            var name = opts.columnList[i].ColumnName;

            var element = `<div class="can-toggle demo-rebrand-2 small" style="padding-top: 5px; padding-bottom: 5px">
                          <input id="disc_` + i + `" type="checkbox" checked class="toggle-vis" data-column=` + i + ` >
                          <label for="disc_` + i + `" style="margin: 0">
                              <div class="can-toggle__switch" data-checked="On" data-unchecked="Off"></div>
                              <div class="can-toggle__label-text">&nbsp;` + name + `</div>
                          </label>
                      </div>`;
            elements = elements + element;
        }


        var htmlcols = `<div class="dropdown" style="padding-right:30px; ">
                  <button type="button" class="btn btn-sm btn-outline-info dropdown-toggle" data-toggle="dropdown" id="cols-` +  that.tableName + `" aria-haspopup="true" aria-expanded="false"> Columns </button>
                  <div class="dropdown-menu" style="width:250px;background-color: #1b2033;color:#ffffff;" aria-labelledby="cols-` + that.tableName + `">
                    ` + elements +
            `</div>
              </div>`;

        $(that.tableId + "_filter").prepend(htmlcols);
    }

    // method that inits the advanced data filters
    this.addAdvancedFilters = function () {
        var html_filters = '<div style="padding-right:30px; "><button type="button" class="btn btn-sm btn-outline-success" id="advanced-filters-' + that.tableName + '" style="cursor:pointer" aria-haspopup="true" aria-expanded="false">' + opts.advancedFiltersTitle + '</button>';
        $(that.tableId + "_filter").prepend(html_filters);
        $("#advanced-filters-" + that.tableName).click(opts.onClickAdvancedFilters);
    }
};

var PromisDataTableStatics = {
    getTableNameAndColumnsFromView: function (viewData) {
        var obj = $(viewData);
        var tableNameT = obj.find('.promisdt').attr("data-tablename");
        tableNameT = JSON.parse('"' + tableNameT + '"');
        var columnsT = obj.find('.promisdt').attr("data-columnlist");
        columnsT = JSON.parse(columnsT);
        tableNameT = _.isString(tableNameT) ? tableNameT : "";
        columnsT = _.isArray(columnsT) ? columnsT : [];
        console.log("Test data", tableNameT, columnsT);
        return {
            tableName: tableNameT,
            columns: columnsT
        };
    }
};