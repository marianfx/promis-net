﻿//
/**
 * Sends a custom request to the server, retrieving or sending data.
 * Options is an object. Inside it you can specify:
 * - callType: GET, POST, PUT, DELETE, etc.
 * - successFunc: function(data: {}) - the method that is invoked with the data received from the server, if the server send a positibe answer
 * - failureFunc: function(errorList: string[]) - the method that is invoked if errors appear (error status code is sent from the server or timeout expires)
 * - data: the object (JSON) to send to the server
 * - contentType: the Content-Type set in the header of the request; you should leave it as default if sending JSON
 * - elementsToAddLoading: adds the status of loading (greys out + displays loading gif) to each DOM element in the list
 * - loadingOptions: {
 *      - color: the color which will be put on top of the element; it is prefered semi-transparent rgba, ex. rgba(0, 0, 0, 0.3)
 *      - position: center, left, right
 *      - hideText: if true, hides away all the text from the elements in 'elementsToAddLoading'; default is false
 *      - loaderPath: path to the gif image; example: '../assets/img/loader2.gif'
 *  }
 * - removeLoaderIfFail: speficies if the loading class should be removed if something fails on loading data for that element. default is false
**/
function callServer(url, options) {

    // options setup
    var defaults = {
        callType: GLOBAL_DATA.CALL_TYPES.GET,
        successFunc: function (data) { console.log(data); },
        failureFunc: defaultErrorFunction,
        data: null,
        contentType: GLOBAL_DATA.CONTENT_TYPES.FORM_DATA,
        processData: true,
        elementsToAddLoading: null,
        filterOnlyDataField: true,
        loadingOptions: {
            color: 'rgba(0, 0, 0, 0.3)',
            position: 'center',
            hideText: false,
            loaderPath: GLOBAL_DATA.CALL_LOADERS.BLUE_LOADER
        },
        removeLoaderIfFail: false
    };
    var opts = $.extend({}, defaults, options);//merge properties, options will override defaults

    // add optional loading
    addLoadingToElements(opts.elementsToAddLoading, opts.loadingOptions);

    $.ajax({
        type: opts.callType, // type of request
        url: url, //path of the request
        data: opts.data,
        contentType: opts.contentType, // data content type (header)
        processData: opts.processData,

        // the function called on Success (no error returned by the server) (result)
        success: function (result) {
            //setTimeout(function () {
                removeLoadingFromElements(opts.elementsToAddLoading);
            //}, 300);

            if (result.hasOwnProperty('next') && result.next != null && result.next != '') {
                window.location.href = GLOBAL_DATA.APP_ROOT + result.next;
                return;
            }
            if (opts.filterOnlyDataField && result.hasOwnProperty('data'))
                return opts.successFunc(result.data);

            return opts.successFunc(result);
        },
        // the function called on error (error returned from server or TimeOut Expired)
        error: function (err) {
            var response = "Unknown error happened. Possibly the address requested is incorrect or timeout happened.";
            if (err.hasOwnProperty("responseJSON"))
                response = err.responseJSON;
            else if (err.hasOwnProperty("responseText")) {
                try {
                    response = JSON.parse(err.responseText);
                }
                catch (e) {
                    response = JSON.stringify(err.responseText);
                }
            }

            //console.log(response);
            var errorArray = parseErrorObject(response);
            //console.log(errorArray);
            removeLoadingFromElements(opts.elementsToAddLoading, opts.removeLoaderIfFail);
            console.log(errorArray);
            return opts.failureFunc(errorArray);
        },
        timeout: 1000000 // the time limit to wait for a response from the server, milliseconds
    });
}

function addLoadingToElements(elements, options) {

    if (elements != null)
        for (var index in elements){
            $(elements[index]).addClass('loading');

            var firstName = elements[index];
            if (elements[index].startsWith("#") || elements[index].startsWith("."))
                firstName = elements[index].substring(1);

            var tempStyleName = firstName + '-tempstyle';

            var tempStyle = $("<style type='text/css' id='" + tempStyleName + "'></style>");
            var updated = `.` + tempStyleName + `:after {
                                background-color: ` + options.color + `!important;
                                background-position: ` + options.position + `!important;
                                background-image: url("` + options.loaderPath + `") !important;
                        }`;

            var noindent = "";
            if (!options.hideText) {
                noindent = `
                            .` + tempStyleName + ` {
                                text-indent: initial !important;
                                white-space: initial !important;
                                overflow: initial !important;
                            }`;
            }

            tempStyle.text(updated + noindent);
            tempStyle.appendTo('head');
            $(elements[index]).addClass(tempStyleName);
        }
}

function removeLoadingFromElements(elements, removeLoading = true) {
    if(!removeLoading)
        return;
    
    if (elements != null){
        for (var index in elements){
            $(elements[index]).removeClass('loading');
            $(elements[index]).removeClass(elements[index].substring(1) + "-tempstyle");
            var styleElem = $('head ' + elements[index] + '-tempstyle');
            styleElem.remove();
        }
    }
}

function parseErrorObject(error) {
    if (error.hasOwnProperty('errors'))
        error = error.errors;

    if (!Array.isArray(error))
        return [parseInnerErrorObject(error)];

    var output = [];
    for (var i in error) {
        output.push(parseInnerErrorObject(error[i]));
    }

    return output;
}

function parseInnerErrorObject(innerErr) {
    if (innerErr.hasOwnProperty('Causes'))
        return innerErr;
    else
    {
        return {
            Causes: ["No details"],
            Message: innerErr,
            StartingMethod: 'No details',
            UniqueId: "No details"
        };
    }
}

function defaultErrorFunction(errors) {
    for (var i in errors) {
        var error = errors[i];

        displayNotify(error);
    }

}

function displayNotify(message, type = 'error', hidePrevious = false, autoH = true) {
    if (hidePrevious)
        $('.notifyjs-corner').html('');

    if (message.hasOwnProperty('UniqueId') && message.UniqueId != null && message.UniqueId != 'No details') {
        // display promis error
        $.notify({ message: message.Message, uniqueid: message.UniqueId, details: message.StartingMethod }, {style: 'promis', autoHide: autoH, clickToHide: false });
        //add send mail link
        $('.notifyjs-promis-uniqueid').attr('href', function (ind, old) {
            var to = 'support@promis.net';
            var subject = "Error happened on PROMIS";
            var refId = $(this).text() != '' ? 'The reference ID for the error is ' + $(this).text() + '.' : '';
            var message = $(this).parent().parent().children()[0].textContent;
            var details = $(this).parent().parent().children()[1].textContent;
            var body = "Error message: " + message + "%0A" + details + "%0A" + refId + '%0A Thank you.%0A';
            $(this).attr('href', 'mailto:' + to + '?subject=' + subject + '&body=' + body);
        });
    }
    else {
        var msgError = "Unknown error happened.";
        //preparse error first
        if (message.hasOwnProperty("Message") && message.Message.hasOwnProperty("Message"))
            msgError = message.Message.Message;
        else if (message.hasOwnProperty("Message") && _.isString(message.Message))
            msgError = message.Message;
        else if (_.isString(message))
            msgError = message;

        $.notify(msgError, type, { autoHide: autoH }); 
    }
}


$(document).ready(function () {
    //add a new style 'foo'
    $.notify.addStyle('promis', {
        html:
        `
        <div>
            <div class="alert alert-danger" role="alert">
              <p data-notify-html='message'></p>
              <p><b>Hints: </b><span  data-notify-html='details'></span></p>
              <p>Reference ID: <a title='Sent mail to support about this.' href='#' class='notifyjs-promis-uniqueid' data-notify-html='uniqueid'></a></p>
            </div>
        </div>`
    });
});