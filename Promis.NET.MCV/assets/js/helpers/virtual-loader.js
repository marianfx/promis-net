﻿/**
 * This is a class (wrapper around properties and methods) that is used to virtually navigate through pages at an endpoint.
 * Options is an object. Inside it you can specify:
    - workingPath: string; Represents the path where this virtual loader will work (will ignore others); Example: '/project'
    - reinitializeMethod: function(data: object); Represents the function that is used by this class to initialize the content of a 'virtual page' (aka executes everytime a page is 'virtually' accessed)
    - titleGetter: function(): string; Override this method to return, when on a page, the title of a page; By default it returns the same title as when first accessing
    - stateDataGetter: function(url): object; Used to return the state of a page; Override this to set or get special content at your needs.
 */
function VirtualLoader(options) {
    // init options
    var defaults = {
        workingPath: "",//where this virtual navigator works; relative to app path eg /engineering/grid; here all the navigation will be virtual
        reinitializeMethod: function (data) {//method called on each navigate call

        },
        titleGetter: function () {//method that should return the title of the page
            return $('title').text();
        },
        stateDataGetter: function (url) {//method that should return the data to add to the state and transmit forward for the page
            return "";
        }
    };
    var opts = $.extend({}, defaults, options);//merge properties, options will override defaults
    var that = this;

    this.titleGetter = opts.titleGetter;

    // method that replaces state on startup (autocalls) 
    this.replaceInitialState = function () {
        history.replaceState({ title: $('title').text(), url: window.location.href }, $('title').text(), window.location.href);
        that.startupCheck();
    }

    // method used as navigator. Needs to have data.title and data.url
    // using API window.history.pushState(data, title, url) to change the URL where the website points at and add the new url to the state
    // override state processing in childs to 
    this.virtualNavigateToUrl = function (data) {
        var boolPushState = false;
        if (typeof history.pushState !== "undefined") {
            boolPushState = true;
        }

        if (boolPushState) {
            that.changePageTitleMaitainingSuffix(data.title);
            history.pushState(data, data.title, data.url);
        }
    }

    // method used to change the title of the page
    this.changePageTitleMaitainingSuffix = function (title) {
        var currentTitle = $('title').text();
        var splitted = currentTitle.split('|');
        var suffix = splitted.length > 1 ? splitted[1] : " PROMIS";
        var newTitle = title + " |" + suffix;
        $('title').text(newTitle);
    }


    // method used on refresh
    this.refreshNavigator = function (url, title) {
        var data = opts.stateDataGetter(url, title);
        if (data != null && data !== undefined) {
            that.changePageTitleMaitainingSuffix(title);
            opts.reinitializeMethod(data);
        }
    }

    this.startupCheck = function() {
        var location = window.location.href;
        if (location.startsWith(GLOBAL_DATA.APP_ROOT + opts.workingPath)) {
            var title = opts.titleGetter();
            that.refreshNavigator(location, title);
        }
    }

    // overriding history.pushstate
    // METHOD that gets called whenever the virtual navigate is called (even on back / forward from browser).
    // If state == null it means we are on the main page (or another page) and we need to push a call to the server
    // otherwise, we need to get the datatable (in this case) from the state and load data for it
    function change(state) {
        if (state != null) {
            if (state.url.indexOf(GLOBAL_DATA.APP_ROOT + opts.workingPath) == -1) {// initial page
                window.location.reload();
            } else { // page added with pushState
                that.refreshNavigator(state.url, state.title);
            }
        }
    }

    $(window).on("popstate", function (e) {
        change(e.originalEvent.state);
    });

    (function (original) { // overwrite history.pushState so that it also calls
        // the change function when called
        history.pushState = function (state) {
            change(state);
            return original.apply(this, arguments);
        };
    })(history.pushState);
    // ##### END #####


    // first time startup
    if (window.$)
    {
        if ($.isReady)
            that.replaceInitialState();
        else
            $(document).ready(that.replaceInitialState);
    }
}