﻿
/**
 * Wrapper object arount the methodology of exporting data. Things to know about OPTIONS:
    - tableName: String must be the name of the table used to get data from
    - columns: String[] an array of columns used in export,
    - advancedFiltersString: String; represents the advanced query, represented in datatables format, for retrieving data,
    - onDataRefresh: function(){}: executed after everything has finished successfully; use it maybe to refresh table
 */
function PromisDataExporter(options) {
    var defaults = {
        tableName: "",
        columns: [],
        advancedFiltersString: "",
        onDataRefresh: function () { }
    };
    var opts = $.extend({}, defaults, options);//merge properties, options will override defaults
    var that = this;

    // load table name
    if (!opts.tableName || opts.tableName == "")
        return displayNotify(GLOBAL_TRANSLATIONS.ErrorTableNameNotSpecifiedWhenInitializingTable);
    this.tableName = opts.tableName.toLowerCase();
    this.filters = opts.advancedFiltersString;

    // on filters change
    this.changeFilters = function (newFilters) {
        if (!_.isString(newFilters) || (newFilters.trim() != "" && !newFilters.startsWith("?")))
            return console.log(GLOBAL_TRANSLATIONS.TryingToSetInvalidFilters);
        that.filters = newFilters;
    }

    // set method that is triggered on click export
    this.onClickExportExcelFile = function () {
        if (!that.tableName || that.tableName == "")
            return displayNotify(GLOBAL_TRANSLATIONS.NoTableAvailableToExecuteTheExportOn);
        that.exportExcelFile(false);
    }

    // set method that is triggered on click export template
    this.onClickExportExcelTemplateFile = function () {
        if (!that.tableName || that.tableName == "")
            return displayNotify(GLOBAL_TRANSLATIONS.NoTableAvailableToExecuteTheExportTemplateOn);
        that.exportExcelFile(true);
    }

    // set method that is triggered on click import
    this.onClickImportExcelFile = function () {
        if (!that.tableName || that.tableName == "")
            return displayNotify(GLOBAL_TRANSLATIONS.NoTableAvailableToExecuteTheImportOn);

        $(".modal-content").css("width", "400px");
        var url = GLOBAL_DATA.APP_ROOT + "/engineering/tables/" + GLOBAL_DATA.PROJECT_DATABASE + "/" + that.tableName + "/import";
        open_modal(url, {
            title: GLOBAL_TRANSLATIONS.ImportExcelDataFor + " " + that.tableName.toUpperCase(),
            next: function (viewData) {
                //console.log(viewData);
                $("#uploadBtn").change(function () {
                    $("#uploadFile").val(this.value);
                    $("#exec_btn").removeClass("disabled")
                });
                $("#dvLoading").hide();
                //if (that.tableName == "work_def")
                //    $("#table_down").hide();

                $("#exec_btn").click(that.importExcelFile);
            },
            version: GLOBAL_DATA.MODAL_VERSIONS.ENGINEERING_IMPORT
        });
    }

    // the method that does the actual export (calls server)
    this.exportExcelFile = function (isTemplate) {
        $(".modal-content").css("width", "300px");

        var title = (isTemplate ? GLOBAL_TRANSLATIONS.ExportTemplate : GLOBAL_TRANSLATIONS.ExportData);
        open_modal_normal({
            title: title,
            content: `<div style="overflow:hidden">
                <div class="load-wrapper2">
                    <div class="sk-three-bounce">
                        <div class="sk-child sk-bounce1"></div>
                        <div class="sk-child sk-bounce2"></div>
                        <div class="sk-child sk-bounce3"></div>
                    </div>
                </div>
            </div>`,
            footerHtml: '',
            version: GLOBAL_DATA.MODAL_VERSIONS.ENGINEERING_EXPORT
        });
        var htmlToPut = `<div align="center">
	                         <table width="100%" border="0">
		                        <tr>
		                          <td align="center" valign="top"><br>
			                          <a href="#" id="linkToDownload" target="_blank"> <span class="ti-download" style="font-size:48px;"></span></a> 
			                          <br>
			                        <br>`+
                                        GLOBAL_TRANSLATIONS.ClickOnIconToDownload + `</td>
		                       </tr>
	                        </table>
                        </div>`;

        var advFilters = "?";
        if (_.isString(that.filters) && that.filters.trim() != "")
            advFilters = that.filters + "&";
        var url = GLOBAL_DATA.APP_ROOT + "/api/engineering/tables/" + GLOBAL_DATA.PROJECT_DATABASE + "/" + that.tableName + "/export" + advFilters + "isFullExport=" + !isTemplate;
        callServer(url, {
            successFunc: function (url) {
                $('#modal-container .modal-body').html(htmlToPut);
                $("#linkToDownload").attr("href", GLOBAL_DATA.APP_ROOT + url);
            }
        });
    }

    // the method that does the actual import. Consists of two parts: sending the file to the server, receiving the server link, and then requesting an import from it (secure).
    this.importExcelFile = function () {
        if (window.FormData === undefined)
            return displayNotify(GLOBAL_TRANSLATIONS.ThisBrowserDoesntSupportHTML5FileUploads);

        var filePath = document.getElementById("uploadFile").value;
        var ext = filePath.split(".");
        ext = ext[ext.length - 1].toLowerCase();
        var arrayExtensions = ["xls", "xlsx"];

        if (arrayExtensions.lastIndexOf(ext) == -1) {
            $("#dvLoading").hide();
            return displayNotify(GLOBAL_TRANSLATIONS.WrongFileExtensionPleaseSelectXlsOrXlsxFiles);
        }

        var fileL = $("#uploadBtn")[0].files.length;
        if (fileL < 1)
            return displayNotify(GLOBAL_TRANSLATIONS.PleaseSelectAFileToImport);

        var files = $("#uploadBtn")[0].files;
        var data = new FormData();
        data.append("file", files[0]);

        // first do the call that uploads the file to a server path, and returns the server relative link back
        $('.load-wrapper2').css("display", "block");
        var url = GLOBAL_DATA.APP_ROOT + '/content/upload?type=excel';
        callServer(url, {
            callType: GLOBAL_DATA.CALL_TYPES.POST,
            contentType: false,
            processData: false,
            data: data,
            successFunc: function (filePath) {
                //console.log(filePath);
                that.importExcelFilePart2(filePath);
            }
        });
    }

    // the method that is executed after having the l
    this.importExcelFilePart2 = function (filePath) {
        var overwrite = $('#metodo_importa').is(":checked");

        var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + that.tableName + '/import?filePath=' + encodeURIComponent(filePath) + "&overWrite=" + overwrite;
        callServer(url, {
            callType: GLOBAL_DATA.CALL_TYPES.POST,
            successFunc: function () {
                $("#importProcessing").append("Done");
                $('.load-wrapper2').css("display", "none");
                $("#importResults").fadeIn(300);
                displayNotify(GLOBAL_TRANSLATIONS.FileHasBeenImportedWithSuccesReloadingTableInSeconds, "success");
                setTimeout(function () {
                    if (_.isFunction(opts.onDataRefresh))
                        opts.onDataRefresh();
                    close_modal();
                }, 3000);

            },
            failureFunc: function (errors) {
                defaultErrorFunction(errors);
                $('.load-wrapper2').hide();

            }
        });
    }
}