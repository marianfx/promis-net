﻿/**
 * Use this, which is included in the _ModuleLayout View, to create a navigating layout for a Module (with submodules / menus)
   Options is an object, with the following properties:
    - workingPath: where the module resides in the application. Example: "/project".
    - contentContainerId: The ID of the container that is prepared  to hold the contents of submodules. For example 'project-container"
    - pageDescriptors: An array with objects of type {id: "page-id", title: "Page Title"}. Holds the connection between page titles and page IDs. Remember that all the IDs must have controller paths defined (in the controller represented by 'workingPath' that match '{workingPath}/{projectId}/{pageId}/partial)
 */
var ModuleLayout = function (opts) {

    // validations checks
    if (!opts["workingPath"])
        return console.log("The parameter 'workingPath' is not specified. The module will not work.");
    if (!opts["contentContainerId"])
        return console.log("The parameter 'contentContainerId' is not specified. The module will not work.");
    if (!opts["pageDescriptors"] || !_.isArray(opts["pageDescriptors"]))
        return console.log("The parameter 'pageDescriptors' is not specified or is not an array. The module will not work.");

    var that = this;

    //module URL
    this.basicModuleUrl = GLOBAL_DATA.APP_ROOT + opts.workingPath + "?page=@page";

    // setup the initializing method
    this.initializeModule = function (page, data) {
        page = (page == "home" ? "" : page);//display "" more beautiful

        // add active
        $(".linkedin, .linkedin2").removeClass("active");
        $(".linkedin[data-page='" + page + "']").addClass("active");
        var possibleParent = $(".linkedin[data-page='" + page + "']").closest("li").children("a").get();
        if (_.isArray(possibleParent) && possibleParent.length > 0 && $(possibleParent[0]).hasClass("nav-link-dropdown"))
            $(possibleParent[0]).addClass("active");

        //put loading html
        setContainerAsLoading("#" + opts.contentContainerId);

        //load data
        var url = GLOBAL_DATA.APP_ROOT + opts.workingPath + "/" + GLOBAL_DATA.PROJECT_ID + "/" + page + "/partial";

        //special cases where data is needed (&key=value); will be ignored if empty
        var additional = "?";
        if (data) {
            _.forEach(data, function (val, key) {
                additional += key + "=" + encodeURIComponent(val) + "&";
            });
        }
        additional = additional.substr(0, additional.length - 1);
        url = url + additional;

        //console.log(url);
        callServer(url, {
            successFunc: function (viewData) {
                var location = window.location.href;
                var params = getAllUrlParams(location);
                // check if this callback comes from a previous page and ignore if so (otherwise it will replace current's page contents)
                if(page == params["page"] || (page == "" && (params["page"] == undefined || params["page"] == "home")))
                    $("#" + opts.contentContainerId).html(viewData);
            }
        });
    }

    // returns the title of the page given a page id
    this.getTitleFromPage = function (page) {
        for (var i = 0; i < opts.pageDescriptors.length; i++) {
            var current = opts.pageDescriptors[i];
            if (current.id == page || (current.id == "home" && (page == "" || !page)))
                return current.title;
        }
    }

    //setup the navigator
    this.history = new VirtualLoader({
        workingPath: opts.workingPath,
        reinitializeMethod: that.initializeModule,
        titleGetter: function () {
            var location = window.location.href;
            var params = getAllUrlParams(location);
            var page = params.page;
            return that.getTitleFromPage(page);
        },
        stateDataGetter: function (url, title) {
            var params = getAllUrlParams(url);
            return (params.page ? params.page : "");
        }
    });

    // assign click loading
    $(document).ready(function () {
        $(".linkedin").click(function () {
            var page = $(this).attr("data-page");
            if (page == undefined || page == null)//ignore items that do not have data-page
                return;

            page = (page == "" ? "home" : page);
            var url = that.basicModuleUrl.replace("@page", page);
            that.history.virtualNavigateToUrl({ url: url, title: that.getTitleFromPage(page) });
        });
    });
}
