﻿var GLOBAL_DATA = {
    APP_ROOT: "http://" + window.location.hostname + ":" + window.location.port,
    USER_ID: "",
    USER_THEME: "",
    PROJECT_DATABASE: "",
    PROJECT_ID: "",
    MODAL_VERSIONS: {
        PROJECT_SELECTION: "F001.M001.V1.0.0.0",
        PERSONAL_INFORMATION: "F001.M002.V1.0.0.0",
        WEATHER_INFORMATION: "F001.M003.V1.0.0.0",
        ENGINEERING_EXPORT: "F004.M003.V1.0.0.1",
        ENGINEERING_IMPORT: "F004.M004.V1.0.0.1",
        ENGINEERING_DATABASE_NORMAL: "F004.M001.V1.0.0.0",
        ENGINEERING_DATABASE_DELETE: "F004.M002.V1.0.0.0",
        ENGINEERING_ADVANCED_SEARCH: "F004.M005.V1.0.0.0",
        CERTIFICATION_MANAGEMENT_TAGS_STATUS: "F005.M001.V1.0.0.0",
        MATERIAL_PRESERVATION_ASSIGNMENT: "F006.M001.V1.0.0.0",
        PDF_EXPORT_PROGRESS: "F008.M001.V1.0.0.0",
        COMMISSIONING_INSPECTION_STATUS_PROGRESS: "F009.M001.V1.0.0.0"
    },
    CONTENT_TYPES: {
        JSON: "application/json",
        FORM_DATA: "application/x-www-form-urlencoded;odata=verbose"
    },
    CALL_TYPES: {
        GET: "GET",
        POST: "POST",
        PUT: "PUT",
        DELETE: "DELETE"
    },

    CALL_LOADERS: {
        BLUE_LOADER: '../assets/img/loader2.gif'
    }
};