﻿var promisExporter = null;
var modifiedActivities = [];

function initSOWAssignmentToSubcontractors() {
    $("#subconstractors").change(loadSubContractor);
    assignSelectDeselect();
    // init exporter
    promisExporter = new PromisDataExporter({
        tableName: "subc_sow",
        advancedFiltersString: getAdvancedFiltersForSubContractors("")
    });

    $("#export-certification-data").click(promisExporter.onClickExportExcelFile);   
    $("#save-activities").click(saveSubcontractorSOW);
}
initSOWAssignmentToSubcontractors();

function initializeSowAccordion() {
    loadSubContractor();
    assignClickOnInputs();
}

function loadSubContractor() {
    var selectedSubcontractor = $("#subconstractors").val();
    console.log("subcontractors:"+selectedSubcontractor);

    promisExporter.changeFilters("");
    //set the getter for advanced filters (to be used in export)
    promisExporter.changeFilters(getAdvancedFiltersForSubContractors(selectedSubcontractor));

    $("div[id^='accordion-subcontractors-'].show").each(function () {

        console.log(this);

        $(".activity-table tbody tr").each(function () {
            var activityCode = $(this).find("td:nth-child(2)").text();
            var subcontractor = $(this).find("td:last").attr("data-code");

            console.log("activity code", activityCode, subcontractor);
            //var indexInSaved = _.findIndex(modifiedActivities, function(o){
            //    return o.SubActivity == activityCode;
            //});
            //if (indexInSaved >= 0 && indexInSaved < modifiedActivities.length)
            //{
            //    var obj = modifiedActivities[indexInSaved];
            //    var subcName = $("#subconstractors option[value='" + activityCode + "']").text();
            //    $("#ck_" + activityCode).attr("checked", obj.Value);
            //    if (obj.Value == true) {
            //        $(this).find("td:last").attr("data-code", obj.Code);
            //        $(this).find("td:last").text(subcName);
            //    }
            //    else {
            //        $(this).find("td:last").attr("data-code", "");
            //        $(this).find("td:last").text("");
            //    }
            //}
            if (subcontractor == selectedSubcontractor || subcontractor.trim() == "")
                $("#ck_" + activityCode).prop("disabled", false);
            else
                $("#ck_" + activityCode).prop("disabled", true);
        });
    });
}

function assignClickOnInputs() {

    $(".clickable").click(function () {

        var selectedSubcontractor = $("#subconstractors").val();
        if (!selectedSubcontractor || selectedSubcontractor == "") {
            $(this).prop("checked", !$(this).prop("checked"));
            return displayNotify(GLOBAL_DATA.TRANSLATIONS.FirstSelectASubcontractorToAssignTo);
        }

        addTheValuesToArray.bind(this)(selectedSubcontractor);
    });
}

function assignSelectDeselect() {
    $(".accordion .card .card-header").each(function () {
        var maCode = $(this).find('a').attr("data-code");
        $(this).append('<button data-activity-code="' + maCode + '" type="button" class="pull-right btn btn-primary btn-xs selection-deselection-subactivity">' + GLOBAL_DATA.TRANSLATIONS.SelectDeselect + '</button>');
    });
    $(".selection-deselection-subactivity").click(selectDeselectSubActivity);
}

function selectDeselectSubActivity() {
    var activityCode = $(this).attr("data-activity-code");

    var selectedSubcontractor = $("#subconstractors").val();
    if (!selectedSubcontractor || selectedSubcontractor == "") {
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.FirstSelectASubcontractorToAssignTo);
    }

    var activeCells = $("#accordion-subcontractors-" + activityCode + " .activity-table tbody tr").find("td:nth-child(2)");
    _.each(activeCells, function (cell) {
        var activeCell = $(cell).text();
        var searchedCell = $("#ck_" + activeCell);

        if (searchedCell.is(':disabled'))
            return;
        
        searchedCell.is(":checked") ? searchedCell.prop('checked', false) : searchedCell.prop('checked', true);
        
        addTheValuesToArray.bind(searchedCell)(selectedSubcontractor);
    });
}

function getAdvancedFiltersForSubContractors(subcontractor) {

    var output = "?columns[0]=code&columns[1]=subcontractors.subcontractor&columns[2]=work_def.sub_act&columns[3]=work_def.sub_act_d"
        + "&join[0].datatable=subcontractors&join[0].leftColumn=subc_sow.code&join[0].rightColumn=subcontractors.code"
        + "&join[1].datatable=work_def&join[1].leftColumn=subc_sow.sub_act&join[1].rightColumn=work_def.sub_act"
        + "&group[0]=id";

    if (subcontractor == "")
        return output;
    output += "&advfilters[0].leftoperand=subcontractors.code&advfilters[0].rightoperand=" + subcontractor + "&advfilters[0].operator=eq";

    return output;
}

function addTheValuesToArray(selectedSubcontractor) {
    var subcId = $(this).closest("tr").find("td:last");
    var isChecked = $(this).is(":checked");
    var activity = $(this).attr("data-activity");
    var oldValue = $(this).attr("data-initialval") == "N" ? false : true;

    if (isChecked) {
        subcId.text($("#subconstractors option:selected").text());
        subcId.attr("data-code", selectedSubcontractor);
    }
    else {
        subcId.text("");
        subcId.attr("data-code", "");
    }

    console.log("Test", activity, selectedSubcontractor, isChecked);
    _.remove(modifiedActivities, function (o) {
        return o.SubActivity == activity && o.Code == selectedSubcontractor;
    });

    if (isChecked == Boolean(oldValue))
        return;

    modifiedActivities.push({
        SubActivity: activity,
        Value: isChecked,
        Code: selectedSubcontractor
    });
}

function saveSubcontractorSOW() {
    var subcontractor = $("#subconstractors").val();
    if (subcontractor == "")
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ChooseASubcontractorFirst);
    
    console.log(modifiedActivities);
    if (!_.isArray(modifiedActivities) || modifiedActivities.length == 0)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.NoDataModified);

    //console.log(arrayM);
    var url = GLOBAL_DATA.APP_ROOT + "/api/certification/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/subcontractors/sow";
    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        data: JSON.stringify(modifiedActivities),
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        elementsToAddLoading: ["body"],
        removeLoaderIfFail: true,
        successFunc: function () {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.SubcontractorSowUpdated, 'success');
        }
    });

}

