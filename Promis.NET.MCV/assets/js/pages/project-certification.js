﻿var promisDt = null;
var promisExporter = null;

function initSetupCertification() {
    // click events
    $('#main-activities').change(loadActivities);
    $('#related-activities').change(loadActivities);
    $('#sub-activities').change(loadTable);

    // init exporter
    promisExporter = new PromisDataExporter({
        tableName: "work_def",
        advancedFiltersString: "",
        onDataRefresh: function () {
            if(promisDt)
                promisDt.reinitContentForTable();
        }
    });

    //click on IMPORT or EXPORT. Works thanks to the use of the import-export.js file
    $('#button-export-excel').click(promisExporter.onClickExportExcelFile);
    $("#button-export-template").click(promisExporter.onClickExportExcelTemplateFile);
    $("#button-import-excel").click(promisExporter.onClickImportExcelFile);
};
initSetupCertification();

function loadActivities() {
    var selected = $(this).val();
    var next = $(this).attr("data-next");
    var path = $(this).attr("data-path");
    promisExporter.changeFilters("");

    if (!selected || !next || !path)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AnActivityMustBeSelectedInOrderToContinue);

    if (path == "related")
        promisExporter.changeFilters("?advfilters[0].leftoperand=act&advfilters[0].rightoperand=" + encodeURIComponent(selected) + "&advfilters[0].operator=inc");
    else
        promisExporter.changeFilters("?advfilters[0].leftoperand=act&advfilters[0].rightoperand=" + encodeURIComponent(selected) + "&advfilters[0].operator=eq");


    var url = GLOBAL_DATA.APP_ROOT + "/api/project/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/activities/" + encodeURIComponent(selected) + "/" + encodeURIComponent(path);
    callServer(url, {
        elementsToAddLoading: ['body'],
        removeLoaderIfFail: true,
        successFunc: function (activities) {
            //clear all others
            var tempNex = next;
            //console.log(tempNex);
            while (tempNex) {
                $(tempNex).html("<option value=''>&nbsp;</option>");
                tempNex = $(tempNex).attr("data-next");
            }

            if (!_.isArray(activities) || activities.length == 0)
                return displayNotify(GLOBAL_DATA.TRANSLATIONS.ThereIsNoDataForThatSelection);

            $('#table-data-container').html('');
            promisDt = null;
            firstCall = true;

            // put actual loaded activities
            _.forEach(activities, function (activity) {
                $(next).append("<option value='" + activity.Code + "'>" + activity.Code + " - " + activity.Description + "</option>");
            });
        }
    });
}

function loadTable() {
    var selected = $("#sub-activities").val();
    if (!selected)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AnSubactivityMustBeSelectedInOrderToLoadTheTableForIt);
    
    var url = GLOBAL_DATA.APP_ROOT + "/project/" + GLOBAL_DATA.PROJECT_ID + "/certification/sub";
    callServer(url, {
        elementsToAddLoading: ["body"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $('#table-data-container').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTable(data.tableName, data.columns, selected);
        },
    });
}

function initializeDataTable(dataTable, theColumns, activity) {
    var filters = getAdvancedFilters(activity);
    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + '/data';
    promisExporter.changeFilters(filters);

    promisDt = new PromisDataTable({
        tableName: dataTable,
        columnList: theColumns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 280,
        isStriped: true,
        onDataPreprocessing: function (d) {//assign a function to determine order by integer when clicking the 'step' column
            var newOrder = [];
            _.forEach(d.order, function (elem) {
                if (elem.column != 0) {
                    newOrder.push(elem);
                    return;
                }
                newOrder.push({
                    column: 0,
                    dir: elem.dir,
                    func: encodeURIComponent("cast(step as signed)")
                });
            });
            d.order = newOrder;
        }
    });
    promisDt.initContentForTable();
}

function getAdvancedFilters(activity) {
    return ("?advfilters[0].leftoperand=sub_act&advfilters[0].rightoperand=" + encodeURIComponent(activity) + "&advfilters[0].operator=eq");
}