﻿var promisExporter = null;

function initProgressPage() {
    // triggers (resize, scroll)
    initScrollActions();
    onResize();
    $(window).resize(onResize);

    // click events
    $('#main-activities').change(loadAllActivities.bind(null, false));
    if (!GLOBAL_DATA.setup.planning.isReschedule) {
        $('#button-progress-overall').click(loadAllActivities.bind(null, true));
        $("#button-progress-subcontractor").click(function () {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.FunctionalityNotAvailable);
        });
    }
    else {
        $("#button-save").click(saveChanges);
    }

    // init
    makeCurrentMonthYellow();
    promisExporter = new PromisDataExporter({
        tableName: "planning",
        advancedFiltersString: ""
    });

    //click on IMPORT or EXPORT. Works thanks to the use of the import-export.js file
    $("#button-export-excel").click(promisExporter.onClickExportExcelFile);
};
initProgressPage();

function initScrollActions() {
    $('.linked-scrollbar').scroll(function () {
        if ($(this).attr("data-scrolling") == "false") {
            $('.linked-scrollbar').not(this).attr("data-scrolling", "true");
            $('.linked-scrollbar').not(this).scrollTop($(this).scrollTop());
        }
        $(this).attr("data-scrolling", "false");
    });

    $('.linked-horizontal-scrollbar').scroll(function () {
        if ($(this).attr("data-horizontal-scrolling") == "false") {
            $('.linked-horizontal-scrollbar').not(this).attr("data-horizontal-scrolling", "true");
            $('.linked-horizontal-scrollbar').not(this).scrollLeft($(this).scrollLeft());
        }
        $(this).attr("data-horizontal-scrolling", "false");
    });
}

function onResize() {
    var h = $(window).innerHeight() - 260;
    h = h > 10 ? h : 10;
    $('#left-dynamic-menu').css("max-height", h + "px");
    $('#right-display').css("max-height", (h) + "px");
    var allWidth = $('#progress-content').outerWidth();
    var rightWidth = allWidth - 350;
    $('#right-outmost-container').css("width", rightWidth + "px");
}


function loadAllActivities(loadAll) {
    var selected = $('#main-activities').val();
    $("#left-dynamic-menu").html("");
    $("#right-display").html("");

    promisExporter.changeFilters("");

    if (!selected && !loadAll)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AMainActivityMustBeSelectedInOrderToContinue);
    
    // set the getter for advanced filters (to be used in export)
    promisExporter.changeFilters(getAdvancedFilters(selected, loadAll));
    

    var url = GLOBAL_DATA.APP_ROOT + "/project/" + GLOBAL_DATA.PROJECT_ID + "/planning/partial/menu?mainActivity=" + encodeURIComponent(selected)
        + "&includeNonzero=false"
        + "&isWithCurves=false"
        + "&isRescheduleEditable=" + GLOBAL_DATA.setup.planning.isReschedule
        + "&isOverall=" + loadAll;

    //deselect if loadAll
    if (loadAll) {
        $('#main-activities').val("");
        if (GLOBAL_DATA.setup.planning.isReschedule == false)//if not already invisible
            url += "&isBaselineVisible=false";
    }
    else {
        url += "&isBaselineVisible=" + GLOBAL_DATA.setup.planning.isBaselineVisible;
    }
        

    callServer(url, {
        elementsToAddLoading: ['body'],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            if (!viewData || viewData.trim() == "" || viewData.trim().startsWith("<script>"))
                return displayNotify("There is no data for that selection.");

            // init left display and its events
            $('#left-dynamic-menu').html(viewData);

            // init the content for the right display
            if (GLOBAL_DATA.setup.planning.activitiesHTML && GLOBAL_DATA.setup.planning.activitiesHTML.trim() != "")
                $('#right-display').html(GLOBAL_DATA.setup.planning.activitiesHTML);
            makeCurrentMonthYellow();
        }
    });
}

function saveChanges() {
    var selected = $('#main-activities').val();
    if (!selected)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AMainActivityMustBeSelectedInOrderToContinue);

    var formData = [];
    var errors = [false];
    var messages = [
        GLOBAL_DATA.TRANSLATIONS.EncounteredInvalidValuesForProgressDistributionMakeSureTheyAreValidPercentAndInLinearProgress
    ];

    _.forEach($('.input-rescheduled'), function (element) {
        // get data
        var type = $(element).attr("data-type");
        var activity = $(element).attr("data-activity");
        var mhrs = $(element).attr("data-mhrs");
        var description = $(element).attr("data-description");
        var projectStart = $(element).attr("data-project-start");
        var projectEnd = $(element).attr("data-project-end");
        
        // go trough progress values
        var hasError = false;
        var first = "";
        var last = "";
        var tempVals = [];
        _.forEach($("[name='col-R-" + activity + "']"), function (input) {
            var inputVal = $(input).val();
            var tempVal = parseFloat(inputVal);

            setElementAsValid(input);
            $(input).parent("td").removeClass("red-colored");
            if (inputVal && inputVal != "" && (!_.isFinite(tempVal) || tempVal < 0 || tempVal > 100)) {
                setElementAsInvalid(input);
                console.log("invalid");
                return (hasError = true);
            }

            // find the closest left element
            var leftNeighbour = $(input).closest("td").prev().find('input');
            var leftVar = NaN;
            while (_.isNaN(leftVar)
                && $(leftNeighbour).get().length > 0) {
                    leftVar = parseFloat($(leftNeighbour).val());
                    leftNeighbour = $(leftNeighbour).closest("td").prev().find("input");
            }

            // find the closest left valued element
            var rightNeighbour = $(input).closest("td").next().find('input');
            var rightVar = NaN;
            while ($(rightNeighbour).get().length > 0
                && _.isNaN(rightVar)) {
                rightVar = parseFloat($(rightNeighbour).val());
                rightNeighbour = $(rightNeighbour).closest("td").prev().find("input");
            }

            //if(!_.isNaN(leftVar) || !_.isNaN(rightVar))
            //    console.log("Neighbours: ", leftVar, tempVal, rightVar);


            if (!_.isFinite(tempVal) || tempVal < 0 || tempVal > 100) {
                return;
            }

            if (tempVals.length >= 1 && tempVal < tempVals[tempVals.length - 1]) {
                setElementAsInvalid(input);
                console.log("invalid");
                return (hasError = true);
            }

            tempVals.push(tempVal);
            var thisDateHere = $(input).attr("data-month").padStart(2, "0") + "-" + $(input).attr("data-year");
            if (first == "")
                first = thisDateHere;
            last = thisDateHere;
        });

        if (hasError)
            return (errors[0] = true);

        // compute required months
        console.log(tempVals);
        var startDate = first;
        var endDate = last;
        var months = getMonthsFromStartUntilEnd(startDate, endDate);
        var monthsTostart = getMonthsFromStartUntilEnd(projectStart, startDate);
        var monthsToEnd = getMonthsFromStartUntilEnd(endDate, projectEnd);
        if (months <= 0)
            return;

        //tempVals = _.drop(tempVals, monthsTostart - 1);
        tempVals = _.take(tempVals, months);
        if (tempVals.length == 0)
            return;
        
        console.log("Info", startDate, endDate, monthsTostart, months, tempVals);

        var activityObject = {};
        activityObject["Code"] = activity;
        activityObject["Description"] = description;
        activityObject["Budget"] = mhrs;
        activityObject["StartDateR"] = startDate;
        activityObject["EndDateR"] = endDate;
        activityObject["RProgress"] = tempVals;

        activityObject["RProgress"] = tempVals;

        var existingIndex = _.findIndex(formData, { 'Code': activityObject.Code });
        if (existingIndex >= 0)
            formData[existingIndex] = _.extend(formData[existingIndex], activityObject);
        else
            formData.push(activityObject);
    });

    //console.log(formData);
    if (_.findIndex(errors, function (o) { return o == true; }) >= 0) {
        for (var i = 0; i < errors.length; i++) {
            if (errors[i])
                return displayNotify(messages[i]);
        }
    }

    // all good here, send to server
    formData = _.filter(formData, function (o) {
        return o["StartDateR"] && o["StartDateR"] != "" && o["EndDateR"] && o["EndDateR"] != "";
    });
    console.log(formData);
    if (formData.length == 0)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.NoDataToSave);

    var activity = $('#main-activities').val();
    var url = GLOBAL_DATA.APP_ROOT + "/api/project/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/activities/" + activity + "/related?isRescheduled=true";
    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(formData),
        elementsToAddLoading: ['body'],
        removeLoaderIfFail: true,
        successFunc: function () {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.SuccessfullySaved, "success");

            //make them red
            _.forEach($('.input-rescheduled'), function (element) {
                var activity = $(element).attr("data-activity");
                _.forEach($("[name='col-R-" + activity + "']"), function (input) {
                    var inputVal = $(input).val();
                    var tempVal = parseFloat(inputVal);
                    if (_.isFinite(tempVal) && tempVal >= 0) {
                        $(input).parent("td").addClass("red-colored");
                    }
                    else {
                        $(input).parent("td").removeClass("red-colored");
                    }
                });
            });
        }
    });
}

// loadAll specifies wether to get data for all main activities or only for one
function getAdvancedFilters(activity, loadAll) {
    if (loadAll)
        return "";

    var output = "?advfilters[0].leftoperand=act&advfilters[0].rightoperand=" + encodeURIComponent(activity) + "&advfilters[0].operator=inc";
    return output;
}