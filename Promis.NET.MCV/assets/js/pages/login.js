$(document).ready(function () {
    imgRandom();

    $('#login-btn').click(login);
    $("#loginform").keypress(pressEnter);
});

function login() {
    var formData = extractObjectFromForm("#loginform");

    if (!formData.Username || formData.Username == "")
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.UsernameRequired);

    if (!formData.Password || formData.Password == "")
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.PasswordRequired);

    var returnurl = getAllUrlParams(window.location.href)["returnurl"];
    formData.ReturnUrl = returnurl;
    var url = GLOBAL_DATA.APP_ROOT + "/api/account/login/pre";
    callServer(url, {
        callType: "POST",
        data: formData,
        elementsToAddLoading: [".side_signing_full"],
        removeLoaderIfFail: true,
        successFunc: function (data) {
            if (!data || !data.loadProjects)
                return;

            // will go here only if multiple projects
            loadProjectsList(data.username);
        }
    });
};

function loadProjectsList(username) {
    console.log(GLOBAL_DATA.TRANSLATIONS.LoadingProjectsFor, username);
    var url = GLOBAL_DATA.APP_ROOT + "/project/all/" + encodeURIComponent(username);
    callServer(url, {
        elementsToAddLoading: [".side_signing_full"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $(".modal .modal-content").css("width", "400px");
            open_modal_normal({
                title: GLOBAL_DATA.TRANSLATIONS.SelectAProject,
                content: viewData,
                footerHtml: "",
                version: GLOBAL_DATA.MODAL_VERSIONS.PROJECT_SELECTION
            });

            // assign click
            $(".project-details-container .info-proj-full").click(finalLogin);
        }
    });
}

function finalLogin() {
    // re-extract all the data
    var formData = extractObjectFromForm("#loginform");

    if (!formData.Username || formData.Username == "")
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.UsernameRequired);

    if (!formData.Password || formData.Password == "")
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.PasswordRequired);

    var returnurl = getAllUrlParams(window.location.href)["returnurl"];
    formData.ReturnUrl = returnurl;

    var projectId = $(this).attr("data-projectid");
    if (!projectId || projectId == "")
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.InvalidProjectId);
    formData.ProjectId = projectId;

    // send it to server. if success, it will redirrect
    var url = GLOBAL_DATA.APP_ROOT + "/api/account/login";
    callServer(url, {
        callType: "POST",
        data: formData,
        elementsToAddLoading: [".modal-content"],
        removeLoaderIfFail: true
    });
}

function imgRandom() {
    var images = ['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg'];
    var imgPath = GLOBAL_DATA.APP_ROOT + "/assets/img/" + images[Math.floor(Math.random() * images.length)];
    $('#bg').css("background", "url(" + imgPath + ") no-repeat center center fixed");
}

function pressEnter(event) {
    if (event.which == 13) {
        login();
    }
};