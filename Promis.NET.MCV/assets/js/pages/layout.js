﻿$(document).ready(function () {
    // close menu if not on homepage
    if (window.location.href != GLOBAL_DATA.APP_ROOT && window.location.href != (GLOBAL_DATA.APP_ROOT + '/')) {
        $('body').addClass("menuclose");
    }
    else {
        $('body').removeClass("menuclose");
    }

    $("body").removeClass("loading");
    $("body").removeClass("loading-body");

    //click on change theme
    $('.theme-changer').click(doSwitchTheme);

    //click on profile info
    var url = GLOBAL_DATA.APP_ROOT + "/account/details";
    $('#user-profile-info').click(open_modal.bind(null, url, {
        title: GLOBAL_TRANSLATIONS.PersonalInformation,
        version: GLOBAL_DATA.MODAL_VERSIONS.PERSONAL_INFORMATION
    }));

    // click on logout
    $('#logout-btn').click(doLogout);

    //modal clear contents on close
    $(".modal").on("hidden.bs.modal", function () {
        $(".modal-body").html("");
        $("#modal-footer-content").html("");
        $(".modal-content").css("width", "auto");
        $(".modal-content").css("height", "auto");
    });

    // initialize weather
    addLayoutWeather();
});

// THEME
function doSwitchTheme() {
    var theme = $(this).attr("data-theme");
    switch_theme(theme);

    callServer(GLOBAL_DATA.APP_ROOT + "/api/account/theme", {
        callType: "PUT",
        contentType: "application/json",
        data: '"' + theme + '"',
        elementsToAddLoading: [],
        removeLoaderIfFail: true
    });
}

// LOGOUT
function doLogout() {
    var url = GLOBAL_DATA.APP_ROOT + "/api/account/logout";
    callServer(url, {
        callType: "POST"
    });
}

$(".tab_setup li").on("click", function () {
    $(".tab_setup li").removeClass("active");
    $(this).addClass("active");
});

function addLayoutWeather() {
    var location = $(".project-weather-container").attr("data-coordinates");
    $.simpleWeather({
        location: location,
        woeid: '',
        unit: 'c',
        success: function (weather) {
            var html = `
                <div>`+
                GLOBAL_TRANSLATIONS.SiteMeteo +
                    `<br>
                    <span>`
                        + weather.temp + `&deg;` + weather.units.temp + `
                    </span>
                </div>`;
            $(".project-weather-container").html(html);
            $(".project-weather-container div").click(initializeLayoutWeather);
        },
        error: function (error) {
            $(".project-weather-container").html("<p style='margin:0'>" + GLOBAL_TRANSLATIONS.CantLoadbrweather+" </p>");
        }
    });
}

function initializeLayoutWeather() {
    var location = $(".project-weather-container").attr("data-coordinates");
    $.simpleWeather({
        location: location,
        woeid: '',
        unit: 'c',
        success: function (weather) {
            var objDate = new Date(),
                locale = "en-us",
                month = objDate.toLocaleString(locale, { month: "long" }),
                day = objDate.getDate(),
                weekday = objDate.toLocaleString(locale, { weekday: "long" });

            var html = `<div class="row weather-row-medatada">
                            <div class="col-md-6 weather-div-monthname">
                                <div class="weather-div-names weather-div-topname">` + month.toUpperCase() + `</div>
                                <div class="weather-div-bignames">` + day + `</div>
                                <div class="weather-div-names weather-div-butname">` + weekday + `</div>
                            </div>
                            <div class="col-md-10 weather-div-bigtemp">` + weather.temp + `<span>&deg;` + weather.units.temp + `</span></div>
                        </div> 
                        <div class="row weather-row-details">
                            <div class="col-md-16">
                                <span class="weather-div-subdetail">`+
                                GLOBAL_TRANSLATIONS.TodaysHigh + `<span class="subdetail-big">` + weather.high + `&deg; ` + weather.units.temp + `</span> - Low: <span class="subdetail-big">` + weather.low + `&deg; ` + weather.units.temp + `</span><br/>
                                <span class="weather-div-subdetail">`+
                                    GLOBAL_TRANSLATIONS.Wind + `<span class = "subdetail-big" > ` + weather.wind.direction + ` ` + weather.wind.speed + ` ` + weather.units.speed + `</span> - ` + GLOBAL_TRANSLATIONS.Chill + `: <span class="subdetail-big">` + weather.wind.chill + `</span> <br />
                                <span class="weather-div-subdetail"> Currently: <span class="subdetail-big">` + weather.currently + `</span><br/>
                                <span class="weather-div-subdetail">`+
                                    GLOBAL_TRANSLATIONS.Humidity + `<span class = "subdetail-big" > ` + weather.humidity + `</span> - ` + GLOBAL_TRANSLATIONS.Visibility + `: <span class="subdetail-big">` + weather.visibility + `</span> <br />
                                <span class="weather-div-subdetail">`+
                                    GLOBAL_TRANSLATIONS.Sunrise +`: <span class="subdetail-big">` + weather.sunrise + `</span> - ` + GLOBAL_TRANSLATIONS.Sunset + `: <span class="subdetail-big">` + weather.sunset + `</span><br/>
                            </div>
                        </div>
                        <div class="row weather-row-medatada"><div class="col-md-12">` + GLOBAL_TRANSLATIONS.ForecastNextDays +`</div ></div >
                        <div class="row weather-row-next">
                            <div class="col-md-16">
                                @contentForNext5Days
                            </div>
                        </div>`;
            var nextFiveDays = "";
            for (var j = 1; j <= 5; j++) {
                nextFiveDays += `
                        <div class="col-md-3 weather-next-cell weather-div-subdetail">`
                            + weather.forecast[j].day + `<br>
                            <span class="subdetail-big">` + weather.forecast[j].high + `&deg;` + weather.units.temp + `</span>
                        </div>`;
            }
            html = html.replace("@contentForNext5Days", nextFiveDays);

            $('#modal-container .modal-content').css("width", "270px");
            open_modal_normal({
                title: GLOBAL_TRANSLATIONS.ProjectSiteMeteoConditions,
                footerHtml: "",
                content: html,
                version: GLOBAL_DATA.MODAL_VERSIONS.WEATHER_INFORMATION
            });
        },
        error: function (error) {
            $('#modal-container .modal-body').css("width", "270px");
            open_modal_normal({
                title: GLOBAL_TRANSLATIONS.ProjectSiteMeteoConditions,
                footerHtml: "",
                content: "<p style='margin:0'>" + GLOBAL_TRANSLATIONS.MeteoStationNotFound + "</p> ",

            });
        }
    });
}