﻿$(document).ready(function () {

    configure_chart(GLOBAL_DATA.USER_THEME);

    // init progress charts
    callServer(GLOBAL_DATA.APP_ROOT + "/api/charts/delay", {
        successFunc: function (data) {
            progress_init_chart(data.ProgressChartLabels, data.ProgressChartValuesPlanned, data.ProgressChartValuesActual, data.ProgressChartValuesCertified, data.ProgressMonthlyChartValuesActual, data.ProgressMonthlyChartValuesDelay);
        },
        elementsToAddLoading: ['#progress-overall', '#progress-monthly']
    });

    // init activities charts
    var container1 = '#kpis-container-planned';
    var container2 = '#kpis-container-actual';
    var container3 = '#kpis-container-certified';
    callServer(GLOBAL_DATA.APP_ROOT + "/api/charts/trends", {
        successFunc: function (data) {
            trends_init_data(GLOBAL_DATA.TRANSLATIONS.Planned.toUpperCase(), '1', container1, data.TrendingChartPlannedPercent, data.TrendingChartPlannedPercentWeekly, data.TrendingChartPlannedPercentMonthly);
            trends_init_data(GLOBAL_DATA.TRANSLATIONS.Physical.toUpperCase(), '2', container2, data.TrendingChartActualPercent, data.TrendingChartActualPercentWeekly, data.TrendingChartActualPercentMonthly);
            trends_init_data(GLOBAL_DATA.TRANSLATIONS.Certified.toUpperCase(), '3', container3, data.TrendingChartCertifiedPercent, data.TrendingChartCertifiedPercentWeekly, data.TrendingChartCertifiedPercentMonthly);

            $('.progress_profile1').circleProgress({
                fill: { gradient: ["#1aa7cf", "#15568c"] },
                lineCap: 'butt'
            });
            $('.progress_profile2').circleProgress({
                fill: { gradient: ["#21dd0d", "#12610a"] },
                lineCap: 'butt'
            });
            $('.progress_profile3').circleProgress({
                fill: { gradient: ["#cfaf1a", "#61520a"] },
                lineCap: 'butt'
            });
        },
        elementsToAddLoading: ['#kpis-container-outer']
    });

    // init activities charts
    callServer(GLOBAL_DATA.APP_ROOT + "/api/charts/activities", {
        successFunc: function (data) {
            activities_init_chart(data.ActivitiesCodes, data.ActivitiesPlannedData, data.ActivitiesActualData);
            activities_init_table(data.ActivitiesWithProgresses);
        },
        elementsToAddLoading: ['#main-activities-overall']
    });

    //init outstandings charts
    callServer(GLOBAL_DATA.APP_ROOT + "/api/charts/outstandings", {
        successFunc: function (data) {
            outstandings_init_chart(data.OutstandingChartFlags, data.OutstandingChartCounts);
            outstandings_init_table(data.OutstandingChartFlags, data.OutstandingChartFlagDescriptions);
            outstandings_init_counts(data.PunchlistCount, data.PunchlistDoneCount, data.ClearancePercentage);

            $('.progress_profile4').circleProgress({
                fill: { gradient: ["#1aa7cf", "#15568c"] },
                lineCap: 'butt'
            });
        },
        elementsToAddLoading:  ['#outstanding-container']
    });
});

function progress_init_chart(labels, data_planned, data_actual, data_certified, data_actual_m, data_delayed_m) {
    //progress overall
    var config = {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: GLOBAL_DATA.TRANSLATIONS.Planned,
                data: data_planned,
                borderColor: 'rgba(54, 162, 235, 1)',
                backgroundColor: 'rgba(54, 162, 235, 0.5)',
                fill: false
            },
            {
                label: GLOBAL_DATA.TRANSLATIONS.Physical,
                data: data_actual,
                borderColor: 'rgba(4,110,23,1.00)',
                backgroundColor: 'rgba(4,110,23,0.3)',
                fill: false
            },
            {
                label: GLOBAL_DATA.TRANSLATIONS.Certified,
                data: data_certified,
                borderColor: 'rgba(255, 159, 64,1)',
                backgroundColor: 'rgba(255, 159, 64, 0.5)',
                fill: false
            }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff",
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.05)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff"
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.05)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    }
                }]
            },
            plugins: {
                filler: {
                    propagate: true
                }
            }
        }
    };

    // progress monthly
    var configm = {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: GLOBAL_DATA.TRANSLATIONS.Physical,
                data: data_actual_m,
                borderColor: 'rgba(4,110,23,1.00)',
                backgroundColor: 'rgba(4,110,23,0.3)',
                fill: false
            },
            {
                label: GLOBAL_DATA.TRANSLATIONS.Delay,
                data: data_delayed_m,
                borderColor: 'rgba(145,20,38,1.00)',
                backgroundColor: 'rgba(145,20,38,0.5)',
                fill: false
            }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff",
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.05)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff"
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.05)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    }
                }]
            }
        }
    };

    var ctx = $("#pr-overall");
    var ctxm = $("#pr-monthly");
    var lineChartProgress = new Chart(ctx, config);
    var lineChartProgressM = new Chart(ctxm, configm);
}

function trends_init_data(title, style, container, percent, percent_w, percent_m) {
    var percentR1 = (percent / 100).toFixed(1);
    var percentR2 = percent.toFixed(1);
    var percent_wR1 = (percent_w / 100).toFixed(1);
    var percent_wR2 = percent_w.toFixed(1);
    var percent_mR1 = (percent_m / 100).toFixed(1);
    var percent_mR2 = percent_m.toFixed(1);

    var html = `<div class="col-md-1 progress-profile">
                    <p class="vertical-text page_title">` + title + `</p>
                </div>
                <div class="col-md-5 progress-profile">
                    <h6 class="dashboard-h6"><small>`+ GLOBAL_DATA.TRANSLATIONS.Today + `</small></h6>
                    <div class="progressprofile col">
                        <div class="progress_profile` + style + `" data-value="` + percentR1 + `" data-size="80" data-thickness="6" data-animation-start-value="0" data-reverse="false"></div>
                        <div class="progress-perc-val"><h6>` + percentR2 + `%</h6></div>
                    </div>
                </div>
                <div class="col-md-5 progress-profile">
                    <h6 class="dashboard-h6"><small>`+ GLOBAL_DATA.TRANSLATIONS.LastWeek + `</small></h6>
                    <div class="progressprofile col">
                        <div class="progress_profile` + style + ` progress-profile" data-value="` + percent_wR1 + `" data-size="80" data-thickness="6" data-animation-start-value="0" data-reverse="false"></div>
                        <div class="progress-perc-val"><h6>` + percent_wR2 + `%</h6></div>
                    </div>
                </div>
    

                <div class="col-md-5 progress-profile">
                    <h6 class="dashboard-h6"><small>` + GLOBAL_DATA.TRANSLATIONS.LastMonth +`</small></h6>
                    <div class="progressprofile col">
                        <div class="progress_profile` + style + ` progress-profile" data-value="` + percent_mR1 + `" data-size="80" data-thickness="6" data-animation-start-value="0" data-reverse="false"></div>
                        <div class="progress-perc-val"><h6>` + percent_mR2 + `%</h6></div>
                    </div>
                </div>`;

    $(container).html(html);
}

function activities_init_chart(codes, data_planned, data_actual) {
    var plannedBgColors = [];
    var plannedBorderColors = [];
    var physicalBgColors = [];
    var physicalBorderColors = [];
    for (code in codes) {
        plannedBgColors.push('rgba(54, 162, 235, 0.8)');
        plannedBorderColors.push('rgba(54, 162, 235, 1)');
        physicalBgColors.push('rgba(4,110,23, 0.7)');
        physicalBorderColors.push('rgba(4,110,23, 1)');
    }

    var data = {
        labels: codes,
        datasets: [{
            label: GLOBAL_DATA.TRANSLATIONS.Planned,
            backgroundColor: plannedBgColors,
            borderColor: plannedBorderColors,
            borderWidth: 1,
            data: data_planned
        },
        {
            label: GLOBAL_DATA.TRANSLATIONS.Physical,
            backgroundColor: physicalBgColors,
            borderColor: physicalBorderColors,
            borderWidth: 1,
            data: data_actual
        }
        ]
    };

    var ctx = $("#main-activ-chart");
    var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: data
    });
}

function activities_init_table(activities) {

    var tableElement = $('<table class="table table-bordered table-sm"></table>');
    for (i in activities) {
        var activity = activities[i];
        var html = `<tr>
                        <td>` + activity.Code + `</td>
                        <td>
                            <p class="no-margin">` + activity.Description + `</p>
                        </td>
                        <td align="right">
                            <p class="no-margin blue-background black-text">
                                <b>` + activity.ProgressPlanned + `</b>&nbsp;
                            </p>
                        </td>
                        <td align="right">
                            <p class="no-margin green-background black-text">
                                <b>` + activity.ProgressActual + `</b>&nbsp;
                            </p>
                        </td>
                    </tr>`;
        tableElement.append(html);
    }
    $('#main-activities-overall-table').html(tableElement);
}

function outstandings_init_chart(flags, counts) {
    var bgColors = [];
    var borderColors = [];
    for (flag in flags) {
        bgColors.push('rgba(75, 192, 192, 0.5)');
        borderColors.push('rgba(75, 192, 192, 1)');
    }

    var data_outs = {
        labels: flags,
        datasets: [{
            label: "",
            backgroundColor: bgColors,
            borderColor: borderColors,
            borderWidth: 1,
            data: counts,
        }]
    };

    var ctx = $("#outstandings-chart");
    var myBarChart2 = new Chart(ctx, {
        type: 'bar',
        data: data_outs,
        options: {
            legend: {
                display: false
            },
            responsive: false
        }
    });
}

function outstandings_init_table(flags, descriptions) {
    var tableElement = $('<table id="outstanding-info-table" class="table table-sm"></table>');
    for (i in flags) {
        var flag = flags[i];
        var description = descriptions[i];
        var html = `<tr>
                        <td class="outstanding-table-flag h6">
                            <b>` + flag + `</b>
                        </td>
                        <td class="h6 outstanding-table-desc">
                            <small>` + description + `</small>
                        </td>
                    </tr>`;
        tableElement.append(html);
    }
    $('#outstanding-info').html(tableElement);
}

function outstandings_init_counts(count, donecount, clearancePercentage) {

    var percentage_rounded = (clearancePercentage / 100).toFixed(1);
    var html = `<div class="row no-margin">
                    <div class="col">
                        <label class="ribbon right danger">
                            <span><small>`+ GLOBAL_DATA.TRANSLATIONS.Outs +`</small> ` + count + ` </span>
                        </label>
                    </div>
                    <div class="col">
                        <label class="ribbon right success">
                            <span><small>`+ GLOBAL_DATA.TRANSLATIONS.Done + `</small> ` + donecount + ` </span>
                        </label>
                    </div>
                </div>
                <div class="row" style="margin-top: 30px">
                    <div class="progress_profile4 col" style="text-align: center" data-value="` + percentage_rounded + `" data-size="130" data-thickness="10" data-animation-start-value="0" data-reverse="false"></div>
                    <div style="position: absolute; top: 90px; left: 0; right: 0; text-align: center">
                        <h6 class="dashboard-h6-simple"><small>`+ GLOBAL_DATA.TRANSLATIONS.Clearancebrpercentage + `</small>&nbsp;</h6>
                        <h5><br>` + clearancePercentage + `%</h5>
                    </div>
                </div>`;
    $('#outstandings-counts').html(html);
}