﻿var modalUserId = 0;
function initModalUser() {

    callServer(GLOBAL_DATA.APP_ROOT + "/api/account/details", {
        successFunc: function (data) {

            if (data.PhotoUrl && data.PhotoUrl !== "") {
                changeIcon(document.getElementById("user-picture-modal"), data.PhotoUrl ? data.PhotoUrl : "");
            }

            modalUserId = data.UserId;
            $('#FirstNamemodal').val(data.FirstName);
            $('#LastNamemodal').val(data.LastName);
            $('#UserNamemodal').val(data.UserName);
            $('#Passwordmodal').val(data.Password);
            $('#Emailmodal').val(data.Email);
            $('#OfficePhonemodal').val(data.OfficePhone);
            $('#Countrymodal').val(data.Country);
            $('#MobilePhonemodal').val(data.MobilePhone);
            $('#Companymodal').val(data.Company);

            var lang = data.Language !== null ? data.Language : "en";
            $("#lang-" + lang + "modal").prop("checked", true);
        },
        elementsToAddLoading: ['.modal-content']
    });

    // Image handling
    setUpImageEncoding();

    /*Area of upload file/update info*/
    setTriggersOnImageButton();

    // Add footer to the page
    setFooter();
}

initModalUser();

function setUpImageEncoding() {
    $("#profile-modal-image").change(function () {
        encodeImageFileAsURL("profile-modal-image", "user-picture-modal");
    });
}

function setTriggersOnImageButton() {
    /*Area of upload file/update info*/
    $("html").addClass('js');
    var fileInput = $(".input-file"),
        button = $(".input-file-trigger"),
        the_return = $(".file-return");

    button.keydown(function (event) {
        if (event.keyCode === 13 || event.keyCode === 32) {
            fileInput.focus();
        }
    });

    button.click(function () {
        fileInput.focus();
        return false;
    });

    fileInput.change(function () {
        the_return.html(GLOBAL_TRANSLATIONS.ClickOnBupdateProfilebToMakeTheChangeEffective);
    });
}

/*modal footer body*/
function setFooter() {
    var footer = `<div class='pull-right'>
                    <button id='update-profile-btn' class='btn btn-success btn-sm'>` + GLOBAL_TRANSLATIONS.UpdateProfile + `</button>
                </div>`;

    $('#modal-container #modal-footer-content').html(footer);
    $('#update-profile-btn').click(doUpdateUser);
}

 /*end of modal footer*/
function doUpdateUser() {
    var url = GLOBAL_DATA.APP_ROOT + "/api/account/" + modalUserId + "?type=basic";
    var formData = extractObjectFromFormModal("#form_profile");
    formData.UserId = modalUserId;
    formData.PhotoUrl = $("#user-picture-modal").attr("src");
    console.log(formData);

    //console.log(formData);
    var formSerialized = JSON.stringify(formData);

    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: formSerialized,
        elementsToAddLoading: [".modal-content"],
        removeLoaderIfFail: true,
        successFunc: function () {
            displayNotify(GLOBAL_TRANSLATIONS.DataUpdatedSuccessfully, "success");

            //update user contents
            var name = "";
            name = formData.FirstName !== "" ? formData.FirstName : "";
            name += " " + (formData.LastName !== "" ? formData.LastName : "");
            name = name.trim();
            name = name !== "" ? name : formData.UserName;
            $("#user-details-name").html(GLOBAL_TRANSLATIONS.Welcome + " <br/>");
            $("#user-details-name").append(name);
            $("#user-location").text(formData.Company);
            $("#user-details-picture").attr("src", formData.PhotoUrl);

            //reresh
            setTimeout(function () { window.location.reload(); }, 1000);
        }
    });
}