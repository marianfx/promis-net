﻿var gridHistory = new VirtualLoader({
    workingPath: "/engineering/grid",
    reinitializeMethod: getContentReadyForGrid,
    titleGetter: function () {
        var location = window.location.href;
        var params = getAllUrlParams(location);
        var tableTitle = params.hasOwnProperty("tabletitle") ? params.tabletitle : (params.hasOwnProperty("datatable") ? params.datatable : "");
        return tableTitle.toUpperCase();
    },
    stateDataGetter: function (url, title) {
        var params = getAllUrlParams(url);
        return {
            dataTable: params.datatable,
            tableTitle: title
        };
    }
});
var advFiltersQueryString = "";
var column_operations = [];

var basicGridUrl = GLOBAL_DATA.APP_ROOT + "/engineering/grid?dataTable=@dataTable&tableTitle=@tableTitle";

$(document).ready(function () {
    $('.dropdown-submenu a.test').on("click", function (e) {
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });
    assignLoading();
    $('#create-tags-button').click(onClickCreateTags);

    //show subitems of dropdown-items
    $(".dropdown-menu,.dropdown-submenu").mouseenter(function () {
        $(this).children(".dropdown-menu").show();
    });
    //hide subitems of dropdown-items
    $(".dropdown-menu,.dropdown-submenu").mouseleave(function () {
        $(this).children(".dropdown-menu").hide();
    });
});

function assignLoading() {
    $(".dd-item").click(function () {
        column_operations = [];
        advFiltersQueryString = "";

        //1/ window.href = url;
        var dataTable = $(this).attr('data-table-name');
        var tabTitle = $(this).attr('data-title');

        if (dataTable != null && dataTable != undefined && tabTitle != null && tabTitle != undefined) {
            tabTitle = tabTitle.toUpperCase();
            var url = basicGridUrl.replace("@dataTable", dataTable).replace("@tableTitle", tabTitle);
            //check if coming from tags
            if (window.location.href.endsWith("engineering/tags"))
                return (window.location.href = url);
            gridHistory.virtualNavigateToUrl({ url: url, title: tabTitle});
        }
        else
            displayNotify(GLOBAL_DATA.TRANSLATIONS.ThereIsNoDataForThisSelection);
        $(this).closest(".dropdown-menu").prev().dropdown("toggle");
    });
}

//for create tags using schemas

function onClickCreateTags() {
  
    window.location.href = GLOBAL_DATA.APP_ROOT + "/engineering/tags"; 
}
