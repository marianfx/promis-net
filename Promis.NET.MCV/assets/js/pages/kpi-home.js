﻿var modified = [];

// ### REGION INIT
function initKpiHome() {

    //assign clicks
    $("#favorites-button").click(onClickSetupFavorites);
    $("#favorites-view-button").click(onClickViewFavorites);
    $("#favorites-view-button").click();
    $(".left-clickable").click(onClickKpiCategory);
}

initKpiHome();
// ### END

function onClickSetupFavorites() {
    var isVisible = $("#favorites-container").is(":visible");
    $("#favorites-container").toggle();
    $("#favorites-container").html("");
    if (isVisible)
        return;//closed it

    // load display
    var url = GLOBAL_DATA.APP_ROOT + "/kpis/" + GLOBAL_DATA.PROJECT_ID + "/favorites/partial";
    callServer(url, {
        elementsToAddLoading: ["#favorites-container"],
        removeLoaderIfFail: true,
        successFunc: function (viewHtml) {
            $("#favorites-container").html(viewHtml);
            assignFavoritesChangeEvent();
            $("#favorites-save-button").click(onClickSaveFavorites);
        }
    });
}

function assignFavoritesChangeEvent() {
    modified = [];
    $(".favorite-toggle").change(function(){
        var checked = $(this).is(":checked");
        var code = $(this).attr("data-code");
        var oldVal = $(this).attr("data-initial");
        var oldValBool = oldVal == "T" ? true : false;

        console.log("changed", checked, code, oldVal);
        _.remove(modified, function (o) {
            return o.Key == code;
        });

        if (oldValBool == checked)//unchanged
            return;

        modified.push({
            Key: code,
            Value: checked
        });
    });
}

function onClickSaveFavorites() {
    console.log("saved", modified);
    if (!_.isArray(modified) || modified.length == 0)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.NoChangesMade);

    var url = GLOBAL_DATA.APP_ROOT + "/api/kpis/" + GLOBAL_DATA.PROJECT_DATABASE + "/favorites/";

    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(modified),
        elementsToAddLoading: ["#favorites-container"],
        removeLoaderIfFail: true,
        successFunc: function () {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.DataSaved, "success");
            setTimeout(onClickViewFavorites, 300);
        }
    });
}

function onClickViewFavorites() {
    loadKpis.bind(this)();
}

function onClickKpiCategory() {
    loadKpis.bind(this)($(this).attr("data-id"));
}

function loadKpis(category) {
    $(".spento").css("background-color", "transparent");
    $(this).css("background-color", "rgba(255,255,255,0.3)")
    var title = $(this).attr("data-title");

    $("#favorites-container").fadeOut();
    $("#kpis-container").html("");

    var url = GLOBAL_DATA.APP_ROOT + "/kpis/" + GLOBAL_DATA.PROJECT_ID + "/load/partial";
    if (_.isString(category) && category.trim() != "")
        url += "?code=" + encodeURIComponent(category);
    callServer(url, {
        elementsToAddLoading: [".act_right"],
        removeLoaderIfFail: true,
        successFunc: function (viewHtml) {
            $("#kpis-container").html(viewHtml);

            // init progresss circles
            $('.progress_profile_onw').circleProgress({
                fill: {
                    gradient: ['rgba(168,99,0,0.7)', 'rgba(168,99,0,1.00)']
                },
                lineCap: 'butt'
            });
            $('.progress_profile_done').circleProgress({
                fill: {
                    gradient: ['rgba(3,94,28,0.7)', 'rgba(3,94,28,1.0)']
                },
                lineCap: 'butt'
            });
            $('.progress_profile_na').circleProgress({
                fill: {
                    gradient: ['rgba(107,2,3,0.7)', 'rgba(107,2,3,1.0)']
                },
                lineCap: 'butt'
            });

            // assign title
            $("#right-subtitle").text(title);
        }
    });
}