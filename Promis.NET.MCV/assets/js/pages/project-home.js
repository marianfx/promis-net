﻿// ### REGION INIT
function initSetupHome() {

    //function for resizing map and other elements of the setup page////
    $(window).resize(onResize);
    
    //functions for general info
    initDatepicker();
    $('#reload-chart').click(loadChartFromDates);

    // init data
    loadProject();
    loadChartFromDates();

    //assign clicks
    $('#save-general').click(onClickSaveProject.bind(null, "general"));
    $('#save-logo').click(onClickSaveProject.bind(null, "logo"));
    $('#save-activities').click(onClickSaveActivities);

    //function for fullscreen of the map
    onResize();
    loadMap();
    $('.fullscreen-btn').on('click', onClickFullScreen);
}

initSetupHome();
// ### END


// ### Load Data
function loadProject() {
    $('#logo-display').attr("src", $('.project-logo').attr("src"));
    $('#logo-display-homepage').attr("src", $('.project-logo').attr("src"));
    setUpProjectLogoImageEncoding();
}

function setUpProjectLogoImageEncoding() {
    $("#logo-input").change(function () {
        encodeImageFileAsURL("logo-input", "logo-display", function () {
            $('#logo-display-homepage').attr("src", $('#logo-display').attr("src"));
            setTimeout(onResize, 100);
        });
    });
}

function initDatepicker() {
    $("#StartDate").datepicker({
        format: "mm-yyyy",
        autoclose: true,
        viewMode: "months",
        minViewMode: "months",
        language: GLOBAL_DATA.USER_LANGUAGE
    });
    $("#EndDate").datepicker({
        format: "mm-yyyy",
        autoclose: true,
        viewMode: "months",
        minViewMode: "months",
        language: GLOBAL_DATA.USER_LANGUAGE
    });

    $('#range.input-daterange').datepicker({
        startView: 1,
        clearBtn: true,
        orientation: "bottom auto",
        daysOfWeekDisabled: "0,6",
        calendarWeeks: true,
        autoclose: true,
        language: GLOBAL_DATA.USER_LANGUAGE
    });
}

function loadChartFromDates() {
    var months = getMonthsFromStartUntilEnd($("#StartDate").val(), $("#EndDate").val())
    if (months <= 0)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.InvalidSelectedStartAndEndDates);

    var values = getMonthsDistribution(months);
    var labels = [];
    _.forEach(values, function (v, k) {
        labels.push(k + 1);
    });
    displayCurve(labels, values, GLOBAL_DATA.USER_THEME);
}

function displayCurve(labels, values, theme) {
    Chart.defaults.global.defaultFontFamily = "light";
    Chart.defaults.global.defaultFontSize = 12;
    switch (theme) {
        case "light":
            Chart.defaults.global.defaultFontColor = "#333";
            break;
        default:
            Chart.defaults.global.defaultFontColor = "#fff";
            break;
    }
    Chart.defaults.global.animation.duration = 5000;
    Chart.defaults.global.responsive = true;
    Chart.defaults.global.maintainAspectRatio = false;
    Chart.defaults.global.title.display = false;
    Chart.defaults.global.tooltips.mode = "label";
    Chart.defaults.global.hover.mode = "dataset";

    var config = {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: "",
                data: values,
                borderColor: 'rgba(54, 162, 235, 1)',
                backgroundColor: 'rgba(54, 162, 235, 0.5)',
                fill: true
            }]
        },
        options: {
            elements: {
                point: {
                    radius: 0
                }
            },
            responsive: true,
            maintainAspectRatio: true,
            title: {
                display: false,
                text: ''
            },
            legend: {
                display: false
            },
            tooltips: {
                mode: 'label'
            },
            hover: {
                mode: 'dataset'
            },
            scales: {
                xAxes: [{
                    display: true,
                    interval: 5,
                    scaleLabel: {
                        show: true,
                        labelString: 'Month'
                    },
                    ticks: {
                        fontColor: "#b7c8ff",
                        maxRotation: 0,
                        minRotation: 0,
                        autoSkip: true
                    }
                }],
                yAxes: [{
                    interval: 4,
                    display: true,
                    scaleLabel: {
                        show: true,
                        labelString: 'Value'
                    },
                    ticks: {
                        suggestedMin: 0,
                        suggestedMax: 100,
                    }
                }]
            }
        }
    };
    var canvas = $("#planned_pro");
    window.myLine = new Chart(canvas, config);
}
// ### End


// ### REGION General Events
function onResize() {
    var logoContainerHeight = $(".block_logo").outerHeight();
    var activitiesHeight = logoContainerHeight;//these must have the same height

    var generalInfoContainerHeight = $(".card-cont").height();
    var divMapHeight = generalInfoContainerHeight - logoContainerHeight - 35;//35 is the header
    var mapHeight = divMapHeight - 69 - 25; // 69 is almost 2xheader (header + footer), 25 is margin top

    mapHeight = Math.round(mapHeight);
    activitiesHeight = Math.round(activitiesHeight);
    $(".block_disc").css("max-height", activitiesHeight + "px")
    $("#map").css("height", mapHeight + "px");
}

function onClickFullScreen() {
    $(this).closest(".full-screen-container").toggleClass("fullscreen");

    if ($(".full-screen-container").hasClass("fullscreen")) {
        $("#map").css("height", $(window).innerHeight() - 127 + "px");
    }
    else {
        onResize();
    }

    google.maps.event.trigger(map, 'resize');
    // added re-center on resize
    if (markers && markers[0] && markers[0].position)
        map.setCenter(markers[0].position);
}
// ### END


// ### REGION Save Buttons
function onClickSaveProject(type) {
    var formData = extractObjectFromForm("#first-form");
    formData.LogoUrl = $("#logo-display").attr("src");
    formData.Location = projectData.location;
    formData.LocationCoordinates = projectData.geolocation;

    var loadingAdd = [];
    if (type == "logo")
        loadingAdd.push(".block_logo");
    else if (type == "general")
        loadingAdd.push(".tab-content");
    
    var url = GLOBAL_DATA.APP_ROOT + "/api/project/" + GLOBAL_DATA.PROJECT_ID + "?type=" + encodeURIComponent(type);
    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        data: formData,
        successFunc: function () {
            $('.project-logo').attr("src", formData.LogoUrl);// update proj logo
            $('.project-location-header').text(formData.Location);//update proj location
            $(".project-weather-container").attr("data-coordinates", formData.LocationCoordinates);// update and trigger weather location update
            if (_.isFunction(addLayoutWeather))
                addLayoutWeather();
            loadChartFromDates();//also reinit chart
            displayNotify(GLOBAL_DATA.TRANSLATIONS.ProjectDetailsUpdatedWithSuccess, "success");
        },
        elementsToAddLoading: loadingAdd,
        removeLoaderIfFail: true,
    });
}

function onClickSaveActivities() {
    var formData = transformObjectToDataCellArray(extractObjectFromForm("#main-activities-container"));
    //console.log(formData);

    var url = GLOBAL_DATA.APP_ROOT + "/api/project/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/activities";
    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(formData),
        successFunc: function () {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.ProjectActivitiesUpdatedWithSuccess, "success");
        },
        elementsToAddLoading: ['.block_disc'],
        removeLoaderIfFail: true
    });
}
// #END


// ### Maps Related
function loadweather(loc) {
    $.simpleWeather({
        location: loc,
        woeid: '',
        unit: 'c',
        success: function (weather) {
            html = '<h2><i class="icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2>';
            $("#weather").html(html);
        },
        error: function (error) {
            $("#weather").html("<p style='margin:0'>" + error + "</p>");
        }
    });
}

var label = '';
var markers = [];
var map;
var geocoder;
var current_location = {
    lat: 47.1584549,
    long: 27.601441799999975
};

function loadMap() {
    if (window.google != undefined && window.google.maps != undefined)
        return initMap();

    $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyBsWa9tc7MUm2Jf4cFYoxU3OamBF20lICM&libraries=places", function () {
        initMap()
    });
}

function initMap() {
    // assign proper sizes;
    onResize();

    // init stuff from db
    $("#address").val(projectData.location);
    loadweather(projectData.geolocation);
    $(".lat").html(GLOBAL_DATA.TRANSLATIONS.Latitude + projectData.latitude);
    $(".lon").html(GLOBAL_DATA.TRANSLATIONS.Longitude + projectData.longitude);

    var centerLatLng = new google.maps.LatLng(parseFloat(projectData.latitude), parseFloat(projectData.longitude));
    geocoder = new google.maps.Geocoder();
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        mapTypeId: 'satellite',
        center: centerLatLng
    });
    addMarker(centerLatLng, map);

    document.getElementById('submit').addEventListener('click', function () {
        var address = document.getElementById('address').value;
        geocodeAddress(geocoder, map, address, null);
    });

    // This event listener calls addMarker() when the map is clicked.
    google.maps.event.addListener(map, 'click', function (event) {
        geocodeAddress(geocoder, map, null, event.latLng);
    });
    
    var autocomplete = new google.maps.places.Autocomplete(address);
    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
        }
        geocodeAddress(geocoder, map, null, place.geometry.location);
    });

    //check for changes
    google.maps.event.trigger(map, "resize");
}

//when map is clicked or location set
function geocodeAddress(geocoder, map, address, latitudelongitude) {
    clearMarkers();
    var parameters = {};
    if (address != null && address)
        parameters['address'] = address;
    else if (latitudelongitude != null && latitudelongitude)
        parameters['latLng'] = latitudelongitude;

    geocoder.geocode(parameters, function (results, status) {
        if (status !== 'OK')
            return displayNotify(GLOBAL_DATA.TRANSLATIONS.GeocodeWasNotSuccessfulForTheFollowingReason + status);

        // filter results to take only the locality names, not full street names
        var expected = _.filter(results, function (ob) {
            return _.indexOf(ob.types, 'locality') >= 0 && _.indexOf(ob.types, 'political') >= 0;
        });

        // more general place
        var place = ((expected && expected.length > 0) ? expected[0] : results[0]);
        projectData.location = place.formatted_address;
        $('#address').val(projectData.location);

        // marker is still where clicked
        var locationToSet = results[0].geometry.location;
        addMarker(locationToSet, map);
        map.panTo(locationToSet);
    });
}

// Adds a marker to the map.
function addMarker(location, map) {
    // Add the marker at the clicked location, and add the next-available label
    // from the array of alphabetical characters.
    clearMarkers();
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });

    markers.push(marker);
    $(".lat").html("latitude: " + marker.position.lat());
    $(".lon").html("longitude: " + marker.position.lng());

    //update project data
    projectData.latitude = marker.position.lat();
    projectData.longitude = marker.position.lng();
    projectData.geolocation = projectData.latitude + "," + projectData.longitude;
    loadweather(projectData.geolocation);
    //console.log("Added marker", markers);
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
    markers = [];
    markers.length = 0;
}

// Shows any markers currently in the array.
function showMarkers() {
    setMapOnAll(map);
}
// ### END
