﻿var promisDt = null;
var selectedSystem = null;

// ### REGION INIT
function initKpiMilestones() {
    ////assign clicks
    $('#save-button').click(onClickSave);

    initializeDatepicker();
    initializeSystemsTable();
}

initKpiMilestones();
// ### END

function initializeDatepicker() {
    $('.sys_date').datepicker({
        clearBtn: false,
        todayHighlight: true,
        orientation: "bottom auto",
        daysOfWeekDisabled: "0,6",
        calendarWeeks: true,
        autoclose: true,
        language: GLOBAL_DATA.USER_LANGUAGE
    });

    $('.sys_date_down').datepicker({
        clearBtn: false,
        todayHighlight: true,
        orientation: "top auto",
        daysOfWeekDisabled: "0,6",
        calendarWeeks: true,
        autoclose: true,
        language: GLOBAL_DATA.USER_LANGUAGE
    });
}

function initializeSystemsTable() {
    var data = PromisDataTableStatics.getTableNameAndColumnsFromView($(".milestones-table-container").html());
    var filters = "";
    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + data.tableName + '/data';

    promisDt = new PromisDataTable({
        tableName: data.tableName,
        columnList: data.columns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 280,
        isStriped: true,
        rowsPerPage: 25,
        fontSize: 12,
        onRowSelect: onSelectRow
        //onrowselect
    });
    promisDt.initContentForTable();
}

function onSelectRow(row) {
    var system = $(row).find("td:first").text();
    $(".milestones-pdf-container iframe").hide();
    if (!system || system == "")
        return displayNotify("Invalid system code.");

    selectedSystem = system;
    var url = GLOBAL_DATA.APP_ROOT + "/api/kpis/" + GLOBAL_DATA.PROJECT_DATABASE + "/completion?system=" + encodeURIComponent(system);
    callServer(url, {
        elementsToAddLoading: [".act_right"],
        removeLoaderIfFail: true,
        successFunc: function (data) {
            afterRowDataRetrieved(selectedSystem, data);
        }
    });
    console.log(row);
}

function afterRowDataRetrieved(selectedSystem, data) {
    if (!data)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.NoDataFound);

    console.log(data);
    _.forEach(data, function (value, key) {
        var propVal = value;
        var date = null;
        if (_.isString(propVal) && propVal.trim() != "" && propVal.split("/").length == 3) {
            var vals = propVal.split("/");
            date = new Date(vals[2], vals[1] - 1, vals[0]);
        }
        $("#" + key).datepicker("setDate", date);
    });

    // generate pdf and attach pdf
    var apiUrl = GLOBAL_DATA.APP_ROOT + "/api/kpis/" + GLOBAL_DATA.PROJECT_ID + "/export/pdf?systemCode=" + encodeURIComponent(selectedSystem);
    callServer(apiUrl, {
        elementsToAddLoading: [".milestones-pdf-container"],
        removeLoaderIfFail: true,
        successFunc: function (fileUrl) {
            if (!fileUrl || fileUrl == "")
                return displayNotify(GLOBAL_DATA.TRANSLATIONS.NoDataFound);

            var url = GLOBAL_DATA.APP_ROOT + "/content/pdf"
                + "?document=" + encodeURIComponent(fileUrl)
                + "&title=" + encodeURIComponent(" ")
                + "&pageMode=single";
            $("#docs").attr("src", url);
            setTimeout(function () {
                $(".milestones-pdf-container iframe").show();
            }, 600);
        }
    });

    //displayNotify(GLOBAL_DATA.TRANSLATIONS.ThePdfShownIsJustForTestingPurposesCurrentlyThePdfGenerationMechanismIsNotImplemented);
}

function onClickSave() {
    if (!selectedSystem || selectedSystem == "")
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.PleaseFirstSelectASystem);

    var modified = {
        Code: selectedSystem
    };
    var fields = _.each($(".milestones-td-input"), function (obj) {
        var propName = $(obj).attr("id");
        var propVal = $(obj).datepicker("getUTCDate");
        modified[propName] = propVal;
    });

    console.log(modified);
    var url = GLOBAL_DATA.APP_ROOT + "/api/kpis/" + GLOBAL_DATA.PROJECT_DATABASE + "/completion?system=" + encodeURIComponent(selectedSystem);
    callServer(url, {
        elementsToAddLoading: [".act_right"],
        removeLoaderIfFail: true,
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(modified),
        successFunc: function () {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.SuccessfullyUpdated, "success");
        }
    })
}