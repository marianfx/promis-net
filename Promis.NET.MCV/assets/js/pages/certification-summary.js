﻿var selectedTag = null;
var promisDt = null;
var rowIdGlobal = null;
var activityGlobal = null;

function initSetupCertification() {
    // click events
    $('#main-act').change(loadDatatables);
    $('#dt-act').change(loadTableSummary);
};
initSetupCertification();

function loadDatatables() {
    $("#dt-act").html("");
    $('#tags-table').html("");
    $('#steps-table').html("");
    $("#steps-table").hide();
    var selected = $('#main-act').val();
    //console.log(selected);
    if (!selected)
        selected = "undefined"
    //    return displayNotify("An activity must be selected in order to continue.");
        var url = GLOBAL_DATA.APP_ROOT + "/api/certification/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/activity/" + encodeURIComponent(selected) + "/table";
    callServer(url, {
        elementsToAddLoading: ['body'],
        removeLoaderIfFail: true,
        successFunc: function (datatables) {
            $('#dt-act').html("<option value=''></option>");
            _.forEach(datatables, function (table) {
                $('#dt-act').append("<option value='" + table.Datatable + "'>" + table.Description + "</option>");
            });
        }
    });
}

function loadTableSummary() {
    $('#tags-table').html("");
    $('#steps-table').html("");
    $("#steps-table").hide();
    var selectedAct = $("#main-act").val();
    var selectedTable = $("#dt-act").val();
    console.log(selectedTable);
    //if (!selectedAct)
    //    return displayNotify("A Activity must be selected in order to load the table for it.");
    if (!selectedTable)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ADatatableMustBeSelectedInOrderToLoadTheTableForIt);

    var url = GLOBAL_DATA.APP_ROOT + "/certification/" + GLOBAL_DATA.PROJECT_ID + "/summary/table/partial?dataTable=" + encodeURIComponent(selectedTable);
    callServer(url, {
        elementsToAddLoading: ["body"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $('#tags-table').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTable(data.tableName, data.columns, selectedAct);
        },
    });
}

function initializeDataTable(dataTable, theColumns, activity) {
    var filters = getAdvancedFiltersSummary(activity);
    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + '/data';

    promisDt = new PromisDataTable({
        tableName: dataTable,
        columnList: theColumns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 300,
        isStriped: true,
        hasRowSelect: true,
        rowsPerPage: 25,
        fontSize: 12,
        onRowSelect: function (row) {
            //selectedTag = $(row).children("td:first-child").text();  for future functionality
            rowIdGlobal = row.id.slice(4);
            activityGlobal = activity;
            var tag = $(row).children("td:first").text();
            console.log(row);
            loadStatusTable(dataTable, activity, tag);
        }
    });
    promisDt.initContentForTable();
}

function loadStatusTable(datatable, activity, tag) {
    // dataTable, string rowId, string mainActivity --
    $('#steps-table').html("");
    var url = GLOBAL_DATA.APP_ROOT + "/certification/" + GLOBAL_DATA.PROJECT_ID + "/summary/stateTable/partial?&dataTable=" + encodeURIComponent(datatable) + "&rowId=" + encodeURIComponent(rowIdGlobal);
    callServer(url, {
        elementsToAddLoading: [".right-container"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $('#steps-table').html(viewData);
            //var subactivity = $("#promisdt-steps").attr("data-subactivity"); for future functionality
            $("#steps-table").show()
            $(".cert-pdf-container #docs-iframe").hide();
            $("#docs-iframe-toggle").click(function () {
                //return displayNotify(GLOBAL_DATA.TRANSLATIONS.FunctionalityNotAvailable);
                //hide-show
                $("#steps-table").hide();

                var subactivity = $("#docs-iframe-toggle").attr("data-subactivity");
                var apiUrl = GLOBAL_DATA.APP_ROOT + "/api/certification/" + GLOBAL_DATA.PROJECT_ID + "/export/pdf"
                    + "?table=" + encodeURIComponent(datatable)
                    + "&subActivity=" + encodeURIComponent(subactivity)
                    + "&tag=" + encodeURIComponent(tag);
                callServer(apiUrl, {
                    elementsToAddLoading: [".right-container"],
                    removeLoaderIfFail: true,
                    successFunc: function (fileUrl) {
                        if (!fileUrl || fileUrl == "")
                            return displayNotify(GLOBAL_DATA.TRANSLATIONS.NoDataFound);

                        var url = GLOBAL_DATA.APP_ROOT + "/content/pdf"
                            + "?document=" + encodeURIComponent(fileUrl)
                            + "&title=" + encodeURIComponent(" ")
                            + "&pageMode=single";
                        $("#docs-iframe").attr("src", url);
                        setTimeout(function () {
                            $(".cert-pdf-container #docs-iframe").show();
                        }, 600);
                    }
                });
            });

            if(isWriter)
                $('.user-selector').change(saveCert);
        },
    });
}

function getAdvancedFiltersSummary(activity) {
    return ("?advfilters[0].leftoperand=ACTIVITY&advfilters[0].rightoperand=" + encodeURIComponent(activity) + "&advfilters[0].operator=inc");
}

function saveCert() {
    var ordId = $(this).attr("data-container");
    var value = $(this).val();
   var Data = {
       Datatable: $("#dt-act").val(),
        RowId: rowIdGlobal,
        Ord: ordId,
        UserValue: value
    };
    var url = GLOBAL_DATA.APP_ROOT + "/api/certification/" + GLOBAL_DATA.PROJECT_DATABASE + "/follou-up/table";
    console.log(Data);
    callServer(url, {
        //elementsToAddLoading: ["#steps-table"],
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(Data),
        successFunc: function (updateMessage) {
            displayNotify(updateMessage, "success");
        },
        removeLoaderIfFail: true
    });
}