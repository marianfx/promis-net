﻿
var promisDt = null;
var conta = 0;
var nr = 0;
var scheduledYearWeek = "";
var schedw = "";
var tag = "";
var code = "";
var theSelectedTypecode = "";
var theSelectedTable = "";

function initPreservationAssignment() {
    $('#dt-pres-follow-up').change(loadTypecodes);
    $('#typecode-pres-follow-up').change(loadDataTable);
    $('#tline').hide();
    $('#save-pres-followup').click(saveFollowUpChanges);
}

initPreservationAssignment();

function loadTypecodes() {
    var selectedTbl = $(this).val();

    if (!selectedTbl)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ADatatableMustBeSelectedInOrderToContinue);

    var url = GLOBAL_DATA.APP_ROOT + "/api/preservation/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/tables/" + encodeURIComponent(selectedTbl) + "/typecodes";
    callServer(url, {
        elementsToAddLoading: ['.act_left'],
        removeLoaderIfFail: true,
        successFunc: function (typecodes) {
            if (!_.isArray(typecodes) || typecodes.length == 0)
                return displayNotify("There is no data for that selection.");

            //clear all others 
            var tempTypeCode = $("#typecode-pres-follow-up");
            if (tempTypeCode) {
                tempTypeCode.html("<option value=''>&nbsp;</option>")
            }

            $('#div-follow-up').html('');//the table disapears
            $('#tline').hide();
            $('#actions-follow-up').html('');
            promisDt = null;
            // put actual loaded datatables
            _.forEach(typecodes, function (typecode) {
                tempTypeCode.append("<option value='" + typecode.Key + "'>" + typecode.Value + "</option>");
            });
        }
    });
}

function loadDataTable() {

    var selectedTable = $("#dt-pres-follow-up").val();
    var selected = $(this).val();

    theSelectedTypecode = selected;
    theSelectedTable = selectedTable.toLowerCase();

    if (!selected)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ATypecodeMustBeSelectedInOrderToLoadTheTableForIt);
    $('#tline').hide();
    $('#actions-follow-up').hide();

    // $("#div-follow-up").html('<div class="sk-three-bounce"><div class="sk-child sk-bounce1"></div><div class="sk-child sk-bounce2"></div><div class="sk-child sk-bounce3"></div></div>')
    var url = GLOBAL_DATA.APP_ROOT + "/preservation/" + GLOBAL_DATA.PROJECT_ID + "/followup/maintaintable?datatable=" + theSelectedTable;
    callServer(url, {
        elementsToAddLoading: [".act_left"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {          
            $('#div-follow-up').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTable(data.tableName, data.columns);
        },
    });
}

function initializeDataTable(dataTable, theColumns) {
    var filters = getAdvancedFiltersForTags();

    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + '/data';
    console.log("url:" + url);
    console.log(theColumns);

    promisDt = new PromisDataTable({
        tableName: dataTable,
        columnList: theColumns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 400,//545,
        rowsPerPage: 25,
        fontSize: 12,
        minHeight: 440,
        onRowSelect: onClickRowSelected
    });
    promisDt.initContentForTable();
}

function getAdvancedFiltersForTags() {
    var year = new Date().getFullYear();

    var schedwL = "pres_" + theSelectedTable.toLowerCase() + ".schedw";
    return ("?distinct=1&advfilters[0].leftoperand=" + theSelectedTable + ".type&advfilters[0].rightoperand=" + encodeURIComponent(theSelectedTypecode) + "&advfilters[0].operator=eq"
        + "&advfilters[1].leftoperand=pres_" + theSelectedTable + ".jp&advfilters[1].rightoperand=MP"//&advfilters[1].operator=eq"
        + "&advfilters[2].leftoperand=pres_" + theSelectedTable + ".schedw&advfilters[2].rightoperand=" + year + "_%&advfilters[2].operator=like"
        + "&join[0].type=inner&join[0].datatable=pres_" + theSelectedTable + "&join[0].leftColumn=" + theSelectedTable + ".tag&join[0].rightColumn=pres_" + theSelectedTable + ".tag&join[0].operator=eq");
}

//when row selected click-->display the timeline area
function onClickRowSelected() {
    $("#list-timeline-date").html("");
    $(".cd-horizontal-timeline .filling-line").attr("style", "");
    $("#init-week").text("");
    $("#rest-date").html("");
    $(".cd-horizontal-timeline").removeClass("loaded");
    $("#actions-follow-up").html("");
    $('#tline').hide();

    //var selectedTable = $("#dt-job-assignment").val();
    rowTag = $('tr.selected').find('td:first').text();
    var success = "btn btn-sm btn-outline-success";
    var dang = "btn btn-sm btn-outline-danger";

    var url = GLOBAL_DATA.APP_ROOT + "/api/preservation/" + GLOBAL_DATA.PROJECT_DATABASE + "/followup/activities?datatable=" + theSelectedTable + "&tag=" + rowTag;
    callServer(url, {
        elementsToAddLoading: ["#tline"],
        removeLoaderIfFail: true,
        successFunc: function (activities) {
            //for jp='IN''
            var dataIN = activities["IN"];
            var scheduledIN = dataIN.ScheduledWeek;

            var doneIN = dataIN.DoneWeek;
            var week = scheduledIN.split("_")[1];
            var year = scheduledIN.split("_")[0];
            var in_date = GLOBAL_DATA.TRANSLATIONS.Week + week + " " + year + ((doneIN != null) ? doneIN : "");

            $("#init-week").text(in_date)
            var getContent = $("#init-week").text();
            var newContent = getContent.replace(GLOBAL_DATA.TRANSLATIONS.Week + week + " " + year, " <br/>" + doneIN);
            $(this).html(newContent);
           //end of jp='IN' area

            ///for jp='RE''
            var dataRE = activities["RE"];
            if (dataRE != null && dataRE != "")
            {
                var scheduledRE = dataRE.ScheduledWeek;
                var doneRE = dataRE.DoneWeek;
                re_date = GLOBAL_DATA.TRANSLATIONS.Week + scheduledRE.split("_")[1] + " " + scheduledRE.split("_")[0] + "<br>" + ((doneRE != null) ? doneRE : "");
                console.log("re date:" + re_date);
                $("#rest-date").attr("class", success);
                $(".restoration-week-container").css("margin-top", "8px");
            }
            else
            {
                re_date = GLOBAL_DATA.TRANSLATIONS.NoDatebrtagNotRestored;
                $("#rest-date").attr("class", dang);
                $(".restoration-week-container").css("margin-top", "0px");
            }
            $("#rest-date").html(re_date);
            $('#tline').show();
            ///end area for jp='RE''
        }
    });


    var urlTimeline = GLOBAL_DATA.APP_ROOT + "/api/preservation/" + GLOBAL_DATA.PROJECT_DATABASE + "/followup/activities/timeline?datatable=" + theSelectedTable + "&tag=" + rowTag;
    callServer(urlTimeline, {
        elementsToAddLoading: ["#tline"],
        removeLoaderIfFail: true,
        successFunc: function(timeline) {//data for timeline-->The key  is the date from table "cut-off" and the value is the list formed by values(Count,ScheduledWeek,Tag, Code)
            console.log("timeline:", timeline);
            var cutOff = Object.keys(timeline);
            //console.log("cut-off dates: " + cutOff);

            if (!cutOff || !_.isArray(cutOff) || cutOff.length == 0)
                return displayNotify("No dates found.");
            
            $(".cd-horizontal-timeline").addClass("loaded");
            for (i = 0; i < cutOff.length;i++)
            {
                var dt = cutOff[i];
                console.log("Example", timeline[dt]);
               
                scheduledYearWeek = timeline[dt].ScheduledWeek;//get value for scheduledweek
                var count = timeline[dt].Count;   //get value for count        
                //console.log("scheduled Year And Week: " + scheduledYearWeek);
                tag = timeline[dt].Tag;//get value for Tag
                code = timeline[dt].Code;////get value for Code
             
                //schedw = "W" + scheduledYearWeek.split("_")[1] + " " + scheduledYearWeek.split("_")[0];
                //add the dates on timeline 
                var timelineTxt = getFormatedDate(dt);
                if ((i + 1) >= count)
                    $("#list-timeline-date").append("<li><a href='#0' class='timeline-date selected' data-date=" + dt + " style='font-size:12px' data-tag='" + tag + "' data-code='" + code + "' data-schedw='" + scheduledYearWeek + "' onclick=getTimeline('"+tag+"','"+code+"','"+scheduledYearWeek+"')>" + timelineTxt +"</a></li>");
                else
                    $("#list-timeline-date").append("<li><a href='#0' class='timeline-date' data-date=" + dt + " style='font-size:12px' data-tag='" + tag + "' data-code='" + code + "' data-schedw='" + scheduledYearWeek + "'  onclick=getTimeline('" + tag + "','" + code + "','" + scheduledYearWeek + "')>" + timelineTxt +"</a></li>");
            }

            $('#tline').show();
            if (!_.isFunction(window.initTimeline)) {
                $.getScript(GLOBAL_DATA.APP_ROOT + "/assets/js/libraries/timeline.js", function () {

                });
            }
            else {
                initTimeline($('.cd-horizontal-timeline'));
            }    
        }
    });   
}

//display table with description, code, sequence and activity and sets value for donew
function getTimeline(tag, code, sheduledYearWeekd) {

    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = (day) + "/" + (month) + "/" + now.getFullYear();

    console.log("Example2", sheduledYearWeekd);

    var url = GLOBAL_DATA.APP_ROOT + "/preservation/" + GLOBAL_DATA.PROJECT_DATABASE + "/followup/activities/status?datatable=" + theSelectedTable + "&tag=" + tag + "&code=" + code + "&scheduledW=" + sheduledYearWeekd;
    callServer(url, {
        elementsToAddLoading: ["#actions-follow-up"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $('#actions-follow-up').html(viewData)
            $("#actions-follow-up").show();
            $('#tline').show();
            $("input[id^='seq']").each(function () {
                $(this).click(function () {
                    ($(this).val() == null || $(this).val() == "") ? $(this).val(today) : $(this).val('');
                })
            });
        }
    });
}

//formatting data as "dd short-name-of-month yy""
function getFormatedDate(dt) {
    var dateSplitted = dt.split("/");
    var objDate = new Date(parseInt(dateSplitted[2]), parseInt(dateSplitted[1]) - 1, parseInt(dateSplitted[0])),
        locale = GLOBAL_DATA.USER_LANGUAGE,
        monthStr = objDate.toLocaleString(locale, { month: "short" }),
        day = objDate.getDate(),
        year = objDate.getFullYear().toString().substr(2, 2);
    return day.toString() + "<br/>" + monthStr.toUpperCase() + " " + year;
}

//function for saving the modifies made for the FollowUp
function saveFollowUpChanges() {

    var selectedTbl = $('#dt-pres-follow-up').val();
    if (!selectedTbl)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ADatatableMustBeSelectedInOrderToContinue);

    tag = $("#tline").find(".selected").attr("data-tag");
    code = $("#tline").find(".selected").attr("data-code");
    schedw = $("#tline").find(".selected").attr("data-schedw");

    var Elements = [];
    _.forEach($("#actions-tbl tr"), function (row, index) {
        if (index < 1)
            return;
        var nr = $(row).find("td:first-child .badge").text();
        var date = $(row).find("td:last-child input").val();
        Elements.push({
            ColumnName: nr,
            Value: date
        });

    })
    //console.log(Elements);
   
    var url = GLOBAL_DATA.APP_ROOT + "/api/preservation/" + GLOBAL_DATA.PROJECT_DATABASE + "/"+ selectedTbl + "/followup/doneweek?tag=" + encodeURIComponent(tag) + "&code=" + encodeURIComponent(code) + "&schedw=" + encodeURIComponent(schedw);
     callServer(url, {
        elementsToAddLoading: [".act_right"],
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(Elements),
        successFunc: function () {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.JobFollowUpSavedWithSuccess , "success");
        },
        removeLoaderIfFail: true
    });

}