﻿var promisDt = null;
var message = GLOBAL_DATA.TRANSLATIONS.TagsSystemAssignmentRemoved;

function initCommissioningSystemSubdivision() {
    //click events
    $('#db-sys').change(loadTypecode);
    $('#type-sys').change(loadDatatable.bind(null, false));
    $('#all-tags').change(loadDatatable.bind(null, true));
    $('#na-tags').change(loadDatatable.bind(null, true));
    $('#select-all').click(checkAllInTable);
    $('#deselect-all').click(unCheckAllInTable);
    $('#save-btn').click(onClickSaveOrDelete);
    $('#delete-btn').click(onClickSaveOrDelete);
}
initCommissioningSystemSubdivision();

function loadTypecode() {
    var selectedTable = $(this).val();
    //clear all before
    $('#type-sys').html("");
    $('#table-data-container').html("");
    promisDt = null
    //--
    var selectedDt = $('#db-sys').val();
    if (!selectedDt)
    {
        getNumberOfTagsAssociated("");
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ADatatableMustBeSelectedInOrderToContinue);
    }
        

    getNumberOfTagsAssociated(selectedTable);
    getTypecodes(selectedDt);
}


function getNumberOfTagsAssociated(datatable) {
   
    var URL = GLOBAL_DATA.APP_ROOT + "/api/commissioning/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/datatable/associatedTags?dataTable=" + datatable;
    console.log(URL);
    callServer(URL, {
        successFunc: function (data) {

            $('#precom-tags').html("<small><b>" + GLOBAL_DATA.TRANSLATIONS.PrecomTags + "</b></small>&nbsp;&nbsp; " + data.Tottags.replace(",", "."));
            $('#assigned-to-system').html("<small><b>" + GLOBAL_DATA.TRANSLATIONS.AssignedToSystem + "</b></small>&nbsp;&nbsp; " + data.TotAss.replace(",", "."));
            $('.trackerball').html('<br>' + data.PrPc.replace(",", ".") + '%<br>');
            $('#progress-bar').attr('style', 'width: ' + data.PrPc.replace(",", ".") + '%');
        }
    })   
}

function getTypecodes(datatable) {
    var URL = GLOBAL_DATA.APP_ROOT + "/api/commissioning/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/datatable/" + encodeURIComponent(datatable) + "/type?isWithJoin=true";
    console.log(URL);
    callServer(URL, {
        elementsToAddLoading: ["body"],
        successFunc: function (data) {
            console.log(data);
            $('#type-sys').html("<option value=''></option>");
            _.forEach(data, function (typecodeData) {
                $('#type-sys').append("<option value='" + typecodeData.Key + "'>" + typecodeData.Value + "</option>");
            });
        }
    })
}

function loadDatatable(isFromRadio) {
    //clearing before elements
    var tableName = $('#db-sys').val();
    var selected = $('#type-sys').val();
    $('#table-data-container').html("");
    promisDt = null;
    if (!tableName) {
        if (!isFromRadio)
            return displayNotify(GLOBAL_DATA.TRANSLATIONS.ADatatableMustBeSelectedInOrderToContinue);
        return
    }
    //--
    var radio = $('input[name=view-tags]:checked').val();

    var url = GLOBAL_DATA.APP_ROOT + "/commissioning/" + GLOBAL_DATA.PROJECT_ID + "/subdivisions/table/partial?&tableName=" + encodeURIComponent(tableName);
    callServer(url, {
        elementsToAddLoading: ["body"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $('#table-data-container').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            console.log(viewData.columns);
            initializeDataTable(tableName, data.columns, radio, selected);
        },
    });
}

function initializeDataTable(dataTable, theColumns, radio,typecode) {
    var filters = getAdvancedFiltersForSys(typecode, radio);
    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + '/data';
   
    promisDt = new PromisDataTable({
        tableName: dataTable,
        columnList: theColumns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 300,
        rowsPerPage: 25,
        hasRowSelect: false,
        isStriped: true,
        onDataPreprocessing: function (data) {
            //moving all columns one index to the left, ignoring the checkbox collumn
            data.columns = _.slice(data.columns, 1);
            data.order = _.map(data.order, function (obj) {
                return {
                    column: obj.column - 1,
                    dir: obj.dir
                };
            });
            //ordering numeric ascendent qty column
            var newOrder = [];
            _.forEach(data.order, function (elem) {
                if (elem.column != 4) {
                    newOrder.push(elem);
                    return;
                }
                newOrder.push({
                    column: 4,
                    dir: elem.dir,
                    func: encodeURIComponent("cast(qty as signed)")
                });
            });
            data.order = newOrder;
        },
        specialRenderingObject: [
            {
                "aTargets": [0],
                "mData": " ",
                "mRender": function (data, type, full) {
                    console.log(full)
                    return "<label class='custom-control custom-checkbox'><input type='checkbox' value= "+ _.toArray(full)[8]+" name='ck-tag' class='custom-control-input'>" +
                        "<span class='custom-control-indicator'></span></label>";
                }
            }]
    });
    promisDt.initContentForTable();
    $('th:first').attr("title") == " " ? $('th:first').replaceWith('<th title=" "></th>') : $('th:first');
}

function getAdvancedFiltersForSys(typecode, radio) {
    var position = "1";
    var typecodeFilter = ""
    var viewTags = "";
    if (radio === "na") {
        viewTags = "&advfilters[1].leftoperand=SYSTEM&advfilters[1].rightoperand=";
        position = "2";
    }
    if (typecode != "") {
        typecodeFilter = "&advfilters[" + position + "].leftoperand=TYPE&advfilters[" + position + "].rightoperand=" + encodeURIComponent(typecode) + "&advfilters[" + position + "].operator=eq"; }
    return ("?advfilters[0].leftoperand=id&advfilters[0].rightoperand=0&advfilters[0].operator=>" + viewTags + typecodeFilter); //+"&order[0].column=1");
}

function checkAllInTable() {
    $('input[name=ck-tag]').prop('checked', true);
}

function unCheckAllInTable() {
    $('input[name=ck-tag]').prop('checked',false);
}

function onClickSaveOrDelete() {
    var datatable = $('#db-sys').val();
    var systemValue = $('#sys').val();
    
    var selectedRows = $("input:checkbox[name=ck-tag]:checked").map(function () { return $(this).val(); });
    if (selectedRows.length === 0)
        return displayNotify("No rows checked!");

    var identity = [];
    _.forEach(selectedRows, function (item) {
        identity.push(item.slice(4));
    });

     var thisObj = {
        System: "",
        Datatable: datatable,
        SelectedRows: identity
    };

    if ($(this).attr("id") === "save-btn") {
        if (!systemValue)
            return displayNotify(GLOBAL_DATA.TRANSLATIONS.ASystemMustBeSelectedInOrderToContinue)
        thisObj.System = systemValue;
        message = GLOBAL_DATA.TRANSLATIONS.TagsSystemAssigned;
    }
    saveAndDeleteActionCallSys(thisObj, datatable);
}

function saveAndDeleteActionCallSys(thisObj, dataTable) {
    var url = GLOBAL_DATA.APP_ROOT + "/api/commissioning/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/datatable/rows/system?dataTable=" + dataTable;
    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(thisObj),
        elementsToAddLoading: ["body"],
        removeLoaderIfFail: true,
        successFunc: function (reciveData) {
            console.log(reciveData);
            $('#precom-tags').html("<small><b>" + GLOBAL_DATA.TRANSLATIONS.PrecomTags +"</b></small>&nbsp;&nbsp; " + reciveData.Tottags);
            $('#assigned-to-system').html("<small><b>" + GLOBAL_DATA.TRANSLATIONS.AssignedToSystem + "</b></small>&nbsp;&nbsp; " +reciveData.TotAss);
            $('.trackerball').html( '<br>'+reciveData.PrPc + '%<br>');
            $('#progress-bar').attr('style','width: '+reciveData.PrPc + '%');
            unCheckAllInTable();
            $('#table-data-container').html("");

            displayNotify(message,"success");
        }
    });
}
