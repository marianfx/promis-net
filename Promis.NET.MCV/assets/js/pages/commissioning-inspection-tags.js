﻿var promisDt = null; 

function initCommissioningInspectionTags() {
    //click events
    $('#main-activities').change(loadDatatables);
    $('#datatable-selector').change(loadTypecode);
    $('#typecode-selector').change(loadDatatable.bind(null, false));
    $('#all-tags').change(loadDatatable.bind(null, true));
    $('#na-tags').change(loadDatatable.bind(null, true));
    $('#select-all').click(checkAllInTable);
    $('#deselect-all').click(unCheckAllInTable);
    $('#save-btn').click(onClickSaveOrDelete);
    $('#delete-btn').click(onClickSaveOrDelete);
}
initCommissioningInspectionTags();

function loadDatatables() {
    //clear all
    $('#datatable-selector').val("");
    $('#datatable-selector .to-hide').hide();
    $('#typecode-selector').html("");
    $('#sub-act').html("");
    $('#table-data-container').html("");
    promisDt = null;
    //--
    var selected = $("#main-activities").val();
    if (!selected)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AnActivityMustBeSelectedInOrderToContinue);
    $('#datatable-selector .to-hide').show();
}

function loadTypecode() {
    //clear all before
    $('#typecode-selector').html("");
    $('#sub-act').html("");
    $('#table-data-container').html("");
    promisDt = null
    //--
    var selectedAct = $('#main-activities').val();
    var selectedDt = $('#datatable-selector').val();
    if (!selectedDt)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ADatatableMustBeSelectedInOrderToContinue);
    getTypecodes(selectedDt);
    getSubactivities(selectedDt, selectedAct)
}

function getTypecodes(datatable) {
    var URL = GLOBAL_DATA.APP_ROOT + "/api/commissioning/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/datatable/" + encodeURIComponent(datatable) + "/type?isWithJoin=false";
    callServer(URL, {
        elementsToAddLoading: ["body"],
        successFunc: function (data) {
            $('#typecode-selector').html("<option value=''></option>");
            _.forEach(data, function (typecodeData) {
                $('#typecode-selector').append("<option value='" + typecodeData.Key + "'>" + typecodeData.Value + "</option>");
            });
        }
    })
}

function getSubactivities(datatable, activity) {
    var URL = GLOBAL_DATA.APP_ROOT + "/api/commissioning/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/datatable/" + encodeURIComponent(datatable) + "/activity/" + encodeURIComponent(activity) + "/sub";
    callServer(URL, {
        elementsToAddLoading: ["body"],
        successFunc: function (data) {
            $('#sub-act').html("<option value=''></option>");
            _.forEach(data, function (subactData) {
                $('#sub-act').append("<option value='" + subactData.Code + "'>"+ subactData.Code + " - " + subactData.Description + "</option>");
            });
        }
    })
}

function loadDatatable(isFromRadio) {
    //clearing before elements
    var tableName = $('#datatable-selector').val();
    var selectedAct = $('#main-activities').val();
    var selected = $('#typecode-selector').val();
    $('#table-data-container').html("");
    promisDt = null;
    //--
    if (!selected && !isFromRadio)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ATypecodeMustBeSelectedInOrderToLoadTheTableForIt);
    if (!selected && isFromRadio)
        return;

    var radio = $('input[name=view-tags]:checked').val();

    var url = GLOBAL_DATA.APP_ROOT + "/commissioning/" + GLOBAL_DATA.PROJECT_ID + "/inspection-tags/table/partial?mainActivity=" + encodeURIComponent(selectedAct) + "&tableName=" + encodeURIComponent(tableName);
    callServer(url, {
        elementsToAddLoading: ["body"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $('#table-data-container').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTable(tableName, data.columns, selectedAct, radio, selected);
        },
    });
}

function initializeDataTable(dataTable, theColumns, activity, radio,typecode) {
    var filters = getAdvancedFilters(typecode, radio, activity);
    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + '/data';
   
    promisDt = new PromisDataTable({
        tableName: dataTable,
        columnList: theColumns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 300,
        rowsPerPage: 25,
        hasRowSelect: false,
        isStriped: true,
        onDataPreprocessing: function (data) {
            //moving all columns one index to the left, ignoring the checkbox collumn
            data.columns = _.slice(data.columns, 1);
            data.order = _.map(data.order, function (obj) {
                return {
                    column: obj.column - 1,
                    dir: obj.dir
                };
            });
            //ordering numeric ascendent qty column
            var newOrder = [];
            _.forEach(data.order, function (elem) {
                if (elem.column != 3) {
                    newOrder.push(elem);
                    return;
                }
                newOrder.push({
                    column: 3,
                    dir: elem.dir,
                    func: encodeURIComponent("cast(qty as signed)")
                });
            });
            data.order = newOrder;
        },
        specialRenderingObject: [
            {
                "aTargets": [0],
                "mData": " ",
                "mRender": function (data, type, full) {
                    return "<label class='custom-control custom-checkbox'><input type='checkbox' value= "+ _.toArray(full)[8]+" name='ck-tag' class='custom-control-input'>" +
                        "<span class='custom-control-indicator'></span></label>";
                }
            }]
    });
    promisDt.initContentForTable();
    $('th:first').attr("title") == " " ? $('th:first').replaceWith('<th title=" "></th>') : $('th:first');
}

function getAdvancedFilters(typecode, radio, mainActivity) {
    var viewTags = "";
    if (radio === "na") { viewTags = "&advfilters[3].leftoperand=" + encodeURIComponent(mainActivity) + "_ACTIVITY&advfilters[3].rightoperand=" }
    return ("?advfilters[0].leftoperand=id&advfilters[0].rightoperand=0&advfilters[0].operator=>&advfilters[1].leftoperand=QTY&advfilters[1].rightoperand=0&advfilters[1].operator=>&advfilters[2].leftoperand=TYPE&advfilters[2].rightoperand=" + encodeURIComponent(typecode) + "&advfilters[2].operator=eq" + viewTags); //+"&order[0].column=1");
}

function checkAllInTable() {
    $('input[name=ck-tag]').prop('checked',true);
}

function unCheckAllInTable() {
    $('input[name=ck-tag]').prop('checked',false);
}

function onClickSaveOrDelete() {
    var subActivity = $('#sub-act').val();
    if (!subActivity)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ASubactivityMustBeSelectedInOrderToContinue);
    var selected = $("#main-activities").val();
    if (!selected)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AnActivityMustBeSelectedInOrderToContinue);
    var datatable = $('#datatable-selector').val();
    var selectedRows = $("input:checkbox[name=ck-tag]:checked").map(function () { return $(this).val(); });
    if (selectedRows.length === 0)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.NoRowsChecked);

    var identity = [];
    _.forEach(selectedRows, function (item) {
        identity.push(item.slice(4));
    });
    var thisObj = {
        Subactivity: subActivity,
        Datatable: datatable,
        SelectedRows: identity,
        MainActivity: selected
    };
    if ($(this).attr("id") == "save-btn")
        saveActionCall(thisObj);
    else {
        deleteActionCall(thisObj);
    }
}

function saveActionCall(thisObj) {
    var url = GLOBAL_DATA.APP_ROOT + "/api/commissioning/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/datatable/rows/activity";
    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(thisObj),
        elementsToAddLoading: ["body"],
        removeLoaderIfFail: true,
        successFunc: function (messageData) {
            displayNotify(messageData, "success");
            $('#table-data-container').html("");
        }
    });
}

function deleteActionCall(thisObj) {
    var url = GLOBAL_DATA.APP_ROOT + "/api/commissioning/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/datatable/rows/activity";
    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.DELETE,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(thisObj),
        elementsToAddLoading: ["body"],
        removeLoaderIfFail: true,
        successFunc: function () {
            displayNotify(GLOBAL_DATA.SuccessfullyDeleted, "success");
            $('#table-data-container').html("");
        }
    });
}