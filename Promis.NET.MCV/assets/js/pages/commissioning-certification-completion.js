﻿var promisDt = null;

function initProgressAndSystems() {
    verifyTagsNotAssigned();
}

initProgressAndSystems();

function verifyTagsNotAssigned() {
   // addPageUnderConstructionMarker("#commissioning-certification");
    $(".certification-tags-to-assign").click(function () {
        var text = $(this).text();
        if (!text || text == 0)
            return;

        var id = $(this).attr('id');
        var prefix = id.indexOf("tblPrecomm") >= 0 ? "pr_" : "co_";
        var dataTable = $(this).attr('data-table');
        viewTagsNotAssigned(dataTable, prefix);
    });
}

function viewTagsNotAssigned(table, prefix) {
    $(".modal-content").css("width", "800px");

    var modalTitle = GLOBAL_DATA.TRANSLATIONS.TagsNotAssigned;
    if (!table)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.TheDatatableCannotBeNull);

    var url = GLOBAL_DATA.APP_ROOT + "/commissioning/" + GLOBAL_DATA.PROJECT_DATABASE + "/progress/tags/notassigned?dataTable=" + table;// + "prefix=" + prefix;
    open_modal(url, {
        title: modalTitle,
        elementsToAddLoading: [".modal-content"],
        next: function (viewData) {
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTable(data.tableName, data.columns, prefix);
        },
        version: GLOBAL_DATA.MODAL_VERSIONS.COMMISSIONING_INSPECTION_STATUS_PROGRESS
    });

}

function initializeDataTable(dataTable, theColumns, prefix) {

    if (!prefix)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ThePrefixCannotBeNull);
    if (!dataTable)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.TheDatatableCannotBeNull);

    var filters = getTypecodeFilter(prefix);
    console.log(filters);

    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + '/data';

    promisDt = new PromisDataTable({
        tableName: dataTable,
        columnList: theColumns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 545,
        fontSize: 14,
        minHeight: 440
    });
    promisDt.initContentForTable();
}

function getTypecodeFilter(thePrefix) {
    return ("?advfilters[0].leftoperand=qty&advfilters[0].rightoperand=0&advfilters[0].operator=>&advfilters[1].leftoperand=" + thePrefix + "activity&advfilters[1].rightoperand=&advfilters[1].operator=eq");
}
