﻿var promisDt = null;
var promisDtRight = null;
var promisFiltersRight = null;
var theSelectedTypecode = "";
var theSelectedTable = "";

function initPreservationAssignment() {
    $('#dt-job-assignment').change(loadTypecodes);
    $('#typecodes-job-assignment').change(loadDataTables);  //load tables from both sides

    $('#pres-plan-info').hide();
    $("#pres-code").click(onClickPreservationPlanCode);
    //set as readonly the textboxes for dates
    $('#init-date').attr("readonly", true);
    $('#maintain-date').attr("readonly", true);
    $('#restor-date').attr("readonly", true);
    initDatepicker();
    $("#save").click(saveAssignment);
} 
initPreservationAssignment();

function initDatepicker() {
    $('#init-date').datepicker({
    clearBtn: false,
    todayHighlight: true,
    orientation: "bottom auto",
    daysOfWeekDisabled: "0,6",
    calendarWeeks: true,
    autoclose: true,
    language: GLOBAL_DATA.USER_LANGUAGE
    });

    $('#maintain-date').datepicker({
        clearBtn: false,
        todayHighlight: true,
        orientation: "bottom auto",
        daysOfWeekDisabled: "0,6",
        calendarWeeks: true,
        autoclose: true,
        language: GLOBAL_DATA.USER_LANGUAGE
    });	

    $('#restor-date').datepicker({
        clearBtn: false,
        todayHighlight: true,
        orientation: "bottom auto",
        daysOfWeekDisabled: "0,6",
        calendarWeeks: true,
        autoclose: true,
        language: GLOBAL_DATA.USER_LANGUAGE
    });
}


function saveAssignment() {
    var selectedTable = $("#dt-job-assignment").val();
    var rowCode = $('tr.selected').find('td:first').text();

    if (!rowCode)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.PleaseSelectARowFromTheTypecodesTableFirst);

    var formData = {};
    var initDate = $('#init-date').datepicker('getUTCDate');//get the date and  the global time
    var maintainDate = $('#maintain-date').datepicker('getUTCDate')
    var restorDate = $('#restor-date').datepicker('getUTCDate');
    formData.TagLocation = $('#tag-location').val();
    
    if (!(initDate != null && maintainDate != null))
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.PleaseSelectSomeDatesAtLeastForInitialisationAndMaintenanceDate);

    formData.InitialisationDate = initDate;
    formData.MaintenanceDate = maintainDate;
    formData.RestorationDate = restorDate != null ? restorDate : null;

    // add tags
    var options = document.getElementsByName("ck-tag");
    var tags = [];
    var rowIds = [];
    for (i = 0; i <= options.length; i++) {
        if (!$(options[i]).is(":checked"))
            continue;
        
        var rowId = $(options[i]).attr("value");
        rowIds.push(rowId);
        var tag = $('#' + rowId).find('td:nth-child(2)').text();
        tags.push(tag);
        console.log(tags);
    }

    if (tags.length == 0)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.InOrderToSaveAtLeastOneTagFromTheRightTableMustBeChecked);

    formData.Tags = tags;
    //console.log(formData);

    var url = GLOBAL_DATA.APP_ROOT + "/api/preservation/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/tables/" + encodeURIComponent(selectedTable) + "/typecodes/" + rowCode;
    callServer(url, {
        elementsToAddLoading: [".act_left"],
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(formData),
        successFunc: function () {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.JobAssignmentPlanSavedWithSuccess, "success");
            for (i = 0; i < rowIds.length; i++) {
                $('#' + rowIds[i]).find('td:first').html("<span class='ti-link' onclick='onClickRowToDelete(this)' title='Delete the associated job card' style='cursor:pointer'></span>");
                $('#' + rowIds[i]).find('td:nth-child(4)').text(rowCode);
            }
        },
        removeLoaderIfFail: true
    });
}

function loadTypecodes() {
    var selectedTbl = $(this).val(); 

    if (!selectedTbl)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ADatatableMustBeSelectedInOrderToContinue);

    var url = GLOBAL_DATA.APP_ROOT + "/api/preservation/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/tables/" + encodeURIComponent(selectedTbl) + "/typecodes";
    callServer(url, {
        elementsToAddLoading: ['.act_left'],
        removeLoaderIfFail: true,
        successFunc: function (typecodes) {
            if (!_.isArray(typecodes) || typecodes.length == 0)
                return displayNotify(GLOBAL_DATA.TRANSLATIONS.ThereIsNoDataForThatSelection);
            console.log(typecodes);
            //clear all others 
            var tempTypeCode = $("#typecodes-job-assignment");
            if (tempTypeCode) {
                tempTypeCode.html("<option value=''>&nbsp;</option>")
            }

            $('#job-assignment').html('');//the table from left side disappears
            $('#act-job-assignment').html('');//the content from the right side disappears
            $('#init-date').val('');//the selected init date is cleared
            $('#maintain-date').val('');//the selected maintenance date is cleared
            $('#restor-date').val('');//the selected restoration date is cleared
            $('#tag-location').val('');//the selected tag location is cleared
            $('#pres-plan-info').hide();//the info related to the preservation plan disappears

            promisDt = null;
            promisDtRight  = null;

            // put actual loaded datatables
            _.forEach(typecodes, function (typecode) {
                tempTypeCode.append("<option value='" + typecode.Key + "'>" + typecode.Value + "</option>");
            });
        }
    });
}


//loading table from left side and the table from the right side(tags table)
function loadDataTables() {
    var selectedTable = $("#dt-job-assignment").val();
    var selected = $(this).val();

    if (!selected)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ATypecodeMustBeSelectedInOrderToLoadTheTableForIt);
    // save these two
    theSelectedTypecode = selected;
    theSelectedTable = selectedTable;

    $('#pres-plan-info').hide();
    $('#job-assignment').html('');//the table from left side disappears
    $('#act-job-assignment').html('');//the content from the right side disappears

    //for left side table
    var url = GLOBAL_DATA.APP_ROOT + "/preservation/" + GLOBAL_DATA.PROJECT_ID + "/assignment/table";
    callServer(url, {
        elementsToAddLoading: [".act_left"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $('#job-assignment').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTable(data.tableName, data.columns);          
        },
    });  

     //for right side table
    var urlTags = GLOBAL_DATA.APP_ROOT + "/preservation/" + GLOBAL_DATA.PROJECT_ID + "/assignment/tagstable?datatable=" + selectedTable;
    callServer(urlTags, {
        elementsToAddLoading: [".act_right"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $('#act-job-assignment').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTableForTags(data.tableName, data.columns);
        },
    });
}


function initializeDataTable(dataTable, theColumns) {
    var filters = getTypecodeFilter();
    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + '/data';

    promisDt = new PromisDataTable({
        tableName: dataTable,
        columnList: theColumns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 545,
        rowsPerPage: 25,
        fontSize: 12,
        minHeight: 440,
        onRowSelect: onClickRowSelected
    });
    promisDt.initContentForTable();
}

function getTypecodeFilter() {
    return ("?advfilters[0].leftoperand=typecode&advfilters[0].rightoperand=" + encodeURIComponent(theSelectedTypecode) + "&advfilters[0].operator=eq");
}


function initializeDataTableForTags(dataTable, theColumns) {
    var filters = getAdvancedFiltersForTags();
    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + '/data';
    console.log(theColumns);

    promisDtRight = new PromisDataTable({
        tableName: dataTable,
        columnList: theColumns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasRowSelect: false,
        hasColumnToggle: false,
        hasAdvancedFilter: true,
        onClickAdvancedFilters: onClickAdvancedSearch.bind(this, dataTable),
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 280,
        rowsPerPage: 25,
        fontSize: 12,
        minHeight: 708,
        onDataPreprocessing: function (data) {
            //console.log(data);
            data.columns = _.slice(data.columns, 1);
            data.order = _.map(data.order, function (obj) {
                return {
                    column: obj.column - 1,
                    dir: obj.dir
                };
            });
        },
        specialRenderingObject: [
            {
                "aTargets": [0],
                "mData": " ",
                "mRender": function (data, type, full) {
                   // console.log("full: "+ _.toArray(full));
                    if (!isWriter || _.toArray(full)[2] == "")
                        //take id for the row
                        return "<label class='custom-control custom-checkbox'><input type='checkbox' name='ck-tag' value=" + _.toArray(full)[7] + " class='custom-control-input'>"+ 
                            "<span class='custom-control-indicator'></span></label>";
                    else
                        return "<span class='ti-link' onclick='onClickRowToDelete(this)' title='Delete the associated job card' style='cursor:pointer'></span>"
                }
            }
        ]
          
    });
    promisDtRight.initContentForTable();
    //hide the ordering arrows for the first column
    $('th:first').attr("title") == " " ? $('th:first').replaceWith('<th title=" "></th>') : $('th:first');
   
}

function getAdvancedFiltersForTags() {
    return ("?distinct=1&advfilters[0].leftoperand=type&advfilters[0].rightoperand=" + encodeURIComponent(theSelectedTypecode) + "&advfilters[0].operator=eq"
        + "&advfilters[1].leftoperand=qty&advfilters[1].operator=neq"
        + "&join[0].type=left&join[0].datatable=pres_" + theSelectedTable + "&join[0].leftColumn=" + theSelectedTable + ".tag&join[0].rightColumn=pres_" + theSelectedTable + ".tag&join[0].operator=eq");
}


//open modal for Advanced Filter when clicking
function onClickAdvancedSearch(selectedTable) {
    if (promisFiltersRight && promisFiltersRight.tableName == promisDtRight.tableName)
        return promisFiltersRight.openModalWithFilters();

    // reinit it only if not previously inited (so it can remember things);
    promisFiltersRight = new PromisAdvancedFilters({
        tableName: promisDtRight.tableName,
        columns: promisDtRight.columns,
        startFrom: 2,
        onResetFilters: function () {
            promisDtRight.changeFilters(getAdvancedFiltersForTags());
        },
        callbackFunction: function (advFiltersString) {
            var newFilters = getAdvancedFiltersForTags() + ((advFiltersString && advFiltersString != "") ? ("&" + advFiltersString.substring(1)) : "");
            console.log(newFilters);
            promisDtRight.changeFilters(newFilters);
        }
    });
    promisFiltersRight.openModalWithFilters();
}

//delete row when clicking in the first cell of the column
function onClickRowToDelete(obj) {

    var selectedTable = $("#dt-job-assignment").val();
    var rowTag = "";
    //get the id of the row in which the selected value to delete is 
    var rowId = $(obj).parent().parent('tr').attr("id");
    console.log(rowId);
    //gets the tag value of the selected row
    rowTag = $('#' + rowId).find('td:nth-child(2)').text();
    //console.log("tag for the row selected: " + rowTag);
        
    var url = GLOBAL_DATA.APP_ROOT + "/api/preservation/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/assignment/tags?datatable=pres_" + selectedTable.toLowerCase() + "&tag=" + rowTag;
    callServer(url, {
        callType: "DELETE",
        elementsToAddLoading: [".act_right"],
        successFunc: function () {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.RecordsSuccessfullyDeleted, 'success');
            $(obj).replaceWith('<label class="custom-control custom-checkbox"><input type="checkbox" name="ck-tag" value='+ rowId + ' class="custom-control-input">' +
                '<span class="custom-control-indicator"></span></label>');
            $("#" + rowId).find("td:nth-child(4)").empty();
        }
    });     
    
}

function onClickRowSelected() {
  
    $('#pres-plan-info').show(); //display the PreservationPlan area
    var rowCode = $('tr.selected').find('td:first').text();
    $("#pres-code").text(rowCode); //set the code of the selected row, on the preservation plan button
    
    var url = GLOBAL_DATA.APP_ROOT + "/api/preservation/" + GLOBAL_DATA.PROJECT_DATABASE + "/assignment/frequency?code=" + rowCode;
    callServer(url, {     
        successFunc: function (viewData) { 
            $('#pres-frequency').html(viewData);//set the value for the frequency of the selected row
        }
    });
}


//for opening modal for Preservation Plan
function onClickPreservationPlanCode() {
    var rowCode = $('tr.selected').find('td:first').text();
    $(".modal-content").css("width", "800px");

    var modalTitle = GLOBAL_DATA.TRANSLATIONS.PreservationActivityListFor + $('tr.selected').find('td:first').text();
     
    var url = GLOBAL_DATA.APP_ROOT + "/preservation/" + GLOBAL_DATA.PROJECT_DATABASE + "/overview/info?code=" + rowCode;
    open_modal(url, {
        title: modalTitle,
        elementsToAddLoading: [".modal-content"],
        next: function (viewData) {
            $('#job-plan-actions').html(viewData)
        },
        version: GLOBAL_DATA.MODAL_VERSIONS.MATERIAL_PRESERVATION_ASSIGNMENT
    });   
} 




