﻿var promisDt = null;
var promisExporter = null;
var saveData = [];

function initQuantities() {
    // click events
    $('#main-activities').change(onMainActivityChange);
    
    // init exporter
    promisExporter = new PromisDataExporter({
        tableName: "budget_subact",
        advancedFiltersString: "",
        onDataRefresh: function () {
            window.location.reload();
        }
    });

    //click on IMPORT or EXPORT. Works thanks to the use of the import-export.js file
    $("#button-save").click(onClickSave);
    $('#button-export').click(promisExporter.onClickExportExcelFile);
    $("#button-export-template").click(promisExporter.onClickExportExcelTemplateFile);
    $("#button-import").click(promisExporter.onClickImportExcelFile);
};
initQuantities();

function onMainActivityChange() {
    var selected = $('#main-activities').val();
    $("#quantities-container").html("");
    promisExporter.changeFilters("");

    if (!selected)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AnActivityMustBeSelectedInOrderToContinue);

    // change export filters to export only the selected
    promisExporter.changeFilters("?advfilters[0].leftoperand=sub_act&advfilters[0].rightoperand=" + encodeURIComponent(selected) + "&advfilters[0].operator=inc");

    var url = GLOBAL_DATA.APP_ROOT + "/progress/" + GLOBAL_DATA.PROJECT_ID + "/quantities/table/partial?activity=" + encodeURIComponent(selected);
    callServer(url, {
        elementsToAddLoading: ["#quantities-container"],
        successFunc: function (viewData) {
            $('#quantities-container').html(viewData);
            initializeDataTable("quantities");//the id of the table is 'promisdt-quantities'
        }
    });
}

function initializeDataTable(dataTable) {
    promisDt = new PromisDataTable({
        tableName: dataTable,
        hasOrdering: false,
        hasScrollCollapse: false,
        hasScrollX: false,
        isResponsive: true,
        hasRowSelect: false,
        hasAjaxDataLoad: false,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 230,
        rowsPerPage: 25,
        onDraw: function () {// each time displayed objects change, add event on cells
            $(".budget-input").blur(onInputChanged);
        }
    });
    promisDt.initContentForTable();
    $(".budget-input").blur(onInputChanged);
}

function onInputChanged() {
    // this here is the selected element
    var parent = $(this).closest(".subactivity-row");
    var elemQty = parent.find(".budget-quantity");
    var elemMhrs = parent.find(".budget-mhrs");
    var elemActual = parent.find(".budget-actual");
    var elemBalance = parent.find(".budget-balance");
    setElementAsValid(elemQty);
    setElementAsValid(elemMhrs);

    var subCode = parent.attr("data-subactivity");
    var actCode = parent.attr("data-activity");
    var valQty = elemQty.val(); valQty = parseFloat(valQty);
    var valQtyOld = elemQty.attr("data-old"); valQtyOld = parseFloat(valQtyOld);
    var valMhrs = elemMhrs.val(); valMhrs = parseFloat(valMhrs);
    var valMhrsOld = elemMhrs.attr("data-old"); valMhrsOld = parseFloat(valMhrsOld);
    console.log("Info", subCode, valQty, valQtyOld, valMhrs, valMhrsOld);

    var detector = function (obj) { return obj.hasOwnProperty("Code") && obj.Code == subCode; };
    _.remove(saveData, detector);// remove it will be added just if correct

    // check if quantity is good
    if (!_.isFinite(valQty)) {
        if (_.isFinite(valQtyOld))
            $(elemQty).val(valQtyOld);
        setElementAsInvalid(elemQty);
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.InvalidQuantityInsertedTheValueHasBeenRevertedToTheOldValueAndWillBeIgnoredUponSaveIfNotCompletedCorrectly);
    }

    // check if mhrs is good
    if (!_.isFinite(valMhrs)) {
        if (_.isFinite(valMhrsOld))
            $(elemMhrs).val(valMhrsOld);
        setElementAsInvalid(elemMhrs);
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.InvalidMenHoursInsertedTheValueHasBeenRevertedToTheOldValueAndWillBeIgnoredUponSaveIfNotCompletedCorrectly);
    }

    //// all good
    // update balance
    var actualQ = parseFloat(elemActual.attr("data-value"));
    var newBalance = valQty - actualQ;
    var difference = 0.5 * valQty;
    var styleToAdd = Math.abs(newBalance) >= difference ? "background-color:rgba(55,1,2,1.00);" : (Math.abs(newBalance) > 0 ? "color: #ffcc00;" : "");
    elemBalance.text(newBalance.toFixed(2));
    elemBalance.attr("style", styleToAdd);
    var thisObj = {
        Code: subCode,
        Quantity: valQty,
        Budget: valMhrs
    };
    saveData.push(thisObj);

    //update total
    var oldTotalValue = $(".total-mhrs-" + actCode).attr("data-initialvalue").replace(",", "");
    var newTotalValue = parseFloat(oldTotalValue) - valMhrsOld + valMhrs;
    $(".total-mhrs-" + actCode).text(newTotalValue);
}

function onClickSave() {
    console.log("Saving", saveData);
    if (!_.isArray(saveData) || saveData.length == 0)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.YouHaveDoneNoChanges, "success");

    // all good
    var url = GLOBAL_DATA.APP_ROOT + "/api/progress/" + GLOBAL_DATA.PROJECT_DATABASE + "/subactivities/quantities";
    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(saveData),
        elementsToAddLoading: ["body"],
        removeLoaderIfFail: true,
        successFunc: function () {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.SuccessfullySaved, "success");
        }
    });
}