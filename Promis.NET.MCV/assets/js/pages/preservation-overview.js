﻿
function initPreservationHome() {
    $('#data-table').change(loadTypecodes);
    $('#table-typecodes').change(loadDataTable);   
} 

initPreservationHome();

function loadTypecodes() {
    var selectedTbl = $(this).val(); 
    var tCode = $("#table-typecodes").attr("id");

    if (!selectedTbl)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ADatatableMustBeSelectedInOrderToContinue);

    var url = GLOBAL_DATA.APP_ROOT + "/api/preservation/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/tables/" + encodeURIComponent(selectedTbl) + "/typecodes";
    callServer(url, {
        elementsToAddLoading: ['.act_left'],
        removeLoaderIfFail: true,
        successFunc: function (typecodes) {
            if (!_.isArray(typecodes) || typecodes.length == 0)
                return displayNotify(GLOBAL_DATA.TRANSLATIONS.ThereIsNoDataForThatSelection);

            //clear all others 
            var tempTypeCode = tCode;
            if (tempTypeCode) {
                $('#' + tempTypeCode).html("<option value=''>&nbsp;</option>");

            }

            $('#job-plans-overview').html('');//the table disapears
            $('#job-plan-actions').html('');//the legend from the right side disappears
            promisDt = null;

            // put actual loaded datatables
            _.forEach(typecodes, function (typecode) {
                $('#' + tCode).append("<option value='" + typecode.Key + "'>" + typecode.Value + "</option>");
            });
        }
    });
}


function loadDataTable() {
    var selected = $("#table-typecodes").val();
    if (!selected)
        return displayNotify( GLOBAL_DATA.TRANSLATIONS.ATypecodeMustBeSelectedInOrderToLoadTheTableForIt);
    
    var url = GLOBAL_DATA.APP_ROOT + "/preservation/" + GLOBAL_DATA.PROJECT_ID + "/overview/table";
    callServer(url, {
        elementsToAddLoading: [".act_left"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $('#job-plans-overview').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTable(data.tableName, data.columns, selected);
        },
    });
    
}

var promisDt = null;
function initializeDataTable(dataTable, theColumns, typecode) {
    var filters = getAdvancedFilters(typecode);
    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + '/data';

    promisDt = new PromisDataTable({
        tableName: dataTable,
        columnList: theColumns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 400,
        onRowSelect: onClickRowSelected
    });
    promisDt.initContentForTable();

    // click on sorting
}
function getAdvancedFilters(typecode) {
    return ("?advfilters[0].leftoperand=typecode&advfilters[0].rightoperand=" + encodeURIComponent(typecode) + "&advfilters[0].operator=eq");
}

function onClickRowSelected() {
    var rowCode = $('tr.selected').find('td:first').text();
    
    var url = GLOBAL_DATA.APP_ROOT + "/preservation/" + GLOBAL_DATA.PROJECT_DATABASE + "/overview/info?code=" + rowCode;
    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.POST,
        elementsToAddLoading: [".act_right"],
        contentType: false,
        processData: false,
        data: rowCode,
        successFunc: function (viewData) {
            $('#job-plan-actions').html(viewData)             
        }
    });
}





