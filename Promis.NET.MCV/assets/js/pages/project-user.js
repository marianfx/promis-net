﻿// ### REGION INIT
function initSetupUsers() {
    // assign clicks
    $("#create-user-btn").click(onClickNewUser);
    $("#save-user-btn").click(onClickSaveUser);
    $("#delete-user-btn").click(deleteUser);

    //setup image
    setUpImageEncoding();
    setTriggersOnImageButton();

    // setup autocomplete
    setAutocompleteUsers();
}
initSetupUsers();
//END

// ### Region Image Encoding
function setUpImageEncoding() {
    $("#profile-users-image").change(function () {
        encodeImageFileAsURL("profile-users-image", "users-user-picture");
    });
}

function setTriggersOnImageButton() {
    /*Area of upload file/update info*/
    $("html").addClass('js');
    var fileInput = $("#profile-users-image"),
        button = $("#profile-users-image-button"),
        the_return = $("#profile-image-return");

    button.keydown(function (event) {
        if (event.keyCode === 13 || event.keyCode === 32) {
            fileInput.focus();
        }
    });

    button.click(function () {
        fileInput.focus();
        return false;
    });

    fileInput.change(function () {
        the_return.html(GLOBAL_DATA.TRANSLATIONS.IclickOnBsavebToMakeTheChangeEffectivei);
    });
}

// ### Region Users autocomplete
function setAutocompleteUsers() {
    var options_user = {
        data: GLOBAL_DATA.setupUsersForSearch,
        placeholder: GLOBAL_DATA.TRANSLATIONS.Search + "...",
        list: {
            match: { enabled: true },
            onChooseEvent: function () {
                var selectedUser = $("#selected-user-autocomplete").getSelectedItemData();
                var selectedId = selectedUser.id;
                var selectedName = selectedUser.name;
                loadUser(selectedId, selectedName);
            }
        },
        getValue: "name",
        template: {
            type: "iconLeft",
            fields: { iconSrc: "icon" }
        }
    };

    $("#selected-user-autocomplete").easyAutocomplete(options_user);

    $("#selected-user-autocomplete").val("");
    if (GLOBAL_DATA.setupSelectedUsername) {
        //console.log(GLOBAL_DATA.setupSelectedUsername);
        $("#selected-user-autocomplete").val(GLOBAL_DATA.setupSelectedUsername);
    }
}

function loadUser(userId, userName) {
    if (!userId) {
        displayNotify(GLOBAL_DATA.TRANSLATIONS.NoUserSelectedWillNotLoadData);
        return;
    }

    // simply reload the setup page, setting the userId (AJAX)
    GLOBAL_DATA.setupSelectedUserId = userId;
    GLOBAL_DATA.setupSelectedUsername = userName;
    usersSavingMode = "save";
    CurrentModule.initializeModule("users", { userId: userId });
}

var usersSavingMode = "save";

function onClickNewUser() {
    reinitUserForm();
    usersSavingMode = "add";
}

function onClickSaveUser() {
    console.log(usersSavingMode);
    if (usersSavingMode === "save")
        return saveUser();
    return addUser();
}

function addUser() {
    var url = GLOBAL_DATA.APP_ROOT + "/api/account/";

    var formData = getFormData();
    formData.ProjectId = GLOBAL_DATA.PROJECT_ID;
    var formSerialized = JSON.stringify(formData);

    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.POST,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: formSerialized,
        elementsToAddLoading: ["#formuser"],
        removeLoaderIfFail: true,
        successFunc: function (created) {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.UserSuccessfullyCreated, "success");

            // make it the selected user
            GLOBAL_DATA.setupSelectedUserId = created.Id;
            GLOBAL_DATA.setupSelectedUsername = created.UserName;
            usersSavingMode = "save";

            // reload autocomplete
            GLOBAL_DATA.setupUsersForSearch.push({
                id: created.Id,
                name: createUserFullName(created),
                email: created.Email,
                icon: created.PhotoUrl && created.PhotoUrl !== "" ? created.PhotoUrl : "assets/img/user-icon.png"
            });
            setAutocompleteUsers();
        }

    });
}

function saveUser() {
    var userId = GLOBAL_DATA.setupSelectedUserId ? GLOBAL_DATA.setupSelectedUserId : GLOBAL_DATA.USER_ID;//if user is selected, change his data, otherwise i am editing mine
    var url = GLOBAL_DATA.APP_ROOT + "/api/account/" + userId + "?type=full";

    var formData = getFormData();
    formData.UserId = userId;
    var formSerialized = JSON.stringify(formData);

    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: formSerialized,
        elementsToAddLoading: ["#formuser"],
        removeLoaderIfFail: true,
        successFunc: function (data) {
            displayNotify(data, "success");
            // update username variables
            GLOBAL_DATA.setupSelectedUsername = formData.UserName;

            // update user contents if updated for current user
            if (userId === GLOBAL_DATA.USER_ID) {
                var name = createUserFullName(formData);
                $("#user-details-name").html(GLOBAL_DATA.TRANSLATIONS.Welcome + "<br/>");
                $("#user-details-name").append(name);
                $("#user-location").text(formData.Company);
                $("#user-details-picture").attr("src", formData.PhotoUrl);

                //reresh
                setTimeout(function () { window.location.reload(); }, 1000);
            }

            // update user if it was in autocomplete
            GLOBAL_DATA.setupUsersForSearch = _.map(GLOBAL_DATA.setupUsersForSearch, function (value) {
                if (value.id !== userId)
                    return value;

                return {
                    id: userId,
                    name: createUserFullName(formData),
                    email: formData.Email,
                    icon: formData.PhotoUrl && formData.PhotoUrl !== "" ? formData.PhotoUrl : "assets/img/user-icon.png"
                };
            });
            //console.log("Testing users", GLOBAL_DATA.setupUsersForSearch);
            setAutocompleteUsers();
        }
    });
}

function deleteUser() {
    var userId = GLOBAL_DATA.setupSelectedUserId ? GLOBAL_DATA.setupSelectedUserId : GLOBAL_DATA.USER_ID;//if user is selected, change his data, otherwise i am editing mine
    var url = GLOBAL_DATA.APP_ROOT + "/api/account/" + userId;

    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.DELETE,
        elementsToAddLoading: ["#formuser"],
        removeLoaderIfFail: true,
        successFunc: function (data) {
            displayNotify(data.message, "success");

            // remove variables
            GLOBAL_DATA.setupSelectedUserId = null;
            GLOBAL_DATA.setupSelectedUsername = null;

            // reload autocomplete
            GLOBAL_DATA.setupUsersForSearch = _.filter(GLOBAL_DATA.setupUsersForSearch, function (value) {
                return value.id !== userId;
            });
            setAutocompleteUsers();

            // reset user form
            reinitUserForm();

            if (data.deletedSelf)
                setTimeout(function () {
                    window.location.href = GLOBAL_DATA.APP_ROOT + "/account/login";
                }, 3000);
        }
    });
}

function getFormData() {
    var formData = extractObjectFromForm("#formuser");
    formData.PhotoUrl = $("#users-user-picture").attr("src");

    //modules
    var modulesArray = [];
    _.forEach(GLOBAL_DATA.setupData.modules, function (module, key) {
        var objName = "module-" + module.Code;
        var value = formData[objName];
        delete formData[objName];
        if (value !== "H") {
            modulesArray.push(module.Code + "_" + value);
        }
    });
    formData.RolesModulesString = _.join(modulesArray, "$");

    //activities
    var activitiesArray = [];
    _.forEach(GLOBAL_DATA.setupData.activities, function (activity, key) {
        var objName = "activity-" + activity.Code;
        var value = formData[objName];
        delete formData[objName];
        if (value !== "H") {
            activitiesArray.push(activity.Code);
        }
    });
    formData.RolesActivitiesString = _.join(activitiesArray, "$");

    //responsability
    if (formData.RolesResponsabilitiesString === "S" && formData.RolesResponsabilitiesSubcontractor && formData.RolesResponsabilitiesSubcontractor.trim() !== "") {
        formData.RolesResponsabilitiesString = "S$" + formData.RolesResponsabilitiesSubcontractor;
    }
    delete formData.RolesResponsabilitiesSubcontractor;

    //console.log(formData);
    return formData;
}

function reinitUserForm() {
    $("#FirstName").val("");
    $("#LastName").val("");
    $("#UserName").val("");
    $("#Password").val("");
    $("#Email").val("");
    $("#OfficePhone").val("");
    $("#Country").val("");
    $("#MobilePhone").val("");
    $("#Company").val("");
    $("#lang-en").prop("checked", true);
    $("#users-user-picture").attr("src", "assets/img/user-icon.png");

    var objName = "RolesResponsabilitiesString";
    var responsabilities = $("input[name='" + objName + "']");
    _.forEach(responsabilities, function (responsability) {
        $(responsability).prop("checked", $(responsability).val() === "" ? true : false);
    });

    _.forEach(GLOBAL_DATA.setupData.modules, function (module, key) {
        var objName = "module-" + module.Code;
        var radios = $("input[name='" + objName + "']");
        _.forEach(radios, function (radio) {
            $(radio).prop("checked", $(radio).val() === "R" ? true: false);
        });
    });

    _.forEach(GLOBAL_DATA.setupData.activities, function (activity, key) {
        var objName = "activity-" + activity.Code;
        var radios = $("input[name='" + objName + "']");
        _.forEach(radios, function (radio) {
            $(radio).prop("checked", $(radio).val() === "P" ? true : false);
        });
    });
}

function createUserFullName(user) {
    var name = "";
    name = user.FirstName !== "" ? user.FirstName : "";
    name += " " + (user.LastName !== "" ? user.LastName : "");
    name = name.trim();
    name = name !== "" ? name : user.UserName;
    return name;
}