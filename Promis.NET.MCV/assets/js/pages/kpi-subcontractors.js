﻿var promisDt = null;
var subActivity = "";
// ### REGION INIT
function initKpiSubcontractors() {
    configure_chart(GLOBAL_DATA.USER_THEME);
    $("#div-subc").html('');
    $("#subcontractors-list").change(loadSOWTable); 
   
}

initKpiSubcontractors();
// ### END

function loadSOWTable() {

    var selectedSubcontractor = $(this).val();
    console.log(selectedSubcontractor);
    if (!selectedSubcontractor) {
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.InOrderToSeeTheSowTableYouMustSelectASubcontractor);
    }

    var url = GLOBAL_DATA.APP_ROOT + "/kpis/" + GLOBAL_DATA.PROJECT_ID + "/subcontractors/sow-table/partial?subcontractorCode=" + selectedSubcontractor;

    callServer(url, {
        elementsToAddLoading: ["#div-sow"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $('#div-sow').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTableForSOW(data.tableName, data.columns, selectedSubcontractor);
            loadAnalysisAndChartData();                   
        },
    });
}

function initializeDataTableForSOW(dataTable, theColumns) {

    var url = GLOBAL_DATA.APP_ROOT + "/api/engineering/tables/" + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + "/data";

    promisDt = new PromisDataTable({
        tableName: dataTable,
        columnList: theColumns,
        hasAjaxDataLoad: false,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 545,
        rowsPerPage: 25,
        fontSize: 12,
        minHeight: 616,
        hasScrollCollapse: false,
        onRowSelect: onClickRowSelected
    });
    promisDt.initContentForTable();
}

function loadAnalysisAndChartData() {

    var selectedSubcontractor = $("#subcontractors-list").val();
    subActivity = $("#promisdt-kpi-sow tbody").find("tr:first-child td:first-child").text().split("-")[0];
    console.log("Sub-act:" + subActivity);
    console.log(selectedSubcontractor, subActivity);
    loadPerformanceAndProgressData(selectedSubcontractor, subActivity);
}

function loadPerformanceAndProgressData(subcontractor, subactivity) {

    var urlPerformance = GLOBAL_DATA.APP_ROOT + "/kpis/" + GLOBAL_DATA.PROJECT_DATABASE + "/subcontractors/analisys/partial?&subcontractorCode=" + subcontractor + "&subActivity=" + subactivity;
    callServer(urlPerformance, {
        elementsToAddLoading: [".act_right .card-block"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $("#div-subc").html(viewData);
            initChart(subcontractor, subactivity);          
        },
    });
}

function onClickRowSelected() {
    var subactivity = "";
    var selectedSubcontractor = $("#subcontractors-list").val();
    var selRow = $("#promisdt-kpi-sow tbody").find("tr").hasClass("selected");
    console.log(selRow);
    if (selRow) {
        subActivity = $("#promisdt-kpi-sow tbody").find("tr.selected").find("td:first-child").text().split("-")[0];
        console.log("Sub-act:" + subActivity);
        loadPerformanceAndProgressData(selectedSubcontractor, subActivity)      
    }
}

function initChart(subcontractorCode, subActivity) {

    var url = GLOBAL_DATA.APP_ROOT + "/api/kpis/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/charts?selectedSubcontractor=" + subcontractorCode + "&subActivity=" + subActivity;

    callServer(url, {
        removeLoaderIfFail: true,
        successFunc: function (data) {
            console.log(data);
            initProgressChart(data.ChartBarsLabels, data.ChartPr, data.ChartBars, data.ChartRit, data.ChartWkty, data.ChartWorkW); 
        },
        elementsToAddLoading: ['.chart-progress']
    });

}

function initProgressChart(labels, subc_progress, subc_bar, rit, wkqty, work_weekly) {

    console.log("Info", labels, subc_progress, subc_bar, rit, wkqty, work_weekly);
    
    var configFirstChart = {
        labels: labels,
        datasets: [{
            type: 'bar',
            label: '',
            barThickness: 1,
            borderColor: 'rgba(21,212,190,1)',
            backgroundColor: 'rgba(21,212,190,1)',
            fill: true,
            data: subc_bar
        },
        {
            type: 'line',
            label: "",
            borderColor: 'rgba(4,110,23,1.00)',
            backgroundColor: 'rgba(4,110,23,0.2)',
            fill: true,
            data: subc_progress
        },
        {
            type: 'line',
            label: "",
            borderWidth: 0,
            backgroundColor: 'rgba(161,2,5,0.7)',
            fill: true,
            data: rit
        }]
    };

    var configSecondChart = {
        labels: labels,
        datasets: [{
            type: 'line',
            label: "",
            borderColor: 'rgba(4,110,23,1.00)',
            backgroundColor: 'rgba(4,110,23,0.2)',
            fill: true,
            data: wkqty
        },
        {
            type: 'line',
            label: "",
            borderColor: 'rgba(161,2,5,0.7)',
            backgroundColor: 'rgba(161,2,5,0.7)',
            fill: false,
            data: work_weekly
        }]
    };

    var ctxFirst = $("#chart-progress-sow");
    var ctxSecond = $("#chart-progress-sow-week");

    var myFirstChart = new Chart(ctxFirst, {
        type: 'bar',
        data: configFirstChart,
        borderWidth: 0,
        options: {
            elements: {
                point: {
                    radius: 0
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff"
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.05)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff",
                        maxRotation: 0,
                        minRotation: 0,
                        autoSkip: false
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.05)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    },
                    barThickness: 3
                }]
            },
            legend: {
                display: false
            },
            layout: {
                padding: {
                    left: 20,
                    right: 20,
                    top: 30,
                    bottom: 20
                }
            }
        }//, 
    });

    var mySecChart = new Chart(ctxSecond, {
        type: 'bar',
        data: configSecondChart,
        borderWidth: 0,
        options: {
            elements: {
                point: {
                    radius: 0
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff"
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.05)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff",
                        maxRotation: 0,
                        minRotation: 0,
                        autoSkip: false
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.05)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    },
                    barThickness: 3
                }]
            },
            legend: {
                display: false
            },
            layout: {
                padding: {
                    left: 20,
                    right: 20,
                    top: 30,
                    bottom: 20
                }
            }
        }//, 
    });
}

