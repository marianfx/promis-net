﻿var promisExporter = null;

function initSetupPlanning() {
    // triggers (resize, scroll)
    initScrollActions();
    onResize();
    $(window).resize(onResize);

    $('#range.input-daterange').datepicker({
        startView: 1,
        clearBtn: true,
        orientation: "bottom auto",
        daysOfWeekDisabled: "0,6",
        calendarWeeks: true,
        autoclose: true,
        language: GLOBAL_DATA.USER_LANGUAGE
    });

    // click events
    $('#main-activities').change(loadAllActivities.bind(null, true));
    $('#button-load-activities').click(loadAllActivities.bind(null, true));
    $('#button-load-mhrs-activities').click(loadAllActivities.bind(null, false));
    $('#button-save').click(saveChanges);

    // init exporter
    promisExporter = new PromisDataExporter({
        tableName: "planning",
        advancedFiltersString: "",
        onDataRefresh: function () {
            if (promisDt)
                promisDt.reinitContentForTable();
        }
    });

    //click on IMPORT or EXPORT. Works thanks to the use of the import-export.js file
    $("#button-export-excel").click(promisExporter.onClickExportExcelFile);
    $("#button-export-template").click(promisExporter.onClickExportExcelTemplateFile);
    $("#button-import-excel").click(promisExporter.onClickImportExcelFile);
    $("#button-import-primavera").click(function () {
        displayNotify(GLOBAL_DATA.TRANSLATIONS.FunctionalityNotAvailable);
    });
};
initSetupPlanning();


function initScrollActions() {
    $('.linked-scrollbar').scroll(function () {
        if ($(this).attr("data-scrolling") == "false") {
            $('.linked-scrollbar').not(this).attr("data-scrolling", "true");
            $('.linked-scrollbar').not(this).scrollTop($(this).scrollTop());
        }
        $(this).attr("data-scrolling", "false");
    });

    $('.linked-horizontal-scrollbar').scroll(function () {
        if ($(this).attr("data-horizontal-scrolling") == "false") {
            $('.linked-horizontal-scrollbar').not(this).attr("data-horizontal-scrolling", "true");
            $('.linked-horizontal-scrollbar').not(this).scrollLeft($(this).scrollLeft());
        }
        $(this).attr("data-horizontal-scrolling", "false");
    });
}

function onResize() {
    var h = $(window).innerHeight() - 260;
    h = h > 10 ? h : 10;
    $('#left-dynamic-menu').css("max-height", h + "px");
    $('#right-display').css("max-height", (h) + "px");
    var allWidth = $('#setup-content').outerWidth();
    var rightWidth = allWidth - 350;
    $('#right-outmost-container').css("width", rightWidth + "px");
}


function loadAllActivities(loadAll) {
    var selected = $('#main-activities').val();
    $("#left-dynamic-menu").html("");
    $("#right-display").html("");

    promisExporter.changeFilters("");

    if (!selected)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AMainActivityMustBeSelectedInOrderToContinue);
    
    // set the getter for advanced filters (to be used in export)
    promisExporter.changeFilters(getAdvancedFilters(selected, loadAll));

    var url = GLOBAL_DATA.APP_ROOT + "/project/" + GLOBAL_DATA.PROJECT_ID + "/planning/partial/menu?mainActivity=" + encodeURIComponent(selected) + "&includeNonzero=" + encodeURIComponent(loadAll);
    callServer(url, {
        elementsToAddLoading: ['body'],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            if (!viewData || viewData.trim() == "" || viewData.trim().startsWith("<script>"))
                return displayNotify(GLOBAL_DATA.TRANSLATIONS.ThereIsNoDataForThatSelection);

            // init left display and its events
            $('#left-dynamic-menu').html(viewData);

            // init the content for the right display
            if (GLOBAL_DATA.setup.planning.activitiesHTML && GLOBAL_DATA.setup.planning.activitiesHTML.trim() != "")
                $('#right-display').html(GLOBAL_DATA.setup.planning.activitiesHTML);

            // init events
            initEventsForPlanningMenu();
        }
    });
}

function initEventsForPlanningMenu() {
    // date picker
    $('.mydate').datepicker({
        format: "yyyy-mm",
        autoclose: true,
        viewMode: "months",
        minViewMode: "months",
        language: GLOBAL_DATA.USER_LANGUAGE
    });

    // assign events after load
    assignOnChangeOnInputs();
    assignUpdateMhrsOnMhrsChange();
    $('.curves-refresher').click(refreshCurvesForActivity);

    // budget
    changeMhrs(GLOBAL_DATA.setup.planning.budget, GLOBAL_DATA.setup.planning.planned);
}

function assignOnChangeOnInputs() {
    $("input").change(function () {
        $(this).removeClass("invalid");
    });
}

function assignUpdateMhrsOnMhrsChange() {
    $(".mymhrs").focusin(function () {
        $(this).attr('data-old', $(this).val());
    });

    $('.mymhrs').change(function () {
        var prev = parseInt($(this).attr('data-old'));
        var actual = parseInt($(this).val());
        var sum = parseInt($('#data-mhrs-planned').text());
        var activityBudget = parseInt($('#data-mhrs-budget').text());

        prev = _.isFinite(prev) ? prev : 0;
        actual = _.isFinite(actual) ? actual : 0;
        sum = _.isFinite(sum) ? sum : 0;
        activityBudget = _.isFinite(activityBudget) ? activityBudget : 0;

        sum = sum - prev + actual;
        changeMhrs(activityBudget, sum);
    });
}

function changeMhrs(budget, planned) {
    $('#mhrs-container table').show();
    $('#data-mhrs-budget').text(budget);
    $('#data-mhrs-planned').text(planned);
    var ballance = budget - planned;
    $('#data-mhrs-balance').text(ballance);
    var color = (ballance != 0 ? "darkred" : "#43b4d7");
    $('#data-mhrs-balance').css("background-color", color);
}

function refreshCurvesForActivity() {
    var type = $(this).attr("data-type");
    var keyWord = type == "B" ? "baseline" : "planned";
    var activity = $(this).attr("data-activity");
    var projectStart = $(this).attr("data-project-start");
    var projectEnd = $(this).attr("data-project-end");

    setElementAsValid('#start-date-' + keyWord + '-' + activity);
    setElementAsValid('#end-date-' + keyWord + '-' + activity);
    setElementAsValid('#curves-refresher-' + keyWord + '-' + activity);

    //get data and compute differences
    var startDate = $("#start-date-" + keyWord + "-" + activity).val();
    var endDate = $("#end-date-" + keyWord + "-" + activity).val();
    var months = getMonthsFromStartUntilEnd(startDate, endDate);
    var monthsTostart = getMonthsFromStartUntilEnd(projectStart, startDate);
    var monthsToEnd = getMonthsFromStartUntilEnd(endDate, projectEnd);
    var monthsBeforeStart = getMonthsFromStartUntilEnd(projectStart, startDate);
    var monthsBeforeEnd = getMonthsFromStartUntilEnd(projectStart, endDate);
    //console.log("Info", projectEnd, startDate, endDate, months, monthsToEnd, monthsBeforeStart, monthsBeforeEnd);

    if (months <= 0 || monthsTostart <= 0 || monthsToEnd <= 0) {
        setElementAsInvalid("#start-date-" + keyWord + "-" + activity);
        setElementAsInvalid("#end-date-" + keyWord + "-" + activity);
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.InvalidSelectedStartAndEndDates);
    }

    // minor tweaks
    months = months - 1;
    monthsBeforeStart = monthsBeforeStart - 1;
    monthsBeforeEnd = monthsBeforeEnd - 1;

    // set progress
    $("#progress-" + keyWord + "-" + activity).css("left", (monthsBeforeStart * 40 + 20) + "px").css("width", (months * 40) + "px");
    months == 0 ? $("#progress-" + keyWord + "-" + activity + " .trackerball").hide() : $("#progress-" + keyWord + "-" + activity + " .trackerball").show()

    // set table distribution values
    var vals = getMonthsDistribution(months + 1);
    //console.log("Distribution values", vals);
    var associatedMonth = 0;
    var index = 0;
    _.forEach($(".row-" + keyWord + "-" + activity + " td"), function (element) {
        if (associatedMonth < monthsBeforeStart || associatedMonth > monthsBeforeEnd)
            $(element).html("&nbsp;");
        else {
            $(element).html("<input name='col-" + keyWord + "-" + activity + "' class='form-control myspin' value='" + vals[index++] + "'>");
        }
        associatedMonth++;
    });
}


function saveChanges() {
    var selected = $('#main-activities').val();
    if (!selected)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AMainActivityMustBeSelectedInOrderToContinue);

    var formData = [];
    var errors = [false, false, false, false];
    var messages = [
         GLOBAL_DATA.TRANSLATIONS.EncounteredInvalidMhInserted,,
         GLOBAL_DATA.TRANSLATIONS.EncounteredInvalidSelectedStartAndEndDates,
         GLOBAL_DATA.TRANSLATIONS.EncounteredInvalidValuesForProgressDistribution,
         GLOBAL_DATA.TRANSLATIONS.TheNumberOfProgressDistributionValuesDoesNotMatchTheSelectedTimeIntervalMakeSureYouClickTheShuffleButtonAfterEachDateChange
        
    ];
    
    _.forEach($('.curves-refresher'), function (element) {
        // get data
        var type = $(element).attr("data-type");
        var keyWord = type == "B" ? "baseline" : "planned";
        var keyWordUpper = type == "B" ? "Baseline" : "Planned";
        var activity = $(element).attr("data-activity");
        var description = $(element).attr("data-description");
        var projectStart = $(element).attr("data-project-start");
        var projectEnd = $(element).attr("data-project-end");

        // compute required months
        var startDate = $("#start-date-" + keyWord + "-" + activity).val();
        var endDate = $("#end-date-" + keyWord + "-" + activity).val();
        var months = getMonthsFromStartUntilEnd(startDate, endDate);
        var monthsTostart = getMonthsFromStartUntilEnd(projectStart, startDate);
        var monthsToEnd = getMonthsFromStartUntilEnd(endDate, projectEnd);
        //console.log("Info", projectEnd, startDate, endDate, months, monthsToEnd);
        var mhrs = parseInt($("#mh-" + activity).val());
        
        // validations
        setElementAsValid("#mh-" + activity);
        if (!_.isFinite(mhrs) || !_.isInteger(mhrs) || mhrs < 0) {
            setElementAsInvalid("#mh-" + activity);
            return (errors[0] = true);
        }


        setElementAsValid("#start-date-" + keyWord + "-" + activity);
        setElementAsValid("#end-date-" + keyWord + "-" + activity);
        console.log("Info", startDate, endDate, months);
        if (startDate != "" && startDate != undefined && endDate != "" && endDate != undefined &&
            (months <= 0 || monthsTostart <= 0 || monthsToEnd <= 0)) {//months can be left null. but cannot be invalid

            setElementAsInvalid("#start-date-" + keyWord + "-" + activity);
            setElementAsInvalid("#end-date-" + keyWord + "-" + activity);
            return (errors[1] = true);
        }

        var activityObject = {};
        activityObject["Code"] = activity;
        activityObject["Budget"] = mhrs;
        activityObject["StartDate" + keyWordUpper] = startDate;
        activityObject["EndDate" + keyWordUpper] = endDate;
        activityObject["Description"] = description;
        activityObject[keyWordUpper + "Progress"] = [];


        // go trough progress values
        var hasError = false;
        var tempVals = [];
        _.forEach($("[name='col-" + keyWord + "-" + activity + "']"), function (input) {
            var tempVal = parseFloat($(input).val());
            setElementAsValid(input);
            if (!_.isFinite(tempVal) || tempVal < 0) {
                setElementAsInvalid(input);
                return (hasError = true);
            }
            tempVals.push(tempVal);
        })

        if (hasError)
            return (errors[2] = true);

        if(tempVals.length != months){
            setElementAsInvalid('#curves-refresher-' + keyWord + '-' + activity);
            setElementAsInvalid("#start-date-" + keyWord + "-" + activity);
            setElementAsInvalid("#end-date-" + keyWord + "-" + activity);
            return (errors[3] = true);
        }

        activityObject[keyWordUpper + "Progress"] = tempVals;

        var existingIndex = _.findIndex(formData, { 'Code': activityObject.Code });
        if (existingIndex >= 0)
            formData[existingIndex] = _.extend(formData[existingIndex], activityObject);
        else
            formData.push(activityObject);
    });

    //console.log(formData);
    if (_.findIndex(errors, function (o) { return o == true; }) >= 0) {
        for (var i = 0; i < errors.length; i++) {
            if (errors[i])
                displayNotify(messages[i]);
        }
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ErrorsOccuredPleaseFillInTheMarkedFieldsCorrectly);
    }

    // all good here, send to server
    var activity = $('#main-activities').val();
    var url = GLOBAL_DATA.APP_ROOT + "/api/project/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/activities/" + activity + "/related";
    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(formData),
        elementsToAddLoading: ['body'],
        removeLoaderIfFail: true,
        successFunc: function () {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.SuccessfullySaved, "success");
        }
    });
}

function getAdvancedFilters(activity, loadAll) {
    var output = "?advfilters[0].leftoperand=act&advfilters[0].rightoperand=" + encodeURIComponent(activity) + "&advfilters[0].operator=inc";
    if (loadAll != undefined && loadAll == false)
        output += "&advfilters[1].leftoperand=mhrs&advfilters[1].rightoperand=0&advfilters[1].operator=>";
    return output;
}