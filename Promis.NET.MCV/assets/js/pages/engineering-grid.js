﻿var promisDt = null;
var promisFilters = null;
var promisExporter = null;

function getContentReadyForGrid(data) {
    setContainerAsLoading("#engin-content");

    //2.callserver 
    var url = GLOBAL_DATA.APP_ROOT + "/engineering/tables/" + GLOBAL_DATA.PROJECT_DATABASE + "/" + data.dataTable + "?tableTitle=" + data.tableTitle;
    callServer(url, {
        successFunc: function (viewData) {
            $('#engin-content').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTable(data.tableName, data.columns);
        }
    });
}

function initializeDataTable(tableName, columnList) {
    promisExporter = new PromisDataExporter({
        tableName: tableName,
        advancedFiltersString: "",
        onDataRefresh: function () {
            promisDt.reinitContentForTable();
        }
    });

    promisDt = new PromisDataTable({
        ajaxUrl: GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + tableName + '/data',
        advancedFiltersString: "",
        tableName: tableName,
        columnList: columnList,
        onClickAddSimilar: onClickAddSimilar,
        onClickAdd: onClickAdd,
        onClickUpdate: onClickUpdate,
        onClickDelete: onClickDelete,
        onClickExport: promisExporter.onClickExportExcelFile,
        onClickExportTemplate: promisExporter.onClickExportExcelTemplateFile,
        onClickImport: promisExporter.onClickImportExcelFile,
        onClickAdvancedFilters: onClickAdvancedSearch
    });
    promisDt.initContentForTable();
}

//addSimilar
function onClickAddSimilar() {
    $(".modal-content").css("width", "500px");
    var url = GLOBAL_DATA.APP_ROOT + "/engineering/tables/" + GLOBAL_DATA.PROJECT_DATABASE + "/" + promisDt.tableName + "/metadata?operation=S";

    var rowNr = $('tr.selected').index();

    if (rowNr < 0)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ThereWasNoRowSelectedyouNeedToChooseOne);

    var rowId = $('tr.selected').attr('id');
    if (!rowId)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.CannotAddARowSimilarToThisRow);

    open_modal(url, {
        title: GLOBAL_DATA.TRANSLATIONS.AddSimilarRecord,
        next: populateWithSelectedRowData.bind(null, null),
        version: GLOBAL_DATA.MODAL_VERSIONS.ENGINEERING_DATABASE_NORMAL
    });
}

//add 
function onClickAdd() {
    $(".modal-content").css("width", "500px");
    var url = GLOBAL_DATA.APP_ROOT + "/engineering/tables/" + GLOBAL_DATA.PROJECT_DATABASE + "/" + promisDt.tableName + "/metadata?operation=I";
    open_modal(url, {
        title: GLOBAL_DATA.TRANSLATIONS.AddRecord,
        version: GLOBAL_DATA.MODAL_VERSIONS.ENGINEERING_DATABASE_NORMAL
    });
}

//update
function onClickUpdate() {
    $(".modal-content").css("width", "500px");
    var url = GLOBAL_DATA.APP_ROOT + "/engineering/tables/" + GLOBAL_DATA.PROJECT_DATABASE + "/" + promisDt.tableName + "/metadata?operation=U";

    var rowNr = $('tr.selected').index();
    
    //console.log("Row number is: " + rowNr);
    if (rowNr < 0)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ThereWasNoRowSelectedyouNeedToChooseOne);
    var rowId = $('tr.selected').attr('id');
    if (!rowId)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.CannotUpdateThisRow);


    open_modal(url, {
        title: GLOBAL_DATA.TRANSLATIONS.UpdateRecord,
        next: populateWithSelectedRowData,
        version: GLOBAL_DATA.MODAL_VERSIONS.ENGINEERING_DATABASE_NORMAL
    });
}

//delete
function onClickDelete() {
    $(".modal-content").css("width", "300px");

    var rowNr = $('tr.selected').index();
    if (rowNr < 0)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ThereWasNoRowSelectedyouNeedToChooseOne);

    open_modal_normal({
        title: GLOBAL_DATA.TRANSLATIONS.DeleteRecord,
        content: GLOBAL_DATA.TRANSLATIONS.DeleteSelectedRecordbrbrareYouSure +
                `<script>var operationType = "Delete";</script>
            <script src="/assets/js/pages/engineering-table.js"></script>`,
        footerButton: GLOBAL_DATA.TRANSLATIONS.Confirm,
        version: GLOBAL_DATA.MODAL_VERSIONS.ENGINEERING_DATABASE_DELETE
    });
}

function populateWithSelectedRowData(param) {
    //gets the index of the selected row
    var myrow = promisDt.table.row(promisDt.table.$('tr.selected')).index();
    //console.log("Dates:" + myrow);
    for (i = 0; i < promisDt.columns.length; i++) {
        v = promisDt.table.cell(myrow, i).data();
        //console.log(v);
        //console.log("#" + promisDt.columns[i].ColumnName);
        $("#" + promisDt.columns[i].ColumnName.toLowerCase()).val(v);     
    }
}

function onClickAdvancedSearch() {
    if (promisFilters && promisFilters.tableName === promisDt.tableName)
        return promisFilters.openModalWithFilters();
    
    // reinit it only if not previously inited (so it can remember things);
    promisFilters = new PromisAdvancedFilters({
        tableName: promisDt.tableName,
        columns: promisDt.columns,
        onResetFilters: function () {
            // set filters to datatable and export
            promisExporter.changeFilters("");
            promisDt.changeFilters("");
        },
        callbackFunction: function (advFiltersString) {
            console.log(advFiltersString);
            // set filters to datatable and export
            promisExporter.changeFilters(advFiltersString);
            promisDt.changeFilters(advFiltersString);
        }
    });
    promisFilters.openModalWithFilters();
}



$(".tab_setup li").on("click", function () {
    $(".tab_setup li").removeClass("active");
    $(this).addClass("active");
});