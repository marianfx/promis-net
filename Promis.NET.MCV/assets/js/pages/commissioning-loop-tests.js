﻿
function initProgressAndSystems() {
    $("#system-loop-check").change(onClickLoadLoop);     
}
initProgressAndSystems();

function onClickLoadLoop() {

    var selectedSystem = $("#system-loop-check").val();
    if (!selectedSystem)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.YouMustSelectASystemInOrderToContinue);

    var url = GLOBAL_DATA.APP_ROOT + "/commissioning/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/loop/tags?system=" + selectedSystem;
    callServer(url, {
        removeLoaderIfFail: true,
        successFunc: function (data) {
            //console.log(data);
            $("#tree_loop").html(data);
            onClickLoadLoopTags();
        },
        elementsToAddLoading: ['#tree_loop']
    });
}

function onClickLoadLoopTags() {
    $(".accordion .card .card-header").click(function () {
        var tag = $(this).attr("id");
        tag = tag.substr(2, tag.length);
        viewTagInfo(tag);
    });
}

function viewTagInfo(tag) {

    var url = GLOBAL_DATA.APP_ROOT + "/commissioning/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/loop/tags/table?tag=" + tag;               
    callServer(url, {
        elementsToAddLoading: ["body"],
        removeLoaderIfFail: true,
        successFunc: function (data) {                      
            $("div[id^='c_" + tag + "']").html(data);
            getTableStyle();
        }
    });
}

function getTableStyle()
{
    var buttonClasses = "";
    var progr = "";
    var coltxt = "#222";

    $(".tags-table .progress-cell").each(function () {
        var cellSearched = $(this);
        var progress = cellSearched.text();
        console.log(progress);

       // if (isNaN(progress)) {
        if (progress == "0") {
            progr = "<span class=ti-link></span>";
            coltxt = "#fff";
            cellSearched.attr("color", coltxt);
            cellSearched.html(progr);
        }
        else {
            progr = progress;
            switch (parseFloat(progress)) {
                case 0:
                    buttonClasses = "badge badge-danger";
                    break;
                case 100:
                    buttonClasses = "badge badge-success";
                    break;
                default:
                    buttonClasses = "badge badge-warning";
                    break;
            }  
            cellSearched.css("color", "#222");
            cellSearched.addClass(buttonClasses);
            cellSearched.text(progr);
        }
       
        
    });
}