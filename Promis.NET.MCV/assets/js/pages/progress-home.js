﻿var overAllChart = null;
var monthlyChart = null;

function initProgressHome() {
    //first timers
    configure_chart(GLOBAL_DATA.USER_THEME);
    reinitCharts();

    // set date
    var objDate = new Date(),
        locale = GLOBAL_DATA.USER_LANGUAGE,
        day = objDate.getDate(),
        month = objDate.toLocaleString(locale, { month: "long" }),
        year = objDate.getFullYear();
    $(".current-datetime span").text(day.toString() + " " + month.toUpperCase() + " " + year.toString());

    // click events
    $('#main-activities').change(reinitCharts);
};
initProgressHome();


function reinitCharts() {
    var selected = $('#main-activities').val();
    var isForAllActivities = false;

    if (!selected) {
        isForAllActivities = true;
    }

    // hide table info and remove charts on change
    $(".table-progress-cell").removeClass("table-progress-cell-current");
    $(".progress-info").hide();
    $(".planned-cell").text("");
    $(".physical-cell").text("");

    if (overAllChart)
        overAllChart.destroy();
    if (monthlyChart)
        monthlyChart.destroy();

    // init progress charts
    var url = GLOBAL_DATA.APP_ROOT + "/api/charts/progress" + (isForAllActivities ? "" : "?activity=" + encodeURIComponent(selected));
    callServer(url, {
        successFunc: function (data) {
            //progress_init_chart(data.ProgressChartLabels, data.ProgressChartValuesPlanned, data.ProgressChartValuesActual, data.ProgressChartValuesCertified, data.ProgressMonthlyChartValuesActual, data.ProgressMonthlyChartValuesDelay);
            console.log(data);
            initProgressCharts(data.ProgressChartLabels, data.ProgressChartValuesPlanned, data.ProgressChartValuesActual, data.ProgressMonthlyChartValuesActual);
            initProgressTable(data.ProgressChartValuesPlanned, data.ProgressChartValuesActual);
        },
        elementsToAddLoading: ['#overall-data-container']
    });
}

function initProgressCharts(labels, data_planned, data_actual, data_actual_m) {
    var config = {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: GLOBAL_DATA.TRANSLATIONS.Planned,
                data: data_planned,
                borderColor: 'rgba(54, 162, 235, 1)',
                backgroundColor: 'rgba(54, 162, 235, 0.5)',
                fill: false
            },
            {
                label: GLOBAL_DATA.TRANSLATIONS.Physical,
                data: data_actual,
                borderColor: 'rgba(4,110,23,1.00)',
                backgroundColor: 'rgba(4,110,23,0.3)',
                fill: true
            }]
        },
        options: {
            title: {
                display: true,
                text: GLOBAL_DATA.TRANSLATIONS.OverallProjectProgressStatus,
                fontSize: 22,
                fontStyle: 'normal',
                padding: 10
            },
            legend: {
                display: true,
                labels: {
                    fontSize: 14
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff",
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.05)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff"
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.05)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    }
                }]
            }
        }
    }
    
    var config2 = {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: GLOBAL_DATA.TRANSLATIONS.Physical,
                data: data_actual_m,
                borderColor: 'rgba(4,110,23,1.00)',
                backgroundColor: 'rgba(4,110,23,1)',
                fill: true
            }]
        },
        options: {
            title: {
                display: true,
                text: GLOBAL_DATA.TRANSLATIONS.MonthlyProgressStatus,
                fontSize: 22,
                fontStyle: 'normal',
                padding: 10
            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff",
                        beginAtZero: true,
                        min: 0,
                        stepSize: 10
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.05)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff"
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.05)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    }
                }]
            }
        }
    };

    var ctx = $("#chart-progress-all");
    overAllChart = new Chart(ctx, config);
    var ctx2 = $("#chart-progress-month");
    monthlyChart = new Chart(ctx2, config2);
}

function initProgressTable(data_planned, data_actual) {
    // visuals
    var currentMonth = new Date().getMonth() + 1;
    var currentYear = new Date().getFullYear();
    
    var currentYearOnTable = $("[data-month='" + currentMonth + "'][data-year='" + currentYear + "']");
    if (!currentYearOnTable)//means old project, not having in it the current month + year => select the last year
        $(".table-progress-cell:last-child").addClass("table-progress-cell-current");
    else
        currentYearOnTable.addClass("table-progress-cell-current");

    // add data to table
    initProgressRow(".planned-cell", data_planned);
    initProgressRow(".physical-cell", data_actual);

    // set delay / completed / etc
    var lastIndexElement = $("#table-reference-index");
    var lastIndex = $('#table-progress-firstrow .table-progress-cell:last-child').attr("data-index");
    if (lastIndexElement)
        lastIndex = lastIndexElement.attr("data-index");
    lastIndex = parseInt(lastIndex);
    var lastValuePlanned = lastIndex < data_planned.length ? data_planned[lastIndex] : "";
    var lastValuePhysical = lastIndex < data_actual.length ? data_actual[lastIndex] : "";

    $("#table-progress-lastrow td:first-child").attr("colspan", (lastIndex + 1));
    if (_.isFinite(lastValuePlanned) && _.isFinite(lastValuePhysical) && parseFloat(lastValuePlanned) > parseFloat(lastValuePhysical)) {
        var diff = (parseFloat(lastValuePlanned) - parseFloat(lastValuePhysical)).toFixed(1);
        $("#table-progress-delayfull").text(GLOBAL_DATA.TRANSLATIONS.Delay+" "+ + diff + "%");
        $(".progress-delayed").show();
    }
    else if (lastValuePhysical == "" || parseFloat(lastValuePhysical) == 100)
        $(".progress-completed").show();
    else if (!_.isFinite(lastValuePlanned) || lastValuePlanned < lastValuePhysical)
        $(".progress-otherx").show();
    else
        $(".progress-other").show();
    console.log("Info", lastValuePlanned, lastValuePhysical);
}

// returns {lastIndex, lastValue}
function initProgressRow(rowTypeIdentifier, data) {
    var index = 0;
    _.forEach($(rowTypeIdentifier), function (cell) {
        var value = data[index];
        value = (!value || value == 0 ? "" : value);
        $(cell).text(value);
        index++;
    });
}