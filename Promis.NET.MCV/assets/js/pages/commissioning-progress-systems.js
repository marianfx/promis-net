﻿var promisDt = null;
var table = "";
var CommissChart = null;

function initProgressAndSystems() {  

    verifyTagsNotAssigned();
    configure_chart(GLOBAL_DATA.USER_THEME);
    initChart("PR");
    initChart("CO");
}

initProgressAndSystems();

function verifyTagsNotAssigned() {

    $(".tags-not-assigned").click(function () {
        var text = $(this).text();
        if (!text || text == 0)
            return;

        var id = $(this).attr('id');
        var dataTable = $(this).attr('data-table');
        viewTagsNotAssigned(dataTable);
    });
}

function viewTagsNotAssigned(table) {
    $(".modal-content").css("width", "800px");

    var modalTitle = GLOBAL_DATA.TRANSLATIONS.TagsNotAssigned;
    if (!table)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.TheTableCannotBeNull);

    var url = GLOBAL_DATA.APP_ROOT + "/commissioning/" + GLOBAL_DATA.PROJECT_DATABASE + "/progress/tags/notassigned?dataTable=" + table;
    open_modal(url, {
        title: modalTitle,
        elementsToAddLoading: [".modal-content"],
        next: function (viewData) {
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTable(data.tableName, data.columns);
        },
        version: GLOBAL_DATA.MODAL_VERSIONS.COMMISSIONING_INSPECTION_STATUS_PROGRESS
    });

}

function initializeDataTable(dataTable, theColumns) {

    if (!dataTable)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.TheDatatableCannotBeNull);

    var filters = getTypecodeFilter();
    console.log(filters);
    
    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + '/data';

    promisDt = new PromisDataTable({
        tableName: dataTable,
        columnList: theColumns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 545,
        fontSize: 14,
        minHeight: 440
    });
    promisDt.initContentForTable();
}

function getTypecodeFilter(thePrefix) {
    return ("?advfilters[0].leftoperand=system&advfilters[0].rightoperand=&advfilters[0].operator=eq");
}

function initChart(sufix) {
    var ctx = "";
    var titleCom = "";

    var url = GLOBAL_DATA.APP_ROOT + "/api/commissioning/data/"+ GLOBAL_DATA.PROJECT_DATABASE +"/charts?sufix=" + sufix;

    if (sufix == "PR") {
        ctx = $("#chart-precom_overall");
        titleCom = GLOBAL_DATA.TRANSLATIONS.PrecommissioningProgressStatus;
    }
    else
    {
        ctx = $("#chart-commissioning_overall");
        console.log(ctx);
        titleCom = GLOBAL_DATA.TRANSLATIONS.CommissioningProgressStatus;
    }

    callServer(url, {
        successFunc: function (data) {
            console.log(data);
            initProgressChart(data.ProgressChartLabels, data.ProgressChartValuesActual, data.ProgressChartValuesPlanned, ctx, titleCom);
        },
        elementsToAddLoading: ['#charts-area']
    });
  
}

function initProgressChart(labels, data_actual, data_planned, context, titleCom) {

    //for charts
    var configChart = {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: GLOBAL_DATA.TRANSLATIONS.Planned,
                data: data_planned,
                borderColor: 'rgba(54, 162, 235, 1)',
                backgroundColor: 'rgba(54, 162, 235, 0.5)',
                fill: false
            },
            {
                label: GLOBAL_DATA.TRANSLATIONS.Physical,
                data: data_actual,
                borderColor: 'rgba(4,110,23,1.00)',
                backgroundColor: 'rgba(4,110,23,0.3)',
                fill: true
            },
            ]
        },
        options: {
            title: {
                display: true,
                text: titleCom,
                fontSize: 20,
                fontStyle: 'normal',
                padding: 10
            },
            legend: {
                display: true,
                labels: {
                    fontSize: 14
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff",
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.05)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff"
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.05)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    }
                }]
            },
            plugins: {
                filler: {
                    propagate: true
                }
            }
        }
    };
    var CommissChart = new Chart(context, configChart);
}
