﻿var selectedTag = null;
var steps = [];

function initInspectionTypeSelection() {
    //addPageUnderConstructionMarker("#commissioning-inspection-followup");
    $('#main-act').change(loadDatatables);
    $('#table-selector').change(loadDataTable);
    $("#save-followup").click(saveAll);
    //$('#save-pres-followup').click(saveFollowUpChanges);
}
initInspectionTypeSelection();


function loadDatatables() {
    //clear all
    $('#table-selector').val("");
    $('#table-selector .to-hide').hide();
    $('#tags-table-container').html("");
    $('#inspections-status-table').html("");
    //--
    var selected = $("#main-act").val();
    if (!selected)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AnActivityMustBeSelectedInOrderToContinue);
    $('#table-selector .to-hide').show();
}

function loadDataTable() {
    //clearing before elements
    var tableName = $('#table-selector').val();
    var selectedAct = $('#main-act').val();
    $('#tags-table-container').html("");
    $('#inspections-status-table').html("");
    promisDt = null;
    //--
    if (!tableName)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ADatatableMustBeSelectedInOrderToLoadTheTableForIt);

    var url = GLOBAL_DATA.APP_ROOT + "/commissioning/" + GLOBAL_DATA.PROJECT_ID + "/inspection-follow-up/table/partial?tableName=" + encodeURIComponent(tableName);
    callServer(url, {
        elementsToAddLoading: ["#tags-table-container"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $('#tags-table-container').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTable(tableName, data.columns, selectedAct);
        },
    });
}

function initializeDataTable(dataTable, theColumns, activity) {
    var filters = getAdvancedFiltersFollowUp(activity);
    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + encodeURIComponent(dataTable) + '/data';
    promisDt = new PromisDataTable({
        tableName: dataTable,
        columnList: theColumns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 420,
        rowsPerPage: 25,
        hasRowSelect: true,
        isStriped: true,
        onRowSelect: function (row) {
            selectedTag = $(row).children("td:first-child").text();
            loadStatusTable(row.id.slice(4), dataTable, activity);
        }
    });
    promisDt.initContentForTable();
}

function loadStatusTable(rowId, datatable, activity) {
    //clearing before elements
    $('#inspections-status-table').html("");
    // dataTable, string rowId, string mainActivity --
    var url = GLOBAL_DATA.APP_ROOT + "/commissioning/" + GLOBAL_DATA.PROJECT_ID + "/inspection-follow-up/statusTable/partial?&dataTable=" + encodeURIComponent(datatable) + "&rowId=" + encodeURIComponent(rowId) + "&mainActivity=" + encodeURIComponent(activity);
    callServer(url, {
        elementsToAddLoading: [".act_right"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $('#inspections-status-table').html(viewData);
            //var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeInspectionDataTable("subdivision");
        },
    });
}

function initializeInspectionDataTable(dataTable) {
    promisDt = new PromisDataTable({
        tableName: dataTable,
        hasOrdering: false,
        hasScrollCollapse: false,
        hasScrollX: false,
        isResponsive: true,
        hasRowSelect: false,
        hasAjaxDataLoad: false,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 230,
        rowsPerPage: 25,
    });
    steps = [];

    $(".clickable").click(function () {
        var parent = $(this).closest("tr");
        var input = parent.find("td .check-input");
        var oldValue = input.attr("data-initialval");
        var textarea = parent.find("td .check-textarea");
        var step = input.attr("data-step");
        _.remove(steps, function (o) {
            return o.Value == step;
        });

        if (textarea.val().trim() == "") {
            input.prop("checked", true);
            textarea.val(userFullName + "\r" + new Date().toLocaleDateString());
        }
        else {
            input.prop("checked", false);
            textarea.val("");
        }
        var result = input.is(":checked") ? "N" : "A";
        if (textarea.val() != oldValue) {
            steps.push({
                Key: result,
                Value: step
            });
        }
    });

}

function getAdvancedFiltersFollowUp(activity) {
    return ("?advfilters[0].leftoperand=" + activity + "_activity&advfilters[0].rightoperand=&advfilters[0].operator=<>")
};

function saveAll() {
    var tag = selectedTag;
    var mainact = $('#main-act').val();
    var datatable = $('#table-selector').val();
    if (!tag)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ARowTagMustBeSelectedInOrderToContinue);
    if (!mainact)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AnActivityMustBeSelectedInOrderToContinue);
    if (!datatable)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ADatatableMustBeSelectedInOrderToContinue);
   
    console.log(steps);
    var url = GLOBAL_DATA.APP_ROOT + "/api/commissioning/" + GLOBAL_DATA.PROJECT_DATABASE + "/inspections/" + encodeURIComponent(tag)
        + "?mainActivity=" + encodeURIComponent(mainact)
        + "&dataTable=" + encodeURIComponent(datatable);

    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(steps),
        elementsToAddLoading: ["body"],
        removeLoaderIfFail: true,
        successFunc: function (reciveData) {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.DataSaved, "success");
        }
    });
}
