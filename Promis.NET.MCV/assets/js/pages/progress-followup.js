﻿var promisExporter = null;
var followupRowsPerPage = 100;
var tableMinHeight = 400;
var marginFromElements = 540;
var followupPaging = null;

function initProgressFollowup() {
    //addPageUnderConstructionMarker("#followup");
    // click events
    $('#main-activities').change(loadActivities);
    $('#related-activities').change(loadActivities);
    $('#sub-activities').change(loadChartsAndData);
    $('#areas').change(function () {
        var subact = $('#sub-activities').val();
        if (!subact || subact == "")
            return displayNotify(GLOBAL_DATA.TRANSLATIONS.AnSubactivityMustBeSelectedInOrderToLoadTheTableForIt);
        initializeTable(subact);
    });
    $(window).resize(onResize);

    initProgressChartsVisually();

    //click on EXPORT. Works thanks to the use of the import-export.js file
    // Export here depends on the main activity
    $('#button-export-excel').click(function () {
        var mainact = $('#main-activities').val();
        var subact = $('#sub-activities').val();
        if (!mainact || mainact == "")
            return displayNotify(GLOBAL_DATA.TRANSLATIONS.AMainActivityMustBeSelectedInOrderToContinue);

        var table = $("#followup-container-card").attr("data-progresstable").replace("@ma", mainact);
        var filters = "?" + getProgressTableFilters(subact).substr(1);
        promisExporter = new PromisDataExporter({
            tableName: table,
            advancedFiltersString: filters
        });
        promisExporter.onClickExportExcelFile();
    });

    $("#button-export-pdf").click(pdfExport);
}
initProgressFollowup();

function onResize() {
    var containerHeight = $('body').innerHeight() - marginFromElements;
    var contentHeight = containerHeight > tableMinHeight ? containerHeight : tableMinHeight;

    $("#followup-table-rows-container").css("height", contentHeight + "px");
    $("#followup-table-rows-container").css("max-height", contentHeight + "px");
}


function loadActivities() {
    var selected = $(this).val();
    var mainActivity = $("#main-activities").val();
    var next = $(this).attr("data-next");
    var path = $(this).attr("data-path");

    // reset all
    $("#promisdt-followup").html("");
    $("#umid").text("");
    initProgressChartsVisually();

    if (!selected || !next || !path)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AnActivityMustBeSelectedInOrderToContinue);

    var url = GLOBAL_DATA.APP_ROOT + "/api/project/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/activities/" + encodeURIComponent(selected) + "/" + encodeURIComponent(path) + "?filterEmpty=true&mainActivity=" + encodeURIComponent(mainActivity);
    callServer(url, {
        elementsToAddLoading: ['.followup-left'],
        removeLoaderIfFail: true,
        successFunc: function (activities) {
            //clear all others
            var tempNex = next;
            //console.log(tempNex);
            while (tempNex) {
                $(tempNex).html("<option value=''>&nbsp;</option>");
                tempNex = $(tempNex).attr("data-next");
            }

            if (!_.isArray(activities) || activities.length == 0)
                return displayNotify(GLOBAL_DATA.TRANSLATIONS.ThereIsNoDataForThatSelection);

            // put actual loaded activities
            _.forEach(activities, function (activity) {
                $(next).append("<option value='" + activity.Code + "'>" + activity.Code + " - " + activity.Description + "</option>");
            });
        }
    });
}

function loadChartsAndData() {
    $("#promisdt-followup").html("");
    $("#umid").text("");
    var mainact = $('#main-activities').val();
    var subact = $('#sub-activities').val();

    if (!subact || subact == "")
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AnSubactivityMustBeSelectedInOrderToLoadTheTableForIt);

    // load UM
    initializeUm(subact);

    // load charts
    initializeCharts();

    followupPaging = getDefaultFollowupPaging();
    var url = GLOBAL_DATA.APP_ROOT + "/progress/" + GLOBAL_DATA.PROJECT_ID + "/followup/table/partial?mainActivity=" + encodeURIComponent(mainact) + "&subActivity=" + encodeURIComponent(subact);
    callServer(url, {
        elementsToAddLoading: ["#followup-container-card"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $("#promisdt-followup").html(viewData);
            initializeTableEvents(subact);
        }
    });
}

function initializeUm() {
    var subact = $('#sub-activities').val();
    if (!subact || subact == "")
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AnSubactivityMustBeSelectedInOrderToLoadTheTableForIt);

    var url = GLOBAL_DATA.APP_ROOT + "/api/project/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/subactivities/" + encodeURIComponent(subact) + "/um";
    callServer(url, {
        successFunc: function (um) {
            $("#umid").text(um);
        }
    });
}


function initializeCharts() {
    var mainact = $('#main-activities').val();
    var act = $("#related-activities").val();
    var subact = $('#sub-activities').val();
    if (!subact || subact == "")
        return displayNotify("An sub-activity must be selected in order to continue.");

    var url = GLOBAL_DATA.APP_ROOT + "/api/progress/" + GLOBAL_DATA.PROJECT_DATABASE + "/trends/all"
        + "?mainActivity=" + encodeURIComponent(mainact)
        + "&activity=" + encodeURIComponent(act)
        + "&subActivity=" + encodeURIComponent(subact);
    callServer(url, {
        elementsToAddLoading: [".followup-left"],
        removeLoaderIfFail: true,
        successFunc: function (data) {
            var progressActArray = [data[0].Percent, data[0].PercentWeekly, data[0].PercentMonthly];
            var progressSubactArray = [data[1].Percent, data[1].PercentWeekly, data[1].PercentMonthly];
            initProgressChartsVisually(progressActArray, progressSubactArray);
        }
    });
}

function initProgressChartsVisually(progressActArray, progressSubactArray) {
    if (!_.isArray(progressActArray))
        progressActArray = [0, 0, 0];
    if (!_.isArray(progressSubactArray))
        progressSubactArray = [0, 0, 0];
    //console.log("Received arrays ", progressActArray, progressSubactArray);

    helpAddingChartValues("#chart-activity-day", progressActArray[0]);
    helpAddingChartValues("#chart-activity-week", progressActArray[1]);
    helpAddingChartValues("#chart-activity-month", progressActArray[2]);

    helpAddingChartValues("#chart-subactivity-day", progressSubactArray[0]);
    helpAddingChartValues("#chart-subactivity-week", progressSubactArray[1]);
    helpAddingChartValues("#chart-subactivity-month", progressSubactArray[2]);
}

function helpAddingChartValues(identifier, v) {
    $(identifier + " h5").text(v);
    v = v > 1 ? (v / 100.0).toFixed(2) : v.toFixed(2);//values need to be in [0,1]
    $(identifier + " .progress_profile").circleProgress({
        value: v,
        fill: { gradient: ["#2ec7cb", "#6c8bef"] },
        lineCap: 'butt'
    });
}


function initializeTableEvents(subact) {
    // click / change events
    $("#searchfield").keypress(function () {
        if (window.event.keyCode == 13 || window.event.keyCode == 9) {
            followupPaging = getDefaultFollowupPaging();
            initializeTable(subact);
        }
    });

    $("#previous-page").click(function () {
        goPage(followupPaging.currentPage - 1);
    });
    $("#next-page").click(function () {
        goPage(followupPaging.currentPage + 1);
    });

    initializeTable(subact);
}

function initializeTable(subact) {
    var start = followupPaging.currentStart;
    var progressTable = $("#promisdt-followup-inner").attr("data-tablename");
    var lastIndex = Number($("#promisdt-followup-inner").attr("data-finalindex"));
    $("#followup-table-rows").html("");

    var url = GLOBAL_DATA.APP_ROOT + "/api/engineering/tables/" + GLOBAL_DATA.PROJECT_DATABASE + "/" + progressTable
        + "/data?start=" + start + "&length=" + followupRowsPerPage
        + "&order[0].column=&order[0].func=tag";
    url += getProgressTableFilters(subact);
    //console.log(url);

    callServer(url, {
        elementsToAddLoading: ["#followup-container-card"],
        removeLoaderIfFail: true,
        filterOnlyDataField: false,
        successFunc: function (result) {
            //console.log("Success", data);
            // stop condition
            if (!result || !_.isArray(result.data) || !result.recordsFiltered || result.recordsFiltered <= 0) {
                displayNotify("No tag data to display.");
                for (var i = 0; i < 20; i++) {
                    $("#followup-table-rows").append("<tr><td>&nbsp;</td></tr>");
                }
                return;
            }

            buildUpPages(result.recordsFiltered, result.data.length);
            var data = result.data;
            onResize();
            _.forEach(data, function (d) {
                var tr = `<tr data-tag='` + d.TAG + `' id="` + d.DT_RowId + `" class="right">
                            <td class="cell-large left">` + d.TAG + `</td>
                            <td class="cell-medium">` + Number(d.QTY).toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + `</td>`;
                var totalRowPercent = 0.0;
                var latestS = "";
                var latestSS = "";
                var latestTag = d.TAG;
                for (var j = 0; j < 12; j++) {
                    var s = d["S" + (j + 1).toString()];
                    var ss = j > lastIndex ? "" : (s == "100,00" ? "N/A" : (s == "100.00" ? "100" : s));
                    var editable = (j <= lastIndex && ss != "N/A");
                    var bgColor = ss == "N/A" ? " style='background-color:rgba(153,0,0,0.2);'" : "";
                    tr += "<td data-s='s" + (j + 1) + "' class='cell-small" + (editable ? " editable" : "") + "'" + bgColor + ">" + ss + "</td>";
                    ss = Number(ss);
                    //console.log("Some data ss", ss);
                    if (editable && _.isFinite(ss)) {
                        var upperPercent = $(".header-cell-" + (j.toString())).text().trim();
                        upperPercent = Number(upperPercent);
                        //console.log("Percent upper", upperPercent);
                        if (_.isFinite(upperPercent))
                            totalRowPercent += (upperPercent * Number(ss) / 100.0);
                        latestS = "s" + (j + 1).toString();
                        latestSS = ss;
                    }
                }

                tr += `     <td id='pr-` + d.DT_RowId + `' class='cell-medium-small'>` + Number(d.PR).toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + `</td>
                            <td>&nbsp;</td>
                        </tr>`;
                $("#followup-table-rows").append(tr);

                if (Math.round(totalRowPercent) != Math.round(Number(d.PR))) {
                    console.log(GLOBAL_DATA.TRANSLATIONS.WillUpdateFor, latestS, latestSS, latestTag);
                    if (isWriter)
                        persistChanges(latestS, latestTag, latestSS);
                }
            });

            // add clicks
            if (isWriter)
                addClick();
        }
    });
}

function getProgressTableFilters(subact) {
    var url = "";
    var searchValue = $("#searchfield").val();
    var area = $("#areas").val();

    if (subact && subact != "")
        url += "&advfilters[0].leftoperand=sub_act&advfilters[0].rightoperand=" + subact;
    if (area && area != "")
        url += "&advfilters[1].leftoperand=wbs&advfilters[1].rightoperand=" + area;
    if (searchValue && searchValue != "")
        url += "&advfilters[1].leftoperand=tag&advfilters[1].operator=inc&advfilters[1].rightoperand=" + searchValue;
    return url;
}

function getDefaultFollowupPaging() {
    return {
        currentPage: 1,
        currentStart: 0,
        totalPages: 1,
        totalRows: 0
    };
}

function buildUpPages(totalRows, shownRows) {
    followupPaging.totalPages = Math.ceil(totalRows / followupRowsPerPage);
    var combo = `Page &nbsp;<select id="followup-paginator">`;
    for (var i = 1; i <= followupPaging.totalPages; i++) {
        combo += `<option value="` + i + `" ` + (i == followupPaging.currentPage ? "selected" : "") + `>` + i + `</option>`
    }
    combo += `</select>&nbsp;/&nbsp;` + followupPaging.totalPages + `"&nbsp;&nbsp;[&nbsp;Tags:` + totalRows + `&nbsp;]`;
    $("#page-text").html(combo);

    $("#followup-paginator").change(function () {
        goPage($(this).val());
    });
}

function goPage(newPage) {
    var prevVal = followupPaging.currentPage;
    followupPaging.currentPage = newPage < 1 ? 1 : (newPage > followupPaging.totalPages ? followupPaging.totalPages : newPage);
    followupPaging.currentStart = (followupPaging.currentPage - 1) * followupRowsPerPage;


    var subact = $('#sub-activities').val();

    console.log("Info", prevVal, followupPaging.currentPage);
    if (prevVal == followupPaging.currentPage)
        return;

    if (!subact)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AnSubactivityMustBeSelectedInOrderToLoadTheTableForIt);

    initializeTable(subact);
}

function addClick() {
    $(".editable").each(function () {
        var value = $(this).text();
        var html = `<input type='text' class='form-control editable-input' value='` + value + `' data-old='` + value + `' maxlength='5'>`;
        $(this).html(html);
    });
    $(".editable-input").blur(onInputValueChanged);
    $(".editable-input").keypress(function () {
        if (window.event.keyCode == 13 || window.event.keyCode == 9) {
            onInputValueChanged.bind(this)();
        }
    });
}

function onInputValueChanged() {
    setElementAsValid(this);
    var oldVal = $(this).attr("data-old"); oldVal = parseFloat(oldVal);
    var thisVal = $(this).val();
    if (thisVal) {
        thisVal = thisVal.replace(",", ".");
        thisVal = parseFloat(thisVal);
    }

    if (thisVal == oldVal)
        return;//no changes

    var parent = $(this).closest(".editable");
    var s = parent.attr("data-s");
    var tag = parent.closest("tr").attr("data-tag");

    if (!_.isFinite(thisVal) || thisVal < 0 || thisVal > 100) {
        setElementAsInvalid(this);
        if (_.isFinite(oldVal))
            $(this).val(oldVal);
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.InvalidValueInsertedMustBeValidPercent);
    }

    $(this).attr("data-old", thisVal);

    //call server
    if (isWriter)
        persistChanges(s, tag, thisVal);
}

function persistChanges(s, tag, thisVal) {
    var mainact = $('#main-activities').val();
    var subact = $('#sub-activities').val();

    if (!subact || subact == "" || !mainact || mainact == "")
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AnSubactivityMustBeSelectedInOrderToLoadTheTableForIt);

    // all good
    var url = GLOBAL_DATA.APP_ROOT + "/api/progress/" + GLOBAL_DATA.PROJECT_DATABASE + "/subactivities/" + encodeURIComponent(subact) + "/progress"
        + "?mainActivity=" + encodeURIComponent(mainact)
        + "&tag=" + encodeURIComponent(tag);
    var data = JSON.stringify({
        Key: s,
        Value: thisVal
    });

    console.log(GLOBAL_DATA.TRANSLATIONS.IClickedSomethingEditable, s, subact, tag, thisVal);
    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: data,
        elementsToAddLoading: ["#followup-container-card"],
        removeLoaderIfFail: true,
        successFunc: function (newPercent) {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.UpdatedWithSuccess, "success");
            newPercent = Number(newPercent).toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })
            $("tr[data-tag='" + tag + "'").children("td:nth-last-child(2)").text(newPercent);
            initializeCharts();
        }
    });
}

function pdfExport() {
    //return displayNotify(GLOBAL_DATA.TRANSLATIONS.FunctionalityNotAvailable);
    var mainact = $('#main-activities').val();
    var subact = $('#sub-activities').val();
    var progressTable = $("#promisdt-followup-inner").attr("data-tablename");

    if (!mainact || mainact == "" || !subact || subact == "")
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.AnActivityMustBeSelectedInOrderToContinue);

    open_modal_normal({
        content: "<iframe id='docs-iframe' allowfullscreen></iframe>",
        footerHtml: "",
        title: "",
        version: GLOBAL_DATA.MODAL_VERSIONS.PDF_EXPORT_PROGRESS,
    });

    // call server to retrieve pdf path
    var apiUrl = GLOBAL_DATA.APP_ROOT + "/api/progress/" + GLOBAL_DATA.PROJECT_ID + "/export/pdf"
        + "?table=" + encodeURIComponent(progressTable)
        + "&subActivity=" + encodeURIComponent(subact);
    callServer(apiUrl, {
        elementsToAddLoading: [".right-container"],
        removeLoaderIfFail: true,
        successFunc: function (fileUrl) {
           if (!fileUrl || fileUrl == "")
               return displayNotify(GLOBAL_DATA.TRANSLATIONS.NoDataFound);

            var url = GLOBAL_DATA.APP_ROOT + "/content/pdf"
                + "?document=" + encodeURIComponent(fileUrl)//fileUrl
                + "&title=" + encodeURIComponent(" ")
                + "&pageMode=single";
            $("#docs-iframe").attr("src", url);
            setTimeout(function () {
                $("#docs-iframe").show();
            }, 600);
        }
    });
}