﻿function initSetupCutoff() {
    // init calendar events
    GLOBAL_DATA.setup.cutoff.language = GLOBAL_DATA.USER_LANGUAGE;
    initializeCalendar();

    // language
    $('.language-changer').click(onChangeLanguage);
}
initSetupCutoff();

var cutoffCalendar;

function onChangeLanguage() {
    var lang = $(this).val();
    cutoffCalendar.setLanguage(lang);
    changeLanguage(lang);
}

function changeLanguage(lang) {
    GLOBAL_DATA.setup.cutoff.language = lang;
    _.forEach($('.language-changer'), function (element) {
        $(element).removeClass('active');
    });
    $(".language-changer[value='" + lang + "'").addClass('active');
}

function initializeCalendar() {
    cutoffCalendar = $('#calendar').calendar({
        style: 'background',
        displayWeekNumber: true,
        language: GLOBAL_DATA.setup.cutoff.language,
        minDate: new Date(GLOBAL_DATA.setup.cutoff.firstDay),
        maxDate: new Date(GLOBAL_DATA.setup.cutoff.lastDay),
        customDayRenderer: function (element, date) {
            var indexFound = _.findIndex(GLOBAL_DATA.setup.cutoff.allDays, function (obj) {
                return date.getTime() === new Date(obj).getTime();
            });
            if (indexFound >= 0) {
                //console.log(indexFound);
                $(element).addClass("cutoff bg-success")
            }
        },
        clickDay: function (e) {
            var isInsert = true;
            if ($(e.element).find("div").hasClass("cutoff bg-success"))
                isInsert = false;

            $(e.element).find("div").toggleClass("cutoff bg-success");
            var year = $(".year-title:not(.hidden-xs)").html();
            var month = e.date.getMonth();
            var day = e.date.getDate();
            checkDay(isInsert, year, month, day);
        }
    });
    changeLanguage(GLOBAL_DATA.USER_LANGUAGE);
}

function checkDay(isInsert, year, month, day) {
    var date = new Date(year, month, day);
    console.log(date);
    console.log(isInsert);
    var successMessage = GLOBAL_DATA.TRANSLATIONS.CutOffDate + (isInsert ? GLOBAL_DATA.TRANSLATIONS.Inserted : GLOBAL_DATA.TRANSLATIONS.Deleted) + GLOBAL_DATA.TRANSLATIONS.WithSuccess;
    var callType = (isInsert ? GLOBAL_DATA.CALL_TYPES.POST : GLOBAL_DATA.CALL_TYPES.DELETE);
    var url = GLOBAL_DATA.APP_ROOT + "/api/project/data/" + encodeURIComponent(GLOBAL_DATA.PROJECT_DATABASE) + "/cutoff";

    callServer(url, {
        callType: callType,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        elementsToAddLoading: [".cat"],
        removeLoaderIfFail: true,
        data: JSON.stringify(date),
        successFunc: function () {
            displayNotify(successMessage, "success");
        }
    });
}
