﻿// ### REGION INIT
function initActivitiesHome() {
    configure_chart(GLOBAL_DATA.USER_THEME);

    //init outstandings charts
    callServer(GLOBAL_DATA.APP_ROOT + "/api/charts/outstandings", {
        successFunc: function (data) {
            outstandings_init_chart(data.OutstandingChartFlags, data.OutstandingChartCounts);
            outstandings_init_table(data.OutstandingChartFlags, data.OutstandingChartFlagDescriptions);
            outstandings_init_counts(data.PunchlistCount, data.PunchlistDoneCount, data.ClearancePercentage);

            $('.progress_profile4').circleProgress({
                fill: { gradient: ["#1aa7cf", "#15568c"] },
                lineCap: 'butt'
            });
        },
        elementsToAddLoading: ['#outstanding-container']
    });
}

initActivitiesHome();
// ### END


function outstandings_init_chart(flags, counts) {
    var bgColors = [];
    var borderColors = [];
    for (flag in flags) {
        bgColors.push('rgba(75, 192, 192, 0.5)');
        borderColors.push('rgba(75, 192, 192, 1)');
    }

    var data_outs = {
        labels: flags,
        datasets: [{
            label: "",
            backgroundColor: bgColors,
            borderColor: borderColors,
            borderWidth: 1,
            data: counts,
        }]
    };

    var ctx = $("#outstandings-chart");
    var myBarChart2 = new Chart(ctx, {
        type: 'bar',
        data: data_outs,
        options: {
            legend: {
                display: false
            },
            responsive: false
        }
    });
}

function outstandings_init_table(flags, descriptions) {
    var tableElement = $('<table id="outstanding-info-table" class="table table-sm"></table>');
    for (i in flags) {
        var flag = flags[i];
        var description = descriptions[i];
        var html = `<tr>
                        <td class="outstanding-table-flag h6">
                            <b>` + flag + `</b>
                        </td>
                        <td class="h6 outstanding-table-desc">
                            <small>` + description + `</small>
                        </td>
                    </tr>`;
        tableElement.append(html);
    }
    $('#outstanding-info').html(tableElement);
}

function outstandings_init_counts(count, donecount, clearancePercentage) {

    var percentage_rounded = (clearancePercentage / 100).toFixed(1);
    var html = `<div class="row no-margin">
                    <div class="col">
                        <label class="ribbon right danger">
                            <span><small>`+ GLOBAL_DATA.TRANSLATIONS.Outs +`</small> ` + count + ` </span>
                        </label>
                    </div>
                    <div class="col">
                        <label class="ribbon right success">
                            <span><small>`+ GLOBAL_DATA.TRANSLATIONS.Done +`</small> ` + donecount + ` </span>
                        </label>
                    </div>
                </div>
                <div class="row" style='margin-top: 20px;'>
                    <div class="progress_profile4 col" style="text-align: center" data-value="` + percentage_rounded + `" data-size="130" data-thickness="10" data-animation-start-value="0" data-reverse="false"></div>
                    <div style="position: absolute; top: 110px; left: 0; right: 0; text-align: center">
                        <h6 class="dashboard-h6-simple"><small>`+ GLOBAL_DATA.TRANSLATIONS.Clearancebrpercentage + `</small>&nbsp;</h6>
                        <h5 style='margin-top: -15px;'><br>` + clearancePercentage + `%</h5>
                    </div>
                </div>`;
    $('#outstandings-counts').html(html);
}