﻿// this file executes operations in a modal DEPENDENT of the engineering-grid page (opens inside it); as a result, it uses
// promisDt variable that is defined there
function initEngineeringTable() {
    switch (operationType) {
        case "Insert":
        case "Insert Similar":
            reloadFooter();
            $("#run-operation-button").click(addRecord);
            break;
        case "Update":
            reloadFooter();
            $("#run-operation-button").click(updateRecord);
            break;
        case "Delete":
            $("#run-operation-button").click(deleteRecord);
            break;     
    }

}
initEngineeringTable();


function reloadFooter() {
    console.log(GLOBAL_DATA.TRANSLATIONS.operations);
    var opText = _.find(GLOBAL_DATA.TRANSLATIONS.operations, function (o) { return o.Key == operationType; }).Value;
    var footer = `<span class='ti-star'></span>&nbsp;`+ GLOBAL_DATA.TRANSLATIONS.MandatoryFields+`&nbsp;&nbsp;&nbsp;&nbsp;
            <button id="run-operation-button" type='button' class='btn btn-sm btn-primary pull-right'>` + opText + `</button>`;
    $('#modal-container #modal-footer-content').html(footer);
}

function addRecord() {
    if (!isValid()) {
        displayNotify(GLOBAL_DATA.TRANSLATIONS.SomeRequiredFieldsAreEmpty, 'error');
        return;
    }
    
    alert("addRecord");
    var url = GLOBAL_DATA.APP_ROOT + "/api/engineering/tables/" + GLOBAL_DATA.PROJECT_DATABASE + "/" + promisDt.tableName;
    var formData = extractObjectFromForm("#form1");
    // console.log(formData);
    var arrayData = transformObjectToDataCellArray(formData);
     

    callServer(url, {
        data: JSON.stringify(arrayData),
        callType: "POST",
        contentType: "application/json",
        elementsToAddLoading: ['.modal-content'],
        removeLoaderIfFail: true,
        successFunc: function (data) {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.NewRecordAdded, 'success');
            //get the values 
            var values = {};
            _.each(arrayData, function (obj) {
                values[obj.ColumnName.toUpperCase()] = obj.Value;
            });
            console.log(values);
            promisDt.table.row.add(values).draw(false);
            //finds the new row and adds the attribute "id" and a value for it
            $('#promis-data-table-body tr:not([id])').attr("id", "row-" + data);
            close_modal();
        }
    });

}

function updateRecord() {

    if (!isValid())
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.SomeRequiredFieldsAreEmpty, 'error');
    
    var rowId = $('tr.selected').attr('id');
  
    //if (!rowId)
    //    return displayNotify("Cannot update this row!");

    var idValue = rowId.split("-")[1];
    console.log("Id value is: " + idValue);

    var url = GLOBAL_DATA.APP_ROOT + "/api/engineering/tables/" + GLOBAL_DATA.PROJECT_DATABASE + "/" + promisDt.tableName + "/" + idValue;
    var formData = extractObjectFromForm("#form1");
    var arrayData = transformObjectToDataCellArray(formData);

    callServer(url, {
        data: JSON.stringify(arrayData),
        callType: "PUT",
        contentType: "application/json",
        elementsToAddLoading: ['.modal-content'],
        removeLoaderIfFail: true,
        successFunc: function () {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.RecordSuccessfullyUpdated, 'success');
            var values = {};
            _.each(arrayData, function (obj) {
                values[obj.ColumnName.toUpperCase()] = obj.Value;
            });
            promisDt.table.row($('#' + rowId)).data(values).draw();
            close_modal();
        }
    });

}

function deleteRecord() {
    var rowId = $('tr.selected').attr('id');
    if (!rowId)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.CannotDeleteThisRow);

    var idValueForDel = rowId.split("-")[1];
    //console.log("Id value is: " + idValueForDel);

    var url = GLOBAL_DATA.APP_ROOT + "/api/engineering/tables/" + GLOBAL_DATA.PROJECT_DATABASE + "/" + promisDt.tableName + "/" + idValueForDel;
    var formData = extractObjectFromForm("#form1");
    var arrayData = transformObjectToDataCellArray(formData);

    callServer(url, {
        data: JSON.stringify(arrayData),
        callType: "DELETE",
        contentType: "application/json",
        elementsToAddLoading: ['.modal-content'],
        removeLoaderIfFail: true,
        successFunc: function () {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.RecordSuccessfullyDeleted, 'success');
            //get the values 
            var values = _.map(arrayData, "Value");
            //delete selected row and refresh table data
            promisDt.table.row($('#' + rowId)).remove().draw(false);
            close_modal();
        }
    });

}

function isValid() {
    var isValid = true
    ctrl = 0;
    setElementAsValid(".req");
    $('.req').each(function (index) {
        tmp = $(this).val()
        //console.log(tmp);
        if (tmp == '') {
            setElementAsInvalid(this);
            isValid = false;
        }
    });
    return isValid;
}

