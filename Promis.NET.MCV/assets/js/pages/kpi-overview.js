﻿var promisDt = null;
var selectedSystem = null;

// ### REGION INIT
function initKpiOverview() {
    initializeSystemsTable();
    onSelectRow(null);

    configure_chart(GLOBAL_DATA.USER_THEME);
}

initKpiOverview();
// ### END

function initializeSystemsTable() {
    var data = PromisDataTableStatics.getTableNameAndColumnsFromView($(".milestones-table-container").html());
    var filters = "";
    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + data.tableName + '/data';

    promisDt = new PromisDataTable({
        tableName: data.tableName,
        columnList: data.columns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 280,
        isStriped: true,
        rowsPerPage: 25,
        fontSize: 12,
        onRowSelect: onSelectRow
        //onrowselect
    });
    promisDt.initContentForTable();
}

function onSelectRow(row) {
    var system = "";
    if(row) system = $(row).find("td:first").text();
    $("#kpi-charts").html("");

    var andQuery = "";
    if (system && system != "") {
        selectedSystem = system;
        andQuery = "?system=" + encodeURIComponent(system);
    }

    var url = GLOBAL_DATA.APP_ROOT + "/kpis/" + GLOBAL_DATA.PROJECT_ID + "/overview/indicators/partial" + andQuery;
    callServer(url, {
        elementsToAddLoading: [".act_right"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            afterRowDataRetrieved(viewData);
        }
    });
}

function afterRowDataRetrieved(viewData) {
    $("#kpi-charts").html(viewData);
    $('.progress_profile1').circleProgress({
        fill: { gradient: ["#21dd0d", "#12610a"] },
        lineCap: 'butt'
    });

    // init punchlist chart
    var labels = JSON.parse($("#pl-status-chart").attr("data-labels"));
    var values = JSON.parse($("#pl-status-chart").attr("data-values"));
    var configPl = {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: "",
                data: values,
                borderColor: 'rgba(21,212,190,0.5)',
                backgroundColor: 'rgba(21,212,190,1)',
                fill: true
            }]
        },
        options: {
            legend: {
                display: false
            },
            layout: {
                padding: {
                    left: 20,
                    right: 20,
                    top: 0,
                    bottom: 20
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        suggestedMin: 0
                    }
                }]
            }
        }

    };
    var ctxPl = $("#pl-status-chart");
    var punchListChart = new Chart(ctxPl, configPl);


    // init construction chart
    labels = JSON.parse($("#construction-chart").attr("data-labels"));
    values = JSON.parse($("#construction-chart").attr("data-values"));
    var configConstr = {
        type: 'horizontalBar',
        data: {
            labels: labels,
            datasets: [{
                label: "",
                data: values,
                borderColor: 'rgba(21,212,190,0.5)',
                backgroundColor: 'rgba(21,212,190,1)',
                fill: true
            }]
        },
        options: {
            legend: {
                display: false
            },
            layout: {
                padding: {
                    left: 20,
                    right: 20,
                    top: 0,
                    bottom: 0
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff"
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.2)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontColor: "#b7c8ff",
                        min: 0,
                        max: 100
                    },
                    gridLines: {
                        color: "rgba(255,255,255,0.2)",
                        zeroLineColor: "rgba(255,255,255,0.2)"
                    }
                }]
            }
        }
    };

    var ctxConstr = $("#construction-chart");
    var constructionChart = new Chart(ctxConstr, configConstr);

    //// init precommissioning chart
    labels = JSON.parse($("#precommissioning-chart").attr("data-labels"));
    values = JSON.parse($("#precommissioning-chart").attr("data-values"));
    var configPreCom = _.cloneDeep(configConstr);
    configPreCom.data.labels = labels;
    configPreCom.data.datasets[0].data = values;
    var ctxPreCom = $("#precommissioning-chart");
    var precommissioningChart = new Chart(ctxPreCom, configPreCom);

    //// init commissioning chart
    labels = JSON.parse($("#commissioning-chart").attr("data-labels"));
    values = JSON.parse($("#commissioning-chart").attr("data-values"));
    var configCom = _.cloneDeep(configConstr);
    configCom.data.labels = labels;
    configCom.data.datasets[0].data = values;
    var ctxCom = $("#commissioning-chart");
    var commissioninghart = new Chart(ctxCom, configCom);
}
