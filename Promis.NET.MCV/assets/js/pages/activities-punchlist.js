﻿var promisDt = null;
var promisExporter = null;
var rowId = null;
var selectedSystem = null;
function initActivitiePunchlist() {
    //addPageUnderConstructionMarker("#punchlist")
    populateSystems();
    $('#all').change(populateSystems);
    $('#sel').change(populateSystems);
    $("#pl-sys").change(dataTable);
    $("#button-insert-new").click(onClickInsertOrUpdate.bind(null, true));
    $("#button-update-selected").click(onClickInsertOrUpdate.bind(null, false));
    $("#outstanding-search").click(searchOutstanding);
    promisExporter = new PromisDataExporter({
        tableName: "punchlist",
        advancedFiltersString: "",
        onDataRefresh: function () {
            if (promisDt)
                promisDt.reinitContentForTable();
        }
    });

    $('#button-export-excel').click(promisExporter.onClickExportExcelFile);
    $("#button-export-template").click(promisExporter.onClickExportExcelTemplateFile);
    $("#button-import-excel").click(promisExporter.onClickImportExcelFile);

    //Datepicker
    $('#pl-fdate').datepicker({
        clearBtn: false,
        todayHighlight: true,
        orientation: "bottom auto",
        daysOfWeekDisabled: "0,6",
        calendarWeeks: true,
        autoclose: true,
        language: GLOBAL_DATA.USER_LANGUAGE
    });
    $('#pl-tdate').datepicker({
        clearBtn: false,
        todayHighlight: true,
        orientation: "bottom auto",
        daysOfWeekDisabled: "0,6",
        calendarWeeks: true,
        autoclose: true,
        language: GLOBAL_DATA.USER_LANGUAGE
    });
    $('#pl-cdate').datepicker({
        clearBtn: false,
        todayHighlight: true,
        orientation: "bottom auto",
        daysOfWeekDisabled: "0,6",
        calendarWeeks: true,
        autoclose: true,
        language: GLOBAL_DATA.USER_LANGUAGE
    });	
    //end Datepicker
}

initActivitiePunchlist();

function populateSystems() {
    var radio = $('input[name=all-sys]:checked').val();
    console.log(radio);
    var URL = GLOBAL_DATA.APP_ROOT + "/api/activities/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/system/" + encodeURIComponent(radio);
    callServer(URL, {
        elementsToAddLoading: ["body"],
        successFunc: function (data) {
            $('#pl-sys').html("<option value=''></option>");
            _.forEach(data, function (system) {
                $('#pl-sys').append("<option value='" + system.Code + "'>" + system.Code + " - " + system.Description + "</option>");
            });
        }
    });
}

function dataTable(table) {
    $('#pl-table').html("")
    clearFields();
    selectedSystem = $("#pl-sys").val();
    if (!selectedSystem)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.TheSystemCannotBeNull);

    var url = GLOBAL_DATA.APP_ROOT + "/activities/" + GLOBAL_DATA.PROJECT_ID + "/system/table?systemCode=" + selectedSystem;
    callServer(url, {
        elementsToAddLoading: ["#pl-table"],
        successFunc: function (viewData) {
            $('#pl-table').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTablePlans(data.tableName, data.columns);
        },
    });

}

function initializeDataTablePlans(dataTable, theColumns) {
    var filters = "?advfilters[0].leftoperand=system&advfilters[0].rightoperand=" + selectedSystem;
    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + '/data';
    promisExporter.changeFilters(filters);

    promisDt = new PromisDataTable({
        tableName: dataTable,
        columnList: theColumns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: true,
        marginFromElements: 410,
        rowsPerPage: 25,
        hasRowSelect: true,
        isStriped: true,
        fontSize: 12,
        onRowSelect: function (row) {
            
            populateFields(row);
        }
    });
    promisDt.initContentForTable();

}

function populateFields(row) {
    var cells = $(row).children();
    $('#pl-item').val($(cells[0]).html());
    $('#pl-wd').val($(cells[1]).html());
    $('#pl-category').val($(cells[2]).html());
    $('#pl-disci').val($(cells[3]).html());
    $('#pl-actionby').val($(cells[4]).html());
    $('#pl-orig').val($(cells[5]).html());
    $('#pl-fdate').val($(cells[6]).html());
    $('#pl-status').val($(cells[7]).html());
    $('#pl-closer').val($(cells[8]).html());
    $('#pl-tdate').val($(cells[9]).html());
    $('#pl-cdate').val($(cells[10]).html());
    $('#pl-ref').val($(cells[11]).html());
    rowId = row.id.slice(4);
    var url = GLOBAL_DATA.APP_ROOT + "/api/activities/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/closure?rowId=" + row.id.slice(4);
    callServer(url, {
        removeLoaderIfFail: true,
        successFunc: function (Values) {
            $('#pl-cdoc').val(Values);
        },
    });
}

function onClickInsertOrUpdate(isInsert) {
    var item = $('#pl-item').val();
    if (!item)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.TheItemIsInvalid);
    var selectedSys = $("#pl-sys").val();
    if (!selectedSys)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.ASystemMustBeSelectedInOrderToContinue);

    var punchObj = {
        SYSTEM: $("#pl-sys").val(),
        SUBSYSTEM: $("#pl-subsys").val(),
        ITEM: $('#pl-item').val(),
        DETAILS: $('#pl-wd').val(),
        CAT: $('#pl-category').val(),
        DISCIPLINE: $('#pl-disci').val(),
        ACTIONBY: $('#pl-actionby').val(),
        ORIGINATOR: $('#pl-orig').val(),
        FOUNDDATE: $('#pl-fdate').val(),
        STATUS: $('#pl-status').val(),
        CLOSER: $('#pl-closer').val(),
        TARGETDATE: $('#pl-tdate').val(),
        CLOSEDATE: $('#pl-cdate').val(),
        DOC: $('#pl-ref').val(),
        CLOSURE: $('#pl-cdoc').val()
    };
    saveActionCall(punchObj, isInsert);
}

function saveActionCall(punchObj, isInsert) {
    if (!isInsert && !rowId) {
        displayNotify(GLOBAL_DATA.TRANSLATIONS.RowIdentifierIsInvalid)
    }
    var url = GLOBAL_DATA.APP_ROOT + "/api/activities/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/punchlist/entry?isInsert=" + isInsert + "&rowId=" + rowId;
    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(punchObj),
        elementsToAddLoading: ["body"],
        removeLoaderIfFail: true,
        successFunc: function (messageData) {
            displayNotify(messageData, "success");
            //$('#table-data-container').html("");
        }
    });
}
function clearFields() {
    $('#pl-item').val("");
    $('#pl-wd').val("");
    $('#pl-category').val("");
    $('#pl-disci').val("");
    $('#pl-actionby').val("");
    $('#pl-orig').val("");
    $('#pl-fdate').val("");
    $('#pl-status').val("");
    $('#pl-closer').val("");
    $('#pl-tdate').val("");
    $('#pl-cdate').val("");
    $('#pl-ref').val("");
    $('#pl-cdoc').val("");
}

function searchOutstanding() {
    $(".modal-content").css("width", "600px");

    var modalTitle = GLOBAL_DATA.TRANSLATIONS.ChooseATagFromRelevantDatatable;
    if (!selectedSystem)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.TheSystemCannotBeNull);
    var url = GLOBAL_DATA.APP_ROOT + "/activities/" + GLOBAL_DATA.PROJECT_ID + "/punchlist/module?system=" + selectedSystem;
    open_modal(url, {
        title: modalTitle,
        elementsToAddLoading: [".modal-content"],
        version: GLOBAL_DATA.MODAL_VERSIONS.ACTIVITIES_SYSTEM_PUNCH_LIST_MANAGEMENT
    });
}

function assignOnChange() {
    var table = $("#pl_datatable").val();
    if (!table)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.TheDatatableCannotBeNull);

    var url = GLOBAL_DATA.APP_ROOT + "/activities/" + GLOBAL_DATA.PROJECT_ID + "/system/modal/table?tableName=" + table;
    callServer(url, {
        elementsToAddLoading: ["#item_table"],
        successFunc: function (viewData) {
            $('#item_table').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTableModule(data.tableName, data.columns);
        },
    });
}

function initializeDataTableModule(table, tableColumns) {
    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + table + '/data';
    var filters = "?advfilters[0].leftoperand=system&advfilters[0].rightoperand=" + selectedSystem;

    console.log(selectedSystem);
    promisDt = new PromisDataTable({
        tableName: table,
        columnList: tableColumns,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: true,
        marginFromElements: 550,
        rowsPerPage: 25,
        hasRowSelect: true,
        isStriped: true,
        fontSize: 12,
        onRowSelect: function (row) {
            
            var id = $(row).children("td:first").text();
            $("#pl-item").val(id);
            close_modal();
        }
    });
    promisDt.initContentForTable();
}

