﻿var promisDt = null;

function initTagsAssignmentStatus() {
    verifyTagsNotAssigned();
}

initTagsAssignmentStatus();

function verifyTagsNotAssigned() {

    $(".tags-not-assigned").click(function () {
        var text = $(this).text();
        if (!text || text == 0)
            return;

        var id = $(this).attr('id');
        console.log(id);
        var code = $(this).attr('code-data');
        viewTagsNotAssigned(code);
    });
}

function viewTagsNotAssigned(code) {
    $(".modal-content").css("width", "760px");
    var modalTitle = GLOBAL_DATA.TRANSLATIONS.TagsNotAssigned;

    var url = GLOBAL_DATA.APP_ROOT + "/certification/" + GLOBAL_DATA.PROJECT_DATABASE + "/progress/tags/notassigned?code=" + code;
    open_modal(url, {
        title: modalTitle,
        elementsToAddLoading: [".modal-content"],
        next: function (viewData) {
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTable("tags_not_assigned", data.columns);
        },
        version: GLOBAL_DATA.MODAL_VERSIONS.CERTIFICATION_MANAGEMENT_TAGS_STATUS
    });

}

function initializeDataTable(dataTable, theColumns) {
   
    if (!dataTable)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.TheDatatableCannotBeNull);

    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + '/data';

    promisDt = new PromisDataTable({
        tableName: dataTable,
        columnList: theColumns,
        hasAjaxDataLoad: false,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        hasActions: false,
        hasImportExport: false,
        marginFromElements: 545,
        fontSize: 14,
        minHeight: 440, 
        rowsPerPage: 100,
        hasScrollCollapse:false
    });
    promisDt.initContentForTable();

}
