﻿
var promisLeftDt = null;
var promisRightDt = null;
// ### REGION INIT
function initActivitieIso() {
    loadtestPackTable();
    $("#save-hydrotest").click(saveHydrotestPackage);
    $("#iso-area").change(loadPipingClasses);
    $("#iso-class").change(loadSystems);
    $("#iso-sys").change(loadSubsystems);
    $("#select-iso").click(loadSketchesTable);
    $("#assign-iso").click(assignIso);
}

initActivitieIso();
// ### END

function loadtestPackTable() {

    var url = GLOBAL_DATA.APP_ROOT + "/activities/" + GLOBAL_DATA.PROJECT_ID + "/iso/testpack-table/partial";
    callServer(url, {
        elementsToAddLoading: ["#tags_table"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $('#tags_table').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTable(data.tableName, data.columns);
        },
    });
}

function initializeDataTable(dataTable, columnsList) {

    console.log("table:" + dataTable);
    if (!dataTable)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.TheDatatableCannotBeNull);

    var url = GLOBAL_DATA.APP_ROOT + '/api/engineering/tables/' + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + '/data';
    var options = {
        tableName: dataTable,
        columnList: columnsList,
        ajaxUrl: url,
        hasActions: false,
        hasRowSelect: true,
        hasImportExport: false,
        hasAdvancedFilter: false,
        hasColumnToggle: false,
        marginFromElements: 300,
        fontSize: 12,
        minHeight: 600,
        rowsPerPage: 25,
        hasScrollCollapse: false,
        onRowSelect:displayAssignedIso
 
    };

    if (isWriter) {
        options.hasAdvancedFilter = true;
        options.onClickAdvancedFilters = deleteHydrotest;
        options.advancedFiltersTitle = GLOBAL_DATA.TRANSLATIONS.DeleteSelected;
    }

    promisLeftDt = new PromisDataTable(options);
    promisLeftDt.initContentForTable();
    $("#advanced-filters-testpack").removeClass("btn btn-sm btn-outline-success").addClass("btn btn-sm btn-primary");
}


function displayAssignedIso() {

    var theSelectedTag = $('tr.selected').find('td:first').text();
    var urlAssignedIso = GLOBAL_DATA.APP_ROOT + "/activities/" + GLOBAL_DATA.PROJECT_ID + "/iso/isometrics/assigned/partial?theSelectedTag=" + theSelectedTag;
    callServer(urlAssignedIso, {
        elementsToAddLoading: [".act_right"],
        successFunc: function (viewData) {
            console.log("viewData", viewData);
            $('#iso_table').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTableForAssignedIsos(data.tableName, data.columns);
        },
    });
    
}
function initializeDataTableForAssignedIsos(dataTable, columnsList) {
    
    console.log("table:" + dataTable);  
    if (!dataTable)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.TheDatatableCannotBeNull);

    var theSelectedTag = $("#promisdt-testpack tr.selected").find('td:first').text();
    var filters = "?distinct=1&advfilters[0].leftoperand=testpack.tag&advfilters[0].rightoperand=" + theSelectedTag+"&join[0].type=inner&join[0].datatable=testpack_link&join[0].leftColumn=maintag&join[0].rightColumn=comp&join[0].operator=eq"
                   + "&join[1].type=inner&join[1].datatable=testpack&join[1].leftColumn=testpack_link.tag&join[1].rightColumn=testpack.tag&join[1].operator=eq";
    //console.log("filters: ", filters);

    var url = GLOBAL_DATA.APP_ROOT + "/api/engineering/tables/" + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + "/data";

    console.log(columnsList);
    promisRightDt = new PromisDataTable({
        tableName: dataTable,
        columnList: columnsList,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasRowSelect: false,
        hasActions: false,
        hasImportExport: false,
        hasColumnToggle: false,
        hasAdvancedFilter: false,
        marginFromElements: 500,
        fontSize: 11,
        minHeight: 600,
        rowsPerPage: 25,
        hasScrollCollapse: false,
        onDataPreprocessing: function (data) {
            console.log(data);
            data.columns = _.slice(data.columns, 1);
            data.order = _.map(data.order, function (obj) {
                return {
                    column: obj.column > 0 ? obj.column - 1 : obj.column,
                    dir: obj.dir
                };
            });
        },
        specialRenderingObject: [
            {
                "aTargets": [0],
                "mData": " ",
                "mRender": function (data, type, full) {
                    //take id for the row
                    return "<label class='custom-control custom-checkbox'><input type='checkbox' name='isometrics-tag' data-tag='" + _.toArray(full)[0] + "' value=" + _.toArray(full)[6].split("-")[1] + " class='custom-control-input' checked>" +
                        "<span class='custom-control-indicator'></span></label>";
                }
            }
        ]
    });
    promisRightDt.initContentForTable();
    //hide the ordering arrows for the first column
    $('#promisdt-sketches_wrapper th:first').attr("title") == " " ? $('#promisdt-sketches_wrapper th:first').replaceWith('<th title=" "></th>') : $('th:first');
}

function deleteHydrotest() {
    $(".modal-content").css("width", "300px");
    var modalTitle = GLOBAL_DATA.TRANSLATIONS.DeleteTestpack;
    open_modal_normal({
        title: modalTitle,
        content: GLOBAL_DATA.TRANSLATIONS.DeleteSelectedTestpackAndItsAssociatedIsosbrbrareYouSure,
        footerButton: GLOBAL_DATA.TRANSLATIONS.Confirm
    });
    $("#run-operation-button").click(deleteSelectedTestPack);
}

function deleteSelectedTestPack() {
   
    var tag = $('tr.selected').find('td:first').text();
    var rowId = $('tr.selected').attr("id").split("-")[1];
    if (!rowId)
        return displayNotify(GLOBAL_DATA.DATa.TRANSLATIONS.CannotDeleteThisRow);

    var url = GLOBAL_DATA.APP_ROOT + "/api/activities/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/testpacks/hydrotest?rowId=" + rowId + "&tag=" + tag;
    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.DELETE,
        contentType: "application/json",       
        successFunc: function () {
            displayNotify(GLOBAL_DATA.TRANSLATIONS.RecordsSuccessfullyDeleted, 'success');
            promisLeftDt.table.row($('#' + rowId)).remove().draw(false);
            close_modal();
        }
    });
}

function saveHydrotestPackage() {

    var tag = $("#package-tag").val();
    var description = $("#package-description").val();
    if (tag == "")
        displayNotify(GLOBAL_DATA.TRANSLATIONS.TestpackCodeIsRequired, "error");

    var url = GLOBAL_DATA.APP_ROOT + "/api/activities/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/hydrotest/package?tag=" + tag + "&description=" + description;
    callServer(url, {
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: "application/json",
        successFunc: function (data) {
            console.log(data);
            if (data === false)
                displayNotify(GLOBAL_DATA.TRANSLATIONS.DuplicatedTestpackCode, 'error');
            else {
                displayNotify(GLOBAL_DATA.TRANSLATIONS.TestpackAddedRememberToAssignIsosToIt, 'success');
                promisLeftDt.reinitDataTable();
            //    promisLeftDt.table.row.add(values).draw(false);
            //    //finds the new row and adds the attribute "id" and a value for it
                $("#package-tag").val('');
                $("#package-description").val('');
            }
                
        }
    });
}

function loadPipingClasses() {

    var pds = $(this).val();
    console.log(pds);
    if (!pds)
        return;

    var url = GLOBAL_DATA.APP_ROOT + "/api/activities/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/iso/pipingClass?pds=" + pds;

    callServer(url, {
        successFunc: function (data) {
            $("#iso-class").html("<option value=''></option>");
            _.forEach(data, function (pipingData) {
                $('#iso-class').append(`<option value='` + pipingData + `'>` + pipingData + `</option>`);
            });
            $("#iso-sys").html('');
            $("#iso-subsys").html('');
            //$("#iso-tbl").hide();
            $('#iso_table').html('');
        }
    })
}

function loadSystems() {
    var pipingClass = $(this).val();
    console.log(pipingClass);
    if (!pipingClass)
        return;

    var url = GLOBAL_DATA.APP_ROOT + "/api/activities/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/iso/systems?pipingClass=" + pipingClass;

    callServer(url, {
        successFunc: function (data) {
            $("#iso-sys").html("<option value=''></option>");
            _.forEach(data, function (system) {
                $('#iso-sys').append(`<option value='` + system.Datatable + `'>` + system.Datatable + " - " + system.Description + `</option>`);
            });
            $("#iso-subsys").html('');
            $('#iso_table').html('');
        }
    })
}

function loadSubsystems() {
    var system = $(this).val();
    console.log(system);
    if (!system)
        return;

    var url = GLOBAL_DATA.APP_ROOT + "/api/activities/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/iso/subsystems?system=" + system;

    callServer(url, {
        successFunc: function (data) {
            $("#iso-subsys").html("<option value=''></option>");
            _.forEach(data, function (subsystem) {
                $('#iso-subsys').append(`<option value='` + subsystem.Datatable + `'>` + subsystem.Datatable + `</option>`);
            });
            $('#iso_table').html('');
        }
    })
}

function loadSketchesTable() {
    if ($("#iso-area").val() == "")
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.YouMustSelectAtLeastOneValueFromPipingDesignList);

    var url = GLOBAL_DATA.APP_ROOT + "/activities/" + GLOBAL_DATA.PROJECT_ID + "/iso/isometrics/partial";
    callServer(url, {
        elementsToAddLoading: [".act_right"],
        removeLoaderIfFail: true,
        successFunc: function (viewData) {
            $('#iso_table').html(viewData);
            var data = PromisDataTableStatics.getTableNameAndColumnsFromView(viewData);
            initializeDataTableForIsometrics(data.tableName, data.columns);
        },
    });
}

function initializeDataTableForIsometrics(dataTable, columnsList) {

    console.log("table:" + dataTable);
    if (!dataTable)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.TheDatatableCannotBeNull);
    
    var filters = getIsometricsFilter();
    var url = GLOBAL_DATA.APP_ROOT + "/api/engineering/tables/" + GLOBAL_DATA.PROJECT_DATABASE + "/" + dataTable + "/data";

    console.log(columnsList);
    promisRightDt = new PromisDataTable({
        tableName: dataTable,
        columnList: columnsList,
        ajaxUrl: url,
        advancedFiltersString: filters,
        hasRowSelect: false,
        hasActions: false,
        hasImportExport: false,
        hasColumnToggle: false,
        hasAdvancedFilter:false,
        marginFromElements: 500,
        fontSize: 11,
        minHeight: 600,
        rowsPerPage: 25,
        hasScrollCollapse: false,
        onDataPreprocessing: function (data) {
            console.log(data);
            data.columns = _.slice(data.columns, 1);
            data.order = _.map(data.order, function (obj) {
                return {
                    column: obj.column > 0 ? obj.column - 1 : obj.column,
                    dir: obj.dir
                };
            });
        },
        specialRenderingObject: [
            {
                "aTargets": [0],
                "mData": " ",
                "mRender": function (data, type, full) {
                    //take id for the row
                    return "<label class='custom-control custom-checkbox'><input type='checkbox' name='isometrics-tag' data-tag='" + _.toArray(full)[0] + "' value=" + _.toArray(full)[6].split("-")[1] + " class='custom-control-input'>" +
                        "<span class='custom-control-indicator'></span></label>";
                }
            }
        ]
    });
    promisRightDt.initContentForTable();
    //hide the ordering arrows for the first column
    $('#promisdt-sketches_wrapper th:first').attr("title") == " " ? $('#promisdt-sketches_wrapper th:first').replaceWith('<th title=" "></th>') : $('th:first');
}

function getIsometricsFilter() {

    var andsql = "";
    var pipingDesign = $("#iso-area").val();
    var geographical = $("#geoarea").val();
    var pipingClas = $("#iso-class").val();
    var system = $("#iso-sys").val();
    var subSystem = $("#iso-subsys").val();

    var i = 0;
    var filters = "?advfilters[" + i + "].leftoperand=id&advfilters[" + i + "].rightoperand=0&advfilters[" + i + "].operator=>";

    if (pipingDesign) {
        i = i + 1;
        andsql = andsql + "&advfilters[" + i + "].leftoperand=pds&advfilters[" + i + "].rightoperand=" + pipingDesign + "&advfilters[" + i + "].operator=eq";
    }
    if (geographical) {
        i = i + 1;
        andsql += "&advfilters[" + i + "].leftoperand=wbs&advfilters[" + i + "].rightoperand=" + geographical + "&advfilters[" + i + "].operator=eq";
    }
                                                                                                                           
    if (pipingClas) {
        i = i + 1;
        andsql += "&advfilters[" + i + "].leftoperand=piping_class&advfilters[" + i + "].rightoperand=" + pipingClas + "&advfilters[" + i + "].operator=eq";
    }
    if (system) {
        i = i + 1;
        andsql += "&advfilters[" + i + "].leftoperand=system&advfilters[" + i + "].rightoperand=" + system + "&advfilters[" + i + "].operator=eq";
    }
    if (subSystem) {
        i = i + 1
        andsql += "&advfilters[" + i + "].leftoperand=subsystem&advfilters[" + i + "].rightoperand=" + subSystem + "&advfilters[" + i + "].operator=eq";
    }
    if (andsql == "")
        return filters;

    filters += andsql;
    console.log(filters);
    return filters;
}

function assignIso() {

    var hasSelectedRow = $("#promisdt-testpack tbody").find(" tr.selected").length > 0 ? true : false;
    console.log(hasSelectedRow);
    if (!hasSelectedRow)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.InOrderToSaveAtLeastOneRowFromTheLeftTableMustBeSelected);

    //search row selected in testpack table
    var searchedRow = $("#promisdt-testpack tbody").find(" tr.selected");
    //get the values for needed columns from the selected row
    var tagTestPack = searchedRow.find("td:first").text();
    //verify if the piping design list has selected item
    if ($("#iso-area").val() == "")
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.YouMustSelectAtLeastOneValueFromPipingDesignList);

    //verify if iso Table is loaded
    var tableLoaded = $('#iso_table').html() == "" ? false : true;
    if (tableLoaded == false)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.YouMustClickOnSelectIsosButtonInOrderToShowSketchesTable);

    var isom = $("#promisdt-sketches tr:has(input:checked)");
    var totalCheckedIso = $("#promisdt-sketches tr:has(input:checked)").length;
    //verify if iso tags are selected
    if (totalCheckedIso == 0)
        return displayNotify(GLOBAL_DATA.TRANSLATIONS.InOrderToSaveAtLeastOneTagFromTheRightTableMustBeChecked);

    var isometrics = [];   
    console.log("Total checked ", totalCheckedIso);
    for (i = 0; i < isom.length;i++)
    {
        var isoTag = $(isom[i]).find('td:nth-child(2)').text();
        var wbs = $(isom[i]).find('td:nth-child(3)').text();
        console.log(isoTag);
        var sys = $(isom[i]).find('td:nth-child(6)').text();
        var subsys = $(isom[i]).find('td:nth-child(7)').text();
        var isometric = {};
        isometric["IsoTag"] = isoTag;
        isometric["Wbs"] = wbs;
        isometric["System"] = sys;
        isometric["Subsystem"] = subsys;
        isometrics.push(isometric);
    }
    console.log("Isom", isometrics);
   
    

    //add obtained data into the formData
    var formData = {};
    formData.TestpackTag = tagTestPack;
    formData.Iso = isometrics;

    var url = GLOBAL_DATA.APP_ROOT + "/api/activities/data/" + GLOBAL_DATA.PROJECT_DATABASE + "/isometrics/assignment";
    callServer(url, {
        elementsToAddLoading: [".act_right"],
        callType: GLOBAL_DATA.CALL_TYPES.PUT,
        contentType: GLOBAL_DATA.CONTENT_TYPES.JSON,
        data: JSON.stringify(formData),
        successFunc: function (hasErrors) {
            //if (hasErrors)
            //    return displayNotify(GLOBAL_DATA.TRANSLATIONS.OneOrMoreRecordsHadNotBeenInsertedBecauseAreDuplicated);

            displayNotify(GLOBAL_DATA.TRANSLATIONS.IsometricsAssignmentWasSavedWithSuccess, "success");
            promisLeftDt.reinitDataTable();
            //$("input[type='checkbox']").prop("checked", false);
        },
        removeLoaderIfFail: true
    });
}

