﻿$(document).ready(function () {

    var urlCivil = GLOBAL_DATA.APP_ROOT + "/api/engineering/summary/civil";
    var urlEngineering = GLOBAL_DATA.APP_ROOT + "/api/engineering/summary/engineering";

    callServer(urlCivil, {
        successFunc: function (data) {

            var rows = "";

            for (var i = 0; i < data.length; i++) {
                var description = data[i].TableDescription;
                var totalCount = data[i].TotalCount;
                var quantity = data[i].Quantities;
                var certifications = data[i].Certification;
                var system = data[i].System;

                var row = `<tr class="table-text-size">
                            <td>`+ description + `</td>
                            <td class="right">` + totalCount + `</td>
                            <td class="right">` + quantity + `</td>
                            <td class="right">` + certifications + `</td>
                            <td class="right">` + system + `</td>
                        </tr>`;
                rows = rows + row;
            }

            $('#tbl-civil').append(rows);
        },
        elementsToAddLoading: ['#civil-data-container']
    });


    callServer(urlEngineering, {
        successFunc: function (data) {

            var rows = "";

            for (var i = 0; i < data.length; i++) {
                var description = data[i].TableDescription;
                var totalCount = data[i].TotalCount;
                var quantity = data[i].Quantities;
                var certifications = data[i].Certification;
                var system = data[i].System;

                var row = `<tr class="table-text-size">
                            <td>`+ description + `</td>
                            <td class="right">` + totalCount + `</td>
                            <td class="right">` + quantity + `</td>
                            <td class="right">` + certifications + `</td>
                            <td class="right">` + system + `</td>
                        </tr>`;
                rows = rows + row;
            }

            $('#tbl-engineering').append(rows);

        },
        elementsToAddLoading: ['#engineering-data-container']
    });

});