/*!
 * Adminux (http://maxartkiller.com)
 * Copyright 2017 The Adminux Author: Maxartkiller
 * purchase licence before use
 * You can not resale and/or modify without prior licences.
*/

"use strict";

$(document).ready(function () {
    /*LAYOUT CONTENT SCRIPT*/
    var body_1 = $('body');
    /* menu  collapsible */
    $(".menu-collapse").click(function () {
        body_1.toggleClass("menuclose");
        setTimeout(function () {
            body_1.trigger("menuclose-changed");
        }, 410);
    });

    if (body_1.hasClass("scroll_header") === true) {
        $(window).on("scroll", function () {
            if ($(document).scrollTop() >= 250) {
                body_1.addClass("active_scroll");
            } else {
                body_1.removeClass("active_scroll");
            }
        });
    }

    //$(window).on("load", function () {
    $("#main_overlay").click(function () {
        $("#main_overlay").hide();
    });

    var h = $(window).innerHeight() - 96;
    setTimeout(function () {
        var url = window.location;
        function menuitems() {
            if (body_1.hasClass('horizontal-menu') === false) {
                var element = $('.sidebar-left #side-menu li a').filter(function () {
                    return this.href == url;
                }).addClass('active').parent("li").addClass('active').closest('.nav').addClass('in').slideDown().prev().addClass('show');

                $('.sidebar-left .nav li a').on('click', function () {
                    if ($(this).hasClass('menudropdown') === true) {
                        $(this).toggleClass("show").next().slideToggle().parent().addClass("in");
                    }
                });
            }
        }
        menuitems();

        /*Full screen result container show*/
        $('#search_header').on('focus', function () {
            $(".search-block").show();
            body_1.addClass('searchshow'); $('#search_header').focusout();

        });

        /* Search window screen fullscreen open */
        $('.search-block .close-btn').on('click', function () {
            $(".search-block").slideUp();
            body_1.removeClass('searchshow');
        });

        /* on keypress hide search block which was in fullscreen */
        $(document).keyup(function (e) {
            if (e.keyCode === 27) { // escape key maps to keycode `27`
                $(".search-block").fadeOut();
                body_1.removeClass('searchshow');
                $('#search_header').focusout();
            }
        });

        /* inbox mail page  collapsible */
        $(".inboxmenu").on("click", function () {
            $(".mailboxnav ").toggleClass("mailboxnavopen");
        });
        $(".filemenu_btn").on("click", function () {
            $(".filemenu ").toggleClass("filemenuopen");
        });

        /* menu  collapsible */
        $(".menu-collapse-right").click(function () {
            body_1.toggleClass("menuclose-right");
        });
        $(".menu-small").click(function () {
            body_1.toggleClass("menusmall");
        });

    }, 500);

    //});

    /* Custome css checkbox script */
    $('.form-check-input').on('change', function () {
        $(this).parent().toggleClass("active");
        $(this).closest(".media").toggleClass("active");
    });
    
    /* Resposnsive Utility hide menu */
    if ($(window).width() >= 1440) { body_1.removeClass('menuclose'); } else { body_1.addClass('menuclose'); }
});

function rgb2hex(rgb) {
    if (rgb.search("rgb") == -1) {
        return rgb;
    } else {
        rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
        function hex(x) {
            return ("0" + parseInt(x).toString(16)).slice(-2);
        }
        return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }
}

function encodeImageFileAsURL(fileId, elementToPut, next) {

    var filesSelected = document.getElementById(fileId).files;

    if ($("#" + fileId).length > 0) {
        var fileToLoad = filesSelected[0];

        var fileReader = new FileReader();
        var fileLoaded = false;
        var srcData = null;
        fileReader.onload = function (fileLoadedEvent) {
            srcData = fileLoadedEvent.target.result;
            $("#" + elementToPut).attr("src", srcData);
            if (_.isFunction(next))
                next();
        }
        fileReader.readAsDataURL(fileToLoad);
    }
}
