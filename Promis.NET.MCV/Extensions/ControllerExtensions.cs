﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Promis.NET.DataAccess.Entities;
using System.Web;
using System.Web.Mvc;

namespace Promis.NET.MCV.Extensions
{
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params string[] roles) : base()
        {
            Roles = string.Join(",", roles);
        }
    }

    public static class ControllerExtensions
    {
        public static ApplicationUser GetApplicationUser(this Controller ctrl)
        {
            var user = ctrl.User;
            string id = null;
            if (user != null && user.Identity.IsAuthenticated)
                id = user.Identity.GetUserId();
            if (id != null)
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(id);

            return null;
        }
    }
}