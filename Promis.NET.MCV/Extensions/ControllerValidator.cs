﻿using Promis.NET.Localization;
using Promis.NET.Roles;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Principal;

namespace Promis.NET.MCV.Extensions
{
    public class ControllerValidator
    {
        public List<string> Errors { get; set; } = new List<string>();

        public ControllerValidator()
        {

        }

        /// <summary>
        /// Validates the fact that the current user is either admin or has read rights on any module.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="customMessage"></param>
        /// <returns></returns>
        public ControllerValidator ValidateUserToBeInAnyReadModuleRole(IPrincipal user, string customMessage = null)
        {
            var message = !string.IsNullOrWhiteSpace(customMessage) ? customMessage
                            : LocalResources.Unauthorized;

            if (user == null)
            {
                Errors.Add(message);
                return this;
            }

            var allReadRoles = LocalRoles.GetAllModuleRoles(getOnlyRead: true);
            foreach (var role in allReadRoles)
            {
                if (user.IsInRole(role))
                    return this;
            }

            // not good
            Errors.Add(message);
            return this;
        }

        /// <summary>
        /// Validates the fact that the current user is either admin or has write rights on any module.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="customMessage"></param>
        /// <returns></returns>
        public ControllerValidator ValidateUserToBeInAnyWriteModuleRole(IPrincipal user, string customMessage = null)
        {
            var message = !string.IsNullOrWhiteSpace(customMessage) ? customMessage
                            : LocalResources.Unauthorized;

            if (user == null)
            {
                Errors.Add(message);
                return this;
            }

            var allWriteModules = LocalRoles.GetAllModuleRoles(getOnlyWrite: true);
            foreach (var role in allWriteModules)
            {
                if (user.IsInRole(role))
                    return this;
            }

            // not good
            Errors.Add(message);
            return this;
        }

        /// <summary>
        /// Validate an object for not being null.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="valueName">The name to display in default error message. Can be ignored, by default is property name.</param>
        /// <param name="customMessage">Set a custom message to display if the default does not suit you.</param>
        /// <returns></returns>
        public ControllerValidator ValidateObject(object value, string valueName = null, string customMessage = null)
        {
            var message = !string.IsNullOrWhiteSpace(customMessage) ? customMessage
                            : string.Format("{0} {1}.", valueName ?? "Value", LocalResources.MustBeSpecified);

            if (value == null)
                Errors.Add(message);

            return this;
        }


        /// <summary>
        /// Validate a string checking if it is a valid double.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="valueName">The name to display in default error message. Can be ignored, by default is property name.</param>
        /// <param name="customMessage">Set a custom message to display if the default does not suit you.</param>
        /// <returns></returns>
        public ControllerValidator ValidateDouble(string value, string valueName = null, CultureInfo culture = null, string customMessage = null)
        {
            culture = culture ?? CultureInfo.CurrentCulture;
            var message = !string.IsNullOrWhiteSpace(customMessage) ? customMessage
                            : string.Format("{0} {1}.", valueName ?? "Value", LocalResources.MustBeAValidDoubleNumber);
            
            if (string.IsNullOrWhiteSpace(value) || !double.TryParse(value, NumberStyles.Float, culture, out double result))
                Errors.Add(message);

            return this;
        }

        /// <summary>
        /// Validates strings. Cannot be null, empty of whitespace.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="valueName">The name to display in default error message. Can be ignored, by default is property name.</param>
        /// <param name="customMessage">Set a custom message to display if the default does not suit you.</param>
        /// <returns></returns>
        public ControllerValidator ValidateString(string value, string valueName = null, string customMessage = null)
        {
            var message = !string.IsNullOrWhiteSpace(customMessage) ? customMessage
                            : string.Format("{0} {1}.", valueName ?? "Value", LocalResources.MustBeSpecified);

            if (string.IsNullOrWhiteSpace(value))
                Errors.Add(message);

            return this;
        }

        /// <summary>
        /// Validates strings with default validation + checks the string to be in a range of values.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="valueName">The name to display in default error message. Can be ignored, by default is property name.</param>
        /// <param name="customMessage">Set a custom message to display if the default does not suit you.</param>
        /// <returns></returns>
        public ControllerValidator ValidateString(string value, IEnumerable<string> range, bool ignoreCase = true, string valueName = null, string customMessage = null)
        {
            var errors = ValidateString(value, valueName, customMessage);
            if (errors.Errors.Count > 0)
                return errors;

            if (ignoreCase && value != null)
                value = value.ToUpper();
            if (ignoreCase && range != null)
                range = range.Select(r => r.ToUpper());

            var message = !string.IsNullOrWhiteSpace(customMessage) ? customMessage
                            : string.Format("{0} {2} ({1})", valueName ?? "Value", string.Join(", ", range), LocalResources.MustBeInRange);

            if (range != null && !range.Contains(value))
                Errors.Add(message);

            return this;
        }

        /// <summary>
        /// Validates an Array (IEnumerable). Cannot be null or empty.
        /// </summary>
        /// <typeparam name="T">The type.</typeparam>
        /// <param name="array">The array to validate</param>
        /// <param name="arrayName">The name to display in default error message.</param>
        /// <param name="customMessage">Set a custom message to display if the default does not suit you.</param>
        /// <returns></returns>
        public ControllerValidator ValidateArray<T>(IEnumerable<T> array, string arrayName, string customMessage = null)
        {
            var message = !string.IsNullOrWhiteSpace(customMessage) ? customMessage
                            : string.Format("{0} {1}.", arrayName ?? "Array", LocalResources.CannotBeNullOrEmpty);

            if (array == null || array.Count() == 0)
                Errors.Add(message);

            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identity">The Identity Provider, to get User Cookies (User.Identity) to get Project ID or Project Database.</param>
        /// <param name="identifier">The project ID or the Project Database Name.</param>
        /// <param name="identifierType">"id" or "db". Represents what the parameter is, a project ID or the name of the database.</param>
        /// <param name="messageMode">"get" or "edit". Represents what default message the output will have, the one for trying to get data or the one for trying to edit data.</param>
        /// <param name="customMessage">Set a custom message to display if the default does not suit you.</param>
        /// <returns></returns>
        public ControllerValidator ValidateProject(IIdentity identity, string identifier, string identifierType, string messageMode, string customMessage = null)
        {
            var validValue = identifierType == "id" 
                                ? identity.GetProjectId()
                                : identity.GetProjectDatabaseName();
            var outputMessage = messageMode == "edit"
                                ? LocalResources.YouCanOnlyEditYourProjectDetailsNotTheDetailsOfAnotherProject
                                : LocalResources.YouCanOnlyGetDataForYourProjectNotForAnotherProject;
            var message = !string.IsNullOrWhiteSpace(customMessage) ? customMessage
                            : outputMessage;

            if (identifier != validValue)
                Errors.Add(message);

            return this;
        }

        /// <summary>
        /// Validates strings. Cannot be null, empty of whitespace.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="valueName">The name to display in default error message. Can be ignored, by default is property name.</param>
        /// <param name="customMessage">Set a custom message to display if the default does not suit you.</param>
        /// <returns></returns>
        public ControllerValidator ValidateDate(string value, out DateTime date, string valueName = null, string customMessage = null)
        {
            var message = !string.IsNullOrWhiteSpace(customMessage) ? customMessage
                            : string.Format("{0} {1}.", valueName ?? "Value", LocalResources.MustBeSpecified);

            date = new DateTime();
            if (string.IsNullOrWhiteSpace(value) || !DateTime.TryParse(value, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                Errors.Add(message);

            return this;
        }

        /// <summary>
        /// Returns the list of all errors found.
        /// </summary>
        /// <returns></returns>
        public List<string> GetErrors()
        {
            return Errors;
        }
    }
}