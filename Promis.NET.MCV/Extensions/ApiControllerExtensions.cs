﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Promis.NET.DataAccess.Entities;
using System;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Promis.NET.MCV.Extensions
{
    public static class ApiControllerExtensions
    {
        public static ApplicationUser GetApplicationUser(this ApiController ctrl)
        {
            var user = ctrl.User;
            string id = null;
            if (user != null && user.Identity.IsAuthenticated)
                id = user.Identity.GetUserId();
            if (id != null)
                return ctrl.Request.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(id);

            return null;
        }

        public static void NotifyControllerSync(this ApiController ctrl, string url)
        {
            //call controller to remove cache
            var client = new HttpClient();
            var appRoot = ctrl.Request.RequestUri.GetLeftPart(UriPartial.Authority);
            appRoot += url;
            var res = client.PostAsync(appRoot, new StringContent("")).Result;
        }
    }
}