﻿using Microsoft.AspNet.Identity;
using Promis.NET.DataAccess.Entities;
using Promis.NET.Utilities.Models;
using System;
using System.Configuration;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;

namespace Promis.NET.MCV.Extensions
{
    public static class IdentityExtensions
    {
        public static string GetProjectId(this IIdentity identity)
        {
            return identity.GetClaimValue("ProjectId");
        }

        public static async Task<ClaimsIdentity> CreateAuthenticationCookie(this ApplicationUser user, UserManager<ApplicationUser> userManager)
        {
            var identity = await userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            identity.AddClaim(new Claim("ProjectId", string.IsNullOrWhiteSpace(user.ProjectId) ? (ConfigurationManager.AppSettings["DefaultProject"] ?? "pw_0001") : user.ProjectId));//set projId stored in claims
            identity.AddClaim(new Claim("UserTheme", string.IsNullOrWhiteSpace(user.UserTheme) ? Global.DefaultTheme : user.UserTheme));//set projId stored in claims
            identity.AddClaim(new Claim("SecurityTimestamp", DateTime.Now.ToLongTimeString()));
            identity.AddClaim(new Claim("Language", string.IsNullOrWhiteSpace(user.Language) ? Global.DefaultLanguage : user.Language));
            return identity;
        }

        public static string GetProjectDatabaseName(this IIdentity identity)
        {
            var projectId = identity.GetClaimValue("ProjectId");
            return projectId != null ? "pw_" + projectId : null;
        }

        public static string GetUserTheme(this IIdentity identity)
        {
            var valClaims = identity.GetClaimValue("UserTheme");
            return valClaims ?? Global.DefaultTheme;
        }

        public static void SetUserTheme(this IPrincipal currentPrincipal, string value)
        {
            currentPrincipal.SetClaimValue("UserTheme", value);
        }

        public static string GetUserLanguage(this IIdentity identity)
        {
            var valClaims = identity.GetClaimValue("Language");
            return valClaims ?? Global.DefaultLanguage;
        }

        public static void SetUserLanguage(this IPrincipal currentPrincipal, string value)
        {
            currentPrincipal.SetClaimValue("Language", value);
        }

        public static string GetClaimValue(this IIdentity identity, string key)
        {
            if (identity == null)
                return null;

            var claimData = (identity as ClaimsIdentity).FindFirst(key);
            return claimData?.Value;
        }
        
        public static void SetClaimValue(this IPrincipal currentPrincipal, string key, string value)
        {
            var identity = currentPrincipal.Identity as ClaimsIdentity;
            if (identity == null)
                return;

            var claimData = (identity as ClaimsIdentity).FindFirst(key);
            if (claimData != null)
                identity.RemoveClaim(claimData);

            identity.AddClaim(new Claim(key, value));
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.AuthenticationResponseGrant = new Microsoft.Owin.Security.AuthenticationResponseGrant(new ClaimsPrincipal(identity), new Microsoft.Owin.Security.AuthenticationProperties() { IsPersistent = true });
        }

        public static void SetClaimValue(this IIdentity iidentity, string key, string value)
        {
            var identity = iidentity as ClaimsIdentity;
            if (identity == null)
                return;

            var claimData = (identity as ClaimsIdentity).FindFirst(key);
            if (claimData != null)
                identity.RemoveClaim(claimData);

            identity.AddClaim(new Claim(key, value));
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.AuthenticationResponseGrant = new Microsoft.Owin.Security.AuthenticationResponseGrant(new ClaimsPrincipal(identity), new Microsoft.Owin.Security.AuthenticationProperties() { IsPersistent = true });
        }
    }
}