﻿using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.MCV.Models.Engineering
{
    public class DataTableResponseData : BaseModel, IBaseModel
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public object[] data { get; set; }
    }
}
