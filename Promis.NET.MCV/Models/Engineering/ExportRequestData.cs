﻿using System.Collections.Generic;
using System.Web.Http.ModelBinding;

namespace Promis.NET.MCV.Models.Engineering
{
    [ModelBinder(typeof(ExportModelBinder))]
    public class ExportRequestData: DataTableRequestData
    {
        public bool IsFullExport { get; set; } = false;
        public List<string> ColumnNames { get; set; } = new List<string>();

        public ExportRequestData(DataTableRequestData data): base(data)
        {
        }
    }
}
