﻿using System;
using System.Collections.Generic;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;

namespace Promis.NET.MCV.Models.Engineering
{
    public class ExportModelBinder : DataTableModelBinder, IModelBinder
    {
        public new bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            base.BindModel(actionContext, bindingContext);

            if (bindingContext.ModelType != typeof(ExportRequestData))
            {
                return false;
            }

            var modelUp = (DataTableRequestData)bindingContext.Model ?? new DataTableRequestData();
            var model = new ExportRequestData(modelUp);

            var isFullExport = GetValue(bindingContext, "isFullExport");
            if (isFullExport != null)
                model.IsFullExport = Convert.ToBoolean(isFullExport);
            
            var d = 0;
            model.ColumnNames = new List<string>();
            while (GetValue(bindingContext, "columns[" + d + "]") != null)
            {
                var col = GetValue(bindingContext, "columns[" + d + "]");
                if(!string.IsNullOrWhiteSpace(col))
                    model.ColumnNames.Add(col);
                d++;
            }
            
            bindingContext.Model = model;
            return true;
        }
    }
}
