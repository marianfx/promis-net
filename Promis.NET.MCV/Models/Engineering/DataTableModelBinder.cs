﻿using Promis.NET.DataAccess.Models;
using Promis.NET.Services.Models.Engineering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;

namespace Promis.NET.MCV.Models.Engineering
{
    public class DataTableModelBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType != typeof(DataTableRequestData) && bindingContext.ModelType.BaseType != typeof(DataTableRequestData))
            {
                return false;
            }

            var model = (DataTableRequestData)bindingContext.Model ?? new DataTableRequestData();

            var dr = GetValue(bindingContext, "draw");
            var st = GetValue(bindingContext, "start");
            var len = GetValue(bindingContext, "length");
            var dist = GetValue(bindingContext, "distinct");
            if(!string.IsNullOrWhiteSpace(dr))
                model.Draw = Convert.ToInt32(dr);
            if(!string.IsNullOrWhiteSpace(st))
                model.Start = Convert.ToInt32(st);
            if(!string.IsNullOrWhiteSpace(len))
                model.Length = Convert.ToInt32(len);
            if (!string.IsNullOrEmpty(dist))
                model.Distinct = (int.TryParse(dist, out int nuconteaza) ? int.Parse(dist) == 1 ? true : false : false);

            // columns
            var p = 0;
            var cols = new List<DataTableColumn>();
            var colName = GetValue(bindingContext, "columns[" + p + "].data");
            while (colName != null)
            {
                var dtColumn = new DataTableColumn();
                var data = GetValue(bindingContext, "columns[" + p + "].name");

                dtColumn.Name = colName;
                if (!string.IsNullOrWhiteSpace(data))
                    dtColumn.Data = data;

                cols.Add(dtColumn);
                p++;
                colName = GetValue(bindingContext, "columns[" + p + "].data");
            }
            model.Columns = cols.ToArray();


            // Search
            var searchVal = GetValue(bindingContext, "search.value");
            var searchRegex = GetValue(bindingContext, "search.regex");
            model.Search = new DataTableSearch();
            if (!string.IsNullOrWhiteSpace(searchVal))
                model.Search.Value = HttpUtility.UrlDecode(searchVal);
            if (!string.IsNullOrWhiteSpace(searchRegex))
                model.Search.Regex = Convert.ToBoolean(searchRegex);


            // Order
            var o = 0;
            var order = new List<DataTableOrder>();
            var colIndex = GetValue(bindingContext, "order[" + o + "].column");
            while (colIndex != null)
            {
                var dtOrder = new DataTableOrder();
                var dir = GetValue(bindingContext, "order[" + o + "].dir");
                var adv = GetValue(bindingContext, "order[" + o + "].func");
                if(!string.IsNullOrWhiteSpace(colIndex))
                    dtOrder.Column = int.Parse(colIndex);
                if (!string.IsNullOrWhiteSpace(dir))
                    dtOrder.Dir = dir;
                if (!string.IsNullOrWhiteSpace(adv))
                    dtOrder.AdvancedFunction = HttpUtility.UrlDecode(adv);

                order.Add(dtOrder);
                o++;
                colIndex = GetValue(bindingContext, "order[" + o + "].column");
            }
            model.Order = order.ToArray();

            // Advanced filters (conditions)
            var c = 0;
            model.AdvFilters = new List<WhereCondition>();
            while (GetValue(bindingContext, "advfilters[" + c + "].leftoperand") != null)
            {
                var condition = new WhereCondition();
                var left = GetValue(bindingContext, "advfilters[" + c + "].leftoperand");
                var right = GetValue(bindingContext, "advfilters[" + c + "].rightoperand");
                var op = GetValue(bindingContext, "advfilters[" + c + "].operator");
                if (!string.IsNullOrWhiteSpace(left))
                    condition.LeftOperand = left;
                if (!string.IsNullOrWhiteSpace(right))
                    condition.RightOperand = double.TryParse(right, out double outputNumber) ? (object)outputNumber : right;
                if (!string.IsNullOrWhiteSpace(op))
                    condition.Operator = op;

                model.AdvFilters.Add(condition);
                c++;
            }

            //for join          
            var d = 0;
            model.Join = new List<JoinCondition>();
            while (GetValue(bindingContext, "join[" + d + "].datatable") != null)
            {
                var joinCond = new JoinCondition();
                joinCond.DataTable = GetValue(bindingContext, "join[" + d + "].datatable");
                var leftOp = GetValue(bindingContext, "join[" + d + "].leftColumn");
                var rightOp = GetValue(bindingContext, "join[" + d + "].rightColumn");
                var op = GetValue(bindingContext, "join[" + d + "].operator");
                var type = GetValue(bindingContext, "join[" + d + "].type");
                if (!string.IsNullOrWhiteSpace(leftOp))
                    joinCond.LeftColumn = leftOp;
                if (!string.IsNullOrWhiteSpace(rightOp))
                    joinCond.RightColumn = rightOp;
                if (!string.IsNullOrWhiteSpace(op))
                    joinCond.Operator = op;
                if (!string.IsNullOrWhiteSpace(type) && new string[] { "inner", "left", "right", "outer", "left outer", "right outer" }.Contains(type))
                    joinCond.JoinType = type;

                model.Join.Add(joinCond);
                d++;
            }
            //end join
            
            //for group 
            var q = 0;
            model.GroupByColumns = new List<string>();
            var gbcolName = GetValue(bindingContext, "group[" + q + "]");
            while (!string.IsNullOrWhiteSpace(gbcolName))
            {
                model.GroupByColumns.Add(gbcolName);
                q++;
                gbcolName = GetValue(bindingContext, "group[" + q + "]");
            }
            
            bindingContext.Model = model;
            return true;
        }

        protected string GetValue(ModelBindingContext context, string key)
        {
            var result = context.ValueProvider.GetValue(key);
            return result?.AttemptedValue;
        }
    }
}
