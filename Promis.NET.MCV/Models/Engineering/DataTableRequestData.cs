﻿using Promis.NET.DataAccess.Models;
using Promis.NET.Services.Models.Engineering;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.ModelBinding;

namespace Promis.NET.MCV.Models.Engineering
{
    [ModelBinder(typeof(DataTableModelBinder))]
    public class DataTableRequestData
    {
        public int Start { get; set; } = 0;
        public int Length { get; set; } = 0;
        public int Draw { get; set; } = 0;
        public bool Distinct { get; set; } = false;

        public DataTableOrder[] Order { get; set; } = new DataTableOrder[0];
        public DataTableColumn[] Columns { get; set; } = new DataTableColumn[0];
        public DataTableSearch Search { get; set; } = new DataTableSearch();
        public List<WhereCondition> AdvFilters { get; set; } = new List<WhereCondition>();
        public List<JoinCondition> Join { get; set; } = new List<JoinCondition>();
        public List<string> GroupByColumns { get; set; } = new List<string>();

        public DataTableRequestData(int start, int length, int draw, bool distinct, IEnumerable<DataTableOrder> order, IEnumerable<DataTableColumn> columns, DataTableSearch search, IEnumerable<WhereCondition> conditions, IEnumerable<JoinCondition> joins, IEnumerable<string> groups)
        {
            Start = start;
            Length = length;
            Draw = draw;
            Distinct = distinct;
            if(order != null)
                Order = order.ToArray();
            if(columns != null)
                Columns = columns.ToArray();
            if(search != null)
                Search = search;
            if(conditions != null)
                AdvFilters = conditions.ToList();
            if(joins != null)
                Join = joins.ToList();
            if (groups != null)
                GroupByColumns = groups.ToList();
        }

        public DataTableRequestData(DataTableRequestData data): this(data.Start, data.Length, data.Draw, data.Distinct, data.Order, data.Columns, data.Search, data.AdvFilters, data.Join, data.GroupByColumns)
        {
        }

        public DataTableRequestData()
        {

        }

    }
}
