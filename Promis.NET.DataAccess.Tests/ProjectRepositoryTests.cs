﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using Promis.NET.DataAccess.DbContexts;
using Promis.NET.DataAccess.Repositories;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.Web.DataAccess.Entities.PromisWeb;
using System.Linq;
using Promis.NET.Utilities.Models;
using System;

namespace Promis.NET.DataAccess.Tests
{
    [TestClass]
    public class ProjectRepositoryTests
    {
        #region Test Settings
        IProjectRepository _projectRepository;

        [TestInitialize]
        public void SetUp()
        {
            _projectRepository = new ProjectRepository(new ProjectDbContext("pw_0001", true));
        }

        [TestCleanup]
        public void TearDown()
        {
            _projectRepository = null;
        }
        #endregion

        [TestMethod]
        public void When_GettingActivitiesWithPlanningForSS_Then_20Rows_Should_BeReturned()
        {
            var result = _projectRepository.GetAssociatedActivitiesWithMainData("SS");
            result.Count.Should().Be(20);
        }

        [TestMethod]
        public void When_ExecutingSPGettingProgressValuesForSP130_Then_9Rows_Should_BeReturned()
        {
            var result = _projectRepository.GetActivityProgressArray("SP130", "P");
            result.Count.Should().Be(9);
        }

        [TestMethod]
        public void When_ExecutingActivitiesUpdateForSP_Then_All_Should_BeSaved()
        {
            _projectRepository = new ProjectRepository(new PromisWebDbContext(true));
            var project = _projectRepository.GetById<project, string>("proj", "0001");

            _projectRepository = new ProjectRepository(new ProjectDbContext("pw_0001", true));
            var activity = _projectRepository.GetAssociatedActivitiesWithMainData("SP")[0];
            var newStartEnd = Global.YearFromString(project.s_date).ToString() + "-" + Global.MonthFromString(project.s_date).ToString().PadLeft(2, '0');
            activity.start_date = newStartEnd;
            activity.end_date = newStartEnd;
            var progressArray = new double[] { 100 }.ToList();

            _projectRepository.UpdateActivityPlanningData(project, activity, progressArray);
            _projectRepository.FinishTransaction();

            var activityDb = _projectRepository.GetAssociatedActivitiesWithMainData("SP")[0];
            activityDb.Should().NotBeNull();
            activityDb.start_date.Should().Be(newStartEnd);
            activityDb.end_date.Should().Be(newStartEnd);
            progressArray = _projectRepository.GetActivityProgressArray(activityDb.act, activityDb.type);
            progressArray.Should().HaveCount(1);
            progressArray[0].Should().Be(100);
        }


        [TestMethod]
        public void When_RunningPrerequisitesForUpdate_Then_All_Should_BeSaved()
        {
            _projectRepository = new ProjectRepository(new PromisWebDbContext(true));
            var oldProject = _projectRepository.GetById<project, string>("proj", "0001");

            var newProject = new project() { s_date = oldProject.s_date, e_date = oldProject.e_date };


            // work on project db
            _projectRepository = new ProjectRepository(new ProjectDbContext("pw_0001", true));

            // should stop and do nothing
            _projectRepository.RunProjectUpdatePrerequisites(newProject, oldProject, "logo");

            // changing start date lower => should be ok
            newProject.s_date = "2015-09";
            _projectRepository.RunProjectUpdatePrerequisites(newProject, oldProject, "general");

            // trying sending invalid start-end => exception
            newProject.s_date = "2018-06";
            Action a = () =>
            {
                _projectRepository.RunProjectUpdatePrerequisites(newProject, oldProject, "general");
            };
            a.ShouldThrow<Exception>();


            // trying dropping columns at the start with values => exception
            newProject.s_date = "2016-01";
            Action b = () =>
            {
                _projectRepository.RunProjectUpdatePrerequisites(newProject, oldProject, "general");
            };
            b.ShouldThrow<Exception>();

            // try dropping ok columns at the start
            newProject.s_date = "2015-12";
            _projectRepository.RunProjectUpdatePrerequisites(newProject, oldProject, "general");

            // changing end-date higher => should be ok
            newProject.e_date = "2018-08";
            _projectRepository.RunProjectUpdatePrerequisites(newProject, oldProject, "general");
            
            // try dropping failing months
            newProject.e_date = "2018-03";
            Action c = () =>
            {
                _projectRepository.RunProjectUpdatePrerequisites(newProject, oldProject, "general");
            };
            c.ShouldThrow<Exception>();
            
            // try dropping first one with full 100%
            newProject.e_date = "2018-04";
            Action d = () =>
            {
                _projectRepository.RunProjectUpdatePrerequisites(newProject, oldProject, "general");
            };
            d.ShouldThrow<Exception>();

            // try dropping OK months and returns back to normal
            newProject.e_date = "2018-05";
            _projectRepository.RunProjectUpdatePrerequisites(newProject, oldProject, "general");
        }
    }
}
