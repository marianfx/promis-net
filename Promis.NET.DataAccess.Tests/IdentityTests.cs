﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.DataAccess.Identity.MySql;
using FluentAssertions;
using Microsoft.AspNet.Identity;
using Promis.NET.DataAccess.Entities;
using Promis.NET.DataAccess.DbContexts;

namespace Promis.NET.DataAccess.Tests
{
    [TestClass]
    public class IdentityTests
    {
        #region Test Settings
        private UserManager<ApplicationUser> _userManager;
        private ApplicationUser _user;

        [TestInitialize]
        public void SetUp()
        {
            ApplicationDbContext.useLocal = false;
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(ApplicationDbContext.Create()));
            _user = CreateUser();
        }

        [TestCleanup]
        public void TearDown()
        {
            var result = _userManager.Delete(_user);
            _user = null;

            ApplicationDbContext.useLocal = false;
            _userManager.Dispose();
        }
        
        private ApplicationUser CreateUser()
        {
            return new ApplicationUser()
            {
                UserName = "randomuser",
                Password = "Marian",
                FirstName = "Marian",
                LastName = "Focsa",
                ProjectId = "0001",
                Company = "KireyEst",
                RolesModulesString = "$SS_R$SS_W",
                //RolesActivitiesString = "",
                RolesResponsabilitiesString = "$A_AA$B_BB",
                Country = "RO",
                Email = "random.random@kireyest.com",
                MobilePhone = "1234123412341234",
                OfficePhone = "123131452345",
                Language = "en",
                //PhotoUrl = ""
                HasDoc = true
            };
        }
        #endregion

        [TestMethod]
        public void When_CreatingUserAndThenDeletingIt_Then_Everything_Should_BeOK()
        {
            _user.RolesResponsabilitiesString = "";
            _user.RolesModulesString = "";
            _user.RolesActivitiesString = "";

            var result = _userManager.Create(_user, _user.Password);
            result.Succeeded.Should().BeTrue();

            result = _userManager.Delete(_user);
            result.Succeeded.Should().BeTrue();
        }
        
        [TestMethod]
        public void When_CreatingUser_Then_TheIdOfTheUser_Should_BeAssigned()
        {
            var result = _userManager.Create(_user, _user.Password);
            result.Succeeded.Should().BeTrue();
            _user.Id.Should().NotBeNullOrWhiteSpace();
        }

        [TestMethod]
        public void When_CreatingUsersWithRoles_Then_InRole_Should_ReturnTrue()
        {
            var result = _userManager.Create(_user, _user.Password);

            // user us created
            result.Succeeded.Should().BeTrue();

            //user is in created modules roles
            _userManager.IsInRole(_user.Id, "M_SS_R").Should().BeTrue();
            _userManager.IsInRole(_user.Id, "M_SS_W").Should().BeTrue();
            _userManager.IsInRole(_user.Id, "M_SS_K").Should().BeFalse();//inexistent
            _userManager.IsInRole(_user.Id, "A_SS_R").Should().BeFalse();//inexistent
            _userManager.IsInRole(_user.Id, "R_SS_R").Should().BeFalse();//inexistent

            _userManager.AddToRole(_user.Id, "A_SD");
            _userManager.IsInRole(_user.Id, "A_SD").Should().BeTrue();//OK
        }
    }
}
