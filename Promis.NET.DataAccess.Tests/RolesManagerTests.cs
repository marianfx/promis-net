﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.DataAccess.Identity.MySql;
using System.Linq;
using FluentAssertions;
using Promis.NET.Utilities.Models;

namespace Promis.NET.DataAccess.Tests
{
    [TestClass]
    public class RolesManagerTests
    {
        #region Test Settings
        private RolesManager _rolesManager;

        [TestInitialize]
        public void SetUp()
        {
            _rolesManager = new RolesManager();
        }

        [TestCleanup]
        public void TearDown()
        {
            _rolesManager.Dispose();
        }
        #endregion

        [TestMethod]
        public void When__rolesManagerIsEmptyInitialized_Then_TheLists_Should_BeEmpty()
        {
            _rolesManager.RolesActivities.Should().BeEmpty();
            _rolesManager.RolesActivitiesLocal.Should().BeEmpty();
            _rolesManager.RolesModules.Should().BeEmpty();
            _rolesManager.RolesModulesLocal.Should().BeEmpty();
            _rolesManager.RolesResponsabilities.Should().BeEmpty();
            _rolesManager.RolesResponsabilitiesLocal.Should().BeEmpty();
        }

        [TestMethod]
        public void When__rolesManagerIsIntiializedWithOneRoleEach_LocalNaming_Then_TheLists_Should_HaveTheOneElement()
        {
            _rolesManager.SetStrings("SS", "P", "CC");
            
            _rolesManager.RolesActivities.Should().HaveCount(1);
            _rolesManager.RolesActivitiesLocal.Should().HaveCount(1);
            _rolesManager.RolesModules.Should().HaveCount(1);
            _rolesManager.RolesModulesLocal.Should().HaveCount(1);
            _rolesManager.RolesResponsabilities.Should().HaveCount(1);
            _rolesManager.RolesResponsabilitiesLocal.Should().HaveCount(1);
        }

        [TestMethod]
        public void When__rolesManagerIsIntiializedWithOneRoleEach_Then_TheLists_Should_HaveTheOneElement()
        {
            _rolesManager.SetStrings("M_SS", "A_P", "R_CC");

            _rolesManager.RolesActivities.Should().HaveCount(1);
            _rolesManager.RolesActivitiesLocal.Should().HaveCount(1);
            _rolesManager.RolesModules.Should().HaveCount(1);
            _rolesManager.RolesModulesLocal.Should().HaveCount(1);
            _rolesManager.RolesResponsabilities.Should().HaveCount(1);
            _rolesManager.RolesResponsabilitiesLocal.Should().HaveCount(1);
        }

        [TestMethod]
        public void When__rolesManagerIsIntiializedWithRandomRoles_Then_TheLists_Should_HaveTheRequiredElements()
        {
            var r = new Random();
            var _rolesManager = new RolesManager();

            var countM = r.Next(1, 10);//gen length
            _rolesManager.RolesModulesString = string.Join(Global.RolesSeparator, Enumerable.Repeat(0, countM).Select(x => r.Next(10, 100).ToString()));
            var countA = r.Next(1, 10);
            _rolesManager.RolesActivitiesString = string.Join(Global.RolesSeparator, Enumerable.Repeat(0, countA).Select(x => r.Next(10, 100).ToString()));
            var countR = r.Next(1, 10);
            _rolesManager.RolesResponsabilitiesString = string.Join(Global.RolesSeparator, Enumerable.Repeat(0, countR).Select(x => r.Next(10, 100).ToString()));
            _rolesManager.UpdateListsFromStrings();


            _rolesManager.RolesActivities.Should().HaveCount(countA);
            _rolesManager.RolesActivitiesLocal.Should().HaveCount(countA);
            _rolesManager.RolesModules.Should().HaveCount(countM);
            _rolesManager.RolesModulesLocal.Should().HaveCount(countM);
            _rolesManager.RolesResponsabilities.Should().HaveCount(countR);
            _rolesManager.RolesResponsabilitiesLocal.Should().HaveCount(countR);
            _rolesManager.RolesAll.Should().HaveCount(countM + countA + countR);

            //check if they start how it should
            _rolesManager.RolesActivities.ForEach(x => x.Should().StartWith("A_"));
            _rolesManager.RolesModules.ForEach(x => x.Should().StartWith("M_"));
            _rolesManager.RolesResponsabilities.ForEach(x => x.Should().StartWith("R_"));

            _rolesManager.RolesActivitiesLocal.ForEach(x => x.Should().NotStartWith("A_"));
            _rolesManager.RolesModulesLocal.ForEach(x => x.Should().NotStartWith("M_"));
            _rolesManager.RolesResponsabilitiesLocal.ForEach(x => x.Should().NotStartWith("R_"));
        }
    }
}
