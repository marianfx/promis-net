﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.DataAccess.Repositories;
using Promis.NET.DataAccess.DbContexts;
using FluentAssertions;
using System.Globalization;

namespace Promis.NET.DataAccess.Tests
{
    [TestClass]
    public class DashboardRepositoryTests
    {
        protected DashboardRepository DashboardRepository;

        [TestInitialize]
        public void SetUp()
        {
            DashboardRepository = new DashboardRepository(new ProjectDbContext("pw_0001", false));
        }

        #region Tests for Man Hours and Cutoff
        [TestMethod]
        public void When_GetManHoursForP0001_Then_Value_Should_BeDouble()
        {
            var actual = DashboardRepository.GetManHoursForProject();
            actual.Should().BeOfType(typeof(double));
        }

        [TestMethod]
        public void When_TestVirguleIT_Then_Value_Should_BeDouble()
        {
            var query = "";
            double? value = 0.0;
            if(CultureInfo.CurrentCulture.ToString() == new CultureInfo("it-IT").ToString())
            {
                query = "SELECT '1,200' from planning limit 1";
                value = DashboardRepository.ExecuteQueryForValue<double?>(query);
                value.Should().BeGreaterOrEqualTo(0);
                value.Should().BeLessOrEqualTo(100);
            }
        }

        [TestMethod]
        public void When_TestVirguleOthers_Then_Value_Should_BeDouble()
        {
            var query = "";
            double? value = 0.0;
            if (CultureInfo.CurrentCulture.ToString() != new CultureInfo("it-IT").ToString())
            {
                query = "SELECT '1.200' from planning limit 1";
                value = DashboardRepository.ExecuteQueryForValue<double?>(query);
                value.Should().BeGreaterOrEqualTo(0);
                value.Should().BeLessOrEqualTo(100);
            }
        }

        [TestMethod]
        public void When_GettingManHoursForNonExistingProject_Then_Throw_Exception()
        {
            var tempDashService = new DashboardRepository(new ProjectDbContext("xxj"));
            Action func = () => { tempDashService.GetManHoursForProject(); };

            func.ShouldThrow<Exception>();
        }

        [TestMethod]
        public void When_GetManHoursPlanned_Then_Value_Should_BeValidPercent()
        {
            var totalMh = DashboardRepository.GetManHoursForProject();
            var value = DashboardRepository.GetManHoursPlanned("p_2016_09", totalMh);
            value.Should().BeGreaterOrEqualTo(0);
            value.Should().BeLessOrEqualTo(100);
        }

        [TestMethod]
        public void When_GetManHoursPlannedWithActivity_Then_Value_Should_BeValidPercent()
        {
            var totalMh = DashboardRepository.GetManHoursForProject();
            var value = DashboardRepository.GetManHoursPlanned("p_2016_09", totalMh, "SP");
            value.Should().BeGreaterOrEqualTo(0);
            value.Should().BeLessOrEqualTo(100);
        }

        [TestMethod]
        public void When_GetMonthActualPercent_Then_Value_Should_BeValidPercent()
        {
            var totalMh = DashboardRepository.GetManHoursForProject();
            var value = DashboardRepository.GetMonthActualPercent(totalMh, "p_2016_09");
            value.Should().BeGreaterOrEqualTo(0);
            value.Should().BeLessOrEqualTo(100);
        }

        [TestMethod]
        public void When_GetMonthActualPercentWithActivity_Then_Value_Should_BeValidPercent()
        {
            var totalMh = DashboardRepository.GetManHoursForProject();
            var value = DashboardRepository.GetMonthActualPercent(totalMh, "p_2016_09", "SP");
            value.Should().BeGreaterOrEqualTo(0);
            value.Should().BeLessOrEqualTo(100);
        }
        #endregion

        #region Tests for Trends

        [TestMethod]
        public void When_GetTrendsActualData_Then_PercendData_Should_BeNotNullAndValid()
        {
            var totalMh = DashboardRepository.GetManHoursForProject();
            var value = DashboardRepository.GetTrendsActualData(totalMh, "2016", "09");
            value.Should().NotBeNull();
            value.Month.Should().NotBeNullOrWhiteSpace();
            value.Week.Should().NotBeNullOrWhiteSpace();
            value.Percent.Should().BeGreaterOrEqualTo(0);
            value.Percent.Should().BeLessOrEqualTo(100);
            value.PercentWeekly.Should().BeGreaterOrEqualTo(0);
            value.PercentWeekly.Should().BeLessOrEqualTo(100);
        }

        [TestMethod]
        public void When_GetTrendsMonthlyData_Then_PercendData_Should_BeNotNullAndValid()
        {
            var totalMh = DashboardRepository.GetManHoursForProject();
            var value = DashboardRepository.GetTrendsMonthlyData(totalMh, "p_2016_09");
            value.Should().NotBeNull();
            value.Month.Should().NotBeNullOrWhiteSpace();
            value.PercentMonthly.Should().BeGreaterOrEqualTo(0);
            value.PercentMonthly.Should().BeLessOrEqualTo(100);
        }

        [TestMethod]
        public void When_GetTrendsPlannedData_Then_PercendData_Should_BeNotNullAndValid()
        {
            var totalMh = DashboardRepository.GetManHoursForProject();
            var value = DashboardRepository.GetTrendsPlannedData("p_2016_09", "09", "p_2016_08");
            value.Should().NotBeNull();
            value.Percent.Should().BeGreaterOrEqualTo(0);
            value.Percent.Should().BeLessOrEqualTo(100);
            value.PercentWeekly.Should().BeGreaterOrEqualTo(0);
            value.PercentWeekly.Should().BeLessOrEqualTo(100);
            value.PercentMonthly.Should().BeGreaterOrEqualTo(0);
            value.PercentMonthly.Should().BeLessOrEqualTo(100);
        }

        #endregion

        #region Tests for Activities

        [TestMethod]
        public void When_GetMainActivities_Then_DataTableRows_Should_HaveCountNonZero()
        {
            var totalMh = DashboardRepository.GetManHoursForProject();
            var value = DashboardRepository.GetMainActivities("2016", "09");
            value.Should().NotBeNull();
            value.Rows.Count.Should().BeGreaterOrEqualTo(1);
        }

        [TestMethod]
        public void When_GetActualPercentByActivity_Then_TheResult_Should_BeValidPercent()
        {
            var totalMh = DashboardRepository.GetManHoursForProject();
            var value = DashboardRepository.GetActualPercentByActivity(totalMh, "SP", "2017", "06");
            value.Should().NotBeNull();
            value.Should().BeGreaterOrEqualTo(0);
            value.Should().BeLessOrEqualTo(100);
        }

        #endregion

        #region Outstandings

        [TestMethod]
        public void When_GetPunchlistCount_Then_TheResult_Should_BePositive()
        {
            var value = DashboardRepository.GetPunchlistCount();
            value.Should().BeGreaterOrEqualTo(1);
        }

        [TestMethod]
        public void When_GetAllPunchlistData_Then_TheResult_Should_HaveRowCountPositive()
        {
            var nrRows = DashboardRepository.GetPunchlistCount();
            var actualRows = DashboardRepository.GetAllPunchlistData();
            actualRows.Should().NotBeNull();
            actualRows.Count.Should().BeGreaterOrEqualTo(1);
        }

        #endregion
    }
}
