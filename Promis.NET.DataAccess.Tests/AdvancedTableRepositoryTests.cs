﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.DataAccess.Repositories;
using Promis.NET.DataAccess.DbContexts;
using FluentAssertions;
using SqlKata.Compilers;
using System.Linq;
using System;
using Promis.NET.DataAccess.Models;

namespace Promis.NET.DataAccess.Tests
{
    [TestClass]
    public class AdvancedTableRepositoryTests
    {
        protected AdvancedTableRepository AdvancedTableRepository;
        protected MySqlCompiler _compiler;

        [TestInitialize]
        public void SetUp()
        {
            AdvancedTableRepository = new AdvancedTableRepository(new ProjectDbContext("pw_0001", true));
            _compiler = new MySqlCompiler();
        }

        [TestMethod]
        public void When_GetAllColumnsStandardized_NoColumnsNoJoinNoFilters_Then_TheCount_Should_BeTheNumberOfColumns_And_TheColumns_ShouldAllHaveTableNameInThem()
        {
            AdvancedTableRepository.Initialize("work_def");
            var allColumns = AdvancedTableRepository.GetAllColumnsStandardized();
            var allColumnsInvolved = AdvancedTableRepository.GetAllColumnsInvolvedStandardized();//should be the same
            allColumns.Should().HaveCount(17);
            allColumnsInvolved.Should().HaveCount(17);

            foreach (var column in allColumns)
            {
                column.Should().StartWith("work_def.");
            }
        }

        [TestMethod]
        public void When_GetAllColumnsInvolvedStandardized_NoFilters_Then_TheCount_Should_BeTheNumberOfColumns_And_TheInvalidColumns_ShouldBeIgnored()
        {
            AdvancedTableRepository.Initialize("work_def", new string[] { "act", "act_d", "sub_act", "sub_act_d", "marian", "progress.mhrs" }, null,
                new JoinCondition[] { new JoinCondition() { DataTable = "progress", LeftColumn = "act", RightColumn = "progress.act" } });
            var allColumnsInvolved = AdvancedTableRepository.GetAllColumnsInvolvedStandardized();//should be the same
            allColumnsInvolved.Should().HaveCount(6);// ID is automatically added
            allColumnsInvolved.Should().NotContain("work_def.marian");
        }

        [TestMethod]
        public void When_GetCountAllQuery_Then_TheSqlStatement_Should_BeCorrect_And_TestQueryValue_ShouldBe0()
        {
            // no columns => starts with "SELECT COUNT(*)"
            AdvancedTableRepository.Initialize("work_def");
            var countQuery = AdvancedTableRepository.GetCountAllQuery();
            var compiled = _compiler.Compile(countQuery);
            compiled.Sql.Should().StartWith("SELECT COUNT(*)");

            // no columns, distinct => starts with "SELECT COUNT(DISTINCT all_column_list)"
            var columnList = String.Join(", ", AdvancedTableRepository.GetAllColumnsInvolvedStandardized().Where(c => c != "work_def.id").Select(c => '`' + c.Replace(".", "`.`") + "`"));
            AdvancedTableRepository.Initialize("work_def", null, null, true);
            countQuery = AdvancedTableRepository.GetCountAllQuery();
            compiled = _compiler.Compile(countQuery);
            compiled.Sql.Should().StartWith("SELECT COUNT(DISTINCT " + columnList + ")");

            // columns => SELECT COUNT(*)
            AdvancedTableRepository.Initialize("work_def", new string[] { "act", "act_d", "sub_act", "sub_act_d", "marian" });
            countQuery = AdvancedTableRepository.GetCountAllQuery();
            compiled = _compiler.Compile(countQuery);
            compiled.Sql.Should().StartWith("SELECT COUNT(*)");

            // columns, distinct => SELECT COUNT(DISTINCT columns)
            AdvancedTableRepository.Initialize("work_def", new string[] { "act", "act_d", "sub_act", "sub_act_d", "marian" }, null, true);
            countQuery = AdvancedTableRepository.GetCountAllQuery();
            compiled = _compiler.Compile(countQuery);
            compiled.Sql.Should().StartWith("SELECT COUNT(*) AS `count` FROM (SELECT DISTINCT `work_def`.`act`, `work_def`.`act_d`, `work_def`.`sub_act`, `work_def`.`sub_act_d`)");
            
            // test value for inexistent value
            AdvancedTableRepository.Initialize("work_def", new string[] { "act", "act_d" }, new WhereCondition[] { new WhereCondition("act", "2222222222222222222222222222222222222222") });
            var countValue = AdvancedTableRepository.CountAll();
            countValue.Should().Be(0);

            // test with JOIN
            AdvancedTableRepository.Initialize("work_def", new string[] { "act", "act_d" },
                new WhereCondition[] { new WhereCondition("act", "2222222222222222222222222222222222222222") },
                new JoinCondition[] { new JoinCondition() { DataTable = "progress", LeftColumn = "work_def.act", RightColumn = "progress.act" } });
            countQuery = AdvancedTableRepository.GetCountAllQuery();
            compiled = _compiler.Compile(countQuery);
            compiled.Sql.ToLower().Should().Contain("inner join `progress` on `work_def`.`act` = `progress`.`act`");
        }
        
        [TestMethod]
        public void When_GetCountAllFilteredQuery_Then_TheSqlStatement_Should_BeCorrect_And_ContainLikeColumns_ShouldBe0()
        {
            // test with JOIN
            var temprepo = new AdvancedTableRepository(new PromisWebDbContext(true));
            temprepo.Initialize("userfulllist", new string[] { "proj", "userid", "activities", "projects.location" },
                new WhereCondition[] { new WhereCondition("userid", "marian") },
                new JoinCondition[] { new JoinCondition() { DataTable = "projects", LeftColumn = "userfulllist.proj", RightColumn = "projects.proj" } },
                false, "marian");
            var countQuery = temprepo.GetCountAllFilteredQuery();
            var compiled = _compiler.Compile(countQuery);
            compiled.Sql.Should().Contain("(LOWER(`userfulllist`.`proj`) LIKE @p1 OR LOWER(`userfulllist`.`userid`) LIKE @p2 OR LOWER(`userfulllist`.`activities`) LIKE @p3 OR LOWER(`projects`.`location`) LIKE @p4)");
            var result = temprepo.CountAllFiltered();
            result.Should().BeGreaterOrEqualTo(1);
        }

        [TestMethod]
        public void When_GetTableDataQuery_Then_TheSqlStatement_Should_BeCorrect_DataReturned_Correct()
        {
            // test with JOIN
            var temprepo = new AdvancedTableRepository(new PromisWebDbContext(true));
            temprepo.Initialize("userfulllist", new string[] { "proj", "userid", "activities", "projects.location" },
                new WhereCondition[] { new WhereCondition("userid", "marian") },
                new JoinCondition[] { new JoinCondition() { DataTable = "projects", LeftColumn = "userfulllist.proj", RightColumn = "projects.proj" } },
                false, "marian");

            // 1 => without limit / offset
            var dataQuery = temprepo.GetTableDataQuery();
            var compiled = _compiler.Compile(dataQuery);
            var result = temprepo.GetTableData();
            result.Rows.Count.Should().BeGreaterOrEqualTo(1);

            // 2. => with limit / offset
            temprepo.Initialize("userfulllist", null, null,
                new JoinCondition[] { new JoinCondition() { DataTable = "projects", LeftColumn = "userfulllist.proj", RightColumn = "projects.proj" } });
            dataQuery = temprepo.GetTableDataQuery(1, 2);
            compiled = _compiler.Compile(dataQuery);
            result = temprepo.GetTableData(1, 2);
            result.Rows.Count.Should().Be(2);

            // 3 => with order
            temprepo.Initialize("userfulllist", new string[] { "proj", "userid", "activities", "projects.location" },
                new WhereCondition[] { new WhereCondition("userid", "marian") },
                new JoinCondition[] { new JoinCondition() { DataTable = "projects", LeftColumn = "userfulllist.proj", RightColumn = "projects.proj" } },
                new DataTableOrder[] { new DataTableOrder() {  Column = 1, Dir = "DESC" } },
                false, "marian");
            dataQuery = temprepo.GetTableDataQuery(limit: 1);
            compiled = _compiler.Compile(dataQuery);
            result = temprepo.GetTableData(limit: 1);
            result.Rows.Count.Should().Be(1);
        }

    }
}
