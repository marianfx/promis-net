﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.DataAccess.DbContexts;
using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Repositories;
using Promis.Web.DataAccess.Entities.PromisWeb;
using System;

namespace Promis.Web.DataAccess.Tests
{
    [TestClass]
    public class RepositoryForPromisWebDbTests
    {
        private IDbContext _dbContext;
        private Repository _projectRepository;

        [TestInitialize]
        public void SetUp()
        {
            _dbContext = new PromisWebDbContext();
            _projectRepository = new Repository(_dbContext);
        }


        [TestMethod]
        public void When_DbContextIsInitialized_Should_BeOfTypeWebDbContext()
        {
            _dbContext.GetType().Should().Be(typeof(PromisWebDbContext));
        }

        [TestMethod]
        public void When_WeQueryTheRefineryCompany_Should_NotBeNull()
        {
            var theCompany = _projectRepository.GetById<project, string>("0001", "proj");

            theCompany.Should().NotBe(null);
        }

        [TestMethod]
        public void When_WeQueryTheRefineryCompany_Should_BeWithProjId001()
        {
            var theCompany = _projectRepository.GetById<project, string>("0001", "proj");
            
            theCompany.proj.Should().Be("0001");
        }
    }
}
