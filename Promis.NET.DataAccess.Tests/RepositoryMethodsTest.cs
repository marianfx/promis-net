﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.DataAccess.DbContexts;
using Promis.NET.DataAccess.DbContexts.Interfaces;
using Promis.NET.DataAccess.Extensions;
using Promis.NET.DataAccess.Repositories;
using Promis.Web.DataAccess.Entities.PromisWeb;
using System.Linq;

namespace Promis.Web.DataAccess.Tests
{
    [TestClass]
    public class RepositoryMethodsTest
    {
        private IDbContext _dbContext;
        private Repository _projectRepository;

        [TestInitialize]
        public void SetUp()
        {
            _dbContext = new PromisWebDbContext();
            _projectRepository = new Repository(_dbContext);
        }

        [TestCleanup]
        public void TearDown()
        {
            _projectRepository.Dispose();
        }
        

        [TestMethod]
        public void When_GettingById0001_Then_TheProject_Should_NotBeNull()
        {
            var theCompany = _projectRepository.GetById<project, string>("proj", "0001");

            theCompany.Should().NotBe(null);
        }

        [TestMethod]
        public void When_GettingById001_DynamicMethod_Then_TheProject_Should_NotBeNull()
        {
            var dataRow = _projectRepository.GetById("projects", "proj", "0001");
            dataRow.Should().NotBeNull();
            dataRow["proj"].Should().Be("0001");
        }

        
        [TestMethod]
        public void When_CombiningTheNormalAndDynamicMethod_Then_NoError_Should_BeThrown()
        {
            var theCompany = _projectRepository.GetById<project, string>("proj", "0001");
            theCompany.Should().NotBe(null);

            var dataRow = _projectRepository.GetById("projects", "proj", "0001");
            dataRow.Should().NotBeNull();
            dataRow["proj"].Should().Be("0001");
        }

        [TestMethod]
        public void When_GettingAllProjects_Then_Count_Should_Be2()
        {
            var allProjects = _projectRepository.GetAll<project>();
            allProjects.Should().HaveCount(2);
        }

        [TestMethod]
        public void When_CountingProjects_Then_Count_Should_Be2()
        {
            var result = _projectRepository.CountAll<project>();
            result.Should().Be(2);

            // method 2
            var result2 = _projectRepository.ExecuteQueryForValue<double>("SELECT COUNT(*) FROM PROJECTS");
            result2.Should().Be(2);
        }

        [TestMethod]
        public void When_WeQueryTheProjectTableBothMethods_Then_TheProject_Should_BeThe0001Project()
        {
            var proj = _projectRepository.ExecuteQuery<project>("SELECT * FROM projects WHERE proj=@proj", parameters: new System.Collections.Generic.Dictionary<string, object>() { { "proj", "0001" } }).FirstOrDefault();
            proj.proj.Should().Be("0001");


            var dataTable = _projectRepository.ExecuteQuery("SELECT * FROM projects WHERE proj=@proj", parameters: new System.Collections.Generic.Dictionary<string, object>() { { "proj", "0001" } });
            var projects = dataTable.ToList<project>();
            projects.Count.Should().BeGreaterOrEqualTo(1);
            var project = projects[0];
            project.proj.Should().Be("0001");
        }
        
        [TestMethod]
        public void When_CheckingIfTableExist_Then_projectsTable_Should_Exist_AND_prjctsTable_Should_NotExist()
        {
            var exists = _projectRepository.DoesTableExist("projects");
            exists.Should().BeTrue();
            exists = _projectRepository.DoesTableExist("prjcts");
            exists.Should().BeFalse();
        }

        [TestMethod]
        public void When_ExecutingTwoUpdatesInTheSameTransaction_Then_RollingBack_Should_RemoveTheTransactions()
        {
            _projectRepository = new Repository(new PromisWebDbContext(true));
            _projectRepository.UpdateById("userfulllist",
                new System.Collections.Generic.List<NET.Utilities.Models.DataKeeper>()
                {
                    new NET.Utilities.Models.DataKeeper("proj", "0002")
                },
                "id", 11986);
            _projectRepository.UpdateById("userfulllist",
                new System.Collections.Generic.List<NET.Utilities.Models.DataKeeper>()
                {
                    new NET.Utilities.Models.DataKeeper("Country", "GB")
                },
                "id", 11986);
            //here transaction is not commited, roll it back
            _projectRepository.FinishTransaction(commit: false);

            //test
            var users = _projectRepository.GetAll<userfulllist>();
            var user = _projectRepository.GetById<userfulllist, long>("id", 11986);
            user.proj.Should().Be("0001");
            user.Country.Should().Be("RO");
        }

        [TestMethod]
        public void When_ExecutingTwoUpdatesInTheSameTransaction_Then_CommitingBothOfThem_Should_SaveTheUpdates()
        {
            _projectRepository = new Repository(new PromisWebDbContext(true));
            _projectRepository.UpdateById("userfulllist",
                new System.Collections.Generic.List<NET.Utilities.Models.DataKeeper>()
                {
                    new NET.Utilities.Models.DataKeeper("proj", "0002")
                },
                "id", 11986);
            _projectRepository.UpdateById("userfulllist",
                new System.Collections.Generic.List<NET.Utilities.Models.DataKeeper>()
                {
                    new NET.Utilities.Models.DataKeeper("Country", "GB")
                },
                "id", 11986);
            _projectRepository.FinishTransaction();

            //here transaction is not commited, but at the disposal of the object, it will be comitted
            var user = _projectRepository.GetById<userfulllist, long>("id", 11986);
            user.proj.Should().Be("0002");
            user.Country.Should().Be("GB");

            //revert
            _projectRepository.UpdateById("userfulllist",
                new System.Collections.Generic.List<NET.Utilities.Models.DataKeeper>()
                {
                    new NET.Utilities.Models.DataKeeper("proj", "0001")
                },
                "id", 11986);
            _projectRepository.UpdateById("userfulllist",
                new System.Collections.Generic.List<NET.Utilities.Models.DataKeeper>()
                {
                    new NET.Utilities.Models.DataKeeper("Country", "RO")
                },
                "id", 11986);
        }

        [TestMethod]
        public void When_ExecutingTwoUpdatesInTheSameTransaction_Then_CommitingEachInTheirOwnScope_Should_SaveTheUpdates()
        {
            _projectRepository = new Repository(new PromisWebDbContext(true));
            //run with commit
            _projectRepository.UpdateById("userfulllist",
                new System.Collections.Generic.List<NET.Utilities.Models.DataKeeper>()
                {
                    new NET.Utilities.Models.DataKeeper("proj", "0002")
                },
                "id", 11986, saveChanges: true);
            //test
            var user = _projectRepository.GetById<userfulllist, long>("id", 11986);
            user.proj.Should().Be("0002");

            //run with commit
            _projectRepository.UpdateById("userfulllist",
                new System.Collections.Generic.List<NET.Utilities.Models.DataKeeper>()
                {
                    new NET.Utilities.Models.DataKeeper("Country", "GB")
                },
                "id", 11986, saveChanges: true);
            //test
            user = _projectRepository.GetById<userfulllist, long>("id", 11986);
            user.Country.Should().Be("GB");

            //revert all
            _projectRepository.UpdateById("userfulllist",
                new System.Collections.Generic.List<NET.Utilities.Models.DataKeeper>()
                {
                    new NET.Utilities.Models.DataKeeper("proj", "0001")
                },
                "id", 11986);
            _projectRepository.UpdateById("userfulllist",
                new System.Collections.Generic.List<NET.Utilities.Models.DataKeeper>()
                {
                    new NET.Utilities.Models.DataKeeper("Country", "RO")
                },
                "id", 11986);
        }
    }
}
