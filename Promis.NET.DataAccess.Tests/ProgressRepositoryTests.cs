﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.DataAccess.Repositories;
using Promis.NET.DataAccess.DbContexts;
using FluentAssertions;

namespace Promis.NET.DataAccess.Tests
{
    [TestClass]
    public class ProgressRepositoryTests
    {
        protected ProgressRepository _progressRepository;
        protected ProjectRepository _projectRepository;

        [TestInitialize]
        public void SetUp()
        {
            _progressRepository = new ProgressRepository(new ProjectDbContext("pw_0001", true));
            _projectRepository = new ProjectRepository(new PromisWebDbContext(true));
        }

        [TestMethod]
        public void When_GetQuantitiesDataForSP_Then_TheList_Should_BePopulated()
        {
            var result = _progressRepository.GetQuantitiesData("SP");
            result.Should().NotBeNull();
            result.Count.Should().BeGreaterOrEqualTo(1);

        }

        [TestMethod]
        public void When_GetSumQuantityForSubactivityFromTablesForSP_Then_TheResult_Should_BeANumberBiggerThan0()
        {
            var tables = _projectRepository.GetMainActivityTables("PI");
            tables.AddRange(tables);
            var sumResult = _progressRepository.GetSumQuantityForSubactivityFromTables(tables, "PI021");
        }

    }
}
