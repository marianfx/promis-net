﻿using System;
using System.Globalization;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.Services.Extensions;

namespace Promis.NET.Services.Tests
{
    [TestClass]
    public class DateTimeExtensionsTests
    {
        [TestMethod]
        public void When_TestingMonth_InputIsMonthWithTwoDigits_Then_Result_Should_BeTheTwoDigitsAsString()
        {
            var result = DateTime.Parse("12/21/2012", CultureInfo.InvariantCulture).MonthAsTwoDigitsString();
            result.Should().Be("12");
        }

        [TestMethod]
        public void When_TestingMonth_InputIsMonthWithOneDigit_Then_Result_Should_BeTheTwoDigitsAsString()
        {
            var result = DateTime.Parse("2/21/2012", CultureInfo.InvariantCulture).MonthAsTwoDigitsString();
            result.Should().Be("02");
        }

        [TestMethod]
        public void When_TestingYear_InputIsYear_Then_Result_Should_HaveLengthFourAndBeTheYear()
        {
            var result = DateTime.Parse("2/21/2012", CultureInfo.InvariantCulture).YearAsString();
            result.Should().Be("2012");
            result.Length.Should().Be(4);
        }

        [TestMethod]
        public void When_TestingWeek_InputIsThe36Week_Then_Result_Should_36()
        {
            var result = DateTime.Parse("09/06/2017", CultureInfo.InvariantCulture).WeekOfTheYearAsTwoDigitString();
            result.Should().Be("36");
        }

        [TestMethod]
        public void When_TestingFlag_InputIs09062017_Then_Result_Should_StartWithpHaveTwoDashesAndTheExactSameYearAndMonth()
        {
            var result = DateTime.Parse("09/06/2017", CultureInfo.InvariantCulture).GetFlagForYearAndMonth();
            result.Should().StartWith("p");
            var splitted = result.Split('_');
            splitted.Should().HaveCount(3);
            splitted[1].Should().Be("2017");
            splitted[2].Should().Be("09");
        }
    }
}
