﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.DataAccess.DbContexts;
using Promis.NET.DataAccess.Repositories;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Services;

namespace Promis.NET.Services.Tests
{
    [TestClass]
    public class ProgressServiceTests
    {
        private ProgressService _progressService;
        protected Project _project;

        [TestInitialize]
        public void SetUp()
        {
            _progressService = new ProgressService(
                    new ProgressRepository(new ProjectDbContext("pw_0001", true)),
                    new ProjectRepository(new PromisWebDbContext(true)),
                    new DashboardRepository(new ProjectDbContext("pw_0001", true)),
                    new PdfCreatorPlanningAndProgress(""));
        }

        [TestCleanup]
        public void TearDown()
        {
            _progressService = null;
        }

        [TestMethod]
        public void When_GetQuantitiesDataForSP_Then_TheOutputList_Should_BeValidAndHaveAtLeastOneElement()
        {
            var output = _progressService.GetQuantitiesData("SP");
            output.Should().NotBeNull();
            output.Count.Should().BeGreaterOrEqualTo(1);
        }
    }
}
