﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.Services.Services.Interfaces;
using System.IO;
using FluentAssertions;
using Promis.NET.Services.Services;
using Promis.NET.DataAccess.Repositories;
using Promis.NET.DataAccess.DbContexts;
using System.Collections.Generic;
using Promis.NET.Services.Models.Kpi;
using System;
using Promis.NET.Services.Models.Certification;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Models.PlanningAndProgress;

namespace Promis.NET.Services.Tests
{
    [TestClass]
    public class ExportToPdfTest
    {
        private IPdfCreator pdfCreator;
        private IProjectService projectService;
        private string serverPath = Path.Combine(Directory.GetCurrentDirectory(), "..", "..");
        private string directoryPath = "App_Data/PDFs/Reports";
        private Project project;

        [TestInitialize]
        public void SetUp()
        {
            var projectRepository = new ProjectRepository(new PromisWebDbContext(false));
            projectService = new ProjectService(projectRepository);
            project = projectService.GetProjectById("0001");
        }

        [TestMethod]
        public void When_CreatingDocumentCompletion_Then_TheFilePath_Should_Exist()
        {
            var systemCode = "SUP01-P22";
            var datatables = new List<string>()
            {
                "Electrical Cables", "Electrical Components", "Instrument Components"
            };
            var dummyKpiSystemList1 = new List<KpiSystem>()
            {
                new KpiSystem("P.02-UPS-1-1A", "Test description 1", systemCode),
                new KpiSystem("P.02-UPS-1-1A-CON", "Test description 2", systemCode),
            };
            var dummyKpiSystemList2 = new List<KpiSystem>()
            {
                new KpiSystem("EL0381-1", "Test description 3", systemCode),
            };
            var data = new Dictionary<string, IEnumerable<KpiSystem>>()
            {
                { "Electrical Cables", dummyKpiSystemList1 },
                { "Electrical Components", dummyKpiSystemList2 }
            };

            pdfCreator = new PdfCreatorCompletion(serverPath, directoryPath, systemCode, "AMMONIA COMPRESSOR STEAM TURBINE", project, null, datatables, data);
            var outFile = pdfCreator.CreatePdf();
            File.Exists(Path.Combine(serverPath, directoryPath, outFile)).Should().Be(true);
        }

        [TestMethod]
        public void When_CreatingDocumentCertification_Then_TheFilePath_Should_Exist()
        {
            var codeTuple = new Tuple<string, string>("SP181", "PLASTIC, GRP SEWERS");
            var tag = "SP181-DP15A-040-150-CS-10518";
            var data = new List<CertificationSummarySteps>()
            {
                new CertificationSummarySteps(){ Description = "EXCAVATION"},
                new CertificationSummarySteps(){ Description = "REINFORCED CONCRETE PITS"},
                new CertificationSummarySteps(){ Description = "PIPES JOINING"},
                new CertificationSummarySteps(){ Description = "JOINING VISUAL CHECK", Ord = "X", Subcontractor1 = new KeyValuePair<string, string>("J. PAGAC", "24/11/2016"), Contractor = new KeyValuePair<string, string>("J. PAGAC", "24/11/2016")},
                new CertificationSummarySteps(){ Description = "PIPES POSITIONING"},
                new CertificationSummarySteps(){ Description = "PIPES/PITS JOINTS SEALING"},
                new CertificationSummarySteps(){ Description = "LEAKAGE (GRAVITY TEST)", Ord = "X", Subcontractor1 = new KeyValuePair<string, string>("J. PAGAC", "24/11/2016"), Contractor = new KeyValuePair<string, string>("J. PAGAC", "24/11/2016")},
                new CertificationSummarySteps(){ Description = "AUTHORIZATION FOR JOINTS PROTECTION RESTORE", Ord = "X", FileText = "X", Subcontractor1 = new KeyValuePair<string, string>("J. PAGAC", "24/11/2016"), Contractor = new KeyValuePair<string, string>("J. PAGAC", "24/11/2016")},
                new CertificationSummarySteps(){ Description = "BACKFILLING"},
            };

            var signingUsers = new string[2] { "J. PAGAC", "K. GENOV" };
            var docToAttach = "documents\\attach.pdf";
            pdfCreator = new PdfCreatorCertification(serverPath, directoryPath, codeTuple, tag, project, data, docToAttach, signingUsers);
            var outFile = pdfCreator.CreatePdf();
            File.Exists(Path.Combine(serverPath, directoryPath, outFile)).Should().Be(true);
        }

        [TestMethod]
        public void When_CreatingDocumentPlanningAndProgress_Then_TheFilePath_Should_Exist()
        {
            var codeTuple = new Tuple<string, string>("SP081", "GRAVEL FINISHING");
            string budget = "1570";
            string[] inspection =  { "FINAL CHECK", "BACKFILLING", "Sand Covery", "", "", "", "", "Test", "Test Description", "", "", "" };
            string[] progress = { "100", "10", "30", "70", "", "", "", "", "", "", "", "" };
            string[] lp = { "100", "100", "100", "", "", "", "", "", "", "", "", "" };
            string[] position = { "1", "2", "3", "4", "", "", "", "", "", "", "", "" };
            var data = new ProgressSteps()
            {
                SubAct = "SP081",
                SubActDescription = "Soil Prep",
                UM = "SQM",
                InspectionArray = inspection,
                ProgressArray = progress,
                LpArray=lp,
                Step = "10",
                Pos = position            
            };
           
            var dataProgr = new List<ProgressData>()
            {
                new ProgressData(){Tag="SP081-DP90-SOIL FINISHING", S1="90", S2="", S3="", S4="", S5="70", S6="", S7="", S8="", S9="", S10="", S11="", S12="", Wbs="000", Pr=10,SubAct="SP081", Mhrs="100.2", DataTable="aaa", Equiv="10",Qty="5" },
                new ProgressData(){Tag="SP081-DP75-SOIL FINISHING", S1="100", S2="100", S3="", S4="", S5="", S6="", S7="", S8="", S9="", S10="", S11="", S12="", Wbs="000", Pr=10,SubAct="SP081", Mhrs="10.5", DataTable="aaa", Equiv="10",Qty="5" }
            };

            var docToAttach = "documents\\attach.pdf";
            pdfCreator = new PdfCreatorPlanningAndProgress(serverPath, directoryPath, codeTuple, project, data, dataProgr, "QM", budget, docToAttach);
            var outFile = pdfCreator.CreatePdf();
            File.Exists(Path.Combine(serverPath, directoryPath, outFile)).Should().Be(true);
    }       

    }
}


