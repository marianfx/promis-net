﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Services.Models.Excel;
using System.IO;
using Promis.NET.Services.Services;
using System.Collections.Generic;
using System.Data;
using FluentAssertions;
using System;

namespace Promis.NET.Services.Tests
{
    [TestClass]
    public class ImportExcelTests
    {
        private IExcelService _excelService;
        private ExcelData _excelData;


        [TestInitialize]
        public void SetUp()
        {
            _excelData = new ExcelData
            {

                //_excelData.WorkbookName = "pw_0001-subcontractors(marian)-6c66f0f2-c0b6-4dad-903d-8836aa025931.xls";
                //_excelData.WorkbookName = "pw_0002-(marian)-2c059fb6-4343-41b5-a991-eee3f6d48254.xls";
                WorkbookName = "aa.xls"
            };
            _excelData.FilePath = Path.Combine(Directory.GetCurrentDirectory(),"ImportExcel", _excelData.WorkbookName);

            _excelData.SheetList = new List<Sheet>();
            var sheet1 = new Sheet
            {
                SheetName = "subcontractors1"
            };
            sheet1.Columns.Add("CODE");
            sheet1.Columns.Add("SUBCONTRACTOR");
            _excelData.SheetList.Add(sheet1);

            var sheet2 = new Sheet
            {
                SheetName = "subcontractors2"
            };
            _excelData.SheetList.Add(sheet2);


            _excelService = new ExcelService(_excelData);
         //   _engineeringService = new EngineeringService(new EngineeringRepository(new ProjectDbContext("pw_0001")));
        }

        #region Tests for the "ReadSheet" function
        [TestMethod]
        public void When_ReadingSheet_Then_TheExpectedColumns_Should_BeTheSameWithTheGivenColumns()
        {
              
          
            DataTable table = new DataTable();
            var columnsExpected = new List<string>() { "CODE", "SUBCONTRACTOR" };
            var columnsRequired = new List<string>() { "CODE", "SUBCONTRACTOR" };

             var res = _excelService.ReadSheet(_excelData.FilePath, _excelData.SheetList[0], columnsExpected, columnsRequired);
            
            columnsExpected.Should().BeEquivalentTo(_excelData.SheetList[0].Columns);
            //_excelData.SheetList[0].SheetName.ShouldBeEquivalentTo("subcontractors1");
        }
        #endregion

        #region Tests for the "ReadSheet" function
        [TestMethod]
        public void When_ReadingSheet_IfThExpectedColumns_AreNotTheSameWithTheGivenColumns_ThenThrowException()
        {

            var columnsExpected = new List<string>() { "CODE", "ID" };
            var columnsRequired = new List<string>() { "CODE", "SUBCONTRACTOR" };

            Action func = () => { _excelService.ReadSheet(_excelData.FilePath, _excelData.SheetList[0], columnsExpected, columnsRequired); };

            func.ShouldThrow<Exception>();

        }
        #endregion

        #region Tests for the "ReadSheet" function, to verify the exception when columns are not the same
        [TestMethod]
        public void When_ReadingSheet_IfTheExpectedColumns_AreNotTheSameWithTheTableColumns_ThenThrowThisException()
        {

            var columnsExpected = new List<string>() { "CODE", "ID" };
            var columnsRequired = new List<string>() { "CODE", "SUBCONTRACTOR" };
            
            Exception caughtException = null;
            try
            {
                _excelService.ReadSheet(_excelData.FilePath, _excelData.SheetList[0], columnsExpected, columnsRequired);
            }
            
            catch (Exception ex)
            {
                caughtException = ex;
            }
            caughtException.Should().NotBeNull();
            caughtException.Message.Should().Be("The column ID cannot be found in the document.");
             
        }
        #endregion


        #region Tests for the "ReadSheet" function, to verify the exception when required columns have no value 
        [TestMethod]
        public void When_ReadingSheet_IfTheExpectedColumns_HaveNoValues_ThenThrowThisException()
        {

            var columnsExpected = new List<string>() { "CODE", "SUBCONTRACTOR" };
            var columnsRequired = new List<string>() { "CODE", "SUBCONTRACTOR" };

            Exception caughtException = null;
            try
            {
                _excelService.ReadSheet(_excelData.FilePath, _excelData.SheetList[0], columnsExpected, columnsRequired);
            }

            catch (Exception ex)
            {
                caughtException = ex;
            }
            caughtException.Should().NotBeNull();
            caughtException.Message.Should().Be("Column SUBCONTRACTOR is required and cannot be empty. Error at row 1.");
             
        }
        #endregion

        #region Tests for the "ReadExcelFile" function, to verify an excel file with more than 1 sheet
        [TestMethod]
        public void When_ReadingExcelFile_TheColumnsShouldBe_TheSameInAllSheets()
        {
            
            var columnsExpected = new List<string>() { "CODE", "SUBCONTRACTOR" };
            var columnsRequired = new List<string>() { "CODE", "SUBCONTRACTOR" };    

           var res = _excelService.ReadExcelFile(_excelData.FilePath, columnsExpected, columnsRequired);
            columnsExpected.Should().BeEquivalentTo(_excelData.SheetList[1].Columns);
            _excelData.SheetList[1].SheetName.ShouldBeEquivalentTo("subcontractors2");

        }
        #endregion
    }




}