﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Services.Models.Excel;
using Promis.NET.Utilities.Models;
using System.Collections.Generic;
using System.IO;
using FluentAssertions;
using Promis.NET.Services.Models.Engineering;
using Promis.NET.Services.Services;
using Promis.NET.DataAccess.Repositories;
using Promis.NET.DataAccess.DbContexts;

namespace Promis.NET.Services.Tests
{
    [TestClass]
    public class ExportToExcelTests
    {
        private IExcelService _excelService;
        private IEngineeringService _engineeringService;
        private ExcelData _excelData;

        [TestInitialize]
        public void SetUp()
        {
            _excelData = new ExcelData();
            _excelData.WorkbookName = "pw_0001-Subcontractors(avasilache)-56789.xls";
            _excelData.FilePath = Path.Combine(Directory.GetCurrentDirectory(), "Export Excel", _excelData.WorkbookName);
            _excelData.SheetList = new List<Sheet>();

            var sheet1 = new Sheet
            {
                SheetName = "xxyz1"
            };
            sheet1.Columns.Add("Code");
            sheet1.Columns.Add("Name");
            sheet1.Columns.Add("Surname");
            _excelData.SheetList.Add(sheet1);

            _excelService = new ExcelService(_excelData);
            _engineeringService = new EngineeringService(new EngineeringRepository(new ProjectDbContext("pw_0001")), new AdvancedTableRepository(new ProjectDbContext("pw_0001")), _excelService);
        }

        #region Tests for the CreatingDocument() function
        [TestMethod]
        public void When_CreatingDocument_Then_ThFilePath_Should_Exist()
        {
            _excelService.CreateDocument();
            new FileInfo(_excelService.ExcelData.FilePath).Exists.Should().Be(true);
        }
        #endregion

        #region Tests for the ExportExcel function
        [TestMethod]
        public void When_GettingDataFortheExcel_Then_TheDataforExcel_Should_BeInitialised()
        {
            var listDBCell = new List<DataKeeper>();
            var row1 = new List<DataKeeper>()
            {
                new DataKeeper("Code","0001"),
                new DataKeeper("Name","Alex"),
                new DataKeeper("Surname","Blabla")
            };

            var row2 = new List<DataKeeper>()
            {
                new DataKeeper("Code","0002"),
                new DataKeeper("Name","Ana"),
                new DataKeeper("Surname","Abc"),
            };

            var row3 = new List<DataKeeper>()
            {
                 new DataKeeper("Code","0003"),
                new DataKeeper("Name","Cristina"),
                new DataKeeper("Surname","Traian")
            };

            var rows = new List<List<DataKeeper>>() { row1, row2, row3 };

            _excelService.AddRows(_excelData.SheetList[0], rows);

            _excelData.WorkbookName.Should().NotBeNullOrEmpty();
            _excelData.FilePath.Should().NotBeNullOrEmpty();
            _excelData.SheetList.Should().NotBeNullOrEmpty();

        }
        #endregion

        #region Tests for the exportingExcel 
        [TestMethod]
        public void When_GeneratingAnExcelFile_Then_SheelListMustHave_theGivenLength()
        {

            var datatable = "ug_piles";
            var projectid = "pw_0001";
            var username = "avasilache";

            var directoryContainer = Path.Combine(Directory.GetCurrentDirectory(), "Export Excel");
            _engineeringService.GenerateExcel(datatable, projectid, username, directoryContainer, false);

            _excelData.SheetList.Count.Should().Be(1);
            #endregion

        }
    }
}


