﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.DataAccess.DbContexts;
using Promis.NET.DataAccess.Repositories;
using Promis.NET.Services.Models.Dashboard;
using Promis.NET.Services.Services;
using Promis.NET.Services.Services.Interfaces;
using System.Linq;

namespace Promis.NET.Services.Tests
{
    [TestClass]
    public class DashboardServiceTests
    {
        private IDashboardService _dashboardService;
        private string _projectDb;

        [TestInitialize]
        public void SetUp()
        {
            _projectDb = "pw_0001";
            _dashboardService = new DashboardService(new DashboardRepository(new ProjectDbContext(_projectDb)));
        }

        [TestCleanup]
        public void TearDown()
        {
            _dashboardService = null;
            _projectDb = null;
        }

        #region Tests for First Big Cumulated Graph
        [TestMethod]
        public void When_GettingPlannedDataForChartsForDefaultProject_Then_TheArray_Should_NotBeNull()
        {
            var chartData = new DashboardData();
            _dashboardService.GetChartsProgressData(ref chartData);
            chartData.ProgressChartValuesPlanned.Should().NotBeNullOrEmpty();
        }
        #endregion

        #region Tests for Delay Graph
        [TestMethod]
        public void When_GettingDelayedDataForChartsWithoutArrayInitializationForDefaultProject_Then_TheProgressData_Should_AutoLoad()
        {
            var chartData = new DashboardData();
            
            _dashboardService.GetChartsDelayData(ref chartData);
            chartData.ProgressChartLabels.Should().NotBeEmpty();
        }
        #endregion

        #region Tests for The Round KPIs
        [TestMethod]
        public void When_GettingPercentDataForChartsForDefaultProject_Then_TheArray_Should_NotBeNull()
        {
            var chartData = new DashboardData();
            _dashboardService.GetChartsTrendsData(ref chartData);

            chartData.TrendingChartActualPercent.Should().NotBe(0);
            chartData.TrendingChartActualPercentWeekly.Should().NotBe(0);
            chartData.TrendingChartActualPercentMonthly.Should().NotBe(0);
            chartData.TrendingChartPlannedPercent.Should().NotBe(0);
            chartData.TrendingChartPlannedPercentWeekly.Should().NotBe(0);
            chartData.TrendingChartPlannedPercentMonthly.Should().NotBe(0);
            chartData.TrendingChartCertifiedPercent.Should().NotBe(0);
            chartData.TrendingChartCertifiedPercentWeekly.Should().NotBe(0);
            chartData.TrendingChartCertifiedPercentMonthly.Should().NotBe(0);
        }

        [TestMethod]
        public void When_VerifyCertifiedPercentDataForChartsForDefaultProject_Then_TheCertifiedValues_Should_BeSmaller_Than_ProgressAndPlannedValues()
        {
            var chartData = new DashboardData();
            _dashboardService.GetChartsTrendsData(ref chartData);

            chartData.TrendingChartCertifiedPercent.Should().BeLessThan(chartData.TrendingChartActualPercent);
            chartData.TrendingChartCertifiedPercentWeekly.Should().BeLessThan(chartData.TrendingChartActualPercentWeekly);
            chartData.TrendingChartCertifiedPercentMonthly.Should().BeLessThan(chartData.TrendingChartActualPercentMonthly);
            chartData.TrendingChartCertifiedPercent.Should().BeLessThan(chartData.TrendingChartPlannedPercent);
            chartData.TrendingChartCertifiedPercentWeekly.Should().BeLessThan(chartData.TrendingChartPlannedPercentWeekly);
            chartData.TrendingChartCertifiedPercentMonthly.Should().BeLessThan(chartData.TrendingChartPlannedPercentMonthly);
        }
        #endregion

        #region Tests for Main Activities and Outstanding



        #endregion
        
        #region  Tests for Main Activities
        [TestMethod]
        public void When_GettingMainActivitiesData_Then_TheLists_Should_NotBeEmpty()
        {
            var chartsModel = new DashboardData();
            _dashboardService.GetChartsMainActivitiesData(ref chartsModel);

            chartsModel.ActivitiesCodes.Should().NotBeNullOrEmpty();
            chartsModel.ActivitiesPlannedData.Should().NotBeNullOrEmpty();
            chartsModel.ActivitiesActualData.Should().NotBeNullOrEmpty();
            chartsModel.ActivitiesWithProgresses.Should().NotBeNullOrEmpty();
        }

        [TestMethod]
        public void When_GettingMainActivitiesData_Then_TheActivities_Should_Be15_And_ContainSPAndIS()
        {
            var chartsModel = new DashboardData();
            _dashboardService.GetChartsMainActivitiesData(ref chartsModel);

            chartsModel.ActivitiesWithProgresses.Should().HaveCount(15);
            chartsModel.ActivitiesWithProgresses.FirstOrDefault(a => a.Code == "SP").Should().NotBeNull();
            chartsModel.ActivitiesWithProgresses.FirstOrDefault(a => a.Code == "IS").Should().NotBeNull();
        }
        #endregion

        #region Tests for Outstanding Data
        [TestMethod]
        public void When_GettingOutstandingData_Then_AllTheLists_Should_BePopulated()
        {
            var chartsModel = new DashboardData();
            _dashboardService.GetChartsOutstandingsData(ref chartsModel);

            chartsModel.OutstandingChartFlags.Should().NotBeNullOrEmpty();
            chartsModel.OutstandingChartFlagDescriptions.Should().NotBeNullOrEmpty();
            chartsModel.OutstandingChartCounts.Should().NotBeNullOrEmpty();
        }
        
        [TestMethod]
        public void When_GettingOutstandingData_Then_AllTheFlags_Should_NotBeNegative()
        {
            var chartsModel = new DashboardData();
            _dashboardService.GetChartsOutstandingsData(ref chartsModel);

            chartsModel.PunchlistCount.Should().BeGreaterOrEqualTo(0);
            chartsModel.PunchlistDoneCount.Should().BeGreaterOrEqualTo(0);
            chartsModel.ClearancePercentage.Should().BeGreaterOrEqualTo(0);
        }

        [TestMethod]
        public void When_GettingOutstandingData_Then_ClearancePercentage_Should_BeAValidPercent()
        {
            var chartsModel = new DashboardData();
            _dashboardService.GetChartsOutstandingsData(ref chartsModel);
            
            chartsModel.ClearancePercentage.Should().BeGreaterOrEqualTo(0);
            chartsModel.ClearancePercentage.Should().BeLessOrEqualTo(100);
        }

        [TestMethod]
        public void When_GettingOutstandingData_Then_AllTheFlags_Should_RespectLogicalValues()
        {
            var chartsModel = new DashboardData();
            _dashboardService.GetChartsOutstandingsData(ref chartsModel);

            chartsModel.PunchlistDoneCount.Should().BeLessOrEqualTo(chartsModel.PunchlistCount);
            if (chartsModel.PunchlistCount == 0)
                chartsModel.ClearancePercentage.Should().Be(0);//a check is needed for Done Count too, but that is done above
            else
                chartsModel.ClearancePercentage.Should().BeGreaterThan(0);
        }
        #endregion
    }
}
