﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.Utilities.Exceptions;
using Promis.NET.Utilities.Logging;
using Promis.NET.Utilities.Models;
using System;

namespace Promis.NET.Utilities.Tests
{
    [TestClass]
    public class PromisExceptionTests
    {
        [TestMethod]
        public void When_ExceptionIsInitialized_Then_AllTheFields_Should_NotBeEmpty()
        {
            var pe = new PromisException();
            pe.ErrorDetailsUserLevel.Should().NotBeNull();
            pe.ErrorDetailsUserLevel.StartingMethod.Should().NotBeEmpty();
            pe.ErrorDetailsUserLevel.Message.Should().NotBeEmpty();
            pe.ErrorDetailsUserLevel.Causes.Should().NotBeEmpty();

            pe.ErrorDetailsDevLevel.Should().NotBeNull();
            pe.ErrorDetailsDevLevel.StackTrace.Should().NotBeEmpty();
            pe.ErrorDetailsDevLevel.FullMessageList.Should().NotBeEmpty();
        }

        [TestMethod]
        public void When_ExceptionIsInitializedFromAnotherException_Then_TheInnerException_Should_BeTheOriginalException()
        {
            var orige = new Exception("My Exception");
            var pe = new PromisException(orige);
            pe.InnerException.Should().Be(orige);
        }

        [TestMethod]
        public void When_ExceptionIsThrownAndPromisCatchesIt_Then_TheStarterMethod_Should_BeThisOne()
        {
            try
            {
                throw new Exception("My exception happened.");
            }
            catch (Exception e)
            {
                var pe = new PromisException(e);
                pe.Message.Should().Contain("My exception happened.");
                var expectedString = Global.RegexThatSplitsByCapitalLetters.Replace("When_ExceptionIsThrownAndPromisCatchesIt_Then_TheStarterMethod_Should_BeThisOne", " ");
                pe.ErrorDetailsUserLevel.StartingMethod.Should().Be(expectedString);
            }
        }
        
        [TestMethod]
        public void When_ExceptionIsThrownAndPromisCatchesIt_Then_TheCauses_Should_CountToOneAndBePromisException()
        {
            try
            {
                throw new Exception("My exception happened.");
            }
            catch (Exception e)
            {
                var pe = new PromisException(e);

                pe.ErrorDetailsUserLevel.Causes.Should().HaveCount(1);
                pe.ErrorDetailsUserLevel.Causes[0].Should().Be("Exception");
            }
        }
    }
}
