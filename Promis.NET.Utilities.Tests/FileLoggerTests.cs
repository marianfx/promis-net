﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promis.NET.Utilities.Logging;
using System;
using System.Configuration;
using System.IO;

namespace Promis.NET.Services.Tests
{
    [TestClass]
    public class FileLoggerTests
    {
        private ILogger _logger;

        [TestInitialize]
        public void SetUp()
        {
            _logger = new FileLogger();
        }

        [TestCleanup]
        public void TearDown()
        {
            _logger = null;
        }

        [TestMethod]
        public void When_LoggerFolderVariableIsSetToCDisk_Then_AFolder_Should_ExistOnC()
        {
            var di = new DirectoryInfo("C:\\Server Logs");
            di.Exists.Should().Be(true);
        }

        [TestMethod]
        public void When_AccessingAnForbiddenPath_Then_FilePath_ShouldBeEmptyString()
        {
            ConfigurationManager.AppSettings.Set("LoggerFolder", "C:\\Program Files (x86)\\IIS Express");
            var tempLogger = new FileLogger();
            tempLogger.FilePath.Should().BeEmpty();
        }

        [TestMethod]
        public void When_LoggerFolderIsInexistentOrEmptyInAppConfig_Then_BaseDirectory_ShouldBeCurrentAppDirectory()
        {
            ConfigurationManager.AppSettings.Set("LoggerFolder", "");
            var tempLogger = new FileLogger();
            var dirContainer = Directory.GetParent(Directory.GetParent(tempLogger.FilePath).FullName).FullName;//get parent of "Server Logs"
            dirContainer.Should().Be(Directory.GetCurrentDirectory());
        }
    }
}
