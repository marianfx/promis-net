﻿using Promis.NET.DataAccess.DbContexts;
using Promis.NET.DataAccess.Repositories;
using Promis.NET.DataAccess.Repositories.Interfaces;
using System.Configuration;
using System.IO;
using System;
using Resources.Utility;
using Promis.NET.Localization;
using Promis.NET.Utilities.Roles;
using Promis.NET.DataAccess.Identity.MySql;

namespace Promis.NET.Utilities.Localization
{
    public class Program
    {
        public static IRepository TheRepository { get; set; } = new Repository(new PromisWebDbContext(ApplicationDbContext.useLocal));
        public static string LocalResourcesNamespace = "Promis.NET.Localization";
        public static string LocalRolesNamespace = "Promis.NET.Roles";
        public static string ProjectToGenerateResourceInto { get; set; } = "Promis.NET.DataAccess";
        public static string ProjectToGenerateRolesInto { get; set; } = "Promis.NET.DataAccess";
        public static string LocalResourceDirectory = "Localization";
        public static string LocalRolesDirectory = "Identity.MySql";
        public static string LocalResourceName = "LocalResources";
        public static string LocalRolesName = "LocalRoles";
        public static string LocalResourcesSummaryRegion = "en";

        public static void Main(params string[] args)
        {
            // resources
            Console.WriteLine("Generating {0} resource file in {1}\\{2}.", LocalResourceName, ProjectToGenerateResourceInto, LocalResourceDirectory);
            var regenerateResources = ConfigurationManager.AppSettings["RegenerateResources"] != null ? bool.Parse(ConfigurationManager.AppSettings["RegenerateResources"]) : false;
            if (!regenerateResources)
            {
                Console.WriteLine("Resources NOT regenerated. App Config file specified to not regenerate file.");
            }
            else
            {
                CreateResourceFile();
                Console.WriteLine("Finished generating resources class.");
            }

            // roles
            Console.WriteLine("Generating {0} roles file in {1}\\{2}.", LocalRolesName, ProjectToGenerateRolesInto, LocalRolesDirectory);
            var regenerateRoles = ConfigurationManager.AppSettings["RegenerateRoles"] != null ? bool.Parse(ConfigurationManager.AppSettings["RegenerateRoles"]) : false;
            if (!regenerateRoles)
            {
                Console.WriteLine("Roles NOT regenerated. App Config file specified to not regenerate file.");
            }
            else
            {
                CreateRolesFile();
                Console.WriteLine("Finished generating roles class.");
            }

            Console.ReadKey();
        }

        public static void CreateResourceFile()
        {
            var useLocalDatabase = ConfigurationManager.AppSettings["UseLocalDatabase"] != null ? bool.Parse(ConfigurationManager.AppSettings["UseLocalDatabase"]) : false;

            var builder = new ResourceBuilder();
            var provider = new ResourceProvider();
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..", "..", "..", ProjectToGenerateResourceInto, LocalResourceDirectory, LocalResourceName + ".cs");
            string outfile = builder.Create(provider, LocalResourcesNamespace, LocalResourceName, filePath, "en");
        }

        public static void CreateRolesFile()
        {
            var useLocalDatabase = ConfigurationManager.AppSettings["UseLocalDatabase"] != null ? bool.Parse(ConfigurationManager.AppSettings["UseLocalDatabase"]) : false;

            var builder = new RolesBuilder();
            var provider = new RolesProvider();
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..", "..", "..", ProjectToGenerateRolesInto, LocalRolesDirectory, LocalRolesName + ".cs");
            string outfile = builder.Create(provider, LocalRolesNamespace, LocalRolesName, filePath, "en");
        }
    }
}
