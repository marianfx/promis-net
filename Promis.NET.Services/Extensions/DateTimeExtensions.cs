﻿using System;
using System.Globalization;

namespace Promis.NET.Services.Extensions
{
    public static class DateTimeExtensions
    {
        public static string DayAsTwoDigitsString(this DateTime date)
        {
            var day = date.Day.ToString();
            return day.Length <= 1 ? "0" + day : day;
        }

        public static string MonthAsTwoDigitsString(this DateTime date)
        {
                var month = date.Month.ToString();
                return month.Length <= 1 ? "0" + month : month;
        }

        public static string YearAsString(this DateTime date)
        {
            return date.Year.ToString();
        }
        

        public static string WeekOfTheYearAsTwoDigitString(this DateTime date)
        {
                var week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek,
                    DayOfWeek.Monday).ToString();
                return week.Length <= 1 ? "0" + week : week;
        }

        public static string GetFlagForYearAndMonth(this DateTime date)
        {
            return "p_" + date.YearAsString() + "_" + date.MonthAsTwoDigitsString();
        }

        public static string GetStandardInvariantDateString(this DateTime date)
        {
            return date.DayAsTwoDigitsString() + "/" + date.MonthAsTwoDigitsString() + "/" + date.YearAsString();
        }
    }
}
