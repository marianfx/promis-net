﻿using Promis.NET.DataAccess.Identity.MySql;
using Promis.NET.Services.Models.Interfaces;
using Promis.NET.Utilities.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Promis.NET.Services.Models.Account
{
    public class PromisUser : BaseModel, IBaseModel
    {
        private RolesManager rolesManager = new RolesManager();

        public long UserId { get; set; }
        public string ProjectId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get
            {
                var name = "";
                name = !string.IsNullOrWhiteSpace(FirstName) ? FirstName : "";
                name += " " + (!string.IsNullOrWhiteSpace(LastName) ? LastName : "");
                name = name.Trim();
                name = !string.IsNullOrWhiteSpace(name) ? name : UserName;
                return name;
            }
        }
        public string Country { get; set; }
        public string Language { get; set; }
        public string Company { get; set; }
        public string MobilePhone { get; set; }
        public string OfficePhone { get; set; }

        public bool HasDoc { get; set; }
        public string PhotoUrl { get; set; }

        public string RolesModulesString { get; set; } = String.Empty;
        public string RolesActivitiesString { get; set; } = String.Empty;
        public string RolesResponsabilitiesString { get; set; } = String.Empty;

        public List<string> RolesModules
        {
            get
            {
                rolesManager.SetStrings(RolesModulesString);
                return rolesManager.RolesModulesLocal;
            }
        }

        public List<string> RolesActivities
        {
            get
            {
                rolesManager.SetStrings(rolesActivitiesString: RolesActivitiesString);
                return rolesManager.RolesActivitiesLocal;
            }
        }

        public List<string> RolesResponsabilities
        {
            get
            {
                rolesManager.SetStrings(rolesResponsabilitiesString: RolesResponsabilitiesString);
                return rolesManager.RolesResponsabilitiesLocal;
            }
        }

        public List<string> RolesAll
        {
            get
            {
                rolesManager.SetStrings(RolesModulesString, RolesActivitiesString, RolesResponsabilitiesString);
                return rolesManager.RolesAll;
            }
        }

        public PromisUser()
        {

        }

        public PromisUser(IdentityUser identityUser): this()
        {
            if (identityUser == null)
                return;

            UserId = identityUser.Id != null ? long.Parse(identityUser.Id) : 0;
            ProjectId = identityUser.ProjectId;
            UserName = identityUser.UserName;
            Email = identityUser.Email;
            Password = identityUser.Password;
            FirstName = identityUser.FirstName;
            LastName = identityUser.LastName;
            Country = identityUser.Country;
            Language = identityUser.Language;
            Company = identityUser.Company;
            MobilePhone = identityUser.MobilePhone;
            OfficePhone = identityUser.OfficePhone;
            HasDoc = identityUser.HasDoc;
            PhotoUrl = identityUser.PhotoUrl;
            RolesModulesString = identityUser.RolesModulesString;
            RolesActivitiesString = identityUser.RolesActivitiesString;
            RolesResponsabilitiesString = identityUser.RolesResponsabilitiesString;
        }

        public IdentityUser GetIdentityUser()
        {
            var identityUser = this;
            return new IdentityUser()
            {
                Id = identityUser.UserId.ToString(),
                ProjectId = identityUser.ProjectId,
                UserName = identityUser.UserName,
                Email = identityUser.Email,
                Password = identityUser.Password,
                FirstName = identityUser.FirstName,
                LastName = identityUser.LastName,
                Country = identityUser.Country,
                Language = identityUser.Language,
                Company = identityUser.Company,
                MobilePhone = identityUser.MobilePhone,
                OfficePhone = identityUser.OfficePhone,
                HasDoc = identityUser.HasDoc,
                PhotoUrl = identityUser.PhotoUrl,
                RolesModulesString = identityUser.RolesModulesString,
                RolesActivitiesString = identityUser.RolesActivitiesString,
                RolesResponsabilitiesString = identityUser.RolesResponsabilitiesString
        };
        }
    }
}
