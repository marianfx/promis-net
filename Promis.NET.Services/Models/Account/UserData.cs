﻿using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.Services.Models.Account
{
    public class UserData : BaseModel, IBaseModel
    {
        public string Name { get; set; } = string.Empty;
        public string Company { get; set; } = string.Empty;
        public string ImageUrl { get; set; } = string.Empty;
    }
}