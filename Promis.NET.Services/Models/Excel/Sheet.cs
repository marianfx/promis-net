﻿using Promis.NET.Utilities.Models;
using System.Collections.Generic;


namespace Promis.NET.Services.Models.Excel
{
    public class Sheet
    {
        public string SheetName { get; set; } = string.Empty;
        public List<string> Columns { get; set; } = new List<string>();
        public List<List<DataKeeper>> Rows { get; set; } = new List<List<DataKeeper>>();
        public short? SheetColor { get; set; } = null; 

        public Sheet()
        {

        }

        public Sheet(Sheet copy)
        {
            SheetName = copy.SheetName;
            SheetColor = copy.SheetColor;
            Columns = new List<string>();
            Columns.AddRange(copy.Columns);
            Rows = new List<List<DataKeeper>>();
            Rows.AddRange(copy.Rows);
        }
    }
}
