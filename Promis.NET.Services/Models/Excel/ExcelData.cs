﻿using Promis.NET.Services.Models.Interfaces;
using System.Collections.Generic;

namespace Promis.NET.Services.Models.Excel
{
    public class ExcelData : BaseModel, IBaseModel
    {
        public string FilePath { get; set; }
        public string WorkbookName { get; set; }
        public List<Sheet> SheetList { get; set; }
        
    }
}
