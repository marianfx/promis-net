﻿
using Promis.NET.Services.Models.Project;
using System.Collections.Generic;
using System.Linq;

namespace Promis.NET.Services.Models.PlanningAndProgress
{
    public class ProgressSteps
    {
        public List<SubActivityProgress> Subactivities { get; set; } = new List<SubActivityProgress>();

        public string SubAct { get; set; } = string.Empty;
        public string SubActDescription { get; set; } = string.Empty;
        public string[] C { get; set; } = new string[12];
        public string UM { get; set; } = string.Empty;
        public string Step { get; set; } = string.Empty;
        public string[] InspectionArray { get; set; } = new string[12];
        public string[] ProgressArray { get; set; } = new string[12];
        public string[] LpArray { get; set; } = new string[12];      
        public string[] Pos { get; set; } = new string[12];

        public ProgressSteps() { }

        public ProgressSteps(string subA, string description, string um,string[] c, string[] inspection, string step, string[] progr, string[] lp, string[] pos)
        {
            SubAct = subA;
            SubActDescription = description;
            C=c;
            UM = um;
            InspectionArray = inspection;
            Step = step;
            Pos = pos;
            ProgressArray = progr;
            LpArray = lp;
        }

        public ProgressSteps(IEnumerable<SubActivityProgress> subactivities)
        {
            SetSubactivities(subactivities);
        }

        public void SetSubactivities(IEnumerable<SubActivityProgress> subactivities)
        {
            Subactivities = subactivities.ToList();
            var lastStep = false;

            for (var i = 0; i < 12; i++)
            {
                InspectionArray[i] = "";
                ProgressArray[i] = "";
                LpArray[i] = "";
                Pos[i] = "";
                C[i] = "";
                if (i < Subactivities.Count)
                {
                    var subAct = Subactivities[i];
                    InspectionArray[i] = subAct.Inspection;
                    ProgressArray[i] = subAct.Progress.ToString();
                    LpArray[i] = subAct.Lp;
                    Pos[i] = subAct.Pos;
                    C[i] = subAct.C;
                }

                if (i == Subactivities.Count - 1 && !lastStep)
                {
                    lastStep = true;
                }
            }

        }

    }

}

