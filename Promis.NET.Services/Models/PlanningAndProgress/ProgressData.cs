﻿

namespace Promis.NET.Services.Models.PlanningAndProgress
{
    public class ProgressData
    {

        public string Tag { get; set; } = string.Empty;
        public string S1 { get; set; } = string.Empty;
        public string S2 { get; set; } = string.Empty;
        public string S3 { get; set; } = string.Empty;
        public string S4 { get; set; } = string.Empty;
        public string S5 { get; set; } = string.Empty;
        public string S6 { get; set; } = string.Empty;
        public string S7 { get; set; } = string.Empty;
        public string S8 { get; set; } = string.Empty;
        public string S9 { get; set; } = string.Empty;
        public string S10 { get; set; } = string.Empty;
        public string S11 { get; set; } = string.Empty;
        public string S12 { get; set; } = string.Empty;

        public string Wbs { get; set; } = "000";
        public float Pr { get; set; } = 0.0f;
        public string SubAct { get; set; } = string.Empty;
        public string Mhrs { get; set; } = string.Empty;
        public string Qty { get; set; } = string.Empty;
        public string DataTable { get; set; } = string.Empty;
        public string Equiv { get; set; } = string.Empty;

        public ProgressData() { }

        public ProgressData(string tag, string wbs, float pr,string subact,string mhrs, string qty, string datatable, string equiv,
                            string s1, string s2, string s3, string s4, string s5, string s6, string s7, string s8, string s9, string s10, string s11, string s12)
        {
            Tag = tag;
            Wbs = wbs;
            Pr = pr;
            SubAct = subact;
            Mhrs = mhrs;
            Qty = qty;
            DataTable = datatable;
            Equiv = equiv;
            S1 = s1;
            S2 = s2;
            S3 = s3;
            S4 = s4;
            S5 = s5;
            S6 = s1;
            S7 = s7;
            S8 = s8;
            S9 = s9;
            S10 = s10;
            S11 = s11;
            S12 = s12;
        }       
    }           
}

