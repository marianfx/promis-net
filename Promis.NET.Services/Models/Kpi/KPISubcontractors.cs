﻿
using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.Services.Models.Kpi
{
    public class KPISubcontractors : Classic, IBaseModel
    {
        public KPISubcontractors() : base() { }

        public KPISubcontractors(string code, string subcontractor) : base(code, subcontractor) { }
    
    }
}
