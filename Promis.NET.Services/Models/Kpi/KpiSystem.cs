﻿
using Promis.Web.DataAccess.Entities.ProjectEntities;

namespace Promis.NET.Services.Models.Kpi
{
    public class KpiSystem
    {
        public string Tag { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Wbs { get; set; } = "000";
        public string System { get; set; } = string.Empty;
        public string Pid { get; set; } = string.Empty;

        public KpiSystem() { }

        public KpiSystem(string tag, string description, string system)
        {
            Tag = tag;
            Description = description;
            System = system;
        }

        public KpiSystem(datatable_common data)
        {
            if (data == null)
                return;

            Tag = data.TAG;
            Description = data.DESCRIPTION;
            Wbs = data.WBS;
            System = data.SYSTEM;
            Pid = data.DWG;
        }
    }
}
