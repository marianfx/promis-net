﻿using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.Services.Models.Interfaces;
using System;

namespace Promis.NET.Services.Models.Kpi
{
    public class KpiData : Classic, IBaseModel
    {
        public string CategoryCode { get; set; } = "";
        public string CategoryDescription { get; set; } = "";
        public double ClosedCount { get; set; } = 0;
        public double TotalCount { get; set; } = 0;
        public double Percent { get; set; } = 0;
        public bool IsFavorite { get; set; } = false;

        public KpiData(): base() { }

        public KpiData(string code, string description): base(code, description) { }

        public KpiData(ftc_container baseData)
        {
            if (baseData == null)
                return;

            CategoryCode = baseData.code;
            CategoryDescription = baseData.description;
            Code = baseData.kpi;
            Description = baseData.kpi_description;
            IsFavorite = !string.IsNullOrWhiteSpace(baseData.fav);
            ClosedCount = baseData.closed;
            TotalCount = baseData.tot;
            if (TotalCount > 0)
                Percent = Math.Round(ClosedCount / TotalCount * 100, 2);
        }
    }
}
