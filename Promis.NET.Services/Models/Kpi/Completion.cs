﻿using Promis.NET.Services.Models.Interfaces;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using System;
using System.Globalization;

namespace Promis.NET.Services.Models.Kpi
{
    public class CompletionStrings
    {
        public string Code { get; set; } = string.Empty;

        public string MechanicalSent { get; set; } = string.Empty;
        public string MechanicalSigned { get; set; } = string.Empty;
        public string MechanicalDoc { get; set; } = string.Empty;

        public string PrecomSent { get; set; } = string.Empty;
        public string PrecomSigned { get; set; } = string.Empty;
        public string PrecomDoc { get; set; } = string.Empty;

        public string ComSent { get; set; } = string.Empty;
        public string ComSigned { get; set; } = string.Empty;
        public string ComDoc { get; set; } = string.Empty;

        public string ReadyStartupSent { get; set; } = string.Empty;
        public string ReadyStartupSigned { get; set; } = string.Empty;
        public string ReadyStartupDoc { get; set; } = string.Empty;

        public string TestRunSent { get; set; } = string.Empty;
        public string TestRunSigned { get; set; } = string.Empty;
        public string TestRunDoc { get; set; } = string.Empty;

        public string GuaranteeSent { get; set; } = string.Empty;
        public string GuaranteeSigned { get; set; } = string.Empty;
        public string GuaranteeDoc { get; set; } = string.Empty;

        public string PacSent { get; set; } = string.Empty;
        public string PacSigned { get; set; } = string.Empty;
        public string PacDoc { get; set; } = string.Empty;
    }

    public class Completion: BaseModel, IBaseModel
    {
        public string Code { get; set; } = string.Empty;

        public DateTime? MechanicalSent { get; set; } = null;
        public DateTime? MechanicalSigned { get; set; } = null;
        public string MechanicalDoc { get; set; } = string.Empty;

        public DateTime? PrecomSent { get; set; } = null;
        public DateTime? PrecomSigned { get; set; } = null;
        public string PrecomDoc { get; set; } = string.Empty;

        public DateTime? ComSent { get; set; } = null;
        public DateTime? ComSigned { get; set; } = null;
        public string ComDoc { get; set; } = string.Empty;

        public DateTime? ReadyStartupSent { get; set; } = null;
        public DateTime? ReadyStartupSigned { get; set; } = null;
        public string ReadyStartupDoc { get; set; } = string.Empty;

        public DateTime? TestRunSent { get; set; } = null;
        public DateTime? TestRunSigned { get; set; } = null;
        public string TestRunDoc { get; set; } = string.Empty;

        public DateTime? GuaranteeSent { get; set; } = null;
        public DateTime? GuaranteeSigned { get; set; } = null;
        public string GuaranteeDoc { get; set; } = string.Empty;

        public DateTime? PacSent { get; set; } = null;
        public DateTime? PacSigned { get; set; } = null;
        public string PacDoc { get; set; } = string.Empty;

        public Completion() { }

        public Completion(completion obj)
        {
            if (obj == null)
                return;

            Code = obj.SYSTEM;

            if(!string.IsNullOrWhiteSpace(obj.MC_SENT))
                MechanicalSent = DateTime.ParseExact(obj.MC_SENT, @"dd\/MM\/yyyy", CultureInfo.InvariantCulture);
            if (!string.IsNullOrWhiteSpace(obj.MC_SIGN))
                MechanicalSigned = DateTime.ParseExact(obj.MC_SIGN, @"dd\/MM\/yyyy", CultureInfo.InvariantCulture);
            MechanicalDoc = obj.MC_DOC;

            if(!string.IsNullOrWhiteSpace(obj.PR_SENT))
                PrecomSent = DateTime.ParseExact(obj.PR_SENT, @"dd\/MM\/yyyy", CultureInfo.InvariantCulture);
            if (!string.IsNullOrWhiteSpace(obj.PR_SIGN))
                PrecomSigned = DateTime.ParseExact(obj.PR_SIGN, @"dd\/MM\/yyyy", CultureInfo.InvariantCulture);
            PrecomDoc = obj.PR_DOC;

            if(!string.IsNullOrWhiteSpace(obj.CO_SENT))
                ComSent = DateTime.ParseExact(obj.CO_SENT, @"dd\/MM\/yyyy", CultureInfo.InvariantCulture);
            if (!string.IsNullOrWhiteSpace(obj.CO_SIGN))
                ComSigned = DateTime.ParseExact(obj.CO_SIGN, @"dd\/MM\/yyyy", CultureInfo.InvariantCulture);
            ComDoc = obj.CO_DOC;

            if (!string.IsNullOrWhiteSpace(obj.SU_SENT))
                ReadyStartupSent = DateTime.ParseExact(obj.SU_SENT, @"dd\/MM\/yyyy", CultureInfo.InvariantCulture);
            if (!string.IsNullOrWhiteSpace(obj.SU_SIGN))
                ReadyStartupSigned = DateTime.ParseExact(obj.SU_SIGN, @"dd\/MM\/yyyy", CultureInfo.InvariantCulture);
            ReadyStartupDoc = obj.SU_DOC;

            if (!string.IsNullOrWhiteSpace(obj.TR_SENT))
                TestRunSent = DateTime.ParseExact(obj.TR_SENT, @"dd\/MM\/yyyy", CultureInfo.InvariantCulture);
            if (!string.IsNullOrWhiteSpace(obj.TR_SIGN))
                TestRunSigned = DateTime.ParseExact(obj.TR_SIGN, @"dd\/MM\/yyyy", CultureInfo.InvariantCulture);
            TestRunDoc = obj.TR_DOC;

            if (!string.IsNullOrWhiteSpace(obj.GA_SENT))
                GuaranteeSent = DateTime.ParseExact(obj.GA_SENT, @"dd\/MM\/yyyy", CultureInfo.InvariantCulture);
            if (!string.IsNullOrWhiteSpace(obj.GA_SIGN))
                GuaranteeSigned = DateTime.ParseExact(obj.GA_SIGN, @"dd\/MM\/yyyy", CultureInfo.InvariantCulture);
            GuaranteeDoc = obj.GA_DOC;

            if (!string.IsNullOrWhiteSpace(obj.PAC_SENT))
                PacSent = DateTime.ParseExact(obj.PAC_SENT, @"dd\/MM\/yyyy", CultureInfo.InvariantCulture);
            if (!string.IsNullOrWhiteSpace(obj.PAC_SIGN))
                PacSigned = DateTime.ParseExact(obj.PAC_SIGN, @"dd\/MM\/yyyy", CultureInfo.InvariantCulture);
            PacDoc = obj.PAC_DOC;
        }

        public CompletionStrings GetAsStrings()
        {
            return new CompletionStrings
            {
                Code = Code,
                MechanicalSent = MechanicalSent.HasValue ? MechanicalSent.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : null,
                MechanicalSigned = MechanicalSigned.HasValue ? MechanicalSigned.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : null,
                MechanicalDoc = MechanicalDoc,

                PrecomSent = PrecomSent.HasValue ? PrecomSent.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : null,
                PrecomSigned = PrecomSigned.HasValue ? PrecomSigned.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : null,
                PrecomDoc = PrecomDoc,

                ComSent = ComSent.HasValue ? ComSent.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : null,
                ComSigned = ComSigned.HasValue ? ComSigned.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : null,
                ComDoc = ComDoc,

                ReadyStartupSent = ReadyStartupSent.HasValue ? ReadyStartupSent.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : null,
                ReadyStartupSigned = ReadyStartupSigned.HasValue ? ReadyStartupSigned.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : null,
                ReadyStartupDoc = ReadyStartupDoc,

                TestRunSent = TestRunSent.HasValue ? TestRunSent.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : null,
                TestRunSigned = TestRunSigned.HasValue ? TestRunSigned.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : null,
                TestRunDoc = TestRunDoc,

                GuaranteeSent = GuaranteeSent.HasValue ? GuaranteeSent.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : null,
                GuaranteeSigned = GuaranteeSigned.HasValue ? GuaranteeSigned.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : null,
                GuaranteeDoc = GuaranteeDoc,

                PacSent = PacSent.HasValue ? PacSent.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : null,
                PacSigned = PacSigned.HasValue ? PacSigned.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : null,
                PacDoc = PacDoc,
            };
        }

        public completion GetCompletion()
        {
            return new completion()
            {
                SYSTEM = Code,

                MC_SENT = MechanicalSent.HasValue ? MechanicalSent.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : "",
                MC_SIGN = MechanicalSigned.HasValue ? MechanicalSigned.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : "",
                MC_DOC = MechanicalDoc,

                PR_SENT = PrecomSent.HasValue ? PrecomSent.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : "",
                PR_SIGN = PrecomSigned.HasValue ? PrecomSigned.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : "",
                PR_DOC = PrecomDoc,

                CO_SENT = ComSent.HasValue ? ComSent.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : "",
                CO_SIGN = ComSigned.HasValue ? ComSigned.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : "",
                CO_DOC = ComDoc,

                SU_SENT = ReadyStartupSent.HasValue ? ReadyStartupSent.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : "",
                SU_SIGN = ReadyStartupSigned.HasValue ? ReadyStartupSigned.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : "",
                SU_DOC = ReadyStartupDoc,

                TR_SENT = TestRunSent.HasValue ? TestRunSent.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : "",
                TR_SIGN = TestRunSigned.HasValue ? TestRunSigned.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : "",
                TR_DOC = TestRunDoc,

                GA_SENT = GuaranteeSent.HasValue ? GuaranteeSent.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : "",
                GA_SIGN = GuaranteeSigned.HasValue ? GuaranteeSigned.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : "",
                GA_DOC = GuaranteeDoc,

                PAC_SENT = PacSent.HasValue ? PacSent.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : "",
                PAC_SIGN = PacSigned.HasValue ? PacSigned.Value.ToString(@"dd\/MM\/yyyy", CultureInfo.InvariantCulture) : "",
                PAC_DOC = PacDoc,
            };
        }
    }
}
