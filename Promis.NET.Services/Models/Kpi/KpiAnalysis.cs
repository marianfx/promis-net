﻿
namespace Promis.NET.Services.Models.Kpi
{
    public class KpiAnalysis
    {
        public double TotalQty { get; set; } = 0.0;
        public double LastQty { get; set; } = 0.0;
        public double AchievedProgress { get; set; } = 0.0;
        public string PlannedStart { get; set; } = string.Empty;
        public string PlannedEnd { get; set; } = string.Empty;
        public string DelayedProgress { get; set; } = string.Empty;
        public string DelayedCompletion { get; set; } = string.Empty;

        public string PlannedProductivity { get; set; } = string.Empty;
        public string ActualProductivity { get; set; } =string.Empty;
        public string WeeklyQuantity { get; set; } = string.Empty;
        public string AditionalDelay { get; set; } = string.Empty;
    }
}
