﻿
using Promis.NET.Services.Models.Interfaces;
using System.Collections.Generic;

namespace Promis.NET.Services.Models.Kpi
{
   public class KPIsChartData : BaseModel, IBaseModel
    {
        public List<double?> ChartBars { get; set; } = new List<double?>();
        public List<double?> ChartRit { get; set; } = new List<double?>();
        public List<double?> ChartWkty { get; set; } = new List<double?>();
        public List<double?> ChartPr { get; set; } = new List<double?>();
        public List<double> ChartWorkW { get; set; } = new List<double>();
        public List<List<string>> ChartBarsLabels { get; set; } = new List<List<string>>();
        public List<List<string>> ChartRitLabels { get; set; } = new List<List<string>>();
    }     
}
