﻿using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.Services.Models.Kpi
{
    public class KpiCategory: Classic, IBaseModel
    {
        public KpiCategory(): base() { }

        public KpiCategory(string code, string description): base(code, description) { }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
