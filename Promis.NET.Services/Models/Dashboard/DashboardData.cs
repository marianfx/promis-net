﻿using Promis.NET.Services.Models.Interfaces;
using System.Collections.Generic;

namespace Promis.NET.Services.Models.Dashboard
{
    public class DashboardData : BaseModel, IBaseModel
    {
        public int PunchlistCount { get; set; } = 0;
        public int PunchlistDoneCount { get; set; } = 0;
        public double ClearancePercentage { get; set; } = 0;

        public List<List<string>> ProgressChartLabels { get; set; } = new List<List<string>>();
        public List<double> ProgressChartValuesPlanned { get; set; } = new List<double>();
        public List<double> ProgressChartValuesActual { get; set; } = new List<double>();
        public List<double> ProgressChartValuesCertified { get; set; } = new List<double>();

        public List<double> ProgressMonthlyChartValuesActual { get; set; } = new List<double>();
        public List<double> ProgressMonthlyChartValuesDelay { get; set; } = new List<double>();

        public double TrendingChartPlannedPercent { get; set; } = 0;
        public double TrendingChartPlannedPercentWeekly { get; set; } = 0;
        public double TrendingChartPlannedPercentMonthly { get; set; } = 0;
        public double TrendingChartActualPercent { get; set; } = 0;
        public double TrendingChartActualPercentWeekly { get; set; } = 0;
        public double TrendingChartActualPercentMonthly { get; set; } = 0;
        public double TrendingChartCertifiedPercent { get; set; } = 0;
        public double TrendingChartCertifiedPercentWeekly { get; set; } = 0;
        public double TrendingChartCertifiedPercentMonthly { get; set; } = 0;

        public List<string> ActivitiesCodes { get; set; } = new List<string>();
        public List<double> ActivitiesPlannedData { get; set; } = new List<double>();
        public List<double> ActivitiesActualData { get; set; } = new List<double>();
        public List<ActivityWithProgress> ActivitiesWithProgresses { get; set; } = new List<ActivityWithProgress>();

        public List<string> OutstandingChartFlags { get; set; } = new List<string>();
        public List<string> OutstandingChartFlagDescriptions { get; set; } = new List<string>();
        public List<double> OutstandingChartCounts { get; set; } = new List<double>();
    }
}