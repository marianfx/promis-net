﻿using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.Services.Models.Dashboard
{
    public class Activity : BaseModel, IBaseModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
