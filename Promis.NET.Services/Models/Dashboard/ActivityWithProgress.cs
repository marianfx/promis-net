﻿namespace Promis.NET.Services.Models.Dashboard
{
    public class ActivityWithProgress: Activity
    {
        public double ProgressActual { get; set; }
        public double ProgressPlanned { get; set; }
    }
}
