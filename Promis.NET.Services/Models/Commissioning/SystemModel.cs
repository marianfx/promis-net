﻿using Promis.NET.Services.Models.Interfaces;
using Promis.Web.DataAccess.Entities.ProjectEntities;

namespace Promis.NET.Services.Models.Commissioning
{
    public class SystemModel : BaseModel, IBaseModel
    {
        public string Code { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;

        public SystemModel()
        {

        }

        public SystemModel(system_base entity)
        {
            Code = entity.SYSTEM;
            Description = entity.SYSTEM_DESCRIPTION;
        }
    }
}
