﻿
using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.Services.Models.Commissioning
{
    public class DatatablesInfo: Datatables, IBaseModel
    {
        public double Count1 { get; set; } = 0;
        public double Count2 { get; set; } = 0;

        public DatatablesInfo()
        {

        }

        public DatatablesInfo(string code,string description, double count1, double count2):base(code,description)
        {
            Count1 = count1;
            Count2 = count2;
        }

        public DatatablesInfo(double count1, double count2)
        {
            Count1 = count1;
            Count2 = count2;
        }

    }
}
