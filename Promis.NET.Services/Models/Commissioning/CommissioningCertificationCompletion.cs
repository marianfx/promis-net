﻿using Promis.NET.Services.Models.Interfaces;
namespace Promis.NET.Services.Models.Commissioning
{
    public class CommissioningCertificationCompletion : Datatables,IBaseModel
    {
        public double Count1Precommissioning { get; set; } = 0;
        public double Count2Precommissioning { get; set; } = 0;
        public double Count1Commissioning { get; set; } = 0;
        public double Count2Commissioning { get; set; } = 0;
        public double CommissioningProgress { get; set; } = 0;
        public double PrecomProgress { get; set; } = 0;


        public CommissioningCertificationCompletion()
        {

        }

        public CommissioningCertificationCompletion(double count1, double count2, double count3, double count4, double precProgress, double commProgress, string dataTable, string description):base(dataTable, description)
        {
            Count1Precommissioning = count1;
            Count2Precommissioning = count2;
            Count1Commissioning = count3;
            Count2Commissioning = count4;
            CommissioningProgress = commProgress;
            PrecomProgress = precProgress;          
        }
    }
}
