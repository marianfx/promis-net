﻿
using Promis.NET.Services.Models.Interfaces;
using System.Collections.Generic;

namespace Promis.NET.Services.Models.Commissioning
{
    public class ChartsData : BaseModel, IBaseModel
    {
        public List<List<string>> ProgressStatusChartLabels { get; set; } = new List<List<string>>();
        public List<double> ProgressStatusChartValuesPlanned { get; set; } = new List<double>();
        public List<double> ProgressStatusChartValuesActual { get; set; } = new List<double>();
    }
}
