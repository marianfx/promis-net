﻿using Promis.NET.Services.Models.Interfaces;
using System.Collections.Generic;

namespace Promis.NET.Services.Models.Commissioning
{
    public class CommissioningSysProgress: IBaseModel
    {
        public string System { get; set; } = string.Empty;
        public string Datatable { get; set; } = string.Empty;
        public List<string> SelectedRows { get; set; }

        public CommissioningSysProgress()
        {
            this.SelectedRows = new List<string>();
        }

    }
}
