﻿using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.Services.Models.Commissioning
{
    public class ActivityTag : IBaseModel
    {
        public string Tag { get; set; } = string.Empty;
        public string Activity { get; set; } = string.Empty;
        public string Id { get; set; } = string.Empty;


        public ActivityTag(activity_tag_multiple entity)
        {
            Tag = entity.tag;
            Activity = entity.activity;
            Id = entity.id;
        }
    }
}
