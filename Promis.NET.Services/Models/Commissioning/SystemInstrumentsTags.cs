﻿using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.Services.Models.Commissioning
{
    public class SystemInstrumentsTags : IBaseModel
    {
        public string Comp { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Act { get; set; } = string.Empty;
        public string Type { get; set; } = string.Empty;
        public string System { get; set; } = string.Empty;
        public string LastStep { get; set; } = string.Empty;
        public string NextStep { get; set; } = string.Empty;
        public string Progress { get; set; } = string.Empty;

        public SystemInstrumentsTags(system_instruments_tags entity)
        {
            Comp = entity.comp;
            Description = entity.description;
            Act = entity.act;
            Type = entity.type;
            System = entity.system;
            LastStep = entity.laststep;
            NextStep = entity.nextstep;
            Progress = entity.progress.ToString();
        }

        public SystemInstrumentsTags()
        {

        }
        //public SystemInstrumentsTags(string comp, string description, string act, string type, string system, string last, string next, string progress)
        //{
        //    Comp = comp;
        //    Description = description;
        //    Act = act;
        //    Type = type;
        //    System = system;
        //    LastStep = last;
        //    NextStep = next;
        //    Progress = progress;
        //}
    }
}
