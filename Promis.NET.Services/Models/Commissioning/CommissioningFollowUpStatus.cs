﻿namespace Promis.NET.Services.Models.Commissioning
{
    public class CommissioningFollowUpStatus
    {
        public string Description { get; set; } = string.Empty;
        public bool result { get; set; } = false;
        public string inspection { get; set; } = string.Empty;
        public string ord { get; set; } = string.Empty;

    }
}
