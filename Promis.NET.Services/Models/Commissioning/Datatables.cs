﻿using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.Services.Models.Commissioning
{
    public class Datatables : BaseModel, IBaseModel
    {
        public string Datatable { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;

        public Datatables()
        {

        }

        public Datatables(tabletab_def_base entity)
        {
            Datatable = entity.datatable;
            Description = entity.description;
        }
        public Datatables(string datatable, string description)
        {
            Datatable = datatable;
            Description = description;
        }
    }
}
