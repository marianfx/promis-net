﻿using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.DataAccess.Entities.PromisWeb;
using Promis.NET.Services.Models.Interfaces;
using Promis.Web.DataAccess.Entities.ProjectEntities;

namespace Promis.NET.Services.Models.Project
{
    public class Activity: ActivityBase, IBaseModel
    {
        public bool Checked { get; set; } = false;
        public int Budget { get; set; } = 0;

        public Activity()
        {

        }

        public Activity(work_disc_budget entity)
        {
            Code = entity.cod;
            Description = entity.descr;
            Checked = entity.flag == "*" ? true : false;
            Budget = entity.budget ?? 0;
        }

        public Activity(work_def_base entity)
        {
            if (entity == null)
                return;

            if(typeof(work_def_subact) == entity.GetType())
            {
                var subActivityEntity = (work_def_subact)entity;
                Code = subActivityEntity.SUB_ACT;
                Description = subActivityEntity.SUB_ACT_D;
            }
            else if(typeof(work_def_budget) == entity.GetType())
            {
                var subEntity = (work_def_budget)entity;
                Code = subEntity.ACT;
                Description = subEntity.ACT_D;
                Budget = subEntity.mhrs;
            }
            else
            {
                Code = entity.ACT;
                Description = entity.ACT_D;
            }
        }
        
        public override int GetHashCode()
        {
            return "ACT".GetHashCode() + Code.GetHashCode();
        }
    }
}
