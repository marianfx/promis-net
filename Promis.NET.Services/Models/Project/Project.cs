﻿using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.PromisWeb;

namespace Promis.NET.Services.Models.Project
{
    public class Project
    {
        public long Id { get; set; } = 0;
        public string Code { get; set; } = string.Empty;
        public string Title { get; set; } = string.Empty;
        public string Client { get; set; } = string.Empty;
        public string Location { get; set; } = string.Empty;
        public string LocationCoordinates { get; set; } = string.Empty;
        public string FirstWorkingDay { get; set; } = string.Empty;
        public string CutOffDay { get; set; } = string.Empty;
        public string StartDate { get; set; } = string.Empty;
        public int StartYear => Global.YearFromString(StartDate);
        public int StartMonth => Global.MonthFromString(StartDate);
        public string EndDate { get; set; } = string.Empty;
        public int EndYear => Global.YearFromString(EndDate);
        public int EndMonth => Global.MonthFromString(EndDate);
        public int MonthsCount => Global.DifferenceInMonths(StartMonth, StartYear, EndMonth, EndYear);
        public string LogoUrl { get; set; } = string.Empty;

        public Project()
        {
        }

        public Project(project project)
        {
            if (project == null)
                return;

            Id = project.id;
            Code = project.proj;
            Title = project.descr;
            Client = project.client;
            Location = project.location;
            LocationCoordinates = project.geoloc;
            FirstWorkingDay = project.wd;
            CutOffDay = project.c_off;
            StartDate = project.s_date;
            EndDate = project.e_date;
            LogoUrl = project.logo;
        }

        public project GetProject()
        {
            return new project
            {
                id = Id,
                proj = Code,
                descr = Title,
                client = Client,
                location = Location,
                geoloc = LocationCoordinates,
                wd = FirstWorkingDay,
                c_off = CutOffDay,
                s_date = StartDate,
                e_date = EndDate,
                logo = LogoUrl
            };
        }
    }
}
