﻿using Promis.NET.Services.Models.Interfaces;
using Promis.Web.DataAccess.Entities.PromisWeb;

namespace Promis.NET.Services.Models.Project
{
    public class ProjectData : BaseModel, IBaseModel
    {
        public string Name { get; set; } = string.Empty;
        public string Location { get; set; } = string.Empty;
        public string LocationCoordinates { get; set; } = string.Empty;
        public string LogoUrl { get; set; } = string.Empty;

        public ProjectData()
        {

        }

        public ProjectData(project project)
        {
            if (project == null)
                return;

            Name = project.descr;
            Location = project.location;
            LocationCoordinates = project.geoloc;
            LogoUrl = project.logo;
        }
    }
}