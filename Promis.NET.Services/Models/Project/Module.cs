﻿using Promis.Web.DataAccess.Entities.PromisWeb;
using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.Services.Models.Project
{
    public class Module: BaseModel, IBaseModel
    {
        public string Code { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Class { get; set; } = string.Empty;
        public long Id { get; set; } = 0;

        public Module()
        {

        }

        public Module(module module)
        {
            Code = module.code;
            Description = module.description;
            Class = module.classCol;
            Id = module.id;
        }
    }
}
