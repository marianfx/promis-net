﻿using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using System.Collections.Generic;

namespace Promis.NET.Services.Models.Project
{
    public enum ActivityPlanningEntityTypes { B, P, R, A};
    public enum ActivityUpdateType { BaselineAndPlanning, RescheduledCurves};

    public class ActivityPlanned: Activity
    {
        public Project Project { get; set; } = new Project();

        public string StartDateBaseline { get; set; } = string.Empty;
        public int StartYearBaseline => Global.YearFromString(StartDateBaseline);
        public int StartMonthBaseline => Global.MonthFromString(StartDateBaseline);
        public string EndDateBaseline { get; set; } = string.Empty;
        public int EndYearBaseline => Global.YearFromString(EndDateBaseline);
        public int EndMonthBaseline => Global.MonthFromString(EndDateBaseline);
        public int MonthsCountBaseline => Global.DifferenceInMonths(StartMonthBaseline, StartYearBaseline, EndMonthBaseline, EndYearBaseline);

        public string StartDatePlanned { get; set; } = string.Empty;
        public int StartYearPlanned => Global.YearFromString(StartDatePlanned);
        public int StartMonthPlanned => Global.MonthFromString(StartDatePlanned);
        public string EndDatePlanned { get; set; } = string.Empty;
        public int EndYearPlanned => Global.YearFromString(EndDatePlanned);
        public int EndMonthPlanned => Global.MonthFromString(EndDatePlanned);
        public int MonthsCountPlanned => Global.DifferenceInMonths(StartMonthPlanned, StartYearPlanned, EndMonthPlanned, EndYearPlanned);

        public string StartDateR { get; set; } = string.Empty;
        public int StartYearR => Global.YearFromString(StartDateR);
        public int StartMonthR => Global.MonthFromString(StartDateR);
        public string EndDateR { get; set; } = string.Empty;
        public int EndYearR => Global.YearFromString(EndDateR);
        public int EndMonthR => Global.MonthFromString(EndDateR);
        public int MonthsCountR => Global.DifferenceInMonths(StartMonthR, StartYearR, EndMonthR, EndYearR);
        
        public string StartDateA { get; set; } = string.Empty;
        public int StartYearA => Global.YearFromString(StartDateA);
        public int StartMonthA => Global.MonthFromString(StartDateA);
        public string EndDateA { get; set; } = string.Empty;
        public int EndYearA => Global.YearFromString(EndDateA);
        public int EndMonthA => Global.MonthFromString(EndDateA);
        public int MonthsCountA => Global.DifferenceInMonths(StartMonthA, StartYearA, EndMonthA, EndYearA);

        public List<double> BaselineProgress { get; set; } = new List<double>();
        public List<double> PlannedProgress { get; set; } = new List<double>();
        public List<double> RProgress { get; set; } = new List<double>();
        public List<double> AProgress { get; set; } = new List<double>();

        public bool HasBaselineSet { get; set; } = false;
        public bool HasPlannedSet { get; set; } = false;
        public bool HasRSet { get; set; } = false;
        public bool HasASet { get; set; } = false;

        public ActivityPlanned() { }

        public ActivityPlanned(work_planning entityBase)
        {
            if (entityBase == null)
                return;

            Code = entityBase.act;
            Description = entityBase.act_d;
            Budget = entityBase.mhrs ?? 0;
        }

        public void SetEntity(work_planning entity)
        {
            if (entity == null)
                return;

            var thisBudget = entity.mhrs ?? (Budget != 0 ? Budget : 0);
            Budget = thisBudget > Budget ? thisBudget : Budget;
            var entityDbType = entity.type?.ToLower().Trim();

            switch (entityDbType)
            {
                case "b":
                    StartDateBaseline = entity.start_date;
                    EndDateBaseline = entity.end_date;
                    HasBaselineSet = true;
                    break;
                case "p":
                    StartDatePlanned = entity.start_date;
                    EndDatePlanned = entity.end_date;
                    HasPlannedSet = true;
                    break;
                case "r":
                    StartDateR = entity.start_date;
                    EndDateR = entity.end_date;
                    HasRSet = true;
                    break;
                case "a":
                    StartDateA = entity.start_date;
                    EndDateA = entity.end_date;
                    HasASet = true;
                    break;
                default:
                    break;
            }
        }

        public work_planning GetPlanningEntityBasic()
        {
            return new work_planning()
            {
                act = Code,
                act_d = Description,
                mhrs = Budget
            };
        }

        public work_planning GetPlanningEntity(ActivityPlanningEntityTypes type)
        {
            var ent = GetPlanningEntityBasic();
            ent.type = type.ToString();

            switch (type)
            {
                case ActivityPlanningEntityTypes.B:
                    ent.start_date = StartDateBaseline;
                    ent.end_date = EndDateBaseline;
                    break;
                case ActivityPlanningEntityTypes.P:
                    ent.start_date = StartDatePlanned;
                    ent.end_date = EndDatePlanned;
                    break;
                case ActivityPlanningEntityTypes.R:
                    ent.start_date = StartDateR;
                    ent.end_date = EndDateR;
                    break;
                case ActivityPlanningEntityTypes.A:
                    ent.start_date = StartDateA;
                    ent.end_date = EndDateA;
                    break;
                default:
                    break;
            }
            return ent;
        }
    }
}
