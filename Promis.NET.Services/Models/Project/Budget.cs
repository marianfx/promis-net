﻿using Promis.NET.DataAccess.Entities.PromisWeb;
using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.Services.Models.Project
{
    public class Budget : BaseModel, IBaseModel
    {
        public int Available { get; set; } = 0;
        public int Planned { get; set; } = 0;
        public int Balance => Available - Planned;

        public Budget()
        {
        }
    }
}
