﻿using Promis.NET.Services.Models.Interfaces;
using Promis.Web.DataAccess.Entities.ProjectEntities;

namespace Promis.NET.Services.Models.Project
{
    public class ActivityBase: BaseModel, IBaseModel
    {
        public string Code { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;

        public ActivityBase()
        {

        }

        public ActivityBase(work_def_base entity)
        {
            if (entity == null)
                return;

            Code = entity.ACT;
            Description = entity.ACT_D;
        }

        public ActivityBase(string code, string description)
        {
            Code = code;
            Description = description;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != this.GetType())
                return false;

            var thisObj = (ActivityBase)obj;
            return thisObj.Code == this.Code;
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }
    }
}
