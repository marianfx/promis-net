﻿using Promis.NET.Services.Models.Interfaces;
using Promis.Web.DataAccess.Entities.ProjectEntities;

namespace Promis.NET.Services.Models.Project
{
    public class SubActivityProgress : ActivityBase, IBaseModel
    {
        public string Inspection { get; set; } = string.Empty;
        public double Progress { get; set; } = 0;
        public string Lp { get; set; } = string.Empty;

        //Added
        public string Pos { get; set; } = string.Empty;
        public string C { get; set; } = string.Empty;


        public SubActivityProgress()
        {

        }

        public SubActivityProgress(work_def_base entity): base(entity)
        {
            if (entity == null)
                return;

            if(typeof(work_def) == entity.GetType())
            {
                var subEntity = (work_def)entity;
                Inspection = subEntity.INSPECTION;
                Lp = subEntity.LP;
                Progress = double.Parse(subEntity.PROGR);
                Pos = subEntity.POS;//added new
                C = subEntity.C;//added new
            }
        }
        
        
        public override int GetHashCode()
        {
            return "SUBPR".GetHashCode() + Code.GetHashCode();
        }
    }
}
