﻿using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.Services.Models.Interfaces;
using Promis.Web.DataAccess.Entities.ProjectEntities;

namespace Promis.NET.Services.Models.Project
{
    public class SubActivity: ActivityBase, IBaseModel
    {
        public string Um { get; set; } = string.Empty;
        public double Quantity { get; set; } = 0;
        public double QuantityActual { get; set; } = 0;
        public int Budget { get; set; } = 0;

        public SubActivity()
        {

        }

        public SubActivity(work_def_base entity)
        {
            if (entity == null)
                return;

            if(typeof(work_def_subact) == entity.GetType())
            {
                var subActivityEntity = (work_def_subact)entity;
                Code = subActivityEntity.SUB_ACT;
                Description = subActivityEntity.SUB_ACT_D;
            }
            else if(typeof(work_def_budget) == entity.GetType())
            {
                var subEntity = (work_def_budget)entity;
                Code = subEntity.sub_act;
                Description = subEntity.sub_act_d;
                Um = subEntity.um;
                Quantity = subEntity.qty;
                Budget = subEntity.sub_mhrs;
            }
            else
            {
                Code = entity.ACT;
                Description = entity.ACT_D;
            }
        }

        public budget_subact GetBudgetSubactEntity()
        {
            return new budget_subact()
            {
                sub_act = Code,
                um = Um,
                mhrs = Budget.ToString(),
                qty = (float)Quantity
            };
        }
        
        public override int GetHashCode()
        {
            return "SUB".GetHashCode() + Code.GetHashCode();
        }
    }
}
