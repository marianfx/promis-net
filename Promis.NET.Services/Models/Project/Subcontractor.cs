﻿using Promis.NET.Services.Models.Interfaces;
using Promis.Web.DataAccess.Entities.ProjectEntities;

namespace Promis.NET.Services.Models.Project
{
    public class Subcontractor: BaseModel, IBaseModel
    {
        public string Code { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public long Id { get; set; } = 0;

        public Subcontractor()
        {

        }

        public Subcontractor(subcontractor subcontractor)
        {
            Code = subcontractor.code;
            Description = subcontractor.subcontractorCol;
            Id = subcontractor.id;
        }
    }
}
