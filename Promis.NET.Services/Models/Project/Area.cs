﻿using Promis.NET.Services.Models.Interfaces;
using Promis.Web.DataAccess.Entities.ProjectEntities;

namespace Promis.NET.Services.Models.Project
{
    public class Area: BaseModel, IBaseModel
    {
        public string Code { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public long Id { get; set; } = 0;

        public Area()
        {

        }

        public Area(area obj)
        {
            if (obj == null)
                return;

            Code = obj.CODE;
            Description = obj.DESCRIPTION;
            Id = obj.id;
        }
    }
}
