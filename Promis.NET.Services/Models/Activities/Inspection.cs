﻿using Promis.NET.Services.Models.Interfaces;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using System;

namespace Promis.NET.Services.Models.Activities
{
    public class Inspection: BaseModel, IBaseModel
    {
        public string Title { get; set; } = string.Empty;
        public int Step { get; set; } = 0;
        public int AcceptedCount { get; set; } = 0;
        public int TotalCount { get; set; } = 0;
        public double AcceptancePercentage { get; set; } = 0;
        public bool HasData { get; set; } = false;

        public Inspection() { }

        public Inspection(testpack_essentials dto, int totalCount)
        {
            if (dto == null)
                return;

            Title = dto.inspection;
            Step = dto.step;
            AcceptedCount = dto.number;
            HasData = dto.exists;
            TotalCount = totalCount;
            if (TotalCount != 0)
                AcceptancePercentage = Math.Round((double)AcceptedCount / TotalCount * 100, 2);
        }
    }
}
