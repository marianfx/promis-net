﻿using Promis.NET.Services.Models.Interfaces;
using System.Collections.Generic;

namespace Promis.NET.Services.Models.Activities
{
    public class Isometrics : BaseModel, IBaseModel
    {
        public string TestpackTag { get; set; } = string.Empty;
        public List<Isometric> Iso { get; set; } = new List<Isometric>();

        public Isometrics()
        {

        }
        public Isometrics(string tag, List<Isometric> isometrics)
        {
            TestpackTag = tag;
            Iso = isometrics;
        }

    }
}
