﻿using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.DataAccess.Entities.PromisWeb;
using Promis.NET.Services.Models.Interfaces;
using Promis.Web.DataAccess.Entities.ProjectEntities;

namespace Promis.NET.Services.Models.Activities
{
    public class PlBaseTables : IBaseModel
    {
        public string Code { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;

        public PlBaseTables()
        {

        }

        public PlBaseTables(system_base entity)
        {
            if (entity == null)
                return;

            Code = entity.SYSTEM;
            Description = entity.SYSTEM_DESCRIPTION;
        }

        public PlBaseTables(pl_disci_base entity)
        {
            if (entity == null)
                return;

            Code = entity.code;
            Description = entity.description;
        }

        public PlBaseTables(pl_orig_base entity)
        {
            if (entity == null)
                return;

            Code = entity.initials;
            Description = entity.name;
        }

        public PlBaseTables(pl_actionby_base entity)
        {
            if (entity == null)
                return;

            Code = entity.code;
            Description = entity.company;
        }
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != this.GetType())
                return false;

            var thisObj = (PlBaseTables)obj;
            return thisObj.Code == this.Code;
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }

    }
}
