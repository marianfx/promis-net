﻿using Promis.NET.Services.Models.Interfaces;
using Promis.Web.DataAccess.Entities.ProjectEntities;

namespace Promis.NET.Services.Models.Activities
{
    public class Punchlist: BaseModel, IBaseModel
    {
        public string SYSTEM { get; set; } = string.Empty;
        public string SUBSYSTEM { get; set; } = string.Empty;
        public string ITEM { get; set; } = string.Empty;
        public string DETAILS { get; set; } = string.Empty;
        public string CAT { get; set; } = string.Empty;
        public string DISCIPLINE { get; set; } = string.Empty;
        public string ACTIONBY { get; set; } = string.Empty;
        public string ORIGINATOR { get; set; } = string.Empty;
        public string FOUNDDATE { get; set; } = string.Empty;
        public string STATUS { get; set; } = string.Empty;
        public string CLOSER { get; set; } = string.Empty;
        public string TARGETDATE { get; set; } = string.Empty;
        public string CLOSEDATE { get; set; } = string.Empty;
        public string DOC { get; set; } = string.Empty;
        public string CLOSURE { get; set; } = string.Empty;

        public Punchlist()
        {

        }

        public Punchlist(punchlist entity)
        {
            if (entity == null)
                return;

            SYSTEM = entity.system;
            SUBSYSTEM = entity.subsystem;
            ITEM = entity.item;
            DETAILS = entity.wd;
            CAT = entity.flag;
            DISCIPLINE = entity.pl_disci;
            ACTIONBY = entity.actionby;
            ORIGINATOR = entity.orig;
            FOUNDDATE = entity.found_date;
            STATUS = entity.js;
            CLOSER = entity.closer;
            TARGETDATE = entity.target_date;
            CLOSEDATE = entity.closure_date;
            DOC = entity.reference;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != this.GetType())
                return false;

            var thisObj = (Punchlist)obj;
            return thisObj.ITEM == this.ITEM;
        }

        public override int GetHashCode()
        {
            return ITEM.GetHashCode();
        }
    }
}