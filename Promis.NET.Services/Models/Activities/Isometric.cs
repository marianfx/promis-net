﻿
using Promis.NET.Services.Models.Interfaces;
namespace Promis.NET.Services.Models.Activities
{
    public class Isometric : BaseModel, IBaseModel
    {
        public string IsoTag { get; set; } = string.Empty;
        public string System { get; set; }=string.Empty;
        public string Subsystem { get; set; }= string.Empty;
        public string Wbs { get; set; } = string.Empty;

        public Isometric() { }

        public Isometric(string isoTag, string system, string subsystem, string wbs)
        {
            IsoTag = isoTag;
            System = system;
            Subsystem = subsystem;
            Wbs = wbs;
        }

    }
}
