﻿using Promis.NET.Services.Models.Interfaces;


namespace Promis.NET.Services.Models.Preservation
{
    public class PreservationFollowUp : BaseModel, IBaseModel
    {
        public string Donew { get; set; }
        public string Sequence { get; set; }
        
        //public string Tag { get; set; }
        //public string Code { get; set; }
        //public string Tag { get; set; }
        //public string SchedWeek { get; set; }       
        //public List<string> Tags { get; set; } = new List<string>();

        public PreservationFollowUp()
        {

        }
    }
}
