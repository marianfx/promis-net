﻿
namespace Promis.NET.Services.Models.Preservation
{
  
    public class PreservationDetails
    {
        public string JobPlan { get; set; } = string.Empty;
        public string Activity { get; set; } = string.Empty;
        public string Sequence { get; set; } = string.Empty;
        public string Frequency { get; set; } = string.Empty;

        public PreservationDetails()
        {
           
        }

        public PreservationDetails(string jobPlan,string activity, string sequence, string frequency)
        {
            JobPlan = jobPlan;
            Activity = activity;
            Sequence = sequence;
            Frequency = frequency;
        }
  
    }
}
