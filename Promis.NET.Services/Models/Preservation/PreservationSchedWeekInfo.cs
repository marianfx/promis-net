﻿
namespace Promis.NET.Services.Models.Preservation
{
    public class PreservationSchedWeekInfo
    {

        public int Count { get; set; } = 0;
        public string ScheduledWeek { get; set; } = string.Empty;
        public string Tag { get; set; } = string.Empty;
        public string Code { get; set; } = string.Empty;


        public PreservationSchedWeekInfo()
        {

        }

        public PreservationSchedWeekInfo(int count, string schedw, string tag, string code)
        {
            Count = count;
            ScheduledWeek = schedw;
            Tag = tag;
            Code = code;
        }
    }
}
