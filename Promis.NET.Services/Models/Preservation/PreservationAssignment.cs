﻿

using Promis.NET.Services.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace Promis.NET.Services.Models.Preservation
{
    public class PreservationAssignment:BaseModel, IBaseModel
    {
        public DateTime? InitialisationDate { get; set; }
        public DateTime? MaintenanceDate { get; set; }
        public DateTime? RestorationDate { get; set; }
        public string TagLocation { get; set; } = string.Empty;
        public List<string>Tags { get; set; } = new List<string>();


        public PreservationAssignment()
        {

        }
    }
    
}
