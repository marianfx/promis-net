﻿
using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.Services.Models.Preservation
{
    public class PreservationActions: BaseModel, IBaseModel
    {
        public string Done { get; set; } = string.Empty;
        public string Activity { get; set; } = string.Empty;
        public string Sequence { get; set; } = string.Empty;
        public string Nr { get; set; } = string.Empty;
        public string Schedw { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Code { get; set; } = string.Empty;

        public PreservationActions()
        {

        }

        public PreservationActions(string activity, string seq,string done, string nr, string schedw, string description, string code)
        {          
            Activity = activity;
            Sequence = seq;
            Done = done;
            Nr = nr;
            Schedw = schedw;
            Description = description;
            Code = code;
        }
    }
}
