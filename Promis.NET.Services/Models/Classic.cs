﻿using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.Services.Models
{
    public class Classic: BaseModel, IBaseModel
    {
        public string Code { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;

        public Classic() { }

        public Classic(string code, string description)
        {
            Code = code;
            Description = description;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            var objAsThis = obj as Classic;
            return Code == objAsThis.Code;
        }

        public override int GetHashCode()
        {
            return Code != null ? Code.GetHashCode() : base.GetHashCode();
        }
    }
}
