﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promis.NET.Services.Models.Certification
{
    public class FollowUpData
    {
        public string Datatable { get; set; } = string.Empty;
        public string RowId { get; set; } = string.Empty;
        public string Ord { get; set; } = string.Empty;
        public string UserValue { get; set; } = string.Empty;
    }
}
