﻿using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.Services.Models.Certification
{
    public class CertificationSubcontractorsAssignment : BaseModel, IBaseModel
    {
        public double BudgetMHRS { get; set; } = 0.0;
        public string Productivity { get; set; } = string.Empty;
        public string SubcontractorCode { get; set; } = string.Empty;
        public string AssignedSubcontractor { get; set; } = string.Empty;
        public string SubActivities { get; set; } = string.Empty;
        public string SubActivitiesDescription { get; set; } = string.Empty;

        public CertificationSubcontractorsAssignment()
        {

        }

        public CertificationSubcontractorsAssignment(double budget, string productivity, string subcontractor,string subcontractorCode, string subact, string description)
        {
            BudgetMHRS = budget;
            Productivity = productivity;
            SubcontractorCode = subcontractorCode;
            AssignedSubcontractor = subcontractor;
            SubActivities = subact;
            SubActivitiesDescription = description;
        }
    }
}
