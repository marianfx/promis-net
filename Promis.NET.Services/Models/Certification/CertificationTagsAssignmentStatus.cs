﻿using Promis.NET.Services.Models.Commissioning;
using Promis.NET.Services.Models.Interfaces;

namespace Promis.NET.Services.Models.Certification
{
    public class CertificationTagsAssignmentStatus : Datatables, IBaseModel
    {
        public double TagsAssigned { get; set; } = 0;
        public double TagsNotAssigned { get; set; } = 0;
        public double Progress { get; set; } = 0;

        public CertificationTagsAssignmentStatus()
        {

        }

        public CertificationTagsAssignmentStatus(double count1, double count2, double progress, string dataTable, string description):base(dataTable, description)
        {
            TagsAssigned = count1;
            TagsNotAssigned = count2;
            Progress = progress;
        }
    }
}
