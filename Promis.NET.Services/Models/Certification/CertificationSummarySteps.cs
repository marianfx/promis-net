﻿using Promis.NET.DataAccess.Entities.ProjectEntities;
using System.Collections.Generic;

namespace Promis.NET.Services.Models.Certification
{
    public class CertificationSummarySteps
    {
        public string Description { get; set; } = string.Empty;
        public KeyValuePair<string, string> Subcontractor1 { get; set; } = new KeyValuePair<string, string>();
        public KeyValuePair<string, string> Contractor { get; set; } = new KeyValuePair<string, string>();
        public KeyValuePair<string, string> Client { get; set; } = new KeyValuePair<string, string>();
        public string FileText { get; set; } = string.Empty;
        public List<string> UsersList { get; set; } = new List< string>();
        public string Ord { get; set; } = string.Empty;

        public CertificationSummarySteps() { }

        public CertificationSummarySteps(work_def_sub_act input)
        {
            if (input == null)
                return;

            Description = input.inspection;
            Subcontractor1 = new KeyValuePair<string, string>(input.s_user, input.SColumn);
            Contractor = new KeyValuePair<string, string>(input.c_user, input.CColumn);
            Client = new KeyValuePair<string, string>(input.o_user, input.OColumn);
            FileText = input.result == "N" ? "X" : "";
            Ord = !string.IsNullOrWhiteSpace(input.code) ? "X" : null;
        }

    }
}
