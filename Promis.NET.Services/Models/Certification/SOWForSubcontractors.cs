﻿using Promis.NET.Services.Models.Interfaces;
using Promis.Web.DataAccess.Entities.ProjectEntities;

namespace Promis.NET.Services.Models.Certification
{
    public class SOWForSubcontractors: BaseModel, IBaseModel
    {    
        public string Code { get; set; } = string.Empty;
        public string SubActivity { get; set; } = string.Empty;
        public bool Value { get; set; } = false;

        public SOWForSubcontractors()
        {

        }

        public SOWForSubcontractors(subc_sow sow)
        {
            Code = sow.code;
            SubActivity = sow.sub_act;
        }
    }
}
