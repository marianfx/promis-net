﻿

namespace Promis.NET.Services.Models.Engineering
{
    public class TagsData
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string PdfName { get; set; }
        public string ImageName { get; set; }
        public bool FileExists { get; set; }
        public string Data { get; set; }
    }
}
