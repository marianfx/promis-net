﻿using Promis.NET.DataAccess.Models;


namespace Promis.NET.Services.Models.Engineering
{
    public class DataTableColumn
    {
        public string Data { get; set; }
        public string Name { get; set; }
        public bool Searchable { get; set; }
        public bool Orderable { get; set; }

        public DataTableSearch Search { get; set; }
    }
}
