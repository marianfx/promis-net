﻿using Promis.NET.Services.Models.Interfaces;
using System.Collections.Generic;

namespace Promis.NET.Services.Models.Engineering
{
    public class EngineeringMenuData : BaseModel, IBaseModel
    {
        public Dictionary<string, Dictionary<string, string>> CivilDataBaseData = new Dictionary<string, Dictionary<string, string>>();
        public Dictionary<string, Dictionary<string, string>> EngineeringData = new Dictionary<string, Dictionary<string, string>>();
    }
   
}

