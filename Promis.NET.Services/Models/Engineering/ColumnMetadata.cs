﻿using Promis.NET.Services.Models.Interfaces;
using System.Collections.Generic;


namespace Promis.NET.Services.Models.Engineering
{
    public class ColumnMetadata : BaseModel, IBaseModel
    {
        public string ColumnName { get; set; } = "";
        public string Description { get; set; } = "";
        public bool IsRequired { get; set; } = false;
        public bool IsIndex { get; set; } = false;
        public Dictionary<string, string> DefaultValues = new Dictionary<string, string>();
    }
}
