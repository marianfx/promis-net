﻿using Promis.NET.Services.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace Promis.NET.Services.Models.Engineering
{
    public class TableMetadata : BaseModel, IBaseModel
    {
        public Boolean TableExists{ get; set; }
        public List<string> MandatoryFields { get; set; }
    }
}
