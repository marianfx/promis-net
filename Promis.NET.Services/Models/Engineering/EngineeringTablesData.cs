﻿using Promis.NET.Services.Models.Interfaces;
using System.Collections.Generic;

namespace Promis.NET.Services.Models.Engineering
{
    public class EngineeringTablesData : BaseModel, IBaseModel
    {
        public List<CountDataForTables> EngineeringData;
        public List<CountDataForTables> CivilData;
    }
}
