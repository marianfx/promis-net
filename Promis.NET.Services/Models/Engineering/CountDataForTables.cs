﻿
namespace Promis.NET.Services.Models.Engineering
{
    public class CountDataForTables
    {
        public double TotalCount { get; set; }
        public double Quantities { get; set; }
        public double Certification { get; set; }
        public double System { get; set; }
        public string TableDescription { get; set; }
    }
}
