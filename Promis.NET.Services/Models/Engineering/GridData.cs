﻿using Promis.NET.Services.Models.Interfaces;
using Promis.NET.Utilities.Models;
using System.Collections.Generic;

namespace Promis.NET.Services.Models.Engineering
{
    public class GridData : BaseModel, IBaseModel
    {
        public List<DataKeeper> Columns = new List<DataKeeper>();
        public List<List<DataKeeper>> Rows { get; set; } = new List<List<DataKeeper>>();
    }
}
