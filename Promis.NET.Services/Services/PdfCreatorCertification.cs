﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using Promis.NET.Localization;
using Promis.NET.Services.Models.Certification;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace Promis.NET.Services.Services
{
    public class PdfCreatorCertification : PdfCreator, IPdfCreatorCertification
    {
        private Color headerColor = Color.FromCmyk(43, 6, 0, 0);
        private Color dtHeaderColor = Color.FromCmyk(0, 0, 0, 17);

        private string _subActivityCode;
        private string _subActivityDescription;
        private string _tag;
        private string _docToAttach;
        private IEnumerable<CertificationSummarySteps> _data;
        private string[] _signingUsers;

        public PdfCreatorCertification(string serverPath): base(serverPath)
        {
        }

        public PdfCreatorCertification(string serverPath, string containerDirectory, Tuple<string, string> codeData, string tag, Project project, IEnumerable<CertificationSummarySteps> data, string documentToAttach = null, params string[] signingUsers) : this(serverPath)
        {
            SetData(containerDirectory, codeData, tag, project, data, documentToAttach, signingUsers);
        }

        public void SetData(string containerDirectory, Tuple<string, string> codeData, string tag, Project project, IEnumerable<CertificationSummarySteps> data, string documentToAttach = null, params string[] signingUsers)
        {
            //use base
            SetData(containerDirectory, project, LocalResources.CertificationStatus + " " + tag, false, true, LocalResources.Tag.ToUpper() + ": " + tag, codeData.Item1 + " - " + codeData.Item2);

            _subActivityCode = codeData.Item1;
            _subActivityDescription = codeData.Item2;
            _tag = tag;
            _docToAttach = documentToAttach;
            _signingUsers = signingUsers;
            if (data != null)
                _data = data;
        }

        protected override void AddData()
        {
            AddFooterFirstPageTable();
            AddDataTable();

        }

        protected override void DoExtraWorkOnPdfDocument(PdfDocument pdfDocument)
        {
            if (string.IsNullOrWhiteSpace(_docToAttach) || !_docToAttach.EndsWith(".pdf"))
                return;

            var docToAttachPath = Path.Combine(ServerParentPath, _docToAttach);
            if (!File.Exists(docToAttachPath))
                return;

            var inputDocument = PdfReader.Open(docToAttachPath, PdfDocumentOpenMode.Import);
            int count = inputDocument.PageCount;
            for (int idx = 0; idx < count; idx++)
            {
                PdfPage page = inputDocument.Pages[idx];
                pdfDocument.AddPage(page);
            }
        }

        private void AddFooterFirstPageTable()
        {
            var table = section.Footers.Primary.AddTable();
            table.Style = "TableFooter";
            table.Borders.Color = Colors.Gray;
            table.Borders.Width = 0.5;

            var columnWidth = PageWidth / 4;
            var column = table.AddColumn(columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn(columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn(columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn(columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;

            var rowHeight = "15";
            var row0 = table.AddRow();
            row0.Height = rowHeight;
            row0.VerticalAlignment = VerticalAlignment.Center;
            row0.Cells[0].MergeRight = 3;
            row0.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row0.Cells[0].AddParagraph(LocalResources.Signatures.ToUpper());

            var row1 = table.AddRow();
            row1.Height = rowHeight;
            row1.VerticalAlignment = VerticalAlignment.Center;
            row1.Cells[0].AddParagraph(LocalResources.For);
            row1.Cells[1].AddParagraph(LocalResources.Subcontractor.ToUpper());
            row1.Cells[2].AddParagraph(LocalResources.Contractor.ToUpper());
            row1.Cells[3].AddParagraph(LocalResources.Owner.ToUpper());

            var row2 = table.AddRow();
            row2.Height = rowHeight;
            row2.VerticalAlignment = VerticalAlignment.Center;
            row2.Cells[0].AddParagraph(LocalResources.Name);
            if(_signingUsers != null && _signingUsers.Length > 0 && !string.IsNullOrWhiteSpace(_signingUsers[0]))
                row2.Cells[1].AddParagraph(_signingUsers[0]);
            if (_signingUsers != null && _signingUsers.Length > 1 && !string.IsNullOrWhiteSpace(_signingUsers[1]))
                row2.Cells[2].AddParagraph(_signingUsers[1]);
            if (_signingUsers != null && _signingUsers.Length > 2 && !string.IsNullOrWhiteSpace(_signingUsers[2]))
                row2.Cells[3].AddParagraph(_signingUsers[2]);

            // 2 empty rows, with headers
            var row3 = table.AddRow();
            row3.Height = rowHeight;
            row3.VerticalAlignment = VerticalAlignment.Center;
            row3.Cells[0].AddParagraph(LocalResources.Signature);
            var row4 = table.AddRow();
            row4.Height = rowHeight;
            row4.VerticalAlignment = VerticalAlignment.Center;
            row4.Cells[0].AddParagraph(LocalResources.Date);
        }
        
        private void AddDataTable()
        {
            // outer table
            var containerTable = section.AddTable();
            containerTable.Style = "TableDataStyleBold";
            containerTable.Borders.Color = Colors.Gray;
            containerTable.Borders.Width = 0.5;

            var smallColumn = PageWidth / 7;
            var column1 = containerTable.AddColumn(3.8 * smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Left;

            var columnx = containerTable.AddColumn(0.2 * smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;

            // add header -> special formatiing
            var rowHeight = "25";
            var row1 = containerTable.AddRow();
            row1.Height = rowHeight;
            row1.VerticalAlignment = VerticalAlignment.Center;
            row1.Shading.Color = headerColor;
            row1.Style = "TableFooter";
            row1.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row1.Cells[0].AddParagraph(LocalResources.CertificationSteps.ToUpper());
            row1.Cells[1].AddParagraph("N/A");
            row1.Cells[2].AddParagraph(LocalResources.Subcontractor.ToUpper());
            row1.Cells[3].AddParagraph(LocalResources.Contractor.ToUpper());
            row1.Cells[4].AddParagraph(LocalResources.Owner.ToUpper());

            // add actual data
            foreach (var entry in _data)
            {
                if(string.IsNullOrWhiteSpace(entry.Ord))//no data
                {
                    var rowDt = containerTable.AddRow();
                    rowDt.Height = rowHeight;
                    rowDt.VerticalAlignment = VerticalAlignment.Center;
                    rowDt.Style = "TableFooter";
                    rowDt.Format.Alignment = ParagraphAlignment.Left;
                    rowDt.Cells[0].Shading.Color = dtHeaderColor;
                    rowDt.Cells[0].AddParagraph(entry.Description);
                    rowDt.Cells[1].MergeRight = 3;
                    rowDt.Cells[1].Shading.Color = dtHeaderColor;
                }
                else
                {
                    var row = containerTable.AddRow();
                    row.Height = rowHeight;
                    row.Style = "TableFooter";
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
                    row.Cells[0].AddParagraph(entry.Description);
                    if(!string.IsNullOrWhiteSpace(entry.FileText))
                        row.Cells[1].AddParagraph(entry.FileText);//X or ""
                    if(!string.IsNullOrWhiteSpace(entry.Subcontractor1.Key))
                        row.Cells[2].AddParagraph(entry.Subcontractor1.Key);
                    if (!string.IsNullOrWhiteSpace(entry.Subcontractor1.Value))
                        row.Cells[2].AddParagraph(entry.Subcontractor1.Value);
                    if (!string.IsNullOrWhiteSpace(entry.Contractor.Key))
                        row.Cells[3].AddParagraph(entry.Contractor.Key);
                    if (!string.IsNullOrWhiteSpace(entry.Contractor.Value))
                        row.Cells[3].AddParagraph(entry.Contractor.Value);
                    if (!string.IsNullOrWhiteSpace(entry.Client.Key))
                        row.Cells[4].AddParagraph(entry.Client.Key);
                    if (!string.IsNullOrWhiteSpace(entry.Client.Value))
                        row.Cells[4].AddParagraph(entry.Client.Value);
                }
            }
        }
    }
}
