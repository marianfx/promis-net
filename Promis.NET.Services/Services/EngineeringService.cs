﻿using Promis.NET.DataAccess.Models;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Services.Models.Engineering;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Promis.NET.Services.Models.Excel;
using System.IO;
using NPOI.HSSF.Util;
using System.Dynamic;
using Promis.NET.DataAccess.Extensions;
using Promis.NET.Localization;

namespace Promis.NET.Services.Services
{
    public class EngineeringService : Service, IEngineeringService
    {
        private IEngineeringRepository _engineeringRepository;
        private IAdvancedTableRepository _tableRepository;
        private IExcelService _excelService;

        public EngineeringService(IEngineeringRepository engineeringRepository, IAdvancedTableRepository tableRepository, IExcelService excelService) : base(engineeringRepository, tableRepository)
        {
            _engineeringRepository = engineeringRepository;
            _tableRepository = tableRepository;
            _excelService = excelService;
        }

        #region Metadata and Summaries
        public EngineeringMenuData GetMenuItemsForProject(string projectDb)
        {
            SetWorkingProject(projectDb);

            var dataEng = _engineeringRepository.GetMenuDataForEngineeringDatabases();
            var dataCivilDb = _engineeringRepository.GetMenuDataForCivilDataBase();
            
            var output = new EngineeringMenuData();
            output.EngineeringData = new Dictionary<string, Dictionary<string, string>>();
            output.CivilDataBaseData = new Dictionary<string, Dictionary<string, string>>();

            foreach (DataRow row in dataEng.Rows)
            {
                var menuItem = row[0].ToString();
                var submenuItem = row[1].ToString();
                var submenuValue = row[2].ToString();
                
                if (!output.EngineeringData.Keys.Contains(menuItem))
                    output.EngineeringData[menuItem] = new Dictionary<string, string>();
                if (!string.IsNullOrWhiteSpace(submenuItem))
                    output.EngineeringData[menuItem].Add(submenuItem, submenuValue);
            }

            foreach (DataRow row in dataCivilDb.Rows)
            {
                var menuItem = row[0].ToString();
                var submenuItem = row[1].ToString();
                var submenuValue = row[2].ToString();
                
                if (!output.CivilDataBaseData.Keys.Contains(menuItem))
                    output.CivilDataBaseData[menuItem] = new Dictionary<string, string>();
                if (!string.IsNullOrWhiteSpace(submenuItem))
                    output.CivilDataBaseData[menuItem].Add(submenuItem, submenuValue);
            }

            return output;
        }
        
        public List<CountDataForTables> GetSummaryForEngineeringTables()
        {
            var output = new List<CountDataForTables>();

            var engineeringTables = _engineeringRepository.GetTableDataFromTableTagDef();
            foreach (var item in engineeringTables)
            {
                var resCount = _engineeringRepository.GetDataForEngineeringPage(item.Key);
                if (resCount != null)
                {
                    var values = new CountDataForTables()
                    {
                        TotalCount = double.Parse(resCount[0].ToString()),
                        Quantities = double.Parse(resCount[1].ToString()),
                        Certification = double.Parse(resCount[2].ToString()),
                        System = double.Parse(resCount[3].ToString()),
                        TableDescription = item.Value.ToString()
                    };
                    output.Add(values);
                }
            }
            return output;
        }

        public List<CountDataForTables> GetSummaryForCivilTables()
        {
            var output = new List<CountDataForTables>();

            var civilTables = _engineeringRepository.GetTableDataFromTableTagDef("*");
            foreach (var item in civilTables)
            {
                var resCount = _engineeringRepository.GetDataForEngineeringPage(item.Key);
                if (resCount != null)
                {
                    var values = new CountDataForTables()
                    {
                        TotalCount = double.Parse(resCount[0].ToString()),
                        Quantities = double.Parse(resCount[1].ToString()),
                        Certification = double.Parse(resCount[2].ToString()),
                        System = double.Parse(resCount[3].ToString()),
                        TableDescription = item.Value.ToString()
                    };
                    output.Add(values);
                }
            }
            return output;
        }
        #endregion


        #region Data Manipulation for Table

        #region Server Side Processing Data
        public long GetRowsNumberForTable(string dataTable, List<WhereCondition> conditions, List<JoinCondition> joinConditions, DataTableColumn[] columns, bool distinct = false)
        {
            var columnNames = columns?.Select(c => c.Name);
            _tableRepository.Initialize(dataTable, columnNames, conditions, joinConditions, distinct);
            return _tableRepository.CountAll();
        }

        public long GetRowsNumberForFilteredData(string dataTable, List<WhereCondition> conditions, List<JoinCondition> joinConditions, string search, DataTableColumn[] columns, bool distinct = false)
        {
            var columnNames = columns?.Select(c => c.Name);
            _tableRepository.Initialize(dataTable, columnNames, conditions, joinConditions, distinct, search);
            return _tableRepository.CountAllFiltered();
        }

        public GridData GetMetadataForTable(string dataTable)
        {
            var output = new GridData
            {         
                Columns = _engineeringRepository.GetColumnsNameAndType(dataTable)
            };

            return output;
        }

        public List<dynamic> GetDataForTableServerSide(string dataTable, int start, int length, DataTableOrder[] order, DataTableSearch search, DataTableColumn[] columns, List<WhereCondition> conditions, List<JoinCondition> joinConditions, bool distinct = false)
        {
            var columnNames = columns?.Select(c => c.Name.Replace("\\.", "."));// remove escaped point received from JS
            _tableRepository.Initialize(dataTable, columnNames, conditions, joinConditions, order, distinct, search?.Value);
            var res = _tableRepository.GetTableData(start, length);

            var resultSet = new List<dynamic>();
            foreach (DataRow dataRow in res.Rows)
            {
                dynamic expando = new ExpandoObject();        
                foreach (DataColumn column in res.Columns)
                {   var dict = (IDictionary<string, object>)expando;
                    var columnUppered = column.ColumnName.ToUpper();// escape point
                    if (columnUppered == "ID")
                        dict["DT_RowId"] = "row-" + dataRow[column.ColumnName].ToString();
                    else
                        dict[columnUppered] = DBNull.Value != dataRow[column.ColumnName] && dataRow[column.ColumnName] != null ? dataRow[column.ColumnName] : "";
                }
                resultSet.Add(expando);
            }
   
            return resultSet;
        }
        #endregion

        public string InsertData(string dataTable, string projectDb, List<DataKeeper> values)
        {
            VerifyDataPersistence(dataTable, projectDb, values);
            return _engineeringRepository.InsertData(dataTable, values);
        }
        
        public void UpdateData(string dataTable, string projectDb, List<DataKeeper> values, string rowId)
        {
            VerifyDataPersistence(dataTable, projectDb, values, "U", rowId);
            _engineeringRepository.UpdateData(dataTable, values, rowId);
        }
        
        public void DeleteData(string dataTable, string rowId)
        {
            _engineeringRepository.DeleteData(dataTable, rowId);
        }

        public List<string> GetColumnsRequiredAndNotPopulated(string dataTable, string projectDb, List<DataKeeper> values)
        {
            var columnOutput = new List<string>();
            var forSystems = new string[] { "activity", "system", "subsystem" };
            var columnMetadata = _engineeringRepository.GetTableColumnsMetadata(dataTable, projectDb);
            for (int i = 0; i < columnMetadata.Rows.Count; i++)
            {
                var valueReq = columnMetadata.Rows[i]["is_req"].ToString();
                var columnName = columnMetadata.Rows[i]["column_name"].ToString();
                var value = values.Where(x => x.ColumnName.ToLower() == columnName.ToLower() && !string.IsNullOrWhiteSpace(x.Value?.ToString())).FirstOrDefault();
                if (valueReq == "y" && value == null)
                    if (dataTable.ToLower() != "systems" && forSystems.Contains(columnName.ToLower()))
                        continue;
                    else
                        columnOutput.Add(columnName);
            }

            return columnOutput;
        }

        public List<WhereCondition> GetIdentifyingColumnsForTableRows(string dataTable, string projectDb, List<ColumnMetadata> metadata, List<DataKeeper> values)
        {
            var conditions = new List<WhereCondition>();
            var forSystems = new string[] { "activity", "system", "subsystem" };

            conditions = metadata.Where(c => c.IsIndex || c.ColumnName.ToLower() == "code")
                                    .Where(c => (dataTable.ToLower() != "systems" && !forSystems.Contains(c.ColumnName.ToLower())) || (dataTable.ToLower() == "systems" && forSystems.Contains(c.ColumnName.ToLower())))
                                        .Select(c => new WhereCondition(c.ColumnName,
                                                                        values.Where(x => x.ColumnName.ToLower() == c.ColumnName.ToLower())
                                                                               .Select(d => d.Value)
                                                                               .FirstOrDefault()))
                                        .ToList();

            if (dataTable == "planning" && conditions.Count == 0)
            {
                conditions.Add(new WhereCondition("act", values.Where(c => c.ColumnName.ToLower() == "act").Select(d => d.Value).FirstOrDefault()));
                conditions.Add(new WhereCondition("type", values.Where(c => c.ColumnName.ToLower() == "type").Select(d => d.Value).FirstOrDefault()));
            }

            // add the default 'id' column
            if (values.Select(c => c.ColumnName.ToLower()).Contains("id"))
                conditions.Add(new WhereCondition("id", values.Where(c => c.ColumnName.ToLower() == "id").FirstOrDefault()));

            return conditions;
        }

        public void VerifyDataPersistence(string dataTable, string projectDb, List<DataKeeper> values, string checkExistenceForOperation = "I", string rowId = "")
        {
            //verify if columnMetadata is null
            var metadata = GetColumnMetadataForTable(dataTable, projectDb);
            if (metadata == null || metadata.Count == 0)
                throw new Exception(LocalResources.CannotRetrieveColumnsForTable + dataTable);

            var columnsToVerify = GetIdentifyingColumnsForTableRows(dataTable, projectDb, metadata, values);

            if (checkExistenceForOperation == "I")
            {
                var colExist = _engineeringRepository.VerifyExistenceOfTheRecord(dataTable, projectDb, columnsToVerify);
                if (colExist)
                    throw new Exception(LocalResources.TheRecordAlreadyExists);
            }

            if (checkExistenceForOperation == "U")
            {
                columnsToVerify.Add(new WhereCondition("id", rowId, "<>"));

                var colExist = _engineeringRepository.VerifyExistenceOfTheRecord(dataTable, projectDb, columnsToVerify);
                //verify if already exists a similar record
                if (colExist)
                    throw new Exception(LocalResources.ARecordWithTheSameIdentifierAlreadyExists);
            }


            var fieldsReqPopulated = GetColumnsRequiredAndNotPopulated(dataTable, projectDb, values);
            if (fieldsReqPopulated.Count > 0)
                throw new Exception(LocalResources.TheFollowingFieldsNeedToBePopulated + string.Join(", ", fieldsReqPopulated));
        }

        public void DeleteRowIfExists(string dataTable, string projectDb, List<DataKeeper> values)
        {
            //verify if columnMetadata is null
            var metadata = GetColumnMetadataForTable(dataTable, projectDb);
            if (metadata == null || metadata.Count == 0)
                throw new Exception(LocalResources.CannotRetrieveColumnsForTable + dataTable);

            var columnsToVerify = GetIdentifyingColumnsForTableRows(dataTable, projectDb, metadata, values);
            _engineeringRepository.DeleteByConditions(dataTable, projectDb, columnsToVerify);
        }

        public List<ColumnMetadata> GetColumnMetadataForTable(string dataTable, string projectDb, bool isFiltering = false)
        {
            var output = new List<ColumnMetadata>();
            var tbl = _engineeringRepository.GetTableColumnsMetadata(dataTable, projectDb);

            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                var row = tbl.Rows[i];
                var required = !string.IsNullOrWhiteSpace(row["is_req"]?.ToString()) ? row["is_req"].ToString() : "n";
                var index = !string.IsNullOrWhiteSpace(row["is_index"]?.ToString()) ? row["is_index"].ToString() : "n";
                var columnData = new ColumnMetadata()
                {
                    IsRequired = required == "y" ? true : false,
                    IsIndex = index == "y" ? true : false,
                    ColumnName = row["column_name"]?.ToString(),
                    Description = row["description"]?.ToString(),
                };

                if (columnData.ColumnName.ToString().ToLower() != "id")
                {
                    columnData.DefaultValues = _engineeringRepository.GetPredefinedValuesForColumn(columnData.ColumnName?.ToLower(), dataTable, isFiltering);
                    output.Add(columnData);
                }
            }
            return output;
        }

        public List<TagsData> GetDataForTags()
        {
            var tagsList = new List<TagsData>();
            var tagResult = _engineeringRepository.GetDataForTags();

            if (tagResult.Count == 0)
                return tagsList;

            for (int i = 0; i < tagResult.Count; i++)
            {
                var tag = new TagsData()
                {
                    Code = tagResult[i].cat,
                    Title = tagResult[i].description,
                    PdfName = tagResult[i].description.ToLower().Replace("<br>", " ") + ".pdf",
                    ImageName = tagResult[i].description.ToLower().Replace("<br>", "-").Replace(" ", "-") + ".png"
                };
                //check if file exists
                var pdfServerPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/PDFs/Schemas/" + tag.PdfName);

                tag.FileExists = File.Exists(pdfServerPath);
                // add code
                var tagUnformatted = tagResult[i].fd_action.Split(',')[1];
                tag.Data = tagUnformatted.Substring(1, tagUnformatted.Length - 1);

                tagsList.Add(tag);
            } 
            
            return tagsList;
        }
        #endregion


        #region Excel Export / Import
        public string GenerateExcel(string dataTable, string projectDb, string username, string directoryContainer, bool isTemplate, List<WhereCondition> conditions = null, List<JoinCondition> joinConditions = null, List<string> columns = null, List<string> groupBys = null)
        {
            var excelData = new ExcelData
            {
                WorkbookName = GenerateExcelFileName(dataTable, projectDb, username),
                SheetList = new List<Sheet>()
            };
            excelData.FilePath = Path.Combine(directoryContainer, excelData.WorkbookName);
            columns = columns != null ? columns.Select(c => c.ToLower()).ToList() : new List<string>();
            
            // construct main table sheet
            var tableSheet = new Sheet
            {
                SheetName = dataTable + "1",
                Columns = new List<string>(),
                Rows = new List<List<DataKeeper>>()
            };

            var dbCellList = new List<DataKeeper>();
            var tempStoredColumns = new List<string>();
            var queryCol = _engineeringRepository.GetTableColumnsMetadata(dataTable, projectDb);
            for (int i = 0; i < queryCol.Rows.Count; i++)
            {
                var colVal = queryCol.Rows[i]["COLUMN_NAME"].ToString().ToLower();
                if (colVal != "id" && colVal != "valid" && colVal != "weld_last" && colVal != "locked" && (columns.Count == 0 || columns.Contains(colVal)))
                {
                    //get the values for the column called "COLUMN_NAME"
                    tableSheet.Columns.Add(colVal);
                    tempStoredColumns.Add(colVal);
                }
            }

            if(joinConditions != null)//add the columns in joins too
            {
                var joinTables = joinConditions.Select(jc => jc.DataTable).Distinct();
                foreach (var table in joinTables)
                {
                    queryCol = _engineeringRepository.GetTableColumnsMetadata(table, projectDb);
                    for (int i = 0; i < queryCol.Rows.Count; i++)
                    {
                        var colVal = queryCol.Rows[i]["COLUMN_NAME"].ToString().ToLower();
                        var withTableColVal = table.ToLower() + "." + colVal;
                        if (colVal != "id" && colVal != "valid" && colVal != "weld_last" && colVal != "locked" && (columns.Count == 0 || columns.Contains(withTableColVal)))
                        {
                            //get the values for the column called "COLUMN_NAME"
                            tableSheet.Columns.Add(colVal);
                            tempStoredColumns.Add(withTableColVal);
                        }
                    }
                }
            }
            excelData.SheetList.Add(tableSheet);

            // if it is template, add two more identical sheets, so the user has space to fill up the void
            if (isTemplate)
            {
                var copy1 = new Sheet(tableSheet)
                {
                    SheetName = dataTable + "2"
                };
                excelData.SheetList.Add(copy1);

                var copy2 = new Sheet(tableSheet)
                {
                    SheetName = dataTable + "3"
                };
                excelData.SheetList.Add(copy2);
            }

            _excelService.SetData(excelData);
            _excelService.CreateDocument();

            // if i do not need the excel template => i take the rows and insert them with "AddRows()" method from ExcelService
            if (!isTemplate)
            {
                var advFilters = conditions ?? new List<WhereCondition>();
                var joinCond = joinConditions ?? new List<JoinCondition>();

                int start = 0;
                int length = 100;              
                var rowsRead = 0;

                do
                {
                    _tableRepository.Initialize(dataTable, columns, advFilters, joinCond);
                    _tableRepository.SetGroupBy(groupBys);

                    var allRows = _tableRepository.GetTableData(start, length);
                    var allRowData = allRows.ToDataKeeperList(tempStoredColumns);
                    rowsRead = allRows.Rows.Count;

                    start = start + 100;
                    if (rowsRead > 0)
                        _excelService.AddRows(tableSheet, allRowData);
                } while (rowsRead != 0);

            }

            // add legend sheet
            var legendSheet = GetDataForLegendsSheet(dataTable, projectDb);
            if(joinConditions != null)
            {
                var joinTables = joinConditions.Select(jc => jc.DataTable).Distinct();
                foreach (var table in joinTables)
                {
                    var tempLegendSheet = GetDataForLegendsSheet(table, projectDb);
                    legendSheet.Rows.AddRange(tempLegendSheet.Rows);
                }
            }

            if (columns.Count > 0)
            {
                legendSheet.Rows = legendSheet.Rows
                                        .Where(r => columns.Contains(r[0].Value.ToLower()) || columns.Any(c => c.EndsWith("." + r[0].Value.ToLower())))
                                        .GroupBy(xl => xl[0].Value)
                                        .Select(group => group.FirstOrDefault())
                                        .ToList();//filter to only have the selected columns
            }

            legendSheet.SheetColor = HSSFColor.DarkGreen.Index;
            _excelService.AddSheet(legendSheet, true);
            _excelService.AddRows(legendSheet, legendSheet.Rows);

            return excelData.WorkbookName;
        }

        public Sheet GetDataForLegendsSheet(string dataTable, string projectDb)
        {
            var sheetLegend = new Sheet
            {
                Columns = new List<string>() { "FIELD", "DESCRIPTION", "REQUIRED", "MAX_LEN" },//static values for the fiels of the sheet legend
                SheetName = "LEGEND",//static value for the sheet legend
                Rows = new List<List<DataKeeper>>()
            };

            var tableMtd = _engineeringRepository.GetTableColumnsMetadata(dataTable, projectDb);
            for (int i = 0; i < tableMtd.Rows.Count; i++)
            {
                var code = tableMtd.Rows[i]["COLUMN_NAME"].ToString().ToLower();

                if (code == "id" || code == "valid" || code == "weld_last" || code == "locked")
                    continue;

                var codeToAdd = code.ToUpper();
                var description = tableMtd.Rows[i]["description"]?.ToString().ToUpper();
                var is_req = tableMtd.Rows[i]["is_req"]?.ToString().ToUpper();
                var fld_len = tableMtd.Rows[i]["fld_len"]?.ToString().ToUpper();

                var row = new List<DataKeeper>()
                {
                    new DataKeeper("code", codeToAdd),
                    new DataKeeper("description", description),
                    new DataKeeper("is_req", is_req),
                    new DataKeeper("fld_len", fld_len)
                };
                sheetLegend.Rows.Add(row);
            }

            return sheetLegend;
        }

        public void ImportDataFromExcel(string dataTable, string projectDb, string filePath, bool isOverWrite)
        {
            var excelData = new ExcelData()
            {
                SheetList = new List<Sheet>()
            };

            var requiredFields = new List<string>();
            var sheetInitial = new Sheet
            {
                Columns = new List<string>(),
                SheetName = dataTable + "1",
                Rows = new List<List<DataKeeper>>()
            };

            var tableMtd = _engineeringRepository.GetTableColumnsMetadata(dataTable, projectDb);

            for (int i = 0; i < tableMtd.Rows.Count; i++)
            {
                var column = tableMtd.Rows[i]["COLUMN_NAME"].ToString().ToLower();
                var isRequiredString = tableMtd.Rows[i]["is_req"]?.ToString();
                var isRequired = !string.IsNullOrWhiteSpace(isRequiredString) && isRequiredString.ToLower() == "y" ? true : false;
                if (column == "id")
                    continue;
                
                sheetInitial.Columns.Add(column.ToUpper());
                if (isRequired)
                    requiredFields.Add(column.ToUpper());
            }

            excelData.SheetList.Add(sheetInitial);
            _excelService.SetData(excelData);
            var res = _excelService.ReadExcelFile(filePath, sheetInitial.Columns, requiredFields);
            
            for(int i = 0; i < res.Rows.Count; i++)
            {
                var listCol = new List<DataKeeper>();
                foreach (var col in sheetInitial.Columns)      
                {           
                    listCol.Add(new DataKeeper(col, res.Rows[i][col]));
                }
                try
                {
                    if (isOverWrite)
                        DeleteRowIfExists(dataTable, projectDb, listCol);
                    InsertData(dataTable, projectDb, listCol);
                }
                catch(Exception ex)
                {
                    throw new Exception(LocalResources.ErrorAtInsertingDataOnRow + (i+1), ex);
                }
               
            }
        }

        /// <summary>
        /// Generates the name of the Excel File of format 'projectdb'-'tableName'('username')-'random-generated-identifier'.xls
        /// </summary>
        private string GenerateExcelFileName(string dataTable, string projectDb, string username)
        {
            string random = Guid.NewGuid().ToString();
            string fileName = projectDb + "-" + dataTable + "(" + username + ")" + "-" + random + ".xls";
            return fileName;
        }
        #endregion
    }
}
