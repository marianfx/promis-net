﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using Promis.NET.Localization;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Services.Interfaces;
using System;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Promis.NET.Services.Services
{
    public abstract class PdfCreator : IPdfCreator
    {
        public string ServerParentPath { get; set; }
        public string Name { get; set; }
        public string FullPath { get; set; }
        public float PageWidth { get; set; } = 595;
        public float PageHeight { get; set; } = 850;
        public float HeaderHeight { get; set; } = 120;
        public float FooterHeight { get; set; } = 0;
        public string DateToAttach { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");

        protected Random r = new Random();
        protected string tempImagePath = string.Empty;
        protected string tempFilePath = string.Empty;
        protected Document _document;
        protected Section section;

        protected Project _project;
        protected string _documentTitle;
        protected bool _hasDifferentFirstPageHeader;
        protected bool _hasExtraTextOnHeader;
        protected string _extraHeader1;
        protected string _extraHeader2;

        public PdfCreator(string serverPath)
        {
            ServerParentPath = serverPath;
            _document = new Document();
        }

        public void SetData(string containerDirectory, Project project, string docTitle, bool hasDifferentFirstPageHeader, bool hasExtraTextOnHeader, string extraHeader1 = null, string extraHeader2 = null)
        {
            _project = project;
            _documentTitle = docTitle;
            _hasDifferentFirstPageHeader = hasDifferentFirstPageHeader;
            _hasExtraTextOnHeader = hasExtraTextOnHeader;
            _extraHeader1 = extraHeader1;
            _extraHeader2 = extraHeader2;

            Name = "report_" + r.Next(1, 10001).ToString() + ".pdf";
            tempFilePath = Path.Combine(containerDirectory, Name);
            FullPath = Path.Combine(ServerParentPath, tempFilePath);
        }


        private void SaveImageToTempFile()
        {
            tempImagePath = Path.Combine(ServerParentPath, "documents", "pw_" + _project.Code, "logo.png");
            if (File.Exists(tempImagePath))
                return;

            if (string.IsNullOrWhiteSpace(_project.LogoUrl))
                return;

            var b64start = _project.LogoUrl.IndexOf("base64,");
            b64start = b64start >= 0 ? b64start + 7 : 0;
            var b64string = _project.LogoUrl.Substring(b64start);
            byte[] bytes = Convert.FromBase64String(b64string);

            Image image;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = Image.FromStream(ms);
            }

            var upperFather = Path.Combine(ServerParentPath, "documents");
            if (!Directory.Exists(upperFather))
                Directory.CreateDirectory(upperFather);
            upperFather = Path.Combine(upperFather, "pw_" + _project.Code);
            if (!Directory.Exists(upperFather))
                Directory.CreateDirectory(upperFather);

            image.Save(tempImagePath);
        }

        private void DefineStyles()
        {
            var style = _document.Styles["Normal"];//default style

            var tableStyle = _document.AddStyle("TableHeader", "Normal");
            tableStyle.Font.Name = "Arial";
            tableStyle.Font.Bold = true;
            tableStyle.Font.Size = 8;

            var titleStyle = _document.AddStyle("TableTitle", "Normal");
            titleStyle.Font.Name = "Helvetica";
            titleStyle.Font.Bold = true;
            titleStyle.Font.Size = 14;
            titleStyle.ParagraphFormat.Alignment = ParagraphAlignment.Center;

            var footerStyle = _document.AddStyle("TableFooter", "TableTitle");
            footerStyle.Font.Size = 7;

            var tableDataStyle = _document.AddStyle("TableDataStyle", "TableTitle");
            tableDataStyle.Font.Bold = false;
            tableDataStyle.Font.Size = 6;

            var tableDataStyleBold = _document.AddStyle("TableDataStyleBold", "TableDataStyle");
            tableDataStyleBold.Font.Bold = true;

            // define page format
            _document.DefaultPageSetup.PageFormat = PageFormat.A4;
            _document.DefaultPageSetup.TopMargin = "20";
            _document.DefaultPageSetup.LeftMargin = "20";
            _document.DefaultPageSetup.RightMargin = "20";
            _document.DefaultPageSetup.BottomMargin = "0";
            PageWidth = _document.DefaultPageSetup.PageWidth - _document.DefaultPageSetup.LeftMargin - _document.DefaultPageSetup.RightMargin;
            PageHeight = _document.DefaultPageSetup.PageHeight - _document.DefaultPageSetup.TopMargin - _document.DefaultPageSetup.BottomMargin;
        }

        public string CreatePdf()
        {
            // with the help of http://pdfsharp.net/wiki/Invoice-sample.ashx
            _document.Info.Title = _documentTitle;
            _document.Info.Author = "Promis.NET";

            DefineStyles();
            SaveImageToTempFile();

            AddHeaderTable();
            AddData();

            var renderer = new PdfDocumentRenderer(true)
            {
                Document = _document
            };
            renderer.RenderDocument();
            DoExtraWorkOnPdfDocument(renderer.PdfDocument);
            renderer.Save(FullPath);
            renderer = null;

            return Name;
        }

        private void AddHeaderTable()
        {
            section = _document.AddSection();
            section.PageSetup.DifferentFirstPageHeaderFooter = _hasDifferentFirstPageHeader;
            section.PageSetup.TopMargin = HeaderHeight;
            section.PageSetup.BottomMargin = FooterHeight;
            var rowHeight = "15";

            Table table;
            if (_hasDifferentFirstPageHeader)
                table = section.Headers.FirstPage.AddTable();
            else
                table = section.Headers.Primary.AddTable();

            table.Style = "TableHeader";
            table.Borders.Color = Colors.Gray;
            table.Borders.Width = 0.25;

            var columnImg = table.AddColumn(PageWidth / 3);
            columnImg.Format.Alignment = ParagraphAlignment.Center;
            var columnTitle = table.AddColumn(PageWidth / 6);
            columnTitle.Format.Alignment = ParagraphAlignment.Right;
            var columnText = table.AddColumn(PageWidth / 3);
            columnText.Format.Alignment = ParagraphAlignment.Left;
            var columnDate = table.AddColumn(PageWidth / 6);
            columnDate.Format.Alignment = ParagraphAlignment.Left;

            var row1 = table.AddRow();
            row1.Height = rowHeight;
            row1.VerticalAlignment = VerticalAlignment.Center;
            row1.Cells[1].Borders.Visible = false;
            row1.Cells[2].Borders.Visible = false;
            row1.Cells[3].Borders.Visible = false;
            row1.Cells[3].Borders.Right.Visible = true;
            row1.Cells[1].Borders.Top.Visible = true;
            row1.Cells[2].Borders.Top.Visible = true;
            row1.Cells[3].Borders.Top.Visible = true;
            var imageParagraph = row1.Cells[0].AddParagraph();
            var image = imageParagraph.AddImage(tempImagePath);
            image.Width = PageWidth / 4 - 10;
            image.LockAspectRatio = true;
            row1.Cells[0].MergeDown = 2;
            row1.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row1.Cells[1].AddParagraph(LocalResources.Project.ToUpper() + ": ");
            row1.Cells[2].AddParagraph(_project.Title);

            var row2 = table.AddRow();
            row2.Height = rowHeight;
            row2.VerticalAlignment = VerticalAlignment.Center;
            row2.Cells[1].Borders.Visible = false;
            row2.Cells[2].Borders.Visible = false;
            row2.Cells[3].Borders.Visible = false;
            row2.Cells[3].Borders.Right.Visible = true;
            row2.Cells[1].AddParagraph(LocalResources.Owner.ToUpper() + ": ");
            row2.Cells[2].AddParagraph(_project.Client);
            var pageParagraph = row2.Cells[3].AddParagraph();
            pageParagraph.AddText("Page ");
            pageParagraph.AddPageField();
            pageParagraph.AddText(" of ");
            pageParagraph.AddNumPagesField();

            var row3 = table.AddRow();
            row3.Height = rowHeight;
            row3.VerticalAlignment = VerticalAlignment.Center;
            row3.Cells[1].Borders.Visible = false;
            row3.Cells[2].Borders.Visible = false;
            row3.Cells[3].Borders.Visible = false;
            row3.Cells[3].Borders.Right.Visible = true;
            row3.Cells[1].Borders.Bottom.Visible = true;
            row3.Cells[2].Borders.Bottom.Visible = true;
            row3.Cells[3].Borders.Bottom.Visible = true;
            row3.Cells[1].AddParagraph(LocalResources.Location.ToUpper() + ": ");
            row3.Cells[2].AddParagraph(_project.Location);
            row3.Cells[3].AddParagraph(LocalResources.Date.ToUpper() + ": " + DateToAttach);

            //row 4  - THIS PART ONLY SHOWS UP ON THE OTHER PAGES
            if ((!_hasDifferentFirstPageHeader && !_hasExtraTextOnHeader) || (string.IsNullOrEmpty(_extraHeader1) && string.IsNullOrEmpty(_extraHeader2)))
                return;
            
            if (_hasDifferentFirstPageHeader)//copy it to primary if it was different
                section.Headers.Primary = section.Headers.FirstPage.Clone();
            table = section.Headers.Primary.Elements.OfType<Table>().FirstOrDefault();
            var row4 = table.AddRow();
            row4.Height = rowHeight;
            row4.VerticalAlignment = VerticalAlignment.Center;
            row4.Cells[0].AddParagraph(_extraHeader1);
            row4.Cells[0].MergeRight = 3;
            row4.Cells[0].Format.Alignment = ParagraphAlignment.Center;

            var row5 = table.AddRow();
            row5.Height = rowHeight;
            row5.VerticalAlignment = VerticalAlignment.Center;
            row5.Cells[0].AddParagraph(_extraHeader2);
            row5.Cells[0].MergeRight = 3;
            row5.Cells[0].Format.Alignment = ParagraphAlignment.Center;
        }

        protected abstract void AddData();
        protected virtual void DoExtraWorkOnPdfDocument(PdfDocument document) { }
    }
}
