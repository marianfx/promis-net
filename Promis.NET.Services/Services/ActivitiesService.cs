﻿using Promis.NET.DataAccess.Entities;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Services.Models.Activities;
using Promis.NET.Services.Models.Commissioning;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using System.Collections.Generic;
using System.Linq;
using Promis.NET.Localization;

namespace Promis.NET.Services.Services
{
    public class ActivitiesService : Service, IActivitiesService
    {
        private IActivitiesRepository _activitiesRepository;

        public ActivitiesService(IActivitiesRepository activitiesRepository) : base(activitiesRepository)
        {
            _activitiesRepository = activitiesRepository;
        }

        public List<Inspection> GetAllTestPackages()
        {
            var output = new List<Inspection>();
            var totalTests = _activitiesRepository.CountAll<testpack>();
            var allCertPackages = _activitiesRepository.GetAllCertifiedTestPacksWithData();
            foreach (var package in allCertPackages)
            {
                var inspection = new Inspection(package, totalTests);
                output.Add(inspection);
            }

            return output;
        }

        #region Hydrotest Packages ISO Assignment
        public List<string> GetPipingList(string isometric, string paramNeeded)
        {
            return _activitiesRepository.GetPipingList(isometric, paramNeeded);
        }

        public List<Datatables> GetIsometricsValues(string isometric, string neededParam)
        {
            var isometricData = new List<Datatables>();
            IEnumerable<descriptor> isoVals = new List<descriptor>();

            switch (isometric)
            {
                case "geographical":
                    isoVals = _activitiesRepository.GetGeographicalWBS();
                    break;
                case "system":
                    isoVals = _activitiesRepository.GetSystemsInfo(neededParam);
                    break;
                case "subsystem":
                    isoVals = _activitiesRepository.GetSubSystemsInfo(neededParam);
                    break;
            }

            if (isoVals.Count() == 0)
                return isometricData;

            foreach (var item in isoVals)
            {
                if (item == null)
                    continue;

                var code = item.code;
                var description = item.description;
                var dataTbl = new Datatables()
                {
                    Datatable = code,
                    Description = description
                };
                isometricData.Add(dataTbl);
            }

            return isometricData;
        }

        public bool AlreadyInserted(string selectedTag, string selectedIsometricTag, string system, string subsystem)
        {
            //select maintag
            var maintag = _activitiesRepository.GetMaintag(selectedIsometricTag);
            //count rows from testpack_link having the same maintag
            var testPackCount = _activitiesRepository.GetCountTagFromTestPack(selectedTag, maintag);
            if (testPackCount > 0)
                return false;

            var sumQtyForMaintag = _activitiesRepository.GetSketchesSum(maintag);
            _activitiesRepository.InsertIntoTestpacklink(selectedTag, maintag, sumQtyForMaintag.ToString(), system, subsystem);
            return true;
        }

        public bool AssignIso(Isometrics isometricData)
        {
            var isInserted = false;
            var hasErrors = false;
            var lastSystem = "";
            var lastSubsystem = "";
            var lastWbs = "";
             _activitiesRepository.DeleteTestPackLink(isometricData.TestpackTag);
            foreach (var iso in isometricData.Iso)
            {
                isInserted = AlreadyInserted(isometricData.TestpackTag, iso.IsoTag, iso.System, iso.Subsystem);
                if (!isInserted)
                    hasErrors = true;

                lastSystem = iso.System;
                lastSubsystem = iso.Subsystem;
                lastWbs = iso.Wbs;
            }
            _activitiesRepository.UpdateTestPack(isometricData.TestpackTag, lastSystem,lastSubsystem, lastWbs);
            return hasErrors;
        }
        #endregion

        #region System Punch Lists Management
        public List<PlBaseTables> GetAllSystemFromPunchlist(bool plOnly)
        {
            var result = new List<PlBaseTables>();
            foreach (var item in _activitiesRepository.GetAllSystemFromPunchlist(plOnly))
            {
                result.Add(new PlBaseTables(item));
            }
            return result;
        }

        public List<PlBaseTables> GetAllDisciplines()
        {
            var result = new List<PlBaseTables>();
            foreach (var item in _activitiesRepository.GetAllDisciplines())
            {
                result.Add(new PlBaseTables(item));
            }
            return result;
        }

        public List<PlBaseTables> GetAllOriginators()
        {
            var result = new List<PlBaseTables>();
            foreach (var item in _activitiesRepository.GetAllOriginators())
            {
                result.Add(new PlBaseTables(item));
            }
            return result;
        }

        public List<PlBaseTables> GetAllCategory()
        {
            var result = new List<PlBaseTables>();
            foreach (var item in _activitiesRepository.GetAllCategory())
            {
                result.Add(new PlBaseTables(item));
            }
            return result;
        }

        public List<PlBaseTables> GetAllActionsBy()
        {
            var result = new List<PlBaseTables>();
            foreach (var item in _activitiesRepository.GetAllActionsBy())
            {
                result.Add(new PlBaseTables(item));
            }
            return result;
        }

        public List<string> GetAllStatus()
        {
            return _activitiesRepository.GetAllStatus();
        }

        public List<PlBaseTables> GetAllCloser()
        {
            var result = new List<PlBaseTables>();
            foreach (var item in _activitiesRepository.GetAllCloser())
            {
                result.Add(new PlBaseTables(item));
            }
            return result;
        }

        public List<DataKeeper> GetColumnsNecessaryForPunchlistTable()
        {
            return _activitiesRepository.GetColumnsNecessaryForPunchlistTable();

        }

        public List<DataKeeper> GetColumnsNecessaryForPunchlistModalTable()
        {
            return _activitiesRepository.GetColumnsNecessaryForPunchlistModalTable();

        }

        public string GetClosureByRowId(string rowId)
        {
            return _activitiesRepository.GetClosureByRowId(rowId);
        }

        public string UpdateOrInsertPunchlistRow(Punchlist punchData, bool isInsert, string rowId)
        {
            string message = string.Empty;
            if (isInsert)
            {
                var data = new Dictionary<string, object>();
                data.Add("system", punchData.SYSTEM);
                data.Add("subsystem", punchData.SUBSYSTEM);
                data.Add("item", punchData.ITEM);
                data.Add("wd", punchData.DETAILS);
                data.Add("flag", punchData.CAT);
                data.Add("pl_disci", punchData.DISCIPLINE);
                data.Add("actionby", punchData.ACTIONBY);
                data.Add("orig", punchData.ORIGINATOR);
                data.Add("found_date", punchData.FOUNDDATE);
                data.Add("js", punchData.STATUS);
                data.Add("closer", punchData.CLOSER);
                data.Add("target_date", punchData.TARGETDATE);
                data.Add("closure_date", punchData.CLOSEDATE);
                data.Add("reference", punchData.DOC);
                data.Add("closure", punchData.CLOSURE);
                _activitiesRepository.InsertPunchlistRow(data);
                message = LocalResources.SuccessfullyInserted;
            }
            else
            {
                var data = new List<DataKeeper>()
                { new DataKeeper("item",punchData.ITEM),
                new DataKeeper("wd",punchData.DETAILS),
                new DataKeeper("flag",punchData.CAT),
                new DataKeeper("pl_disci",punchData.DISCIPLINE),
                new DataKeeper("actionby",punchData.ACTIONBY),
                new DataKeeper("orig",punchData.ORIGINATOR),
                new DataKeeper("found_date",punchData.FOUNDDATE),
                new DataKeeper("js",punchData.STATUS),
                new DataKeeper("closer",punchData.CLOSER),
                new DataKeeper("target_date",punchData.TARGETDATE),
                new DataKeeper("closure_date",punchData.CLOSEDATE),
                new DataKeeper("reference",punchData.DOC),
                new DataKeeper("closure",punchData.CLOSURE),
                };
                _activitiesRepository.UpdateById("punchlist", data, "id", rowId);
                message = LocalResources.UpdateSuccesful;
            }
            return message;
        }
        public List<PlBaseTables> GetDataTablesForModal(string system)
        {
            var results = new List<PlBaseTables>();
            var tables = _activitiesRepository.GetDataTablesForModal();
            foreach (var table in tables)
            {
                if (_activitiesRepository.CountRowsInTableBySystem(table.datatable, system) > 0)
                    results.Add(new PlBaseTables() { Code = table.datatable, Description = table.description });
            }
            return results;
        }

        #endregion
    }
}