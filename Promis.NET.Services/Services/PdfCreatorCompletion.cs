﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using Promis.NET.Localization;
using Promis.NET.Services.Models.Kpi;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Promis.NET.Services.Services
{
    public class PdfCreatorCompletion : PdfCreator, IPdfCreatorCompletion
    {
        private const string PW_DVG_TEXT = "PID / DWG";
        private Color headerColor = Color.FromCmyk(43, 6, 0, 0);
        private Color dtHeaderColor = Color.FromCmyk(0, 0, 0, 17);
        
        private string _systemCode;
        private string _systemDescription;
        private List<string> _pids = new List<string>();
        private List<string> _datatables = new List<string>();
        private Dictionary<string, IEnumerable<KpiSystem>> _data = new Dictionary<string, IEnumerable<KpiSystem>>();

        public PdfCreatorCompletion(string serverPath): base(serverPath)
        {
        }

        public PdfCreatorCompletion(string serverPath, string containerDirectory, string systemCode, string systemDescription, Project project, IEnumerable<string> pids, IEnumerable<string> datatables, Dictionary<string, IEnumerable<KpiSystem>> data): this(serverPath)
        {
            SetData(containerDirectory, systemCode, systemDescription, project, pids, datatables, data);
        }

        public void SetData(string containerDirectory, string systemCode, string systemDescription, Project project, IEnumerable<string> pids, IEnumerable<string> datatables, Dictionary<string, IEnumerable<KpiSystem>> data)
        {
            //use base
            SetData(containerDirectory, project, LocalResources.SystemComponentList + " " + systemCode, true, true, LocalResources.System.ToUpper() + ": " + systemCode, systemDescription);

            _systemCode = systemCode;
            _systemDescription = systemDescription;
            if (pids != null)
                _pids = pids.ToList();
            if (datatables != null)
                _datatables = datatables.ToList();
            if (data != null)
                _data = data;
        }

        protected override void AddData()
        {
            AddFooterFirstPageTable();
            AddFirstPageTitle();
            AddMiddleTables();
            AddDataTable();
        }
        
        private void AddFooterFirstPageTable()
        {
            section.Footers.Primary.AddParagraph();//dummy paragraph empty
            var table = section.Footers.FirstPage.AddTable();
            table.Style = "TableFooter";
            table.Borders.Color = Colors.Gray;
            table.Borders.Width = 0.5;

            var columnWidth = PageWidth / 7;
            var column = table.AddColumn(columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn(columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn(2 * columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn(columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn(columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn(columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;

            // three empty rows
            var rowHeight = "15";
            var row1 = table.AddRow();
            row1.Height = rowHeight;
            var row2 = table.AddRow();
            row2.Height = rowHeight;
            var row3 = table.AddRow();
            row3.Height = rowHeight;
            
            var row4 = table.AddRow();
            row4.Height = rowHeight;
            row4.VerticalAlignment = VerticalAlignment.Center;
            row4.Cells[0].AddParagraph(LocalResources.Rev.ToUpper() + ".");
            row4.Cells[1].AddParagraph(LocalResources.Date.ToUpper());
            row4.Cells[2].AddParagraph(LocalResources.Status.ToUpper());
            row4.Cells[3].AddParagraph(LocalResources.WrittenBy.ToUpper());
            row4.Cells[4].AddParagraph(LocalResources.CheckedBy.ToUpper());
            row4.Cells[5].AddParagraph(LocalResources.ApprovedBy.ToUpper());

            var row5 = table.AddRow();
            row5.Height = rowHeight;
            row5.VerticalAlignment = VerticalAlignment.Center;
            row5.Cells[0].MergeRight = 5;
            row5.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row5.Cells[0].AddParagraph(LocalResources.DocumentRevisions.ToUpper());
        }

        private void AddFirstPageTitle()
        {
            var delimiter = section.AddParagraph();
            delimiter.Format.SpaceAfter = PageHeight / 9;
            var table = section.AddTable();
            table.Style = "TableTitle";
            table.Borders.Visible = false;

            var columnImg = table.AddColumn(PageWidth);
            columnImg.Format.Alignment = ParagraphAlignment.Center;

            var row1 = table.AddRow();
            row1.Height = "25";
            row1.VerticalAlignment = VerticalAlignment.Center;
            row1.Cells[0].AddParagraph(LocalResources.SystemComponentList.ToUpper());

            var row2 = table.AddRow();
            row2.Height = "25";
            row2.VerticalAlignment = VerticalAlignment.Center;
            row2.Cells[0].AddParagraph(_systemCode);

            var row3 = table.AddRow();
            row3.Height = "25";
            row3.VerticalAlignment = VerticalAlignment.Center;
            row3.Cells[0].AddParagraph(_systemDescription);
        }

        private void AddMiddleTables()
        {
            // some space before
            var delimiter = section.AddParagraph();
            delimiter.Format.SpaceAfter = "20";

            // outer table
            var containerTable = section.AddTable();
            containerTable.Style = "TableDataStyle";
            containerTable.Borders.Visible = false;

            var bigColumn = (int)(PageWidth / 3);
            var smallColumn = (int)(PageWidth / 12);
            var column1 = containerTable.AddColumn(smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Center;
            column1 = containerTable.AddColumn(bigColumn);
            column1.Format.Alignment = ParagraphAlignment.Center;
            column1 = containerTable.AddColumn(smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Center;
            column1 = containerTable.AddColumn(smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Center;
            column1 = containerTable.AddColumn(bigColumn);
            column1.Format.Alignment = ParagraphAlignment.Center;
            column1 = containerTable.AddColumn(smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Center;

            var row1 = containerTable.AddRow();
            row1.VerticalAlignment = VerticalAlignment.Top;
            
            var rowHeight = "15";
            
            // left table pid/dwg
            var table1 = new Table();
            table1.Style = "TableDataStyle";
            table1.Borders.Color = Colors.Gray;
            table1.Borders.Width = 0.5;
            table1.LeftPadding = table1.RightPadding = 0;

            var column11 = table1.AddColumn(bigColumn / 2);
            column11.Format.Alignment = ParagraphAlignment.Center;
            column11 = table1.AddColumn(bigColumn / 2);
            column11.Format.Alignment = ParagraphAlignment.Center;

            var row11 = table1.AddRow();
            row11.Height = rowHeight;
            row11.VerticalAlignment = VerticalAlignment.Center;
            row11.Cells[0].AddParagraph(PW_DVG_TEXT);
            row11.Cells[0].Shading.Color = headerColor;
            row11.Cells[0].MergeRight = 1;
            row11.Cells[0].Format.Alignment = ParagraphAlignment.Center;

            // add the actual data; on two columns
            var count = 0;
            Row rowPid = new Row();
            foreach (var pid in _pids)
            {
                if(count % 2 == 0)
                {
                    rowPid = table1.AddRow();
                    rowPid.Height = rowHeight;
                    rowPid.VerticalAlignment = VerticalAlignment.Center;
                    rowPid.Cells[0].AddParagraph(pid.ToUpper());
                }
                else
                {
                    rowPid.Cells[1].AddParagraph(pid.ToUpper());
                }
                count++;
            }


            // right table datatables
            var table2 = new Table();
            table2.Style = "TableDataStyle";
            table2.Borders.Color = Colors.Gray;
            table2.Borders.Width = 0.5;
            table2.LeftPadding = table2.RightPadding = 0;

            var column21 = table2.AddColumn(bigColumn);
            column21.Format.Alignment = ParagraphAlignment.Center;

            var row21 = table2.AddRow();
            row21.Height = rowHeight;
            row21.VerticalAlignment = VerticalAlignment.Center;
            row21.Cells[0].AddParagraph(LocalResources.Datatables.ToUpper());
            row21.Cells[0].Shading.Color = headerColor;
            
            // add the actual data
            foreach (var datatable in _datatables)
            {
                var row = table2.AddRow();
                row.Height = rowHeight;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[0].AddParagraph(datatable.ToUpper());
            }

            // add the tables
            row1.Cells[1].Elements.Add(table1);
            row1.Cells[4].Elements.Add(table2);
        }

        private void AddDataTable()
        {
            // add start a new page
            section.AddPageBreak();

            // outer table
            var containerTable = section.AddTable();
            containerTable.Style = "TableDataStyleBold";
            containerTable.Borders.Color = Colors.Gray;
            containerTable.Borders.Width = 0.5;

            var smallColumn = PageWidth / 8;
            var column1 = containerTable.AddColumn(2 * smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Left;
            column1 = containerTable.AddColumn(3 * smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Left;
            column1 = containerTable.AddColumn(smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Center;
            column1 = containerTable.AddColumn(smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Center;
            column1 = containerTable.AddColumn(smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Center;

            // add header -> special formatiing
            var rowHeight = "15";
            var row1 = containerTable.AddRow();
            row1.Height = "20";
            row1.VerticalAlignment = VerticalAlignment.Center;
            row1.Shading.Color = headerColor;
            row1.Style = "TableFooter";
            row1.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row1.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row1.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row1.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row1.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            row1.Cells[0].AddParagraph(LocalResources.Tag.ToUpper());
            row1.Cells[1].AddParagraph(LocalResources.Description.ToUpper());
            row1.Cells[2].AddParagraph(LocalResources.Wbs.ToUpper());
            row1.Cells[3].AddParagraph(LocalResources.System.ToUpper());
            row1.Cells[4].AddParagraph(PW_DVG_TEXT);

            // add actual data
            foreach (var datatable in _datatables)
            {
                var entriesForThisDt = _data.Keys.Contains(datatable) ? _data[datatable] : new List<KpiSystem>();
                // add header row for datatables
                var rowDt = containerTable.AddRow();
                rowDt.Height = "20";
                rowDt.VerticalAlignment = VerticalAlignment.Center;
                rowDt.Style = "TableFooter";
                rowDt.Cells[0].Shading.Color = dtHeaderColor;
                var headerTxt = datatable.ToUpper() + " (" + entriesForThisDt.Count() + ")";
                rowDt.Cells[0].AddParagraph(headerTxt);
                rowDt.Cells[1].MergeRight = 3;

                foreach (var entry in entriesForThisDt)
                {
                    var row = containerTable.AddRow();
                    row.Height = rowHeight;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[0].AddParagraph(entry.Tag);
                    row.Cells[1].AddParagraph(entry.Description);
                    row.Cells[2].AddParagraph(entry.Wbs);
                    row.Cells[3].AddParagraph(entry.System);
                    row.Cells[4].AddParagraph(entry.Pid);
                }
            }
        }
    }
}
