﻿using Promis.NET.DataAccess.Entities.ProjectEntities;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Services.Extensions;
using Promis.NET.Services.Models.Kpi;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using Promis.NET.Localization;
using Promis.Web.DataAccess.Entities.PromisWeb;
using Promis.NET.Services.Models.Project;

namespace Promis.NET.Services.Services
{
    public class KpiService : Service, IKpiService
    {
        private IKpiRepository _kpiRepository;
        private IProgressRepository _progressRepository;
        private IProjectRepository _projectRepository;
        private IPdfCreatorCompletion _pdfCreator;

        public KpiService(IKpiRepository kpiRepository, IProgressRepository progressRepository, IProjectRepository projectRepository, IPdfCreatorCompletion pdfCreator) : base(kpiRepository, progressRepository, projectRepository)
        {
            _kpiRepository = kpiRepository;
            _progressRepository = progressRepository;
            _projectRepository = projectRepository;
            _pdfCreator = pdfCreator;
        }

        public List<KpiCategory> GetAllKpiCategories()
        {
            var eq = new KpiCategory("1", "abc") == new KpiCategory("1", "acb");

            var categories = _kpiRepository.GetAll<ftc_container>().Select(ftc => new KpiCategory(ftc.code, ftc.description)).ToList();
            categories = categories.Distinct().ToList();
            return categories;
        }

        public List<KpiData> GetAllKpis(string code = null, bool getFavorites = false)
        {
            var query = _kpiRepository.GetAll<ftc_container>();
            if (!string.IsNullOrWhiteSpace(code))
                query = query.Where(f => f.code == code);
            if (getFavorites)
                query = query.Where(f => f.fav == "x");

            return query.OrderBy(f => f.code).ThenBy(f => f.kpi)
                            .Select(f => new KpiData(f))
                            .ToList(); ;
        }

        public void UpdateFavoriteKpis(IEnumerable<KeyValuePair<string, bool>> updated)
        {
            if (updated == null)
                return;

            foreach (var kpi in updated)
            {
                var currentFtc = new ftc_container() { kpi = kpi.Key, fav = kpi.Value == true ? "x" : "" };
                var includedFields = new[] { "fav" };
                _kpiRepository.UpdateById<ftc_container, string>(currentFtc, "kpi", kpi.Key, includedFields: includedFields);
            }
        }


        public CompletionStrings GetCompletion(string system)
        {
            var dbData = _kpiRepository.GetById<completion, string>("SYSTEM", system);
            return new Completion(dbData).GetAsStrings();
        }

        public void SaveCompletion(completion data)
        {
            if (data == null || string.IsNullOrWhiteSpace(data.SYSTEM))
                throw new ArgumentNullException("system");

            var excludedFields = new string[] { "SYSTEM", "SYSTEM_DESCRIPTION" }.ToList();
            _kpiRepository.UpdateById<completion, string>(data, "SYSTEM", data.SYSTEM, excludedFields);
        }

        #region Subcontractor Performance Indicators
        public List<KPISubcontractors> GetSubContractors()
        {
            var subcontractors = new List<KPISubcontractors>();
            var subContractorsResult = _kpiRepository.GetSubContractors();
            foreach (var item in subContractorsResult)
            {
                var subcontractor = new KPISubcontractors()
                {
                    Code = item.code,
                    Description = item.subcontractorCol
                };
                subcontractors.Add(subcontractor);
            }
            return subcontractors;
        }

        public List<List<DataKeeper>> GetSOWData(string subcCode)
        {
            return _kpiRepository.GetRowsForSOWTable(subcCode);
        }

        public KpiAnalysis GetForecastAnalisysData(string projectDb, string subActivity, string selectedSubcontractor)
        {
            var lastqty = 0.0;
            var adw_resp = "";
            var month = "";
            var monthEnd = "";
            var yearStart = "";
            var yearEnd = "";
            var totQty = 0.0;
            var ended = false;
            var add_w = 0.0;
            var rit_comp = 0;          
            var lastaqty = 0.0;
            var work_w = 0.0;
            var lastMonth = "";
            var count = 0;
            var totQtyFirst = 0.0;
            var start = 0;
            var end = 0;

            var currentMonthFlag = DateTime.Now.GetFlagForYearAndMonth();
            //1.obtain act and qty
            var actAndQty = _kpiRepository.GetActAndQtyForSubact(subActivity);

            if (actAndQty != null)
            {
                lastqty = Math.Round(actAndQty.qty, 2);

                //2. Obtain start and end date
                var startAndEndTerm = _kpiRepository.GetActivityStartAndEnd(actAndQty.act);
                if (startAndEndTerm != null)
                {

                    var yearAndMonthStart = startAndEndTerm.start_date.Replace("p_", "");
                    yearStart = yearStart + yearAndMonthStart.Substring(0, 4);
                    var monthId = yearAndMonthStart.Substring(5, 2);
                    month = month + Localization.Global.Months[int.Parse(monthId) - 1];

                    var yearAndMonthEnd = startAndEndTerm.end_date.Replace("p_", "");
                    yearEnd = yearEnd + yearAndMonthEnd.Substring(0, 4);
                    var monthEndId = yearAndMonthEnd.Substring(5, 2);
                    monthEnd = monthEnd + Localization.Global.Months[int.Parse(monthEndId) - 1];
                    //3.get count from cut_off
                    var countCutOff = _kpiRepository.GetStartEndActivityDates(startAndEndTerm.start_date, startAndEndTerm.end_date);
                    start = countCutOff.Key;
                    end = countCutOff.Value;
      
                    //select din cut_off
                    var cutOff = _kpiRepository.GetCutOffData();
                    if (cutOff != null)
                    {
                        foreach (var item in cutOff)
                        {
                            count = count + 1;
                            var mese = item.m.Replace("p_", "");
                            var year_week = item.w.Replace("-", "_ ");
                            var week = year_week.Substring(5, 2);
                            var year = item.w.Substring(0, 4);
                            var actCode = subActivity.Substring(0, 2);
                            //get the db.project_name
                            var projName = _progressRepository.GetProgressTableName(projectDb, actCode, year, week);
                            //verify the existance of the table
                            var tableExists = _kpiRepository.DoesTableExist(projName);
                            if (tableExists)
                            {
                                totQtyFirst = _kpiRepository.GetTotalQty(subActivity, projName);
                                totQty = Math.Round(_kpiRepository.GetTotalQty(subActivity, projName), 2);
                                if (totQty == 0)
                                {
                                    var result1 = String.Compare(item.m, currentMonthFlag);
                                    if (count > end && !ended && result1 <= 0)
                                        rit_comp = rit_comp + 1;
                                }
                                else
                                {
                                    if (totQty > 0 && !ended)
                                        work_w = work_w + 1;
                                    var valq = totQty - lastaqty;

                                    if (valq < 0)
                                        valq = 0;

                                    if (lastqty == Math.Round(totQty, 2))
                                    {
                                        if (!ended)
                                        {
                                            rit_comp = rit_comp + 1;
                                        }
                                        ended = true;
                                    }
                                    else
                                    {
                                        if (count > end && !ended && String.Compare(item.m, currentMonthFlag) <= 0)
                                        {
                                            rit_comp = rit_comp + 1;
                                        }
                                    }
                                    lastaqty = totQty;
                                }
                            }
                            lastMonth = item.m;
                        }
                    }                 
                }
            }

            var rit_progr = "";
            var rit_completedTxt = "";
            if (rit_comp > 0)
                rit_completedTxt = rit_comp.ToString() + LocalResources.Weeks;
            else
                rit_completedTxt = LocalResources.NoDelay;

            if (actAndQty.qty > totQtyFirst)
                rit_progr = String.Format("{0:0,0.00}",lastqty - totQty).ToString();
            else rit_progr = LocalResources.No;

            if (work_w > 0)
            {
                work_w = Math.Round((lastqty / work_w), 2);
            }
                
            if (!ended)
            {
                if (work_w > 0)
                    add_w = Math.Round(((lastqty - totQty) / work_w), 2);
                else
                    add_w = 0;

                if (add_w > Math.Floor(add_w))
                {
                    add_w = Math.Floor(add_w) + 1;
                    adw_resp = add_w.ToString() + LocalResources.Weeks; //adw_resp = add_w.ToString() + " week(s)";
                }
                else adw_resp = LocalResources.No;

                
            }
            //last step:Get Planned And Actual Productivity Values
            var budgetPerSubactivity = _kpiRepository.GetBudgetForSubact(subActivity);

            var budgetKpi = new KpiAnalysis()
            {
                TotalQty = totQty,
                LastQty = Math.Round(actAndQty.qty, 2),
                AchievedProgress = (totQty / actAndQty.qty * 100),

                PlannedStart = month + " " + yearStart,
                PlannedEnd = monthEnd + " " + yearEnd,

                DelayedProgress = rit_progr,
                DelayedCompletion = rit_completedTxt,

                WeeklyQuantity = String.Format("{0:#,0.00}",work_w).ToString(),
                PlannedProductivity = budgetPerSubactivity.prod + " " + budgetPerSubactivity.um + "/Mh",
                ActualProductivity = String.Format("{0:0,0.00}",Math.Round((totQty / Convert.ToDouble(budgetPerSubactivity.mhrs)), 2).ToString())+ " " + budgetPerSubactivity.um + "/Mh",
                AditionalDelay = adw_resp
            };
            return budgetKpi;
        }

        public KPIsChartData GetKpiChartsData(string projectDb, string subcontractorCode, string subActivity)
        {
            var chartsModel = new KPIsChartData();

            var rit_comp = 0;
            var lastqty = 0.0;
            var totQty = 0.0;
            var ended = false;
            var lastaqty = 0.0;
            var work_w = 0.0;
            var lastMonth = "";
            var k = 0;
            var start = 0;
            var end = 0;
            var s_bar = new List<double?>();
            var rit = new List<double?>();
            var listToAddLabels = new List<string>(new string[] { "" });
            var lbl = new List<List<string>>();
            var subc_progress = new List<double?>();
            var wkqty = new List<double?>();

            var currentMonthFlag = DateTime.Now.GetFlagForYearAndMonth();
            //1.obtain act and qty
            var actAndQty = _kpiRepository.GetActAndQtyForSubact(subActivity);

            if (actAndQty == null)
                return chartsModel;

            lastqty = Math.Round(actAndQty.qty, 2);

            //2. Obtain start and end date
            var startAndEndTerm = _kpiRepository.GetActivityStartAndEnd(actAndQty.act);
            if (startAndEndTerm == null)
                return chartsModel;
            
            var yearAndMonthStart = startAndEndTerm.start_date.Replace("p_", "");
            //3.get count from cut_off
            var countCutOff = _kpiRepository.GetStartEndActivityDates(startAndEndTerm.start_date, startAndEndTerm.end_date);
            start = countCutOff.Key;
            end = countCutOff.Value;

            // init bar data
            for (int z = 1; z <= start; z++)
            {
                s_bar.Add(null);
                rit.Add(null);
            }
            s_bar.Add(lastqty);
            rit.Add(null);
            for (int z = start + 1; z <= end - 1; z++)
            {
                s_bar.Add(null);
                rit.Add(null);
            }
            s_bar.Add(lastqty); 
            //select from cut_off
            var cutOff = _kpiRepository.GetCutOffData();
            if (cutOff == null)
                return chartsModel;

            var count = 0;
            foreach (var item in cutOff)
            {
                count = count + 1;
                var month_year = item.m.Replace("p_", "");
                var monthCode = int.Parse(month_year.Substring(month_year.Length - 2, 2)) - 1;
                var year = item.w.Substring(0, 4);
                var year_week = item.w.Replace("-", "_ ");
                var week = year_week.Substring(5, 2);
                var actCode = subActivity.Substring(0, 2);
                //get the db.project_name
                var projName = _progressRepository.GetProgressTableName(projectDb, actCode, year, week);
                //verify the existance of the table
                var tableExists = _kpiRepository.DoesTableExist(projName);
                listToAddLabels = new List<string>();
                if (!tableExists)
                {
                    if (item.m == lastMonth)
                        listToAddLabels.AddRange(new[] {""});
                    else
                    {
                        if (k % 3 == 0)
                            listToAddLabels.AddRange(new[] { Localization.Global.Months[monthCode] ,  year});
                        else
                            listToAddLabels.AddRange(new[] { "" });                                   
                        k = k + 1;
                    }

                    var result = String.Compare(item.m, currentMonthFlag);
                    if (result <= 0)
                    {
                        subc_progress.Add(null);
                        wkqty.Add(null);
                    }

                    lbl.Add(listToAddLabels);
                    lastMonth = item.m;
                    continue;
                }
                
                totQty = Math.Round(_kpiRepository.GetTotalQty(subActivity, projName), 2);

                if (item.m == lastMonth)
                    listToAddLabels.AddRange(new[] { "" });
                else
                {
                    if (k % 3 ==0)
                        listToAddLabels.AddRange(new[] { Localization.Global.Months[monthCode],year});
                    else
                        listToAddLabels.AddRange(new[] { ""});
                    k = k + 1;
                }

                if (totQty == 0)
                {
                    if (!ended && String.Compare(item.m, currentMonthFlag) <= 0 && lastaqty > 0)
                    {
                        subc_progress.Add(Math.Round(lastaqty, 2));
                        wkqty.Add(Math.Round(0.0, 2));
                    }
                    else
                    {
                        subc_progress.Add(null);
                        wkqty.Add(null);
                    }
                    var result1 = String.Compare(item.m, currentMonthFlag);
                    if (count > end && !ended && result1 <= 0)
                    {
                        rit.Add(Math.Round(actAndQty.qty,2));
                        rit_comp = rit_comp + 1;
                    }
                }
                else
                {
                    if (totQty > 0 && !ended)
                        work_w = work_w + 1;
                    var valq = totQty - lastaqty;

                    if (valq < 0)
                        valq = 0;

                    if (lastqty == Math.Round(totQty, 2))
                    {
                        if (!ended)
                        {
                            subc_progress.Add(Math.Round(totQty, 2));
                            wkqty.Add(Math.Round(valq, 2));
                            if (count > end && String.Compare(item.m, currentMonthFlag) <= 0)
                            {
                                rit.Add(lastqty);
                                rit_comp = rit_comp + 1;
                            }                                         
                        }
                        ended = true;
                    }
                    else
                    {
                        if (count > end && !ended && String.Compare(item.m, currentMonthFlag) <= 0)
                            rit.Add(lastqty);

                        subc_progress.Add(Math.Round(totQty, 2));

                        if (!ended && String.Compare(item.m, currentMonthFlag) <= 0)
                            wkqty.Add(Math.Round(valq, 2));
                    }
                    lastaqty = totQty;
                }
                lbl.Add(listToAddLabels);
                lastMonth = item.m;
            }

            if (!ended && rit_comp > 0)
                rit.Add(lastaqty);

            if (work_w > 0)
                work_w = Math.Round((lastqty / work_w), 2);

            var w_line = new List<double>();
            for (int i = 1; i <= count; i++)
                w_line.Add(work_w);

            chartsModel.ChartBarsLabels=lbl;
            chartsModel.ChartBars = s_bar;
            chartsModel.ChartPr = subc_progress;
            chartsModel.ChartRit=rit;
            chartsModel.ChartWkty = wkqty;
            chartsModel.ChartWorkW = w_line;
            return chartsModel;
        }
        #endregion

        public string ExportMilestonesSystemDataToPdf(string serverDirectory, string projectId, string systemCode)
        {
            var fullSystemData = _kpiRepository.GetFullSystemData(systemCode);
            var projectData = _projectRepository.GetById<project, string>("proj", projectId);
            var projectObject = new Project(projectData);
            var allDataDb = _kpiRepository.GetDataForSystemCodeFromDatatables(systemCode);
            var allData = allDataDb == null ? new Dictionary<string, IEnumerable<KpiSystem>>()
                                            : allDataDb.ToDictionary(k => k.Key, v => v.Value.Select(x => new KpiSystem(x)));
            var allDatatables = allData.Keys;
            var allPids = new List<string>();
            foreach (var item in allData)
            {
                foreach (var subItem in item.Value)
                {
                    var pid = subItem.Pid;
                    if (!string.IsNullOrWhiteSpace(pid) && !allPids.Contains(pid))
                        allPids.Add(pid);
                }
            }

            _pdfCreator.SetData(serverDirectory, systemCode, fullSystemData.description, projectObject, allPids, allDatatables, allData);

            return _pdfCreator.CreatePdf();
        }
    }

}