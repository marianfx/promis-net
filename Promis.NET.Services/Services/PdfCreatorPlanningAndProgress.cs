﻿using MigraDoc.DocumentObjectModel;
using Promis.NET.Localization;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Services.Interfaces;
using System.Collections.Generic;
using Promis.NET.Services.Models.PlanningAndProgress;
using System;
using MigraDoc.DocumentObjectModel.Tables;
using System.Linq;

namespace Promis.NET.Services.Services
{
    public class PdfCreatorPlanningAndProgress : PdfCreator, IPdfCreatorPlanningAndProgress
    {

        private Color headerColor = Color.FromCmyk(43, 6, 0, 0);
        private Color dtHeaderColor = Color.FromCmyk(0, 0, 0, 17);
        private Color progressWeight1 = Color.FromCmyk(0, 100, 100, 40);//H990000   
        private Color progressWeight2 = Color.FromCmyk(67, 0, 100, 40);//H339900
        private Color progressWeight3 = Color.FromCmyk(0, 40, 100, 0);//HFF9900
        private Color progressWeight4 = Color.FromCmyk(0, 60, 100, 0);//HFF6600
        private Color progressWeight5 = Color.FromCmyk(0, 20, 60, 0);//HFFCC66
        private Color progressWeight6 = Color.FromCmyk(0, 0, 40, 0);//HFFFF99  
        private Color specColor = Color.FromCmyk(100, 33, 0, 40);//H006699
        private Color inspectColors = Color.FromCmyk(46, 18, 0, 2);

        private string _subActivityCode;
        private string _budget;
        private string _subActivityDescription;
        private string _docToAttach;
        private ProgressSteps _progressSteps;
        private IEnumerable<ProgressData> _progressData;
        private string _clientHiddenValue;

        public PdfCreatorPlanningAndProgress(string serverPath) : base(serverPath)
        {
        }

        public PdfCreatorPlanningAndProgress(string serverPath, string containerDirectory, Tuple<string, string> codeData, Project project, ProgressSteps progress, IEnumerable<ProgressData> progressData, string clientHiddenVal, string budget, string documentToAttach = null, params string[] signingUsers) : this(serverPath)
        {
            SetData(containerDirectory, codeData, project, progress, progressData, clientHiddenVal, budget, documentToAttach);
        }

        public void SetData(string containerDirectory, Tuple<string, string> subActivityData, Project project, ProgressSteps progressDetails, IEnumerable<ProgressData> progressData, string clientHiddenValue, string budget, string documentToAttach = null)
        {
            SetData(containerDirectory, project, "progress-report", false, false, "", subActivityData.Item1 + " - " + subActivityData.Item2);
            _subActivityCode = subActivityData.Item1;
            _subActivityDescription = subActivityData.Item2;
            _budget = budget;
            _clientHiddenValue = clientHiddenValue;
            _docToAttach = documentToAttach;
            if (progressDetails != null)
                _progressSteps = progressDetails;
            if (progressData != null)
                _progressData = progressData;

            HeaderHeight = 277;
            FooterHeight = 85;
        }

        protected override void AddData()
        {
            AddHeaderFirstTable();
            AddHeaderSecondTable();
            AddFooterForPageTable();
            AddDataTable();
        }

        private void AddHeaderFirstTable()
        {

            Table firstTable;
            firstTable = section.Headers.Primary.AddTable();

            firstTable.Style = "TableHeader";
            firstTable.Borders.Color = Colors.Gray;
            firstTable.Borders.Width = 0.25;
            firstTable.Format.Font.Name = "Arial";
            firstTable.Format.Font.Bold = false;
            firstTable.Format.Font.Size = 7;
            firstTable.Format.Font.Bold = false;

            var rowHeight = 15;
            var smallColumn = PageWidth / 12;
            var columnNew = firstTable.AddColumn(4 * smallColumn);
            columnNew.Format.Alignment = ParagraphAlignment.Left;
            var column1 = firstTable.AddColumn(2 * smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Left;
            column1 = firstTable.AddColumn(smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Center;
            column1 = firstTable.AddColumn(smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Center;
            column1 = firstTable.AddColumn(smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Center;
            column1 = firstTable.AddColumn(2 * smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Center;
            column1 = firstTable.AddColumn(smallColumn);
            column1.Format.Alignment = ParagraphAlignment.Center;

            var row11 = firstTable.AddRow();
            row11.Height = rowHeight;
            row11.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row11.Cells[0].AddParagraph(LocalResources.ThirdDetailWorksheet);
            row11.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row11.Cells[0].Borders.Bottom.Visible = false;
            row11.Cells[1].AddParagraph(LocalResources.Proj);
            row11.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row11.Cells[2].AddParagraph(LocalResources.Unit);
            row11.Cells[3].AddParagraph(LocalResources.Area);
            row11.Cells[4].AddParagraph(LocalResources.Doc);
            row11.Cells[5].AddParagraph(LocalResources.Rev);
            row11.Cells[6].AddParagraph(LocalResources.Sheet);

            var row22 = firstTable.AddRow();
            row22.Height = rowHeight;
            row22.Cells[0].AddParagraph(LocalResources.ProgressForm);
            row22.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row22.Cells[1].AddParagraph(_project.Code);
            row22.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row22.Cells[2].AddParagraph(LocalResources.All);
            row22.Cells[3].AddParagraph(LocalResources.All);
            row22.Cells[4].AddParagraph();

        }

        private void AddHeaderSecondTable()
        {
            var rowHeight = 15;
            Table secondTable;

            secondTable = section.Headers.Primary.AddTable();
            secondTable.Style = "TableHeader";
            secondTable.Borders.Color = Colors.Black;
            secondTable.Borders.Width = 0.5;
            secondTable.Format.Font.Name = "Arial";
            secondTable.Format.Font.Size = 5;
            secondTable.Format.Font.Bold = false;

            var smallCol = PageWidth / 22;
            var columnx = secondTable.AddColumn(0.5 * smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Left;
            columnx = secondTable.AddColumn(2.5 * smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Left;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Left;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Left;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = secondTable.AddColumn(0.5 * smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Left;
            columnx = secondTable.AddColumn(0.5 * smallCol);
            columnx.Format.Alignment = ParagraphAlignment.Center;


            var row1 = secondTable.AddRow();
            row1.Height = 25;
            row1.Borders.Bottom.Visible = true;
            row1.VerticalAlignment = VerticalAlignment.Center;
            row1.Shading.Color = headerColor;
            row1.Cells[0].AddParagraph();
            row1.Cells[0].MergeRight = 8;
            row1.Cells[0].Borders.Right.Visible = false;
            row1.Cells[8].Borders.Right.Visible = false;
            row1.Cells[9].Format.Alignment = ParagraphAlignment.Left;
            row1.Cells[9].AddParagraph("WORK STEPS");
            row1.Cells[9].Shading.Color = specColor;
            row1.Cells[9].MergeRight = 3;
            row1.Cells[9].Borders.Left.Visible = false;
            row1.Cells[9].Borders.Right.Visible = false;
            row1.Cells[12].Borders.Right.Visible = false;
            row1.Cells[13].AddParagraph();
            row1.Cells[13].MergeRight = 8;
            row1.Cells[13].Borders.Left.Visible = false;
            row1.Shading.Color = specColor;

            var row2 = secondTable.AddRow();
            row2.Height = rowHeight;
            row2.VerticalAlignment = VerticalAlignment.Center;
            row2.Cells[0].AddParagraph("6");
            row2.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row2.Cells[0].Borders.Top.Visible = true;
            row2.Cells[0].Borders.Right.Visible = true;
            row2.Cells[0].Shading.Color = specColor;
            row2.Cells[1].AddParagraph(_progressSteps.InspectionArray[5]);
            row2.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            row2.Cells[1].MergeRight = 8;
            row2.Cells[1].Shading.Color = Colors.White;
            row2.Cells[1].Borders.Right.Visible = false;
            row2.Cells[9].Borders.Right.Visible = false;
            row2.Cells[10].AddParagraph();
            row2.Cells[10].MergeDown = 5;
            row2.Cells[10].Shading.Color = Colors.White;
            row2.Cells[10].Borders.Left.Visible = false;
            row2.Cells[11].AddParagraph();
            row2.Cells[11].Borders.Right.Visible = false;
            row2.Cells[11].Borders.Bottom.Visible = false;
            row2.Cells[12].MergeRight = 8;
            row2.Cells[12].AddParagraph(_progressSteps.InspectionArray[6]);
            row2.Cells[12].Format.Alignment = ParagraphAlignment.Right;
            row2.Cells[12].Shading.Color = inspectColors;
            row2.Cells[12].Borders.Left.Visible = false;
            row2.Cells[12].Borders.Right.Visible = true;
            row2.Cells[11].Shading.Color = inspectColors;
            row2.Cells[21].AddParagraph("7");
            row2.Cells[21].Shading.Color = specColor;

            var row3 = secondTable.AddRow();
            row3.Height = rowHeight;
            row3.VerticalAlignment = VerticalAlignment.Center;
            row3.Cells[0].AddParagraph("5");
            row3.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row3.Cells[0].Borders.Top.Visible = true;
            row3.Cells[0].Borders.Right.Visible = true;
            row3.Cells[0].Shading.Color = specColor;
            row3.Cells[1].AddParagraph(_progressSteps.InspectionArray[4]);
            row3.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            row3.Cells[1].MergeRight = 7;
            row3.Cells[1].Shading.Color = inspectColors;
            row3.Cells[1].Borders.Right.Visible = false;
            row3.Cells[8].Borders.Right.Visible = false;
            row3.Cells[9].AddParagraph();
            row3.Cells[9].MergeDown = 4;
            row3.Cells[9].Shading.Color = inspectColors;
            row3.Cells[9].Borders.Left.Visible = false;
            row3.Cells[11].Shading.Color = inspectColors;
            row3.Cells[11].Borders.Right.Visible = true;
            row3.Cells[11].Borders.Bottom.Visible = false;
            row3.Cells[11].Borders.Top.Visible = false;
            //row3.Cells[12].MergeDown = 4;
            row3.Cells[12].AddParagraph();
            row3.Cells[12].Shading.Color = Colors.White;
            row3.Cells[12].Borders.Right.Visible = false;
            row3.Cells[12].Borders.Left.Visible = true;
            row3.Cells[12].Borders.Bottom.Visible = false;
            row3.Cells[13].AddParagraph(_progressSteps.InspectionArray[7]);
            row3.Cells[13].Format.Alignment = ParagraphAlignment.Right;
            row3.Cells[13].MergeRight = 7;
            row3.Cells[13].Borders.Left.Visible = false;
            row3.Cells[13].Shading.Color = Colors.White;
            row3.Cells[21].AddParagraph("8");
            row3.Cells[21].Shading.Color = specColor;
            row3.Cells[21].Borders.Top.Visible = true;
            row3.Cells[21].Borders.Left.Visible = true;

            var row4 = secondTable.AddRow();
            row4.Height = rowHeight;
            row4.VerticalAlignment = VerticalAlignment.Center;
            row4.Cells[0].AddParagraph("4");
            row4.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row4.Cells[0].Borders.Top.Visible = true;
            row4.Cells[0].Borders.Right.Visible = true;
            row4.Cells[0].Shading.Color = specColor;
            row4.Cells[1].AddParagraph(_progressSteps.InspectionArray[3]);
            row4.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            row4.Cells[1].MergeRight = 6;
            row4.Cells[1].Shading.Color = Colors.White;
            row4.Cells[1].Borders.Right.Visible = false; ;
            row4.Cells[7].Borders.Right.Visible = false;
            row4.Cells[8].AddParagraph();
            row4.Cells[8].MergeDown = 3;
            row4.Cells[9].AddParagraph();
            row4.Cells[9].Shading.Color = Colors.White;
            row4.Cells[9].MergeDown = 3;
            row4.Cells[9].Shading.Color = inspectColors;
            row4.Cells[11].Shading.Color = inspectColors;
            row4.Cells[12].Borders.Right.Visible = true;
            row4.Cells[11].Borders.Bottom.Visible = false;
            row4.Cells[11].Borders.Top.Visible = false;
            row4.Cells[12].MergeDown = 3;//Here
            row4.Cells[12].Borders.Left.Visible = true;
            row4.Cells[12].Borders.Right.Visible = true;
            row4.Cells[12].Borders.Bottom.Visible = false;
            row4.Cells[13].AddParagraph();
            row4.Cells[13].Shading.Color = inspectColors;
            row4.Cells[13].Borders.Right.Visible = false;
            row4.Cells[13].Borders.Left.Visible = true;
            row4.Cells[13].Borders.Bottom.Visible = false;
            row4.Cells[14].MergeRight = 6;
            row4.Cells[14].AddParagraph(_progressSteps.InspectionArray[8]);
            row4.Cells[14].Format.Alignment = ParagraphAlignment.Right;
            row4.Cells[14].Borders.Left.Visible = false;
            row4.Cells[14].Shading.Color = inspectColors;
            row4.Cells[21].AddParagraph("9");
            row4.Cells[21].Shading.Color = specColor;
            row4.Cells[21].Borders.Top.Visible = true;
            row4.Cells[21].Borders.Left.Visible = true;

            var row5 = secondTable.AddRow();
            row5.Height = rowHeight;
            row5.VerticalAlignment = VerticalAlignment.Center;
            row5.Cells[0].AddParagraph("3");
            row5.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row5.Cells[0].Borders.Top.Visible = true;
            row5.Cells[0].Borders.Right.Visible = true;
            row5.Cells[0].Shading.Color = specColor;
            row5.Cells[1].AddParagraph(_progressSteps.InspectionArray[2]);
            row5.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            row5.Cells[1].MergeRight = 5;
            row5.Cells[1].Shading.Color = inspectColors;
            row5.Cells[1].Borders.Right.Visible = false;
            row5.Cells[6].Borders.Right.Visible = false;
            row5.Cells[7].AddParagraph();
            row5.Cells[7].Shading.Color = inspectColors;
            row5.Cells[7].Borders.Left.Visible = false;
            row5.Cells[7].Borders.Bottom.Visible = false;
            row5.Cells[9].Shading.Color = inspectColors;
            row5.Cells[11].Shading.Color = inspectColors;
            row5.Cells[11].Borders.Bottom.Visible = false;
            row5.Cells[11].Borders.Top.Visible = false;
            row5.Cells[12].Borders.Right.Visible = true;
            row5.Cells[12].Borders.Left.Visible = true;
            row5.Cells[12].Borders.Top.Visible = false;
            row5.Cells[12].Borders.Bottom.Visible = false;
            row5.Cells[13].Shading.Color = inspectColors;
            row5.Cells[13].Borders.Right.Visible = true;
            row5.Cells[13].Borders.Top.Visible = false;
            row5.Cells[13].Borders.Bottom.Visible = false;
            row5.Cells[14].AddParagraph();
            row5.Cells[14].Borders.Left.Visible = true;
            row5.Cells[14].Borders.Bottom.Visible = false;
            row5.Cells[15].AddParagraph(_progressSteps.InspectionArray[9]);
            row5.Cells[15].Format.Alignment = ParagraphAlignment.Right;
            row5.Cells[15].MergeRight = 5;
            row5.Cells[15].Borders.Left.Visible = true;
            row5.Cells[14].Borders.Right.Visible = false;
            row5.Cells[21].AddParagraph("10");
            row5.Cells[21].Shading.Color = specColor;
            row5.Cells[21].Borders.Top.Visible = true;
            row5.Cells[21].Borders.Left.Visible = true;

            var row6 = secondTable.AddRow();
            row6.Height = rowHeight;
            row6.VerticalAlignment = VerticalAlignment.Center;
            row6.Cells[0].AddParagraph("2");
            row6.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row6.Cells[0].Borders.Top.Visible = true;
            row6.Cells[0].Borders.Right.Visible = true;
            row6.Cells[0].Shading.Color = specColor;
            row6.Cells[1].AddParagraph(_progressSteps.InspectionArray[1]);
            row6.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            row6.Cells[1].MergeRight = 4;
            row6.Cells[1].Borders.Right.Visible = false;
            row6.Cells[5].Borders.Right.Visible = false;
            row6.Cells[6].AddParagraph();
            row6.Cells[6].Borders.Bottom.Visible = false;
            row6.Cells[6].Borders.Left.Visible = false;
            row6.Cells[1].Shading.Color = Colors.White;
            row6.Cells[7].Shading.Color = inspectColors;
            row6.Cells[7].Borders.Bottom.Visible = false;
            row6.Cells[7].Borders.Top.Visible = false;
            row6.Cells[9].Shading.Color = inspectColors;
            row6.Cells[11].Shading.Color = inspectColors;
            row6.Cells[11].Borders.Bottom.Visible = false;
            row6.Cells[11].Borders.Top.Visible = false;
            row6.Cells[12].Borders.Right.Visible = true;
            row6.Cells[12].Borders.Left.Visible = true;
            row6.Cells[12].Borders.Top.Visible = false;
            row6.Cells[12].Borders.Bottom.Visible = false;
            row6.Cells[13].Shading.Color = inspectColors;
            row6.Cells[13].Borders.Bottom.Visible = false;
            row6.Cells[13].Borders.Left.Visible = true;
            row6.Cells[14].MergeDown = 1;
            row6.Cells[14].Borders.Bottom.Visible = false;
            row6.Cells[14].Borders.Right.Visible = true;
            row6.Cells[15].AddParagraph();
            row6.Cells[15].Borders.Bottom.Visible = false;
            row6.Cells[15].Borders.Right.Visible = false;
            row6.Cells[15].Borders.Left.Visible = true;
            row6.Cells[16].Borders.Left.Visible = false;
            row6.Cells[15].Shading.Color = inspectColors;
            row6.Cells[16].AddParagraph(_progressSteps.InspectionArray[10]);
            row6.Cells[16].Format.Alignment = ParagraphAlignment.Right;
            row6.Cells[16].MergeRight = 4;
            row6.Cells[16].Borders.Left.Visible = false;
            row6.Cells[16].Shading.Color = inspectColors;
            row6.Cells[21].AddParagraph("11");
            row6.Cells[21].Shading.Color = specColor;
            row6.Cells[21].Borders.Top.Visible = true;
            row6.Cells[21].Borders.Left.Visible = true;

            var row7 = secondTable.AddRow();
            row7.Height = rowHeight;
            row7.VerticalAlignment = VerticalAlignment.Center;
            row7.Cells[0].AddParagraph("1");
            row7.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row7.Cells[0].Borders.Top.Visible = true;
            row7.Cells[0].Borders.Right.Visible = true;
            row7.Cells[0].Shading.Color = specColor;
            row7.Cells[1].AddParagraph(_progressSteps.InspectionArray[0]);
            row7.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            row7.Cells[1].MergeRight = 4;
            row7.Cells[1].Shading.Color = inspectColors;
            row7.Cells[6].Borders.Top.Visible = false;
            row7.Cells[7].Borders.Top.Visible = false;
            row7.Cells[7].Shading.Color = inspectColors;
            row7.Cells[9].Shading.Color = inspectColors;
            row7.Cells[11].Shading.Color = inspectColors;
            row7.Cells[11].Borders.Top.Visible = false;
            row7.Cells[12].Borders.Right.Visible = true;
            row7.Cells[12].Borders.Left.Visible = true;
            row7.Cells[12].Borders.Top.Visible = false;
            row7.Cells[13].Shading.Color = inspectColors;
            row7.Cells[13].Borders.Right.Visible = true;
            row7.Cells[13].Borders.Top.Visible = true;
            row7.Cells[14].Borders.Right.Visible = true;
            row7.Cells[14].Borders.Left.Visible = true;
            row7.Cells[14].Borders.Top.Visible = false;
            row7.Cells[15].Shading.Color = inspectColors;
            row7.Cells[15].Borders.Top.Visible = false;
            row7.Cells[15].Borders.Left.Visible = true;
            row7.Cells[16].AddParagraph(_progressSteps.InspectionArray[11]);
            row7.Cells[16].Format.Alignment = ParagraphAlignment.Right;
            row7.Cells[16].MergeRight = 4;
            row7.Cells[16].Borders.Right.Visible = true;
            row7.Cells[21].AddParagraph("12");
            row7.Cells[21].Shading.Color = specColor;
            row7.Cells[21].Borders.Top.Visible = true;
            row7.Cells[21].Borders.Left.Visible = true;

            var row8 = secondTable.AddRow();
            row8.Height = rowHeight;
            row8.VerticalAlignment = VerticalAlignment.Center;
            row8.Cells[0].AddParagraph();
            row8.Cells[0].MergeRight = 4;
            row8.Cells[5].AddParagraph(LocalResources.ProgressWeightPerWorkStep);
            row8.Cells[5].MergeRight = 11;
            row8.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row8.Cells[17].AddParagraph(LocalResources.WeightOnActivity);
            row8.Cells[17].MergeRight = 4;
            row8.Cells[17].Format.Alignment = ParagraphAlignment.Center;
            row8.Cells[5].Borders.Top.Visible = true;
            row8.Cells[5].Shading.Color = specColor;

            var row9 = secondTable.AddRow();
            row9.Height = rowHeight;
            row9.VerticalAlignment = VerticalAlignment.Center;
            row9.Cells[0].AddParagraph(_subActivityCode + " - " + _subActivityDescription);
            row9.Cells[0].MergeRight = 4;
            row9.Cells[5].AddParagraph((GetTotalValue()[0] != 0 ? GetTotalValue()[0].ToString() : ""));
            row9.Cells[5].Shading.Color = ColorForCell(0);
            row9.Cells[5].Borders.Top.Visible = true;
            row9.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row9.Cells[6].AddParagraph(GetTotalValue()[1] != 0 ? GetTotalValue()[1].ToString() : "");
            row9.Cells[6].Shading.Color = ColorForCell(1);
            row9.Cells[7].AddParagraph(GetTotalValue()[2] != 0 ? GetTotalValue()[2].ToString() : "");
            row9.Cells[7].Shading.Color = ColorForCell(2);
            row9.Cells[8].AddParagraph(GetTotalValue()[3] != 0 ? GetTotalValue()[3].ToString() : "");
            row9.Cells[8].Shading.Color = ColorForCell(3);
            row9.Cells[9].AddParagraph(GetTotalValue()[4] != 0 ? GetTotalValue()[4].ToString() : "");
            row9.Cells[9].Shading.Color = ColorForCell(4);
            row9.Cells[10].AddParagraph(GetTotalValue()[5] != 0 ? GetTotalValue()[5].ToString() : "");
            row9.Cells[10].Shading.Color = ColorForCell(5);
            row9.Cells[11].AddParagraph(GetTotalValue()[6] != 0 ? GetTotalValue()[6].ToString() : "");
            row9.Cells[11].Shading.Color = ColorForCell(6);
            row9.Cells[12].AddParagraph(GetTotalValue()[7] != 0 ? GetTotalValue()[7].ToString() : "");
            row9.Cells[12].Shading.Color = ColorForCell(7);
            row9.Cells[13].AddParagraph(GetTotalValue()[8] != 0 ? GetTotalValue()[8].ToString() : "");
            row9.Cells[13].Shading.Color = ColorForCell(8);
            row9.Cells[14].AddParagraph(GetTotalValue()[9] != 0 ? GetTotalValue()[9].ToString() : "");
            row9.Cells[14].Shading.Color = ColorForCell(9);
            row9.Cells[15].AddParagraph(GetTotalValue()[10] != 0 ? GetTotalValue()[10].ToString() : "");
            row9.Cells[15].Shading.Color = ColorForCell(10);
            row9.Cells[16].AddParagraph(GetTotalValue()[11] != 0 ? GetTotalValue()[11].ToString() : "");
            row9.Cells[16].Shading.Color = ColorForCell(11);

            row9.Cells[17].AddParagraph(LocalResources.TotAchieved);
            row9.Cells[17].MergeDown = 1;
            row9.Cells[17].Format.Alignment = ParagraphAlignment.Center;
            if (_clientHiddenValue.Contains("Q"))
                row9.Cells[18].AddParagraph(LocalResources.EquivQtyAchieved);

            row9.Cells[18].MergeDown = 1;
            row9.Cells[18].Format.Alignment = ParagraphAlignment.Center;
            row9.Cells[19].AddParagraph(LocalResources.Notes);
            row9.Cells[19].MergeRight = 2;
            row9.Cells[19].MergeDown = 1;
            row9.Cells[19].Format.Alignment = ParagraphAlignment.Center;

            var row10 = secondTable.AddRow();
            row10.Height = rowHeight;
            row10.VerticalAlignment = VerticalAlignment.Center;
            row10.Cells[0].AddParagraph();
            row10.Cells[1].AddParagraph(LocalResources.Description.ToUpper());
            row10.Cells[1].Format.Alignment = ParagraphAlignment.Left;
            row10.Cells[1].MergeRight = 2;
            row10.Cells[4].AddParagraph(_progressSteps.UM);
            row10.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            //verify data for progress depending on position value
            row10.Cells[5].AddParagraph(_progressSteps.ProgressArray[0]);
            row10.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row10.Cells[5].Shading.Color = inspectColors;
            row10.Cells[6].AddParagraph(_progressSteps.ProgressArray[1]);
            row10.Cells[6].Shading.Color = Colors.White;
            row10.Cells[7].AddParagraph(_progressSteps.ProgressArray[2]);
            row10.Cells[7].Shading.Color = inspectColors;
            row10.Cells[8].AddParagraph(_progressSteps.ProgressArray[3]);
            row10.Cells[8].Shading.Color = Colors.White;
            row10.Cells[9].AddParagraph(_progressSteps.ProgressArray[4]);
            row10.Cells[9].Shading.Color = inspectColors;
            row10.Cells[10].AddParagraph(_progressSteps.ProgressArray[5]);
            row10.Cells[10].Shading.Color = Colors.White;
            row10.Cells[11].AddParagraph(_progressSteps.ProgressArray[6]);
            row10.Cells[11].Shading.Color = inspectColors;
            row10.Cells[12].AddParagraph(_progressSteps.ProgressArray[7]);
            row10.Cells[12].Shading.Color = Colors.White;
            row10.Cells[13].AddParagraph(_progressSteps.ProgressArray[8]);
            row10.Cells[13].Shading.Color = inspectColors;
            row10.Cells[14].AddParagraph(_progressSteps.ProgressArray[9]);
            row10.Cells[14].Shading.Color = Colors.White;
            row10.Cells[15].AddParagraph(_progressSteps.ProgressArray[10]);
            row10.Cells[15].Shading.Color = inspectColors;
            row10.Cells[16].AddParagraph(_progressSteps.ProgressArray[11]);
            row10.Cells[16].Shading.Color = Colors.White;
            row10.Borders.Bottom.Visible = false;
        }

        private double[] GetTotalValue()
        {
            var pr1 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[0]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[0]);
            var pr2 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[1]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[1]);
            var pr3 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[2]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[2]);
            var pr4 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[3]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[3]);
            var pr5 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[4]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[4]);
            var pr6 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[5]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[5]);
            var pr7 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[6]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[6]);
            var pr8 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[7]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[7]);
            var pr9 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[8]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[8]);
            var pr10 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[9]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[9]);
            var pr11 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[10]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[10]);
            var pr12 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[11]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[11]);

            var tot = 0;
            var tot1 = pr1 > 0 ? tot + pr1 : 0;
            var tot2 = pr2 > 0 ? tot1 + pr2 : 0;
            var tot3 = pr3 > 0 ? tot2 + pr3 : 0;
            var tot4 = pr4 > 0 ? tot3 + pr4 : 0;
            var tot5 = pr5 > 0 ? tot4 + pr5 : 0;
            var tot6 = pr6 > 0 ? tot5 + pr6 : 0;
            var tot7 = pr7 > 0 ? tot6 + pr7 : 0;
            var tot8 = pr8 > 0 ? tot7 + pr8 : 0;
            var tot9 = pr9 > 0 ? tot8 + pr9 : 0;
            var tot10 = pr10 > 0 ? tot9 + pr10 : 0;
            var tot11 = pr11 > 0 ? tot10 + pr11 : 0;
            var tot12 = pr12 > 0 ? tot11 + pr12 : 0;

            double[] totalValue = new double[12] { tot1, tot2, tot3, tot4, tot5, tot6, tot7, tot8, tot9, tot10, tot11, tot12 };
            return totalValue;
        }

        private void AddFooterForPageTable()
        {
            var table = section.Footers.Primary.AddTable();
            table.Style = "TableFooter";
            table.Borders.Color = Colors.Gray;
            table.Borders.Width = 0.5;

            table.Format.Font.Name = "Helvetica";
            table.Format.Font.Size = 9;
            table.Format.Font.Bold = false;

            var columnWidth = PageWidth / 6;
            var column = table.AddColumn(columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn(2 * columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn(columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn(columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn(columnWidth);
            column.Format.Alignment = ParagraphAlignment.Center;

            var row0 = table.AddRow();
            row0.Height = "15";
            row0.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row0.Cells[0].AddParagraph(LocalResources.Notes);
            row0.Cells[0].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Top;
            row0.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row0.Cells[0].MergeRight = 1;
            row0.Cells[0].MergeDown = 1;
            row0.Cells[2].AddParagraph(LocalResources.PreparedBy);
            row0.Cells[3].AddParagraph(LocalResources.CheckedBy);
            row0.Cells[4].AddParagraph(LocalResources.ApprovedBy);

            var row1 = table.AddRow();
            row1.Height = "35";
            row1.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row1.Cells[0].AddParagraph("");
            row1.Cells[0].MergeRight = 1;
            row1.Cells[2].AddParagraph();
            row1.Cells[3].AddParagraph();
            row1.Cells[4].AddParagraph();
        }


        private void AddDataTable()
        {
            var containerTable = section.AddTable();
            containerTable.Style = "TableDataStyle";
            containerTable.Borders.Color = Colors.Black;
            containerTable.Borders.Width = 0.5;
            containerTable.Format.Font.Name = "Arial";
            containerTable.Format.Font.Size = 4;

            var smallColumn = PageWidth / 22;
            var columnx = containerTable.AddColumn(0.5 * smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Left;
            columnx = containerTable.AddColumn(2.5 * smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Left;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Left;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Left;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            columnx = containerTable.AddColumn(0.5 * smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Left;
            columnx = containerTable.AddColumn(0.5 * smallColumn);
            columnx.Format.Alignment = ParagraphAlignment.Center;
            var rowHeightForTable = 15;

            var pr1 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[0]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[0]);
            var pr2 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[1]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[1]);
            var pr3 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[2]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[2]);
            var pr4 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[3]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[3]);
            var pr5 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[4]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[4]);
            var pr6 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[5]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[5]);
            var pr7 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[6]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[6]);
            var pr8 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[7]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[7]);
            var pr9 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[8]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[8]);
            var pr10 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[9]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[9]);
            var pr11 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[10]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[10]);
            var pr12 = string.IsNullOrWhiteSpace(_progressSteps.ProgressArray[11]) == true ? 0 : Convert.ToDouble(_progressSteps.ProgressArray[11]);

            int i = 1;
            var equiv_tot = 0.0;
            var qtyTotal = 0.0;
            double[] qty_tag = new double[12];

            foreach (var item in _progressData)
            {
                var rowData = containerTable.AddRow();
                rowData.Height = rowHeightForTable;
                rowData.VerticalAlignment = VerticalAlignment.Center;
                rowData.Cells[0].AddParagraph(i.ToString());
                rowData.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                rowData.Cells[1].AddParagraph(item.Tag);
                rowData.Cells[1].MergeRight = 2;
                rowData.Cells[1].Format.Alignment = ParagraphAlignment.Left;
                rowData.Cells[4].AddParagraph(item.Qty);

                rowData.Cells[5].AddParagraph(GetColorAndValueForCellsWithS(item.S1).Item1);
                rowData.Cells[5].Format.Alignment = ParagraphAlignment.Center;
                rowData.Cells[5].Format.Font.Color = GetColorAndValueForCellsWithS(item.S1).Item2;
                rowData.Cells[6].AddParagraph(GetColorAndValueForCellsWithS(item.S2).Item1);
                rowData.Cells[6].Format.Font.Color = GetColorAndValueForCellsWithS(item.S2).Item2;
                rowData.Cells[7].AddParagraph(GetColorAndValueForCellsWithS(item.S3).Item1);
                rowData.Cells[7].Format.Font.Color = GetColorAndValueForCellsWithS(item.S3).Item2;
                rowData.Cells[8].AddParagraph(GetColorAndValueForCellsWithS(item.S4).Item1);
                rowData.Cells[8].Format.Font.Color = GetColorAndValueForCellsWithS(item.S4).Item2;
                rowData.Cells[9].AddParagraph(GetColorAndValueForCellsWithS(item.S5).Item1);
                rowData.Cells[9].Format.Font.Color = GetColorAndValueForCellsWithS(item.S5).Item2;
                rowData.Cells[10].AddParagraph(GetColorAndValueForCellsWithS(item.S6).Item1);
                rowData.Cells[10].Format.Font.Color = GetColorAndValueForCellsWithS(item.S6).Item2;
                rowData.Cells[11].AddParagraph(GetColorAndValueForCellsWithS(item.S7).Item1);
                rowData.Cells[11].Format.Font.Color = GetColorAndValueForCellsWithS(item.S7).Item2;
                rowData.Cells[12].AddParagraph(GetColorAndValueForCellsWithS(item.S8).Item1);
                rowData.Cells[12].Format.Font.Color = GetColorAndValueForCellsWithS(item.S8).Item2;
                rowData.Cells[13].AddParagraph(GetColorAndValueForCellsWithS(item.S9).Item1);
                rowData.Cells[13].Format.Font.Color = GetColorAndValueForCellsWithS(item.S9).Item2;
                rowData.Cells[14].AddParagraph(GetColorAndValueForCellsWithS(item.S10).Item1);
                rowData.Cells[14].Format.Font.Color = GetColorAndValueForCellsWithS(item.S10).Item2;
                rowData.Cells[15].AddParagraph(GetColorAndValueForCellsWithS(item.S11).Item1);
                rowData.Cells[15].Format.Font.Color = GetColorAndValueForCellsWithS(item.S11).Item2;
                rowData.Cells[16].AddParagraph(GetColorAndValueForCellsWithS(item.S12).Item1);
                rowData.Cells[16].Format.Font.Color = GetColorAndValueForCellsWithS(item.S12).Item2;

                qtyTotal = qtyTotal + Math.Round(Convert.ToDouble(item.Qty), 2);
                var achieved = Convert.ToDouble(item.Equiv.ToString());

                if (Convert.ToDouble(item.Qty) > 0 && achieved > 0)
                {
                    var my_percent = Math.Round((achieved / Convert.ToDouble(item.Qty) * 100), 2);
                    if (my_percent > 100)
                    {
                        my_percent = 100;
                    }
                    rowData.Cells[17].AddParagraph(my_percent.ToString("0.00"));
                }

                double myDoubleEquiv;
                var result = double.TryParse(item.Equiv.ToString(), out myDoubleEquiv);
                if (result && Convert.ToDouble(item.Equiv.ToString()) > 0)
                {
                    if (_clientHiddenValue.Contains("Q"))
                        rowData.Cells[18].AddParagraph(Math.Round(Convert.ToDouble(item.Equiv.Replace(",", "."))).ToString("0.00"));
                    equiv_tot = equiv_tot + Math.Round(Convert.ToDouble(item.Equiv.ToString()), 2);
                }
                else
                    rowData.Cells[18].AddParagraph();

                rowData.Cells[19].AddParagraph();
                rowData.Cells[19].MergeRight = 2;


                var s1 = string.IsNullOrWhiteSpace(GetColorAndValueForCellsWithS(item.S1).Item1) == true ? 0.0 : Math.Round(Convert.ToDouble(item.S1), 2);
                var s2 = string.IsNullOrWhiteSpace(GetColorAndValueForCellsWithS(item.S2).Item1) == true ? 0 : Math.Round(Convert.ToDouble(item.S2), 2);
                var s3 = string.IsNullOrWhiteSpace((GetColorAndValueForCellsWithS(item.S3).Item1)) == true ? 0 : Math.Round(Convert.ToDouble(item.S3), 2);
                var s4 = string.IsNullOrWhiteSpace(GetColorAndValueForCellsWithS(item.S4).Item1) == true ? 0 : Math.Round(Convert.ToDouble(item.S4), 2);
                var s5 = string.IsNullOrWhiteSpace(GetColorAndValueForCellsWithS(item.S5).Item1) == true ? 0 : Math.Round(Convert.ToDouble(item.S5), 2);
                var s6 = string.IsNullOrWhiteSpace(GetColorAndValueForCellsWithS(item.S6).Item1) == true ? 0 : Math.Round(Convert.ToDouble(item.S6), 2);
                var s7 = string.IsNullOrWhiteSpace(GetColorAndValueForCellsWithS(item.S7).Item1) == true ? 0 : Math.Round(Convert.ToDouble(item.S7), 2);
                var s8 = string.IsNullOrWhiteSpace(GetColorAndValueForCellsWithS(item.S8).Item1) == true ? 0 : Math.Round(Convert.ToDouble(item.S8), 2);
                var s9 = string.IsNullOrWhiteSpace(GetColorAndValueForCellsWithS(item.S9).Item1) == true ? 0 : Math.Round(Convert.ToDouble(item.S9), 2);
                var s10 = string.IsNullOrWhiteSpace(GetColorAndValueForCellsWithS(item.S10).Item1) == true ? 0 : Math.Round(Convert.ToDouble(item.S10), 2);
                var s11 = string.IsNullOrWhiteSpace(GetColorAndValueForCellsWithS(item.S11).Item1) == true ? 0 : Math.Round(Convert.ToDouble(item.S11), 2);
                var s12 = string.IsNullOrWhiteSpace(GetColorAndValueForCellsWithS(item.S12).Item1) == true ? 0 : Math.Round(Convert.ToDouble(item.S12), 2);

                if (s1.ToString() == "100,00")
                    qty_tag[0] = qty_tag[0] + 100 * pr1 * Convert.ToDouble(item.Qty) / 10000;
                else
                    qty_tag[0] = qty_tag[0] + s1 * pr1 * Convert.ToDouble(item.Qty) / 10000;

                if (s2.ToString() == "100,00")
                    qty_tag[1] = qty_tag[1] + 100 * pr2 * Convert.ToDouble(item.Qty) / 10000;
                else
                    qty_tag[1] = qty_tag[1] + s2 * pr2 * Convert.ToDouble(item.Qty) / 10000;

                if (s3.ToString() == "100,00")
                    qty_tag[2] = qty_tag[2] + 100 * pr3 * Convert.ToDouble(item.Qty) / 10000;
                else
                    qty_tag[2] = qty_tag[2] + s3 * pr3 * Convert.ToDouble(item.Qty) / 10000;

                if (s4.ToString() == "100,00")
                    qty_tag[3] = qty_tag[3] + 100 * pr4 * Convert.ToDouble(item.Qty) / 10000;
                else
                    qty_tag[3] = qty_tag[3] + s4 * pr4 * Convert.ToDouble(item.Qty) / 10000;

                if (s5.ToString() == "100,00")
                    qty_tag[4] = qty_tag[4] + 100 * pr5 * Convert.ToDouble(item.Qty) / 10000;
                else
                    qty_tag[4] = qty_tag[4] + s5 * pr5 * Convert.ToDouble(item.Qty) / 10000;

                if (s6.ToString() == "100,00")
                    qty_tag[5] = qty_tag[5] + 100 * pr6 * Convert.ToDouble(item.Qty) / 10000;
                else
                    qty_tag[5] = qty_tag[5] + s6 * pr6 * Convert.ToDouble(item.Qty) / 10000;

                if (s7.ToString() == "100,00")
                    qty_tag[6] = qty_tag[6] + 100 * pr7 * Convert.ToDouble(item.Qty) / 10000;
                else
                    qty_tag[6] = qty_tag[6] + s7 * pr7 * Convert.ToDouble(item.Qty) / 10000;

                if (s8.ToString() == "100,00")
                    qty_tag[7] = qty_tag[7] + 100 * pr8 * Convert.ToDouble(item.Qty) / 10000;
                else
                    qty_tag[7] = qty_tag[7] + s8 * pr8 * Convert.ToDouble(item.Qty) / 10000;

                if (s9.ToString() == "100,00")
                    qty_tag[8] = qty_tag[8] + 100 * pr9 * Convert.ToDouble(item.Qty) / 10000;
                else
                    qty_tag[8] = qty_tag[8] + s9 * pr9 * Convert.ToDouble(item.Qty) / 10000;

                if (s10.ToString() == "100,00")
                    qty_tag[9] = qty_tag[9] + 100 * pr10 * Convert.ToDouble(item.Qty) / 10000;
                else
                    qty_tag[9] = qty_tag[9] + s10 * pr10 * Convert.ToDouble(item.Qty) / 10000;

                if (s11.ToString() == "100,00")
                    qty_tag[10] = qty_tag[10] + 100 * pr11 * Convert.ToDouble(item.Qty) / 10000;
                else
                    qty_tag[10] = qty_tag[10] + s11 * pr11 * Convert.ToDouble(item.Qty) / 10000;

                if (s12.ToString() == "100,00")
                    qty_tag[11] = qty_tag[11] + 100 * pr12 * Convert.ToDouble(item.Qty) / 10000;
                else
                    qty_tag[11] = qty_tag[11] + s12 * pr12 * Convert.ToDouble(item.Qty) / 10000;

                i++;
            }

            var row12 = containerTable.AddRow();//empty row
            row12.Cells[0].MergeRight = 21;

            var row13 = containerTable.AddRow();
            row13.Height = rowHeightForTable;
            row13.Format.Alignment = ParagraphAlignment.Right;
            row13.VerticalAlignment = VerticalAlignment.Center;
            if (_clientHiddenValue.Contains("Q"))
            {
                //var balanceValue = Convert.ToDouble(_budget) - qtyTotal;
                //row13.Cells[0].AddParagraph("Balance: " + (balanceValue.ToString()));
            }
            if (_clientHiddenValue.Contains("Q"))
            {
                row13.Cells[1].AddParagraph("BUDGET QTY");
                row13.Cells[1].Shading.Color = Colors.White;
            }

            row13.Cells[1].MergeRight = 2;
            row13.Cells[1].Format.Alignment = ParagraphAlignment.Right;

            if (_clientHiddenValue.Contains("Q"))
            {
                row13.Cells[4].AddParagraph(String.Format("{0:N2}", Math.Round(Convert.ToDouble(_budget), 2)));
                row13.Cells[4].Format.Font.Color = Colors.White;
                row13.Cells[4].Format.Alignment = ParagraphAlignment.Right;
                row13.Cells[4].Shading.Color = specColor;
            }
            row13.Cells[3].MergeRight = 1;
            row13.Cells[3].Format.Alignment = ParagraphAlignment.Right;

            row13.Cells[5].AddParagraph(Math.Round(GetTotalValueForTag(qty_tag[0], _budget, pr1.ToString()), 2).ToString("0.0"));
            row13.Cells[6].AddParagraph(Math.Round(GetTotalValueForTag(qty_tag[1], _budget, pr2.ToString()), 2).ToString("0.0"));
            row13.Cells[7].AddParagraph(Math.Round(GetTotalValueForTag(qty_tag[2], _budget, pr3.ToString()), 2).ToString("0.0"));
            row13.Cells[8].AddParagraph(Math.Round(GetTotalValueForTag(qty_tag[3], _budget, pr4.ToString()), 2).ToString("0.0"));
            row13.Cells[9].AddParagraph(Math.Round(GetTotalValueForTag(qty_tag[4], _budget, pr5.ToString()), 2).ToString("0.0"));
            row13.Cells[10].AddParagraph(Math.Round(GetTotalValueForTag(qty_tag[5], _budget, pr6.ToString()), 2).ToString("0.0"));
            row13.Cells[11].AddParagraph(Math.Round(GetTotalValueForTag(qty_tag[6], _budget, pr7.ToString()), 2).ToString("0.0"));
            row13.Cells[12].AddParagraph(Math.Round(GetTotalValueForTag(qty_tag[7], _budget, pr8.ToString()), 2).ToString("0.0"));
            row13.Cells[13].AddParagraph(Math.Round(GetTotalValueForTag(qty_tag[8], _budget, pr9.ToString()), 2).ToString("0.0"));
            row13.Cells[14].AddParagraph(Math.Round(GetTotalValueForTag(qty_tag[9], _budget, pr10.ToString()), 2).ToString("0.0"));
            row13.Cells[15].AddParagraph(Math.Round(GetTotalValueForTag(qty_tag[10], _budget, pr11.ToString()), 2).ToString("0.0"));
            row13.Cells[16].AddParagraph(Math.Round(GetTotalValueForTag(qty_tag[11], _budget, pr12.ToString()), 2).ToString("0.0"));

            var achievedTotal = Math.Round((equiv_tot / Convert.ToDouble(_budget)) * 100, 2);
            row13.Cells[17].AddParagraph(achievedTotal.ToString("0.00"));
            row13.Cells[17].Format.Font.Color = Colors.White;//
            row13.Cells[17].Shading.Color = specColor;
            if (Convert.ToDouble(_budget) > 0)
            {
                row13.Cells[18].AddParagraph(Math.Round(equiv_tot, 2).ToString("0.00"));
                row13.Cells[18].Format.Font.Color = Colors.White;
            }

            else
                row13.Cells[18].AddParagraph("0");
            row13.Cells[18].Shading.Color = specColor;
            row13.Cells[19].MergeRight = 2;
        }

        //method for getting the color of the cell
        private Color ColorForCell(int index)
        {
            var cellColor = new Color();
            if (_progressSteps.ProgressArray[index] == "")
                return Colors.White;
            else
            {
                var sum = _progressSteps.ProgressArray.Take(1 + index).Where(t => t != string.Empty).Select(t => Convert.ToInt32(t)).Sum();
                cellColor = sum == 100 ? progressWeight1 : (_progressSteps.LpArray[index] == "X" ? progressWeight2 : (_progressSteps.C[index] == "W" ? progressWeight3 : (_progressSteps.C[index] == "H" ? progressWeight4 : (_progressSteps.C[index] == "S" ? progressWeight5 : progressWeight6))));
                return cellColor;
            }

        }

        private Tuple<string, Color> GetColorAndValueForCellsWithS(string item)
        {
            if (item == "100,00")
                return new Tuple<string, Color>("NA", Colors.Red);
            if (item == "100.00")
                return new Tuple<string, Color>("100", Colors.Orange);
            if (item == "0")
                return new Tuple<string, Color>("", Color.Empty);

            return new Tuple<string, Color>(item, Color.Empty);
        }

        private double GetTotalValueForTag(double qtyTag, string budgetQty, string percent)
        {
            double valueForTag = 0.0;
            if (Convert.ToDouble(budgetQty) > 0)
            {
                if (Convert.ToDouble(percent) != 0)
                    valueForTag = valueForTag + (qtyTag * 10000) / (Convert.ToDouble(budgetQty) * Convert.ToDouble(percent));
            }
            return Math.Round(valueForTag, 1);
        }

    }
}
