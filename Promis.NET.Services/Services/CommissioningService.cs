﻿using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Models.Commissioning;
using Promis.NET.Services.Services.Interfaces;
using System.Collections.Generic;
using Promis.NET.Utilities.Models;
using System;
using Promis.NET.Services.Extensions;
using System.Linq;
using Promis.NET.Localization;

namespace Promis.NET.Services.Services
{
    public class CommissioningService : Service, ICommissioningService
    {

        private IDashboardRepository _dashboardRepository;
        private ICommissioningRepository _commissioningRepository;

        public CommissioningService(ICommissioningRepository commissioningRepository, IDashboardRepository dashboardRepository) : base(commissioningRepository, dashboardRepository)
        {
            _commissioningRepository = commissioningRepository;
            _dashboardRepository = dashboardRepository;
        }

        public ICommissioningRepository GetRepository()
        {
            return _commissioningRepository;
        }

        #region Funtions for common use
        public List<Datatables> GetDatatables(bool isFromSys = false)
        {
            var output = new List<Datatables>();
            var dbList = _commissioningRepository.GetDatatables(isFromSys);
            foreach (var item in dbList)
            {
                var condition = (isFromSys) ? item.Item3 : item.Item2;
                if (condition > 0)
                    output.Add(new Datatables(item.Item1));
            }
            return output;

        }

        public List<KeyValuePair<string, string>> GetTypecodesForDatatables(string tableName, bool isWithJoin = false)
        {
            var dbList = _commissioningRepository.GetTypecodesForDatatables(tableName, isWithJoin);
            return dbList;
        }       

        #endregion

        #region Status
        #region Progress&Systems
        public Dictionary<string, DatatablesInfo> GetDataTableInfoComplete()
        {
            var output = new Dictionary<string, DatatablesInfo>();
            var dataTblInfo = _commissioningRepository.GetDataTablesForStatusTab(false);
            if (dataTblInfo.Count == 0)
                return output;

            foreach (var item in dataTblInfo)
            {
                var datatable = item.datatable;

                var count = _commissioningRepository.GetDataTableCount(datatable);
                var dtInfo = new DatatablesInfo()
                {
                    Count1 = count.Key,
                    Count2 = count.Value,
                    Description = item.description,
                    Datatable = datatable
                };
                output.Add(datatable, dtInfo);
            }
            return output;
        }

        //adapted from dashboard
        public ChartsData GetChartsProgressStatusData(ref ChartsData chartsModel, string sufix)
        {
            chartsModel.ProgressStatusChartLabels = new List<List<string>>();
            chartsModel.ProgressStatusChartLabels = new List<List<string>>();
            chartsModel.ProgressStatusChartValuesActual = new List<double>();

            var lastValue = false;
            var continueAddingCommissioningActualValues = true;

            var commissioning_actual = 0.0;
            var currentMonthFlag = DateTime.Now.GetFlagForYearAndMonth();
            //get total man hours for precommissioning and commissioning
            var totalManHours = _commissioningRepository.GetManHoursForProjectStatus(sufix.ToUpper());
            //get all planning data
            var results = _dashboardRepository.GetAllPlanningData();

            for (var i = 6; i <= results.Columns.Count - 1; i++)
            {
                var column = results.Columns[i].ColumnName.ToLower();
                if (column == "id" || totalManHours == 0)
                    break;

                //add labels
                var listToAdd = new List<string>(new string[] { "" });
                var listToAddLabels = new List<string>(new string[] { "" });
                var yearAndMonth = column.Replace("p_", "");
                var year = yearAndMonth.Substring(0, 4);
                var monthId = yearAndMonth.Substring(5, 2);
                var month = Localization.Global.Months[int.Parse(monthId) - 1];

                // add planned value for Commissioning Chart
                var percent = _commissioningRepository.GetManHoursPlannedForStatus(column, totalManHours, sufix.ToUpper());
                var value = percent.HasValue ? Math.Round(percent.Value, 1) : 0;

                if (value > 0 && !lastValue)
                {
                    listToAddLabels.Clear();
                    listToAddLabels.AddRange(new[] { month, year });
                    chartsModel.ProgressStatusChartLabels.Add(listToAddLabels);
                    chartsModel.ProgressStatusChartValuesPlanned.Add(value);

                    if (!continueAddingCommissioningActualValues)
                        continue;

                    var sumEarned = _commissioningRepository.GetSumEarned("_" + sufix.ToLower(), column, totalManHours);
                    commissioning_actual = sumEarned ?? 0;
                    var valToAdd = Math.Round(commissioning_actual, 1);

                    if (column == currentMonthFlag)
                    {
                        continueAddingCommissioningActualValues = false;
                        break;
                    }
                    chartsModel.ProgressStatusChartValuesActual.Add(valToAdd);//add actual value for commissioning Chart

                    if (value == 100)
                    {
                        lastValue = true;
                        break;
                    }
                }
            }
            return chartsModel;
        }
        #endregion Progress&Systems

        #region Loop Tests by System
        public List<SystemModel> GetSystemListForLoops()
        {
            var output = new List<SystemModel>();
            var dbList = _commissioningRepository.GetSystemsForLoop();
            foreach (var item in dbList)
            {
                output.Add(new SystemModel(item));
            }
            return output;
        }

        public List<string> GetTagsForSystem(string system)
        {
            return _commissioningRepository.GetTagsForSystem(system);
        }

        public Dictionary<string, List<SystemInstrumentsTags>> GetDataForLoopTagsTable(string tag)
        {
            var dict = new Dictionary<string, List<SystemInstrumentsTags>>();
            var instrumentsList = new List<SystemInstrumentsTags>();
            var next = "";

            var instrumLoopData = _commissioningRepository.GetAllInstrumentsData(tag);
            foreach (var item in instrumLoopData)
            {
                var comp = item.comp;
                var stepAct = _commissioningRepository.GetInfoForInstruments(comp);
                var act = (stepAct == null) ? "na" : stepAct.sub_act;
                var inspection = (stepAct == null) ? "na" : stepAct.inspection;
                var step = stepAct == null ? "0" : stepAct.step;

                var progressSum = _commissioningRepository.GetProgressSum(comp);
                if (progressSum > 0)
                {
                    var nextStep = _commissioningRepository.GetNextStepForLoop(step, act);
                    if (nextStep != null)
                        next = nextStep.inspection + "- >" + nextStep.progr + " %";
                    else
                        next = LocalResources.ReadyToLoopCheck;
                }
                else
                    next = "";

                var dtInfo = new SystemInstrumentsTags()
                {
                    Comp = comp,
                    Description = item.description,
                    Act = act,
                    Type = item.type,
                    System = item.system,
                    LastStep = inspection,
                    NextStep = next,
                    Progress = progressSum.ToString()
                };

                instrumentsList.Add(dtInfo);
                if (!dict.ContainsKey(tag))
                    dict.Add(tag, new List<SystemInstrumentsTags>());
                dict[tag] = instrumentsList;
            }
            return dict;
        }

        #endregion Loop Tests by System

        #region Certification
        public Dictionary<string, CommissioningCertificationCompletion> GetCertificationCompletionData()
        {
            var output = new Dictionary<string, CommissioningCertificationCompletion>();
            var totalPrecommissioning = 0.0;
            var totalCommissioning = 0.0;
            var dataTblInfo = _commissioningRepository.GetDataTablesForStatusTab();
            if (dataTblInfo.Count == 0)
                return output;

            foreach (var item in dataTblInfo)
            {
                var datatable = item.datatable;
                var countPrecommissioning = _commissioningRepository.GetDataTableCount(datatable, "pr");
                totalPrecommissioning = countPrecommissioning.Key + countPrecommissioning.Value;
                var resultPrecomm = Math.Round((countPrecommissioning.Key / totalPrecommissioning) * 100, 1);

                var countCommissioning = _commissioningRepository.GetDataTableCount(datatable, "co");
                totalCommissioning = countCommissioning.Key + countCommissioning.Value;
                var resultComm = Math.Round((countCommissioning.Key / totalCommissioning) * 100, 1);

                var dtInfo = new CommissioningCertificationCompletion()
                {
                    Count1Commissioning = countCommissioning.Key,
                    Count2Commissioning = countCommissioning.Value,
                    Count1Precommissioning = countPrecommissioning.Key,
                    Count2Precommissioning = countPrecommissioning.Value,
                    PrecomProgress = resultPrecomm,
                    CommissioningProgress = resultComm,
                    Description = item.description,
                    Datatable = datatable
                };
                output.Add(datatable, dtInfo);
            }
            return output;
        }
        #endregion Certification
        #endregion

        #region Assign Inspection to tags

        public List<Activity> GetSubactForDatatables(string tableName, string activity)
        {
            var output = new List<Activity>();
            var dbList = _commissioningRepository.GetSubactForDatatables(tableName, activity);
            foreach (var item in dbList)
            {
                output.Add(new Activity(item));
            }
            return output;
        }

        public List<DataKeeper> GetColumnsNecessaryForTagsTable(string mainActivity)
        {
            return _commissioningRepository.GetColumnsNecessaryForTagsTable(mainActivity);
        }

        public List<ActivityTag> GetTagAndActivityforDatatables(string prefix, string tableName, List<string> contor)
        {
            var result = new List<ActivityTag>();
            var dbResult = _commissioningRepository.GetTagAndActivityforDatatables(prefix, tableName, contor);
            foreach (var item in dbResult)
            {
                result.Add(new ActivityTag(item));
            }
            return result;
        }

        public string UpdateTagAndActivityforDatatables(CommissioningInspectionTagsProgress commData)
        {
            List<string> saveMessages = new List<string>();
            
            var actList = GetTagAndActivityforDatatables(commData.MainActivity, commData.Datatable, commData.SelectedRows);
            foreach (var item in actList)
            {
                if (item.Activity.Equals(commData.Subactivity))
                    continue;

                if (!item.Activity.Equals(""))
                {
                    saveMessages.Add(" TAG > " + item.Activity);
                    continue;
                }
                _commissioningRepository.CreateCertTableRow(commData.Datatable, item.Tag, commData.Subactivity);
                _commissioningRepository.UpdateById(commData.Datatable, new List<DataKeeper>() { new DataKeeper(commData.MainActivity + "activity", commData.Subactivity) }, "id", item.Id);
            }
            var message = "";
            if (saveMessages.Count > 0)
            { message = LocalResources.TagsAssignedExceptFor + " " + string.Join(",", saveMessages.Distinct()); }
            else
            {
                message = LocalResources.TagsAssigned;
            }
            return message;
        }

        public void UpdateTagAndDeleteActivityforDatatables(CommissioningInspectionTagsProgress commData)
        {
            var actList = GetTagAndActivityforDatatables(commData.MainActivity, commData.Datatable, commData.SelectedRows);
            foreach (var item in actList)
            {
                if (item.Activity.Equals(""))
                    continue;

                _commissioningRepository.DeleteCertTableRow(commData.Datatable, item.Tag, item.Activity);
                _commissioningRepository.UpdateById(commData.Datatable, new List<DataKeeper>() { new DataKeeper(commData.MainActivity + "activity", "") }, "id", item.Id);
            }

        }
        #endregion

        #region System subdivision
        public List<DataKeeper> GetColumnsNecessaryForSystemTable()
        {
            return _commissioningRepository.GetColumnsNecessaryForSystemTable();
        }

        public List<SystemModel> GetSystemList()
        {
            var output = new List<SystemModel>();
            var dbList = _commissioningRepository.GetSystemList();
            foreach (var item in dbList)
            {
                output.Add(new SystemModel(item));
            }
            return output;
        }

        public Tuple<string, string, string> GetTotalCountsAndPercentage(string dataTable)
        {
            int tottags = 0;
            int totass = 0;
            string result = "0";
            if (!string.IsNullOrWhiteSpace(dataTable))
            {
                var countsForTable= _commissioningRepository.GetTagCountsAndPercentageForTable(dataTable);
                if(countsForTable.Key > 0)
                    tottags = countsForTable.Key;

                if (countsForTable.Value > 0)
                    totass = countsForTable.Value;

                if (tottags > 0) { result = string.Format("{0:0.0}", ((double)totass / tottags) * 100); }
                return new Tuple<string, string, string>(result, string.Format("{0:N0}", tottags), string.Format("{0:N0}", totass));
            }
                           
            var dbList = _commissioningRepository.GetDatatables(true);
            foreach (var item in dbList)
            {
                if (item.Item2 > 0)
                    tottags = tottags + item.Item2;

                if (item.Item3 > 0)
                    totass = totass + item.Item3;
            }
            if (tottags > 0) { result = string.Format("{0:0.0}", ((double)totass / tottags) * 100); }
            return new Tuple<string, string, string>(result, string.Format("{0:N0}", tottags), string.Format("{0:N0}", totass));

        }

        public void UpdateOrDeleteSysforDatatables(CommissioningSysProgress commData)
        {
            foreach (var item in commData.SelectedRows)
            {
                _commissioningRepository.UpdateById(commData.Datatable, new List<DataKeeper>() { new DataKeeper("system", commData.System) }, "id", item);
            }
        }

        #endregion

        #region Inspection Follow Up
        public List<DataKeeper> GetColumnsNecessaryForFollowUpTable()
        {
            return _commissioningRepository.GetColumnsNecessaryForFollowUpTable();

        }

        public KeyValuePair<List<CommissioningFollowUpStatus>, ActivityBase> GetStatusOfSelectedRow(string datatable, string rowId, string activity)
        {
            var tablePrefix = "precomm_";
            var keyresult = new List<CommissioningFollowUpStatus>();
            var subactDetails = new ActivityBase();
            if (activity.Equals("CO")) { tablePrefix = "comm_"; }
            var subatAndTag = _commissioningRepository.GetTagAndSubAct(datatable, rowId, activity+"_");
            subactDetails.Description = _commissioningRepository.GetSubactForFollowUpSelectedRow(subatAndTag.activity).activity;
            subactDetails.Code = subatAndTag.activity;
            foreach (var item in _commissioningRepository.GetAllForInspectionStatus(tablePrefix + datatable, subatAndTag.tag))
            {
                keyresult.Add(new CommissioningFollowUpStatus()
                {
                    Description = item.inspection,
                    result = (item.result.Equals("N")) ? true : false,
                    inspection = item.c_user + " " + item.code,
                    ord = item.ord
                });
            }
            return new KeyValuePair<List<CommissioningFollowUpStatus>, ActivityBase>(keyresult, subactDetails);
        }

        public void UpdateinspectionforDatatables(string dataTable, string userName, string tag, string ord, string result)
        {
            _commissioningRepository.UpdateInspectionsForDatatable(dataTable, userName, tag, ord, result);
        }
        #endregion
    }
}

