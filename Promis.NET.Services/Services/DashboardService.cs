﻿using System;
using System.Collections.Generic;
using System.Data;
using Promis.NET.Services.Extensions;
using Promis.NET.Utilities.Models;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Services.Models.Dashboard;

namespace Promis.NET.Services.Services
{
    public class DashboardService: Service, IDashboardService
    {
        private IDashboardRepository _dashboardRepository;

        public DashboardService(IDashboardRepository dashboardRepository): base(dashboardRepository)
        {
            _dashboardRepository = dashboardRepository;
        }

        public DashboardData GetChartsProgressData(ref DashboardData chartsModel, string activity = null)
        {
            chartsModel.ProgressChartLabels = new List<List<string>>();
            chartsModel.ProgressChartValuesPlanned = new List<double>();
            chartsModel.ProgressChartValuesActual = new List<double>();
            chartsModel.ProgressChartValuesCertified = new List<double>();
            chartsModel.ProgressMonthlyChartValuesActual = new List<double>();

            var k = 0;
            var continueAddingActualValues = true;
            var lastValue = 0.0;
            var currentMonthFlag = DateTime.Now.GetFlagForYearAndMonth();
            
            //get total man hours
            var totalManHours = _dashboardRepository.GetManHoursForProject(activity);
            
            //get all planning data
            var results = _dashboardRepository.GetAllPlanningData();

            for (var i = 6; i <= results.Columns.Count - 1; i++)
            {
                var column = results.Columns[i].ColumnName.ToLower();
                if (column == "id" || totalManHours == 0)
                    break;

                //add labels
                k = (k + 1) % 3;
                var listToAdd = new List<string>(new string[] { "" });
                var yearAndMonth = column.Replace("p_", "");
                if (k == 0)
                {
                    var year = yearAndMonth.Substring(0, 4);
                    var monthId = yearAndMonth.Substring(5, 2);
                    var month = Localization.Global.Months[int.Parse(monthId) - 1];//use "trad" to translate
                    listToAdd.Clear();
                    listToAdd.AddRange(new[] { month, year });
                }
                chartsModel.ProgressChartLabels.Add(listToAdd);

                // add planned value
                var percent = _dashboardRepository.GetManHoursPlanned(column, totalManHours, activity);
                var value = Math.Round(percent, 1);
                chartsModel.ProgressChartValuesPlanned.Add(value);

                // add actual value
                if (!continueAddingActualValues)
                    continue;

                percent = _dashboardRepository.GetMonthActualPercent(totalManHours, column, activity);
                value = percent;

                var valToAdd = Math.Round(value, 1);
                chartsModel.ProgressChartValuesActual.Add(valToAdd);
                chartsModel.ProgressChartValuesCertified.Add(Math.Round(value * 0.8, 1));
                chartsModel.ProgressMonthlyChartValuesActual.Add(Math.Round(valToAdd - lastValue, 1));
                lastValue = valToAdd;

                if (column == currentMonthFlag)
                    continueAddingActualValues = false;
            }

            return chartsModel;
        }
        
        public DashboardData GetChartsDelayData(ref DashboardData chartsModel)
        {
            chartsModel.ProgressMonthlyChartValuesDelay = new List<double>();

            if (chartsModel.ProgressChartValuesPlanned == null || chartsModel.ProgressChartValuesPlanned.Count == 0 || chartsModel.ProgressChartValuesActual == null || chartsModel.ProgressChartValuesActual.Count == 0)
                GetChartsProgressData(ref chartsModel);//autoload previous data if not available
            
            for (var i = 0; i <= chartsModel.ProgressChartValuesActual.Count - 1; i++)
            {
                var planned = chartsModel.ProgressChartValuesPlanned[i];
                var actual = chartsModel.ProgressChartValuesActual[i];

                var percentDelay = Math.Round(planned - actual, 1);
                chartsModel.ProgressMonthlyChartValuesDelay.Add(percentDelay);
            }

            return chartsModel;
        }

        public DashboardData GetChartsMainActivitiesData(ref DashboardData chartsModel)
        {
            chartsModel.ActivitiesCodes = new List<string>();
            chartsModel.ActivitiesPlannedData = new List<double>();
            chartsModel.ActivitiesActualData = new List<double>();
            chartsModel.ActivitiesWithProgresses = new List<ActivityWithProgress>();

            var month = DateTime.Now.MonthAsTwoDigitsString();
            var year = DateTime.Now.YearAsString();
            var week = DateTime.Now.WeekOfTheYearAsTwoDigitString();

            var results = _dashboardRepository.GetMainActivities(year, month);

            foreach (DataRow row in results.Rows)
            {
                var code = Global.DataCellToString(row["cod"]);
                var manHours = double.Parse(Global.DataCellToString(row["mhrs"], "0"));
                var hasPercentage = double.TryParse(Global.DataCellToString(row["pr"]), out double percentPlanned);
                if (!hasPercentage)
                    continue;

                var percent = _dashboardRepository.GetActualPercentByActivity(manHours, code, year, week);
                var percentActualValue = percent ?? 0;

                var activityWithProgress = new ActivityWithProgress()
                {
                    Code = code,
                    Description = Global.DataCellToString(row["descr"]),
                    ProgressPlanned = percentPlanned,
                    ProgressActual = percentActualValue
                };

                chartsModel.ActivitiesCodes.Add(code);
                chartsModel.ActivitiesPlannedData.Add(percentPlanned);
                chartsModel.ActivitiesActualData.Add(percentActualValue);
                chartsModel.ActivitiesWithProgresses.Add(activityWithProgress);
            }

            return chartsModel;
        }

        public DashboardData GetChartsTrendsData(ref DashboardData chartsModel)
        {
            //get total man hours
            var totalManHours = _dashboardRepository.GetManHoursForProject();

            var year = DateTime.Now.YearAsString();
            var weekNumber = DateTime.Now.WeekOfTheYearAsTwoDigitString();

            // get actual percent
            var percentData = _dashboardRepository.GetTrendsActualData(totalManHours, year, weekNumber);
            chartsModel.TrendingChartActualPercent = percentData.Percent; 
            chartsModel.TrendingChartActualPercentWeekly = percentData.PercentWeekly;


            //get monthly percent
            var percentDataMonthly = _dashboardRepository.GetTrendsMonthlyData(totalManHours, percentData.Month);
            chartsModel.TrendingChartActualPercentMonthly = percentDataMonthly.PercentMonthly;


            // get Percent Cumulated
            var percentDataCumulated = _dashboardRepository.GetTrendsPlannedData(percentData.Month, percentData.Week, percentDataMonthly.Month);
            chartsModel.TrendingChartPlannedPercent = percentDataCumulated.Percent;
            chartsModel.TrendingChartPlannedPercentMonthly = percentDataCumulated.PercentMonthly;
            chartsModel.TrendingChartPlannedPercentWeekly = percentDataCumulated.PercentWeekly;

            //set percent certified (module not done yet)
            chartsModel.TrendingChartCertifiedPercent = Math.Round(chartsModel.TrendingChartActualPercent * 0.8, 2);
            chartsModel.TrendingChartCertifiedPercentWeekly = Math.Round(chartsModel.TrendingChartActualPercentWeekly * 0.8, 2);
            chartsModel.TrendingChartCertifiedPercentMonthly = Math.Round(chartsModel.TrendingChartActualPercentMonthly * 0.8, 2);

            return chartsModel;
        }

        public DashboardData GetChartsOutstandingsData(ref DashboardData chartModel)
        {
            chartModel.OutstandingChartFlags = new List<string>();
            chartModel.OutstandingChartFlagDescriptions = new List<string>();
            chartModel.OutstandingChartCounts = new List<double>();
            chartModel.PunchlistDoneCount = 0;
            chartModel.PunchlistCount = _dashboardRepository.GetPunchlistOutsCount();
            var allCount = _dashboardRepository.GetPunchlistCount();

            var results = _dashboardRepository.GetAllPunchlistData();

            foreach (var row in results)
            {
                chartModel.OutstandingChartFlags.Add(row.flag);
                chartModel.OutstandingChartFlagDescriptions.Add(row.wd);
                var count = row.pos ?? 0;
                chartModel.PunchlistDoneCount += count;
                chartModel.OutstandingChartCounts.Add(count);
            }

            chartModel.ClearancePercentage = allCount == 0 ? 0 :
                Math.Round((double) chartModel.PunchlistDoneCount / (double)allCount * 100, 1);//avoid divide by 0

            return chartModel;
        }      

    }
}
