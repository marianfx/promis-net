﻿using Promis.NET.DataAccess.Extensions;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Services.Extensions;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Models;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using Promis.Web.DataAccess.Entities.PromisWeb;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Promis.NET.Localization;

namespace Promis.NET.Services.Services
{
    public class ProjectService: Service, IProjectService
    {
        private IProjectRepository _projectRepository;

        public ProjectService(IProjectRepository projectRepository): base(projectRepository)
        {
            _projectRepository = projectRepository;
        }


        public Project GetProjectById(string projectId)
        {
            SetContext();
            var theProject = _projectRepository.GetById<project, string>("proj", projectId);
            if (theProject == null)
                throw new Exception(LocalResources.CannotFindTheSpecifiedProject);

            return new Project(theProject);
        }

        public ProjectData GetProjectSmallDataById(string projectId)
        {
            SetContext();
            var theProject = _projectRepository.GetById<project, string>("proj", projectId);
            if (theProject == null)
                throw new Exception(LocalResources.CannotFindTheSpecifiedProject);

            return new ProjectData(theProject);
        }

        public List<Project> GetAllProjectsDataForUsername(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentNullException(LocalResources.InvalidArgumentUsername);

            var projects = _projectRepository.GetAllProjectsForUsername(username);
            var output = new List<Project>();
            foreach (var project in projects)
            {
                output.Add(new Project(project));
            }
            return output;
        }

        public int GetAllProjectsCountForUsername(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentNullException(LocalResources.InvalidArgumentUsername);

            return _projectRepository.GetAllProjectsCountForUsername(username);
        }

        public void UpdateProjectDetails(string projectDatabase, Project project, string type)
        {
            // get old project
            SetContext();
            var oldProject = _projectRepository.GetById<project, string>("proj", project.Code);
            if (oldProject == null)
                throw new Exception(LocalResources.TryingToUpdateAProjectThatDoesNotExistInTheDatabase);

            // run pre-requisites before update
            SetContext("project", projectDatabase);
            _projectRepository.RunProjectUpdatePrerequisites(project.GetProject(), oldProject, type);

            // update the actual info in the projects table
            SetContext();
            _projectRepository.UpdateProjectSelectedDetails(project.GetProject(), type);
        }


        public List<Activity> GetMainActivitiesList(string projectDatabase, string flag = null, ActivityOrderType orderType = ActivityOrderType.None)
        {
            SetContext("project", projectDatabase);
            var output = new List<Activity>();
            var tempOutput = _projectRepository.GetMainActivities(flag, orderType);
            foreach (var workdisk in tempOutput)
            {
                output.Add(new Activity(workdisk));
            }

            return output;
        }

        public void UpdateMainActivities(string projectDatabase, IEnumerable<DataKeeper> activitiesData)
        {
            SetContext("project", projectDatabase);
            _projectRepository.ResetProjectActivities();
            
            var wdData = _projectRepository.GetAll<Web.DataAccess.Entities.ProjectEntities.work_disc>().ToList();
            foreach (var wd in wdData)
            {
                var isSelected = activitiesData.FirstOrDefault(ad => ad.ColumnName == "disc_" + wd.cod)?.Value;
                var menHours = int.Parse(activitiesData.FirstOrDefault(ad => ad.ColumnName == "mh_" + wd.cod).Value);

                wd.flag = isSelected ? "*" : "";
                _projectRepository.UpdateById(wd, "id", wd.id);
                
                // insert budget
                if (isSelected)
                {
                    var bud = new pw_budget() { act = wd.cod, budget = menHours };
                    _projectRepository.Create(bud, "act", bud.act);
                }
            }
        }

        public List<Activity> GetAllAssociatedActivities(string projectDatabase, string mainActivityCode, bool filterEmpty = false)
        {
            var activityTables = new List<string>();
            if(filterEmpty)
            {
                SetContext();
                activityTables = _projectRepository.GetMainActivityTables(mainActivityCode);
            }

            var output = new List<Activity>();
            SetContext("project", projectDatabase);

            var dbList = _projectRepository.GetAssociatedActivities(mainActivityCode);
            foreach (var item in dbList)
            {
                if (filterEmpty)
                {
                    // check if there is data for any of this activity sub-activities
                    var subActivities = _projectRepository.GetSubActivities(item.ACT);
                    var hasData = _projectRepository.CheckIfAnySubactivityHasDataInTables(subActivities, activityTables);
                    if (!hasData)
                        continue;
                }

                output.Add(new Activity(item));
            }

            return output;
        }

        public List<ActivityPlanned> GetAllAssociatedActivitiesWithPlanningData(string projectId, string projectDatabase, string mainActivityCode, bool includeNonzero, bool getOnlyBaseAndPlanning = true, bool isOverall = false, bool populateProgressArrays = true)
        {
            var output = new List<ActivityPlanned>();
            var project = GetProjectById(projectId);

            SetContext("project", projectDatabase);
            var dbList = new List<work_planning>();
            if (!isOverall)
                dbList = _projectRepository.GetAssociatedActivitiesWithMainData(mainActivityCode, getOnlyBaseAndPlanning);
            else
                dbList = _projectRepository.GetMainActivities("<>", ActivityOrderType.Under16)
                                    .Select(ma => new work_planning(ma)).ToList();
            if (!includeNonzero)
                dbList = dbList.Where(l => l.mhrs.HasValue && l.mhrs > 0).ToList();

            if(populateProgressArrays)
                _projectRepository.StartGettingAllActivityProgressArrays();

            ActivityPlanned currentActivity = null;
            for(var i = 0; i < dbList.Count; i++)
            {
                if(currentActivity == null)
                    currentActivity = new ActivityPlanned(dbList[i]) { Project = project };
                currentActivity.SetEntity(dbList[i]);

                if (!(i == dbList.Count - 1 || (i + 1 < dbList.Count && dbList[i].act != dbList[i + 1].act)))
                    continue;//moving on to next activity until end is reached

                // end -> load and assign progress arrays
                if (populateProgressArrays)
                {
                    currentActivity.BaselineProgress = _projectRepository.GetActivityProgressArray(currentActivity.Code, "B", isOverall);
                    currentActivity.PlannedProgress = _projectRepository.GetActivityProgressArray(currentActivity.Code, "P", isOverall);
                }

                if (isOverall)//dates not available, must load them
                {
                    var workDatesB = _projectRepository.GetActivityDatesForType(currentActivity.Code, "B", isOverall);
                    var workDatesP = _projectRepository.GetActivityDatesForType(currentActivity.Code, "P", isOverall);
                    if (workDatesB != null)
                    {
                        currentActivity.StartDateBaseline = workDatesB.s_date;
                        currentActivity.EndDateBaseline = workDatesB.e_date;
                    }

                    if (workDatesP != null)
                    {
                        currentActivity.StartDatePlanned = workDatesP.s_date;
                        currentActivity.EndDatePlanned = workDatesP.e_date;
                    }
                }

                if (getOnlyBaseAndPlanning)
                {
                    output.Add(currentActivity);
                    currentActivity = null;
                    continue;
                }

                // case when retrieving R + A (Rescheduled + Actual Progress)
                if (populateProgressArrays)
                {
                    currentActivity.RProgress = _projectRepository.GetActivityProgressArray(currentActivity.Code, "R", isOverall);
                    currentActivity.AProgress = _projectRepository.GetActivityProgressArray(currentActivity.Code, "A", isOverall);//this will be retrieved from the 'progress' table.
                }

                if (!currentActivity.HasRSet)
                {
                    //date not found in planning table, but data exists => set dates based on data
                    var workDates = _projectRepository.GetActivityDatesForType(currentActivity.Code, "R", isOverall);
                    if (workDates != null && !string.IsNullOrWhiteSpace(workDates.s_date?.Trim()) && !string.IsNullOrWhiteSpace(workDates.e_date?.Trim()))
                    {
                        currentActivity.HasRSet = true;
                        currentActivity.StartDateR = workDates.s_date;
                        currentActivity.EndDateR = workDates.e_date;
                    }
                }

                if (!currentActivity.HasASet)
                {
                    //date not found in planning table, but data exists => set dates based on data
                    var workDates = _projectRepository.GetActivityDatesForType(currentActivity.Code, "A", isOverall);
                    if (workDates != null && !string.IsNullOrWhiteSpace(workDates.s_date?.Trim()) && !string.IsNullOrWhiteSpace(workDates.e_date?.Trim()))
                    {
                        currentActivity.HasASet = true;
                        currentActivity.StartDateA = workDates.s_date;
                        currentActivity.EndDateA = workDates.e_date;
                    }
                }
                output.Add(currentActivity);
                currentActivity = null;
            }
            if(populateProgressArrays)
                _projectRepository.FinishGettingAllActivityProgressArrays();

            return output;
        }

        public void UpdateAllAssociatedActivities(string projectId, string projectDatabase, string mainActivity, List<ActivityPlanned> activities, ActivityUpdateType updateType = ActivityUpdateType.BaselineAndPlanning)
        {
            var project = GetProjectById(projectId);

            SetContext("project", projectDatabase);
            foreach (var activity in activities)
            {
                if(updateType == ActivityUpdateType.BaselineAndPlanning)
                {
                    var activityBaseline = activity.GetPlanningEntity(ActivityPlanningEntityTypes.B);
                    var activityPlanned = activity.GetPlanningEntity(ActivityPlanningEntityTypes.P);
                    _projectRepository.UpdateActivityPlanningData(project.GetProject(), activityBaseline, activity.BaselineProgress);
                    _projectRepository.UpdateActivityPlanningData(project.GetProject(), activityPlanned, activity.PlannedProgress);
                }
                else
                {
                    var activityRescheduled = activity.GetPlanningEntity(ActivityPlanningEntityTypes.R);
                    _projectRepository.UpdateActivityPlanningData(project.GetProject(), activityRescheduled, activity.RProgress);
                }
            }
        }
        

        public string GetTableNameSubactivities()
        {
            return _projectRepository.GetTableNameSubactivities();
        }

        public List<DataKeeper> GetColumnsNecessaryForSubactivities()
        {
            return _projectRepository.GetColumnsNecessaryForSubactivities();
        }

        public List<Activity> GetAllSubActivities(string projectDatabase, string activityCode, bool filterEmpty = false, string mainActivity = null)
        {
            if (filterEmpty && string.IsNullOrWhiteSpace(mainActivity))//special case
                throw new ArgumentNullException(LocalResources.InvalidRequestWhenFilteringTheEmptySubactivitiesMainActivityMustBeSpecified);

            var activityTables = new List<string>();
            if (filterEmpty)
            {
                SetContext();
                activityTables = _projectRepository.GetMainActivityTables(mainActivity);
            }

            var output = new List<Activity>();
            SetContext("project", projectDatabase);

            var dbList = _projectRepository.GetSubActivities(activityCode);
            foreach (var item in dbList)
            {
                if (filterEmpty)
                {
                    // check if there is data for this sub-activity
                    var hasData = _projectRepository.CheckIfAnySubactivityHasDataInTables(new work_def_subact[] { item }, activityTables);
                    if (!hasData)
                        continue;
                }
                output.Add(new Activity(item));
            }

            return output;
        }

        public List<SubActivityProgress> GetAllDataForSubActivity(string projectDatabase, string subActivityCode)
        {
            SetContext("project", projectDatabase);
            var output = new List<SubActivityProgress>();
            var dbList = _projectRepository.GetAllDataForSubActivity(subActivityCode);
            foreach (var subactivity in dbList)
            {
                output.Add(new SubActivityProgress(subactivity));
            }
            return output;
        }

        public string GetUnitMeasureForSubActivity(string projectDatabase, string subActivityCode)
        {
            SetContext("project", projectDatabase);
            return _projectRepository.GetSubactivityUnitMeasure(subActivityCode);
        }


        public Budget GetActivityBudget(string projectDatabase, string activityCode)
        {
            SetContext("project", projectDatabase);
            var output = new Budget()
            {
                Available = _projectRepository.GetActivityBudget(activityCode),
                Planned = _projectRepository.GetActivityBudgetPlanned(activityCode)
            };

            return output;
        }

        public List<Module> GetActivityActions(string projectDatabase)
        {
            SetContext("project", projectDatabase);
            var output = new List<Module>();
            var actionsDb = _projectRepository.GetAll<act_actions>().ToList();
            foreach (var item in actionsDb)
            {
                output.Add(new Module()
                {
                    Code = item.code,
                    Description = item.description
                });
            }

            return output;
        }

        
        public List<DateTime> GetAllCutoffDaysOrderedByWeek(string projectDatabase)
        {
            SetContext("project", projectDatabase);
            var receivedFromDb = _projectRepository.GetAll<cut_offf>().OrderBy(c => c.w).Select(c => c.cut_off).ToList();
            var lastDate = new DateTime();
            // return them as dates
            return receivedFromDb.Select(c =>
            {
                var splitted = c.Split('/');
                if (splitted.Count() < 3)//safety check
                    return lastDate;

                lastDate = new DateTime(int.Parse(splitted[2]), int.Parse(splitted[1]), int.Parse(splitted[0]));
                return lastDate;
            }).ToList();
        }

        public void InsertCutoffDay(string projectDatabase, DateTime newDate)
        {
            SetContext("project", projectDatabase);
            var cutoff = new cut_offf()
            {
                w = newDate.YearAsString() + "_" + newDate.WeekOfTheYearAsTwoDigitString(),
                m = newDate.GetFlagForYearAndMonth(),
                cut_off = newDate.GetStandardInvariantDateString()
            };
            _projectRepository.Create(cutoff, "cut_off", cutoff.cut_off);
        }

        public void DeleteCutoffDay(string projectDatabase, DateTime oldDate)
        {
            SetContext("project", projectDatabase);
            _projectRepository.Delete<cut_offf, string>("cut_off", oldDate.GetStandardInvariantDateString());
        }


        public List<Module> GetModules()
        {
            SetContext();
            var modulesDb = _projectRepository.GetAllModules();
            var output = new List<Module>();
            foreach (var module in modulesDb)
            {
                output.Add(new Module(module));
            }

            return output;
        }

        public List<Subcontractor> GetSubcontractors(string projectDatabase)
        {
            SetContext("project", projectDatabase);
            var subsDb = _projectRepository.GetAllSubcontractors();
            var output = new List<Subcontractor>();
            foreach (var sub in subsDb)
            {
                output.Add(new Subcontractor(sub));
            }

            return output;
        }
        
        public List<double> GetTagCurvesForMonths(int months)
        {
            SetContext();
            var output = new List<double>();
            var row = _projectRepository.GetById("promisweb.tag_curves", "wk", months);
            if (row == null)
                return output;

            for(int i = 1; i < row.ItemArray.Count(); i++)
            {
                var dbValue = row.ItemArray[i];
                var tmpvalue = !string.IsNullOrWhiteSpace(dbValue?.ToString()) ? dbValue.ToString() : "0";
                if (tmpvalue != "0")
                {
                    tmpvalue = tmpvalue.Replace(",", ".");
                    var value = double.Parse(tmpvalue, NumberStyles.Any, CultureInfo.InvariantCulture);
                    value = Math.Round(value * 100, 2);
                    output.Add(value);
                }
            }

            return output;
        }

        public List<Area> GetAllAreas(string projectDatabase, bool orderByCode = true)
        {
            SetContext("project", projectDatabase);
            var output = new List<Area>();
            var areas = _projectRepository.GetAll<area>();
            if (orderByCode)
                areas = areas.OrderBy(x => x.CODE);

            foreach (var area in areas)
            {
                output.Add(new Area(area));
            }

            return output;
        }
    }
}
