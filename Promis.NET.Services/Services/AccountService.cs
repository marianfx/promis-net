﻿using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Services.Models.Account;
using Promis.NET.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using Promis.NET.Localization;

namespace Promis.NET.Services.Services
{
    public class AccountService : Service, IAccountService
    {
        private IAccountRepository _accountRepository;

        public AccountService(IAccountRepository accountRepository): base(accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public PromisUser GetUserDetailsById(string userId)
        {
            var identityUser = _accountRepository.GetUserById(userId);
            if (identityUser == null)
                throw new Exception(@LocalResources.ThereIsNotDataForThatUserId);

            return new PromisUser(identityUser);
        }

        public PromisUser GetUserDetails(string username, string project)
        {
            var identityUser = _accountRepository.GetUserByNameAndProject(username, project);
            if (identityUser == null)
                throw new Exception(@LocalResources.ThereIsNoDataForThatUsernameProjectAssociation);

            return new PromisUser(identityUser);
        }

        public UserData GetUserSmallDataById(string userId)
        {
            var pUser = GetUserDetailsById(userId);
            return new UserData()
            {
                Name = pUser.FullName,
                Company = pUser.Company,
                ImageUrl = pUser.PhotoUrl
            };
        }

        public List<PromisUser> GetAllNonAdminUsersForProject(string projectId)
        {
            var output = new List<PromisUser>();
            var dbUsers = _accountRepository.GetAllNonAdminUsersForProject(projectId);
            foreach (var user in dbUsers)
            {
                output.Add(new PromisUser(user));
            }
            return output;
        }

        public void UpdateUserDetails(PromisUser newDetails, bool updateFullDetails = false)
        {
            _accountRepository.UpdateBasicUserInfoById(newDetails.GetIdentityUser(), updateFullDetails);
        }

        public void DeleteUserById(string userId)
        {
            _accountRepository.DeleteUserById(userId);
        }
    }
}