﻿
using Promis.NET.Services.Models.PlanningAndProgress;
using Promis.NET.Services.Models.Project;
using System;
using System.Collections.Generic;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface IPdfCreatorPlanningAndProgress : IPdfCreator
    {
        //void SetData(string containerDirectory, Tuple<string, string> codeData, Project project, ProgressSteps inspectProgr, IEnumerable<ProgressData> progressData, bool isClientHidden, string budget, string documentToAttach = null, params string[] signingUsers);
        /// <summary>
        /// Sets the data for the pdf file
        /// </summary>
        /// <param name="containerDirectory">The directory name</param>
        /// <param name="codeData">The activity code and description</param>
        /// <param name="project"></param>
        /// <param name="inspectProgr"></param>
        /// <param name="progressData"></param>
        /// <param name="isClientHidden"></param>
        /// <param name="budget"></param>
        /// <param name="documentToAttach"></param>
        void SetData(string containerDirectory, Tuple<string, string> subActivityData, Project project, ProgressSteps progressDetails, IEnumerable<ProgressData> progressData, string clientHidden, string budget, string documentToAttach = null);
    }
}
