﻿
using Promis.NET.Services.Models.Commissioning;
using Promis.NET.Services.Models.Kpi;
using Promis.Web.DataAccess.Entities.ProjectEntities;
using Promis.NET.Utilities.Models;
using System.Collections.Generic;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface IKpiService : IService
    {
        /// <summary>
        /// Returns a list containing all the categories for KPI charts.
        /// </summary>
        /// <returns></returns>
        List<KpiCategory> GetAllKpiCategories();

        /// <summary>
        /// Returns all data for kpis (with counts, percent, code, description - for sub-kpis, not kpi categories)
        /// </summary>
        /// <param name="code">If this is specified, will get all the KPIs for a certain category (not all). Default = not specified.</param>
        /// <param name="getFavorites">Specify if you want to filter the data by favorites</param>
        /// <returns></returns>
        List<KpiData> GetAllKpis(string code = null, bool getFavorites = false);

        /// <summary>
        /// Performs updates according to the specified list of changes (does not affect fields not included in list).
        /// </summary>
        /// <param name="updated">The list of Key-Value pairs, where the key is the code of the KPI and the value is true or false on wether this is favorite or not.</param>
        void UpdateFavoriteKpis(IEnumerable<KeyValuePair<string, bool>> updated);

        /// <summary>
        /// Returns completion data for a system.
        /// </summary>
        /// <param name="system"></param>
        /// <returns></returns>
        CompletionStrings GetCompletion(string system);

        /// <summary>
        /// Updates data for a certain completion, identified by system code.
        /// </summary>
        /// <param name="data"></param>
        void SaveCompletion(completion data);

        #region Subcontractor Performance Indicators
        /// <summary>
        /// Gets The list of Subcontractors
        /// </summary>
        /// <returns></returns>
        List<KPISubcontractors> GetSubContractors();

        /// <summary>
        /// Gets the data for the SOW table
        /// </summary>
        /// <param name="subcCode">The subcontractor Code</param>
        /// <returns></returns>
        List<List<DataKeeper>> GetSOWData(string subcCode);

        /// <summary>
        /// Get the data for the Analysis tables
        /// </summary>
        /// <param name="projectDb">The name of the project db</param>
        /// <param name="subActivity">The subActvity code</param>
        /// <param name="selectedSubcontractor">The code for the selected subcontractor</param>
        /// <returns></returns>
        KpiAnalysis GetForecastAnalisysData(string projectDb, string subActivity, string selectedSubcontractor);

        /// <summary>
        /// Gets the data for the charts
        /// </summary>
        /// <param name="projectDb">The name of the project db</param>
        /// <param name="subcontractorCode">The selected suncontractor code</param>
        /// <param name="subActivity">The subActivity</param>
        /// <returns></returns>
        KPIsChartData GetKpiChartsData(string projectDb, string subcontractorCode, string subActivity);
        #endregion

        string ExportMilestonesSystemDataToPdf(string serverDirectory, string projectId, string systemCode);
    }
}
