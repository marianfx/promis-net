﻿using Promis.NET.Services.Models.Dashboard;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface IDashboardService: IService
    {
        /// <summary>
        /// Gets the data for the PROGRESS charts, for the current project.
        /// </summary>
        /// <param name="chartsModel">The object that main contain already populated data for the other charts. Will be populated with the current data as well.</param>
        /// <returns>Returns the object passed as parameter, populated with the progress data. Can throw errors when there is no data.</returns>
        DashboardData GetChartsProgressData(ref DashboardData chartsModel, string activity = null);

        /// <summary>
        /// Gets the data for the DELAY charts, for the current project. This requires the PROGRESS data to be loaded, and if it is not loaded, it will be loaded automatically. So if you want to get the data for the progress and for the delay too, use this method.
        /// </summary>
        /// <param name="chartsModel">The object that main contain already populated data for the other charts. Will be populated with the current data as well.</param>
        /// <returns>Returns the object passed as parameter, populated with the progress data. Can throw errors when there is no data.</returns>
        DashboardData GetChartsDelayData(ref DashboardData chartsModel);

        /// <summary>
        /// Gets the data for the MAIN ACTIVITIES charts, for the current project.
        /// </summary>
        /// <param name="chartsModel">The object that main contain already populated data for the other charts. Will be populated with the current data as well.</param>
        /// <returns>Returns the object passed as parameter, populated with the progress data. Can throw errors when there is no data.</returns>
        DashboardData GetChartsMainActivitiesData(ref DashboardData chartsModel);

        /// <summary>
        /// Gets the data for TRENDS charts, for the current project.
        /// </summary>
        /// <param name="chartsModel">The object that main contain already populated data for the other charts. Will be populated with the current data as well.</param>
        /// <returns>Returns the object passed as parameter, populated with the progress data. Can throw errors when there is no data.</returns>
        DashboardData GetChartsTrendsData(ref DashboardData chartsModel);

        /// <summary>
        /// Gets the data for the OUTSTANDINGS charts, for the current project.
        /// </summary>
        /// <param name="chartsModel">The object that main contain already populated data for the other charts. Will be populated with the current data as well.</param>
        /// <returns>Returns the object passed as parameter, populated with the progress data. Can throw errors when there is no data.</returns>
        DashboardData GetChartsOutstandingsData(ref DashboardData chartModel);

       
    }
}
