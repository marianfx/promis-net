﻿using Promis.NET.Services.Models.Kpi;
using Promis.NET.Services.Models.Project;
using System.Collections.Generic;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface IPdfCreatorCompletion: IPdfCreator
    {
        void SetData(string containerDirectory, string systemCode, string systemDescription, Project project, IEnumerable<string> pids, IEnumerable<string> datatables, Dictionary<string, IEnumerable<KpiSystem>> data);
    }
}
