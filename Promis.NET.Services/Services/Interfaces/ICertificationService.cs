﻿using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Services.Models.Certification;
using Promis.NET.Services.Models.Commissioning;
using Promis.NET.Services.Models.Project;
using Promis.NET.Utilities.Models;
using System.Collections.Generic;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface ICertificationService : IService
    {
        /// <summary>
        /// Returns a reference to the internal repository.
        /// </summary>
        /// <returns></returns>
        ICertificationRepository GetRepository();
        //for Status Tab
        /// <summary>
        /// Gets the list of tables for the tag assignment
        /// </summary>
        /// <param name="code">The given code</param>
        /// <returns></returns>
        List<Datatables> GetTablesForTag(string code);

        /// <summary>
        /// Gets the data for Certification Status page
        /// </summary>
        /// <returns></returns>
        Dictionary<string, CertificationTagsAssignmentStatus> GetCertificationCompletionData();

        /// <summary>
        /// Gets all the unasigned tags
        /// </summary>
        /// <param name="code">The code</param>
        /// <returns></returns>
        List<List<DataKeeper>> GetAllUnassignedTags(string code);


        #region Assign SOW to SubContractors
        /// <summary>
        /// Gets the list of subcontractors
        /// </summary>
        /// <param name="projectDatabase">The name of the project</param>
        /// <returns></returns>
        List<Subcontractor> GetSubcontractors(string projectDatabase);

        /// <summary>
        /// Gets the budget for every main activity
        /// </summary>
        /// <returns></returns>
        Dictionary<string, double> GetMainActivityBudget();

        /// <summary>
        /// Gets all the info about each main activity
        /// </summary>
        /// <param name="activityId">The code of the activity</param>
        /// <returns></returns>
        List<CertificationSubcontractorsAssignment> GetMainActivitiesDetails(string activityId);

        /// <summary>
        /// Updates the SOW for the Subcontractor
        /// </summary>
        /// <param name="updateList">The list of subcontractors info tot be updated</param>
        void UpdateSOWForSubcontractor(IEnumerable<SOWForSubcontractors> updateList);
        #endregion

        #region Assign plan to tags
        /// <summary>
        /// Gets the list of datatables
        /// </summary>
        /// <param name="mainActivity">Activity that specifies what datatables are shown</param>
        /// <returns></returns>
        List<Datatables> GetDatatables(string mainActivity);

        /// <summary>
        /// Gets a list of subactivities 
        /// </summary>
        /// <param name="mainActivity">Activity code that determines the list of subactivities</param>
        /// <returns></returns>
        List<Activity> GetSubactivityByMainActivity(string mainActivity);

        /// <summary>
        /// Gets the columns for a datatatable
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForTagsTable();
        #endregion

        #region Certification Summary
        /// <summary>
        /// Gets list of datatables by activity
        /// </summary>
        /// <param name="activity">Activity code</param>
        /// <returns></returns>
        List<Datatables> GetDatatablesByActivity(string activity);

        /// <summary>
        /// Gets list of fields of provided datatable of the specific row by id
        /// </summary>
        /// <param name="datatable">The datatable</param>
        /// <param name="rowId">The row Id</param>
        /// <returns></returns>
        KeyValuePair<List<CertificationSummarySteps>, ActivityBase> GetStatusOfSelectedRow(string projectID, string datatable, string rowId);

        /// <summary>
        /// Process of updateing certification Follow Up
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <param name="mainActivity"></param>
        /// <param name="rowId"></param>
        /// <param name="ord"></param>
        /// <param name="userValue"></param>
        /// <returns></returns>
        string UpdateCertTableByUser(string projectDatabase, string mainActivity, string rowId, string ord, string userValue);

        string ExportCertificationDataToPdf(string serverDirectory, string projectId, string table, string subactivityCode, string tag);
        #endregion
    }
}