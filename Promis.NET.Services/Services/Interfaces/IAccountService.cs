﻿using Promis.NET.Services.Models.Account;
using System.Collections.Generic;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface IAccountService: IService
    {
        /// <summary>
        /// Gets the full data for the user from the database, using the repository, by user id (unique).
        /// </summary>
        /// <param name="userId">The ID of the user.</param>
        /// <returns>The object populated  with the data. If there is no such user, throws exception.</returns>
        PromisUser GetUserDetailsById(string userId);

        /// <summary>
        /// Gets the full data for the user from the database, using the repository, by username and project id.
        /// </summary>
        /// <param name="username">The username of the user.</param>
        /// <param name="project">The project ID of the user.</param>
        /// <returns>The object populated  with the data. If there is no such user, throws exception.</returns>
        PromisUser GetUserDetails(string username, string project);

        /// <summary>
        /// Returns the most basic info about the user (name, location, picture).
        /// </summary>
        /// <param name="userId">The ID of the user to set data for.</param>
        /// <returns>The object populated with the data. If  the user does not exist, returns null.</returns>
        UserData GetUserSmallDataById(string userId);

        /// <summary>
        /// Returns the list of all the users for the current project, that are not admins
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        List<PromisUser> GetAllNonAdminUsersForProject(string projectId);

        /// <summary>
        /// Updates the given user.
        /// </summary>
        /// <param name="newDetails">The user to update. The ID field must be populated in order to update.</param>
        void UpdateUserDetails(PromisUser newDetails, bool updateFullDetails = false);

        /// <summary>
        /// Deletes the given user by id.
        /// </summary>
        /// <param name="newDetails">The user to update. The ID field must be populated in order to update.</param>
        void DeleteUserById(string userId);
    }
}
