﻿using Promis.NET.Services.Models.Project;
using Promis.NET.Utilities.Models;
using System.Collections.Generic;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface IProgressService : IService
    {
        /// <summary>
        /// Computes for a main activiti of the project, all the data regarding the quantities for activities and sub-activities.
        /// Meaning it returns a dictionary, where the keys are the unique activities (with code, description and total men hours) and the values are lists with all the sub-activities for that activity and quantity data for them (alongside basic info).
        /// </summary>
        /// <param name="mainActivity">The main activity to get data for.</param>
        /// <returns></returns>
        Dictionary<Activity, List<SubActivity>> GetQuantitiesData(string mainActivity);

        /// <summary>
        /// Updates the data regarding quantities (quantity and men hours) for all the subactivities in the array (budget for subactivities)
        /// </summary>
        /// <param name="subactivitiesData"></param>
        void UpdateSubactivitiesQuantities(IEnumerable<SubActivity> subactivitiesData);


        /// <summary>
        /// Returns the name of the progress table. Initial format: <dbName>_progress.<mainAct>_<year>_<weekNr>
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <param name="mainActivity"></param>
        /// <returns></returns>
        string GetTableNameProgress(string projectDatabase, string mainActivity);
        
        /// <summary>
        /// Runs a series of things (table creation and data copy) that need to be run before reading progress data for a certain activity, to ensure availability.
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <param name="mainActivity"></param>
        /// <param name="subActivity"></param>
        /// <returns>Returns the name of the progress table.</returns>
        string RunPrerequisitesForProgressTables(string projectDatabase, string mainActivity, string subActivity);

        /// <summary>
        /// Returns an array with two elements, the first one with data for the progress percents for the selected activity (agregate all sub-activities) and the second with data specific to a sub-activity (the specified one).
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <param name="mainActivity"></param>
        /// <param name="fullActivity"></param>
        /// <param name="subActivity"></param>
        /// <returns></returns>
        PercentData[] GetPlanningDataForSubactivity(string projectDatabase, string mainActivity, string fullActivity, string subActivity);

        /// <summary>
        /// Updates progress data for a column (from s1-s12) of the progress table related to the subactivity given. Returne the new computed overall percent.
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <param name="mainActivity"></param>
        /// <param name="subActivity"></param>
        /// <param name="tag"></param>
        /// <param name="columnS"></param>
        /// <param name="valueS"></param>
        /// <returns></returns>
        double UpdateSubactivityProgress(string projectDatabase, string mainActivity, string subActivity, string tag, string columnS, string valueS);

        //export data to pdf document
        //string ExportProgressDataToPdf(string serverDirectory, string projectId, string table, string subactivityCode, string tag);
        string ExportProgressDataToPdf(string serverDirectory, string projectId, string table, string subactivityCode);
    }
}
