﻿using Promis.NET.DataAccess.Models;
using Promis.NET.Services.Models.Engineering;
using Promis.NET.Services.Models.Excel;
using Promis.NET.Utilities.Models;
using System.Collections.Generic;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface IEngineeringService: IService
    {
        /// <summary>
        /// Given a project, returns the menu Items for the Engineering Database Page (headers and Tables for Engineering and Civil Tables)
        /// </summary>
        /// <param name="projectDb">The project database.</param>
        /// <returns></returns>
        EngineeringMenuData GetMenuItemsForProject(string projectDb);

        /// <summary>
        /// Returns the summary (with table counts) for the Engineering Tables
        /// </summary>
        /// <returns></returns>
        List<CountDataForTables> GetSummaryForEngineeringTables();
        /// <summary>
        /// Returns the summary (with table counts) for the Civil Tables
        /// </summary>
        /// <returns></returns>
        List<CountDataForTables> GetSummaryForCivilTables();


        /// <summary>
        /// Given a table, returns a list of Column Metadata for that table (columns names, if they are required, default values etc)
        /// </summary>
        /// <param name="dataTable">The table.</param>
        /// <param name="projectDb">The table's database.</param>
        /// <param name="isFiltering">Indicates if the request comes from the Advanced Filtering functionality, as there it will add a few more cases</param>
        /// <returns></returns>
        List<ColumnMetadata> GetColumnMetadataForTable(string dataTable, string projectDb, bool isFiltering = false);

        /// <summary>
        /// Returns the columns for the given table
        /// </summary>
        /// <param name="dataTable">The name of the given table</param>
        /// <returns></returns>
        GridData GetMetadataForTable(string dataTable);

        /// <summary>
        /// Returns the number of given table's rows
        /// </summary>
        /// <param name="dataTable">The name of the given table</param>
        /// <returns></returns>
        long GetRowsNumberForTable(string dataTable, List<WhereCondition> conditions, List<JoinCondition> joinConditions, DataTableColumn[] columns, bool distinct = false);

        /// <summary>
        /// Returns the number of the records after applying the filters
        /// </summary>
        /// <param name="dataTable">The name of the given Table</param>
        /// <param name="search">The search Value</param>
        /// <returns></returns>
        long GetRowsNumberForFilteredData(string dataTable, List<WhereCondition> conditions, List<JoinCondition> joinConditions, string search, DataTableColumn[] columns, bool distinct = false);

        /// <summary>
        /// Given a list of values of a table, it checks which columns of the table are required and returns back a list of columns that are not populated with values but should be. If the list is empty, it means everything is OK with the data.
        /// </summary>
        /// <param name="dataTable">The table.</param>
        /// <param name="projectDb">The database of the table.</param>
        /// <param name="values">The values for an row of the table.</param>
        /// <returns></returns>
        List<string> GetColumnsRequiredAndNotPopulated(string dataTable, string projectDb, List<DataKeeper> values);

        /// <summary>
        /// Returns a list of conditions that are intendet to identify a row from the database for this table. If the list returned is empty, then this row dows not have an ID column, does not have index columns and not any column called 'code'.
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="projectDb"></param>
        /// <param name="metadata"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        List<WhereCondition> GetIdentifyingColumnsForTableRows(string dataTable, string projectDb, List<ColumnMetadata> metadata, List<DataKeeper> values);

        /// <summary>
        /// Checks, according to the specified operation, if the given data for a row of the table is persistent (all the required columns are populated, the record does not exist etc.)
        /// </summary>
        /// <param name="dataTable">The table.</param>
        /// <param name="projectDb">The table's database.</param>
        /// <param name="values">The values for a row of the table.</param>
        /// <param name="checkExistenceForOperation">The type of operation that will be executed. I = insert, U = Update</param>
        /// <param name="rowId">The ID of the row.</param>
        void VerifyDataPersistence(string dataTable, string projectDb, List<DataKeeper> values, string checkExistenceForOperation = "I", string rowId = "");


        /// <summary>
        /// Returns the data for the given table (if it exists) using Server Side Processing.
        /// </summary>
        /// <param name="dataTable">Is the table (from the current selected database) to get data from.</param>
        /// <param name="start">The value for the start</param>
        /// <param name="length">The length</param>
        /// <param name="order">The value for order by clause: column and direction</param>
        /// <param name="search">The searched value</param>     
        /// <param name="conditions">The list of the the where condition</param>
        /// <returns></returns>
        List<dynamic> GetDataForTableServerSide(string dataTable, int start, int length, DataTableOrder[] order, DataTableSearch search, DataTableColumn[] columns, List<WhereCondition> conditions, List<JoinCondition> joinConditions, bool distinct = false);

        /// <summary>
        /// Inserts data, represented as a list of DataKeeper, into the table. It executes checks for persistance (record with the same code already exists).
        /// </summary>
        /// <param name="dataTable">The table to insert into.</param>
        /// <param name="projectDb">The project's database.</param>
        /// <param name="values">The values, represented as a list of DataKeeper</param>
        /// <returns>The ID of the inserted row.</returns>
        string InsertData(string dataTable, string projectDb, List<DataKeeper> values);
        
        /// <summary>
        /// Update data, represented as a list of DataKeeper, of a row from the table. It executes checks for persistance (record with the same code but different ID already exists).
        /// </summary>
        /// <param name="dataTable">The table to update.</param>
        /// <param name="projectDb">The project's database.</param>
        /// <param name="values">The values, represented as a list of DataKeeper</param>
        /// <param name="rowId">The ID of the row to update.</param>
        void UpdateData(string dataTable, string projectDb, List<DataKeeper> values, string rowId);

        /// <summary>
        /// DELETE data, represented as a list of DataKeeper, of a row from the table.
        /// </summary>
        /// <param name="dataTable">The table to update.</param>
        /// <param name="rowId">The ID of the row to delete.</param>
        /// <returns></returns>
        void DeleteData(string dataTable, string rowId);

        /// <summary>
        /// Checks if a row exists (by using the metod that gets Identifying columns for a table) and deletes it. May delete several rows if the table has not well defined metadata.
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="projectDb"></param>
        /// <param name="values"></param>
        void DeleteRowIfExists(string dataTable, string projectDb, List<DataKeeper> values);


        /// <summary>
        /// Generates the LEGEND Sheet for the given table.
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="projectDb"></param>
        /// <returns>The LEGEND Sheet populated with the table legend</returns>
        Sheet GetDataForLegendsSheet(string dataTable, string projectDb);

        /// <summary>
        /// Generates the excel file with all the data from the given table spanned on multiple sheets (or just the template if specified).
        /// </summary>
        /// <param name="dataTable">The table.</param>
        /// <param name="projectDb">The table's database.</param>
        /// <param name="username">The username.</param>
        /// <param name="directoryContainer"></param>
        /// <param name="isTemplate"></param>
        /// <param name="columns">The list of columns. Optional.</param>
        /// <param name="conditions">The conditions to filter by. Optional</param>
        /// <param name="groupBys">The group bys.</param>
        /// <returns></returns>
        string GenerateExcel(string dataTable, string projectDb, string username, string directoryContainer, bool isTemplate, List<WhereCondition> conditions = null, List<JoinCondition> joinConditions = null, List<string> columns = null, List<string> groupBys = null);

        /// <summary>
        /// Inserts data from an imported Excel file into a selected table
        /// </summary>
        /// <param name="datatable">The table name</param>
        /// <param name="projectid">The project id</param>
        /// <param name="filePath">The filepath of the imported file</param>
        /// <param name="isOverWrite">Must OverwriteData or not (has true or false as values)</param>
        void ImportDataFromExcel(string datatable, string projectid, string filePath,bool isOverWrite);

        //method for create tags using schemas page
        /// <summary>
        /// Returns the data for tags
        /// </summary>
        /// <returns></returns>
        List<TagsData> GetDataForTags();

    }
}
