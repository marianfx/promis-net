﻿
using Promis.NET.Services.Models.Activities;
using Promis.NET.Services.Models.Commissioning;
using Promis.NET.Utilities.Models;
using System.Collections.Generic;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface IActivitiesService : IService
    {
        /// <summary>
        /// Returns the basic data for certification test packages, alongside completion percentages (the list with all Inspection data).
        /// </summary>
        /// <returns></returns>
        List<Inspection> GetAllTestPackages();

        #region Hydrotest Packages ISO Assignment
        /// <summary>
        /// Gets the list of Piping
        /// </summary>
        /// <param name="isometric">The isometric selected</param>
        /// <param name="paramNeeded">the value for a column</param>
        /// <returns></returns>
        List<string> GetPipingList(string isometric, string paramNeeded);

        /// <summary>
        /// Gets the information about system
        /// </summary>
        /// <param name="isometric">The isometric</param>
        /// <param name="neededParam">the parameter</param>
        /// <returns></returns>
        List<Datatables> GetIsometricsValues(string isometric, string neededParam);

        /// <summary>
        /// Assigns Isometric data to the selected testpackage
        /// </summary>
        /// <param name="isometricData"><The isometric data/param>

        bool AssignIso(Isometrics isometricData);
        #endregion

        #region System Punch Lists Management
        /// <summary>
        /// Gets all system codes and theyr description from punchlist
        /// </summary>
        /// <returns></returns>
        List<PlBaseTables> GetAllSystemFromPunchlist(bool plOnly);

        /// <summary>
        /// Gests all disciplines
        /// </summary>
        /// <returns></returns>
        List<PlBaseTables> GetAllDisciplines();

        /// <summary>
        /// Gets all Originators
        /// </summary>
        /// <returns></returns>
        List<PlBaseTables> GetAllOriginators();

        /// <summary>
        /// Gets all categoryes
        /// </summary>
        /// <returns></returns>
        List<PlBaseTables> GetAllCategory();

        /// <summary>
        /// Gets all records from ActionsBy
        /// </summary>
        /// <returns></returns>
        List<PlBaseTables> GetAllActionsBy();

        /// <summary>
        /// Gets all status codes
        /// </summary>
        /// <returns></returns>
        List<string> GetAllStatus();

        /// <summary>
        /// Gets all records from Closer table
        /// </summary>
        /// <returns></returns>
        List<PlBaseTables> GetAllCloser();

        /// <summary>
        /// Gets datatable columns
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForPunchlistTable();

        /// <summary>
        /// Gets closure field in punchlist table at specified row
        /// </summary>
        /// <param name="rowId">The Id of the specified Row</param>
        /// <returns></returns>
        string GetClosureByRowId(string rowId);

        /// <summary>
        /// Updates or inserts a new row in the table punchlist!
        /// </summary>
        /// <param name="commData">The data that will be added</param>
        /// <param name="isInsert">The flag that specifies</param>
        /// <returns></returns>
        string UpdateOrInsertPunchlistRow(Punchlist commData, bool isInsert, string rowId);
        /// <summary>
        /// Gets columns for table
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForPunchlistModalTable();

        /// <summary>
        /// Gets list of tablenames
        /// </summary>
        /// <returns></returns>
        List<PlBaseTables> GetDataTablesForModal(string system);


        #endregion
    }
}
