﻿using System.Collections.Generic;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Models.Commissioning;
using Promis.NET.Utilities.Models;
using Promis.NET.DataAccess.Repositories.Interfaces;
using System;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface ICommissioningService: IService
    {

        /// <summary>
        /// Returns a reference to the internal repository.
        /// </summary>
        /// <returns></returns>
        ICommissioningRepository GetRepository();
        /// <summary>
        /// Works on project's db. Gets the list of all datatables.
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        List<Datatables> GetDatatables(bool isFromSys = false);

        /// <summary>
        /// Works on project's db. Gets the list of the sub-activities based on the tablename and subactivity provied.
        /// </summary>
        /// <param name="tableName"> The name of the datatable provided.</param>
        /// <param name="activity"> The code of the activity of which the subactivity belongs to.</param>
        /// <returns></returns>
        List<Activity> GetSubactForDatatables(string tableName, string activity);

        /// <summary>
        /// Works on project's db. Gets the list of the typecodes belonging to the tablename provied.
        /// </summary>
        /// <param name="tableName"> The name of the datatable provided.</param>
        /// <returns></returns>
        List<KeyValuePair<string, string>> GetTypecodesForDatatables(string tableName, bool isWithJoin = false);

        /// <summary>
        /// Works on project's db. Gets the list of the colums belonging to the tablename provied.
        /// </summary>
        /// <param name="tableName"> The name of the datatable provided.</param>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForTagsTable(string mainActivity);

        /// <summary>
        /// Works on project's db. Gets the list of tags and activityes belonging to the provided table within the indicators list.
        /// </summary>
        /// <param name="tableName"> The name of the datatable provided.</param>
        /// <param name="contor"> The list of the indicators that determin the range of the table rows.</param>
        /// <returns></returns>
        List<ActivityTag> GetTagAndActivityforDatatables(string prefix,string tableName, List<string> contor);

        /// <summary>
        /// Works on project's db. Updates the list of tags and activityes belonging to the provided data.
        /// </summary>
        /// <param name="commData"> The name of the data object provided.</param>
        /// <returns></returns>
        string UpdateTagAndActivityforDatatables(CommissioningInspectionTagsProgress commData);

        /// <summary>
        /// Works on project's db. Updates the list of tags and deletes the activityes belonging to the provided table.
        /// </summary>
        /// <param name="commData"> The name of the data object provided.</param>
        /// <returns></returns>
        void UpdateTagAndDeleteActivityforDatatables(CommissioningInspectionTagsProgress commData);


        /// <summary>
        /// Gets the number of tags assigned, tags not assigned and the name of the datatable
        /// </summary>
        /// <returns></returns>
        Dictionary<string, DatatablesInfo> GetDataTableInfoComplete();

        /// <summary>
        /// Gets the data for the precommissioning and the commissioning chart
        /// </summary>
        /// <param name="chartsModel">The name of the model</param>
        /// <param name="sufix">The sufix ("CO" or "PR")</param>
        /// <returns></returns>
        ChartsData GetChartsProgressStatusData(ref ChartsData chartsModel, string sufix);


        //loop tests by systems 
        /// <summary>
        /// Gets the List of the code and description for the existing systems
        /// </summary>
        /// <returns></returns>
        List<SystemModel> GetSystemListForLoops();

        /// <summary>
        /// Gets the list of tags for the chosen system
        /// </summary>
        /// <param name="system">The code of the system</param>
        /// <returns></returns>
        List<string> GetTagsForSystem(string system);

        /// <summary>
        /// Gets all the data for the tag's table for the chosen system
        /// </summary>
        /// <param name="tag">The chosen tag</param>
        /// <returns></returns>
        Dictionary<string, List<SystemInstrumentsTags>> GetDataForLoopTagsTable(string tag);

        //Certification Completion
        /// <summary>
        /// Returns the data neccessary for the Certification Completion
        /// </summary>
        /// <returns></returns>
        Dictionary<string, CommissioningCertificationCompletion> GetCertificationCompletionData();

        //System Subdivision

        /// <summary>
        /// Works on project's db. Gets the list of the colums belonging to the tablename provied.
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForSystemTable();

        /// <summary>
        /// Works on project's db. Gets the list of the available system codes.
        /// </summary>
        /// <returns></returns>
        List<SystemModel> GetSystemList();

        /// <summary>
        /// Works on project's db. Counts all actions assigned with a system(if no datatable was selected), thone not assigned and determines the percentage between the two. 
        /// </summary>
        /// <param name="dataTable">The selected table</param>
        /// <returns></returns>
        Tuple<string, string, string> GetTotalCountsAndPercentage(string dataTable);

        /// <summary>
        /// Works on project's db. Sets or removes the system associeted to the selected rows in the provided datatable.
        /// </summary>
        /// <param name="commData">Object containing the nececery data specified above</param>
        /// <returns></returns>
        void UpdateOrDeleteSysforDatatables(CommissioningSysProgress commData);

        //Inspection Follow Up

        /// <summary>
        /// Works on project's db. Gets the list of the colums belonging to the tablename provied.
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForFollowUpTable();

        /// <summary>
        /// Works on project's db. Gets the inspection status of the selected row.
        /// </summary>
        /// <param name="datatable">The table form witch the row is selected above</param>
        /// <param name="rowId">The identifier of the row</param>
        /// <param name="activity">The provided Activity</param>
        /// <returns></returns>
        KeyValuePair<List<CommissioningFollowUpStatus>, ActivityBase> GetStatusOfSelectedRow(string datatable, string rowId, string activity);

        /// <summary>
        /// Works on project's db. Saves the added inspections.
        /// </summary>
        /// <param name="datatable">The table that has been inspected</param>
        /// <param name="userName">The person representing the inspector</param>
        /// <param name="tag">The specific tag code that was inspected</param>
        /// <param name="ord">The order of the rows belonging to the tag code that was inspected</param>
        /// <param name="result">The final result of the inspection</param>
        /// <returns></returns>
        void UpdateinspectionforDatatables(string dataTable, string userName, string tag, string ord, string result);




    }
}
