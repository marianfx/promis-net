﻿using Promis.NET.Services.Models.Certification;
using Promis.NET.Services.Models.Project;
using System;
using System.Collections.Generic;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface IPdfCreatorCertification: IPdfCreator
    {
        void SetData(string containerDirectory, Tuple<string, string> codeData, string tag, Project project, IEnumerable<CertificationSummarySteps> data, string documentToAttach = null, params string[] signingUsers);
    }
}
