﻿namespace Promis.NET.Services.Services.Interfaces
{
    public interface IPdfCreator
    {
        string ServerParentPath { get; set; }
        string Name { get; set; }
        string FullPath { get; set; }
        float PageWidth { get; set; }
        float PageHeight { get; set; }
        float HeaderHeight { get; set; }
        string DateToAttach { get; set; }

        /// <summary>
        /// Creates the document and returns the created file name. All the directories are configurable.
        /// </summary>
        /// <returns></returns>
        string CreatePdf();
    }
}
