﻿using Promis.NET.DataAccess.Repositories.Interfaces;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface IService
    {
        /// <summary>
        /// Returns the repository of the type of T parameter. If none found, returns null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetRepository<T>() where T : class;

        /// <summary>
        /// Switches the database of the project (as well as the context).
        /// </summary>
        /// <param name="projectName"></param>
        void SetWorkingProject(string projectName);

        /// <summary>
        /// Sets the Context of the Database for all the reposotories from this service. So we can do operations either on the project's own database, or on the general database.
        /// </summary>
        /// <param name="context">Can be 'general' (default) for the general database usage or 'project' for the project's database.</param>
        void SetContext(string context = "default", string databasename = "");

        /// <summary>
        /// Changes the context of the database for the specified repository from this service.
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="context"></param>
        /// <param name="databasename"></param>
        void SetContext(IRepository repository, string context = "default", string databasename = "");
    }
}
