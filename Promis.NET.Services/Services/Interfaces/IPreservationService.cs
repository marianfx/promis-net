﻿
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Services.Models.Preservation;
using Promis.NET.Utilities.Models;
using System;
using System.Collections.Generic;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface IPreservationService : IService
    {
        /// <summary>
        /// Returns a reference to the internal repository.
        /// </summary>
        /// <returns></returns>
        IPreservationRepository GetRepository();

        /// <summary>
        /// Get the list with all dataTables
        /// </summary>
        /// <param name="isFollowUp">Indicates if the follow up tab is selected</param>
        /// <returns>Returns the list of datatables</returns>
        List<string> GetDatatableNames(bool isFollowUp);

        /// <summary>
        /// Gives the typecodes list
        /// </summary>
        /// <param name="dataTable">The name of the chosen dataTable </param>
        /// <returns>Returns the list of typecodes for the chosen dataTable</returns>
        Dictionary<string, List<string>> GetTypecodesNames(string dataTable);


        /// <summary>
        /// gets the name of the table 
        /// </summary>
        /// <returns>Returns the name of the table from which are displayed the typecodes</returns>
        string GetTableNameTypecodes();


        /// <summary>
        /// Gets the preservation data
        /// </summary>
        /// <param name="code">The code for the preservation</param>
        /// <returns>Returns the description, the sequence and the activity for the chosen preservation</returns>
        Dictionary<string, List<PreservationDetails>> GetAllDataForPreservation(string code);

        //for job assignments tab
        /// <summary>
        /// Gets the locations
        /// </summary>
        /// <returns>Returns the list with the locations for the preservations</returns>
        List<string> GetLocations();

        /// <summary>
        /// Gets the Frequency value
        /// </summary>
        /// <param name="code">The code of the preservation</param>
        /// <returns>Returns the value of the selected preservation frequency</returns>
        string GetFrequencyForPreservation(string code);

        /// <summary>
        /// Deletes the selected tag
        /// </summary>
        /// <param name="datatable">Name of the selected table</param>
        /// <param name="tag">Name of the selected tag</param>        
        void DeleteTags(string datatable, string tag);

        /// <summary>
        /// Gets The year and the week number for a given date
        /// </summary>
        /// <param name="fullDate">The given date</param>
        /// <returns></returns>
        string GetYearWeek(DateTime fullDate);

        /// <summary>
        /// Saves the changes made for the selected preservation
        /// </summary>
        /// <param name="dataTable">The name of the selected table</param>
        /// <param name="preservation">The dates and location selected for the preservation</param>
        /// <param name="rowCode">The selected code for the preservation</param>
        void SaveChangesToTags(string dataTable, PreservationAssignment preservation, string rowCode);


        /// <summary>
        /// Gets the info about the done and scheduled week
        /// </summary>
        /// <param name="datatable">The dataTable</param>
        /// <param name="tagVal">The tag</param>
        /// <returns></returns>
        Dictionary<string, PreservationActivities> GetScheduledWeeksForPreservation(string datatable, string tagVal);

        /// <summary>
        /// Gets The cutOff onfo
        /// </summary>
        /// <param name="dataTable">The datatable</param>
        /// <param name="schedVal">The scheduled week</param>
        /// <param name="tagVal">Thge tag</param>
        /// <returns></returns>
        Dictionary<string, PreservationSchedWeekInfo> GetCutOffAndSchedule(string dataTable, string schedVal, string tagVal);

        /// <summary>
        /// Gets the info for the preservation Actions area(code, description, activity and sequence
        /// </summary>
        /// <param name="dataTable">The datatable</param>
        /// <param name="code">The code</param>
        /// <param name="tagVal">The tag</param>
        /// <param name="schedVal">The scheduled week</param>
        /// <returns></returns>
        Dictionary<string, List<PreservationActions>> GetPreservationActions(string dataTable, string code, string tagVal, string schedVal);

        /// <summary>
        /// Save the changes made for the preservation
        /// </summary>
        /// <param name="dataTable">The dataTable</param>
        /// <param name="preservation">The list of sequence and donew</param>
        /// <param name="tag">The tag</param>
        /// <param name="Code">codeThe job plan </param>
        /// <param name="schedw">The scheduled week</param>
        void SaveActivtiesStatusChanges(string dataTable, List<DataKeeper> preservation, string tag, string code, string schedw);



    }
}
