﻿using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Services.Models.Project;
using Promis.NET.Utilities.Models;
using System;
using System.Collections.Generic;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface IProjectService: IService
    {
        /// <summary>
        /// Gets the full set of data for the given project, using the projects table
        /// </summary>
        /// <param name="projectId">The ID of the project to retrieve</param>
        /// <returns></returns>
        Project GetProjectById(string projectId);

        /// <summary>
        /// Given the project ID, returns the small set of identifying data for it (name, location, picture).
        /// </summary>
        /// <param name="projectId">The ID of the project to retrieve.</param>
        /// <returns></returns>
        ProjectData GetProjectSmallDataById(string projectId);

        /// <summary>
        /// Returns a list of small project data, for all the projects associated with an username. If the list is empty, it means the user is not associated to any project.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        List<Project> GetAllProjectsDataForUsername(string username);

        /// <summary>
        /// Returns the number of projects associated to a given user.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        int GetAllProjectsCountForUsername(string username);

        /// <summary>
        /// Updates the details of a project and persists changes. It will not update the readonly fields.
        /// </summary>
        /// <param name="project">The object that contains the new project data.</param>
        /// <param name="type">The type of the update. Can be 'general' or 'logo'</param>
        void UpdateProjectDetails(string projectDatabase, Project project, string type);


        /// <summary>
        /// Returns a list of data that identify the main activities for the given project (identified by database).
        /// </summary>
        /// <param name="projectDatabase">The database of the project.</param>
        /// <param name="flag">If specified, will return only activities with that flag. If the value is '<>' then it will select the ones not null and not empty strings.</param>
        /// <param name="orderType">Specifies  the ordering type. Leave empty if nothing special. Under16 gets only the first 16 activities, Over16 the others.</param>
        /// <returns></returns>
        List<Activity> GetMainActivitiesList(string projectDatabase, string flag = null, ActivityOrderType orderType = ActivityOrderType.None);
        
        /// <summary>
        /// Updates the main activities for the given project. This means basically restoring activities to promisweb defaults, then checking the selected activities and inserting the budget in MHrs
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <param name="activitiesData"></param>
        void UpdateMainActivities(string projectDatabase, IEnumerable<DataKeeper> activitiesData);

        /// <summary>
        /// Returns a list of Activity, that contains the related activities to a given main activity.
        /// </summary>
        /// <param name="projectDatabase">The project's database</param>
        /// <param name="mainActivityCode">The code of the main activity.</param>
        /// <param name="filterEmpty">Specify this only if wanting to filter only the activities that have data in the main-activity's tables.</param>
        /// <returns></returns>
        List<Activity> GetAllAssociatedActivities(string projectDatabase, string mainActivityCode, bool filterEmpty = false);

        /// <summary>
        /// For a given main activity, loads all the associated activities from a project.
        /// </summary>
        /// <param name="projectDatabase">The project's database</param>
        /// <param name="mainActivityCode">The code of the main activity.</param>
        /// <param name="includeNonzero">If set to true, will load activities even if Mhrs is 0; otherwise, the activities with Men Hours set to 0 will be ignored.</param>
        /// <param name="getOnlyBaseAndPlanning">IF set to true, will return only data for baseline and planning curves. Default is true.</param>
        /// <param name="isOverall">If this is set to true, will get data for main activities, computing overall progress. Default is false.</param>
        /// <param name="populateProgressArrays">Default true. Populates the arrays with progress / planning.</param>
        /// <returns></returns>
        List<ActivityPlanned> GetAllAssociatedActivitiesWithPlanningData(string projectId, string projectDatabase, string mainActivityCode, bool includeNonzero, bool getOnlyBaseAndPlanning = true, bool isOverall = false, bool populateProgressArrays = true);

        /// <summary>
        ///  Given the list that contains Basic + Planning Data for all the activities associated with a Main Activity, parses the data and persists it in the database.
        /// </summary>
        /// <param name="projectId">The project's ID</param>
        /// <param name="projectDatabase">The project's database</param>
        /// <param name="mainActivity">The code of the main activity.</param>
        /// <param name="activities">The list with data for the updated activities</param>
        /// <param name="updateType">Whether to update planned and baseline curves or the rescheduled curves</param>
        void UpdateAllAssociatedActivities(string projectId, string projectDatabase, string mainActivity, List<ActivityPlanned> activities, ActivityUpdateType updateType = ActivityUpdateType.BaselineAndPlanning);


        /// <summary>
        /// Returns the name of the table that contains data for subactivities.
        /// </summary>
        /// <returns></returns>
        string GetTableNameSubactivities();

        /// <summary>
        /// Simply return a list of columns that are representative for the table that contains subactivities
        /// </summary>
        /// <returns></returns>
        List<DataKeeper> GetColumnsNecessaryForSubactivities();

        /// <summary>
        /// Returns a list of Activity, that contains all the subactivities of an actual activity (not a main activity)
        /// </summary>
        /// <param name="projectDatabase">The project's database</param>
        /// <param name="activityCode">The code of the activity.</param>
        /// <param name="filterEmpty">Specify this only if wanting to filter only the sub-activities that have data in the main-activity's tables.</param>
        /// <param name="mainActivity">This MUST be specified if filtering the empty sub-activities. The code for the main activity.</param>
        /// <returns></returns>
        List<Activity> GetAllSubActivities(string projectDatabase, string activityCode, bool filterEmpty = false, string mainActivity = null);

        /// <summary>
        /// Gets a list with all the data referenced in thw work definition, for a certain sub-activity.
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <param name="subActivityCode"></param>
        /// <returns></returns>
        List<SubActivityProgress> GetAllDataForSubActivity(string projectDatabase, string subActivityCode);

        /// <summary>
        /// Returns the unit measure associated with an sub-activity.
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <param name="subActivityCode"></param>
        /// <returns></returns>
        string GetUnitMeasureForSubActivity(string projectDatabase, string subActivityCode);


        /// <summary>
        /// Given the project, and an activity, gets the budget data (expected budget, planned budget, balance).
        /// </summary>
        /// <param name="projectDatabase">The project's database.</param>
        /// <param name="activityCode">The code of the activity.</param>
        /// <returns></returns>
        Budget GetActivityBudget(string projectDatabase, string activityCode);
        
        /// <summary>
        /// Returns a list which contains the actions for activities (sort of like the legend to display when watching over certification steps).
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <returns></returns>
        List<Module> GetActivityActions(string projectDatabase);


        /// <summary>
        /// Gets the full list of dates that are recorded as cut-off days for this project. If a date from the db is invalid, it will be saved as the last recorded date (no error).
        /// </summary>
        /// <param name="projectDatabase">Represents the name of the database for the project.</param>
        /// <returns></returns>
        List<DateTime> GetAllCutoffDaysOrderedByWeek(string projectDatabase);

        /// <summary>
        /// Inserts for the current project a cutoff day, only if it does not exist. If it exists, throws error.
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <param name="newDate"></param>
        void InsertCutoffDay(string projectDatabase, DateTime newDate);

        /// <summary>
        /// Deletes, for the current project, the specified cutoff day, only if it exists. If it does not exist, throws error.
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <param name="oldDate"></param>
        void DeleteCutoffDay(string projectDatabase, DateTime oldDate);


        /// <summary>
        /// Returns a list with the modules available
        /// </summary>
        /// <returns></returns>
        List<Module> GetModules();

        /// <summary>
        /// Returns a list with the subcontractors available
        /// </summary>
        /// <returns></returns>
        List<Subcontractor> GetSubcontractors(string projectDatabase);

        /// <summary>
        /// OBSOLETE. Gets the array that identifies the planned curves for a project, given the expected months to finish.
        /// </summary>
        /// <param name="months"></param>
        /// <returns></returns>
        List<double> GetTagCurvesForMonths(int months);

        /// <summary>
        /// Returns the list of all Areas, ordered (optionally, by default true) by code.
        /// </summary>
        /// <param name="projectDatabase"></param>
        /// <param name="orderByCode"></param>
        /// <returns></returns>
        List<Area> GetAllAreas(string projectDatabase, bool orderByCode = true);
    }
}
