﻿using NPOI.SS.UserModel;
using Promis.NET.Services.Models.Excel;
using Promis.NET.Utilities.Models;
using System.Collections.Generic;
using System.Data;

namespace Promis.NET.Services.Services.Interfaces
{
    public interface IExcelService
    {
        ExcelData ExcelData { get; set; }
        /// <summary>
        /// Sets the Excel Data for this service to handle.
        /// </summary>
        /// <param name="excelData">An object containing information about an excel document.</param>
        void SetData(ExcelData excelData);

        /// <summary>
        /// Creates and saves to disk the excel document, given the data set to this service  through the ExcelData Object.
        /// </summary>
        void CreateDocument();

        /// <summary>
        /// Adds a sheet to a document. If reload is set to true, will read the document from the disk and will add the given sheet. If not, will read the data for the current working on document (created with CreateDocument).
        /// </summary>
        /// <param name="sheetToAdd">This contains data about the sheet to add to the document.</param>
        /// <param name="reload">Indicates if should re-read data for the excel file from the disk and then after adding the sheet save it.</param>
        /// <returns>The reference to the sheet added.</returns>
        ISheet AddSheet(Sheet sheetToAdd, bool reload = false);

        /// <summary>
        /// Adds the given rows to the sheet (and saves the data to the disk)
        /// </summary>
        /// <param name="whereToAdd">The sheet to add in (the name matters as it will read the excel document from the disk and will try to add the data to the existing sheet, so the sheet must be pre-created).</param>
        /// <param name="rows">The data holder.</param>
        void AddRows(Sheet whereToAdd, List<List<DataKeeper>> rows);

        //for import excel
        /// <summary>
        /// Reads the data from an excel file
        /// </summary>
        /// <param name="filePath">The path of the imported file</param>
        /// <param name="columnsExpected">The list of the wanted columns</param>
        /// <param name="columnRequired">The list of the mandatory column</param>
        /// <returns></returns>
        DataTable ReadExcelFile(string filePath, List<string> columnsExpected, List<string> columnRequired);
        /// <summary>
        /// Reads the data from an excel sheet
        /// </summary>
        /// <param name="filePath">The path of the imported file</param>
        /// <param name="sheet">the sheet which must be read</param>
        /// <param name="columnsExpected">The list of the wanted columns</param>
        /// <param name="columnRequired">The list of the mandatory fields</param>
        /// <returns></returns>
        DataTable ReadSheet(string filePath, Sheet sheet, List<string> columnsExpected, List<string> columnRequired);
    }
}
