﻿using Promis.NET.Services.Models.Excel;
using Promis.NET.Services.Services.Interfaces;
using System;
using System.IO;
using System.Collections.Generic;
using Promis.NET.Utilities.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Model;
using NPOI.SS.UserModel;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data;
using Promis.NET.Localization;

namespace Promis.NET.Services.Services
{
    public class ExcelService : IExcelService
    {
        public ExcelData ExcelData { get; set; }
        private HSSFWorkbook wb;
        private int currentRow;

        public ExcelService()
        {
            ExcelData = new ExcelData();
        }

        public ExcelService(ExcelData excelData)
        {
            SetData(excelData);
        }

        public void SetData(ExcelData excelData)
        {
            ExcelData = excelData;
        }


        public void CreateDocument()
        {
            var newFile = new FileInfo(ExcelData.FilePath);
            if (newFile.Exists) // CHECK IF THE FILE EXISTS. 
            {
                newFile.Delete();  // DELETE THE FILE IF IT ALREADY EXISTS
                newFile = new FileInfo(ExcelData.FilePath);
            }
            var dir = newFile.Directory;
            if (!dir.Exists)
                dir.Create();

            wb = HSSFWorkbook.Create(InternalWorkbook.CreateWorkbook());
            try
            {
                for (int i = 0; i < ExcelData.SheetList.Count; i++)
                {
                    var sheet = ExcelData.SheetList[i];
                    AddSheet(sheet);
                }

                SaveXls();
            }
            catch (Exception ex)
            {
                throw new Exception(LocalResources.CannotCreateTheXlsFile + ex.Message);
            }
        }

        public ISheet AddSheet(Sheet sheetToAdd, bool reload = false)
        {
            if (reload)
            {
                ReadXls();
            }

            if (wb == null)
                throw new Exception(LocalResources.ThereIsNoExcelDocumentOpenedInTheServiceCannotExecuteOperationsOnNothing);

            var worksheet = (HSSFSheet)wb.CreateSheet(sheetToAdd.SheetName);
            if (sheetToAdd.SheetColor.HasValue)
                worksheet.TabColorIndex = sheetToAdd.SheetColor.Value;

            var row = worksheet.CreateRow(0);
            for (int j = 0; j < sheetToAdd.Columns.Count; j++)
            {
                var cell = row.CreateCell(j);
                cell.SetCellValue(sheetToAdd.Columns[j].ToUpper());
                worksheet.AutoSizeColumn(j);
            }

            if (reload)
                SaveXls();

            return worksheet;
        }

        public void AddRows(Sheet sheetMetadata, List<List<DataKeeper>> rows)
        {
            try
            {
                ReadXls();

                var worksheet = wb.GetSheet(sheetMetadata.SheetName);
                currentRow = worksheet.PhysicalNumberOfRows - 1;

                for (int j = 0; j < rows.Count; j++)
                {
                    PrepareWorksheetForMaxRows(ref worksheet, sheetMetadata);

                    var row = worksheet.CreateRow(currentRow);
                    for (int k = 0; k < sheetMetadata.Columns.Count; k++)
                    {
                        // insert data into the cells
                        var rowValue =  (!Convert.IsDBNull(rows[j][k].Value) && rows[j][k].Value != null ? rows[j][k].Value : "");
                        var cell = row.CreateCell(k);

                        cell.SetCellValue(rowValue);
                    }
                }

                SaveXls();
            }
            catch (Exception ex)
            {
                throw new Exception(LocalResources.CannotAddRowsToTheXlsFile + ex.Message);
            }

        }


        public DataTable ReadExcelFile(string filePath, List<string> columnsExpected, List<string> columnRequired)
        {
            DataTable output = new DataTable();
            HSSFWorkbook workbook = null;

            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                workbook = new HSSFWorkbook(fs);
            }
            HSSFSheet worksheet = null;
            //loop through sheets
            for (int i = 0; i < workbook.NumberOfSheets; i++)
            {
                //get every sheet
                worksheet = workbook.GetSheetAt(i) as HSSFSheet;
                //verify if the actual sheet name is not the LEGEND sheet
                if (worksheet.SheetName == "LEGEND")
                    continue;
                var sheet = new Sheet();
                sheet.SheetName = worksheet.SheetName;

                var dt = ReadSheet(filePath, sheet, columnsExpected, columnRequired);
                //output.AsEnumerable().Union(dt.AsEnumerable());
                output.Merge(dt);

            }
            return output;
        }

        public DataTable ReadSheet(string filePath, Sheet sheet, List<string> columnsExpected, List<string> columnRequired)
        {
            DataTable dt = new DataTable();
            HSSFWorkbook workbook = null;

            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                workbook = new HSSFWorkbook(fs);
            }

            HSSFSheet worksheet = null;
            //loop through sheets
            for (int i = 0; i < workbook.NumberOfSheets; i++)
                if (workbook.GetSheetAt(i).SheetName == sheet.SheetName)
                {
                    worksheet = workbook.GetSheetAt(i) as HSSFSheet;
                    break;
                }
            if (worksheet == null)
                throw new Exception(LocalResources.CannotFindTheDesiredWorksheetInTheDocument);

            //get the first sheet
            //worksheet = workbook.GetSheetAt(0) as HSSFSheet;
            IEnumerator rows = worksheet.GetRowEnumerator();

            IRow headerRow = worksheet.GetRow(0);
            int cellCount = headerRow.LastCellNum;

            //loop through cols
            for (int j = 0; j < cellCount; j++)
            {
                ICell cell = headerRow.GetCell(j);
                if (cell == null || cell.StringCellValue != columnsExpected[j])
                    throw new Exception(LocalResources.TheColumn + columnsExpected[j] + LocalResources.CannotBeFoundInTheDocumentInTheSheet + sheet.SheetName);

                dt.Columns.Add(cell.StringCellValue);
            }

            for (int i = (worksheet.FirstRowNum + 1); i < worksheet.PhysicalNumberOfRows; i++)
            {
                IRow row = worksheet.GetRow(i);
                DataRow dataRow = dt.NewRow();

                // check if whole row is empty. if so, ignore it
                var allrowempty = true;
                for (int j = 0; j < cellCount; j++)
                {
                    var columnValue = row.GetCell(j);
                    if(columnValue != null && !string.IsNullOrWhiteSpace(columnValue.ToString()))
                    {
                        allrowempty = false;
                        break;
                    }
                }
                if (allrowempty)
                    continue;

                // add the row
                for (int j = 0; j < cellCount; j++)
                {
                    var columnName = columnsExpected[j];
                    var columnValue = row.GetCell(j);

                    if (columnRequired.Contains(columnName) && (columnValue == null || string.IsNullOrWhiteSpace(columnValue.ToString())))
                        throw new Exception(LocalResources.TheColumn + columnName + LocalResources.IsRequiredAndCannotBeEmptyErrorAtRow + i + ", "+LocalResources.InTheSheet + sheet.SheetName);
                    
                    dataRow[j] = columnValue?.ToString();
                }

                dt.Rows.Add(dataRow);
            }
            return dt;
        }

        #region Utilities
        /// <summary>
        /// Reads the XLS File from disk (assumes the filepath is set in ExcelData object) and opens it in the WB object.
        /// </summary>
        private void ReadXls()
        {
            using (var fs = new FileStream(ExcelData.FilePath, FileMode.Open, FileAccess.Read))
            {
                wb = new HSSFWorkbook(fs);
            }
        }

        /// <summary>
        /// Saves the current document on the disk.
        /// </summary>
        private void SaveXls()
        {
            using (var fs = new FileStream(ExcelData.FilePath, FileMode.Create, FileAccess.Write))
            {
                wb.Write(fs);
            }
        }

        /// <summary>
        /// The .xls files have a max rows per sheet limit. This method checks if that max rows is achieved and if so, generates another sheet do add data in
        /// </summary>
        /// <param name="sheet">The current Sheet. Will be re-referenced to a new sheet if the number of rows exceeds the maximum.</param>
        /// <param name="sheetMetadata">The data about this sheet.</param>
        private void PrepareWorksheetForMaxRows(ref ISheet sheet, Sheet sheetMetadata)
        {
            if (currentRow <= 65535)
            {
                currentRow++;
                return;
            }

            //executes if the number of rows exceeds the sheet limit
            var result = Regex.Match(sheet.SheetName, @"\d+$").Value;
            var newSheetName = sheet.SheetName;
            if (!string.IsNullOrWhiteSpace(result))
            {
                var number = int.Parse(result);
                var sheetBaseName = newSheetName.Substring(0, newSheetName.Length - result.Length);
                for (int i = 0; i < wb.NumberOfSheets; i++)
                {
                    var thisName = wb.GetSheetAt(i).SheetName;
                    if (!thisName.StartsWith(sheetBaseName))
                        continue;
                    
                    var possibleNumber = Regex.Match(thisName, @"\d+$").Value;
                    if (string.IsNullOrWhiteSpace(possibleNumber))
                        continue;

                    var possibleNumberValue = int.Parse(possibleNumber);
                    number = possibleNumberValue > number ? possibleNumberValue : number;
                }
                //here number will be the biggest
                number++;
                newSheetName = sheetBaseName + number.ToString();
            }
            else
            {
                newSheetName = sheet.SheetName + "1";
            }

            sheetMetadata.SheetName = newSheetName;
            sheet = AddSheet(sheetMetadata);
            currentRow = 0;
        }
        #endregion
    }
}
