﻿using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Services.Extensions;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Globalization;
using System.Linq;
using Promis.NET.Localization;
using Promis.Web.DataAccess.Entities.PromisWeb;
using Promis.NET.Services.Models;
using Promis.NET.Services.Models.PlanningAndProgress;

namespace Promis.NET.Services.Services
{
    public class ProgressService : Service, IProgressService
    {
        private IProgressRepository _progressRepository;
        private IProjectRepository _projectRepository;
        private IDashboardRepository _dashboardRepository;
        private IPdfCreatorPlanningAndProgress _pdfCreator;

        public ProgressService(IProgressRepository progressRepository, IProjectRepository projectRepository, IDashboardRepository dashboardRepository, IPdfCreatorPlanningAndProgress pdfCreator) : base(progressRepository, projectRepository, dashboardRepository)
        {
            _progressRepository = progressRepository;
            _projectRepository = projectRepository;
            _dashboardRepository = dashboardRepository;
            _pdfCreator = pdfCreator;
        }

        public Dictionary<Activity, List<SubActivity>> GetQuantitiesData(string mainActivity)
        {
            var output = new Dictionary<Activity, List<SubActivity>>();
            var tables = _projectRepository.GetMainActivityTables(mainActivity);
            var allData = _progressRepository.GetQuantitiesData(mainActivity);

            foreach (var data in allData)
            {
                var activity = new Activity(data);
                if (!output.ContainsKey(activity))
                    output.Add(activity, new List<SubActivity>());

                var subActivity = new SubActivity(data);
                subActivity.QuantityActual = _progressRepository.GetSumQuantityForSubactivityFromTables(tables, subActivity.Code);
                output[activity].Add(subActivity);
            }

            return output;
        }

        public void UpdateSubactivitiesQuantities(IEnumerable<SubActivity> subactivitiesData)
        {
            foreach (var subactivity in subactivitiesData)
            {
                var entityData = subactivity.GetBudgetSubactEntity();
                try
                {
                    _progressRepository.UpdateById(entityData, "sub_act", entityData.sub_act, null, new string[] { "qty", "mhrs" });
                }
                catch (ObjectNotFoundException)
                {
                    // this is fine. was trying to update something not in the database.
                }
            }
        }



        public string GetTableNameProgress(string projectDatabase, string mainActivity)
        {
            var year = DateTime.Now.YearAsString();
            var weekNumber = DateTime.Now.WeekOfTheYearAsTwoDigitString();
            return _progressRepository.GetProgressTableName(projectDatabase, mainActivity, year, weekNumber);
        }

        public string RunPrerequisitesForProgressTables(string projectDatabase, string mainActivity, string subActivity)
        {
            var tables = _projectRepository.GetMainActivityTables(mainActivity);
            var year = DateTime.Now.YearAsString();
            var weekNumber = DateTime.Now.WeekOfTheYearAsTwoDigitString();

            // run the two pre-requisite methods (one that assure the progress table exists, the other that copies data from real tables to it).
            _progressRepository.CreateProgressTableForWeek(projectDatabase, mainActivity, year, weekNumber);
            _progressRepository.CopyDataFromRealTablesToProgress(projectDatabase, mainActivity, year, weekNumber, subActivity, tables);

            return _progressRepository.GetProgressTableName(projectDatabase, mainActivity, year, weekNumber);
        }

        public PercentData[] GetPlanningDataForSubactivity(string projectDatabase, string mainActivity, string fullActivity, string subActivity)
        {
            var year = DateTime.Now.YearAsString();
            var weekNumber = DateTime.Now.WeekOfTheYearAsTwoDigitString();
            var tableName = _progressRepository.GetProgressTableName(projectDatabase, mainActivity, year, weekNumber);
            var totalMhrsPerSub = _dashboardRepository.GetManHoursForProject(subActivity, DataAccess.Repositories.ActivityType.Sub);
            var totalMhrsPerActivity = _dashboardRepository.GetManHoursForProject(fullActivity, DataAccess.Repositories.ActivityType.Full);

            //SetContext(_projectRepository, "project", projectDatabase);
            //var subActivities = _projectRepository.GetSubActivities(fullActivity).Select(a => a.SUB_ACT);

            var output = new PercentData[2];
            //output[0] = _progressRepository.GetTrendsDataForSubActivity(projectDatabase, tableName, mainActivity, subActivity, totalMhrsPerSub, weekNumber, year, subActivities, totalMhrsPerActivity);
            output[0] = _dashboardRepository.GetTrendsActualData(totalMhrsPerActivity, year, weekNumber, fullActivity);
            output[0].PercentMonthly = _dashboardRepository.GetTrendsMonthlyData(totalMhrsPerActivity, output[0].Month, fullActivity).PercentMonthly;

            output[1] = _progressRepository.GetTrendsDataForSubActivity(projectDatabase, tableName, mainActivity, subActivity, totalMhrsPerSub, weekNumber, year);
            return output;
        }

        public double UpdateSubactivityProgress(string projectDatabase, string mainActivity, string subActivity, string tag, string columnS, string valueS)
        {
            var year = DateTime.Now.YearAsString();
            var weekNumber = DateTime.Now.WeekOfTheYearAsTwoDigitString();
            var tableName = _progressRepository.GetProgressTableName(projectDatabase, mainActivity, year, weekNumber);

            // get subactivity work data
            SetContext(_projectRepository, "project", projectDatabase);
            var subactivities = _projectRepository.GetAllDataForSubActivity(subActivity);

            var canBeDouble = double.TryParse(valueS, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out double number);
            if (!canBeDouble)
                throw new Exception(LocalResources.InvalidNumberSpecified);
            if (number < 0 || number > 100)
                throw new Exception(LocalResources.NumberMustBeAValidPercent);

            // run updates
            _progressRepository.UpdateSubActivityProgressColumn(tableName, subActivity, tag, columnS, valueS);
            _progressRepository.FinishTransaction();// saves the updates (need to be used on next step)
            return _progressRepository.UpdateSubActivityProgressPercent(tableName, subActivity, subactivities, tag);
        }

        //for export Pdf document
        public string ExportProgressDataToPdf(string serverDirectory, string projectId, string table, string subactivityCode)
        {
            var fullSubactData = _progressRepository.GetSubactivityData(subactivityCode);
            var projectData = _projectRepository.GetById<project, string>("proj", projectId);
            var projectObject = new Project(projectData);
            var totalBudget = _progressRepository.GetTotalBudget(subactivityCode).ToString();
            var allDataForFirstTable= new List<SubActivityProgress>();
            var dbList = _projectRepository.GetAllDataForSubActivity(subactivityCode);

            foreach (var subactivity in dbList)
            {
                allDataForFirstTable.Add(new SubActivityProgress(subactivity));
            }
            
            var inspctProgr = new ProgressSteps();        
            inspctProgr.SetSubactivities(allDataForFirstTable);
            inspctProgr.UM = dbList[0].UM;
            
            var allProgressData = _progressRepository.GetProgressAllData(table, subactivityCode);
            
            var qtyTag = new List<ProgressData>();
           
            foreach (var progressData in allProgressData)
            {
                var tag = progressData.tag;
                var qty = Convert.ToDouble(progressData.qty);
                var progressDt = new ProgressData()
                {
                    Tag = progressData.tag,
                    S1 = progressData.s1==null ? string.Empty:progressData.s1,
                    S2 = progressData.s2 == null ? string.Empty : progressData.s2,
                    S3 = progressData.s3 == null ? string.Empty : progressData.s3,
                    S4 = progressData.s4 == null ? string.Empty : progressData.s4,
                    S5 = progressData.s5 == null ? string.Empty : progressData.s5,
                    S6 = progressData.s6 == null ? string.Empty : progressData.s6,
                    S7 = progressData.s7 == null ? string.Empty : progressData.s7,
                    S8 = progressData.s8 == null ? string.Empty : progressData.s8,
                    S9 = progressData.s9 == null ? string.Empty : progressData.s9,
                    S10 = progressData.s10 == null ? string.Empty : progressData.s10,
                    S11 = progressData.s11 == null ? string.Empty : progressData.s11,
                    S12 = progressData.s12 == null ? string.Empty : progressData.s12,
                    Wbs = "000",
                    Pr = progressData.pr.HasValue ? progressData.pr.Value : 0,
                    SubAct = progressData.subact,
                    Mhrs = progressData.mhrs,
                    Qty = progressData.qty.Replace(",","."),
                    DataTable = progressData.datatable,
                    Equiv = progressData.equiv.HasValue ? progressData.equiv.Value.ToString() : "0"//string.Empty,
                };
                qtyTag.Add(progressDt);
            }

            string docToAttach = null;
            var clientHidden = _projectRepository.ClientHidden();
            _pdfCreator.SetData(serverDirectory, fullSubactData, projectObject, inspctProgr, qtyTag, clientHidden, totalBudget, docToAttach);
            return _pdfCreator.CreatePdf();
        }
    }
}
