﻿using Promis.NET.DataAccess.DbContexts;
using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Services.Services.Interfaces;
using System.Collections.Generic;
using Promis.NET.Localization;

namespace Promis.NET.Services.Services
{
    public class Service : IService
    {
        protected string _projectname;
        protected List<IRepository> _repositories;

        public Service(IRepository repository)
        {
            _repositories = new List<IRepository>();
            if(repository != null)
                _repositories.Add(repository);
        }

        public Service(params IRepository[] repositories)
        {
            _repositories = new List<IRepository>();
            if (repositories != null)
                _repositories.AddRange(repositories);
        }

        public T GetRepository<T>() where T: class
        {
            foreach (var repo in _repositories)
            {
                if (repo.GetType() == typeof(T))
                    return repo as T;
            }

            return null;
        }
        
        public void SetWorkingProject(string projectName)
        {
            _projectname = projectName;
            foreach (var repo in _repositories)
            {
                if(repo.DbContext.GetType() != typeof(PromisWebDbContext))
                    repo.ChangeDatabase(projectName);
            }
        }
        
        /// <summary>
        /// Sets the Context of the Database. So we can do operations either on the project's own database, or on the general database.
        /// </summary>
        /// <param name="context">Can be 'general' (default) for the general database usage or 'project' for the project's database.</param>
        public void SetContext(string context = "default", string databasename = "")
        {
            foreach (var repo in _repositories)
            {
                switch (context)
                {
                    case "default":
                        if(repo.DbContext.GetType() != typeof(PromisWebDbContext))
                            repo.ChangeDbContext(new PromisWebDbContext(repo.DbContext.IsLocalDb));
                        break;
                    case "project":
                        if(repo.DbContext.GetType() != typeof(ProjectDbContext))
                            repo.ChangeDbContext(new ProjectDbContext(databasename, repo.DbContext.IsLocalDb));
                        break;

                }
            }
        }

        /// <summary>
        /// Changes the context of the database for the specified repository from this service.
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="context"></param>
        /// <param name="databasename"></param>
        public void SetContext(IRepository repository, string context = "default", string databasename = "")
        {
            if (_repositories == null || !_repositories.Contains(repository))
                throw new System.Exception(LocalResources.TryingToChangeContextForInvalidRepository);

            switch (context)
            {
                case "default":
                    if (repository.DbContext.GetType() != typeof(PromisWebDbContext))
                        repository.ChangeDbContext(new PromisWebDbContext(repository.DbContext.IsLocalDb));
                    break;
                case "project":
                    if (repository.DbContext.GetType() != typeof(ProjectDbContext))
                        repository.ChangeDbContext(new ProjectDbContext(databasename, repository.DbContext.IsLocalDb));
                    break;

            }
        }
    }
}
