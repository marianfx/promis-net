﻿using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Services.Extensions;
using Promis.NET.Services.Models.Preservation;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Models;
using System;
using System.Collections.Generic;

namespace Promis.NET.Services.Services
{
    public class PreservationService : Service, IPreservationService
    {
        private IPreservationRepository _preservationRepository;

        public PreservationService(IPreservationRepository preservationRepository) : base(preservationRepository)
        {
            _preservationRepository = preservationRepository;
        }

        public IPreservationRepository GetRepository()
        {
            return _preservationRepository;
        }

        #region All Tabs
        public List<string> GetDatatableNames(bool isFollowUp)
        {
            return _preservationRepository.GetDatatableNames(isFollowUp);
        }

        public Dictionary<string, List<string>> GetTypecodesNames(string dataTable)
        {
            return _preservationRepository.GetDatatableTypecodes(dataTable); ;
        }

        public string GetTableNameTypecodes()
        {
            return _preservationRepository.GetTableNameTypecodes();
        }
        #endregion All Tabs

        #region Plan Overview
        public Dictionary<string, List<PreservationDetails>> GetAllDataForPreservation(string code)
        {
            var output = new Dictionary<string, List<PreservationDetails>>();
            var dataPreservation = _preservationRepository.GetPreservationData(code);

            foreach (var row in dataPreservation)
            {
                var title = row.description;
                var currentRow = new PreservationDetails()
                {
                    Activity = row.activity,
                    Sequence = row.seq.ToString(),
                    Frequency = row.frequency,
                    JobPlan = row.jp
                };

                if (!output.ContainsKey(title))
                {
                    output.Add(title, new List<PreservationDetails>());
                }

                output[title].Add(currentRow);
            }
            return output;
        }
        #endregion Plan Overview

        #region Job Assignment tab
        public List<string> GetLocations()
        {
            return _preservationRepository.GetTagsLocation();
        }

        public string GetFrequencyForPreservation(string code)
        {
            return _preservationRepository.GetPreservationFrequency(code);
        }

        public void DeleteTags(string datatable, string tag)
        {
            _preservationRepository.DeleteTags(datatable, tag);
        }

        public string GetYearWeek(DateTime fullDate)
        {
            var year = fullDate.YearAsString();
            var week = fullDate.WeekOfTheYearAsTwoDigitString();
            var year_week = year + "_" + week;

            return year_week;
        }

        public void SaveChangesToTags(string dataTable, PreservationAssignment preservation, string rowCode)
        {
            var presMaintainWeek = string.Empty;
            var presInitWeek = string.Empty;
            var presRestorWeek = string.Empty;
            var cutOffWeeks = new List<string>();
            var tags = preservation.Tags;

            for (int i = 0; i < tags.Count; i++)
            {
                if (preservation.InitialisationDate != null && preservation.InitialisationDate != DateTime.MinValue)
                {
                    presInitWeek = GetYearWeek(preservation.InitialisationDate.Value);
                    _preservationRepository.SaveTagChanges(dataTable, presInitWeek, "IN", tags[i], rowCode);
                }

                if (preservation.RestorationDate != null && preservation.RestorationDate != DateTime.MinValue)
                {
                    presRestorWeek = GetYearWeek(preservation.RestorationDate.Value);
                    _preservationRepository.SaveTagChanges(dataTable, presRestorWeek, "RE", tags[i], rowCode);
                }

                if (preservation.MaintenanceDate != null && preservation.MaintenanceDate != DateTime.MinValue)
                {
                    presMaintainWeek = GetYearWeek(preservation.MaintenanceDate.Value);
                    //method for selecting data from cut-off table
                    cutOffWeeks = _preservationRepository.GetCutOffW(presMaintainWeek, presInitWeek, presRestorWeek);
                    for (int j = 0; j < cutOffWeeks.Count; j++)
                    {
                        _preservationRepository.SaveTagChanges(dataTable, cutOffWeeks[j], "MP", tags[i], rowCode);
                    }
                }
            }
        }
        #endregion Job Assignment tab

        #region Preservation Follow Up
        public Dictionary<string, PreservationActivities> GetScheduledWeeksForPreservation(string datatable, string tagVal)
        {
            var output = new Dictionary<string, PreservationActivities>();
            var dataPreserFollowUp = _preservationRepository.GetScheduledAndDoneWeek(datatable, tagVal);

            foreach (var row in dataPreserFollowUp)
            {
                var jp = row.jp;
                var currentRow = new PreservationActivities()
                {
                    DoneWeek = row.donew,
                    ScheduledWeek = row.schedw
                };

                if (!output.ContainsKey(jp))
                {
                    output.Add(jp, new PreservationActivities());
                }
                output[jp] = currentRow;
            }
            return output;
        }

        public Dictionary<string, PreservationSchedWeekInfo> GetCutOffAndSchedule(string dataTable, string schedVal, string tagVal)
        {
            var output = new Dictionary<string, PreservationSchedWeekInfo>();
            var sched = _preservationRepository.GetScheduledWeek(dataTable, schedVal, tagVal);
            if (sched.Count <= 0)
                return output;
            
            foreach (var row in sched)
            {
                var currentRow = new PreservationSchedWeekInfo()
                {
                    Count = row.count,
                    ScheduledWeek = row.schedw.ToString(),
                    Tag = row.tag.ToString(),
                    Code = row.code.ToString()
                };
                var resCut = _preservationRepository.GetCutOffValue(currentRow.ScheduledWeek);

                if (!output.ContainsKey(resCut))
                {
                    output.Add(resCut, new PreservationSchedWeekInfo());
                }
                output[resCut] = currentRow;
            }          
            return output;
        }

        public Dictionary<string, List<PreservationActions>> GetPreservationActions(string dataTable, string code, string tagVal, string schedVal)
        {
            var output = new Dictionary<string, List<PreservationActions>>();

            var dataPreservation = _preservationRepository.GetPreservationInfo(dataTable, code, tagVal, schedVal);
            if (dataPreservation.Count <= 0)
                return output;
           
            foreach (var row in dataPreservation)
            {
                var description = row.description;
                var codePres = row.code;
                var title = codePres + "-" + description;

                var act = row.activity.Replace("vbcrlf", "<br>").Replace("<br><br>", "<br>");
                var actSubstr = act.Substring(0, 4);
                if (actSubstr == "<br>")
                    act = act.Substring(5);

                var currentRow = new PreservationActions()
                {
                    Activity = act,
                    Sequence = row.seq,
                    Done = row.donew,
                    Nr= row.nr,
                    Schedw=row.schedw,
                    Description=description,
                    Code=row.code
                };

                if (!output.ContainsKey(title))
                {
                    output.Add(title, new List<PreservationActions>());
                }
                output[title].Add(currentRow);
            }
            return output;
        }

        public void SaveActivtiesStatusChanges(string dataTable, List<DataKeeper> preservation, string tag, string code, string schedw)
        {
            if (preservation.Count <= 0)
                return;
            
            for(int i=0;i<preservation.Count;i++)
            {
                _preservationRepository.SaveActivitiesStatusChanges(dataTable, preservation[i].ColumnName, preservation[i].Value, tag, code, schedw);
            }           
        }
        #endregion Preservation Follow Up
    }
}
