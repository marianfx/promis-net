﻿using Promis.NET.DataAccess.Repositories.Interfaces;
using Promis.NET.Services.Extensions;
using Promis.NET.Services.Models.Certification;
using Promis.NET.Services.Models.Commissioning;
using Promis.NET.Services.Models.Project;
using Promis.NET.Services.Services.Interfaces;
using Promis.NET.Utilities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Promis.NET.Localization;
using Promis.Web.DataAccess.Entities.PromisWeb;

namespace Promis.NET.Services.Services
{
    public class CertificationService : Service, ICertificationService
    {
        private ICertificationRepository _certificationRepository;
        private IProjectRepository _projectRepository;
        private ICommissioningRepository _commissioningRepository;
        private IProgressRepository _progressRepository;
        private IPdfCreatorCertification _pdfCreator;

        public CertificationService(ICertificationRepository certificationRepository, IProjectRepository projectRepository, ICommissioningRepository commissioningRepository, IProgressRepository progressRepository, IPdfCreatorCertification pdfCreator) : base(certificationRepository, projectRepository, commissioningRepository, progressRepository)
        {
            _certificationRepository = certificationRepository;
            _projectRepository = projectRepository;
            _commissioningRepository = commissioningRepository;
            _progressRepository = progressRepository;
            _pdfCreator = pdfCreator;
        }

        public ICertificationRepository GetRepository()
        {
            return _certificationRepository;
        }

        #region Tags Assignment Status
        public List<Datatables> GetTablesForTag(string code)
        {
            var tables = new List<Datatables>();
            var result = _certificationRepository.GetTablesForTag(code);
            foreach (var item in result)
            {
                var tbl = new Datatables()
                {
                    Datatable = item.datatable,
                    Description = item.description
                };
                tables.Add(tbl);
            }
            return tables;
        }

        public Dictionary<string, CertificationTagsAssignmentStatus> GetCertificationCompletionData()
        {
            var output = new Dictionary<string, CertificationTagsAssignmentStatus>();
            var dataTblInfo = _certificationRepository.GetDataTablesForStatusTab();
            if (dataTblInfo.Count == 0)
                return output;

            foreach (var item in dataTblInfo)
            {
                var code = item.cod;
                var descr = item.descr;

                var datatable = _certificationRepository.GetTablesForTag(code);
                var tagsNotAssigned = 0.0;
                var tagsAssigned = 0.0;
                var totalTags = 0.0;
                var percent = 0.0;

                foreach (var elem in datatable)
                {
                    var tags = _certificationRepository.GetDataTableCount(elem.datatable);
                    tagsAssigned = tagsAssigned + tags.Key;
                    tagsNotAssigned = tagsNotAssigned + tags.Value;
                    totalTags = tagsAssigned + tagsNotAssigned;
                }

                if (totalTags > 0)
                {
                    percent = Math.Round((tagsAssigned / totalTags) * 100, 1);

                    var dtInfo = new CertificationTagsAssignmentStatus()
                    {
                        TagsAssigned = tagsAssigned,
                        TagsNotAssigned = tagsNotAssigned,
                        Progress = percent,
                        Description = descr,
                        Datatable = code
                    };
                    if (!output.ContainsKey(descr))
                        output.Add(descr, new CertificationTagsAssignmentStatus());
                    output[descr] = dtInfo;
                }
            }
            return output;
        }

        public List<List<DataKeeper>> GetAllUnassignedTags(string code)
        {
            var allRowsForAllTables = new List<List<DataKeeper>>();
            var tables = GetTablesForTag(code);

            foreach (var table in tables)
            {
                var rowsForThisTable = _certificationRepository.GetRowsForTagsNotAssigned(table.Datatable, table.Description);
                allRowsForAllTables.AddRange(rowsForThisTable);
            }
            return allRowsForAllTables;
        }
        #endregion Tags Assignment Status

        #region Assign SOW to SubContractors
        public List<Subcontractor> GetSubcontractors(string projectDatabase)
        {
            SetContext("project", projectDatabase);
            var subsDb = _projectRepository.GetAllSubcontractors();
            var output = new List<Subcontractor>();
            foreach (var sub in subsDb)
            {
                output.Add(new Subcontractor(sub));
            }
            return output;
        }

        public Dictionary<string, double> GetMainActivityBudget()
        {
            return _certificationRepository.GetMainActivityBudget();
        }

        public List<CertificationSubcontractorsAssignment> GetMainActivitiesDetails(string activityId)
        {
            var disabled = string.Empty;
            var productivity_budget = "";
            var activitiesDetails = new List<CertificationSubcontractorsAssignment>();

            if (activityId == null)
                return activitiesDetails;

            var subactivities = _certificationRepository.GetSubActivities(activityId);
            foreach (var subActivity in subactivities)
            {
                productivity_budget = Math.Round(subActivity.prod, 2) + " " + subActivity.um + "/h";
                var currentActivity = new CertificationSubcontractorsAssignment()
                {
                    SubActivities = subActivity.sub_act,
                    SubActivitiesDescription = subActivity.sub_act_d,
                    BudgetMHRS = subActivity.mhrs,
                    Productivity = productivity_budget,
                    SubcontractorCode = subActivity.subcode,
                    AssignedSubcontractor = subActivity.subcontractor

                };
                activitiesDetails.Add(currentActivity);
            }
            return activitiesDetails;
        }

        public void UpdateSOWForSubcontractor(IEnumerable<SOWForSubcontractors> updateList)
        {
            var toDelete = updateList.Where(x => !x.Value);
            var toInsert = updateList.Where(x => x.Value);
            foreach (var item in toDelete)
            {
                _certificationRepository.DeleteSOWForSubcontractor(item.Code, item.SubActivity);
            }
            foreach (var item in toInsert)
            {
                _certificationRepository.InsertSOWForSubcontractor(item.Code, item.SubActivity);
            }
        }
        #endregion Assign SOW to SubContractors

        #region Assign plan to tags
        public List<Datatables> GetDatatables(string mainActivity)
        {
            var output = new List<Datatables>();
            var dbList = _certificationRepository.GetDatatables(mainActivity);
            foreach (var item in dbList)
            {
                output.Add(new Datatables(item));
            }
            return output;

        }

        public List<Activity> GetSubactivityByMainActivity(string mainActivity)
        {
            var output = new List<Activity>();
            var dbList = _certificationRepository.GetSubactivityForPlans(mainActivity);
            foreach (var item in dbList)
            {
                var act = new Activity();
                act.Code = item.act;
                act.Description = item.description;
                output.Add(act);
            }
            return output;

        }

        public List<DataKeeper> GetColumnsNecessaryForTagsTable()
        {
            return _certificationRepository.GetColumnsForTableForPlanToTags();
        }
        #endregion

        #region Certification Summary
        public List<Datatables> GetDatatablesByActivity(string activity)
        {
            List<Datatables> result = new List<Datatables>();
            var tables = _certificationRepository.GetDatatablesByActivity(activity);
            foreach (var table in tables)
            {
                result.Add(new Datatables(table));
            }
            return result;
        }

        public KeyValuePair<List<CertificationSummarySteps>, ActivityBase> GetStatusOfSelectedRow(string projectID, string datatable, string rowId)
        {
            var keyresult = new List<CertificationSummarySteps>();
            var subactDetails = new ActivityBase();
            var subatAndTag = _commissioningRepository.GetTagAndSubAct(datatable, rowId, "");
            var actDescriptionAndMax = _commissioningRepository.GetSubactForFollowUpSelectedRow(subatAndTag.activity);
            subactDetails.Description = actDescriptionAndMax.activity;
            subactDetails.Code = subatAndTag.activity;
            var users = _certificationRepository.GetUsers(projectID, subatAndTag.activity.Substring(0, 2));
            foreach (var item in _certificationRepository.GetAllForInspectionStatus("cert_" + datatable, subatAndTag.tag))
            {
                var tempUsers = new List<string>(users);
                if (!users.Contains(item.c_user)) { tempUsers.Add(item.c_user); }
                var fileAvail = string.Empty;
                if (item.ord.Equals(actDescriptionAndMax.maxOrd))
                {
                    fileAvail=(item.code.Equals(string.Empty)) ? LocalResources.TagDossierNotAvailable : LocalResources.TagDossierUploadedAt  + item.code;
                }
                keyresult.Add(new CertificationSummarySteps()
                {
                    Description = item.inspection,
                    Subcontractor1 = new KeyValuePair<string, string>(item.s_user, item.SColumn),
                    Contractor = new KeyValuePair<string, string>(item.c_user, item.code),
                    Client = new KeyValuePair<string, string>(item.o_user, item.OColumn),
                    FileText = fileAvail,
                    UsersList = tempUsers,
                    Ord = item.ord
                });

            }
            return new KeyValuePair<List<CertificationSummarySteps>, ActivityBase>(keyresult, subactDetails);
        }

        public string UpdateCertTableByUser(string projectDatabase, string datatable, string rowId, string ord, string userValue)
        {
            bool pass = false;
            var activityAndTag = _commissioningRepository.GetTagAndSubAct(datatable, rowId, "");
            var tag = activityAndTag.tag;
            var mainActivity = activityAndTag.activity;
            var year = DateTime.Now.YearAsString();
            var weekNumber = DateTime.Now.WeekOfTheYearAsTwoDigitString();
            var tableName = _progressRepository.GetProgressTableName(projectDatabase, mainActivity.Substring(0,2), year, weekNumber);
            var certRows = _certificationRepository.GetIdAndPosFromCerttable(datatable, tag, ord);

            var inputs = new Dictionary<string, object>();                                                           //
            inputs.Add("C_USER", userValue);                                                                         //This block of code was inside the following
            inputs.Add("C", (string.IsNullOrEmpty(userValue)) ? "" : String.Format("{0:dd/MM/yyyy}",DateTime.Today));// Foreach,  but that would just update the same row 
            _certificationRepository.UpdateCertTableByUser(inputs, "cert_" + datatable, tag, mainActivity, ord);     // with the same values over and over for each row!!!!!

            foreach (var row in certRows)
            {
                int pos = 0;
                int.TryParse(row.pos, out pos);
                if (pos > 0 && !string.IsNullOrEmpty(userValue))
                {
                    _certificationRepository.UpdateProgressTable(tableName, pos.ToString(), mainActivity, tag);
                    pass = true;
                }
            }
            if (pass)
            {
                double percentage = 0;
                var bigTableRow = _certificationRepository.GetProgressTableRows(tableName, tag, mainActivity).Rows[0];
                var workDefRows = _certificationRepository.GetPosAndPRogr(mainActivity);
                foreach (var row in workDefRows)
                {
                    var rowValue = bigTableRow["s" + row.POS].ToString();
                    if (!string.IsNullOrEmpty(rowValue))
                    {
                        rowValue=rowValue.Replace(',', '.');
                        var sw = double.Parse(row.PROGR);
                        var number = double.Parse(rowValue);
                        percentage = percentage + (sw * number / 100);
                    }
                }
                _certificationRepository.UpdatePercentageInProgressTable(tableName, percentage.ToString(), tag, mainActivity);
            }
            return "Saved";

        }

        public string ExportCertificationDataToPdf(string serverDirectory, string projectId, string table, string subactivityCode, string tag)
        {
            var fullSubactData = _certificationRepository.GetSubactivityData(subactivityCode);
            var projectData = _projectRepository.GetById<project, string>("proj", projectId);
            var projectObject = new Project(projectData);
            var allData = _certificationRepository.GetCertificationWorkDefData(table, subactivityCode, tag);

            //compute date if it has been certified
            string docToAttach = null;
            var docDate = "";// DateTime.Now.ToString("dd/MM/yyyy");
            var maxOrd = _certificationRepository.GetMaxOrdForSubact(subactivityCode);
            if (!string.IsNullOrWhiteSpace(tag) && !string.IsNullOrWhiteSpace(maxOrd))
            {
                var dataWithMaxOrd = allData.FirstOrDefault(x => x.ord == maxOrd);
                if (dataWithMaxOrd != null && !string.IsNullOrWhiteSpace(dataWithMaxOrd.CColumn))
                {
                    docDate = dataWithMaxOrd.CColumn;
                }
                if(dataWithMaxOrd != null)
                {
                    allData.Remove(dataWithMaxOrd);
                }
            }
            if (!string.IsNullOrWhiteSpace(docDate))
            {
                _pdfCreator.DateToAttach = docDate;
                if (table == "testpack")
                    docToAttach = "documents\\attach.pdf";
                else
                    docToAttach = "documents\\ag_attach.pdf";
            }
            var signingUsers = new List<string>();
            var sUser = allData.LastOrDefault(x => !string.IsNullOrWhiteSpace(x.s_user))?.s_user;
            if (!string.IsNullOrWhiteSpace(sUser))
                signingUsers.Add(sUser);
            var cUser = allData.LastOrDefault(x => !string.IsNullOrWhiteSpace(x.c_user))?.c_user;
            if (!string.IsNullOrWhiteSpace(cUser))
                signingUsers.Add(cUser);
            var oUser = allData.LastOrDefault(x => !string.IsNullOrWhiteSpace(x.o_user))?.o_user;
            if (!string.IsNullOrWhiteSpace(oUser))
                signingUsers.Add(oUser);

            var dataToTransmit = allData.Select(c => new CertificationSummarySteps(c)).ToList();
            _pdfCreator.SetData(serverDirectory, fullSubactData, tag, projectObject, dataToTransmit, docToAttach, signingUsers.ToArray());

            return _pdfCreator.CreatePdf();
        }

        #endregion

    }
}
